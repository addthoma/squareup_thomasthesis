.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->sendProxyMessageToServer(Ljava/lang/String;[BLjava/lang/String;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lio/reactivex/SingleSource<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connId:Ljava/lang/String;

.field final synthetic $tmnData:[B

.field final synthetic $transactionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$tmnData:[B

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$connId:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$transactionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lio/reactivex/Single;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
            ">;>;"
        }
    .end annotation

    .line 490
    sget-object v0, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$tmnData:[B

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object v0

    .line 491
    new-instance v1, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;-><init>()V

    .line 492
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$connId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;

    move-result-object v1

    .line 493
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$transactionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;

    move-result-object v1

    .line 494
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->packet_data(Lokio/ByteString;)Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;

    move-result-object v1

    .line 495
    invoke-virtual {v1}, Lcom/squareup/protos/client/felica/ProxyMessageRequest$Builder;->build()Lcom/squareup/protos/client/felica/ProxyMessageRequest;

    move-result-object v1

    .line 497
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v2

    sget-object v3, Lcom/squareup/tmn/What;->WORKFLOW_SEND_TO_SERVER:Lcom/squareup/tmn/What;

    invoke-virtual {v2, v3}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 499
    new-instance v2, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    const-wide/16 v3, -0x1

    iput-wide v3, v2, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    .line 501
    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-virtual {v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getMiryoWorkerDelayer()Lcom/squareup/tmn/MiryoWorkerDelayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/tmn/MiryoWorkerDelayer;->maybeDelayAfterFirstRoundTripAfterMiryo()V

    .line 502
    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getFelicaService$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/server/felica/FelicaService;

    move-result-object v3

    const-string v4, "request"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->$transactionId:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v5}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getEnableFelicaCertEnv$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v3, v1, v4, v5}, Lcom/squareup/server/felica/FelicaService;->proxyMessage(Lcom/squareup/protos/client/felica/ProxyMessageRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;

    move-result-object v1

    .line 503
    invoke-virtual {v1}, Lcom/squareup/server/felica/FelicaService$ProxyMessageStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 504
    new-instance v3, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;

    invoke-direct {v3, p0, v2, v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;Lkotlin/jvm/internal/Ref$LongRef;Lokio/ByteString;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v3}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v4

    const-string v0, "felicaService.proxyMessa\u2026  )\n          )\n        }"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 525
    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 526
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getMainScheduler$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lio/reactivex/Scheduler;

    move-result-object v9

    const/4 v5, 0x1

    const-wide/16 v6, 0x1

    .line 522
    invoke-static/range {v4 .. v9}, Lcom/squareup/receiving/StandardReceiverKt;->retryExponentialBackoff(Lio/reactivex/Single;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;->call()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
