.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "+",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "it",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connectionId:Ljava/lang/String;

.field final synthetic $transactionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->$transactionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->$connectionId:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TMN: playing audio "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;->getAudioRequest()Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 701
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->$transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->complete(Ljava/lang/String;)V

    .line 702
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v0

    .line 703
    new-instance v10, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 704
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->PLAYED_AUDIO_MESSAGE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->$transactionId:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->$connectionId:Ljava/lang/String;

    .line 705
    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;->getAudioRequest()Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->name()Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x24

    const/4 v9, 0x0

    move-object v1, v10

    .line 703
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 702
    invoke-virtual {v0, v10}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 708
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;->invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
