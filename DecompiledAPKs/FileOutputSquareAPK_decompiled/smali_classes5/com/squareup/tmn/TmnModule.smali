.class public abstract Lcom/squareup/tmn/TmnModule;
.super Ljava/lang/Object;
.source "TmnModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0005\u001a\u00020\u000eH!\u00a2\u0006\u0002\u0008\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnModule;",
        "",
        "()V",
        "provideTmnObservablesHelper",
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "workflow",
        "Lcom/squareup/tmn/RealTmnObservablesHelper;",
        "provideTmnObservablesHelper$impl_wiring_release",
        "provideTmnTransactionWorkflow",
        "Lcom/squareup/tmn/TmnTransactionWorkflow;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow;",
        "provideTmnTransactionWorkflow$impl_wiring_release",
        "provideTmnWorkflow",
        "Lcom/squareup/tmn/TmnStarterWorkflow;",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow;",
        "provideTmnWorkflow$impl_wiring_release",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideTmnObservablesHelper$impl_wiring_release(Lcom/squareup/tmn/RealTmnObservablesHelper;)Lcom/squareup/tmn/TmnObservablesHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideTmnTransactionWorkflow$impl_wiring_release(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTransactionWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideTmnWorkflow$impl_wiring_release(Lcom/squareup/tmn/RealTmnStarterWorkflow;)Lcom/squareup/tmn/TmnStarterWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
