.class public final Lcom/squareup/tmn/DevMiryoWorkerDelayer;
.super Ljava/lang/Object;
.source "DevMiryoWorkerDelayer.kt"

# interfaces
.implements Lcom/squareup/tmn/MiryoWorkerDelayer;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\t\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\t\u001a\u00020\u0004H\u0016J\u0008\u0010\n\u001a\u00020\u0008H\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000cH\u0016J\u0008\u0010\u000e\u001a\u00020\u000cH\u0016J\u0008\u0010\u000f\u001a\u00020\u000cH\u0016J\u0008\u0010\u0010\u001a\u00020\u000cH\u0016J\u0008\u0010\u0011\u001a\u00020\u000cH\u0002J\u0010\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0013\u001a\u00020\u000c2\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\u0014\u001a\u00020\u000cH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/tmn/DevMiryoWorkerDelayer;",
        "Lcom/squareup/tmn/MiryoWorkerDelayer;",
        "()V",
        "delayMs",
        "",
        "miryoPhase",
        "Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;",
        "miryoType",
        "Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;",
        "getDelay",
        "getMiryoType",
        "maybeDelayAfterFirstRoundTripAfterMiryo",
        "",
        "maybeDelayAfterWriteNotify",
        "maybeDelayBeforeWriteNotify",
        "miryoStarted",
        "reset",
        "sendProxyMessageToServerCalled",
        "setDelay",
        "setMiryoType",
        "sleepThreadIfDelayed",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private delayMs:J

.field private miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

.field private miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;->BeforeWriteNotify:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    iput-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    .line 22
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->BeforeMiryo:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    iput-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    return-void
.end method

.method private final sendProxyMessageToServerCalled()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    sget-object v1, Lcom/squareup/tmn/DevMiryoWorkerDelayer$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    return-void

    .line 79
    :cond_0
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->Done:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    goto :goto_0

    .line 78
    :cond_1
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->BeforeSecondRoundTripAfterMiryo:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    .line 77
    :goto_0
    iput-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    return-void
.end method

.method private final sleepThreadIfDelayed()V
    .locals 5

    .line 85
    iget-wide v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->delayMs:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    const-wide/16 v2, 0x3e8

    mul-long v0, v0, v2

    .line 88
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    return-void
.end method


# virtual methods
.method public getDelay()J
    .locals 2

    .line 36
    iget-wide v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->delayMs:J

    return-wide v0
.end method

.method public getMiryoType()Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    return-object v0
.end method

.method public maybeDelayAfterFirstRoundTripAfterMiryo()V
    .locals 5

    .line 62
    iget-wide v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->delayMs:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    sget-object v1, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;->AfterFirstRoundTrip:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    sget-object v1, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->BeforeSecondRoundTripAfterMiryo:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    if-ne v0, v1, :cond_1

    .line 67
    invoke-direct {p0}, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->sleepThreadIfDelayed()V

    .line 69
    :cond_1
    invoke-direct {p0}, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->sendProxyMessageToServerCalled()V

    return-void
.end method

.method public maybeDelayAfterWriteNotify()V
    .locals 5

    .line 48
    iget-wide v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->delayMs:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    sget-object v1, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;->AfterWriteNotify:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    if-ne v0, v1, :cond_1

    .line 53
    invoke-direct {p0}, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->sleepThreadIfDelayed()V

    :cond_1
    return-void
.end method

.method public maybeDelayBeforeWriteNotify()V
    .locals 5

    .line 39
    iget-wide v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->delayMs:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    sget-object v1, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;->BeforeWriteNotify:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    if-ne v0, v1, :cond_1

    .line 43
    invoke-direct {p0}, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->sleepThreadIfDelayed()V

    :cond_1
    return-void
.end method

.method public miryoStarted()V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->BeforeFirstRoundTripAfterMiryo:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    iput-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    return-void
.end method

.method public reset()V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;->BeforeMiryo:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    iput-object v0, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoPhase:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoPhase;

    return-void
.end method

.method public setDelay(J)V
    .locals 0

    .line 33
    iput-wide p1, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->delayMs:J

    return-void
.end method

.method public setMiryoType(Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;)V
    .locals 1

    const-string v0, "miryoType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/tmn/DevMiryoWorkerDelayer;->miryoType:Lcom/squareup/tmn/MiryoWorkerDelayer$MiryoDelayType;

    return-void
.end method
