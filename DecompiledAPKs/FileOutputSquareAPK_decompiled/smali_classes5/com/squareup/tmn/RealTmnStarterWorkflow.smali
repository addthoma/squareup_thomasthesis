.class public final Lcom/squareup/tmn/RealTmnStarterWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealTmnStarterWorkflow.kt"

# interfaces
.implements Lcom/squareup/tmn/TmnStarterWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/tmn/TmnInput;",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
        "Lcom/squareup/tmn/TmnOutput;",
        "Lkotlin/Unit;",
        ">;",
        "Lcom/squareup/tmn/TmnStarterWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTmnStarterWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTmnStarterWorkflow.kt\ncom/squareup/tmn/RealTmnStarterWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,305:1\n41#2:306\n56#2,2:307\n276#3:309\n*E\n*S KotlinDebug\n*F\n+ 1 RealTmnStarterWorkflow.kt\ncom/squareup/tmn/RealTmnStarterWorkflow\n*L\n92#1:306\n92#1,2:307\n92#1:309\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008f\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003*\u0001\u000f\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00012\u00020\u0006:\u00015B\u001f\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ0\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00122\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0004H\u0002J,\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J.\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00122\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J\u001a\u0010%\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00022\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0018\u0010(\u001a\u00020)2\u0006\u0010#\u001a\u00020$2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010*\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$H\u0002J \u0010+\u001a\u00020\u00032\u0006\u0010,\u001a\u00020\u00022\u0006\u0010-\u001a\u00020\u00022\u0006\u0010.\u001a\u00020\u0003H\u0016J,\u0010/\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u00022\u0006\u0010.\u001a\u00020\u00032\u0012\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000401H\u0016J\u001a\u00102\u001a\u0002032\u0006\u0010!\u001a\u00020\"2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J\u0010\u00104\u001a\u00020\'2\u0006\u0010.\u001a\u00020\u0003H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/tmn/RealTmnStarterWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/tmn/TmnInput;",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
        "Lcom/squareup/tmn/TmnOutput;",
        "",
        "Lcom/squareup/tmn/TmnStarterWorkflow;",
        "tmnTransactionWorkflow",
        "Lcom/squareup/tmn/TmnTransactionWorkflow;",
        "cardReaderHelper",
        "Lcom/squareup/tmn/CardReaderHelper;",
        "analytics",
        "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
        "(Lcom/squareup/tmn/TmnTransactionWorkflow;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;)V",
        "teardownWorker",
        "com/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1;",
        "enterStateAndLog",
        "Lcom/squareup/workflow/WorkflowAction;",
        "newState",
        "tmnEventValue",
        "Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;",
        "emittingOutput",
        "getTmnTransactionInput",
        "Lcom/squareup/tmn/TmnTransactionInput;",
        "input",
        "transactionType",
        "Lcom/squareup/tmn/TmnTransactionType;",
        "action",
        "Lcom/squareup/tmn/Action;",
        "afterWriteNotifyData",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
        "handleStartingPaymentFailure",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "failed",
        "Lcom/squareup/tmn/TmnTransactionOutput$Failed;",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "mapFailureMiryoOutput",
        "Lcom/squareup/tmn/TmnOutput$MiryoOutput;",
        "mapFailureTmnTransactionOutput",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "shouldEnterMiryoState",
        "",
        "snapshotState",
        "TmnState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

.field private final cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

.field private final teardownWorker:Lcom/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1;

.field private final tmnTransactionWorkflow:Lcom/squareup/tmn/TmnTransactionWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/TmnTransactionWorkflow;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "tmnTransactionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->tmnTransactionWorkflow:Lcom/squareup/tmn/TmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    .line 61
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1;-><init>(Lcom/squareup/tmn/RealTmnStarterWorkflow;)V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->teardownWorker:Lcom/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1;

    return-void
.end method

.method public static final synthetic access$enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/tmn/RealTmnStarterWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    return-object p0
.end method

.method public static final synthetic access$handleStartingPaymentFailure(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionOutput$Failed;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->handleStartingPaymentFailure(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionOutput$Failed;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
            "Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;",
            "Lcom/squareup/tmn/TmnOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
            "Lcom/squareup/tmn/TmnOutput;",
            ">;"
        }
    .end annotation

    .line 271
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 272
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 273
    invoke-virtual {p2}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->name()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 271
    array-length p2, v0

    invoke-static {v0, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    const-string v0, "TMN Starter Workflow State Transition: %s Reason: %s"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "java.lang.String.format(format, *args)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v0, v2, [Ljava/lang/Object;

    .line 275
    invoke-static {p2, v0}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnStateTransitionEvent;

    invoke-direct {v1, p2}, Lcom/squareup/tmn/logging/TmnEvents$TmnStateTransitionEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 277
    sget-object p2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method static synthetic enterStateAndLog$default(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 269
    check-cast p3, Lcom/squareup/tmn/TmnOutput;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final getTmnTransactionInput(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/tmn/Action;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/tmn/TmnTransactionInput;
    .locals 8

    .line 172
    new-instance v7, Lcom/squareup/tmn/TmnTransactionInput;

    .line 173
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getAmountAuthorized()J

    move-result-wide v3

    if-eqz p4, :cond_0

    .line 174
    invoke-virtual {p4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getMiryoData()[B

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v6, p1

    move-object v0, v7

    move-object v1, p2

    move-object v5, p3

    .line 172
    invoke-direct/range {v0 .. v6}, Lcom/squareup/tmn/TmnTransactionInput;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;[B)V

    return-object v7
.end method

.method static synthetic getTmnTransactionInput$default(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/tmn/Action;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;ILjava/lang/Object;)Lcom/squareup/tmn/TmnTransactionInput;
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 171
    check-cast p4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->getTmnTransactionInput(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/tmn/Action;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/tmn/TmnTransactionInput;

    move-result-object p0

    return-object p0
.end method

.method private final handleStartingPaymentFailure(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionOutput$Failed;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
            "Lcom/squareup/tmn/TmnTransactionOutput$Failed;",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
            "Lcom/squareup/tmn/TmnOutput;",
            ">;"
        }
    .end annotation

    .line 228
    invoke-direct {p0, p1, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->shouldEnterMiryoState(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 230
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    if-nez p3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p1, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    check-cast p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    sget-object v0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SHOULD_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    .line 231
    invoke-direct {p0, p2, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->mapFailureMiryoOutput(Lcom/squareup/tmn/TmnTransactionOutput$Failed;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/tmn/TmnOutput$MiryoOutput;

    move-result-object p2

    check-cast p2, Lcom/squareup/tmn/TmnOutput;

    .line 229
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 235
    :cond_1
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;

    const/4 p3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, p3, v0, v1}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    sget-object p3, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SHOULD_NOT_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-direct {p0, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->mapFailureTmnTransactionOutput(Lcom/squareup/tmn/TmnTransactionOutput$Failed;)Lcom/squareup/tmn/TmnOutput;

    move-result-object p2

    .line 234
    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final mapFailureMiryoOutput(Lcom/squareup/tmn/TmnTransactionOutput$Failed;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/tmn/TmnOutput$MiryoOutput;
    .locals 2

    .line 251
    instance-of v0, p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput$NetworkErrorInMiryo;

    .line 252
    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getBeforeBalance()I

    move-result v0

    .line 253
    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getAmount()I

    move-result p2

    .line 251
    invoke-direct {p1, v0, p2}, Lcom/squareup/tmn/TmnOutput$MiryoOutput$NetworkErrorInMiryo;-><init>(II)V

    check-cast p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput;

    goto :goto_0

    .line 255
    :cond_0
    instance-of p1, p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;

    .line 256
    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getBeforeBalance()I

    move-result v0

    .line 257
    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getAmount()I

    move-result p2

    .line 258
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    invoke-virtual {v1}, Lcom/squareup/tmn/CardReaderHelper;->hasEmoneyCompatibleReader$impl_release()Z

    move-result v1

    .line 255
    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;-><init>(IIZ)V

    check-cast p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final mapFailureTmnTransactionOutput(Lcom/squareup/tmn/TmnTransactionOutput$Failed;)Lcom/squareup/tmn/TmnOutput;
    .locals 1

    .line 241
    instance-of v0, p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/tmn/TmnOutput$Failed$NetworkError;

    check-cast p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/tmn/TmnOutput$Failed$NetworkError;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tmn/TmnOutput;

    goto :goto_0

    .line 242
    :cond_0
    instance-of p1, p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/tmn/TmnOutput$Failed$ActiveCardReaderDisconnected;->INSTANCE:Lcom/squareup/tmn/TmnOutput$Failed$ActiveCardReaderDisconnected;

    move-object v0, p1

    check-cast v0, Lcom/squareup/tmn/TmnOutput;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final shouldEnterMiryoState(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Z
    .locals 0

    if-eqz p2, :cond_0

    .line 220
    sget-object p2, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/tmn/TmnInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;
    .locals 3

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p2

    .line 72
    sget-object v0, Lcom/squareup/tmn/Action$StartPayment;->INSTANCE:Lcom/squareup/tmn/Action$StartPayment;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 73
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    invoke-direct {p1, v2, v1, v2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    goto :goto_0

    .line 75
    :cond_0
    sget-object v0, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;

    const/4 p2, 0x0

    invoke-direct {p1, p2, v1, v2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    :goto_0
    return-object p1

    .line 76
    :cond_1
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid illegal state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/tmn/TmnInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->initialState(Lcom/squareup/tmn/TmnInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;)Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;
    .locals 2

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p3

    .line 187
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/tmn/TmnInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p1

    .line 188
    sget-object v0, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_2

    .line 189
    :cond_1
    sget-object v0, Lcom/squareup/tmn/Action$StartPayment;->INSTANCE:Lcom/squareup/tmn/Action$StartPayment;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_5

    .line 190
    instance-of p1, p3, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    if-eqz p1, :cond_3

    invoke-virtual {p2}, Lcom/squareup/tmn/TmnInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object p1

    sget-object v0, Lcom/squareup/tmn/TmnTransactionType;->Miryo:Lcom/squareup/tmn/TmnTransactionType;

    if-ne p1, v0, :cond_2

    goto :goto_0

    .line 191
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 192
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unable to start a payment with transaction type "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {p2}, Lcom/squareup/tmn/TmnInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " while the state is in "

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-class p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x2e

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 191
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 198
    :cond_3
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/tmn/TmnInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object p1

    sget-object p2, Lcom/squareup/tmn/TmnTransactionType;->Miryo:Lcom/squareup/tmn/TmnTransactionType;

    if-ne p1, p2, :cond_4

    .line 199
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    check-cast p3, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    invoke-virtual {p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    goto :goto_1

    .line 201
    :cond_4
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    const/4 p2, 0x0

    invoke-direct {p1, p2, v1, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 198
    :goto_1
    move-object p3, p1

    check-cast p3, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    goto :goto_2

    .line 204
    :cond_5
    sget-object p2, Lcom/squareup/tmn/Action$CancelPayment;->INSTANCE:Lcom/squareup/tmn/Action$CancelPayment;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    .line 205
    new-instance p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;

    invoke-direct {p1, v1}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;-><init>(Z)V

    move-object p3, p1

    check-cast p3, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    goto :goto_2

    .line 207
    :cond_6
    sget-object p2, Lcom/squareup/tmn/Action$CancelPaymentRequest;->INSTANCE:Lcom/squareup/tmn/Action$CancelPaymentRequest;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 208
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    invoke-virtual {p1}, Lcom/squareup/tmn/CardReaderHelper;->cancelTmnRequest$impl_release()V

    :goto_2
    return-object p3

    .line 209
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/tmn/TmnInput;

    check-cast p2, Lcom/squareup/tmn/TmnInput;

    check-cast p3, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->onPropsChanged(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;)Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/tmn/TmnInput;

    check-cast p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->render(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/workflow/RenderContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public render(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/workflow/RenderContext;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/TmnInput;",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
            "-",
            "Lcom/squareup/tmn/TmnOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->teardownWorker:Lcom/squareup/tmn/RealTmnStarterWorkflow$teardownWorker$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p3, v0, v1, v2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    invoke-virtual {v0}, Lcom/squareup/tmn/CardReaderHelper;->anyCardReaderHasSecureEmoneySessionObservable$impl_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 306
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v1, "this.toFlowable(BUFFER)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_5

    .line 308
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 309
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 93
    sget-object v0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$1;->INSTANCE:Lcom/squareup/tmn/RealTmnStarterWorkflow$render$1;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 91
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 98
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->tmnTransactionWorkflow:Lcom/squareup/tmn/TmnTransactionWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 102
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object v5

    check-cast p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;->getCancelPayment()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/tmn/Action$CancelPayment;->INSTANCE:Lcom/squareup/tmn/Action$CancelPayment;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    :goto_0
    check-cast p2, Lcom/squareup/tmn/Action;

    move-object v6, p2

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, p0

    move-object v4, p1

    .line 101
    invoke-static/range {v3 .. v9}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->getTmnTransactionInput$default(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/tmn/Action;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;ILjava/lang/Object;)Lcom/squareup/tmn/TmnTransactionInput;

    move-result-object v3

    const/4 v4, 0x0

    .line 104
    sget-object p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$2;->INSTANCE:Lcom/squareup/tmn/RealTmnStarterWorkflow$render$2;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    move-object v1, p3

    .line 99
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 108
    :cond_1
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    if-eqz v0, :cond_3

    .line 110
    move-object v0, p2

    check-cast v0, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/tmn/TmnTransactionType;->Miryo:Lcom/squareup/tmn/TmnTransactionType;

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object v1

    .line 112
    :goto_1
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->tmnTransactionWorkflow:Lcom/squareup/tmn/TmnTransactionWorkflow;

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 114
    sget-object v2, Lcom/squareup/tmn/Action$StartPayment;->INSTANCE:Lcom/squareup/tmn/Action$StartPayment;

    check-cast v2, Lcom/squareup/tmn/Action;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v0

    .line 113
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->getTmnTransactionInput(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/tmn/Action;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/tmn/TmnTransactionInput;

    move-result-object v5

    const/4 v6, 0x0

    .line 116
    new-instance v0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;-><init>(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/TmnInput;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 111
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 154
    :cond_3
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    if-eqz v0, :cond_4

    .line 156
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow;->tmnTransactionWorkflow:Lcom/squareup/tmn/TmnTransactionWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 158
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object v0

    sget-object v1, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v1, Lcom/squareup/tmn/Action;

    check-cast p2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object p2

    .line 157
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->getTmnTransactionInput(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/tmn/Action;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/tmn/TmnTransactionInput;

    move-result-object v3

    const/4 v4, 0x0

    .line 160
    sget-object p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$4;->INSTANCE:Lcom/squareup/tmn/RealTmnStarterWorkflow$render$4;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 155
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    :cond_4
    :goto_2
    return-void

    .line 308
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->snapshotState(Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
