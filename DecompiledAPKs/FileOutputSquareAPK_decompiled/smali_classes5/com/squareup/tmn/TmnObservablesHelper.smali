.class public interface abstract Lcom/squareup/tmn/TmnObservablesHelper;
.super Ljava/lang/Object;
.source "TmnObservablesHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0003H&J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0003H&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0003H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "",
        "getAudioRequestObservable",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
        "getDisplayRequestObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "getOnTmnCompletionObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;",
        "getOnTmnDataToTmnObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
        "getOnTmnWriteNotifyObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;",
        "getTmnAuthRequest",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getAudioRequestObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDisplayRequestObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOnTmnCompletionObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOnTmnDataToTmnObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOnTmnWriteNotifyObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTmnAuthRequest()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;",
            ">;"
        }
    .end annotation
.end method
