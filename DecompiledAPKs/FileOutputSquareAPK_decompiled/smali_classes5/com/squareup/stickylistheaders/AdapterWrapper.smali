.class Lcom/squareup/stickylistheaders/AdapterWrapper;
.super Landroid/widget/BaseAdapter;
.source "AdapterWrapper.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

.field private mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerHeight:I

.field private final mHeaderCache:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mOnHeaderClickListener:Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V
    .locals 1

    .line 47
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 30
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mHeaderCache:Ljava/util/List;

    .line 35
    new-instance v0, Lcom/squareup/stickylistheaders/AdapterWrapper$1;

    invoke-direct {v0, p0}, Lcom/squareup/stickylistheaders/AdapterWrapper$1;-><init>(Lcom/squareup/stickylistheaders/AdapterWrapper;)V

    iput-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 48
    iput-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    .line 50
    iget-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {p2, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/stickylistheaders/AdapterWrapper;)Ljava/util/List;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mHeaderCache:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$101(Lcom/squareup/stickylistheaders/AdapterWrapper;)V
    .locals 0

    .line 23
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    return-void
.end method

.method static synthetic access$201(Lcom/squareup/stickylistheaders/AdapterWrapper;)V
    .locals 0

    .line 23
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/stickylistheaders/AdapterWrapper;)Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    return-object p0
.end method

.method private configureHeader(Lcom/squareup/stickylistheaders/WrapperView;I)Landroid/view/View;
    .locals 2

    .line 116
    invoke-direct {p0, p2}, Lcom/squareup/stickylistheaders/AdapterWrapper;->skipHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 120
    :cond_0
    iget-object v0, p1, Lcom/squareup/stickylistheaders/WrapperView;->mHeader:Landroid/view/View;

    if-nez v0, :cond_1

    .line 121
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->popHeader()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/squareup/stickylistheaders/WrapperView;->mHeader:Landroid/view/View;

    .line 124
    :goto_0
    iget-object v1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v1, p2, v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    .line 130
    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 131
    new-instance v0, Lcom/squareup/stickylistheaders/AdapterWrapper$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/stickylistheaders/AdapterWrapper$2;-><init>(Lcom/squareup/stickylistheaders/AdapterWrapper;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1

    .line 126
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "Header view must not be null."

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private popHeader()Landroid/view/View;
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mHeaderCache:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mHeaderCache:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private previousPositionHasSameHeader(I)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v1, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderId(I)J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    sub-int/2addr p1, v0

    invoke-interface {v3, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderId(I)J

    move-result-wide v3

    cmp-long p1, v1, v3

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private recycleHeaderIfExists(Lcom/squareup/stickylistheaders/WrapperView;)V
    .locals 2

    .line 99
    iget-object v0, p1, Lcom/squareup/stickylistheaders/WrapperView;->mHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 102
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mHeaderCache:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p1, Lcom/squareup/stickylistheaders/WrapperView;->mHeader:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/WrapperView;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 107
    iput-object v0, p1, Lcom/squareup/stickylistheaders/WrapperView;->mHeader:Landroid/view/View;

    :cond_0
    return-void
.end method

.method private skipDivider(I)Z
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private skipHeader(I)Z
    .locals 4

    .line 156
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderId(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getCount()I
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/BaseAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getHeaderId(I)J
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getItemViewType(I)I

    move-result p1

    return p1
.end method

.method public bridge synthetic getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Lcom/squareup/stickylistheaders/WrapperView;

    move-result-object p1

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Lcom/squareup/stickylistheaders/WrapperView;
    .locals 3

    if-nez p2, :cond_0

    .line 164
    new-instance p2, Lcom/squareup/stickylistheaders/WrapperView;

    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mContext:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/squareup/stickylistheaders/WrapperView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    check-cast p2, Lcom/squareup/stickylistheaders/WrapperView;

    .line 165
    :goto_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    iget-object v1, p2, Lcom/squareup/stickylistheaders/WrapperView;->mItem:Landroid/view/View;

    invoke-interface {v0, p1, v1, p3}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 167
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 168
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->skipHeader(I)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 169
    invoke-direct {p0, p2}, Lcom/squareup/stickylistheaders/AdapterWrapper;->recycleHeaderIfExists(Lcom/squareup/stickylistheaders/WrapperView;)V

    .line 170
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->skipDivider(I)Z

    move-result p1

    if-eqz p1, :cond_3

    move-object v0, v2

    goto :goto_1

    .line 173
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->previousPositionHasSameHeader(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 174
    invoke-direct {p0, p2}, Lcom/squareup/stickylistheaders/AdapterWrapper;->recycleHeaderIfExists(Lcom/squareup/stickylistheaders/WrapperView;)V

    goto :goto_1

    .line 176
    :cond_2
    invoke-direct {p0, p2, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->configureHeader(Lcom/squareup/stickylistheaders/WrapperView;I)Landroid/view/View;

    move-result-object v2

    .line 178
    :cond_3
    :goto_1
    instance-of p1, p3, Landroid/widget/Checkable;

    if-eqz p1, :cond_4

    instance-of v1, p2, Lcom/squareup/stickylistheaders/CheckableWrapperView;

    if-nez v1, :cond_4

    .line 180
    new-instance p2, Lcom/squareup/stickylistheaders/CheckableWrapperView;

    iget-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mContext:Landroid/content/Context;

    invoke-direct {p2, p1}, Lcom/squareup/stickylistheaders/CheckableWrapperView;-><init>(Landroid/content/Context;)V

    goto :goto_2

    :cond_4
    if-nez p1, :cond_5

    .line 181
    instance-of p1, p2, Lcom/squareup/stickylistheaders/CheckableWrapperView;

    if-eqz p1, :cond_5

    .line 182
    new-instance p2, Lcom/squareup/stickylistheaders/WrapperView;

    iget-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mContext:Landroid/content/Context;

    invoke-direct {p2, p1}, Lcom/squareup/stickylistheaders/WrapperView;-><init>(Landroid/content/Context;)V

    .line 184
    :cond_5
    :goto_2
    iget p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDividerHeight:I

    invoke-virtual {p2, p3, v2, v0, p1}, Lcom/squareup/stickylistheaders/WrapperView;->update(Landroid/view/View;Landroid/view/View;Landroid/graphics/drawable/Drawable;I)V

    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;->isEnabled(I)Z

    move-result p1

    return p1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    return-void
.end method

.method setDivider(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 55
    iput p2, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDividerHeight:I

    .line 56
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->notifyDataSetChanged()V

    return-void
.end method

.method public setOnHeaderClickListener(Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;)V
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
