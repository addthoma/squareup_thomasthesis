.class public Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.super Landroid/widget/FrameLayout;
.source "StickyListHeadersListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;,
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;,
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;,
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;,
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;,
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;,
        Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

.field private mAreHeadersSticky:Z

.field private mClippingToPadding:Z

.field private mDataSetObserver:Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

.field private mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerHeight:I

.field private mDownY:F

.field private mHeader:Landroid/view/View;

.field private mHeaderId:Ljava/lang/Long;

.field private mHeaderOffset:Ljava/lang/Integer;

.field private mHeaderOwnsTouch:Z

.field private mHeaderPosition:Ljava/lang/Integer;

.field private mIsDrawingListUnderStickyHeader:Z

.field private mList:Lcom/squareup/stickylistheaders/WrapperViewList;

.field private mOnHeaderClickListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

.field private mOnScrollListenerDelegate:Landroid/widget/AbsListView$OnScrollListener;

.field private mOnStickyHeaderChangedListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

.field private mOnStickyHeaderOffsetChangedListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

.field private mPaddingBottom:I

.field private mPaddingLeft:I

.field private mPaddingRight:I

.field private mPaddingTop:I

.field private mStickyHeaderTopOffset:I

.field private mTouchSlop:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 113
    invoke-direct {p0, p1, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 117
    sget v0, Lcom/squareup/stickylistheaders/R$attr;->square_stickyListHeadersListViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .line 122
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    .line 90
    iput-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAreHeadersSticky:Z

    .line 91
    iput-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    .line 92
    iput-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mIsDrawingListUnderStickyHeader:Z

    const/4 v1, 0x0

    .line 93
    iput v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mStickyHeaderTopOffset:I

    .line 94
    iput v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    .line 95
    iput v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    .line 96
    iput v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    .line 97
    iput v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingBottom:I

    .line 124
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mTouchSlop:F

    .line 127
    new-instance v2, Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-direct {v2, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 130
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 131
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getDividerHeight()I

    move-result v2

    iput v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    .line 132
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setDividerHeight(I)V

    if-eqz p2, :cond_9

    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object v2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView:[I

    .line 137
    invoke-virtual {p1, p2, v2, p3, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 141
    :try_start_0
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_padding:I

    .line 142
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 143
    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_paddingLeft:I

    .line 144
    invoke-virtual {p1, p3, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    .line 146
    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_paddingTop:I

    .line 147
    invoke-virtual {p1, p3, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    .line 149
    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_paddingRight:I

    .line 150
    invoke-virtual {p1, p3, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    .line 152
    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_paddingBottom:I

    .line 153
    invoke-virtual {p1, p3, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingBottom:I

    .line 156
    iget p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    iget p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    iget v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    iget v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingBottom:I

    invoke-virtual {p0, p2, p3, v2, v4}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setPadding(IIII)V

    .line 160
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_clipToPadding:I

    .line 161
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    .line 162
    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    .line 163
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    iget-boolean p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setClipToPadding(Z)V

    .line 166
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_scrollbars:I

    const/16 p3, 0x200

    .line 167
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 168
    iget-object p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    and-int/lit16 v2, p2, 0x200

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p3, v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setVerticalScrollBarEnabled(Z)V

    .line 169
    iget-object p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    and-int/lit16 p2, p2, 0x100

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p3, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setHorizontalScrollBarEnabled(Z)V

    .line 172
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x9

    if-lt p2, p3, :cond_2

    .line 173
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_overScrollMode:I

    .line 174
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    .line 173
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOverScrollMode(I)V

    .line 178
    :cond_2
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_fadingEdgeLength:I

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 180
    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getVerticalFadingEdgeLength()I

    move-result v2

    .line 179
    invoke-virtual {p1, p3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    .line 178
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setFadingEdgeLength(I)V

    .line 181
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_requiresFadingEdge:I

    .line 182
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    const/16 p3, 0x1000

    if-ne p2, p3, :cond_3

    .line 184
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p2, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setVerticalFadingEdgeEnabled(Z)V

    .line 185
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p2, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setHorizontalFadingEdgeEnabled(Z)V

    goto :goto_2

    :cond_3
    const/16 p3, 0x2000

    if-ne p2, p3, :cond_4

    .line 187
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p2, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setVerticalFadingEdgeEnabled(Z)V

    .line 188
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p2, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setHorizontalFadingEdgeEnabled(Z)V

    goto :goto_2

    .line 190
    :cond_4
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p2, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setVerticalFadingEdgeEnabled(Z)V

    .line 191
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p2, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setHorizontalFadingEdgeEnabled(Z)V

    .line 193
    :goto_2
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_cacheColorHint:I

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 195
    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getCacheColorHint()I

    move-result v2

    .line 194
    invoke-virtual {p1, p3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    .line 193
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setCacheColorHint(I)V

    .line 196
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0xb

    if-lt p2, p3, :cond_5

    .line 197
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget v2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_choiceMode:I

    iget-object v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 198
    invoke-virtual {v4}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChoiceMode()I

    move-result v4

    .line 197
    invoke-virtual {p1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setChoiceMode(I)V

    .line 200
    :cond_5
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget v2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_drawSelectorOnTop:I

    .line 201
    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 200
    invoke-virtual {p2, v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setDrawSelectorOnTop(Z)V

    .line 202
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget v2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_fastScrollEnabled:I

    iget-object v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 204
    invoke-virtual {v4}, Lcom/squareup/stickylistheaders/WrapperViewList;->isFastScrollEnabled()Z

    move-result v4

    .line 203
    invoke-virtual {p1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 202
    invoke-virtual {p2, v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setFastScrollEnabled(Z)V

    .line 205
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p2, p3, :cond_6

    .line 206
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_fastScrollAlwaysVisible:I

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 208
    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->isFastScrollAlwaysVisible()Z

    move-result v2

    .line 207
    invoke-virtual {p1, p3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    .line 206
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setFastScrollAlwaysVisible(Z)V

    .line 211
    :cond_6
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_scrollbarStyle:I

    .line 212
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    .line 211
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setScrollBarStyle(I)V

    .line 214
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_listSelector:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 215
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_listSelector:I

    .line 216
    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    .line 215
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 219
    :cond_7
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_scrollingCache:I

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 221
    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->isScrollingCacheEnabled()Z

    move-result v2

    .line 220
    invoke-virtual {p1, p3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    .line 219
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setScrollingCacheEnabled(Z)V

    .line 223
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_divider:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result p2

    if-eqz p2, :cond_8

    .line 224
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_divider:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 227
    :cond_8
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_stackFromBottom:I

    .line 228
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    .line 227
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setStackFromBottom(Z)V

    .line 230
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_dividerHeight:I

    iget p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    .line 231
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    .line 234
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    sget p3, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_android_transcriptMode:I

    .line 235
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    .line 234
    invoke-virtual {p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->setTranscriptMode(I)V

    .line 239
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_square_hasStickyHeaders:I

    .line 240
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAreHeadersSticky:Z

    .line 241
    sget p2, Lcom/squareup/stickylistheaders/R$styleable;->StickyListHeadersListView_square_isDrawingListUnderStickyHeader:I

    .line 242
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mIsDrawingListUnderStickyHeader:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_3

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    .line 250
    :cond_9
    :goto_3
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    new-instance p2, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;

    invoke-direct {p2, p0, v3}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V

    invoke-virtual {p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setLifeCycleListener(Lcom/squareup/stickylistheaders/WrapperViewList$LifeCycleListener;)V

    .line 251
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    new-instance p2, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;

    invoke-direct {p2, p0, v3}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V

    invoke-virtual {p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 253
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Z
    .locals 0

    .line 37
    iget-boolean p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)I
    .locals 0

    .line 37
    iget p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    return p0
.end method

.method static synthetic access$1200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 0

    .line 37
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p0

    return p0
.end method

.method static synthetic access$1300(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 0

    .line 37
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/view/View;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Ljava/lang/Integer;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderPosition:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Ljava/lang/Long;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderId:Ljava/lang/Long;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->clearHeader()V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/widget/AbsListView$OnScrollListener;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnScrollListenerDelegate:Landroid/widget/AbsListView$OnScrollListener;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Lcom/squareup/stickylistheaders/WrapperViewList;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/stickylistheaders/StickyListHeadersListView;I)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateOrClearHeader(I)V

    return-void
.end method

.method private clearHeader()V
    .locals 2

    .line 305
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 307
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    .line 308
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderId:Ljava/lang/Long;

    .line 309
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderPosition:Ljava/lang/Integer;

    .line 310
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    .line 313
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setTopClippingLength(I)V

    .line 314
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateHeaderVisibilities()V

    :cond_0
    return-void
.end method

.method private ensureHeaderHasCorrectLayoutParams(Landroid/view/View;)V
    .locals 4

    .line 262
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    const/4 v2, -0x1

    if-nez v0, :cond_0

    .line 264
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 265
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 266
    :cond_0
    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v3, v2, :cond_1

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v3, v1, :cond_2

    .line 267
    :cond_1
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 268
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 269
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private isStartOfSection(I)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 576
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-virtual {v1, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getHeaderId(I)J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    sub-int/2addr p1, v0

    invoke-virtual {v3, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getHeaderId(I)J

    move-result-wide v3

    cmp-long p1, v1, v3

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private measureHeader(Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    sub-int/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    .line 276
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v1, 0x0

    .line 277
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 278
    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->measureChild(Landroid/view/View;II)V

    :cond_0
    return-void
.end method

.method private requireSdkVersion(I)Z
    .locals 2

    .line 690
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, p1, :cond_0

    .line 691
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Api lvl must be at least "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " to call this method"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "StickyListHeaders"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method private setHeaderOffet(I)V
    .locals 2

    .line 467
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_2

    .line 468
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    .line 469
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0xb

    if-lt p1, v0, :cond_1

    .line 470
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 472
    :cond_1
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 473
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 474
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 476
    :goto_0
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnStickyHeaderOffsetChangedListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

    if-eqz p1, :cond_2

    .line 477
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    .line 478
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    neg-int v1, v1

    .line 477
    invoke-interface {p1, p0, v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;->onStickyHeaderOffsetChanged(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/view/View;I)V

    :cond_2
    return-void
.end method

.method private stickyHeaderTop()I
    .locals 2

    .line 594
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mStickyHeaderTopOffset:I

    iget-boolean v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method private swapHeader(Landroid/view/View;)V
    .locals 1

    .line 414
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->removeView(Landroid/view/View;)V

    .line 418
    :cond_0
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    .line 419
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->addView(Landroid/view/View;)V

    .line 420
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    if-eqz p1, :cond_1

    .line 421
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;

    invoke-direct {v0, p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 428
    :cond_1
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    return-void
.end method

.method private updateHeader(IZ)V
    .locals 7

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 358
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderPosition:Ljava/lang/Integer;

    .line 359
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    const-wide/16 p1, -0x1

    .line 360
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderId:Ljava/lang/Long;

    .line 361
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateHeaderVisibilities()V

    return-void

    .line 366
    :cond_0
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderPosition:Ljava/lang/Integer;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    if-eq p2, p1, :cond_5

    .line 367
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderPosition:Ljava/lang/Integer;

    .line 368
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-virtual {p2, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getHeaderId(I)J

    move-result-wide v1

    .line 369
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderId:Ljava/lang/Long;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long p2, v3, v1

    if-eqz p2, :cond_5

    .line 370
    :cond_2
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderId:Ljava/lang/Long;

    .line 371
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderPosition:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {p2, v1, v2, p0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 372
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eq v1, p2, :cond_3

    .line 373
    invoke-direct {p0, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->swapHeader(Landroid/view/View;)V

    .line 375
    :cond_3
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->ensureHeaderHasCorrectLayoutParams(Landroid/view/View;)V

    .line 376
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->measureHeader(Landroid/view/View;)V

    .line 377
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnStickyHeaderChangedListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

    if-eqz v1, :cond_4

    .line 378
    iget-object v3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderId:Ljava/lang/Long;

    .line 379
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object v2, p0

    move v4, p1

    .line 378
    invoke-interface/range {v1 .. v6}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;->onStickyHeaderChanged(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/view/View;IJ)V

    .line 384
    :cond_4
    iput-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    .line 388
    :cond_5
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->stickyHeaderTop()I

    move-result p1

    const/4 p2, 0x0

    const/4 v0, 0x0

    .line 393
    :goto_0
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_9

    .line 394
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v1, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 395
    instance-of v2, v1, Lcom/squareup/stickylistheaders/WrapperView;

    if-eqz v2, :cond_6

    move-object v2, v1

    check-cast v2, Lcom/squareup/stickylistheaders/WrapperView;

    .line 396
    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperView;->hasHeader()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    .line 397
    :goto_1
    iget-object v3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v3, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->containsFooterView(Landroid/view/View;)Z

    move-result v3

    .line 398
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->stickyHeaderTop()I

    move-result v5

    if-lt v4, v5, :cond_8

    if-nez v2, :cond_7

    if-eqz v3, :cond_8

    .line 399
    :cond_7
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_2

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 404
    :cond_9
    :goto_2
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setHeaderOffet(I)V

    .line 406
    iget-boolean p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mIsDrawingListUnderStickyHeader:Z

    if-nez p1, :cond_a

    .line 407
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr p2, v0

    invoke-virtual {p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setTopClippingLength(I)V

    .line 410
    :cond_a
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateHeaderVisibilities()V

    return-void
.end method

.method private updateHeaderVisibilities()V
    .locals 7

    .line 434
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->stickyHeaderTop()I

    move-result v0

    .line 435
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_4

    .line 439
    iget-object v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v4, v3}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 440
    instance-of v5, v4, Lcom/squareup/stickylistheaders/WrapperView;

    if-nez v5, :cond_0

    goto :goto_1

    .line 445
    :cond_0
    check-cast v4, Lcom/squareup/stickylistheaders/WrapperView;

    .line 446
    invoke-virtual {v4}, Lcom/squareup/stickylistheaders/WrapperView;->hasHeader()Z

    move-result v5

    if-nez v5, :cond_1

    goto :goto_1

    .line 451
    :cond_1
    iget-object v5, v4, Lcom/squareup/stickylistheaders/WrapperView;->mHeader:Landroid/view/View;

    .line 452
    invoke-virtual {v4}, Lcom/squareup/stickylistheaders/WrapperView;->getTop()I

    move-result v4

    if-ge v4, v0, :cond_2

    .line 453
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/4 v6, 0x4

    if-eq v4, v6, :cond_3

    .line 454
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 457
    :cond_2
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_3

    .line 458
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method private updateOrClearHeader(I)V
    .locals 8

    .line 319
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getCount()I

    move-result v0

    :goto_0
    if-eqz v0, :cond_a

    .line 320
    iget-boolean v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAreHeadersSticky:Z

    if-nez v2, :cond_1

    goto/16 :goto_5

    .line 324
    :cond_1
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getHeaderViewsCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    .line 325
    invoke-virtual {v2, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getHeaderId(I)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    .line 326
    invoke-direct {p0, p1, v3}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateHeader(IZ)V

    return-void

    .line 330
    :cond_2
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr p1, v2

    .line 332
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 333
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 334
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->stickyHeaderTop()I

    move-result v4

    if-ge v2, v4, :cond_3

    add-int/lit8 p1, p1, 0x1

    .line 342
    :cond_3
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_5

    .line 343
    iget-object v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 344
    invoke-virtual {v4}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFirstVisiblePosition()I

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 345
    invoke-virtual {v4, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->stickyHeaderTop()I

    move-result v5

    if-lt v4, v5, :cond_5

    const/4 v4, 0x1

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    :goto_2
    sub-int/2addr v0, v3

    if-gt p1, v0, :cond_7

    if-gez p1, :cond_6

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    :cond_7
    :goto_3
    if-eqz v2, :cond_9

    if-nez v3, :cond_9

    if-eqz v4, :cond_8

    goto :goto_4

    .line 353
    :cond_8
    invoke-direct {p0, p1, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateHeader(IZ)V

    return-void

    .line 349
    :cond_9
    :goto_4
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->clearHeader()V

    :cond_a
    :goto_5
    return-void
.end method


# virtual methods
.method public addFooterView(Landroid/view/View;)V
    .locals 1

    .line 807
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->addFooterView(Landroid/view/View;)V

    return-void
.end method

.method public addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    .line 803
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    return-void
.end method

.method public addHeaderView(Landroid/view/View;)V
    .locals 1

    .line 791
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->addHeaderView(Landroid/view/View;)V

    return-void
.end method

.method public addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    .line 787
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    return-void
.end method

.method public areHeadersSticky()Z
    .locals 1

    .line 611
    iget-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAreHeadersSticky:Z

    return v0
.end method

.method public canScrollVertically(I)Z
    .locals 1

    .line 1094
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->canScrollVertically(I)Z

    move-result p1

    return p1
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 297
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    const-wide/16 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_1
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .line 484
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 486
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDownY:F

    .line 487
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDownY:F

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOffset:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOwnsTouch:Z

    .line 491
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOwnsTouch:Z

    if-eqz v0, :cond_4

    .line 492
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDownY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mTouchSlop:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_2

    .line 493
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    goto :goto_1

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 496
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    const/4 v2, 0x3

    .line 497
    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 498
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 499
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 503
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iget v9, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDownY:F

    .line 504
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v10

    .line 503
    invoke-static/range {v3 .. v10}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object p1

    .line 505
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 506
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 507
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 508
    iput-boolean v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeaderOwnsTouch:Z

    move p1, v0

    goto :goto_1

    .line 511
    :cond_4
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    :goto_1
    return p1
.end method

.method public getAdapter()Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;
    .locals 1

    .line 737
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    :goto_0
    return-object v0
.end method

.method public getAreHeadersSticky()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 618
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->areHeadersSticky()Z

    move-result v0

    return v0
.end method

.method public getCheckedItemCount()I
    .locals 1

    const/16 v0, 0xb

    .line 947
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getCheckedItemCount()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCheckedItemIds()[J
    .locals 1

    const/16 v0, 0x8

    .line 954
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getCheckedItemIds()[J

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCheckedItemPosition()I
    .locals 1

    .line 961
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getCheckedItemPosition()I

    move-result v0

    return v0
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .locals 1

    .line 965
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .line 969
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getCount()I

    move-result v0

    return v0
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 755
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerHeight()I
    .locals 1

    .line 759
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .line 823
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getEmptyView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .line 930
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFirstVisiblePosition()I

    move-result v0

    return v0
.end method

.method public getFooterViewsCount()I
    .locals 1

    .line 815
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFooterViewsCount()I

    move-result v0

    return v0
.end method

.method public getHeaderOverlap(I)I
    .locals 3

    .line 580
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p1, v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->isStartOfSection(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/stickylistheaders/AdapterWrapper;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 586
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->ensureHeaderHasCorrectLayoutParams(Landroid/view/View;)V

    .line 587
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->measureHeader(Landroid/view/View;)V

    .line 588
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    return p1

    .line 584
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "header may not be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return v1
.end method

.method public getHeaderViewsCount()I
    .locals 1

    .line 799
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getHeaderViewsCount()I

    move-result v0

    return v0
.end method

.method public getItemAtPosition(I)Ljava/lang/Object;
    .locals 1

    .line 973
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemIdAtPosition(I)J
    .locals 2

    .line 977
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getItemIdAtPosition(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .line 934
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getLastVisiblePosition()I

    move-result v0

    return v0
.end method

.method public getListChildAt(I)Landroid/view/View;
    .locals 1

    .line 672
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getListChildCount()I
    .locals 1

    .line 676
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getOverScrollMode()I
    .locals 1

    const/16 v0, 0x9

    .line 843
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getOverScrollMode()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getPaddingBottom()I
    .locals 1

    .line 1035
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingBottom:I

    return v0
.end method

.method public getPaddingLeft()I
    .locals 1

    .line 1023
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    return v0
.end method

.method public getPaddingRight()I
    .locals 1

    .line 1031
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    return v0
.end method

.method public getPaddingTop()I
    .locals 1

    .line 1027
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    return v0
.end method

.method public getPositionForView(Landroid/view/View;)I
    .locals 1

    .line 1069
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getPositionForView(Landroid/view/View;)I

    move-result p1

    return p1
.end method

.method public getScrollBarStyle()I
    .locals 1

    .line 1065
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->getScrollBarStyle()I

    move-result v0

    return v0
.end method

.method public getStickyHeaderTopOffset()I
    .locals 1

    .line 630
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mStickyHeaderTopOffset:I

    return v0
.end method

.method public getWrappedList()Landroid/widget/ListView;
    .locals 1

    .line 686
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    return-object v0
.end method

.method public invalidateViews()V
    .locals 1

    .line 989
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->invalidateViews()V

    return-void
.end method

.method public isDrawingListUnderStickyHeader()Z
    .locals 1

    .line 640
    iget-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mIsDrawingListUnderStickyHeader:Z

    return v0
.end method

.method public isFastScrollAlwaysVisible()Z
    .locals 2

    .line 1054
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    .line 1057
    :cond_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->isFastScrollAlwaysVisible()Z

    move-result v0

    return v0
.end method

.method public isHorizontalScrollBarEnabled()Z
    .locals 1

    .line 831
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->isHorizontalScrollBarEnabled()Z

    move-result v0

    return v0
.end method

.method public isStackFromBottom()Z
    .locals 1

    .line 1110
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->isStackFromBottom()Z

    move-result v0

    return v0
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 1

    .line 827
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->isVerticalScrollBarEnabled()Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 283
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getHeight()I

    move-result p3

    const/4 p4, 0x0

    invoke-virtual {p1, p4, p4, p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->layout(IIII)V

    .line 284
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 286
    iget p1, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 287
    iget-object p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    iget p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    iget p5, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    add-int/2addr p4, p5

    iget-object p5, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    .line 288
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result p5

    add-int/2addr p5, p1

    .line 287
    invoke-virtual {p2, p3, p1, p4, p5}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 257
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 258
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->measureHeader(Landroid/view/View;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 1085
    instance-of v0, p1, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    if-eqz v0, :cond_0

    .line 1086
    check-cast p1, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    .line 1087
    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1088
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;->getListState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 1080
    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    .line 1081
    invoke-virtual {v2}, Lcom/squareup/stickylistheaders/WrapperViewList;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method protected recomputePadding()V
    .locals 4

    .line 1019
    iget v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    iget v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    iget v3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingBottom:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setPadding(IIII)V

    return-void
.end method

.method public removeFooterView(Landroid/view/View;)V
    .locals 1

    .line 811
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->removeFooterView(Landroid/view/View;)Z

    return-void
.end method

.method public removeHeaderView(Landroid/view/View;)V
    .locals 1

    .line 795
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->removeHeaderView(Landroid/view/View;)Z

    return-void
.end method

.method public setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_2

    .line 702
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    instance-of v1, p1, Lcom/squareup/stickylistheaders/SectionIndexerAdapterWrapper;

    if-eqz v1, :cond_0

    .line 703
    check-cast p1, Lcom/squareup/stickylistheaders/SectionIndexerAdapterWrapper;

    iput-object v0, p1, Lcom/squareup/stickylistheaders/SectionIndexerAdapterWrapper;->mSectionIndexerDelegate:Landroid/widget/SectionIndexer;

    .line 705
    :cond_0
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    if-eqz p1, :cond_1

    .line 706
    iput-object v0, p1, Lcom/squareup/stickylistheaders/AdapterWrapper;->mDelegate:Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    .line 708
    :cond_1
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 709
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->clearHeader()V

    return-void

    .line 712
    :cond_2
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    if-eqz v1, :cond_3

    .line 713
    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDataSetObserver:Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    invoke-virtual {v1, v2}, Lcom/squareup/stickylistheaders/AdapterWrapper;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 716
    :cond_3
    instance-of v1, p1, Landroid/widget/SectionIndexer;

    if-eqz v1, :cond_4

    .line 717
    new-instance v1, Lcom/squareup/stickylistheaders/SectionIndexerAdapterWrapper;

    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/stickylistheaders/SectionIndexerAdapterWrapper;-><init>(Landroid/content/Context;Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    iput-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    goto :goto_0

    .line 719
    :cond_4
    new-instance v1, Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/stickylistheaders/AdapterWrapper;-><init>(Landroid/content/Context;Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    iput-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    .line 721
    :goto_0
    new-instance p1, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    invoke-direct {p1, p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V

    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDataSetObserver:Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    .line 722
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDataSetObserver:Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    invoke-virtual {p1, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 724
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    if-eqz p1, :cond_5

    .line 725
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    new-instance v1, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;

    invoke-direct {v1, p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V

    invoke-virtual {p1, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setOnHeaderClickListener(Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;)V

    goto :goto_1

    .line 727
    :cond_5
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setOnHeaderClickListener(Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;)V

    .line 730
    :goto_1
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setDivider(Landroid/graphics/drawable/Drawable;I)V

    .line 732
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 733
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->clearHeader()V

    return-void
.end method

.method public setAreHeadersSticky(Z)V
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAreHeadersSticky:Z

    if-nez p1, :cond_0

    .line 602
    invoke-direct {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->clearHeader()V

    goto :goto_0

    .line 604
    :cond_0
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateOrClearHeader(I)V

    .line 607
    :goto_0
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->invalidate()V

    return-void
.end method

.method public setBlockLayoutChildren(Z)V
    .locals 1

    .line 1102
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setBlockLayoutChildren(Z)V

    return-void
.end method

.method public setChoiceMode(I)V
    .locals 1

    .line 938
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setChoiceMode(I)V

    return-void
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .line 993
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    if-eqz v0, :cond_0

    .line 994
    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setClipToPadding(Z)V

    .line 996
    :cond_0
    iput-boolean p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    return-void
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 741
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 742
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    if-eqz p1, :cond_0

    .line 743
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setDivider(Landroid/graphics/drawable/Drawable;I)V

    :cond_0
    return-void
.end method

.method public setDividerHeight(I)V
    .locals 2

    .line 748
    iput p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    .line 749
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    if-eqz p1, :cond_0

    .line 750
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDivider:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mDividerHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setDivider(Landroid/graphics/drawable/Drawable;I)V

    :cond_0
    return-void
.end method

.method public setDrawingListUnderStickyHeader(Z)V
    .locals 1

    .line 634
    iput-boolean p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mIsDrawingListUnderStickyHeader:Z

    .line 636
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setTopClippingLength(I)V

    return-void
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 1

    .line 819
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public setFastScrollAlwaysVisible(Z)V
    .locals 1

    const/16 v0, 0xb

    .line 1044
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setFastScrollAlwaysVisible(Z)V

    :cond_0
    return-void
.end method

.method public setFastScrollEnabled(Z)V
    .locals 1

    .line 1039
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setFastScrollEnabled(Z)V

    return-void
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .locals 1

    .line 839
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setHorizontalScrollBarEnabled(Z)V

    return-void
.end method

.method public setItemChecked(IZ)V
    .locals 1

    .line 943
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setItemChecked(IZ)V

    return-void
.end method

.method public setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V
    .locals 1

    const/16 v0, 0xb

    .line 1074
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    :cond_0
    return-void
.end method

.method public setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
    .locals 1

    .line 981
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    return-void
.end method

.method public setOnHeaderClickListener(Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;)V
    .locals 2

    .line 644
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    .line 645
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    if-eqz p1, :cond_1

    .line 646
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnHeaderClickListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 647
    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;

    invoke-direct {v0, p0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setOnHeaderClickListener(Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;)V

    .line 649
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mHeader:Landroid/view/View;

    if-eqz p1, :cond_1

    .line 650
    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;

    invoke-direct {v0, p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 658
    :cond_0
    invoke-virtual {p1, v1}, Lcom/squareup/stickylistheaders/AdapterWrapper;->setOnHeaderClickListener(Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .line 779
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1

    .line 783
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .line 763
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnScrollListenerDelegate:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method public setOnStickyHeaderChangedListener(Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;)V
    .locals 0

    .line 668
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnStickyHeaderChangedListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

    return-void
.end method

.method public setOnStickyHeaderOffsetChangedListener(Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;)V
    .locals 0

    .line 664
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mOnStickyHeaderOffsetChangedListener:Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 768
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    new-instance v1, Lcom/squareup/stickylistheaders/StickyListHeadersListView$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$3;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 774
    :cond_0
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_0
    return-void
.end method

.method public setOverScrollMode(I)V
    .locals 1

    const/16 v0, 0x9

    .line 850
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    if-eqz v0, :cond_0

    .line 852
    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setOverScrollMode(I)V

    :cond_0
    return-void
.end method

.method public setPadding(IIII)V
    .locals 1

    .line 1000
    iput p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingLeft:I

    .line 1001
    iput p2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    .line 1002
    iput p3, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingRight:I

    .line 1003
    iput p4, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingBottom:I

    .line 1005
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    if-eqz v0, :cond_0

    .line 1006
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/stickylistheaders/WrapperViewList;->setPadding(IIII)V

    :cond_0
    const/4 p1, 0x0

    .line 1008
    invoke-super {p0, p1, p1, p1, p1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 1009
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requestLayout()V

    return-void
.end method

.method public setScrollBarStyle(I)V
    .locals 1

    .line 1061
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setScrollBarStyle(I)V

    return-void
.end method

.method public setSelection(I)V
    .locals 1

    const/4 v0, 0x0

    .line 908
    invoke-virtual {p0, p1, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setSelectionFromTop(II)V

    return-void
.end method

.method public setSelectionAfterHeaderView()V
    .locals 1

    .line 912
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->setSelectionAfterHeaderView()V

    return-void
.end method

.method public setSelectionFromTop(II)V
    .locals 2

    .line 916
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getHeaderOverlap(I)I

    move-result v0

    :goto_0
    add-int/2addr p2, v0

    .line 917
    iget-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    :goto_1
    sub-int/2addr p2, v1

    .line 918
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->setSelectionFromTop(II)V

    return-void
.end method

.method public setSelector(I)V
    .locals 1

    .line 926
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setSelector(I)V

    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 922
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setSelector(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setSelectorPadding(I)V
    .locals 1

    .line 1013
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setSelectorPadding(I)V

    return-void
.end method

.method public setStackFromBottom(Z)V
    .locals 1

    .line 1106
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setStackFromBottom(Z)V

    return-void
.end method

.method public setStickyHeaderTopOffset(I)V
    .locals 0

    .line 625
    iput p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mStickyHeaderTopOffset:I

    .line 626
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->updateOrClearHeader(I)V

    return-void
.end method

.method public setTranscriptMode(I)V
    .locals 1

    .line 1098
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setTranscriptMode(I)V

    return-void
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 1

    .line 835
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->setVerticalScrollBarEnabled(Z)V

    return-void
.end method

.method public showContextMenu()Z
    .locals 1

    .line 985
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->showContextMenu()Z

    move-result v0

    return v0
.end method

.method public smoothScrollBy(II)V
    .locals 1

    const/16 v0, 0x8

    .line 858
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollBy(II)V

    :cond_0
    return-void
.end method

.method public smoothScrollByOffset(I)V
    .locals 1

    const/16 v0, 0xb

    .line 864
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollByOffset(I)V

    :cond_0
    return-void
.end method

.method public smoothScrollToPosition(I)V
    .locals 3

    const/16 v0, 0x8

    .line 871
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 872
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 873
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollToPosition(I)V

    goto :goto_2

    .line 875
    :cond_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getHeaderOverlap(I)I

    move-result v0

    .line 876
    :goto_0
    iget-boolean v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    :goto_1
    sub-int/2addr v0, v1

    .line 877
    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollToPositionFromTop(II)V

    :cond_3
    :goto_2
    return-void
.end method

.method public smoothScrollToPosition(II)V
    .locals 1

    const/16 v0, 0x8

    .line 884
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollToPosition(II)V

    :cond_0
    return-void
.end method

.method public smoothScrollToPositionFromTop(II)V
    .locals 2

    const/16 v0, 0xb

    .line 891
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 892
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getHeaderOverlap(I)I

    move-result v0

    :goto_0
    add-int/2addr p2, v0

    .line 893
    iget-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    :goto_1
    sub-int/2addr p2, v1

    .line 894
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollToPositionFromTop(II)V

    :cond_2
    return-void
.end method

.method public smoothScrollToPositionFromTop(III)V
    .locals 2

    const/16 v0, 0xb

    .line 900
    invoke-direct {p0, v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->requireSdkVersion(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 901
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mAdapter:Lcom/squareup/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->getHeaderOverlap(I)I

    move-result v0

    :goto_0
    add-int/2addr p2, v0

    .line 902
    iget-boolean v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mClippingToPadding:Z

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mPaddingTop:I

    :goto_1
    sub-int/2addr p2, v1

    .line 903
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->mList:Lcom/squareup/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/stickylistheaders/WrapperViewList;->smoothScrollToPositionFromTop(III)V

    :cond_2
    return-void
.end method
