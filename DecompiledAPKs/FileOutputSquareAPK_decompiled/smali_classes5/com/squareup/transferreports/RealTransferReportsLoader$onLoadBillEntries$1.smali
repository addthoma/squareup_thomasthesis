.class final Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;
.super Ljava/lang/Object;
.source "RealTransferReportsLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/RealTransferReportsLoader;->onLoadBillEntries(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currentSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

.field final synthetic $settledBillEndIndex:I

.field final synthetic $settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

.field final synthetic this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;

    iput-object p2, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$currentSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iput-object p3, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iput p4, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$settledBillEndIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;

    .line 218
    iget-object v1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$currentSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iget-object v2, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$settlementReport:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    iget v3, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$settledBillEndIndex:I

    .line 217
    invoke-static {v0, v1, v2, p1, v3}, Lcom/squareup/transferreports/RealTransferReportsLoader;->access$onLoadBillEntriesSuccess(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;I)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p1

    goto :goto_0

    .line 220
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;

    iget-object v0, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->$currentSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-static {p1, v0}, Lcom/squareup/transferreports/RealTransferReportsLoader;->access$onLoadBillEntriesFailure(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader$onLoadBillEntries$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p1

    return-object p1
.end method
