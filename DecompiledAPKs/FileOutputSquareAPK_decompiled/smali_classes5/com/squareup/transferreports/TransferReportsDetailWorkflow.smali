.class public final Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "TransferReportsDetailWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;,
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;,
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsDetailWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsDetailWorkflow.kt\ncom/squareup/transferreports/TransferReportsDetailWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,191:1\n32#2,12:192\n149#3,5:204\n149#3,5:209\n149#3,5:214\n149#3,5:219\n*E\n*S KotlinDebug\n*F\n+ 1 TransferReportsDetailWorkflow.kt\ncom/squareup/transferreports/TransferReportsDetailWorkflow\n*L\n55#1,12:192\n80#1,5:204\n89#1,5:209\n98#1,5:214\n106#1,5:219\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0003\'()B\'\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u001a\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00022\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016JN\u0010\u0017\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0003H\u0016J.\u0010\u001c\u001a\u00020\u001d2\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "transferReportsLoader",
        "Lcom/squareup/transferreports/TransferReportsLoader;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "analytics",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "feeTutorial",
        "Lcom/squareup/feetutorial/FeeTutorial;",
        "(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/feetutorial/FeeTutorial;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "transferReportsDetailScreen",
        "Lcom/squareup/transferreports/TransferReportsDetailScreen;",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;",
        "reportsSnapshot",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "feeDetailsAvailable",
        "",
        "checkedButtonType",
        "Lcom/squareup/transferreports/ButtonType;",
        "Action",
        "GetTransferDetailsWorker",
        "LoadBillEntriesWorker",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

.field private final transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;


# direct methods
.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositAnalytics;Lcom/squareup/feetutorial/FeeTutorial;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transferReportsLoader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feeTutorial"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/instantdeposit/InstantDepositAnalytics;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getFeeTutorial$p(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/feetutorial/FeeTutorial;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-object p0
.end method

.method public static final synthetic access$getTransferReportsLoader$p(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/transferreports/TransferReportsLoader;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;

    return-object p0
.end method

.method private final transferReportsDetailScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;)Lcom/squareup/transferreports/TransferReportsDetailScreen;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;",
            ">;",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Z",
            "Lcom/squareup/transferreports/ButtonType;",
            ")",
            "Lcom/squareup/transferreports/TransferReportsDetailScreen;"
        }
    .end annotation

    .line 117
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailScreen;

    new-instance v10, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    .line 118
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 122
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 123
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$2;

    invoke-direct {v1, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 124
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$3;-><init>(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 128
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$4;

    invoke-direct {v1, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$transferReportsDetailScreen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, v1

    check-cast v9, Lkotlin/jvm/functions/Function1;

    move-object v1, v10

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    .line 117
    invoke-direct/range {v1 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;-><init>(ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    invoke-direct {v0, v10}, Lcom/squareup/transferreports/TransferReportsDetailScreen;-><init>(Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;)V

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/transferreports/TransferReportsDetailState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 192
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 197
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 199
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 200
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 201
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 203
    :cond_3
    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 55
    :cond_4
    sget-object p1, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingTransferReportsDetail;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailState$LoadingTransferReportsDetail;

    move-object v1, p1

    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->initialState(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/transferreports/TransferReportsDetailState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    check-cast p2, Lcom/squareup/transferreports/TransferReportsDetailState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->render(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Lcom/squareup/transferreports/TransferReportsDetailState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Lcom/squareup/transferreports/TransferReportsDetailState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;",
            "Lcom/squareup/transferreports/TransferReportsDetailState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/transferreports/TransferReportsDetailState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v8, p3

    const-string v3, "props"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v8, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingTransferReportsDetail;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailState$LoadingTransferReportsDetail;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const/4 v9, 0x0

    const-string v10, ""

    if-eqz v3, :cond_0

    .line 63
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;

    invoke-direct {v2, v0, v1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;-><init>(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;)V

    move-object v3, v2

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    sget-object v2, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$1;

    move-object v5, v2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 72
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v2

    .line 73
    new-instance v3, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    .line 74
    sget-object v12, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 75
    sget-object v17, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/16 v18, 0x0

    const/16 v19, 0x0

    .line 76
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;->getDepositType()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0xede

    const/16 v25, 0x0

    move-object v11, v3

    .line 73
    invoke-direct/range {v11 .. v25}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 79
    sget-object v1, Lcom/squareup/transferreports/ButtonType;->COLLECTED:Lcom/squareup/transferreports/ButtonType;

    .line 71
    invoke-direct {v0, v2, v3, v9, v1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->transferReportsDetailScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;)Lcom/squareup/transferreports/TransferReportsDetailScreen;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 205
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 206
    const-class v3, Lcom/squareup/transferreports/TransferReportsDetailScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 207
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 205
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 81
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 84
    :cond_0
    instance-of v1, v2, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingBillEntries;

    if-eqz v1, :cond_1

    .line 85
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;

    check-cast v2, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingBillEntries;

    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingBillEntries;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$LoadBillEntriesWorker;-><init>(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    sget-object v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$2;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$2;

    move-object v4, v1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 89
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailScreen;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen;-><init>(Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 210
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 211
    const-class v3, Lcom/squareup/transferreports/TransferReportsDetailScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 212
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 210
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 90
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 93
    :cond_1
    instance-of v1, v2, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    if-eqz v1, :cond_3

    .line 94
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 95
    check-cast v2, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v3

    .line 96
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->SHOW_CP_PRICING_CHANGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {v4}, Lcom/squareup/feetutorial/FeeTutorial;->getCanShow()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v9, 0x1

    .line 97
    :cond_2
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->getCheckedButtonType()Lcom/squareup/transferreports/ButtonType;

    move-result-object v2

    .line 93
    invoke-direct {v0, v1, v3, v9, v2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->transferReportsDetailScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;ZLcom/squareup/transferreports/ButtonType;)Lcom/squareup/transferreports/TransferReportsDetailScreen;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 215
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 216
    const-class v3, Lcom/squareup/transferreports/TransferReportsDetailScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 217
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 215
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 98
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 100
    :cond_3
    instance-of v1, v2, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingFeeDetailDialog;

    if-eqz v1, :cond_4

    new-instance v1, Lcom/squareup/transferreports/TransferReportsFeeDetailDialog;

    .line 101
    new-instance v3, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$3;

    invoke-direct {v3, v0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$3;-><init>(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 105
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;

    invoke-direct {v4, v8, v2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/transferreports/TransferReportsDetailState;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 100
    invoke-direct {v1, v3, v4}, Lcom/squareup/transferreports/TransferReportsFeeDetailDialog;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 220
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 221
    const-class v3, Lcom/squareup/transferreports/TransferReportsFeeDetailDialog;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 222
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 220
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 106
    sget-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_4
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/transferreports/TransferReportsDetailState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsDetailState;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->snapshotState(Lcom/squareup/transferreports/TransferReportsDetailState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
