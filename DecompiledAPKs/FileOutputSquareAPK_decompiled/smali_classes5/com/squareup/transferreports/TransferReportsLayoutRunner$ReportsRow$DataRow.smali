.class public abstract Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow;
.super Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;
.source "TransferReportsLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DataRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$ActiveSalesRow;,
        Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$PendingSettlementRow;,
        Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$DepositHistoryRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow;",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;",
        "()V",
        "ActiveSalesRow",
        "DepositHistoryRow",
        "PendingSettlementRow",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$ActiveSalesRow;",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$PendingSettlementRow;",
        "Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow$DepositHistoryRow;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 470
    invoke-direct {p0, v0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 470
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsLayoutRunner$ReportsRow$DataRow;-><init>()V

    return-void
.end method
