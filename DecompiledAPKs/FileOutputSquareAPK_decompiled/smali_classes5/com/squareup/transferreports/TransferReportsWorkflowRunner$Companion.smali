.class public final Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "TransferReportsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsWorkflowRunner.kt\ncom/squareup/transferreports/TransferReportsWorkflowRunner$Companion\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "startWorkflow",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "props",
        "Lcom/squareup/transferreports/TransferReportsProps;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startWorkflow(Lmortar/MortarScope;Lcom/squareup/transferreports/TransferReportsProps;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    .line 48
    check-cast p1, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;

    .line 49
    invoke-static {p1, p2}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->access$setProps$p(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;Lcom/squareup/transferreports/TransferReportsProps;)V

    .line 50
    invoke-static {p1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->access$ensureWorkflow(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)V

    return-void
.end method
