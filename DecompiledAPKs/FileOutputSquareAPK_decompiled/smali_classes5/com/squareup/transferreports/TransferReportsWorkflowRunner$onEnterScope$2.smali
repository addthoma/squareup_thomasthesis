.class final Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsWorkflowRunner.kt\ncom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,55:1\n152#2:56\n*E\n*S KotlinDebug\n*F\n+ 1 TransferReportsWorkflowRunner.kt\ncom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2\n*L\n26#1:56\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/transferreports/TransferReportsWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/transferreports/TransferReportsWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/transferreports/TransferReportsWorkflowRunner;->access$getContainer$p(Lcom/squareup/transferreports/TransferReportsWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 56
    const-class v1, Lcom/squareup/transferreports/TransferReportsScope;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    return-void
.end method
