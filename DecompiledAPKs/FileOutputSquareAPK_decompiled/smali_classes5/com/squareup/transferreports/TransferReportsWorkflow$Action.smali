.class public abstract Lcom/squareup/transferreports/TransferReportsWorkflow$Action;
.super Ljava/lang/Object;
.source "TransferReportsWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;,
        Lcom/squareup/transferreports/TransferReportsWorkflow$Action$Finish;,
        Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;,
        Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;,
        Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadTransferReports;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/transferreports/TransferReportsState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0005\u0007\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0006H\u0016\u0082\u0001\u0005\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/transferreports/TransferReportsState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "DelegateToDetailWorkflow",
        "Finish",
        "LoadMoreTransferReports",
        "LoadTransferReports",
        "ShowTransferReports",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$Finish;",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadTransferReports;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/transferreports/TransferReportsState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/transferreports/TransferReportsState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Lcom/squareup/transferreports/TransferReportsState$ShowingTransferReports;

    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;->getShowCurrentBalanceAndActiveSales()Z

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/transferreports/TransferReportsState$ShowingTransferReports;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :cond_0
    sget-object v0, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/transferreports/TransferReportsWorkflow$Action$Finish;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 154
    :cond_1
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;

    if-eqz v0, :cond_2

    .line 155
    new-instance v0, Lcom/squareup/transferreports/TransferReportsState$LoadingMoreTransferReports;

    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadMoreTransferReports;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/transferreports/TransferReportsState$LoadingMoreTransferReports;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 157
    :cond_2
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;

    if-eqz v0, :cond_3

    .line 158
    new-instance v0, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;

    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;->getDetailProps()Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$DelegateToDetailWorkflow;->getShowCurrentBalanceAndActiveSales()Z

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 160
    :cond_3
    sget-object v0, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadTransferReports;->INSTANCE:Lcom/squareup/transferreports/TransferReportsWorkflow$Action$LoadTransferReports;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 161
    sget-object v0, Lcom/squareup/transferreports/TransferReportsState$LoadingTransferReports;->INSTANCE:Lcom/squareup/transferreports/TransferReportsState$LoadingTransferReports;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
