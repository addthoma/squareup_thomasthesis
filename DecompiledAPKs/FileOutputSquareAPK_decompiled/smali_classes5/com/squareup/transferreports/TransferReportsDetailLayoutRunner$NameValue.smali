.class public final Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;
.super Ljava/lang/Object;
.source "TransferReportsDetailLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NameValue"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;",
        "",
        "name",
        "",
        "value",
        "forFees",
        "",
        "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V",
        "getForFees",
        "()Z",
        "getName",
        "()Ljava/lang/CharSequence;",
        "getValue",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final forFees:Z

.field private final name:Ljava/lang/CharSequence;

.field private final value:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->name:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->value:Ljava/lang/CharSequence;

    iput-boolean p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->forFees:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 958
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    return-void
.end method


# virtual methods
.method public final getForFees()Z
    .locals 1

    .line 958
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->forFees:Z

    return v0
.end method

.method public final getName()Ljava/lang/CharSequence;
    .locals 1

    .line 956
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->name:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getValue()Ljava/lang/CharSequence;
    .locals 1

    .line 957
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->value:Ljava/lang/CharSequence;

    return-object v0
.end method
