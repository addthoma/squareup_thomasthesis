.class final Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;
.super Ljava/lang/Object;
.source "TransferReportsLayoutRunner.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsLayoutRunner;->setHistoryOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

.field final synthetic $token:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->$depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->$token:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .line 458
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    invoke-static {p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->access$getDetailViewEnabled$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 459
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->this$0:Lcom/squareup/transferreports/TransferReportsLayoutRunner;

    invoke-static {p1}, Lcom/squareup/transferreports/TransferReportsLayoutRunner;->access$getScreen$p(Lcom/squareup/transferreports/TransferReportsLayoutRunner;)Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->getOnDetail()Lkotlin/jvm/functions/Function3;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->$depositType:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$setHistoryOnClickListener$1;->$token:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
