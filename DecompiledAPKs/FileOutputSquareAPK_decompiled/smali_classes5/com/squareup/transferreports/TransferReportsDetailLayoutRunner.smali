.class public final Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;
.super Ljava/lang/Object;
.source "TransferReportsDetailLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;,
        Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;,
        Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;,
        Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;,
        Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/transferreports/TransferReportsDetailScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsDetailLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsDetailLayoutRunner.kt\ncom/squareup/transferreports/TransferReportsDetailLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 8 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,1044:1\n1360#2:1045\n1429#2,3:1046\n49#3:1049\n50#3,3:1055\n53#3:1216\n599#4,4:1050\n601#4:1054\n310#5,3:1058\n313#5,3:1067\n310#5,3:1070\n313#5,3:1079\n310#5,3:1082\n313#5,3:1091\n310#5,3:1094\n313#5,3:1103\n310#5,3:1106\n313#5,3:1115\n310#5,3:1118\n313#5,3:1127\n310#5,3:1130\n313#5,3:1139\n310#5,3:1142\n313#5,3:1151\n310#5,3:1154\n313#5,3:1163\n310#5,3:1166\n313#5,3:1175\n310#5,3:1178\n313#5,3:1187\n310#5,3:1190\n313#5,3:1199\n355#5,3:1202\n358#5,3:1211\n35#6,6:1061\n35#6,6:1073\n35#6,6:1085\n35#6,6:1097\n35#6,6:1109\n35#6,6:1121\n35#6,6:1133\n35#6,6:1145\n35#6,6:1157\n35#6,6:1169\n35#6,6:1181\n35#6,6:1193\n35#6,6:1205\n43#7,2:1214\n17#8,2:1217\n17#8,2:1219\n*E\n*S KotlinDebug\n*F\n+ 1 TransferReportsDetailLayoutRunner.kt\ncom/squareup/transferreports/TransferReportsDetailLayoutRunner\n*L\n631#1:1045\n631#1,3:1046\n802#1:1049\n802#1,3:1055\n802#1:1216\n802#1,4:1050\n802#1:1054\n802#1,3:1058\n802#1,3:1067\n802#1,3:1070\n802#1,3:1079\n802#1,3:1082\n802#1,3:1091\n802#1,3:1094\n802#1,3:1103\n802#1,3:1106\n802#1,3:1115\n802#1,3:1118\n802#1,3:1127\n802#1,3:1130\n802#1,3:1139\n802#1,3:1142\n802#1,3:1151\n802#1,3:1154\n802#1,3:1163\n802#1,3:1166\n802#1,3:1175\n802#1,3:1178\n802#1,3:1187\n802#1,3:1190\n802#1,3:1199\n802#1,3:1202\n802#1,3:1211\n802#1,6:1061\n802#1,6:1073\n802#1,6:1085\n802#1,6:1097\n802#1,6:1109\n802#1,6:1121\n802#1,6:1133\n802#1,6:1145\n802#1,6:1157\n802#1,6:1169\n802#1,6:1181\n802#1,6:1193\n802#1,6:1205\n802#1,2:1214\n123#1,2:1217\n124#1,2:1219\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\n\u008b\u0001\u008c\u0001\u008d\u0001\u008e\u0001\u008f\u0001B#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ8\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002002\u0006\u00102\u001a\u00020\u00072\u0006\u00103\u001a\u00020&H\u0002J@\u00104\u001a\u00020*2\u0006\u0010+\u001a\u0002052\u0006\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002002\u0006\u00106\u001a\u00020\u00072\u0006\u00107\u001a\u00020\u00072\u0006\u00103\u001a\u00020&H\u0002J\u0018\u00108\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u00109\u001a\u000200H\u0002J\u0018\u0010:\u001a\u0002002\u0006\u0010;\u001a\u0002002\u0006\u0010<\u001a\u000200H\u0002J\u0012\u0010=\u001a\u0002002\u0008\u0010>\u001a\u0004\u0018\u00010?H\u0002J\u001e\u0010@\u001a\u00020A2\u000c\u0010B\u001a\u0008\u0012\u0004\u0012\u00020D0C2\u0006\u0010E\u001a\u00020FH\u0002J\u001e\u0010G\u001a\n\u0012\u0004\u0012\u00020?\u0018\u00010C2\u000c\u0010H\u001a\u0008\u0012\u0004\u0012\u00020I0CH\u0002J6\u0010J\u001a\u0008\u0012\u0004\u0012\u00020K0\"2\u0006\u0010L\u001a\u00020M2\u0006\u0010N\u001a\u00020&2\u0006\u0010O\u001a\u00020&2\u0006\u0010P\u001a\u00020&2\u0006\u0010Q\u001a\u00020&H\u0002J\u0008\u0010R\u001a\u00020*H\u0002J\u0008\u0010S\u001a\u00020*H\u0002J\u0008\u0010T\u001a\u00020*H\u0002J\u0008\u0010U\u001a\u00020*H\u0002J\u0008\u0010V\u001a\u00020*H\u0002J\u0010\u0010W\u001a\u00020*2\u0006\u0010#\u001a\u00020$H\u0002J\u0010\u0010X\u001a\u00020*2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0018\u0010Y\u001a\u00020*2\u0006\u0010L\u001a\u00020M2\u0006\u0010Q\u001a\u00020&H\u0002J \u0010Z\u001a\u00020*2\u0006\u0010[\u001a\u00020\\2\u0006\u0010;\u001a\u00020]2\u0006\u0010^\u001a\u00020AH\u0002J\u0018\u0010_\u001a\u00020*2\u0006\u0010`\u001a\u00020\\2\u0006\u0010a\u001a\u00020bH\u0002J \u0010c\u001a\u00020*2\u0006\u0010L\u001a\u00020M2\u0006\u0010d\u001a\u00020&2\u0006\u0010Q\u001a\u00020&H\u0002J\u0018\u0010e\u001a\u00020*2\u0006\u0010L\u001a\u00020M2\u0006\u0010a\u001a\u00020bH\u0002J \u0010f\u001a\u00020*2\u0006\u0010L\u001a\u00020M2\u0006\u0010P\u001a\u00020&2\u0006\u0010Q\u001a\u00020&H\u0002J\u0018\u0010g\u001a\u00020*2\u0006\u0010L\u001a\u00020M2\u0006\u0010Q\u001a\u00020&H\u0002J \u0010h\u001a\u00020*2\u0006\u0010L\u001a\u00020M2\u0006\u0010P\u001a\u00020&2\u0006\u0010Q\u001a\u00020&H\u0002J\u0018\u0010i\u001a\u00020*2\u0006\u0010j\u001a\u00020k2\u0006\u0010l\u001a\u00020mH\u0002J\u0018\u0010n\u001a\u00020*2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010o\u001a\u00020pH\u0002J\u0008\u0010q\u001a\u00020*H\u0002J\u0008\u0010r\u001a\u00020*H\u0002J\u0018\u0010s\u001a\u00020*2\u0006\u0010t\u001a\u00020\u00022\u0006\u0010u\u001a\u00020vH\u0016J\u0008\u0010w\u001a\u00020*H\u0002J \u0010x\u001a\u00020*2\u0006\u0010y\u001a\u00020\\2\u0006\u0010z\u001a\u00020A2\u0006\u0010{\u001a\u00020AH\u0002J \u0010|\u001a\u00020*2\u0006\u0010[\u001a\u00020\\2\u0006\u0010;\u001a\u0002002\u0006\u0010<\u001a\u000200H\u0002J\u001e\u0010}\u001a\u00020*2\u000c\u0010B\u001a\u0008\u0012\u0004\u0012\u00020D0C2\u0006\u0010%\u001a\u00020&H\u0002J\u0019\u0010~\u001a\u00020*2\u0007\u0010\u007f\u001a\u00030\u0080\u00012\u0006\u0010%\u001a\u00020&H\u0002J\u001b\u0010\u0081\u0001\u001a\u00020*2\u0006\u0010`\u001a\u00020\\2\u0008\u0010\u0082\u0001\u001a\u00030\u0083\u0001H\u0002J\u001b\u0010\u0084\u0001\u001a\u00020*2\u0007\u0010\u0085\u0001\u001a\u00020\\2\u0007\u0010\u0086\u0001\u001a\u000200H\u0002J\u0018\u0010\u0087\u0001\u001a\u00020*2\r\u0010\u0088\u0001\u001a\u0008\u0012\u0004\u0012\u00020K0CH\u0002J\u001b\u0010\u0089\u0001\u001a\u00020*2\u0007\u0010/\u001a\u00030\u008a\u00012\u0007\u0010\u0086\u0001\u001a\u000200H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00170\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0090\u0001"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/transferreports/TransferReportsDetailScreen;",
        "view",
        "Landroid/view/View;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "actionBarConfigBuilder",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
        "context",
        "Landroid/content/Context;",
        "dataRecyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "defaultWeight",
        "Landroid/text/style/CharacterStyle;",
        "depositDetailRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;",
        "errorSection",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "layoutParams",
        "Landroid/view/ViewGroup$LayoutParams;",
        "listPhrase",
        "Lcom/squareup/util/ListPhrase;",
        "mediumWeight",
        "res",
        "Landroid/content/res/Resources;",
        "rows",
        "",
        "screen",
        "Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;",
        "showToggle",
        "",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "addCardRowForMobile",
        "",
        "row",
        "Lcom/squareup/ui/account/view/SmartLineRow;",
        "type",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;",
        "title",
        "",
        "subtitle",
        "money",
        "hasSplitTender",
        "addCardRowForTablet",
        "Landroid/widget/LinearLayout;",
        "collectedMoney",
        "netTotalMoney",
        "addSummaryRow",
        "name",
        "formatCard",
        "cardBrand",
        "cardSuffix",
        "formatItemization",
        "itemization",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "getFormattedDate",
        "",
        "settledBillEntryList",
        "",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;",
        "index",
        "",
        "getItemizations",
        "returnLineItems",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
        "getNameValueList",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;",
        "settlementReport",
        "Lcom/squareup/protos/client/settlements/SettlementReport;",
        "isActive",
        "isPending",
        "isWithdrawal",
        "feeDetailsAvailable",
        "hideError",
        "hideSections",
        "hideSpinner",
        "onFailure",
        "onLoading",
        "onScreen",
        "onSuccess",
        "setActiveSales",
        "setCard",
        "card",
        "Lcom/squareup/widgets/MessageView;",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "panSuffix",
        "setDateTime",
        "dateTime",
        "sendDate",
        "Lcom/squareup/protos/common/time/DateTime;",
        "setInstantDeposit",
        "showCard",
        "setPayInDeposit",
        "setPendingDeposit",
        "setSameDayDeposit",
        "setStandardDeposit",
        "setToggles",
        "toggles",
        "Landroid/widget/RadioGroup;",
        "checkedButtonType",
        "Lcom/squareup/transferreports/ButtonType;",
        "setUpActionBar",
        "depositType",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "setUpDepositDetailRecycler",
        "showError",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showSpinner",
        "updateBank",
        "bank",
        "bankName",
        "bankAccountSuffix",
        "updateCard",
        "updateCardPaymentsSection",
        "updateCardRow",
        "rowData",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;",
        "updateDateTime",
        "date",
        "Ljava/util/Date;",
        "updateDepositNumber",
        "depositNumber",
        "text",
        "updateDepositSection",
        "nameValueList",
        "updateTitle",
        "Landroid/widget/TextView;",
        "Binding",
        "Factory",
        "NameValue",
        "ReportsRow",
        "RowData",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

.field private final context:Landroid/content/Context;

.field private final dataRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final defaultWeight:Landroid/text/style/CharacterStyle;

.field private depositDetailRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;",
            ">;"
        }
    .end annotation
.end field

.field private final errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final layoutParams:Landroid/view/ViewGroup$LayoutParams;

.field private final listPhrase:Lcom/squareup/util/ListPhrase;

.field private final mediumWeight:Landroid/text/style/CharacterStyle;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Landroid/content/res/Resources;

.field private final rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;",
            ">;"
        }
    .end annotation
.end field

.field private screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

.field private final showToggle:Z

.field private final spinner:Landroid/widget/ProgressBar;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 112
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->data_recycler_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.data_recycler_view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->dataRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 113
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->deposits_report_detail_spinner:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.d\u2026ts_report_detail_spinner)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    .line 115
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/transferreports/R$id;->deposits_report_detail_error_section:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.d\u2026ort_detail_error_section)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 116
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string p2, "ActionBarView.findIn(view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 117
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo p2, "view.resources"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    .line 118
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo p2, "view.context"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->context:Landroid/content/Context;

    .line 119
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhone()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->showToggle:Z

    .line 120
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    .line 121
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget p3, Lcom/squareup/marin/R$dimen;->marin_tall_list_row_height:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/4 p3, -0x1

    .line 120
    invoke-direct {p1, p3, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    .line 123
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->context:Landroid/content/Context;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string p3, "DEFAULT"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1218
    new-instance p3, Lcom/squareup/fonts/FontSpan;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result p2

    invoke-direct {p3, p1, p2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast p3, Landroid/text/style/CharacterStyle;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    .line 124
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->context:Landroid/content/Context;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 1220
    new-instance p3, Lcom/squareup/fonts/FontSpan;

    invoke-static {p2, v0}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result p2

    invoke-direct {p3, p1, p2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast p3, Landroid/text/style/CharacterStyle;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    .line 126
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget p2, Lcom/squareup/utilities/R$string;->list_pattern_long_two_separator:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .line 127
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget p3, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 128
    iget-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/utilities/R$string;->list_pattern_long_final_separator:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p3

    .line 125
    invoke-static {p1, p2, p3}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    const-string p2, "ListPhrase.from(\n      r\u2026long_final_separator)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->listPhrase:Lcom/squareup/util/ListPhrase;

    .line 130
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 131
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 137
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    sget-object p2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$1;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->clearGlyph()V

    .line 139
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setUpDepositDetailRecycler()V

    return-void
.end method

.method public static final synthetic access$addCardRowForMobile(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)V
    .locals 0

    .line 106
    invoke-direct/range {p0 .. p6}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->addCardRowForMobile(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method public static final synthetic access$addCardRowForTablet(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Landroid/widget/LinearLayout;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V
    .locals 0

    .line 106
    invoke-direct/range {p0 .. p7}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->addCardRowForTablet(Landroid/widget/LinearLayout;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method public static final synthetic access$addSummaryRow(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/CharSequence;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->addSummaryRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final synthetic access$formatItemization(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/CharSequence;
    .locals 0

    .line 106
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->formatItemization(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getLayoutParams$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 106
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Landroid/content/res/Resources;
    .locals 0

    .line 106
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$getRows$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Ljava/util/List;
    .locals 0

    .line 106
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;
    .locals 1

    .line 106
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setCard(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setCard(Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setDateTime(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setDateTime(Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/common/time/DateTime;)V

    return-void
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    return-void
.end method

.method public static final synthetic access$setToggles(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Landroid/widget/RadioGroup;Lcom/squareup/transferreports/ButtonType;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setToggles(Landroid/widget/RadioGroup;Lcom/squareup/transferreports/ButtonType;)V

    return-void
.end method

.method public static final synthetic access$updateBank(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/widgets/MessageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateBank(Lcom/squareup/widgets/MessageView;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$updateDepositNumber(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Lcom/squareup/widgets/MessageView;Ljava/lang/CharSequence;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDepositNumber(Lcom/squareup/widgets/MessageView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final synthetic access$updateTitle(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateTitle(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final addCardRowForMobile(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)V
    .locals 1

    if-eqz p6, :cond_0

    .line 736
    iget-object p6, p5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p6}, Lcom/squareup/util/ProtoGlyphs;->splitTenderCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p6

    invoke-virtual {p1, p6}, Lcom/squareup/ui/account/view/SmartLineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 738
    :cond_0
    iget-object p6, p5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p6}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p6

    invoke-virtual {p1, p6}, Lcom/squareup/ui/account/view/SmartLineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 740
    :goto_0
    sget p6, Lcom/squareup/widgets/pos/R$drawable;->selector_payment_badge_refund:I

    invoke-virtual {p1, p6}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeImage(I)V

    .line 741
    sget-object p6, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;->REFUND:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    const/4 v0, 0x1

    if-ne p2, p6, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeVisible(Z)V

    .line 742
    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 743
    invoke-virtual {p1, p4}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 744
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 745
    sget p2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 746
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisible(Z)V

    .line 747
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 748
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method private final addCardRowForTablet(Landroid/widget/LinearLayout;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V
    .locals 2

    .line 760
    sget v0, Lcom/squareup/transferreports/R$id;->glyph:I

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "row.findViewById(R.id.glyph)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p7, :cond_0

    .line 762
    iget-object p7, p5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p7}, Lcom/squareup/util/ProtoGlyphs;->splitTenderCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p7

    invoke-virtual {v0, p7}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    goto :goto_0

    .line 764
    :cond_0
    iget-object p7, p5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p7}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p7

    invoke-virtual {v0, p7}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 766
    :goto_0
    sget p7, Lcom/squareup/transferreports/R$id;->badge:I

    invoke-virtual {p1, p7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p7

    const-string v0, "row.findViewById(R.id.badge)"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p7, Landroid/widget/ImageView;

    .line 767
    sget v0, Lcom/squareup/widgets/pos/R$drawable;->selector_payment_badge_refund:I

    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 768
    check-cast p7, Landroid/view/View;

    sget-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;->REFUND:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    if-ne p2, v0, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    invoke-static {p7, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 769
    sget p2, Lcom/squareup/transferreports/R$id;->title:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p7, "row.findViewById(R.id.title)"

    invoke-static {p2, p7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    .line 770
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 771
    sget p2, Lcom/squareup/transferreports/R$id;->subtitle:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "row.findViewById(R.id.subtitle)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    .line 772
    invoke-virtual {p2, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 774
    sget p2, Lcom/squareup/transferreports/R$id;->collected:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "row.findViewById(R.id.collected)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/widgets/ShorteningTextView;

    .line 775
    iget-object p3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p3, p5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 776
    sget p2, Lcom/squareup/transferreports/R$id;->net_total:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "row.findViewById(R.id.net_total)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/ShorteningTextView;

    .line 777
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final addSummaryRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/CharSequence;)V
    .locals 0

    .line 724
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final formatCard(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .line 695
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/common/strings/R$string;->instant_deposits_linked_card_hint:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "card_brand"

    .line 696
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "card_suffix"

    .line 697
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 698
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(res, com.squ\u2026Suffix)\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final formatItemization(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/CharSequence;
    .locals 5

    const-string v0, ""

    if-nez p1, :cond_0

    .line 635
    check-cast v0, Ljava/lang/CharSequence;

    return-object v0

    .line 637
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_3

    .line 638
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_2

    .line 640
    :cond_3
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    .line 643
    :goto_2
    invoke-static {p1}, Lcom/squareup/quantity/ItemQuantities;->displayNameOnly(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v2

    const-string v3, "name"

    if-eqz v2, :cond_4

    .line 644
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_5

    .line 647
    :cond_4
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/common/strings/R$string;->payment_note_item_name_quantity_unit:I

    .line 646
    invoke-static {v2, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 649
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 650
    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "quantity"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 651
    invoke-static {p1}, Lcom/squareup/quantity/ItemQuantities;->measurementUnit(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    if-eqz p1, :cond_5

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    invoke-static {p1, v2}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    :cond_5
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_6

    goto :goto_4

    :cond_6
    move-object p1, v0

    :goto_4
    check-cast p1, Ljava/lang/CharSequence;

    const-string/jumbo v0, "unit"

    invoke-virtual {v1, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 652
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    const-string p1, "Phrase.from(\n          r\u2026ty())\n          .format()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_5
    return-object v1
.end method

.method private final getFormattedDate(Ljava/util/List;I)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v0, 0x3

    .line 564
    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    .line 565
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p1, p2}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "getDateInstance(SHORT)\n \u2026.request_created_at, US))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getItemizations(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation

    .line 630
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Iterable;

    .line 1045
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1046
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1047
    check-cast v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 631
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1048
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method private final getNameValueList(Lcom/squareup/protos/client/settlements/SettlementReport;ZZZZ)Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/settlements/SettlementReport;",
            "ZZZZ)",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p5

    .line 449
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 451
    new-instance v10, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 452
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/transferreports/R$string;->deposits_report_total_collected:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "res.getText(R.string.dep\u2026s_report_total_collected)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/lang/CharSequence;

    .line 453
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v6, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    const-string v4, "moneyFormatter.format(se\u2026rt.total_collected_money)"

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v10

    .line 451
    invoke-direct/range {v4 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 450
    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "res.getText(R.string.dep\u2026ts_report_estimated_fees)"

    const-string v5, "moneyFormatter.format(se\u2026ntReport.total_fee_money)"

    const-string v6, "moneyFormatter.format(se\u2026tReport.settlement_money)"

    if-eqz p2, :cond_0

    .line 458
    new-instance v7, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 459
    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v9, Lcom/squareup/transferreports/R$string;->deposits_report_estimated_fees:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v8, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 460
    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v9, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v8, v9}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    invoke-direct {v7, v4, v8, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 457
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 466
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/transferreports/R$string;->reports_net_total:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "res.getText(R.string.reports_net_total)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Ljava/lang/CharSequence;

    .line 467
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Ljava/lang/CharSequence;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    move-object v9, v2

    .line 465
    invoke-direct/range {v9 .. v14}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 464
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_0
    if-eqz p3, :cond_2

    .line 472
    new-instance v7, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 473
    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v9, Lcom/squareup/transferreports/R$string;->deposits_report_estimated_fees:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v8, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 474
    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v9, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v8, v9}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 472
    invoke-direct {v7, v4, v8, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 471
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_1

    .line 480
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 481
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/transferreports/R$string;->deposits_report_pending_withdrawal:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "res.getText(R.string.dep\u2026eport_pending_withdrawal)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Ljava/lang/CharSequence;

    .line 482
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Ljava/lang/CharSequence;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    move-object v9, v2

    .line 480
    invoke-direct/range {v9 .. v14}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 479
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 487
    :cond_1
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 488
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/transferreports/R$string;->deposits_report_pending_deposit:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "res.getText(R.string.dep\u2026s_report_pending_deposit)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/lang/CharSequence;

    .line 489
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v2

    .line 487
    invoke-direct/range {v4 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 486
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 495
    :cond_2
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 496
    iget-object v7, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v8, Lcom/squareup/transferreports/R$string;->deposits_report_fees:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    const-string v8, "res.getText(R.string.deposits_report_fees)"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v7, v8}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    .line 497
    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v9, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v8, v9}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 495
    invoke-direct {v4, v7, v8, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 494
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_3

    .line 503
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 504
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/transferreports/R$string;->deposits_report_withdrawal:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "res.getText(R.string.deposits_report_withdrawal)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Ljava/lang/CharSequence;

    .line 505
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Ljava/lang/CharSequence;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    move-object v9, v2

    .line 503
    invoke-direct/range {v9 .. v14}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 502
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 510
    :cond_3
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 511
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/transferreports/R$string;->deposits_report_deposit:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "res.getText(R.string.deposits_report_deposit)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/lang/CharSequence;

    .line 512
    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v2

    .line 510
    invoke-direct/range {v4 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 509
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    :goto_0
    check-cast v3, Ljava/util/List;

    return-object v3
.end method

.method private final hideError()V
    .locals 2

    .line 797
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method private final hideSections()V
    .locals 2

    .line 781
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->depositDetailRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "depositDetailRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->clear()V

    return-void
.end method

.method private final hideSpinner()V
    .locals 2

    .line 789
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final onFailure()V
    .locals 0

    .line 290
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->hideSpinner()V

    .line 291
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->hideSections()V

    .line 292
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->showError()V

    return-void
.end method

.method private final onLoading()V
    .locals 0

    .line 190
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->hideSections()V

    .line 191
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->hideError()V

    .line 192
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->showSpinner()V

    return-void
.end method

.method private final onScreen(Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;)V
    .locals 2

    .line 150
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    .line 151
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onScreen$1;-><init>(Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getDepositType()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setUpActionBar(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V

    .line 155
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getDepositDetailsState()Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-result-object p1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 158
    :cond_1
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->onFailure()V

    goto :goto_0

    .line 157
    :cond_2
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->onSuccess(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    goto :goto_0

    .line 156
    :cond_3
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->onLoading()V

    :goto_0
    return-void
.end method

.method private final onSuccess(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V
    .locals 10

    .line 196
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    const-string v1, "screen"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettlementReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object v2

    if-nez v2, :cond_1

    .line 198
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v3, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    .line 199
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 200
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getDepositType()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-nez v4, :cond_2

    goto/16 :goto_0

    :cond_2
    sget-object v7, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v4}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v4

    aget v4, v7, v4

    const-string v7, "settlementReportWrapper.send_date"

    const-string v8, "settlementReport"

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 255
    :pswitch_0
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setPayInDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;Lcom/squareup/protos/common/time/DateTime;)V

    goto/16 :goto_0

    .line 247
    :pswitch_1
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v9, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v9, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;-><init>(Lcom/squareup/protos/common/time/DateTime;)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getShowCard()Z

    move-result v2

    .line 251
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v4, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v4}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v4

    .line 248
    invoke-direct {p0, v3, v2, v4}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setInstantDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V

    goto/16 :goto_0

    .line 240
    :pswitch_2
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v9, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v9, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;-><init>(Lcom/squareup/protos/common/time/DateTime;)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 241
    invoke-direct {p0, v3, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setSameDayDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;Z)V

    goto/16 :goto_0

    .line 232
    :pswitch_3
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v9, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v9, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;-><init>(Lcom/squareup/protos/common/time/DateTime;)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 233
    invoke-direct {p0, v3, v5, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setStandardDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V

    goto/16 :goto_0

    .line 224
    :pswitch_4
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v9, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v9, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;-><init>(Lcom/squareup/protos/common/time/DateTime;)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 225
    invoke-direct {p0, v3, v5, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setStandardDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V

    goto :goto_0

    .line 216
    :pswitch_5
    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v9, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;

    iget-object v2, v2, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v9, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DateTimeRow;-><init>(Lcom/squareup/protos/common/time/DateTime;)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 217
    invoke-direct {p0, v3, v6, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setStandardDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V

    goto :goto_0

    .line 211
    :pswitch_6
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 210
    invoke-direct {p0, v3, v6, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setPendingDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V

    goto :goto_0

    .line 206
    :pswitch_7
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 205
    invoke-direct {p0, v3, v5, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setPendingDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V

    goto :goto_0

    .line 202
    :pswitch_8
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v2, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getFeeDetailsAvailable()Z

    move-result v2

    .line 201
    invoke-direct {p0, v3, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setActiveSales(Lcom/squareup/protos/client/settlements/SettlementReport;Z)V

    .line 258
    :goto_0
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 260
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getSettledBillEntriesResponseList()Ljava/util/List;

    move-result-object p1

    .line 262
    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v6

    if-eqz v3, :cond_e

    .line 263
    iget-boolean v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->showToggle:Z

    if-eqz v3, :cond_d

    .line 264
    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$MobileCardPaymentsTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$MobileCardPaymentsTitleRow;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$MobileToggleRow;

    iget-object v7, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez v7, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {v7}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getCheckedButtonType()Lcom/squareup/transferreports/ButtonType;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$MobileToggleRow;-><init>(Lcom/squareup/transferreports/ButtonType;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 267
    :cond_d
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TabletCardPaymentsTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TabletCardPaymentsTitleRow;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    .line 271
    iget-object v1, v1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;->settled_bill_entry:Ljava/util/List;

    const-string v3, "response.settled_bill_entry"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    iget-boolean v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->showToggle:Z

    .line 270
    invoke-direct {p0, v1, v3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateCardPaymentsSection(Ljava/util/List;Z)V

    goto :goto_2

    .line 277
    :cond_e
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->hideSpinner()V

    .line 278
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->hideError()V

    .line 280
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v6

    if-eqz p1, :cond_f

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->getHasMoreBillEntries()Z

    move-result p1

    if-eqz p1, :cond_f

    const/4 v5, 0x1

    .line 281
    :cond_f
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->depositDetailRecycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_10

    const-string v0, "depositDetailRecycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 282
    :cond_10
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;

    invoke-direct {v0, p0, v5}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;-><init>(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final setActiveSales(Lcom/squareup/protos/client/settlements/SettlementReport;Z)V
    .locals 8

    .line 314
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$SummaryTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$SummaryTitleRow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v3, p1

    move v7, p2

    .line 315
    invoke-direct/range {v2 .. v7}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getNameValueList(Lcom/squareup/protos/client/settlements/SettlementReport;ZZZZ)Ljava/util/List;

    move-result-object p1

    .line 322
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDepositSection(Ljava/util/List;)V

    return-void
.end method

.method private final setCard(Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)V
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    invoke-static {p2, v0}, Lcom/squareup/card/CardBrands;->toCharSequence(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p2

    check-cast p3, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateCard(Lcom/squareup/widgets/MessageView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setDateTime(Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 1

    .line 299
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p2, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    const-string v0, "asDate(sendDate, US)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDateTime(Lcom/squareup/widgets/MessageView;Ljava/util/Date;)V

    return-void
.end method

.method private final setInstantDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V
    .locals 9

    if-eqz p2, :cond_0

    .line 419
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$CardRow;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v2, "settlementReport.card_brand"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    const-string v3, "settlementReport.pan_suffix"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$CardRow;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 420
    :cond_0
    iget-object p2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 421
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$BankRow;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    const-string v2, "settlementReport.bank_name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    const-string v3, "settlementReport.bank_account_suffix"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$BankRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$DepositTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$DepositTitleRow;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositNumberRow;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    const-string v2, "settlementReport.token"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositNumberRow;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move-object v4, p1

    move v8, p3

    .line 425
    invoke-direct/range {v3 .. v8}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getNameValueList(Lcom/squareup/protos/client/settlements/SettlementReport;ZZZZ)Ljava/util/List;

    move-result-object p2

    .line 433
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p3

    add-int/lit8 p3, p3, -0x1

    .line 434
    new-instance v6, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 435
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/transferreports/R$string;->deposits_report_instant_deposit_fee:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.getText(R.string.dep\u2026port_instant_deposit_fee)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v0, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    .line 436
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string p1, "moneyFormatter.format(se\u2026Report.deposit_fee_money)"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    .line 434
    invoke-direct/range {v0 .. v5}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 432
    invoke-interface {p2, p3, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 439
    invoke-direct {p0, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDepositSection(Ljava/util/List;)V

    return-void
.end method

.method private final setPayInDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 5

    .line 389
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$SummaryTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$SummaryTitleRow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 391
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;

    .line 392
    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/common/strings/R$string;->amount:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "res.getText(com.squareup\u2026.strings.R.string.amount)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v2, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 393
    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "moneyFormatter.format(se\u2026tReport.settlement_money)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    invoke-direct {v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 390
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 398
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/transferreports/R$string;->deposits_report_pay_in_source:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "res.getText(R.string.dep\u2026its_report_pay_in_source)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 400
    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    invoke-static {v2, v3}, Lcom/squareup/card/CardBrands;->toCharSequence(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 401
    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    .line 399
    invoke-direct {p0, v2, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->formatCard(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 397
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;

    invoke-direct {v2, v1, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 396
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 406
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;

    .line 407
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/transferreports/R$string;->deposits_report_pay_in_date:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "res.getText(R.string.deposits_report_pay_in_date)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x3

    .line 408
    invoke-static {v2, v2}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p2, v3}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    const-string v2, "getDateTimeInstance(SHOR\u2026mat(asDate(sendDate, US))"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/CharSequence;

    .line 406
    invoke-direct {v0, v1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 405
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private final setPendingDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V
    .locals 8

    .line 330
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$SummaryTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$SummaryTitleRow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v2, p0

    move-object v3, p1

    move v6, p2

    move v7, p3

    .line 331
    invoke-direct/range {v2 .. v7}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getNameValueList(Lcom/squareup/protos/client/settlements/SettlementReport;ZZZZ)Ljava/util/List;

    move-result-object p1

    .line 338
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDepositSection(Ljava/util/List;)V

    return-void
.end method

.method private final setSameDayDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;Z)V
    .locals 10

    .line 365
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$CardRow;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const-string v3, "settlementReport.card_brand"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    const-string v4, "settlementReport.pan_suffix"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$CardRow;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$DepositTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$DepositTitleRow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositNumberRow;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    const-string v3, "settlementReport.token"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositNumberRow;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v4, p0

    move-object v5, p1

    move v9, p2

    .line 368
    invoke-direct/range {v4 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getNameValueList(Lcom/squareup/protos/client/settlements/SettlementReport;ZZZZ)Ljava/util/List;

    move-result-object p2

    .line 376
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 377
    new-instance v7, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 378
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/transferreports/R$string;->deposits_report_same_day_deposit_fee:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "res.getText(R.string.dep\u2026ort_same_day_deposit_fee)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/CharSequence;

    .line 379
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string p1, "moneyFormatter.format(se\u2026Report.deposit_fee_money)"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v7

    .line 377
    invoke-direct/range {v1 .. v6}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 375
    invoke-interface {p2, v0, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 382
    invoke-direct {p0, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDepositSection(Ljava/util/List;)V

    return-void
.end method

.method private final setStandardDeposit(Lcom/squareup/protos/client/settlements/SettlementReport;ZZ)V
    .locals 10

    .line 346
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$BankRow;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    const-string v3, "settlementReport.bank_name"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    const-string v4, "settlementReport.bank_account_suffix"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$BankRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$DepositTitleRow;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TitleRow$DepositTitleRow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositNumberRow;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    const-string v3, "settlementReport.token"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositNumberRow;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v4, p0

    move-object v5, p1

    move v8, p2

    move v9, p3

    .line 351
    invoke-direct/range {v4 .. v9}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getNameValueList(Lcom/squareup/protos/client/settlements/SettlementReport;ZZZZ)Ljava/util/List;

    move-result-object p1

    .line 358
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateDepositSection(Ljava/util/List;)V

    return-void
.end method

.method private final setToggles(Landroid/widget/RadioGroup;Lcom/squareup/transferreports/ButtonType;)V
    .locals 1

    .line 573
    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p2}, Lcom/squareup/transferreports/ButtonType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 575
    sget p2, Lcom/squareup/transferreports/R$id;->deposits_report_detail_net_total:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 574
    :cond_1
    sget p2, Lcom/squareup/transferreports/R$id;->deposits_report_detail_collected:I

    .line 572
    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->check(I)V

    .line 579
    new-instance p2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;-><init>(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Landroid/widget/RadioGroup;)V

    check-cast p2, Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;)V
    .locals 3

    .line 186
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x1

    .line 167
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 168
    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpActionBar$1;

    invoke-direct {v1, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpActionBar$1;-><init>(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 171
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    .line 172
    sget-object v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p2}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result p2

    aget p2, v2, p2

    packed-switch p2, :pswitch_data_0

    .line 181
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_pay_in:I

    goto :goto_0

    .line 180
    :pswitch_1
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_no_deposit:I

    goto :goto_0

    .line 179
    :pswitch_2
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_instant_deposit:I

    goto :goto_0

    .line 178
    :pswitch_3
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_same_day_deposit:I

    goto :goto_0

    .line 177
    :pswitch_4
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_next_business_day_deposit:I

    goto :goto_0

    .line 176
    :pswitch_5
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_withdrawal:I

    goto :goto_0

    .line 175
    :pswitch_6
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_pending_withdrawal:I

    goto :goto_0

    .line 174
    :pswitch_7
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_pending_deposit:I

    goto :goto_0

    .line 173
    :pswitch_8
    sget p2, Lcom/squareup/transferreports/R$string;->deposits_report_active_sales:I

    .line 171
    :goto_0
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 170
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 186
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final setUpDepositDetailRecycler()V
    .locals 6

    .line 802
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->dataRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 1049
    sget-object v2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 1050
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1051
    new-instance v2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 1055
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 1056
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 1059
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 804
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_detail_date_time:I

    .line 1061
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1059
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1058
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1071
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 812
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_detail_bank_or_card:I

    .line 1073
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1071
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1070
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1083
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 820
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_detail_bank_or_card:I

    .line 1085
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$3;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$3;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1083
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1082
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1095
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$4;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 828
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_title_section:I

    .line 1097
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$4;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$4;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1095
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1094
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1107
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$5;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 847
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_detail_deposit_number:I

    .line 1109
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$5;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$5;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1107
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1106
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1119
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$6;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$6;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 855
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_clickable_fee_row:I

    .line 1121
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$6;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$6;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 866
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 1118
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1131
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$7;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$7;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 870
    sget v4, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    .line 1133
    new-instance v5, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$7;

    invoke-direct {v5, v4, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$7;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 880
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v0, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 1130
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1143
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$8;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$8;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 884
    sget v4, Lcom/squareup/transferreports/R$layout;->transfer_reports_detail_toggles:I

    .line 1145
    new-instance v5, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$8;

    invoke-direct {v5, v4, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$8;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1143
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1142
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1155
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$9;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$9;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 892
    sget v4, Lcom/squareup/transferreports/R$layout;->transfer_reports_detail_card_payments_title:I

    .line 1157
    new-instance v5, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$create$9;

    invoke-direct {v5, v4}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$create$9;-><init>(I)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1155
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1154
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1167
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$10;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$10;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 896
    sget v4, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    .line 1169
    new-instance v5, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$9;

    invoke-direct {v5, v4, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$9;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 904
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v0, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 1166
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1179
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$11;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$11;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 908
    sget v4, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    .line 1181
    new-instance v5, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$10;

    invoke-direct {v5, v4, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$10;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 922
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v0, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 1178
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1191
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$12;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$row$12;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 926
    sget v4, Lcom/squareup/transferreports/R$layout;->transfer_reports_card_payments_row:I

    .line 1193
    new-instance v5, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$11;

    invoke-direct {v5, v4, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$11;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v5}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 939
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    invoke-static {v0, v3}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 1190
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1203
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$$special$$inlined$extraItem$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 943
    sget v3, Lcom/squareup/transferreports/R$layout;->transfer_reports_load_more_layout:I

    .line 1205
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$12;

    invoke-direct {v4, v3, p0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setUpDepositDetailRecycler$$inlined$adopt$lambda$12;-><init>(ILcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1203
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1202
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1214
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v3, 0x0

    .line 949
    invoke-virtual {v0, v3}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 950
    sget v3, Lcom/squareup/transferreports/R$style;->DepositsRecyclerEdges:I

    invoke-virtual {v0, v3}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefStyleRes(I)V

    .line 951
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 1214
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 1053
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->depositDetailRecycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 1050
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final showError()V
    .locals 2

    .line 793
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->errorSection:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method private final showSpinner()V
    .locals 2

    .line 785
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->spinner:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final updateBank(Lcom/squareup/widgets/MessageView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 670
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/transferreports/R$string;->deposits_report_bank_name_and_bank_account_suffix:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 671
    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "bank_name"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 672
    check-cast p3, Ljava/lang/CharSequence;

    const-string v0, "bank_account_suffix"

    invoke-virtual {p2, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 673
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateCard(Lcom/squareup/widgets/MessageView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .line 681
    invoke-direct {p0, p2, p3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->formatCard(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateCardPaymentsSection(Ljava/util/List;Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;",
            ">;Z)V"
        }
    .end annotation

    .line 597
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_6

    .line 598
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;

    .line 599
    invoke-direct {p0, p1, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getFormattedDate(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v2, :cond_0

    if-lez v2, :cond_1

    add-int/lit8 v6, v2, -0x1

    .line 600
    invoke-direct {p0, p1, v6}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getFormattedDate(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v5

    if-eqz v6, :cond_1

    .line 601
    :cond_0
    iget-object v6, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    new-instance v7, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$SummaryRow;

    check-cast v4, Ljava/lang/CharSequence;

    iget-object v8, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->mediumWeight:Landroid/text/style/CharacterStyle;

    invoke-static {v4, v8}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v7, v4}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$SummaryRow;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 607
    :cond_1
    iget-object v4, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    sget-object v6, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;->PAYMENT:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    if-ne v4, v6, :cond_4

    .line 608
    iget-object v4, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    iget-object v4, v4, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;->payment_token:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v5, :cond_2

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    .line 609
    :goto_1
    iget-object v4, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v4, :cond_3

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    .line 611
    :cond_4
    iget-object v4, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_token_group:Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;

    iget-object v4, v4, Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;->refund_uuid:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v5, :cond_5

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    .line 612
    :goto_2
    iget-object v4, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    const-string v6, "settledBillEntry.cart.return_line_items"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->getItemizations(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    :goto_3
    move-object v9, v4

    move v12, v5

    .line 617
    new-instance v4, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;

    .line 618
    iget-object v7, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->settled_bill_entry_type:Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    const-string v5, "settledBillEntry.settled_bill_entry_type"

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 619
    iget-object v8, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->request_created_at:Lcom/squareup/protos/common/time/DateTime;

    const-string v5, "settledBillEntry.request_created_at"

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 621
    iget-object v10, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->collected_money:Lcom/squareup/protos/common/Money;

    const-string v5, "settledBillEntry.collected_money"

    invoke-static {v10, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 622
    iget-object v11, v3, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->to_user_money:Lcom/squareup/protos/common/Money;

    const-string v3, "settledBillEntry.to_user_money"

    invoke-static {v11, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v4

    .line 617
    invoke-direct/range {v6 .. v12}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;-><init>(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V

    .line 615
    invoke-direct {p0, v4, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->updateCardRow(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;Z)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_6
    return-void
.end method

.method private final updateCardRow(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;Z)V
    .locals 10

    .line 524
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getItemization()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->listPhrase:Lcom/squareup/util/ListPhrase;

    check-cast v0, Ljava/lang/Iterable;

    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$updateCardRow$items$1$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    invoke-direct {v2, v3}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$updateCardRow$items$1$1;-><init>(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$sam$i$com_squareup_text_Formatter$0;

    invoke-direct {v3, v2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$sam$i$com_squareup_text_Formatter$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/text/Formatter;

    invoke-virtual {v1, v0, v3}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.getText(com.squareup\u2026default_itemization_name)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    move-object v5, v0

    const-string v0, "getTimeInstance(SHORT).f\u2026e(rowData.createdAt, US))"

    const/4 v1, 0x3

    if-eqz p2, :cond_4

    .line 529
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->screen:Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    if-nez p2, :cond_1

    const-string v2, "screen"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getCheckedButtonType()Lcom/squareup/transferreports/ButtonType;

    move-result-object p2

    sget-object v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p2}, Lcom/squareup/transferreports/ButtonType;->ordinal()I

    move-result p2

    aget p2, v2, p2

    const/4 v2, 0x1

    if-eq p2, v2, :cond_3

    const/4 v2, 0x2

    if-ne p2, v2, :cond_2

    .line 531
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getNetTotal()Lcom/squareup/protos/common/Money;

    move-result-object p2

    goto :goto_1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 530
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getCollected()Lcom/squareup/protos/common/Money;

    move-result-object p2

    :goto_1
    move-object v6, p2

    .line 533
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 534
    new-instance v8, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$MobileCardRow;

    .line 535
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getType()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    move-result-object v3

    .line 536
    invoke-static {v1}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getCreatedAt()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v2

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v2, v4}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 537
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    .line 536
    invoke-static {v1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    .line 541
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getHasSplitTender()Z

    move-result v7

    move-object v2, v8

    .line 534
    invoke-direct/range {v2 .. v7}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$MobileCardRow;-><init>(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)V

    .line 533
    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 545
    :cond_4
    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 546
    new-instance v9, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TabletCardRow;

    .line 547
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getType()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    move-result-object v3

    .line 548
    invoke-static {v1}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getCreatedAt()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v2

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v2, v4}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 549
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->defaultWeight:Landroid/text/style/CharacterStyle;

    .line 548
    invoke-static {v1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    .line 552
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getCollected()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 553
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getNetTotal()Lcom/squareup/protos/common/Money;

    move-result-object v7

    .line 554
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$RowData;->getHasSplitTender()Z

    move-result v8

    move-object v2, v9

    .line 546
    invoke-direct/range {v2 .. v8}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$TabletCardRow;-><init>(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V

    .line 545
    invoke-interface {p2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    return-void
.end method

.method private final updateDateTime(Lcom/squareup/widgets/MessageView;Ljava/util/Date;)V
    .locals 2

    .line 660
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/transferreports/R$string;->deposits_report_sent_date_time:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const/4 v1, 0x3

    .line 661
    invoke-static {v1, v1}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "sent_date_time"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 662
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateDepositNumber(Lcom/squareup/widgets/MessageView;Ljava/lang/CharSequence;)V
    .locals 0

    .line 705
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateDepositSection(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;",
            ">;)V"
        }
    .end annotation

    .line 709
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;

    .line 710
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->rows:Ljava/util/List;

    .line 711
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->getForFees()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 712
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$FeeBreakdownRow;

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->getValue()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$FeeBreakdownRow;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;

    goto :goto_1

    .line 714
    :cond_0
    new-instance v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$NameValue;->getValue()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$DepositBreakdownRow;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;

    .line 710
    :goto_1
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final updateTitle(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0

    .line 688
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/transferreports/TransferReportsDetailScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailScreen;->getContent()Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object p2, p0

    check-cast p2, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    invoke-direct {p2, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->onScreen(Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 106
    check-cast p1, Lcom/squareup/transferreports/TransferReportsDetailScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->showRendering(Lcom/squareup/transferreports/TransferReportsDetailScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
