.class final Lcom/squareup/transferreports/RealTransferReportsLoader$onGetTransferReports$1;
.super Ljava/lang/Object;
.source "RealTransferReportsLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/RealTransferReportsLoader;->onGetTransferReports()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u001e\u0010\u0002\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/RealTransferReportsLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/RealTransferReportsLoader$onGetTransferReports$1;->this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
            ">;+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
            ">;>;)",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 78
    instance-of v2, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v2, :cond_0

    move-object/from16 v2, p0

    .line 79
    iget-object v3, v2, Lcom/squareup/transferreports/RealTransferReportsLoader$onGetTransferReports$1;->this$0:Lcom/squareup/transferreports/RealTransferReportsLoader;

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    check-cast v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    invoke-static {v3, v0, v1}, Lcom/squareup/transferreports/RealTransferReportsLoader;->access$onSuccess(Lcom/squareup/transferreports/RealTransferReportsLoader;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object/from16 v2, p0

    .line 81
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    sget-object v4, Lcom/squareup/transferreports/TransferReportsLoader$State;->FAILURE:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xffe

    const/16 v17, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v17}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/RealTransferReportsLoader$onGetTransferReports$1;->apply(Lkotlin/Pair;)Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object p1

    return-object p1
.end method
