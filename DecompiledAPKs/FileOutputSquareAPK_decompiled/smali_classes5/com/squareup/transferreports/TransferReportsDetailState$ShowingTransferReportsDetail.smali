.class public final Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;
.super Lcom/squareup/transferreports/TransferReportsDetailState;
.source "TransferReportsDetailState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingTransferReportsDetail"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0019\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "reportsSnapshot",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "checkedButtonType",
        "Lcom/squareup/transferreports/ButtonType;",
        "(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;)V",
        "getCheckedButtonType",
        "()Lcom/squareup/transferreports/ButtonType;",
        "getReportsSnapshot",
        "()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final checkedButtonType:Lcom/squareup/transferreports/ButtonType;

.field private final reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail$Creator;

    invoke-direct {v0}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail$Creator;-><init>()V

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;)V
    .locals 1

    const-string v0, "reportsSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkedButtonType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/transferreports/TransferReportsDetailState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;ILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->copy(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;)Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public final component2()Lcom/squareup/transferreports/ButtonType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    return-object v0
.end method

.method public final copy(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;)Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;
    .locals 1

    const-string v0, "reportsSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkedButtonType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    invoke-direct {v0, p1, p2}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    iget-object p1, p1, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCheckedButtonType()Lcom/squareup/transferreports/ButtonType;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    return-object v0
.end method

.method public final getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingTransferReportsDetail(reportsSnapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", checkedButtonType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->reportsSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->checkedButtonType:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
