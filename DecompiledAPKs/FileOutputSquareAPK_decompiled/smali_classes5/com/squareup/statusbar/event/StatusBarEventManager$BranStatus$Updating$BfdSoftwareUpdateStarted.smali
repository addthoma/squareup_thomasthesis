.class public final Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;
.super Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;
.source "StatusBarEventManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BfdSoftwareUpdateStarted"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;",
        "()V",
        "percentage",
        "",
        "getPercentage",
        "()I",
        "step",
        "getStep",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;

.field private static final percentage:I = 0x0

# The value of this static final field might be set in the static constructor
.field private static final step:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 169
    new-instance v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;

    invoke-direct {v0}, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;-><init>()V

    sput-object v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;->INSTANCE:Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;

    const/4 v0, 0x1

    .line 170
    sput v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;->step:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, v0}, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public getPercentage()I
    .locals 1

    .line 171
    sget v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;->percentage:I

    return v0
.end method

.method public getStep()I
    .locals 1

    .line 170
    sget v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;->step:I

    return v0
.end method
