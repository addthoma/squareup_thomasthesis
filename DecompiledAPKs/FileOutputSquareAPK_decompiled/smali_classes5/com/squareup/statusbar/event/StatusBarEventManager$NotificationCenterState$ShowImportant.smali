.class public final Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;
.super Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;
.source "StatusBarEventManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowImportant"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;",
        "count",
        "",
        "(I)V",
        "getCount",
        "()I",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final count:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 143
    invoke-direct {p0, v0}, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;IILjava/lang/Object;)Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->copy(I)Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;
    .locals 1

    new-instance v0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;

    invoke-direct {v0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;

    iget v0, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    iget p1, p1, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()I
    .locals 1

    .line 143
    iget v0, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowImportant(count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState$ShowImportant;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
