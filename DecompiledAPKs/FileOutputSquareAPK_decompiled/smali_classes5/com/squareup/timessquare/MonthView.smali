.class public Lcom/squareup/timessquare/MonthView;
.super Landroid/widget/LinearLayout;
.source "MonthView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/timessquare/MonthView$Listener;
    }
.end annotation


# instance fields
.field private alwaysDigitNumbers:Z

.field dayNamesHeaderRowView:Landroid/view/View;

.field private decorators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;"
        }
    .end annotation
.end field

.field grid:Lcom/squareup/timessquare/CalendarGridView;

.field private isRtl:Z

.field private listener:Lcom/squareup/timessquare/MonthView$Listener;

.field private locale:Ljava/util/Locale;

.field title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 103
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static create(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Ljava/text/DateFormat;Lcom/squareup/timessquare/MonthView$Listener;Ljava/util/Calendar;IIIIZIZLjava/util/Locale;ZLcom/squareup/timessquare/DayViewAdapter;)Lcom/squareup/timessquare/MonthView;
    .locals 16

    const/4 v13, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p13

    move-object/from16 v14, p12

    move-object/from16 v15, p14

    .line 34
    invoke-static/range {v0 .. v15}, Lcom/squareup/timessquare/MonthView;->create(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Ljava/text/DateFormat;Lcom/squareup/timessquare/MonthView$Listener;Ljava/util/Calendar;IIIIZIZZLjava/util/List;Ljava/util/Locale;Lcom/squareup/timessquare/DayViewAdapter;)Lcom/squareup/timessquare/MonthView;

    move-result-object v0

    return-object v0
.end method

.method public static create(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Ljava/text/DateFormat;Lcom/squareup/timessquare/MonthView$Listener;Ljava/util/Calendar;IIIIZIZZLjava/util/List;Ljava/util/Locale;Lcom/squareup/timessquare/DayViewAdapter;)Lcom/squareup/timessquare/MonthView;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/LayoutInflater;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/timessquare/MonthView$Listener;",
            "Ljava/util/Calendar;",
            "IIIIZIZZ",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;",
            "Ljava/util/Locale;",
            "Lcom/squareup/timessquare/DayViewAdapter;",
            ")",
            "Lcom/squareup/timessquare/MonthView;"
        }
    .end annotation

    move-object v0, p4

    move/from16 v1, p6

    .line 44
    sget v2, Lcom/squareup/timessquare/R$layout;->month:I

    const/4 v3, 0x0

    move-object v4, p0

    move-object v5, p1

    invoke-virtual {p1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/timessquare/MonthView;

    .line 47
    new-instance v4, Landroid/widget/TextView;

    new-instance v5, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v2}, Lcom/squareup/timessquare/MonthView;->getContext()Landroid/content/Context;

    move-result-object v6

    move/from16 v7, p8

    invoke-direct {v5, v6, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v4, v2, Lcom/squareup/timessquare/MonthView;->title:Landroid/widget/TextView;

    .line 48
    sget v4, Lcom/squareup/timessquare/R$id;->calendar_grid:I

    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/MonthView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/timessquare/CalendarGridView;

    iput-object v4, v2, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    .line 49
    sget v4, Lcom/squareup/timessquare/R$id;->day_names_header_row:I

    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/MonthView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v2, Lcom/squareup/timessquare/MonthView;->dayNamesHeaderRowView:Landroid/view/View;

    .line 52
    iget-object v4, v2, Lcom/squareup/timessquare/MonthView;->title:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v3}, Lcom/squareup/timessquare/MonthView;->addView(Landroid/view/View;I)V

    move-object/from16 v4, p15

    .line 54
    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/MonthView;->setDayViewAdapter(Lcom/squareup/timessquare/DayViewAdapter;)V

    move v4, p5

    .line 55
    invoke-virtual {v2, p5}, Lcom/squareup/timessquare/MonthView;->setDividerColor(I)V

    move/from16 v4, p7

    .line 56
    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/MonthView;->setDayTextColor(I)V

    move/from16 v4, p9

    .line 57
    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/MonthView;->setDisplayHeader(Z)V

    move/from16 v4, p10

    .line 58
    invoke-virtual {v2, v4}, Lcom/squareup/timessquare/MonthView;->setHeaderTextColor(I)V

    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {v2, v1}, Lcom/squareup/timessquare/MonthView;->setDayBackground(I)V

    .line 64
    :cond_0
    invoke-static/range {p14 .. p14}, Lcom/squareup/timessquare/MonthView;->isRtl(Ljava/util/Locale;)Z

    move-result v1

    iput-boolean v1, v2, Lcom/squareup/timessquare/MonthView;->isRtl:Z

    move-object/from16 v1, p14

    .line 65
    iput-object v1, v2, Lcom/squareup/timessquare/MonthView;->locale:Ljava/util/Locale;

    move/from16 v1, p12

    .line 66
    iput-boolean v1, v2, Lcom/squareup/timessquare/MonthView;->alwaysDigitNumbers:Z

    .line 67
    invoke-virtual {p4}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v1

    .line 68
    iget-object v4, v2, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v4, v3}, Lcom/squareup/timessquare/CalendarGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/timessquare/CalendarRowView;

    if-eqz p11, :cond_2

    const/4 v5, 0x7

    .line 71
    invoke-virtual {p4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    :goto_0
    if-ge v3, v5, :cond_1

    .line 73
    iget-boolean v7, v2, Lcom/squareup/timessquare/MonthView;->isRtl:Z

    invoke-static {v1, v3, v7}, Lcom/squareup/timessquare/MonthView;->getDayOfWeek(IIZ)I

    move-result v7

    invoke-virtual {p4, v5, v7}, Ljava/util/Calendar;->set(II)V

    .line 74
    invoke-virtual {v4, v3}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 75
    invoke-virtual {p4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    move-object v9, p2

    invoke-virtual {p2, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {p4, v5, v6}, Ljava/util/Calendar;->set(II)V

    goto :goto_1

    .line 79
    :cond_2
    iget-object v0, v2, Lcom/squareup/timessquare/MonthView;->dayNamesHeaderRowView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    move-object v0, p3

    .line 82
    iput-object v0, v2, Lcom/squareup/timessquare/MonthView;->listener:Lcom/squareup/timessquare/MonthView$Listener;

    move-object/from16 v0, p13

    .line 83
    iput-object v0, v2, Lcom/squareup/timessquare/MonthView;->decorators:Ljava/util/List;

    return-object v2
.end method

.method private static getDayOfWeek(IIZ)I
    .locals 0

    add-int/2addr p0, p1

    if-eqz p2, :cond_0

    rsub-int/lit8 p0, p0, 0x8

    :cond_0
    return p0
.end method

.method private static isRtl(Ljava/util/Locale;)Z
    .locals 3

    .line 97
    invoke-virtual {p0, p0}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    invoke-static {p0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result p0

    const/4 v1, 0x1

    if-eq p0, v1, :cond_0

    const/4 v2, 0x2

    if-ne p0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public getDecorators()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->decorators:Ljava/util/List;

    return-object v0
.end method

.method public init(Lcom/squareup/timessquare/MonthDescriptor;Ljava/util/List;ZLandroid/graphics/Typeface;Landroid/graphics/Typeface;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/timessquare/MonthDescriptor;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/MonthCellDescriptor;",
            ">;>;Z",
            "Landroid/graphics/Typeface;",
            "Landroid/graphics/Typeface;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 116
    invoke-static/range {p0 .. p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const-string v6, "Initializing MonthView (%d) for %s"

    invoke-static {v6, v3}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 118
    iget-object v3, v0, Lcom/squareup/timessquare/MonthView;->title:Landroid/widget/TextView;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/timessquare/MonthDescriptor;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-boolean v3, v0, Lcom/squareup/timessquare/MonthView;->alwaysDigitNumbers:Z

    if-eqz v3, :cond_0

    .line 121
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    goto :goto_0

    .line 123
    :cond_0
    iget-object v3, v0, Lcom/squareup/timessquare/MonthView;->locale:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    .line 126
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v8

    .line 127
    iget-object v9, v0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v9, v8}, Lcom/squareup/timessquare/CalendarGridView;->setNumRows(I)V

    const/4 v9, 0x0

    :goto_1
    const/4 v10, 0x6

    if-ge v9, v10, :cond_6

    .line 129
    iget-object v11, v0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Lcom/squareup/timessquare/CalendarGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/squareup/timessquare/CalendarRowView;

    .line 130
    iget-object v13, v0, Lcom/squareup/timessquare/MonthView;->listener:Lcom/squareup/timessquare/MonthView$Listener;

    invoke-virtual {v11, v13}, Lcom/squareup/timessquare/CalendarRowView;->setListener(Lcom/squareup/timessquare/MonthView$Listener;)V

    if-ge v9, v8, :cond_5

    .line 132
    invoke-virtual {v11, v5}, Lcom/squareup/timessquare/CalendarRowView;->setVisibility(I)V

    move-object/from16 v13, p2

    .line 133
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    const/4 v14, 0x0

    .line 134
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v15

    if-ge v14, v15, :cond_4

    .line 135
    iget-boolean v15, v0, Lcom/squareup/timessquare/MonthView;->isRtl:Z

    if-eqz v15, :cond_1

    rsub-int/lit8 v15, v14, 0x6

    goto :goto_3

    :cond_1
    move v15, v14

    :goto_3
    invoke-interface {v9, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/timessquare/MonthCellDescriptor;

    .line 136
    invoke-virtual {v11, v14}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    move-object/from16 v10, v16

    check-cast v10, Lcom/squareup/timessquare/CalendarCellView;

    .line 138
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->getValue()I

    move-result v5

    int-to-long v4, v5

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 139
    invoke-virtual {v10}, Lcom/squareup/timessquare/CalendarCellView;->getDayOfMonthTextView()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 140
    invoke-virtual {v10}, Lcom/squareup/timessquare/CalendarCellView;->getDayOfMonthTextView()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    :cond_2
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->isCurrentMonth()Z

    move-result v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setEnabled(Z)V

    const/4 v4, 0x1

    xor-int/lit8 v5, p3, 0x1

    .line 143
    invoke-virtual {v10, v5}, Lcom/squareup/timessquare/CalendarCellView;->setClickable(Z)V

    .line 145
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->isSelectable()Z

    move-result v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setSelectable(Z)V

    .line 146
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->isSelected()Z

    move-result v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setSelected(Z)V

    .line 147
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->isCurrentMonth()Z

    move-result v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setCurrentMonth(Z)V

    .line 148
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->isToday()Z

    move-result v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setToday(Z)V

    .line 149
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->getRangeState()Lcom/squareup/timessquare/RangeState;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setRangeState(Lcom/squareup/timessquare/RangeState;)V

    .line 150
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->isHighlighted()Z

    move-result v4

    invoke-virtual {v10, v4}, Lcom/squareup/timessquare/CalendarCellView;->setHighlighted(Z)V

    .line 151
    invoke-virtual {v10, v15}, Lcom/squareup/timessquare/CalendarCellView;->setTag(Ljava/lang/Object;)V

    .line 153
    iget-object v4, v0, Lcom/squareup/timessquare/MonthView;->decorators:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 154
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/timessquare/CalendarCellDecorator;

    move-object/from16 v17, v3

    .line 155
    invoke-virtual {v15}, Lcom/squareup/timessquare/MonthCellDescriptor;->getDate()Ljava/util/Date;

    move-result-object v3

    invoke-interface {v5, v10, v3}, Lcom/squareup/timessquare/CalendarCellDecorator;->decorate(Lcom/squareup/timessquare/CalendarCellView;Ljava/util/Date;)V

    move-object/from16 v3, v17

    goto :goto_4

    :cond_3
    move-object/from16 v17, v3

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v3, v17

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x6

    goto/16 :goto_2

    :cond_4
    move-object/from16 v17, v3

    goto :goto_5

    :cond_5
    move-object/from16 v13, p2

    move-object/from16 v17, v3

    const/16 v3, 0x8

    .line 160
    invoke-virtual {v11, v3}, Lcom/squareup/timessquare/CalendarRowView;->setVisibility(I)V

    :goto_5
    move v9, v12

    move-object/from16 v3, v17

    const/4 v4, 0x1

    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_6
    if-eqz v1, :cond_7

    .line 165
    iget-object v3, v0, Lcom/squareup/timessquare/MonthView;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_7
    if-eqz v2, :cond_8

    .line 168
    iget-object v1, v0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v1, v2}, Lcom/squareup/timessquare/CalendarGridView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_8
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "MonthView.init took %d ms"

    invoke-static {v2, v1}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public setDayBackground(I)V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarGridView;->setDayBackground(I)V

    return-void
.end method

.method public setDayTextColor(I)V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarGridView;->setDayTextColor(I)V

    return-void
.end method

.method public setDayViewAdapter(Lcom/squareup/timessquare/DayViewAdapter;)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarGridView;->setDayViewAdapter(Lcom/squareup/timessquare/DayViewAdapter;)V

    return-void
.end method

.method public setDecorators(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/timessquare/CalendarCellDecorator;",
            ">;)V"
        }
    .end annotation

    .line 107
    iput-object p1, p0, Lcom/squareup/timessquare/MonthView;->decorators:Ljava/util/List;

    return-void
.end method

.method public setDisplayHeader(Z)V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarGridView;->setDisplayHeader(Z)V

    return-void
.end method

.method public setDividerColor(I)V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarGridView;->setDividerColor(I)V

    return-void
.end method

.method public setHeaderTextColor(I)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/timessquare/MonthView;->grid:Lcom/squareup/timessquare/CalendarGridView;

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarGridView;->setHeaderTextColor(I)V

    return-void
.end method
