.class Lcom/squareup/timessquare/CalendarPickerView$1;
.super Ljava/lang/Object;
.source "CalendarPickerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/timessquare/CalendarPickerView;->scrollToSelectedMonth(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/timessquare/CalendarPickerView;

.field final synthetic val$selectedIndex:I

.field final synthetic val$smoothScroll:Z


# direct methods
.method constructor <init>(Lcom/squareup/timessquare/CalendarPickerView;IZ)V
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iput p2, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->val$selectedIndex:I

    iput-boolean p3, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->val$smoothScroll:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 422
    iget v1, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->val$selectedIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Scrolling to position %d"

    invoke-static {v1, v0}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 424
    iget-boolean v0, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->val$smoothScroll:Z

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget v1, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->val$selectedIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/timessquare/CalendarPickerView;->smoothScrollToPosition(I)V

    goto :goto_0

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->this$0:Lcom/squareup/timessquare/CalendarPickerView;

    iget v1, p0, Lcom/squareup/timessquare/CalendarPickerView$1;->val$selectedIndex:I

    invoke-virtual {v0, v1}, Lcom/squareup/timessquare/CalendarPickerView;->setSelection(I)V

    :goto_0
    return-void
.end method
