.class Lcom/squareup/squarewave/gen2/Streak;
.super Ljava/util/ArrayList;
.source "Streak.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList<",
        "Lcom/squareup/squarewave/gen2/Peak;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final polarity:I


# direct methods
.method constructor <init>(Lcom/squareup/squarewave/gen2/Peak;)V
    .locals 1

    const/4 v0, 0x4

    .line 16
    invoke-direct {p0, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/gen2/Streak;->add(Ljava/lang/Object;)Z

    .line 18
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Peak;->polarity()I

    move-result p1

    iput p1, p0, Lcom/squareup/squarewave/gen2/Streak;->polarity:I

    return-void
.end method


# virtual methods
.method biggestPeak()Lcom/squareup/squarewave/gen2/Peak;
    .locals 5

    const/4 v0, 0x0

    .line 23
    invoke-virtual {p0, v0}, Lcom/squareup/squarewave/gen2/Streak;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/gen2/Peak;

    const/4 v1, 0x1

    .line 24
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/squarewave/gen2/Streak;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 25
    invoke-virtual {p0, v1}, Lcom/squareup/squarewave/gen2/Streak;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/squarewave/gen2/Peak;

    .line 26
    invoke-virtual {v2}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v3

    invoke-virtual {v0}, Lcom/squareup/squarewave/gen2/Peak;->absoluteAmplitude()S

    move-result v4

    if-le v3, v4, :cond_0

    move-object v0, v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method
