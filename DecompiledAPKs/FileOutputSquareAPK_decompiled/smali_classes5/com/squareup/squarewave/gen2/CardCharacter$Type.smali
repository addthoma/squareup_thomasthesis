.class public final Lcom/squareup/squarewave/gen2/CardCharacter$Type;
.super Ljava/lang/Object;
.source "CardCharacter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/gen2/CardCharacter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Type"
.end annotation


# static fields
.field public static final CONTROL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final DATA_ALPHA:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final END_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final SEPARATOR:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final SPECIAL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final START_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

.field public static final UNKNOWN:Lcom/squareup/squarewave/gen2/CardCharacter$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->START_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 13
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->END_SENTINEL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 14
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_NUMERIC:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 15
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->SEPARATOR:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 16
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->CONTROL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 17
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->SPECIAL:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 18
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->DATA_ALPHA:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    .line 19
    new-instance v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/CardCharacter$Type;-><init>()V

    sput-object v0, Lcom/squareup/squarewave/gen2/CardCharacter$Type;->UNKNOWN:Lcom/squareup/squarewave/gen2/CardCharacter$Type;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
