.class Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;
.super Ljava/lang/Object;
.source "EventDataForwarder.java"

# interfaces
.implements Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/EventDataForwarder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoOpCarrierDetectListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/squarewave/EventDataForwarder;


# direct methods
.method private constructor <init>(Lcom/squareup/squarewave/EventDataForwarder;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;->this$0:Lcom/squareup/squarewave/EventDataForwarder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/squarewave/EventDataForwarder$1;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/EventDataForwarder$NoOpCarrierDetectListener;-><init>(Lcom/squareup/squarewave/EventDataForwarder;)V

    return-void
.end method


# virtual methods
.method public onCarrierDetect(Lcom/squareup/squarewave/Signal;)V
    .locals 1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Ha, we got a signal from headset removal!"

    .line 48
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
