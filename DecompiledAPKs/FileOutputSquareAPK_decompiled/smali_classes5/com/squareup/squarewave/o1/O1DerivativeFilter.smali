.class public final Lcom/squareup/squarewave/o1/O1DerivativeFilter;
.super Ljava/lang/Object;
.source "O1DerivativeFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/o1/O1SwipeFilter;


# instance fields
.field private final next:Lcom/squareup/squarewave/o1/O1SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/o1/O1SwipeFilter;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1DerivativeFilter;->next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    return-void
.end method


# virtual methods
.method public filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;
    .locals 2

    .line 31
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->withDerivative()Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object p1

    .line 32
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1DerivativeFilter;->next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/o1/O1SwipeFilter;->filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v1, v0, Lcom/squareup/squarewave/o1/O1DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v1}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getDerivative()[S

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([SS)V

    :cond_0
    return-object v0
.end method
