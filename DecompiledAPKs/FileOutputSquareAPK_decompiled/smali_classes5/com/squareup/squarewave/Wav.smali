.class public Lcom/squareup/squarewave/Wav;
.super Ljava/lang/Object;
.source "Wav.java"


# static fields
.field public static final WAV_HEADER_LENGTH:I = 0x2c


# instance fields
.field private final file:Ljava/io/File;

.field private final path:Ljava/lang/String;

.field private final sampleRate:I

.field private final samples:[S


# direct methods
.method public constructor <init>(Ljava/io/File;[SI)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/squarewave/Wav;->file:Ljava/io/File;

    .line 22
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/Wav;->path:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/squareup/squarewave/Wav;->samples:[S

    .line 24
    iput p3, p0, Lcom/squareup/squarewave/Wav;->sampleRate:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[SI)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/squarewave/Wav;->path:Ljava/lang/String;

    const/4 p1, 0x0

    .line 29
    iput-object p1, p0, Lcom/squareup/squarewave/Wav;->file:Ljava/io/File;

    .line 30
    iput-object p2, p0, Lcom/squareup/squarewave/Wav;->samples:[S

    .line 31
    iput p3, p0, Lcom/squareup/squarewave/Wav;->sampleRate:I

    return-void
.end method

.method public static emptyWav()Lcom/squareup/squarewave/Wav;
    .locals 4

    .line 35
    new-instance v0, Lcom/squareup/squarewave/Wav;

    const/4 v1, 0x0

    new-array v1, v1, [S

    const-string v2, "empty.wav"

    const v3, 0xac44

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/squarewave/Wav;-><init>(Ljava/lang/String;[SI)V

    return-object v0
.end method

.method public static fromFile(Ljava/io/File;)Lcom/squareup/squarewave/Wav;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 40
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v1, v0

    const/16 v0, 0x2c

    if-lt v1, v0, :cond_0

    new-array v2, v0, [B

    sub-int/2addr v1, v0

    .line 46
    new-array v0, v1, [B

    .line 49
    array-length v1, v0

    shr-int/lit8 v1, v1, 0x1

    new-array v1, v1, [S

    .line 51
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "r"

    invoke-direct {v3, p0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 53
    :try_start_0
    invoke-virtual {v3, v2}, Ljava/io/RandomAccessFile;->readFully([B)V

    .line 54
    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->readFully([B)V

    const/16 v4, 0x18

    .line 56
    invoke-static {v2, v4}, Lcom/squareup/squarewave/LittleEndian;->readInt([BI)I

    move-result v2

    .line 57
    array-length v4, v0

    invoke-static {v0, v4, v1}, Lcom/squareup/squarewave/LittleEndian;->bytesToShorts([BI[S)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    .line 62
    new-instance v0, Lcom/squareup/squarewave/Wav;

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/squarewave/Wav;-><init>(Ljava/io/File;[SI)V

    return-object v0

    :catchall_0
    move-exception p0

    .line 59
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V

    .line 60
    throw p0

    .line 42
    :cond_0
    new-instance p0, Ljava/io/IOException;

    const-string v0, "WAV file is too short."

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static fromInputStream(Ljava/io/InputStream;Ljava/lang/String;)Lcom/squareup/squarewave/Wav;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 67
    invoke-static {p0}, Lcom/squareup/squarewave/Wav;->inputStreamToByteArray(Ljava/io/InputStream;)[B

    move-result-object p0

    .line 68
    array-length v0, p0

    const/16 v1, 0x2c

    if-lt v0, v1, :cond_0

    new-array v2, v1, [B

    sub-int/2addr v0, v1

    .line 74
    new-array v0, v0, [B

    .line 77
    array-length v1, v0

    shr-int/lit8 v1, v1, 0x1

    new-array v1, v1, [S

    .line 79
    array-length v3, v2

    const/4 v4, 0x0

    invoke-static {p0, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    array-length v3, v2

    array-length v5, v0

    invoke-static {p0, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 p0, 0x18

    .line 82
    invoke-static {v2, p0}, Lcom/squareup/squarewave/LittleEndian;->readInt([BI)I

    move-result p0

    .line 83
    array-length v2, v0

    invoke-static {v0, v2, v1}, Lcom/squareup/squarewave/LittleEndian;->bytesToShorts([BI[S)V

    .line 85
    new-instance v0, Lcom/squareup/squarewave/Wav;

    invoke-direct {v0, p1, v1, p0}, Lcom/squareup/squarewave/Wav;-><init>(Ljava/lang/String;[SI)V

    return-object v0

    .line 70
    :cond_0
    new-instance p0, Ljava/io/IOException;

    const-string p1, "WAV file is too short."

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static inputStreamToByteArray(Ljava/io/InputStream;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 90
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x4000

    new-array v1, v1, [B

    .line 95
    :goto_0
    array-length v2, v1

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 96
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 101
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 102
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    .line 103
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    return-object p0
.end method


# virtual methods
.method public getFile()Ljava/io/File;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/squarewave/Wav;->file:Ljava/io/File;

    return-object v0
.end method

.method public sampleRate()I
    .locals 1

    .line 117
    iget v0, p0, Lcom/squareup/squarewave/Wav;->sampleRate:I

    return v0
.end method

.method public samples()[S
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/squarewave/Wav;->samples:[S

    return-object v0
.end method

.method public samplesAsBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/squarewave/Wav;->samples()[S

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/squarewave/gum/Buffers;->asBuffer([S)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method
