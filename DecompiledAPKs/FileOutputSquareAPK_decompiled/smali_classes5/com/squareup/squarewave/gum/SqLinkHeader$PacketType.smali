.class public final enum Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;
.super Ljava/lang/Enum;
.source "SqLinkHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/gum/SqLinkHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PacketType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

.field public static final enum AWAKE:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

.field public static final enum BLANK:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

.field public static final enum CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

.field public static final enum DEAD:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

.field private static final byByte:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;


# instance fields
.field private final byteValue:B

.field private final protoPacketType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 16
    new-instance v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_AWAKE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const-string v4, "AWAKE"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;-><init>(Ljava/lang/String;ILjava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)V

    sput-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->AWAKE:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    .line 17
    new-instance v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_CARD_DATA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const-string v5, "CARD_DATA"

    invoke-direct {v0, v5, v2, v3, v4}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;-><init>(Ljava/lang/String;ILjava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)V

    sput-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    .line 18
    new-instance v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_DEAD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const/4 v6, 0x2

    const-string v7, "DEAD"

    invoke-direct {v0, v7, v6, v4, v5}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;-><init>(Ljava/lang/String;ILjava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)V

    sput-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->DEAD:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    .line 19
    new-instance v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v7, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->R4_BLANK:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    const-string v8, "BLANK"

    invoke-direct {v0, v8, v3, v5, v7}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;-><init>(Ljava/lang/String;ILjava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)V

    sput-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->BLANK:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    new-array v0, v4, [Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    .line 15
    sget-object v4, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->AWAKE:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    aput-object v4, v0, v1

    sget-object v4, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->CARD_DATA:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    aput-object v4, v0, v2

    sget-object v2, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->DEAD:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    aput-object v2, v0, v6

    sget-object v2, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->BLANK:Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    aput-object v2, v0, v3

    sput-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->$VALUES:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    .line 21
    sput-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->byByte:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    .line 24
    invoke-static {}, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->values()[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 25
    sget-object v4, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->byByte:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    iget-byte v5, v3, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->byteValue:B

    aput-object v3, v4, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;",
            ")V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    invoke-virtual {p3}, Ljava/lang/Integer;->byteValue()B

    move-result p1

    iput-byte p1, p0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->byteValue:B

    .line 38
    iput-object p4, p0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->protoPacketType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-void
.end method

.method static fromByte(B)Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->byByte:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->$VALUES:[Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    invoke-virtual {v0}, [Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;

    return-object v0
.end method


# virtual methods
.method public asProtoPacketType()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/squarewave/gum/SqLinkHeader$PacketType;->protoPacketType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    return-object v0
.end method
