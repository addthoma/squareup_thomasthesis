.class public final Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;
.super Ljava/lang/Object;
.source "SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/logging/SwipeEventLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    return-void
.end method

.method public static create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)V

    return-object v0
.end method

.method public static provideSwipeEventLogger(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/logging/SwipeEventLogger;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->provideSwipeEventLogger()Lcom/squareup/logging/SwipeEventLogger;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/logging/SwipeEventLogger;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/logging/SwipeEventLogger;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    invoke-static {v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;->provideSwipeEventLogger(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/logging/SwipeEventLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSwipeEventLoggerFactory;->get()Lcom/squareup/logging/SwipeEventLogger;

    move-result-object v0

    return-object v0
.end method
