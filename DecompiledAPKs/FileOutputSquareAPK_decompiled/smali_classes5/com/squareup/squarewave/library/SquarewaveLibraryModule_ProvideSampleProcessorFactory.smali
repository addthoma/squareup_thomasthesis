.class public final Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;
.super Ljava/lang/Object;
.source "SquarewaveLibraryModule_ProvideSampleProcessorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/gum/SampleProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

.field private final processorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/MessengerSampleProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/MessengerSampleProcessor;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    .line 26
    iput-object p2, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->processorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/MessengerSampleProcessor;",
            ">;)",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSampleProcessor(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/gum/MessengerSampleProcessor;)Lcom/squareup/squarewave/gum/SampleProcessor;
    .locals 0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->provideSampleProcessor(Lcom/squareup/squarewave/gum/MessengerSampleProcessor;)Lcom/squareup/squarewave/gum/SampleProcessor;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/gum/SampleProcessor;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/gum/SampleProcessor;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    iget-object v1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->processorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/squarewave/gum/MessengerSampleProcessor;

    invoke-static {v0, v1}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->provideSampleProcessor(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/gum/MessengerSampleProcessor;)Lcom/squareup/squarewave/gum/SampleProcessor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideSampleProcessorFactory;->get()Lcom/squareup/squarewave/gum/SampleProcessor;

    move-result-object v0

    return-object v0
.end method
