.class public interface abstract Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;
.super Ljava/lang/Object;
.source "SquarewaveLibraryComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentComponent"
.end annotation


# virtual methods
.method public abstract application()Landroid/app/Application;
.end method

.method public abstract mainThread()Lcom/squareup/thread/executor/MainThread;
.end method

.method public abstract swipeBus()Lcom/squareup/wavpool/swipe/SwipeBus;
.end method

.method public abstract telephonyManager()Landroid/telephony/TelephonyManager;
.end method
