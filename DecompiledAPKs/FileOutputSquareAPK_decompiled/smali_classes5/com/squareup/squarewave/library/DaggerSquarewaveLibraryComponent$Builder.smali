.class public final Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;
.super Ljava/lang/Object;
.source "DaggerSquarewaveLibraryComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

.field private squarewaveLibraryModule:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$1;)V
    .locals 0

    .line 158
    invoke-direct {p0}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/squarewave/library/SquarewaveLibraryComponent;
    .locals 4

    .line 177
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;->squarewaveLibraryModule:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    const-class v1, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    const-class v1, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 179
    new-instance v0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;

    iget-object v1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;->squarewaveLibraryModule:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    iget-object v2, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$1;)V

    return-object v0
.end method

.method public parentComponent(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;
    .locals 0

    .line 172
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    return-object p0
.end method

.method public squarewaveLibraryModule(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;
    .locals 0

    .line 167
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$Builder;->squarewaveLibraryModule:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    return-object p0
.end method
