.class public final Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;
.super Ljava/lang/Object;
.source "SquarewaveLibrary_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/library/SquarewaveLibrary;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventDataListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final recorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataListener;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;->recorderProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;->eventDataListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Recorder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataListener;",
            ">;)",
            "Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/wavpool/swipe/Recorder;Lcom/squareup/squarewave/EventDataListener;)Lcom/squareup/squarewave/library/SquarewaveLibrary;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibrary;

    invoke-direct {v0, p0, p1}, Lcom/squareup/squarewave/library/SquarewaveLibrary;-><init>(Lcom/squareup/wavpool/swipe/Recorder;Lcom/squareup/squarewave/EventDataListener;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/library/SquarewaveLibrary;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;->recorderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/Recorder;

    iget-object v1, p0, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;->eventDataListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/squarewave/EventDataListener;

    invoke-static {v0, v1}, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;->newInstance(Lcom/squareup/wavpool/swipe/Recorder;Lcom/squareup/squarewave/EventDataListener;)Lcom/squareup/squarewave/library/SquarewaveLibrary;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibrary_Factory;->get()Lcom/squareup/squarewave/library/SquarewaveLibrary;

    move-result-object v0

    return-object v0
.end method
