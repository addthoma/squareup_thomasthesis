.class Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;
.super Ljava/lang/Object;
.source "DaggerSquarewaveLibraryComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Lcom/squareup/wavpool/swipe/SwipeBus;",
        ">;"
    }
.end annotation


# instance fields
.field private final parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;


# direct methods
.method constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V
    .locals 0

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    iput-object p1, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/SwipeBus;
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;->parentComponent:Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    invoke-interface {v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;->swipeBus()Lcom/squareup/wavpool/swipe/SwipeBus;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable component method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/SwipeBus;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 211
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/DaggerSquarewaveLibraryComponent$com_squareup_squarewave_library_SquarewaveLibraryComponent_ParentComponent_swipeBus;->get()Lcom/squareup/wavpool/swipe/SwipeBus;

    move-result-object v0

    return-object v0
.end method
