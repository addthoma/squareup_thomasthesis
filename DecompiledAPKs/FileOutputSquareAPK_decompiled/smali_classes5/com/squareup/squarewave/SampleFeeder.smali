.class public Lcom/squareup/squarewave/SampleFeeder;
.super Ljava/lang/Object;
.source "SampleFeeder.java"

# interfaces
.implements Lcom/squareup/squarewave/AudioFilter;


# static fields
.field public static final SAMPLE_RATE:I = 0xac44


# instance fields
.field private final sampleProcessor:Lcom/squareup/squarewave/gum/SampleProcessor;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gum/SampleProcessor;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/squarewave/SampleFeeder;->sampleProcessor:Lcom/squareup/squarewave/gum/SampleProcessor;

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 0

    return-void
.end method

.method public process(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .line 19
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 20
    iget-object v0, p0, Lcom/squareup/squarewave/SampleFeeder;->sampleProcessor:Lcom/squareup/squarewave/gum/SampleProcessor;

    invoke-interface {v0, p1, p2}, Lcom/squareup/squarewave/gum/SampleProcessor;->feedSamples(Ljava/nio/ByteBuffer;I)V

    .line 21
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    return-void
.end method

.method public start(I)V
    .locals 0

    return-void
.end method
