.class public final Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;
.super Ljava/lang/Object;
.source "SposReceiptAutoCloseProvider.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;",
        "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
        "apiTransactionState",
        "Lcom/squareup/api/ApiTransactionState;",
        "(Lcom/squareup/api/ApiTransactionState;)V",
        "getReceiptAutoCloseOverride",
        "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;


# direct methods
.method public constructor <init>(Lcom/squareup/api/ApiTransactionState;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "apiTransactionState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    return-void
.end method


# virtual methods
.method public getReceiptAutoCloseOverride()Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;
    .locals 3

    .line 19
    iget-object v0, p0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->hasReceiptScreenTimeout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;

    iget-object v1, p0, Lcom/squareup/spos/receipts/SposReceiptAutoCloseProvider;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionState;->getReceiptScreenTimeout()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;-><init>(J)V

    check-cast v0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;

    goto :goto_0

    .line 22
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$NoOverride;->INSTANCE:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$NoOverride;

    check-cast v0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;

    :goto_0
    return-object v0
.end method

.method public hasReceiptAutoCloseOverride()Z
    .locals 1

    .line 14
    invoke-static {p0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider$DefaultImpls;->hasReceiptAutoCloseOverride(Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;)Z

    move-result v0

    return v0
.end method
