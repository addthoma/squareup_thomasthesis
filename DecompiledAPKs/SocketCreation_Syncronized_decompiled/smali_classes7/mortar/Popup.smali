.class public interface abstract Lmortar/Popup;
.super Ljava/lang/Object;
.source "Popup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Landroid/os/Parcelable;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract dismiss(Z)V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract isShowing()Z
.end method

.method public abstract show(Landroid/os/Parcelable;ZLmortar/PopupPresenter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;Z",
            "Lmortar/PopupPresenter<",
            "TD;TR;>;)V"
        }
    .end annotation
.end method
