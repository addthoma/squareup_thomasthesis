.class public final Lleakcanary/internal/AppWatcherInstaller$LeakCanaryProcess;
.super Lleakcanary/internal/AppWatcherInstaller;
.source "AppWatcherInstaller.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/internal/AppWatcherInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeakCanaryProcess"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lleakcanary/internal/AppWatcherInstaller$LeakCanaryProcess;",
        "Lleakcanary/internal/AppWatcherInstaller;",
        "()V",
        "onCreate",
        "",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, v0}, Lleakcanary/internal/AppWatcherInstaller;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public onCreate()Z
    .locals 10

    .line 27
    invoke-super {p0}, Lleakcanary/internal/AppWatcherInstaller;->onCreate()Z

    .line 28
    invoke-static {}, Lleakcanary/AppWatcher;->getConfig()Lleakcanary/AppWatcher$Config;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Lleakcanary/AppWatcher$Config;->copy$default(Lleakcanary/AppWatcher$Config;ZZZZZJILjava/lang/Object;)Lleakcanary/AppWatcher$Config;

    move-result-object v0

    invoke-static {v0}, Lleakcanary/AppWatcher;->setConfig(Lleakcanary/AppWatcher$Config;)V

    const/4 v0, 0x1

    return v0
.end method
