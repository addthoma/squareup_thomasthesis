.class public final Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;
.super Ljava/lang/Object;
.source "ActivityDestroyWatcher.kt"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lleakcanary/internal/ActivityDestroyWatcher;-><init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nActivityDestroyWatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ActivityDestroyWatcher.kt\nleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1\n+ 2 InternalAppWatcher.kt\nleakcanary/internal/InternalAppWatcher\n*L\n1#1,52:1\n80#2,6:53\n*E\n*S KotlinDebug\n*F\n+ 1 ActivityDestroyWatcher.kt\nleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1\n*L\n30#1,6:53\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J-\u0010\u0002\u001a\u00020\u00032\u0010\u0008\u0001\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00052\u0010\u0008\u0001\u0010\u0007\u001a\n \u0006*\u0004\u0018\u00010\u00080\u0008H\u0096\u0001J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0005H\u0016J\u001b\u0010\u000b\u001a\u00020\u00032\u0010\u0008\u0001\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\u0096\u0001J\u001b\u0010\u000c\u001a\u00020\u00032\u0010\u0008\u0001\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\u0096\u0001J-\u0010\r\u001a\u00020\u00032\u0010\u0008\u0001\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00052\u0010\u0008\u0001\u0010\u0007\u001a\n \u0006*\u0004\u0018\u00010\u00080\u0008H\u0096\u0001J\u001b\u0010\u000e\u001a\u00020\u00032\u0010\u0008\u0001\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\u0096\u0001J\u001b\u0010\u000f\u001a\u00020\u00032\u0010\u0008\u0001\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\u0096\u0001\u00a8\u0006\u0010"
    }
    d2 = {
        "leakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1",
        "Landroid/app/Application$ActivityLifecycleCallbacks;",
        "onActivityCreated",
        "",
        "p0",
        "Landroid/app/Activity;",
        "kotlin.jvm.PlatformType",
        "p1",
        "Landroid/os/Bundle;",
        "onActivityDestroyed",
        "activity",
        "onActivityPaused",
        "onActivityResumed",
        "onActivitySaveInstanceState",
        "onActivityStarted",
        "onActivityStopped",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

.field final synthetic this$0:Lleakcanary/internal/ActivityDestroyWatcher;


# direct methods
.method constructor <init>(Lleakcanary/internal/ActivityDestroyWatcher;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->this$0:Lleakcanary/internal/ActivityDestroyWatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object p1, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    .line 53
    const-class p1, Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 54
    sget-object v0, Lleakcanary/internal/InternalAppWatcher$noOpDelegate$noOpHandler$1;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$noOpDelegate$noOpHandler$1;

    check-cast v0, Ljava/lang/reflect/InvocationHandler;

    .line 58
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 57
    invoke-static {v1, v2, v0}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/app/Application$ActivityLifecycleCallbacks;

    iput-object p1, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.app.Application.ActivityLifecycleCallbacks"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-interface {v0, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->this$0:Lleakcanary/internal/ActivityDestroyWatcher;

    invoke-static {v0}, Lleakcanary/internal/ActivityDestroyWatcher;->access$getConfigProvider$p(Lleakcanary/internal/ActivityDestroyWatcher;)Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lleakcanary/AppWatcher$Config;

    invoke-virtual {v0}, Lleakcanary/AppWatcher$Config;->getWatchActivities()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->this$0:Lleakcanary/internal/ActivityDestroyWatcher;

    invoke-static {v0}, Lleakcanary/internal/ActivityDestroyWatcher;->access$getObjectWatcher$p(Lleakcanary/internal/ActivityDestroyWatcher;)Lleakcanary/ObjectWatcher;

    move-result-object v0

    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " received Activity#onDestroy() callback"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 33
    invoke-virtual {v0, p1, v1}, Lleakcanary/ObjectWatcher;->watch(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-interface {v0, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPaused(Landroid/app/Activity;)V

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-interface {v0, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityResumed(Landroid/app/Activity;)V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-interface {v0, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-interface {v0, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStarted(Landroid/app/Activity;)V

    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;->$$delegate_0:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-interface {v0, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStopped(Landroid/app/Activity;)V

    return-void
.end method
