.class public final Lleakcanary/AppWatcher$Config;
.super Ljava/lang/Object;
.source "AppWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/AppWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lleakcanary/AppWatcher$Config$Builder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\"BA\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\tH\u00c6\u0003JE\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\u0008\u0010\u001e\u001a\u00020\u001fH\u0007J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006#"
    }
    d2 = {
        "Lleakcanary/AppWatcher$Config;",
        "",
        "enabled",
        "",
        "watchActivities",
        "watchFragments",
        "watchFragmentViews",
        "watchViewModels",
        "watchDurationMillis",
        "",
        "(ZZZZZJ)V",
        "getEnabled",
        "()Z",
        "getWatchActivities",
        "getWatchDurationMillis",
        "()J",
        "getWatchFragmentViews",
        "getWatchFragments",
        "getWatchViewModels",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "newBuilder",
        "Lleakcanary/AppWatcher$Config$Builder;",
        "toString",
        "",
        "Builder",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enabled:Z

.field private final watchActivities:Z

.field private final watchDurationMillis:J

.field private final watchFragmentViews:Z

.field private final watchFragments:Z

.field private final watchViewModels:Z


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/16 v8, 0x3f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lleakcanary/AppWatcher$Config;-><init>(ZZZZZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZZZZJ)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    iput-boolean p2, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    iput-boolean p3, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    iput-boolean p4, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    iput-boolean p5, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    iput-wide p6, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    return-void
.end method

.method public synthetic constructor <init>(ZZZZZJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    .line 28
    sget-object p1, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    invoke-virtual {p1}, Lleakcanary/internal/InternalAppWatcher;->isDebuggableBuild()Z

    move-result p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    const/4 v0, 0x1

    if-eqz p9, :cond_1

    const/4 p9, 0x1

    goto :goto_0

    :cond_1
    move p9, p2

    :goto_0
    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    move v1, p3

    :goto_1
    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    move v2, p4

    :goto_2
    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    goto :goto_3

    :cond_4
    move v0, p5

    :goto_3
    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    .line 64
    sget-object p2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p3, 0x5

    invoke-virtual {p2, p3, p4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p6

    :cond_5
    move-wide v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v1

    move p6, v2

    move p7, v0

    move-wide p8, v3

    invoke-direct/range {p2 .. p9}, Lleakcanary/AppWatcher$Config;-><init>(ZZZZZJ)V

    return-void
.end method

.method public static synthetic copy$default(Lleakcanary/AppWatcher$Config;ZZZZZJILjava/lang/Object;)Lleakcanary/AppWatcher$Config;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-wide p6, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    :cond_5
    move-wide v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move-wide p8, v3

    invoke-virtual/range {p2 .. p9}, Lleakcanary/AppWatcher$Config;->copy(ZZZZZJ)Lleakcanary/AppWatcher$Config;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    return v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    return-wide v0
.end method

.method public final copy(ZZZZZJ)Lleakcanary/AppWatcher$Config;
    .locals 9

    new-instance v8, Lleakcanary/AppWatcher$Config;

    move-object v0, v8

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-wide v6, p6

    invoke-direct/range {v0 .. v7}, Lleakcanary/AppWatcher$Config;-><init>(ZZZZZJ)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lleakcanary/AppWatcher$Config;

    if-eqz v0, :cond_0

    check-cast p1, Lleakcanary/AppWatcher$Config;

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    iget-boolean v1, p1, Lleakcanary/AppWatcher$Config;->enabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    iget-boolean v1, p1, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    iget-boolean v1, p1, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    iget-boolean v1, p1, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    iget-boolean v1, p1, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    iget-wide v2, p1, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEnabled()Z
    .locals 1

    .line 28
    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    return v0
.end method

.method public final getWatchActivities()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    return v0
.end method

.method public final getWatchDurationMillis()J
    .locals 2

    .line 64
    iget-wide v0, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    return-wide v0
.end method

.method public final getWatchFragmentViews()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    return v0
.end method

.method public final getWatchFragments()Z
    .locals 1

    .line 42
    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    return v0
.end method

.method public final getWatchViewModels()Z
    .locals 1

    .line 57
    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    iget-boolean v0, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    if-eqz v2, :cond_4

    goto :goto_0

    :cond_4
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    const/16 v3, 0x20

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v2, v1

    add-int/2addr v0, v2

    return v0
.end method

.method public final newBuilder()Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 74
    new-instance v0, Lleakcanary/AppWatcher$Config$Builder;

    invoke-direct {v0, p0}, Lleakcanary/AppWatcher$Config$Builder;-><init>(Lleakcanary/AppWatcher$Config;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Config(enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lleakcanary/AppWatcher$Config;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", watchActivities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lleakcanary/AppWatcher$Config;->watchActivities:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", watchFragments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lleakcanary/AppWatcher$Config;->watchFragments:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", watchFragmentViews="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lleakcanary/AppWatcher$Config;->watchFragmentViews:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", watchViewModels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lleakcanary/AppWatcher$Config;->watchViewModels:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", watchDurationMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lleakcanary/AppWatcher$Config;->watchDurationMillis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
