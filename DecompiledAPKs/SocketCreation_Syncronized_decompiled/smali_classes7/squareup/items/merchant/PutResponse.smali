.class public final Lsquareup/items/merchant/PutResponse;
.super Lcom/squareup/wire/Message;
.source "PutResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/PutResponse$ProtoAdapter_PutResponse;,
        Lsquareup/items/merchant/PutResponse$Status;,
        Lsquareup/items/merchant/PutResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/PutResponse;",
        "Lsquareup/items/merchant/PutResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/PutResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LOCK_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MODIFICATION_TIMESTAMP:Ljava/lang/Long;

.field public static final DEFAULT_STATUS:Lsquareup/items/merchant/PutResponse$Status;

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_definition:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.AttributeDefinition#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final catalog_object:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CatalogObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public final invalid_catalog_object:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.InvalidCatalogObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/InvalidCatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public final lock_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final modification_timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final status:Lsquareup/items/merchant/PutResponse$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.PutResponse$Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lsquareup/items/merchant/PutResponse$ProtoAdapter_PutResponse;

    invoke-direct {v0}, Lsquareup/items/merchant/PutResponse$ProtoAdapter_PutResponse;-><init>()V

    sput-object v0, Lsquareup/items/merchant/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lsquareup/items/merchant/PutResponse$Status;->DO_NOT_USE:Lsquareup/items/merchant/PutResponse$Status;

    sput-object v0, Lsquareup/items/merchant/PutResponse;->DEFAULT_STATUS:Lsquareup/items/merchant/PutResponse$Status;

    const-wide/16 v0, 0x0

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/PutResponse;->DEFAULT_MODIFICATION_TIMESTAMP:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/PutResponse$Status;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/PutResponse$Status;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/InvalidCatalogObject;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;)V"
        }
    .end annotation

    .line 89
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lsquareup/items/merchant/PutResponse;-><init>(Lsquareup/items/merchant/PutResponse$Status;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/PutResponse$Status;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/PutResponse$Status;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/InvalidCatalogObject;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 95
    sget-object v0, Lsquareup/items/merchant/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    const-string p1, "catalog_object"

    .line 97
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    const-string p1, "invalid_catalog_object"

    .line 98
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    .line 99
    iput-object p4, p0, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    .line 100
    iput-object p5, p0, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    const-string p1, "attribute_definition"

    .line 101
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 120
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/PutResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 121
    :cond_1
    check-cast p1, Lsquareup/items/merchant/PutResponse;

    .line 122
    invoke-virtual {p0}, Lsquareup/items/merchant/PutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/PutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    iget-object v3, p1, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    .line 124
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    .line 125
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    iget-object v3, p1, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    iget-object p1, p1, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    .line 128
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 133
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 135
    invoke-virtual {p0}, Lsquareup/items/merchant/PutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/items/merchant/PutResponse$Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lsquareup/items/merchant/PutResponse;->newBuilder()Lsquareup/items/merchant/PutResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/PutResponse$Builder;
    .locals 2

    .line 106
    new-instance v0, Lsquareup/items/merchant/PutResponse$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/PutResponse$Builder;-><init>()V

    .line 107
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    iput-object v1, v0, Lsquareup/items/merchant/PutResponse$Builder;->status:Lsquareup/items/merchant/PutResponse$Status;

    .line 108
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/PutResponse$Builder;->catalog_object:Ljava/util/List;

    .line 109
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/PutResponse$Builder;->invalid_catalog_object:Ljava/util/List;

    .line 110
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/PutResponse$Builder;->modification_timestamp:Ljava/lang/Long;

    .line 111
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/PutResponse$Builder;->lock_token:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/PutResponse$Builder;->attribute_definition:Ljava/util/List;

    .line 113
    invoke-virtual {p0}, Lsquareup/items/merchant/PutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/PutResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->status:Lsquareup/items/merchant/PutResponse$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", catalog_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->catalog_object:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", invalid_catalog_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->invalid_catalog_object:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 153
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", modification_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->modification_timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", lock_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->lock_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_4
    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", attribute_definition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse;->attribute_definition:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PutResponse{"

    .line 156
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
