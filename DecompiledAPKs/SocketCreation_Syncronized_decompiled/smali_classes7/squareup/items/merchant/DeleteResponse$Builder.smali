.class public final Lsquareup/items/merchant/DeleteResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/DeleteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/DeleteResponse;",
        "Lsquareup/items/merchant/DeleteResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public count:Ljava/lang/Long;

.field public deleted_objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
            ">;"
        }
    .end annotation
.end field

.field public lock_token:Ljava/lang/String;

.field public modification_timestamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 141
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->deleted_objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse$Builder;->build()Lsquareup/items/merchant/DeleteResponse;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/DeleteResponse;
    .locals 7

    .line 180
    new-instance v6, Lsquareup/items/merchant/DeleteResponse;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->count:Ljava/lang/Long;

    iget-object v2, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->modification_timestamp:Ljava/lang/Long;

    iget-object v3, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->deleted_objects:Ljava/util/List;

    iget-object v4, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->lock_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lsquareup/items/merchant/DeleteResponse;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public count(Ljava/lang/Long;)Lsquareup/items/merchant/DeleteResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->count:Ljava/lang/Long;

    return-object p0
.end method

.method public deleted_objects(Ljava/util/List;)Lsquareup/items/merchant/DeleteResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
            ">;)",
            "Lsquareup/items/merchant/DeleteResponse$Builder;"
        }
    .end annotation

    .line 164
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 165
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->deleted_objects:Ljava/util/List;

    return-object p0
.end method

.method public lock_token(Ljava/lang/String;)Lsquareup/items/merchant/DeleteResponse$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->lock_token:Ljava/lang/String;

    return-object p0
.end method

.method public modification_timestamp(Ljava/lang/Long;)Lsquareup/items/merchant/DeleteResponse$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$Builder;->modification_timestamp:Ljava/lang/Long;

    return-object p0
.end method
