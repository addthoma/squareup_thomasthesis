.class public final Lsquareup/items/merchant/PutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/PutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/PutRequest;",
        "Lsquareup/items/merchant/PutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public idempotency_key:Ljava/lang/String;

.field public lock_duration_ms:Ljava/lang/Long;

.field public lock_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public open_transaction:Ljava/lang/Boolean;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 222
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 223
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lsquareup/items/merchant/PutRequest$Builder;->build()Lsquareup/items/merchant/PutRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/PutRequest;
    .locals 10

    .line 289
    new-instance v9, Lsquareup/items/merchant/PutRequest;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object:Ljava/util/List;

    iget-object v3, p0, Lsquareup/items/merchant/PutRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v4, p0, Lsquareup/items/merchant/PutRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v5, p0, Lsquareup/items/merchant/PutRequest$Builder;->open_transaction:Ljava/lang/Boolean;

    iget-object v6, p0, Lsquareup/items/merchant/PutRequest$Builder;->lock_token:Ljava/lang/String;

    iget-object v7, p0, Lsquareup/items/merchant/PutRequest$Builder;->lock_duration_ms:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lsquareup/items/merchant/PutRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v9
.end method

.method public catalog_object(Ljava/util/List;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;)",
            "Lsquareup/items/merchant/PutRequest$Builder;"
        }
    .end annotation

    .line 232
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 233
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object:Ljava/util/List;

    return-object p0
.end method

.method public idempotency_key(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method

.method public lock_duration_ms(Ljava/lang/Long;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0

    .line 283
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->lock_duration_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public lock_token(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->lock_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public open_transaction(Ljava/lang/Boolean;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->open_transaction:Ljava/lang/Boolean;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
