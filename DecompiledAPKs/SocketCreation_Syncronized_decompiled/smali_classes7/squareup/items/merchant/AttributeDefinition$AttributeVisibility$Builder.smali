.class public final Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;",
        "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public object_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 513
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 514
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->object_types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 510
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->build()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
    .locals 3

    .line 525
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->object_types:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public object_types(Ljava/util/List;)Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;)",
            "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;"
        }
    .end annotation

    .line 518
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 519
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->object_types:Ljava/util/List;

    return-object p0
.end method
