.class public final enum Lsquareup/items/merchant/Query$Type;
.super Ljava/lang/Enum;
.source "Query.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/Query$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/Query$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATTRIBUTE_VALUES:Lsquareup/items/merchant/Query$Type;

.field public static final enum DO_NOT_USE:Lsquareup/items/merchant/Query$Type;

.field public static final enum EXACT:Lsquareup/items/merchant/Query$Type;

.field public static final enum LEGACY_ID:Lsquareup/items/merchant/Query$Type;

.field public static final enum NEIGHBOR:Lsquareup/items/merchant/Query$Type;

.field public static final enum PREFIX:Lsquareup/items/merchant/Query$Type;

.field public static final enum RANGE:Lsquareup/items/merchant/Query$Type;

.field public static final enum SET:Lsquareup/items/merchant/Query$Type;

.field public static final enum SORTED_ATTRIBUTE:Lsquareup/items/merchant/Query$Type;

.field public static final enum TEXT:Lsquareup/items/merchant/Query$Type;

.field public static final enum TOKEN:Lsquareup/items/merchant/Query$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 317
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->DO_NOT_USE:Lsquareup/items/merchant/Query$Type;

    .line 322
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v2, 0x1

    const-string v3, "PREFIX"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->PREFIX:Lsquareup/items/merchant/Query$Type;

    .line 327
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v3, 0x2

    const-string v4, "EXACT"

    invoke-direct {v0, v4, v3, v3}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->EXACT:Lsquareup/items/merchant/Query$Type;

    .line 332
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v4, 0x3

    const-string v5, "RANGE"

    invoke-direct {v0, v5, v4, v4}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->RANGE:Lsquareup/items/merchant/Query$Type;

    .line 337
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v5, 0x4

    const-string v6, "TOKEN"

    invoke-direct {v0, v6, v5, v5}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->TOKEN:Lsquareup/items/merchant/Query$Type;

    .line 343
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v6, 0x5

    const-string v7, "TEXT"

    invoke-direct {v0, v7, v6, v6}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->TEXT:Lsquareup/items/merchant/Query$Type;

    .line 348
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v7, 0x6

    const-string v8, "LEGACY_ID"

    invoke-direct {v0, v8, v7, v7}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->LEGACY_ID:Lsquareup/items/merchant/Query$Type;

    .line 354
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/4 v8, 0x7

    const-string v9, "NEIGHBOR"

    invoke-direct {v0, v9, v8, v8}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->NEIGHBOR:Lsquareup/items/merchant/Query$Type;

    .line 361
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/16 v9, 0x8

    const-string v10, "SORTED_ATTRIBUTE"

    invoke-direct {v0, v10, v9, v9}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->SORTED_ATTRIBUTE:Lsquareup/items/merchant/Query$Type;

    .line 367
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/16 v10, 0x9

    const-string v11, "SET"

    invoke-direct {v0, v11, v10, v10}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->SET:Lsquareup/items/merchant/Query$Type;

    .line 374
    new-instance v0, Lsquareup/items/merchant/Query$Type;

    const/16 v11, 0xa

    const-string v12, "ATTRIBUTE_VALUES"

    invoke-direct {v0, v12, v11, v11}, Lsquareup/items/merchant/Query$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->ATTRIBUTE_VALUES:Lsquareup/items/merchant/Query$Type;

    const/16 v0, 0xb

    new-array v0, v0, [Lsquareup/items/merchant/Query$Type;

    .line 316
    sget-object v12, Lsquareup/items/merchant/Query$Type;->DO_NOT_USE:Lsquareup/items/merchant/Query$Type;

    aput-object v12, v0, v1

    sget-object v1, Lsquareup/items/merchant/Query$Type;->PREFIX:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/Query$Type;->EXACT:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/items/merchant/Query$Type;->RANGE:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v4

    sget-object v1, Lsquareup/items/merchant/Query$Type;->TOKEN:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v5

    sget-object v1, Lsquareup/items/merchant/Query$Type;->TEXT:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v6

    sget-object v1, Lsquareup/items/merchant/Query$Type;->LEGACY_ID:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v7

    sget-object v1, Lsquareup/items/merchant/Query$Type;->NEIGHBOR:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v8

    sget-object v1, Lsquareup/items/merchant/Query$Type;->SORTED_ATTRIBUTE:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v9

    sget-object v1, Lsquareup/items/merchant/Query$Type;->SET:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v10

    sget-object v1, Lsquareup/items/merchant/Query$Type;->ATTRIBUTE_VALUES:Lsquareup/items/merchant/Query$Type;

    aput-object v1, v0, v11

    sput-object v0, Lsquareup/items/merchant/Query$Type;->$VALUES:[Lsquareup/items/merchant/Query$Type;

    .line 376
    new-instance v0, Lsquareup/items/merchant/Query$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 381
    iput p3, p0, Lsquareup/items/merchant/Query$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/Query$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 399
    :pswitch_0
    sget-object p0, Lsquareup/items/merchant/Query$Type;->ATTRIBUTE_VALUES:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 398
    :pswitch_1
    sget-object p0, Lsquareup/items/merchant/Query$Type;->SET:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 397
    :pswitch_2
    sget-object p0, Lsquareup/items/merchant/Query$Type;->SORTED_ATTRIBUTE:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 396
    :pswitch_3
    sget-object p0, Lsquareup/items/merchant/Query$Type;->NEIGHBOR:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 395
    :pswitch_4
    sget-object p0, Lsquareup/items/merchant/Query$Type;->LEGACY_ID:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 394
    :pswitch_5
    sget-object p0, Lsquareup/items/merchant/Query$Type;->TEXT:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 393
    :pswitch_6
    sget-object p0, Lsquareup/items/merchant/Query$Type;->TOKEN:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 392
    :pswitch_7
    sget-object p0, Lsquareup/items/merchant/Query$Type;->RANGE:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 391
    :pswitch_8
    sget-object p0, Lsquareup/items/merchant/Query$Type;->EXACT:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 390
    :pswitch_9
    sget-object p0, Lsquareup/items/merchant/Query$Type;->PREFIX:Lsquareup/items/merchant/Query$Type;

    return-object p0

    .line 389
    :pswitch_a
    sget-object p0, Lsquareup/items/merchant/Query$Type;->DO_NOT_USE:Lsquareup/items/merchant/Query$Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/Query$Type;
    .locals 1

    .line 316
    const-class v0, Lsquareup/items/merchant/Query$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/Query$Type;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/Query$Type;
    .locals 1

    .line 316
    sget-object v0, Lsquareup/items/merchant/Query$Type;->$VALUES:[Lsquareup/items/merchant/Query$Type;

    invoke-virtual {v0}, [Lsquareup/items/merchant/Query$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/Query$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 406
    iget v0, p0, Lsquareup/items/merchant/Query$Type;->value:I

    return v0
.end method
