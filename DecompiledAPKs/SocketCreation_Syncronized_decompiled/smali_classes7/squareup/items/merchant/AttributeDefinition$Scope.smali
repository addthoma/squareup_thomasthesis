.class public final enum Lsquareup/items/merchant/AttributeDefinition$Scope;
.super Ljava/lang/Enum;
.source "AttributeDefinition.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Scope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/AttributeDefinition$Scope$ProtoAdapter_Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/AttributeDefinition$Scope;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/AttributeDefinition$Scope;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/AttributeDefinition$Scope;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPLICATION:Lsquareup/items/merchant/AttributeDefinition$Scope;

.field public static final enum MERCHANT:Lsquareup/items/merchant/AttributeDefinition$Scope;

.field public static final enum SCOPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Scope;

.field public static final enum SQUARE:Lsquareup/items/merchant/AttributeDefinition$Scope;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 364
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/4 v1, 0x0

    const-string v2, "SCOPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/AttributeDefinition$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->SCOPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 366
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/4 v2, 0x1

    const-string v3, "SQUARE"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/AttributeDefinition$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->SQUARE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 368
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/4 v3, 0x2

    const-string v4, "MERCHANT"

    invoke-direct {v0, v4, v3, v3}, Lsquareup/items/merchant/AttributeDefinition$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->MERCHANT:Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 370
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/4 v4, 0x3

    const-string v5, "APPLICATION"

    invoke-direct {v0, v5, v4, v4}, Lsquareup/items/merchant/AttributeDefinition$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->APPLICATION:Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/4 v0, 0x4

    new-array v0, v0, [Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 363
    sget-object v5, Lsquareup/items/merchant/AttributeDefinition$Scope;->SCOPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    aput-object v5, v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Scope;->SQUARE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Scope;->MERCHANT:Lsquareup/items/merchant/AttributeDefinition$Scope;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Scope;->APPLICATION:Lsquareup/items/merchant/AttributeDefinition$Scope;

    aput-object v1, v0, v4

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->$VALUES:[Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 372
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Scope$ProtoAdapter_Scope;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$Scope$ProtoAdapter_Scope;-><init>()V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 376
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 377
    iput p3, p0, Lsquareup/items/merchant/AttributeDefinition$Scope;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/AttributeDefinition$Scope;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 388
    :cond_0
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Scope;->APPLICATION:Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object p0

    .line 387
    :cond_1
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Scope;->MERCHANT:Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object p0

    .line 386
    :cond_2
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Scope;->SQUARE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object p0

    .line 385
    :cond_3
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Scope;->SCOPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Scope;
    .locals 1

    .line 363
    const-class v0, Lsquareup/items/merchant/AttributeDefinition$Scope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/AttributeDefinition$Scope;
    .locals 1

    .line 363
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->$VALUES:[Lsquareup/items/merchant/AttributeDefinition$Scope;

    invoke-virtual {v0}, [Lsquareup/items/merchant/AttributeDefinition$Scope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 395
    iget v0, p0, Lsquareup/items/merchant/AttributeDefinition$Scope;->value:I

    return v0
.end method
