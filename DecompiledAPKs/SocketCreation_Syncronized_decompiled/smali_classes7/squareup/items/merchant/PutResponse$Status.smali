.class public final enum Lsquareup/items/merchant/PutResponse$Status;
.super Ljava/lang/Enum;
.source "PutResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/PutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/PutResponse$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/PutResponse$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/PutResponse$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/PutResponse$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lsquareup/items/merchant/PutResponse$Status;

.field public static final enum ERROR:Lsquareup/items/merchant/PutResponse$Status;

.field public static final enum SUCCESS:Lsquareup/items/merchant/PutResponse$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 231
    new-instance v0, Lsquareup/items/merchant/PutResponse$Status;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/PutResponse$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/PutResponse$Status;->DO_NOT_USE:Lsquareup/items/merchant/PutResponse$Status;

    .line 233
    new-instance v0, Lsquareup/items/merchant/PutResponse$Status;

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/PutResponse$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/PutResponse$Status;->SUCCESS:Lsquareup/items/merchant/PutResponse$Status;

    .line 235
    new-instance v0, Lsquareup/items/merchant/PutResponse$Status;

    const/4 v3, 0x2

    const-string v4, "ERROR"

    invoke-direct {v0, v4, v3, v3}, Lsquareup/items/merchant/PutResponse$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/PutResponse$Status;->ERROR:Lsquareup/items/merchant/PutResponse$Status;

    const/4 v0, 0x3

    new-array v0, v0, [Lsquareup/items/merchant/PutResponse$Status;

    .line 230
    sget-object v4, Lsquareup/items/merchant/PutResponse$Status;->DO_NOT_USE:Lsquareup/items/merchant/PutResponse$Status;

    aput-object v4, v0, v1

    sget-object v1, Lsquareup/items/merchant/PutResponse$Status;->SUCCESS:Lsquareup/items/merchant/PutResponse$Status;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/PutResponse$Status;->ERROR:Lsquareup/items/merchant/PutResponse$Status;

    aput-object v1, v0, v3

    sput-object v0, Lsquareup/items/merchant/PutResponse$Status;->$VALUES:[Lsquareup/items/merchant/PutResponse$Status;

    .line 237
    new-instance v0, Lsquareup/items/merchant/PutResponse$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lsquareup/items/merchant/PutResponse$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lsquareup/items/merchant/PutResponse$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 241
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 242
    iput p3, p0, Lsquareup/items/merchant/PutResponse$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/PutResponse$Status;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 252
    :cond_0
    sget-object p0, Lsquareup/items/merchant/PutResponse$Status;->ERROR:Lsquareup/items/merchant/PutResponse$Status;

    return-object p0

    .line 251
    :cond_1
    sget-object p0, Lsquareup/items/merchant/PutResponse$Status;->SUCCESS:Lsquareup/items/merchant/PutResponse$Status;

    return-object p0

    .line 250
    :cond_2
    sget-object p0, Lsquareup/items/merchant/PutResponse$Status;->DO_NOT_USE:Lsquareup/items/merchant/PutResponse$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/PutResponse$Status;
    .locals 1

    .line 230
    const-class v0, Lsquareup/items/merchant/PutResponse$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/PutResponse$Status;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/PutResponse$Status;
    .locals 1

    .line 230
    sget-object v0, Lsquareup/items/merchant/PutResponse$Status;->$VALUES:[Lsquareup/items/merchant/PutResponse$Status;

    invoke-virtual {v0}, [Lsquareup/items/merchant/PutResponse$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/PutResponse$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 259
    iget v0, p0, Lsquareup/items/merchant/PutResponse$Status;->value:I

    return v0
.end method
