.class public final Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
        "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_definition_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public int_value:Ljava/lang/Long;

.field public string_value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 935
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 936
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->attribute_definition_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_definition_tokens(Ljava/util/List;)Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;"
        }
    .end annotation

    .line 943
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 944
    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->attribute_definition_tokens:Ljava/util/List;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 928
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->build()Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;
    .locals 5

    .line 968
    new-instance v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->attribute_definition_tokens:Ljava/util/List;

    iget-object v2, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->int_value:Ljava/lang/Long;

    iget-object v3, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->string_value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;-><init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public int_value(Ljava/lang/Long;)Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;
    .locals 0

    .line 952
    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->int_value:Ljava/lang/Long;

    const/4 p1, 0x0

    .line 953
    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->string_value:Ljava/lang/String;

    return-object p0
.end method

.method public string_value(Ljava/lang/String;)Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;
    .locals 0

    .line 961
    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->string_value:Ljava/lang/String;

    const/4 p1, 0x0

    .line 962
    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->int_value:Ljava/lang/Long;

    return-object p0
.end method
