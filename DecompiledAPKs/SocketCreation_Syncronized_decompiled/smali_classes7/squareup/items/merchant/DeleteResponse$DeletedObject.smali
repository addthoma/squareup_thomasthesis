.class public final Lsquareup/items/merchant/DeleteResponse$DeletedObject;
.super Lcom/squareup/wire/Message;
.source "DeleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/DeleteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeletedObject"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/DeleteResponse$DeletedObject$ProtoAdapter_DeletedObject;,
        Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
        "Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lsquareup/items/merchant/CatalogObjectType;

.field private static final serialVersionUID:J


# instance fields
.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lsquareup/items/merchant/CatalogObjectType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CatalogObjectType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 185
    new-instance v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$ProtoAdapter_DeletedObject;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject$ProtoAdapter_DeletedObject;-><init>()V

    sput-object v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 189
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->DO_NOT_USE:Lsquareup/items/merchant/CatalogObjectType;

    sput-object v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->DEFAULT_TYPE:Lsquareup/items/merchant/CatalogObjectType;

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/lang/String;)V
    .locals 1

    .line 206
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;-><init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 210
    sget-object v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 211
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    .line 212
    iput-object p2, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 227
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/DeleteResponse$DeletedObject;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 228
    :cond_1
    check-cast p1, Lsquareup/items/merchant/DeleteResponse$DeletedObject;

    .line 229
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    iget-object p1, p1, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    .line 231
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 236
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 238
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/items/merchant/CatalogObjectType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 241
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 184
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->newBuilder()Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;
    .locals 2

    .line 217
    new-instance v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;-><init>()V

    .line 218
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->type:Lsquareup/items/merchant/CatalogObjectType;

    .line 219
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->token:Ljava/lang/String;

    .line 220
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->type:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeletedObject{"

    .line 251
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
