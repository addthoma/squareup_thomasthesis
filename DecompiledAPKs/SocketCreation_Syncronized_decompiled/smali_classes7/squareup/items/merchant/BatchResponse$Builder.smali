.class public final Lsquareup/items/merchant/BatchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/BatchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/BatchResponse;",
        "Lsquareup/items/merchant/BatchResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public response:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 80
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/BatchResponse$Builder;->response:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lsquareup/items/merchant/BatchResponse$Builder;->build()Lsquareup/items/merchant/BatchResponse;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/BatchResponse;
    .locals 3

    .line 91
    new-instance v0, Lsquareup/items/merchant/BatchResponse;

    iget-object v1, p0, Lsquareup/items/merchant/BatchResponse$Builder;->response:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lsquareup/items/merchant/BatchResponse;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public response(Ljava/util/List;)Lsquareup/items/merchant/BatchResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Response;",
            ">;)",
            "Lsquareup/items/merchant/BatchResponse$Builder;"
        }
    .end annotation

    .line 84
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 85
    iput-object p1, p0, Lsquareup/items/merchant/BatchResponse$Builder;->response:Ljava/util/List;

    return-object p0
.end method
