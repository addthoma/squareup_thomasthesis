.class final Lsquareup/items/merchant/Query$ProtoAdapter_Query;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/Query;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1229
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/Query;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1227
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$ProtoAdapter_Query;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Query;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Query;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1262
    new-instance v0, Lsquareup/items/merchant/Query$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$Builder;-><init>()V

    .line 1263
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1264
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1283
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1281
    :pswitch_0
    sget-object v3, Lsquareup/items/merchant/Query$AttributeValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/Query$AttributeValues;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Builder;->attribute_values(Lsquareup/items/merchant/Query$AttributeValues;)Lsquareup/items/merchant/Query$Builder;

    goto :goto_0

    .line 1280
    :pswitch_1
    iget-object v3, v0, Lsquareup/items/merchant/Query$Builder;->set_query_values:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279
    :pswitch_2
    sget-object v3, Lsquareup/items/merchant/Query$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/Query$SortedAttribute;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Builder;->sorted_attribute(Lsquareup/items/merchant/Query$SortedAttribute;)Lsquareup/items/merchant/Query$Builder;

    goto :goto_0

    .line 1278
    :pswitch_3
    iget-object v3, v0, Lsquareup/items/merchant/Query$Builder;->neighbor_types:Ljava/util/List;

    sget-object v4, Lsquareup/items/merchant/Query$NeighborType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1277
    :pswitch_4
    sget-object v3, Lsquareup/items/merchant/CogsId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/CogsId;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Builder;->cogs_id(Lsquareup/items/merchant/CogsId;)Lsquareup/items/merchant/Query$Builder;

    goto :goto_0

    .line 1276
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Builder;->key(Ljava/lang/String;)Lsquareup/items/merchant/Query$Builder;

    goto :goto_0

    .line 1275
    :pswitch_6
    sget-object v3, Lsquareup/items/merchant/Query$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/Query$Range;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Builder;->range(Lsquareup/items/merchant/Query$Range;)Lsquareup/items/merchant/Query$Builder;

    goto :goto_0

    .line 1274
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Builder;->text_term(Ljava/lang/String;)Lsquareup/items/merchant/Query$Builder;

    goto :goto_0

    .line 1268
    :pswitch_8
    :try_start_0
    sget-object v4, Lsquareup/items/merchant/Query$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/Query$Type;

    invoke-virtual {v0, v4}, Lsquareup/items/merchant/Query$Builder;->type(Lsquareup/items/merchant/Query$Type;)Lsquareup/items/merchant/Query$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1270
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/items/merchant/Query$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1287
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/Query$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1288
    invoke-virtual {v0}, Lsquareup/items/merchant/Query$Builder;->build()Lsquareup/items/merchant/Query;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1227
    check-cast p2, Lsquareup/items/merchant/Query;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/Query$ProtoAdapter_Query;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Query;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Query;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1248
    sget-object v0, Lsquareup/items/merchant/Query$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1249
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1250
    sget-object v0, Lsquareup/items/merchant/Query$NeighborType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1251
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1252
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1253
    sget-object v0, Lsquareup/items/merchant/Query$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1254
    sget-object v0, Lsquareup/items/merchant/CogsId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1255
    sget-object v0, Lsquareup/items/merchant/Query$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1256
    sget-object v0, Lsquareup/items/merchant/Query$AttributeValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1257
    invoke-virtual {p2}, Lsquareup/items/merchant/Query;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1227
    check-cast p1, Lsquareup/items/merchant/Query;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$ProtoAdapter_Query;->encodedSize(Lsquareup/items/merchant/Query;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/Query;)I
    .locals 4

    .line 1234
    sget-object v0, Lsquareup/items/merchant/Query$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1235
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/Query$NeighborType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1236
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1237
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1238
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/Query$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    const/4 v3, 0x3

    .line 1239
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/CogsId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    const/4 v3, 0x5

    .line 1240
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/Query$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    const/4 v3, 0x7

    .line 1241
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/Query$AttributeValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    const/16 v3, 0x9

    .line 1242
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1243
    invoke-virtual {p1}, Lsquareup/items/merchant/Query;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1227
    check-cast p1, Lsquareup/items/merchant/Query;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$ProtoAdapter_Query;->redact(Lsquareup/items/merchant/Query;)Lsquareup/items/merchant/Query;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/Query;)Lsquareup/items/merchant/Query;
    .locals 2

    .line 1293
    invoke-virtual {p1}, Lsquareup/items/merchant/Query;->newBuilder()Lsquareup/items/merchant/Query$Builder;

    move-result-object p1

    .line 1294
    iget-object v0, p1, Lsquareup/items/merchant/Query$Builder;->neighbor_types:Ljava/util/List;

    sget-object v1, Lsquareup/items/merchant/Query$NeighborType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1295
    iget-object v0, p1, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/items/merchant/Query$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/Query$Range;

    iput-object v0, p1, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    .line 1296
    :cond_0
    iget-object v0, p1, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/items/merchant/CogsId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/CogsId;

    iput-object v0, p1, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 1297
    :cond_1
    iget-object v0, p1, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/items/merchant/Query$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/Query$SortedAttribute;

    iput-object v0, p1, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 1298
    :cond_2
    iget-object v0, p1, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    if-eqz v0, :cond_3

    sget-object v0, Lsquareup/items/merchant/Query$AttributeValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/Query$AttributeValues;

    iput-object v0, p1, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    .line 1299
    :cond_3
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1300
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Builder;->build()Lsquareup/items/merchant/Query;

    move-result-object p1

    return-object p1
.end method
