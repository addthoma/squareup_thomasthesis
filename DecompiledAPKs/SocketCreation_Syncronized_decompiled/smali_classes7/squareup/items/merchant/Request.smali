.class public final Lsquareup/items/merchant/Request;
.super Lcom/squareup/wire/Message;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Request$ProtoAdapter_Request;,
        Lsquareup/items/merchant/Request$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Request;",
        "Lsquareup/items/merchant/Request$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Request;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final delete_request:Lsquareup/items/merchant/DeleteRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.DeleteRequest#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final get_request:Lsquareup/items/merchant/GetRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.GetRequest#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final put_request:Lsquareup/items/merchant/PutRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.PutRequest#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lsquareup/items/merchant/Request$ProtoAdapter_Request;

    invoke-direct {v0}, Lsquareup/items/merchant/Request$ProtoAdapter_Request;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/GetRequest;Lsquareup/items/merchant/PutRequest;Lsquareup/items/merchant/DeleteRequest;)V
    .locals 1

    .line 46
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lsquareup/items/merchant/Request;-><init>(Lsquareup/items/merchant/GetRequest;Lsquareup/items/merchant/PutRequest;Lsquareup/items/merchant/DeleteRequest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/GetRequest;Lsquareup/items/merchant/PutRequest;Lsquareup/items/merchant/DeleteRequest;Lokio/ByteString;)V
    .locals 1

    .line 51
    sget-object v0, Lsquareup/items/merchant/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 52
    iput-object p1, p0, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    .line 53
    iput-object p2, p0, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    .line 54
    iput-object p3, p0, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 70
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Request;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 71
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Request;

    .line 72
    invoke-virtual {p0}, Lsquareup/items/merchant/Request;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Request;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    iget-object v3, p1, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    .line 73
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    iget-object v3, p1, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    iget-object p1, p1, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 82
    invoke-virtual {p0}, Lsquareup/items/merchant/Request;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/items/merchant/GetRequest;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/items/merchant/PutRequest;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/items/merchant/DeleteRequest;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 86
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lsquareup/items/merchant/Request;->newBuilder()Lsquareup/items/merchant/Request$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Request$Builder;
    .locals 2

    .line 59
    new-instance v0, Lsquareup/items/merchant/Request$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Request$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    iput-object v1, v0, Lsquareup/items/merchant/Request$Builder;->get_request:Lsquareup/items/merchant/GetRequest;

    .line 61
    iget-object v1, p0, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    iput-object v1, v0, Lsquareup/items/merchant/Request$Builder;->put_request:Lsquareup/items/merchant/PutRequest;

    .line 62
    iget-object v1, p0, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    iput-object v1, v0, Lsquareup/items/merchant/Request$Builder;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    .line 63
    invoke-virtual {p0}, Lsquareup/items/merchant/Request;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    iget-object v1, p0, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    if-eqz v1, :cond_0

    const-string v1, ", get_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 95
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    if-eqz v1, :cond_1

    const-string v1, ", put_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    if-eqz v1, :cond_2

    const-string v1, ", delete_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Request{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
