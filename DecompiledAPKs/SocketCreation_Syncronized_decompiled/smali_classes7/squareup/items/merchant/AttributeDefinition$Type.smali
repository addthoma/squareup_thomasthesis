.class public final enum Lsquareup/items/merchant/AttributeDefinition$Type;
.super Ljava/lang/Enum;
.source "AttributeDefinition.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/AttributeDefinition$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/AttributeDefinition$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/AttributeDefinition$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATTRIBUTE:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum BOOLEAN:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum DECIMAL:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum ENUM:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum FOREIGN_TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum INT:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum SELECTION:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum STRING:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum TEXT:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public static final enum TYPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 266
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v1, 0x0

    const-string v2, "TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->TYPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 271
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v2, 0x1

    const-string v3, "STRING"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->STRING:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 276
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v3, 0x2

    const-string v4, "INT"

    invoke-direct {v0, v4, v3, v3}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->INT:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 281
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v4, 0x3

    const-string v5, "BOOLEAN"

    invoke-direct {v0, v5, v4, v4}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->BOOLEAN:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 286
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v5, 0x4

    const-string v6, "TOKEN"

    invoke-direct {v0, v6, v5, v5}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 291
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v6, 0x5

    const-string v7, "ENUM"

    invoke-direct {v0, v7, v6, v6}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->ENUM:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 296
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v7, 0x6

    const-string v8, "TEXT"

    invoke-direct {v0, v8, v7, v7}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->TEXT:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 301
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/4 v8, 0x7

    const-string v9, "FOREIGN_TOKEN"

    invoke-direct {v0, v9, v8, v8}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->FOREIGN_TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 306
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/16 v9, 0x8

    const-string v10, "ATTRIBUTE"

    invoke-direct {v0, v10, v9, v9}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->ATTRIBUTE:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 311
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/16 v10, 0x9

    const-string v11, "DECIMAL"

    invoke-direct {v0, v11, v10, v10}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->DECIMAL:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 316
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    const/16 v11, 0xa

    const-string v12, "SELECTION"

    invoke-direct {v0, v12, v11, v11}, Lsquareup/items/merchant/AttributeDefinition$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->SELECTION:Lsquareup/items/merchant/AttributeDefinition$Type;

    const/16 v0, 0xb

    new-array v0, v0, [Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 265
    sget-object v12, Lsquareup/items/merchant/AttributeDefinition$Type;->TYPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v12, v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->STRING:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->INT:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->BOOLEAN:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v4

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v5

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->ENUM:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v6

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->TEXT:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v7

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->FOREIGN_TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v8

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->ATTRIBUTE:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v9

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->DECIMAL:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v10

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$Type;->SELECTION:Lsquareup/items/merchant/AttributeDefinition$Type;

    aput-object v1, v0, v11

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->$VALUES:[Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 318
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 322
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 323
    iput p3, p0, Lsquareup/items/merchant/AttributeDefinition$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/AttributeDefinition$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 341
    :pswitch_0
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->SELECTION:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 340
    :pswitch_1
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->DECIMAL:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 339
    :pswitch_2
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->ATTRIBUTE:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 338
    :pswitch_3
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->FOREIGN_TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 337
    :pswitch_4
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->TEXT:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 336
    :pswitch_5
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->ENUM:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 335
    :pswitch_6
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->TOKEN:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 334
    :pswitch_7
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->BOOLEAN:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 333
    :pswitch_8
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->INT:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 332
    :pswitch_9
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->STRING:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    .line 331
    :pswitch_a
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$Type;->TYPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Type;
    .locals 1

    .line 265
    const-class v0, Lsquareup/items/merchant/AttributeDefinition$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/AttributeDefinition$Type;
    .locals 1

    .line 265
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->$VALUES:[Lsquareup/items/merchant/AttributeDefinition$Type;

    invoke-virtual {v0}, [Lsquareup/items/merchant/AttributeDefinition$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 348
    iget v0, p0, Lsquareup/items/merchant/AttributeDefinition$Type;->value:I

    return v0
.end method
