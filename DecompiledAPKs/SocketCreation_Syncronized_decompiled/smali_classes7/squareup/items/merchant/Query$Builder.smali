.class public final Lsquareup/items/merchant/Query$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Query;",
        "Lsquareup/items/merchant/Query$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

.field public cogs_id:Lsquareup/items/merchant/CogsId;

.field public key:Ljava/lang/String;

.field public neighbor_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$NeighborType;",
            ">;"
        }
    .end annotation
.end field

.field public range:Lsquareup/items/merchant/Query$Range;

.field public set_query_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

.field public text_term:Ljava/lang/String;

.field public type:Lsquareup/items/merchant/Query$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 218
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 219
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/Query$Builder;->neighbor_types:Ljava/util/List;

    .line 220
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/Query$Builder;->set_query_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_values(Lsquareup/items/merchant/Query$AttributeValues;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 299
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    const/4 p1, 0x0

    .line 300
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    .line 301
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    .line 302
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 303
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 199
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$Builder;->build()Lsquareup/items/merchant/Query;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Query;
    .locals 12

    .line 309
    new-instance v11, Lsquareup/items/merchant/Query;

    iget-object v1, p0, Lsquareup/items/merchant/Query$Builder;->type:Lsquareup/items/merchant/Query$Type;

    iget-object v2, p0, Lsquareup/items/merchant/Query$Builder;->key:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/items/merchant/Query$Builder;->neighbor_types:Ljava/util/List;

    iget-object v4, p0, Lsquareup/items/merchant/Query$Builder;->set_query_values:Ljava/util/List;

    iget-object v5, p0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    iget-object v6, p0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    iget-object v7, p0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    iget-object v8, p0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    iget-object v9, p0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lsquareup/items/merchant/Query;-><init>(Lsquareup/items/merchant/Query$Type;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lsquareup/items/merchant/Query$Range;Lsquareup/items/merchant/CogsId;Lsquareup/items/merchant/Query$SortedAttribute;Lsquareup/items/merchant/Query$AttributeValues;Lokio/ByteString;)V

    return-object v11
.end method

.method public cogs_id(Lsquareup/items/merchant/CogsId;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    const/4 p1, 0x0

    .line 282
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    .line 283
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    .line 284
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 285
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    return-object p0
.end method

.method public key(Ljava/lang/String;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public neighbor_types(Ljava/util/List;)Lsquareup/items/merchant/Query$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$NeighborType;",
            ">;)",
            "Lsquareup/items/merchant/Query$Builder;"
        }
    .end annotation

    .line 247
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 248
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->neighbor_types:Ljava/util/List;

    return-object p0
.end method

.method public range(Lsquareup/items/merchant/Query$Range;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    const/4 p1, 0x0

    .line 273
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    .line 274
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 275
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 276
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    return-object p0
.end method

.method public set_query_values(Ljava/util/List;)Lsquareup/items/merchant/Query$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lsquareup/items/merchant/Query$Builder;"
        }
    .end annotation

    .line 257
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 258
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->set_query_values:Ljava/util/List;

    return-object p0
.end method

.method public sorted_attribute(Lsquareup/items/merchant/Query$SortedAttribute;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    const/4 p1, 0x0

    .line 291
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    .line 292
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    .line 293
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 294
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    return-object p0
.end method

.method public text_term(Ljava/lang/String;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    const/4 p1, 0x0

    .line 264
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    .line 265
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 266
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 267
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    return-object p0
.end method

.method public type(Lsquareup/items/merchant/Query$Type;)Lsquareup/items/merchant/Query$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lsquareup/items/merchant/Query$Builder;->type:Lsquareup/items/merchant/Query$Type;

    return-object p0
.end method
