.class public final Lsquareup/items/merchant/Query$AttributeValues;
.super Lcom/squareup/wire/Message;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttributeValues"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$AttributeValues$ProtoAdapter_AttributeValues;,
        Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;,
        Lsquareup/items/merchant/Query$AttributeValues$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Query$AttributeValues;",
        "Lsquareup/items/merchant/Query$AttributeValues$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$AttributeValues;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$AttributeValues$AttributeValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 749
    new-instance v0, Lsquareup/items/merchant/Query$AttributeValues$ProtoAdapter_AttributeValues;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$AttributeValues$ProtoAdapter_AttributeValues;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$AttributeValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
            ">;)V"
        }
    .end annotation

    .line 764
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lsquareup/items/merchant/Query$AttributeValues;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 768
    sget-object v0, Lsquareup/items/merchant/Query$AttributeValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p2, "attribute_values"

    .line 769
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 783
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Query$AttributeValues;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 784
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Query$AttributeValues;

    .line 785
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Query$AttributeValues;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    iget-object p1, p1, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    .line 786
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 791
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 793
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 794
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 795
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 748
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues;->newBuilder()Lsquareup/items/merchant/Query$AttributeValues$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Query$AttributeValues$Builder;
    .locals 2

    .line 774
    new-instance v0, Lsquareup/items/merchant/Query$AttributeValues$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$AttributeValues$Builder;-><init>()V

    .line 775
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/Query$AttributeValues$Builder;->attribute_values:Ljava/util/List;

    .line 776
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Query$AttributeValues$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 802
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 803
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", attribute_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues;->attribute_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AttributeValues{"

    .line 804
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
