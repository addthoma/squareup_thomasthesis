.class public final Lsquareup/spe/STM32F2Manifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "STM32F2Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/STM32F2Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/STM32F2Manifest;",
        "Lsquareup/spe/STM32F2Manifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bootloader:Lsquareup/spe/AssetManifest;

.field public chipid:Lokio/ByteString;

.field public configuration:Lsquareup/spe/UnitConfiguration;

.field public fw_a:Lsquareup/spe/AssetManifest;

.field public fw_b:Lsquareup/spe/AssetManifest;

.field public running_slot:Lsquareup/spe/AssetTypeV2;

.field public signer_fingerprint:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 191
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 176
    invoke-virtual {p0}, Lsquareup/spe/STM32F2Manifest$Builder;->build()Lsquareup/spe/STM32F2Manifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/STM32F2Manifest;
    .locals 10

    .line 252
    new-instance v9, Lsquareup/spe/STM32F2Manifest;

    iget-object v1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v2, p0, Lsquareup/spe/STM32F2Manifest$Builder;->chipid:Lokio/ByteString;

    iget-object v3, p0, Lsquareup/spe/STM32F2Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    iget-object v4, p0, Lsquareup/spe/STM32F2Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    iget-object v5, p0, Lsquareup/spe/STM32F2Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    iget-object v6, p0, Lsquareup/spe/STM32F2Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    iget-object v7, p0, Lsquareup/spe/STM32F2Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lsquareup/spe/STM32F2Manifest;-><init>(Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;)V

    return-object v9
.end method

.method public chipid(Lokio/ByteString;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->chipid:Lokio/ByteString;

    return-object p0
.end method

.method public configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    return-object p0
.end method

.method public fw_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public fw_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->running_slot:Lsquareup/spe/AssetTypeV2;

    return-object p0
.end method

.method public signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/STM32F2Manifest$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lsquareup/spe/STM32F2Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    return-object p0
.end method
