.class public final Lsquareup/spe/K21Manifest;
.super Lcom/squareup/wire/Message;
.source "K21Manifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/K21Manifest$ProtoAdapter_K21Manifest;,
        Lsquareup/spe/K21Manifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/K21Manifest;",
        "Lsquareup/spe/K21Manifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/K21Manifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHIPID:Lokio/ByteString;

.field public static final DEFAULT_CONFIGURATION:Lsquareup/spe/UnitConfiguration;

.field public static final DEFAULT_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

.field public static final DEFAULT_SIGNER_FINGERPRINT:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final bootloader:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final chipid:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final configuration:Lsquareup/spe/UnitConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.UnitConfiguration#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final fw_a:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final fw_b:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final running_slot:Lsquareup/spe/AssetTypeV2;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetTypeV2#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final signer_fingerprint:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lsquareup/spe/K21Manifest$ProtoAdapter_K21Manifest;

    invoke-direct {v0}, Lsquareup/spe/K21Manifest$ProtoAdapter_K21Manifest;-><init>()V

    sput-object v0, Lsquareup/spe/K21Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    sput-object v0, Lsquareup/spe/K21Manifest;->DEFAULT_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

    .line 26
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K21Manifest;->DEFAULT_CHIPID:Lokio/ByteString;

    .line 28
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K21Manifest;->DEFAULT_SIGNER_FINGERPRINT:Lokio/ByteString;

    .line 30
    sget-object v0, Lsquareup/spe/UnitConfiguration;->CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;

    sput-object v0, Lsquareup/spe/K21Manifest;->DEFAULT_CONFIGURATION:Lsquareup/spe/UnitConfiguration;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;)V
    .locals 9

    .line 77
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lsquareup/spe/K21Manifest;-><init>(Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;)V
    .locals 1

    .line 83
    sget-object v0, Lsquareup/spe/K21Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 84
    iput-object p1, p0, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    .line 85
    iput-object p2, p0, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    .line 86
    iput-object p3, p0, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    .line 87
    iput-object p4, p0, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    .line 88
    iput-object p5, p0, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    .line 89
    iput-object p6, p0, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    .line 90
    iput-object p7, p0, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 110
    :cond_0
    instance-of v1, p1, Lsquareup/spe/K21Manifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 111
    :cond_1
    check-cast p1, Lsquareup/spe/K21Manifest;

    .line 112
    invoke-virtual {p0}, Lsquareup/spe/K21Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/K21Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v3, p1, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    iget-object p1, p1, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 119
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 124
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 126
    invoke-virtual {p0}, Lsquareup/spe/K21Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/AssetTypeV2;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lsquareup/spe/UnitConfiguration;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 134
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lsquareup/spe/K21Manifest;->newBuilder()Lsquareup/spe/K21Manifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/K21Manifest$Builder;
    .locals 2

    .line 95
    new-instance v0, Lsquareup/spe/K21Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/K21Manifest$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->running_slot:Lsquareup/spe/AssetTypeV2;

    .line 97
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->chipid:Lokio/ByteString;

    .line 98
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    .line 99
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    .line 100
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    .line 101
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    .line 102
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    iput-object v1, v0, Lsquareup/spe/K21Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 103
    invoke-virtual {p0}, Lsquareup/spe/K21Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/K21Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    if-eqz v1, :cond_0

    const-string v1, ", running_slot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_0
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", chipid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->chipid:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_1
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", signer_fingerprint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->signer_fingerprint:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_2
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_3

    const-string v1, ", bootloader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_3
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_4

    const-string v1, ", fw_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_4
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_5

    const-string v1, ", fw_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_5
    iget-object v1, p0, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    if-eqz v1, :cond_6

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K21Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "K21Manifest{"

    .line 149
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
