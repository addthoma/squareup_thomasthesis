.class public final Lsquareup/spe/K400Manifest;
.super Lcom/squareup/wire/Message;
.source "K400Manifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;,
        Lsquareup/spe/K400Manifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/K400Manifest;",
        "Lsquareup/spe/K400Manifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/K400Manifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONFIGURATION:Lsquareup/spe/UnitConfiguration;

.field public static final DEFAULT_JTAG_FUSES:Lokio/ByteString;

.field public static final DEFAULT_SIGNER_FINGERPRINT:Lokio/ByteString;

.field public static final DEFAULT_TMS_CAPK_STRUCT_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_TMS_CAPK_STRUCT_VERSION_SUPPORTED_BY_CPU0:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final configuration:Lsquareup/spe/UnitConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.UnitConfiguration#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final cpu0_bootloader:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cpu0_firmware:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final cpu1_bootloader:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final cpu1_firmware:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final fpga:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final jtag_fuses:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x9
    .end annotation
.end field

.field public final signer_fingerprint:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x7
    .end annotation
.end field

.field public final tms_capk:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final tms_capk_struct_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0xa
    .end annotation
.end field

.field public final tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;

    invoke-direct {v0}, Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;-><init>()V

    sput-object v0, Lsquareup/spe/K400Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K400Manifest;->DEFAULT_SIGNER_FINGERPRINT:Lokio/ByteString;

    .line 27
    sget-object v0, Lsquareup/spe/UnitConfiguration;->CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;

    sput-object v0, Lsquareup/spe/K400Manifest;->DEFAULT_CONFIGURATION:Lsquareup/spe/UnitConfiguration;

    .line 29
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K400Manifest;->DEFAULT_JTAG_FUSES:Lokio/ByteString;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lsquareup/spe/K400Manifest;->DEFAULT_TMS_CAPK_STRUCT_VERSION:Ljava/lang/Integer;

    .line 33
    sput-object v0, Lsquareup/spe/K400Manifest;->DEFAULT_TMS_CAPK_STRUCT_VERSION_SUPPORTED_BY_CPU0:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lokio/ByteString;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 13

    .line 106
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lsquareup/spe/K400Manifest;-><init>(Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lokio/ByteString;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lokio/ByteString;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 114
    sget-object v0, Lsquareup/spe/K400Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 115
    iput-object p1, p0, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    .line 116
    iput-object p2, p0, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    .line 117
    iput-object p3, p0, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    .line 118
    iput-object p4, p0, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    .line 119
    iput-object p5, p0, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    .line 120
    iput-object p6, p0, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    .line 121
    iput-object p7, p0, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    .line 122
    iput-object p8, p0, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 123
    iput-object p9, p0, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    .line 124
    iput-object p10, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    .line 125
    iput-object p11, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 149
    :cond_0
    instance-of v1, p1, Lsquareup/spe/K400Manifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 150
    :cond_1
    check-cast p1, Lsquareup/spe/K400Manifest;

    .line 151
    invoke-virtual {p0}, Lsquareup/spe/K400Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/K400Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    iget-object p1, p1, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    .line 162
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 169
    invoke-virtual {p0}, Lsquareup/spe/K400Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lsquareup/spe/UnitConfiguration;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 181
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lsquareup/spe/K400Manifest;->newBuilder()Lsquareup/spe/K400Manifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/K400Manifest$Builder;
    .locals 2

    .line 130
    new-instance v0, Lsquareup/spe/K400Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/K400Manifest$Builder;-><init>()V

    .line 131
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    .line 132
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    .line 133
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    .line 134
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    .line 135
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->tms_capk:Lsquareup/spe/AssetManifest;

    .line 136
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->fpga:Lsquareup/spe/AssetManifest;

    .line 137
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    .line 138
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 139
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->jtag_fuses:Lokio/ByteString;

    .line 140
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version:Ljava/lang/Integer;

    .line 141
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    .line 142
    invoke-virtual {p0}, Lsquareup/spe/K400Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/K400Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_0

    const-string v1, ", cpu0_bootloader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_0
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_1

    const-string v1, ", cpu1_bootloader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_1
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_2

    const-string v1, ", cpu0_firmware="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_2
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_3

    const-string v1, ", cpu1_firmware="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_3
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_4

    const-string v1, ", tms_capk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_4
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_5

    const-string v1, ", fpga="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_5
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_6

    const-string v1, ", signer_fingerprint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    :cond_6
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    if-eqz v1, :cond_7

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_7
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    if-eqz v1, :cond_8

    const-string v1, ", jtag_fuses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 198
    :cond_8
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", tms_capk_struct_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_9
    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    const-string v1, ", tms_capk_struct_version_supported_by_cpu0="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "K400Manifest{"

    .line 200
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
