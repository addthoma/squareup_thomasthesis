.class public final Lsquareup/spe/SPEDeviceManifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SPEDeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/SPEDeviceManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/SPEDeviceManifest;",
        "Lsquareup/spe/SPEDeviceManifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public hwid:Lokio/ByteString;

.field public mlb_serial_number:Ljava/lang/String;

.field public r12c_manifest:Lsquareup/spe/R12CManifest;

.field public s1_manifest:Lsquareup/spe/S1DeviceManifest;

.field public serial_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lsquareup/spe/SPEDeviceManifest$Builder;->build()Lsquareup/spe/SPEDeviceManifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/SPEDeviceManifest;
    .locals 8

    .line 199
    new-instance v7, Lsquareup/spe/SPEDeviceManifest;

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->hwid:Lokio/ByteString;

    iget-object v2, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->serial_number:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->mlb_serial_number:Ljava/lang/String;

    iget-object v4, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    iget-object v5, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lsquareup/spe/SPEDeviceManifest;-><init>(Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lsquareup/spe/R12CManifest;Lsquareup/spe/S1DeviceManifest;Lokio/ByteString;)V

    return-object v7
.end method

.method public hwid(Lokio/ByteString;)Lsquareup/spe/SPEDeviceManifest$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->hwid:Lokio/ByteString;

    return-object p0
.end method

.method public mlb_serial_number(Ljava/lang/String;)Lsquareup/spe/SPEDeviceManifest$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->mlb_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public r12c_manifest(Lsquareup/spe/R12CManifest;)Lsquareup/spe/SPEDeviceManifest$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    const/4 p1, 0x0

    .line 184
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    return-object p0
.end method

.method public s1_manifest(Lsquareup/spe/S1DeviceManifest;)Lsquareup/spe/SPEDeviceManifest$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    const/4 p1, 0x0

    .line 193
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    return-object p0
.end method

.method public serial_number(Ljava/lang/String;)Lsquareup/spe/SPEDeviceManifest$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest$Builder;->serial_number:Ljava/lang/String;

    return-object p0
.end method
