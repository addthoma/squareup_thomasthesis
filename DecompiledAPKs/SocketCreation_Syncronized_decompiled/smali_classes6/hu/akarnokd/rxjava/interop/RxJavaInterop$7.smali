.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lrx/Observable$Transformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/ObservableTransformer;Lio/reactivex/BackpressureStrategy;)Lrx/Observable$Transformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$Transformer<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic val$strategy:Lio/reactivex/BackpressureStrategy;

.field final synthetic val$transformer:Lio/reactivex/ObservableTransformer;


# direct methods
.method constructor <init>(Lio/reactivex/ObservableTransformer;Lio/reactivex/BackpressureStrategy;)V
    .locals 0

    .line 547
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;->val$transformer:Lio/reactivex/ObservableTransformer;

    iput-object p2, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;->val$strategy:Lio/reactivex/BackpressureStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 547
    check-cast p1, Lrx/Observable;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;->call(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public call(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "TR;>;"
        }
    .end annotation

    .line 550
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;->val$transformer:Lio/reactivex/ObservableTransformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/ObservableTransformer;->apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object p1

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;->val$strategy:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
