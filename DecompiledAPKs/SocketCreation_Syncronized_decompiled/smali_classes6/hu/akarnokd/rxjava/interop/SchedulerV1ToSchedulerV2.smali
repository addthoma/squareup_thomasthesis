.class final Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;
.super Lio/reactivex/Scheduler;
.source "SchedulerV1ToSchedulerV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$RunnableToV1Action0;,
        Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;
    }
.end annotation


# instance fields
.field final source:Lrx/Scheduler;


# direct methods
.method constructor <init>(Lrx/Scheduler;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lio/reactivex/Scheduler;-><init>()V

    .line 31
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;->source:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public createWorker()Lio/reactivex/Scheduler$Worker;
    .locals 2

    .line 55
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;

    iget-object v1, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;->source:Lrx/Scheduler;

    invoke-virtual {v1}, Lrx/Scheduler;->createWorker()Lrx/Scheduler$Worker;

    move-result-object v1

    invoke-direct {v0, v1}, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2$WorkerV1ToWorkerV2;-><init>(Lrx/Scheduler$Worker;)V

    return-object v0
.end method

.method public now(Ljava/util/concurrent/TimeUnit;)J
    .locals 3

    .line 36
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;->source:Lrx/Scheduler;

    invoke-virtual {v0}, Lrx/Scheduler;->now()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public shutdown()V
    .locals 2

    .line 48
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;->source:Lrx/Scheduler;

    instance-of v1, v0, Lrx/internal/schedulers/SchedulerLifecycle;

    if-eqz v1, :cond_0

    .line 49
    check-cast v0, Lrx/internal/schedulers/SchedulerLifecycle;

    invoke-interface {v0}, Lrx/internal/schedulers/SchedulerLifecycle;->shutdown()V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 2

    .line 41
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;->source:Lrx/Scheduler;

    instance-of v1, v0, Lrx/internal/schedulers/SchedulerLifecycle;

    if-eqz v1, :cond_0

    .line 42
    check-cast v0, Lrx/internal/schedulers/SchedulerLifecycle;

    invoke-interface {v0}, Lrx/internal/schedulers/SchedulerLifecycle;->start()V

    :cond_0
    return-void
.end method
