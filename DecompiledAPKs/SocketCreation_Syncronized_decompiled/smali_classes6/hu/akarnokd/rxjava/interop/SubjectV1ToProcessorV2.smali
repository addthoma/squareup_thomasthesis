.class final Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;
.super Lio/reactivex/processors/FlowableProcessor;
.source "SubjectV1ToProcessorV2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/processors/FlowableProcessor<",
        "TT;>;"
    }
.end annotation


# instance fields
.field error:Ljava/lang/Throwable;

.field final source:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation
.end field

.field volatile terminated:Z


# direct methods
.method constructor <init>(Lrx/subjects/Subject;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/subjects/Subject<",
            "TT;TT;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Lio/reactivex/processors/FlowableProcessor;-><init>()V

    .line 32
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->source:Lrx/subjects/Subject;

    return-void
.end method


# virtual methods
.method public getThrowable()Ljava/lang/Throwable;
    .locals 1

    .line 105
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->error:Ljava/lang/Throwable;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasComplete()Z
    .locals 1

    .line 95
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->error:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasSubscribers()Z
    .locals 1

    .line 90
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/subjects/Subject;->hasObservers()Z

    move-result v0

    return v0
.end method

.method public hasThrowable()Z
    .locals 1

    .line 100
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->error:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onComplete()V
    .locals 1

    .line 71
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 72
    iput-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    .line 73
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0}, Lrx/subjects/Subject;->onCompleted()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 57
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    .line 59
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Throwable was null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 61
    :cond_0
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->error:Ljava/lang/Throwable;

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    .line 63
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0, p1}, Lrx/subjects/Subject;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 65
    :cond_1
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 46
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    .line 48
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->source:Lrx/subjects/Subject;

    invoke-virtual {v0, p1}, Lrx/subjects/Subject;->onNext(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onSubscribe(Lorg/reactivestreams/Subscription;)V
    .locals 2

    .line 37
    iget-boolean v0, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->terminated:Z

    if-eqz v0, :cond_0

    .line 38
    invoke-interface {p1}, Lorg/reactivestreams/Subscription;->cancel()V

    goto :goto_0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    .line 40
    invoke-interface {p1, v0, v1}, Lorg/reactivestreams/Subscription;->request(J)V

    :goto_0
    return-void
.end method

.method protected subscribeActual(Lorg/reactivestreams/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/reactivestreams/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 79
    new-instance v0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;-><init>(Lorg/reactivestreams/Subscriber;)V

    .line 81
    new-instance v1, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;

    invoke-direct {v1, v0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriberSubscription;-><init>(Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2$ObservableSubscriber;)V

    .line 83
    invoke-interface {p1, v1}, Lorg/reactivestreams/Subscriber;->onSubscribe(Lorg/reactivestreams/Subscription;)V

    .line 85
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;->source:Lrx/subjects/Subject;

    invoke-virtual {p1, v0}, Lrx/subjects/Subject;->unsafeSubscribe(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
