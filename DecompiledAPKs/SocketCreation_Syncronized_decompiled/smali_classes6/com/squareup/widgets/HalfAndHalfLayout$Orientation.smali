.class final enum Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;
.super Ljava/lang/Enum;
.source "HalfAndHalfLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/HalfAndHalfLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

.field public static final enum HORIZONTAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

.field public static final enum VERTICAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 19
    new-instance v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    const/4 v1, 0x0

    const-string v2, "HORIZONTAL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->HORIZONTAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    new-instance v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    const/4 v2, 0x1

    const-string v3, "VERTICAL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->VERTICAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    sget-object v3, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->HORIZONTAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->VERTICAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->$VALUES:[Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->$VALUES:[Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    invoke-virtual {v0}, [Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    return-object v0
.end method
