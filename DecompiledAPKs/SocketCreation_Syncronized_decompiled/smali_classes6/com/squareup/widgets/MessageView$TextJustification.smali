.class public final enum Lcom/squareup/widgets/MessageView$TextJustification;
.super Ljava/lang/Enum;
.source "MessageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/MessageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TextJustification"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/widgets/MessageView$TextJustification;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/widgets/MessageView$TextJustification;

.field public static final enum CENTER:Lcom/squareup/widgets/MessageView$TextJustification;

.field public static final enum GRAVITY:Lcom/squareup/widgets/MessageView$TextJustification;

.field public static final enum LEFT:Lcom/squareup/widgets/MessageView$TextJustification;

.field public static final enum RIGHT:Lcom/squareup/widgets/MessageView$TextJustification;


# instance fields
.field private final horizontalGravity:I

.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 257
    new-instance v0, Lcom/squareup/widgets/MessageView$TextJustification;

    const/4 v1, 0x0

    const-string v2, "GRAVITY"

    invoke-direct {v0, v2, v1, v1, v1}, Lcom/squareup/widgets/MessageView$TextJustification;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->GRAVITY:Lcom/squareup/widgets/MessageView$TextJustification;

    .line 258
    new-instance v0, Lcom/squareup/widgets/MessageView$TextJustification;

    const/4 v2, 0x3

    const/4 v3, 0x1

    const-string v4, "LEFT"

    invoke-direct {v0, v4, v3, v3, v2}, Lcom/squareup/widgets/MessageView$TextJustification;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->LEFT:Lcom/squareup/widgets/MessageView$TextJustification;

    .line 259
    new-instance v0, Lcom/squareup/widgets/MessageView$TextJustification;

    const/4 v4, 0x2

    const-string v5, "RIGHT"

    const/4 v6, 0x5

    invoke-direct {v0, v5, v4, v4, v6}, Lcom/squareup/widgets/MessageView$TextJustification;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->RIGHT:Lcom/squareup/widgets/MessageView$TextJustification;

    .line 260
    new-instance v0, Lcom/squareup/widgets/MessageView$TextJustification;

    const-string v5, "CENTER"

    invoke-direct {v0, v5, v2, v2, v3}, Lcom/squareup/widgets/MessageView$TextJustification;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->CENTER:Lcom/squareup/widgets/MessageView$TextJustification;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/widgets/MessageView$TextJustification;

    .line 252
    sget-object v5, Lcom/squareup/widgets/MessageView$TextJustification;->GRAVITY:Lcom/squareup/widgets/MessageView$TextJustification;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/widgets/MessageView$TextJustification;->LEFT:Lcom/squareup/widgets/MessageView$TextJustification;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/widgets/MessageView$TextJustification;->RIGHT:Lcom/squareup/widgets/MessageView$TextJustification;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/widgets/MessageView$TextJustification;->CENTER:Lcom/squareup/widgets/MessageView$TextJustification;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->$VALUES:[Lcom/squareup/widgets/MessageView$TextJustification;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 265
    iput p3, p0, Lcom/squareup/widgets/MessageView$TextJustification;->value:I

    .line 266
    iput p4, p0, Lcom/squareup/widgets/MessageView$TextJustification;->horizontalGravity:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/MessageView$TextJustification;)I
    .locals 0

    .line 252
    iget p0, p0, Lcom/squareup/widgets/MessageView$TextJustification;->horizontalGravity:I

    return p0
.end method

.method public static fromValue(I)Lcom/squareup/widgets/MessageView$TextJustification;
    .locals 5

    .line 270
    invoke-static {}, Lcom/squareup/widgets/MessageView$TextJustification;->values()[Lcom/squareup/widgets/MessageView$TextJustification;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 271
    iget v4, v3, Lcom/squareup/widgets/MessageView$TextJustification;->value:I

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/widgets/MessageView$TextJustification;
    .locals 1

    .line 252
    const-class v0, Lcom/squareup/widgets/MessageView$TextJustification;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/widgets/MessageView$TextJustification;

    return-object p0
.end method

.method public static values()[Lcom/squareup/widgets/MessageView$TextJustification;
    .locals 1

    .line 252
    sget-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->$VALUES:[Lcom/squareup/widgets/MessageView$TextJustification;

    invoke-virtual {v0}, [Lcom/squareup/widgets/MessageView$TextJustification;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/widgets/MessageView$TextJustification;

    return-object v0
.end method
