.class public Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;
.super Ljava/lang/Object;
.source "AlertController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/dialog/internal/AlertController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlertParams"
.end annotation


# instance fields
.field public adapter:Landroid/widget/ListAdapter;

.field public cancelable:Z

.field public checkedItem:I

.field public final context:Landroid/content/Context;

.field public dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

.field public icon:Landroid/graphics/drawable/Drawable;

.field public iconId:I

.field public isSingleChoice:Z

.field public items:[Ljava/lang/CharSequence;

.field public message:Ljava/lang/CharSequence;

.field public messageContentDescription:Ljava/lang/CharSequence;

.field public negativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field public negativeButtonText:Ljava/lang/CharSequence;

.field public neutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field public neutralButtonText:Ljava/lang/CharSequence;

.field public onCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field public onClickListener:Landroid/content/DialogInterface$OnClickListener;

.field public onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field public onKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field public positiveButtonBackground:I

.field public positiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field public positiveButtonText:Ljava/lang/CharSequence;

.field public positiveButtonTextColor:I

.field public primaryButton:I

.field public title:Ljava/lang/CharSequence;

.field public titleContentDescription:Ljava/lang/CharSequence;

.field public type:I

.field public verticalButtonOrientation:Z

.field public view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 559
    iput v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->iconId:I

    const/4 v0, -0x1

    .line 560
    iput v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->checkedItem:I

    .line 580
    sget-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->DEFAULT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    iput-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    .line 589
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    const/4 p1, 0x1

    .line 590
    iput-boolean p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->cancelable:Z

    return-void
.end method

.method private createListView(Lcom/squareup/widgets/dialog/internal/AlertController;)V
    .locals 7

    .line 652
    new-instance v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 653
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v3, v2, [I

    sget v4, Lcom/squareup/widgets/R$attr;->alertDialogButtonOuterPadding:I

    const/4 v5, 0x0

    aput v4, v3, v5

    .line 654
    invoke-virtual {v1, v3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 655
    invoke-virtual {v1, v5, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 656
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 657
    invoke-virtual {v0, v5, v3, v5, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 658
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 660
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->adapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->context:Landroid/content/Context;

    .line 661
    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$1100(Lcom/squareup/widgets/dialog/internal/AlertController;)I

    move-result v4

    iget-object v6, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->items:[Ljava/lang/CharSequence;

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    :goto_0
    iput-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->adapter:Landroid/widget/ListAdapter;

    .line 663
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->adapter:Landroid/widget/ListAdapter;

    invoke-static {p1, v1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$1202(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;

    .line 664
    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->checkedItem:I

    invoke-static {p1, v1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$1302(Lcom/squareup/widgets/dialog/internal/AlertController;I)I

    .line 666
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_1

    .line 667
    new-instance v1, Lcom/squareup/widgets/dialog/internal/-$$Lambda$AlertController$AlertParams$0ecEwXOIg-gUp8RW5YXC7oAYM-o;

    invoke-direct {v1, p0, p1}, Lcom/squareup/widgets/dialog/internal/-$$Lambda$AlertController$AlertParams$0ecEwXOIg-gUp8RW5YXC7oAYM-o;-><init>(Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;Lcom/squareup/widgets/dialog/internal/AlertController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 675
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->isSingleChoice:Z

    if-eqz v1, :cond_2

    .line 676
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 679
    :cond_2
    invoke-static {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$1402(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/widget/ListView;)Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/widgets/dialog/internal/AlertController;)V
    .locals 14

    .line 594
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->title:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setTitle(Ljava/lang/CharSequence;)V

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->titleContentDescription:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 598
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setTitleContentDescription(Ljava/lang/CharSequence;)V

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 601
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 603
    :cond_2
    iget v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->iconId:I

    if-ltz v0, :cond_3

    .line 604
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setIcon(I)V

    .line 606
    :cond_3
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->message:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    .line 607
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setMessage(Ljava/lang/CharSequence;)V

    .line 609
    :cond_4
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->messageContentDescription:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    .line 610
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setMessageContentDescription(Ljava/lang/CharSequence;)V

    .line 612
    :cond_5
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->items:[Ljava/lang/CharSequence;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->adapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_7

    .line 613
    :cond_6
    invoke-direct {p0, p1}, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->createListView(Lcom/squareup/widgets/dialog/internal/AlertController;)V

    .line 615
    :cond_7
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->view:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 616
    invoke-static {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$800(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/view/View;)V

    .line 619
    :cond_8
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    if-eqz v0, :cond_9

    .line 620
    invoke-static {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$900(Lcom/squareup/widgets/dialog/internal/AlertController;Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;)V

    .line 623
    :cond_9
    iget-boolean v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->verticalButtonOrientation:Z

    if-eqz v0, :cond_a

    .line 624
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->setVerticalButtonOrientation()V

    .line 627
    :cond_a
    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonText:Ljava/lang/CharSequence;

    const/4 v0, 0x1

    const/4 v7, 0x0

    if-eqz v3, :cond_c

    const/4 v2, -0x1

    .line 628
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v5, 0x0

    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->primaryButton:I

    const/4 v6, -0x1

    if-ne v1, v6, :cond_b

    const/4 v6, 0x1

    goto :goto_0

    :cond_b
    const/4 v6, 0x0

    :goto_0
    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/widgets/dialog/internal/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;Z)V

    .line 631
    :cond_c
    iget-object v10, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->negativeButtonText:Ljava/lang/CharSequence;

    if-eqz v10, :cond_e

    const/4 v9, -0x2

    .line 632
    iget-object v11, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->negativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v12, 0x0

    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->primaryButton:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_d

    const/4 v13, 0x1

    goto :goto_1

    :cond_d
    const/4 v13, 0x0

    :goto_1
    move-object v8, p1

    invoke-virtual/range {v8 .. v13}, Lcom/squareup/widgets/dialog/internal/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;Z)V

    .line 635
    :cond_e
    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->neutralButtonText:Ljava/lang/CharSequence;

    if-eqz v3, :cond_10

    const/4 v2, -0x3

    .line 636
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->neutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v5, 0x0

    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->primaryButton:I

    const/4 v6, -0x3

    if-ne v1, v6, :cond_f

    const/4 v6, 0x1

    goto :goto_2

    :cond_f
    const/4 v6, 0x0

    :goto_2
    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/widgets/dialog/internal/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;Z)V

    .line 639
    :cond_10
    iget v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonTextColor:I

    if-eqz v0, :cond_11

    .line 640
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setPositiveButtonTextColor(I)V

    .line 642
    :cond_11
    iget v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->positiveButtonBackground:I

    if-eqz v0, :cond_12

    .line 643
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setPositiveButtonBackground(I)V

    .line 646
    :cond_12
    iget v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->type:I

    if-lez v0, :cond_13

    .line 647
    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$1000(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/view/Window;

    move-result-object p1

    iget v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->type:I

    invoke-virtual {p1, v0}, Landroid/view/Window;->setType(I)V

    :cond_13
    return-void
.end method

.method public synthetic lambda$createListView$0$AlertController$AlertParams(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 668
    iget-object p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->onClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$600(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/content/DialogInterface;

    move-result-object p3

    invoke-interface {p2, p3, p4}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 669
    iget-boolean p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;->isSingleChoice:Z

    if-nez p2, :cond_0

    .line 670
    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$600(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/content/DialogInterface;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :cond_0
    return-void
.end method
