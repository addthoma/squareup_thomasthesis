.class public Lcom/squareup/widgets/dialog/internal/AlertController;
.super Ljava/lang/Object;
.source "AlertController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/dialog/internal/AlertController$AlertParams;,
        Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;,
        Lcom/squareup/widgets/dialog/internal/AlertController$ButtonHandler;
    }
.end annotation


# static fields
.field private static final BIT_BUTTON_NEGATIVE:I = 0x2

.field private static final BIT_BUTTON_NEUTRAL:I = 0x4

.field private static final BIT_BUTTON_POSITIVE:I = 0x1


# instance fields
.field private adapter:Landroid/widget/ListAdapter;

.field private alertDialogLayout:I

.field private buttonNegative:Landroid/widget/Button;

.field private buttonNegativeMessage:Landroid/os/Message;

.field private buttonNegativeText:Ljava/lang/CharSequence;

.field private buttonNeutral:Landroid/widget/Button;

.field private buttonNeutralMessage:Landroid/os/Message;

.field private buttonNeutralText:Ljava/lang/CharSequence;

.field private buttonPositive:Landroid/widget/Button;

.field private buttonPositiveMessage:Landroid/os/Message;

.field private buttonPositiveText:Ljava/lang/CharSequence;

.field private checkedItem:I

.field private dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

.field private final dialogInterface:Landroid/content/DialogInterface;

.field private handler:Landroid/os/Handler;

.field private icon:Landroid/graphics/drawable/Drawable;

.field private iconId:I

.field private iconView:Landroid/widget/ImageView;

.field private listItemLayout:I

.field private listView:Landroid/widget/ListView;

.field mButtonHandler:Landroid/view/View$OnClickListener;

.field private message:Ljava/lang/CharSequence;

.field private messageContentDescription:Ljava/lang/CharSequence;

.field private messageView:Landroid/widget/TextView;

.field private positiveButtonBackground:I

.field private positiveButtonTextColor:I

.field private primaryButton:I

.field private scrollView:Landroid/widget/ScrollView;

.field private title:Ljava/lang/CharSequence;

.field private titleContentDescription:Ljava/lang/CharSequence;

.field private titleView:Landroid/widget/TextView;

.field private verticalButtonOrientation:Z

.field private view:Landroid/view/View;

.field private final window:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V
    .locals 2

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-object v0, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->DEFAULT:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    iput-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    const/4 v0, 0x0

    .line 68
    iput v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->positiveButtonTextColor:I

    .line 69
    iput v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->positiveButtonBackground:I

    const/4 v1, -0x1

    .line 71
    iput v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconId:I

    .line 78
    iput v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->checkedItem:I

    .line 79
    iput v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    .line 86
    new-instance v1, Lcom/squareup/widgets/dialog/internal/AlertController$1;

    invoke-direct {v1, p0}, Lcom/squareup/widgets/dialog/internal/AlertController$1;-><init>(Lcom/squareup/widgets/dialog/internal/AlertController;)V

    iput-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    .line 132
    iput-object p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->dialogInterface:Landroid/content/DialogInterface;

    .line 133
    iput-object p3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    .line 134
    new-instance p3, Lcom/squareup/widgets/dialog/internal/AlertController$ButtonHandler;

    invoke-direct {p3, p2}, Lcom/squareup/widgets/dialog/internal/AlertController$ButtonHandler;-><init>(Landroid/content/DialogInterface;)V

    iput-object p3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->handler:Landroid/os/Handler;

    const/4 p2, 0x2

    new-array p2, p2, [I

    .line 136
    sget p3, Lcom/squareup/widgets/R$attr;->square_layout:I

    aput p3, p2, v0

    sget p3, Lcom/squareup/widgets/R$attr;->square_listItemLayout:I

    const/4 v1, 0x1

    aput p3, p2, v1

    const/4 p3, 0x0

    invoke-virtual {p1, p3, p2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 139
    sget p2, Lcom/squareup/widgets/R$layout;->alert_dialog_holo:I

    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->alertDialogLayout:I

    const p2, 0x1090003

    .line 140
    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listItemLayout:I

    .line 142
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/widget/Button;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositiveMessage:Landroid/os/Message;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/view/Window;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/widgets/dialog/internal/AlertController;)I
    .locals 0

    .line 42
    iget p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listItemLayout:I

    return p0
.end method

.method static synthetic access$1202(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->adapter:Landroid/widget/ListAdapter;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/squareup/widgets/dialog/internal/AlertController;I)I
    .locals 0

    .line 42
    iput p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->checkedItem:I

    return p1
.end method

.method static synthetic access$1402(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/widget/Button;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegativeMessage:Landroid/os/Message;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/widget/Button;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutralMessage:Landroid/os/Message;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/content/DialogInterface;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->dialogInterface:Landroid/content/DialogInterface;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Handler;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->handler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/widgets/dialog/internal/AlertController;Landroid/view/View;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->setView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$900(Lcom/squareup/widgets/dialog/internal/AlertController;Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->setDialogContentLayout(Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;)V

    return-void
.end method

.method static canTextInput(Landroid/view/View;)Z
    .locals 4

    .line 146
    invoke-virtual {p0}, Landroid/view/View;->onCheckIsTextEditor()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 150
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 154
    :cond_1
    check-cast p0, Landroid/view/ViewGroup;

    .line 155
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :cond_2
    if-lez v0, :cond_3

    add-int/lit8 v0, v0, -0x1

    .line 158
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 159
    invoke-static {v3}, Lcom/squareup/widgets/dialog/internal/AlertController;->canTextInput(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v1

    :cond_3
    return v2
.end method

.method private setDialogContentLayout(Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;)V
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    return-void
.end method

.method private setView(Landroid/view/View;)V
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->view:Landroid/view/View;

    return-void
.end method

.method private setupButtons()Z
    .locals 8

    .line 466
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    .line 467
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 469
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositiveText:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x0

    goto :goto_0

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositiveText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 473
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x1

    .line 477
    :goto_0
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const v5, 0x102001a

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    .line 478
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 480
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegativeText:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 481
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 483
    :cond_1
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegativeText:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 484
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    invoke-virtual {v4, v3}, Landroid/widget/Button;->setVisibility(I)V

    or-int/lit8 v0, v0, 0x2

    .line 489
    :goto_1
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const v5, 0x102001b

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    .line 490
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->mButtonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutralText:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 493
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 495
    :cond_2
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutralText:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 496
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    or-int/lit8 v0, v0, 0x4

    .line 501
    :goto_2
    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->positiveButtonBackground:I

    if-eqz v1, :cond_3

    .line 502
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 504
    :cond_3
    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->positiveButtonTextColor:I

    if-eqz v1, :cond_4

    .line 505
    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 508
    :cond_4
    iget-boolean v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->verticalButtonOrientation:Z

    const/4 v4, -0x1

    if-eqz v1, :cond_5

    .line 509
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v5, Lcom/squareup/widgets/R$id;->buttonContainer:I

    invoke-virtual {v1, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 510
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 512
    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    .line 513
    invoke-virtual {v5}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v5

    new-array v6, v2, [I

    sget v7, Lcom/squareup/widgets/R$attr;->square_dividerHorizontal:I

    aput v7, v6, v3

    invoke-virtual {v5, v6}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 514
    invoke-virtual {v5, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 515
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 517
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 518
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 519
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 524
    :cond_5
    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    const/4 v5, -0x3

    const/4 v6, -0x2

    if-nez v1, :cond_9

    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v1

    if-ne v1, v2, :cond_9

    and-int/lit8 v1, v0, 0x1

    if-lez v1, :cond_6

    const/4 v1, -0x1

    goto :goto_3

    :cond_6
    and-int/lit8 v1, v0, 0x2

    if-lez v1, :cond_7

    const/4 v1, -0x2

    goto :goto_3

    :cond_7
    and-int/lit8 v1, v0, 0x4

    if-lez v1, :cond_8

    const/4 v1, -0x3

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    .line 525
    :goto_3
    iput v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    .line 530
    :cond_9
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    iget v7, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    if-ne v7, v4, :cond_a

    const/4 v4, 0x1

    goto :goto_4

    :cond_a
    const/4 v4, 0x0

    :goto_4
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setActivated(Z)V

    .line 531
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    iget v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    if-ne v4, v6, :cond_b

    const/4 v4, 0x1

    goto :goto_5

    :cond_b
    const/4 v4, 0x0

    :goto_5
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setActivated(Z)V

    .line 532
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    iget v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    if-ne v4, v5, :cond_c

    const/4 v4, 0x1

    goto :goto_6

    :cond_c
    const/4 v4, 0x0

    :goto_6
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setActivated(Z)V

    if-eqz v0, :cond_d

    goto :goto_7

    :cond_d
    const/4 v2, 0x0

    :goto_7
    return v2
.end method

.method private setupContent(Landroid/widget/LinearLayout;)V
    .locals 4

    .line 436
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v1, Lcom/squareup/widgets/R$id;->scrollView:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->scrollView:Landroid/widget/ScrollView;

    .line 437
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->scrollView:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 440
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v2, Lcom/squareup/widgets/R$id;->message:I

    invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    .line 441
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    .line 445
    :cond_0
    iget-object v2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->message:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    .line 446
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageContentDescription:Ljava/lang/CharSequence;

    if-eqz p1, :cond_3

    .line 448
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x8

    .line 451
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->scrollView:Landroid/widget/ScrollView;

    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 454
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v2, Lcom/squareup/widgets/R$id;->scrollView:I

    invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 456
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 457
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 459
    :cond_2
    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_3
    :goto_0
    return-void
.end method

.method private setupTitle(Landroid/widget/LinearLayout;)Z
    .locals 6

    .line 395
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->title:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 397
    iget-object v2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const v3, 0x1020006

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    if-eqz v0, :cond_3

    .line 400
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v0, Lcom/squareup/widgets/R$id;->alertTitle:I

    invoke-virtual {p1, v0}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleView:Landroid/widget/TextView;

    .line 402
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->title:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleContentDescription:Ljava/lang/CharSequence;

    if-eqz p1, :cond_0

    .line 404
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 411
    :cond_0
    iget p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconId:I

    if-lez p1, :cond_1

    .line 412
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 414
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    if-nez p1, :cond_4

    .line 420
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v0

    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    .line 421
    invoke-virtual {v4}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v5

    .line 420
    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 422
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 426
    :cond_3
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v1, Lcom/squareup/widgets/R$id;->title_template:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 427
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 429
    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v1, 0x0

    :cond_4
    :goto_0
    return v1
.end method

.method private setupView()V
    .locals 8

    .line 333
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v1, Lcom/squareup/widgets/R$id;->contentPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 334
    invoke-direct {p0, v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setupContent(Landroid/widget/LinearLayout;)V

    .line 335
    invoke-direct {p0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setupButtons()Z

    move-result v0

    .line 337
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v2, Lcom/squareup/widgets/R$id;->topPanel:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 338
    invoke-direct {p0, v1}, Lcom/squareup/widgets/dialog/internal/AlertController;->setupTitle(Landroid/widget/LinearLayout;)Z

    move-result v1

    .line 340
    iget-object v2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v3, Lcom/squareup/widgets/R$id;->buttonPanel:I

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    if-nez v0, :cond_0

    .line 342
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->view:Landroid/view/View;

    const/4 v2, 0x0

    const/4 v4, -0x1

    if-eqz v0, :cond_3

    .line 347
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v3, Lcom/squareup/widgets/R$id;->customPanel:I

    invoke-virtual {v0, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 348
    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v5, Lcom/squareup/widgets/R$id;->custom:I

    invoke-virtual {v3, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    .line 349
    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    iget-boolean v5, v5, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->matchParent:Z

    if-eqz v5, :cond_1

    .line 350
    iget-object v5, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->view:Landroid/view/View;

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v6, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 354
    :cond_1
    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const v5, 0x1020002

    invoke-virtual {v3, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 355
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 356
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 357
    iget-object v6, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->dialogContentLayout:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    iget-boolean v6, v6, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->wrapContent:Z

    if-eqz v6, :cond_2

    .line 358
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    :cond_2
    invoke-virtual {v3, v5}, Landroid/view/View;->setMinimumWidth(I)V

    .line 362
    iget-object v3, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    if-eqz v3, :cond_4

    .line 363
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v5, Lcom/squareup/widgets/R$id;->customPanel:I

    invoke-virtual {v0, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    :goto_0
    if-eqz v1, :cond_7

    .line 372
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->view:Landroid/view/View;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_5

    goto :goto_1

    .line 375
    :cond_5
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v1, Lcom/squareup/widgets/R$id;->titleDividerTop:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 373
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    sget v1, Lcom/squareup/widgets/R$id;->titleDivider:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_7

    .line 379
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 383
    :cond_7
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->adapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_8

    .line 384
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 385
    iget v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->checkedItem:I

    if-le v0, v4, :cond_8

    .line 386
    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 387
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->checkedItem:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_8
    return-void
.end method


# virtual methods
.method public getButton(I)Landroid/widget/Button;
    .locals 1

    const/4 v0, -0x3

    if-eq p1, v0, :cond_2

    const/4 v0, -0x2

    if-eq p1, v0, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 296
    :cond_0
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositive:Landroid/widget/Button;

    return-object p1

    .line 298
    :cond_1
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegative:Landroid/widget/Button;

    return-object p1

    .line 300
    :cond_2
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutral:Landroid/widget/Button;

    return-object p1
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->listView:Landroid/widget/ListView;

    return-object v0
.end method

.method public getMessage()Ljava/lang/CharSequence;
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->message:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public installContent()V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 171
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->view:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->canTextInput(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->window:Landroid/view/Window;

    iget v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->alertDialogLayout:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setContentView(I)V

    .line 177
    invoke-direct {p0}, Lcom/squareup/widgets/dialog/internal/AlertController;->setupView()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 316
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->scrollView:Landroid/widget/ScrollView;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 321
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->scrollView:Landroid/widget/ScrollView;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;Z)V
    .locals 0

    if-nez p4, :cond_0

    if-eqz p3, :cond_0

    .line 237
    iget-object p4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->handler:Landroid/os/Handler;

    invoke-virtual {p4, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p4

    :cond_0
    if-eqz p5, :cond_1

    .line 241
    iput p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->primaryButton:I

    :cond_1
    const/4 p3, -0x3

    if-eq p1, p3, :cond_4

    const/4 p3, -0x2

    if-eq p1, p3, :cond_3

    const/4 p3, -0x1

    if-ne p1, p3, :cond_2

    .line 247
    iput-object p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositiveText:Ljava/lang/CharSequence;

    .line 248
    iput-object p4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonPositiveMessage:Landroid/os/Message;

    goto :goto_0

    .line 262
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Button does not exist"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 252
    :cond_3
    iput-object p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegativeText:Ljava/lang/CharSequence;

    .line 253
    iput-object p4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNegativeMessage:Landroid/os/Message;

    goto :goto_0

    .line 257
    :cond_4
    iput-object p2, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutralText:Ljava/lang/CharSequence;

    .line 258
    iput-object p4, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->buttonNeutralMessage:Landroid/os/Message;

    :goto_0
    return-void
.end method

.method public setIcon(I)V
    .locals 1

    .line 272
    iput p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconId:I

    .line 273
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    if-lez p1, :cond_0

    .line 275
    iget p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconId:I

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    const/16 p1, 0x8

    .line 277
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 283
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->icon:Landroid/graphics/drawable/Drawable;

    .line 284
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->iconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 285
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 195
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->message:Ljava/lang/CharSequence;

    .line 196
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setMessageContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .line 202
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageContentDescription:Ljava/lang/CharSequence;

    .line 203
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->messageView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setPositiveButtonBackground(I)V
    .locals 0

    .line 217
    iput p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->positiveButtonBackground:I

    return-void
.end method

.method public setPositiveButtonTextColor(I)V
    .locals 0

    .line 213
    iput p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->positiveButtonTextColor:I

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 181
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->title:Ljava/lang/CharSequence;

    .line 182
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setTitleContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .line 188
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleContentDescription:Ljava/lang/CharSequence;

    .line 189
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->titleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setVerticalButtonOrientation()V
    .locals 1

    const/4 v0, 0x1

    .line 209
    iput-boolean v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController;->verticalButtonOrientation:Z

    return-void
.end method
