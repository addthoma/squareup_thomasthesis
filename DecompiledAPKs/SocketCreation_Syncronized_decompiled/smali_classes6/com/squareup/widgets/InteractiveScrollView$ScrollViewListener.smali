.class public interface abstract Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;
.super Ljava/lang/Object;
.source "InteractiveScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/InteractiveScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScrollViewListener"
.end annotation


# virtual methods
.method public abstract onScrollTopNotReached()V
.end method

.method public abstract onScrollTopReached()V
.end method
