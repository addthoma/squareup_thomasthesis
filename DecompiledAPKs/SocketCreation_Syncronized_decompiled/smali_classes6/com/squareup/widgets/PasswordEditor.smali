.class public Lcom/squareup/widgets/PasswordEditor;
.super Landroidx/appcompat/widget/AppCompatEditText;
.source "PasswordEditor.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/widgets/PasswordEditor;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/widgets/PasswordEditor;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/widgets/PasswordEditor;->init()V

    return-void
.end method

.method private init()V
    .locals 1

    const/16 v0, 0x81

    .line 34
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PasswordEditor;->setInputType(I)V

    .line 35
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PasswordEditor;->setTypeface(Landroid/graphics/Typeface;)V

    const/4 v0, 0x1

    .line 37
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PasswordEditor;->setHorizontallyScrolling(Z)V

    .line 38
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PasswordEditor;->setHorizontalFadingEdgeEnabled(Z)V

    return-void
.end method
