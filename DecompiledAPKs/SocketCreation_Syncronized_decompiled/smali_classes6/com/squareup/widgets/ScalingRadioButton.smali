.class public Lcom/squareup/widgets/ScalingRadioButton;
.super Lcom/squareup/marketfont/MarketRadioButton;
.source "ScalingRadioButton.java"


# instance fields
.field private availableWidth:I

.field private dirty:Z

.field private includeFontPadding:Z

.field private layout:Landroid/text/Layout;

.field private maxTextSize:F

.field private minTextSize:F

.field private textPaint:Landroid/text/TextPaint;

.field private yOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/ScalingRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x101007e

    .line 53
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/ScalingRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->dirty:Z

    .line 59
    sget-object v1, Lcom/squareup/widgets/R$styleable;->ScalingRadioButton:[I

    const/4 v2, 0x0

    .line 60
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 62
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextSize()F

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/ScalingRadioButton;->maxTextSize:F

    .line 63
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object p2

    iget p3, p0, Lcom/squareup/widgets/ScalingRadioButton;->maxTextSize:F

    invoke-virtual {p2, p3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 64
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object p2

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTypeface()Landroid/graphics/Typeface;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 66
    sget p2, Lcom/squareup/widgets/R$styleable;->ScalingRadioButton_minTextSize:I

    const/high16 p3, 0x41800000    # 16.0f

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/ScalingRadioButton;->minTextSize:F

    .line 67
    sget p2, Lcom/squareup/widgets/R$styleable;->ScalingRadioButton_android_includeFontPadding:I

    .line 68
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/widgets/ScalingRadioButton;->includeFontPadding:Z

    .line 70
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private autoFit()V
    .locals 7

    .line 136
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingLeft()I

    move-result v0

    .line 137
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingRight()I

    move-result v1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/squareup/widgets/ScalingRadioButton;->availableWidth:I

    .line 140
    iget-object v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/squareup/widgets/ScalingRadioButton;->availableWidth:I

    iget v3, p0, Lcom/squareup/widgets/ScalingRadioButton;->minTextSize:F

    iget v4, p0, Lcom/squareup/widgets/ScalingRadioButton;->maxTextSize:F

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFF)V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v1

    iget v2, p0, Lcom/squareup/widgets/ScalingRadioButton;->availableWidth:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    iget v1, p0, Lcom/squareup/widgets/ScalingRadioButton;->availableWidth:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/widgets/ScalingRadioButton;->createLayout(Ljava/lang/String;I)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->layout:Landroid/text/Layout;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingTop()I

    move-result v0

    .line 149
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingBottom()I

    move-result v1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getHeight()I

    move-result v2

    sub-int v3, v2, v0

    sub-int/2addr v3, v1

    .line 152
    iget-object v4, p0, Lcom/squareup/widgets/ScalingRadioButton;->layout:Landroid/text/Layout;

    invoke-virtual {v4}, Landroid/text/Layout;->getHeight()I

    move-result v4

    .line 154
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getGravity()I

    move-result v5

    and-int/lit8 v5, v5, 0x70

    const/16 v6, 0x10

    if-eq v5, v6, :cond_1

    const/16 v3, 0x50

    if-eq v5, v3, :cond_0

    int-to-float v0, v0

    .line 164
    iput v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->yOffset:F

    goto :goto_0

    :cond_0
    sub-int/2addr v2, v1

    sub-int/2addr v2, v4

    int-to-float v0, v2

    .line 160
    iput v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->yOffset:F

    goto :goto_0

    :cond_1
    sub-int/2addr v3, v4

    shr-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    int-to-float v0, v0

    .line 157
    iput v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->yOffset:F

    :goto_0
    const/4 v0, 0x0

    .line 167
    iput-boolean v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->dirty:Z

    return-void
.end method

.method private createLayout(Ljava/lang/String;I)Landroid/text/Layout;
    .locals 9

    .line 174
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 183
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    :goto_0
    move-object v4, v0

    goto :goto_1

    .line 180
    :cond_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 177
    :cond_1
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 186
    :goto_1
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;

    move-result-object v0

    if-nez v0, :cond_2

    .line 190
    new-instance v0, Landroid/text/BoringLayout$Metrics;

    invoke-direct {v0}, Landroid/text/BoringLayout$Metrics;-><init>()V

    .line 193
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    :cond_2
    move-object v7, v0

    .line 196
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    const/4 v0, 0x0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    iget-boolean v8, p0, Lcom/squareup/widgets/ScalingRadioButton;->includeFontPadding:Z

    move-object v1, p1

    invoke-static/range {v1 .. v8}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    move-result-object p1

    return-object p1
.end method

.method private getTextPaint()Landroid/text/TextPaint;
    .locals 1

    .line 228
    iget-object v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->textPaint:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->textPaint:Landroid/text/TextPaint;

    .line 230
    iget-object v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->textPaint:Landroid/text/TextPaint;

    invoke-static {v0}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->textPaint:Landroid/text/TextPaint;

    return-object v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .line 118
    iget-boolean v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->dirty:Z

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->autoFit()V

    .line 122
    :cond_0
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 124
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 125
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/squareup/widgets/ScalingRadioButton;->yOffset:F

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 126
    iget-object v1, p0, Lcom/squareup/widgets/ScalingRadioButton;->layout:Landroid/text/Layout;

    invoke-virtual {v1, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 128
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    const/4 v0, 0x1

    .line 83
    iput-boolean v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->dirty:Z

    .line 85
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 86
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 87
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 88
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 90
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingLeft()I

    move-result v4

    .line 91
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingRight()I

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    if-ne v0, v6, :cond_0

    move v0, v1

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    if-ne v2, v6, :cond_1

    goto :goto_1

    .line 106
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sub-int/2addr v1, v4

    sub-int/2addr v1, v5

    invoke-direct {p0, v2, v1}, Lcom/squareup/widgets/ScalingRadioButton;->createLayout(Ljava/lang/String;I)Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getPaddingBottom()I

    move-result v1

    add-int/2addr v2, v1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 111
    :goto_1
    invoke-static {v0, p1}, Lcom/squareup/widgets/ScalingRadioButton;->resolveSize(II)I

    move-result p1

    .line 112
    invoke-static {v3, p2}, Lcom/squareup/widgets/ScalingRadioButton;->resolveSize(II)I

    move-result p2

    .line 111
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/ScalingRadioButton;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 237
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/marketfont/MarketRadioButton;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 240
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->requestLayout()V

    .line 241
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->invalidate()V

    return-void
.end method

.method public setGravity(I)V
    .locals 0

    .line 202
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketRadioButton;->setGravity(I)V

    .line 204
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->requestLayout()V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->invalidate()V

    return-void
.end method

.method public setShadowLayer(FFFI)V
    .locals 1

    .line 75
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/marketfont/MarketRadioButton;->setShadowLayer(FFFI)V

    .line 77
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->invalidate()V

    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .line 218
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketRadioButton;->setTextSize(F)V

    .line 219
    iput p1, p0, Lcom/squareup/widgets/ScalingRadioButton;->maxTextSize:F

    .line 220
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object p1

    iget v0, p0, Lcom/squareup/widgets/ScalingRadioButton;->maxTextSize:F

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    const/4 p1, 0x1

    .line 221
    iput-boolean p1, p0, Lcom/squareup/widgets/ScalingRadioButton;->dirty:Z

    .line 223
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->requestLayout()V

    .line 224
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->invalidate()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .line 210
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketRadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 212
    invoke-direct {p0}, Lcom/squareup/widgets/ScalingRadioButton;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 213
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->requestLayout()V

    .line 214
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingRadioButton;->invalidate()V

    return-void
.end method
