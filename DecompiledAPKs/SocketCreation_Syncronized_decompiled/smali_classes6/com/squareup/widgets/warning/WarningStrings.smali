.class public Lcom/squareup/widgets/warning/WarningStrings;
.super Ljava/lang/Object;
.source "WarningStrings.java"

# interfaces
.implements Lcom/squareup/widgets/warning/Warning;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/widgets/warning/WarningStrings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final message:Ljava/lang/String;

.field final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings$1;

    invoke-direct {v0}, Lcom/squareup/widgets/warning/WarningStrings$1;-><init>()V

    sput-object v0, Lcom/squareup/widgets/warning/WarningStrings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/widgets/warning/WarningStrings;->title:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/squareup/widgets/warning/WarningStrings;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 21
    :cond_1
    check-cast p1, Lcom/squareup/widgets/warning/WarningStrings;

    .line 23
    iget-object v1, p0, Lcom/squareup/widgets/warning/WarningStrings;->title:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/widgets/warning/WarningStrings;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    return v0

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/squareup/widgets/warning/WarningStrings;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/widgets/warning/WarningStrings;->message:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1

    :cond_3
    :goto_0
    return v0
.end method

.method public getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;
    .locals 2

    .line 32
    new-instance p1, Lcom/squareup/widgets/warning/Warning$Strings;

    iget-object v0, p0, Lcom/squareup/widgets/warning/WarningStrings;->title:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/widgets/warning/WarningStrings;->message:Ljava/lang/String;

    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/Warning$Strings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 28
    iget-object v1, p0, Lcom/squareup/widgets/warning/WarningStrings;->title:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/widgets/warning/WarningStrings;->message:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 40
    iget-object p2, p0, Lcom/squareup/widgets/warning/WarningStrings;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object p2, p0, Lcom/squareup/widgets/warning/WarningStrings;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
