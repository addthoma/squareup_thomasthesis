.class Lcom/squareup/widgets/glass/GlassConfirmView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "GlassConfirmView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/widgets/glass/GlassConfirmView;-><init>(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/glass/GlassConfirmView;

.field final synthetic val$cancelListener:Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/glass/GlassConfirmView;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmView$1;->this$0:Lcom/squareup/widgets/glass/GlassConfirmView;

    iput-object p2, p0, Lcom/squareup/widgets/glass/GlassConfirmView$1;->val$cancelListener:Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 71
    iget-object p1, p0, Lcom/squareup/widgets/glass/GlassConfirmView$1;->val$cancelListener:Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;

    invoke-interface {p1}, Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;->onCancel()V

    return-void
.end method
