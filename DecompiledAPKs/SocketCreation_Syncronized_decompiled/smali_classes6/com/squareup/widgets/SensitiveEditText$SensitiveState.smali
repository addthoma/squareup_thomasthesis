.class Lcom/squareup/widgets/SensitiveEditText$SensitiveState;
.super Ljava/lang/Object;
.source "SensitiveEditText.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/SensitiveEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SensitiveState"
.end annotation


# instance fields
.field private final transient superState:Landroid/os/Parcelable;


# direct methods
.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/widgets/SensitiveEditText$SensitiveState;->superState:Landroid/os/Parcelable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/SensitiveEditText$SensitiveState;)Landroid/os/Parcelable;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/widgets/SensitiveEditText$SensitiveState;->superState:Landroid/os/Parcelable;

    return-object p0
.end method
