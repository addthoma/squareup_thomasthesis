.class public Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;
.super Lcom/squareup/widgets/SelectableAutoCompleteEditText;
.source "OnScreenRectangleAutoCompleteEditText.java"


# static fields
.field private static final SCROLL_MARGIN_BOTTOM:I = 0x3


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private adjustRect(Landroid/graphics/Rect;)V
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method


# virtual methods
.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 0

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->adjustRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public getOffsetForPosition(FF)I
    .locals 0

    .line 39
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getOffsetForPosition(FF)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->adjustRect(Landroid/graphics/Rect;)V

    .line 45
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    move-result p1

    return p1
.end method
