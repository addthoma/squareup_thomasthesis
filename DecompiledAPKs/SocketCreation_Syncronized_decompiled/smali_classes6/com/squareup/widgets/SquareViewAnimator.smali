.class public Lcom/squareup/widgets/SquareViewAnimator;
.super Landroid/widget/ViewAnimator;
.source "SquareViewAnimator.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewAnimator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public animateTo(Landroid/view/View;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .locals 0

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/widgets/SquareViewAnimator;->animateToById(ILandroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    return-void
.end method

.method public animateToById(ILandroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .locals 0

    .line 67
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 68
    invoke-virtual {p0, p3}, Lcom/squareup/widgets/SquareViewAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public getDisplayedChildId()I
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/widgets/SquareViewAnimator;->getDisplayedChildView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    return v0
.end method

.method public getDisplayedChildView()Landroid/view/View;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/widgets/SquareViewAnimator;->getDisplayedChild()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SquareViewAnimator;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayedChild(I)V
    .locals 1

    if-ltz p1, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/widgets/SquareViewAnimator;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 30
    invoke-super {p0, p1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    return-void

    .line 27
    :cond_0
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    const-string v0, "Must pass a child index, not an id."

    invoke-direct {p1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDisplayedChildById(I)V
    .locals 3

    .line 40
    invoke-virtual {p0}, Lcom/squareup/widgets/SquareViewAnimator;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 41
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SquareViewAnimator;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 42
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/widgets/SquareViewAnimator;->getDisplayedChild()I

    move-result p1

    if-eq v0, p1, :cond_0

    .line 44
    invoke-super {p0, v0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 49
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "View with ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " not found."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
