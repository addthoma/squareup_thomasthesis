.class public Lcom/squareup/widgets/EmailSuggestionHandler;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EmailSuggestionHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;
    }
.end annotation


# static fields
.field private static suggestedEmail:Ljava/lang/String;


# instance fields
.field private emailField:Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;

.field emailSuggestion:Landroid/widget/TextView;

.field private emailSuggestionPhrase:Lcom/squareup/phrase/Phrase;

.field private worldEnabled:Z


# direct methods
.method private constructor <init>(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailField:Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;

    .line 57
    iput-object p2, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestion:Landroid/widget/TextView;

    .line 58
    iget-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestion:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    sget p1, Lcom/squareup/widgets/R$string;->email_suggestion:I

    invoke-static {p3, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestionPhrase:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/widgets/EmailSuggestionHandler;->suggestedEmail:Ljava/lang/String;

    return-object v0
.end method

.method public static wireEmailSuggestions(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)Lcom/squareup/widgets/EmailSuggestionHandler;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/widgets/EmailSuggestionHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/widgets/EmailSuggestionHandler;-><init>(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)V

    .line 45
    invoke-interface {p0, v0}, Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    new-instance p2, Lcom/squareup/widgets/EmailSuggestionHandler$1;

    invoke-direct {p2, p0}, Lcom/squareup/widgets/EmailSuggestionHandler$1;-><init>(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;)V

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 68
    iget-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailField:Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;

    invoke-interface {p1}, Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-boolean v0, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->worldEnabled:Z

    invoke-static {p1, v0}, Lcom/squareup/util/MailCheck;->containsTld(Ljava/lang/String;Z)Z

    move-result p1

    const/16 v0, 0x8

    if-nez p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestion:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailField:Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;

    invoke-interface {p1}, Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-boolean v1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->worldEnabled:Z

    invoke-static {p1, v1}, Lcom/squareup/util/MailCheck;->check(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/squareup/widgets/EmailSuggestionHandler;->suggestedEmail:Ljava/lang/String;

    .line 75
    sget-object p1, Lcom/squareup/widgets/EmailSuggestionHandler;->suggestedEmail:Ljava/lang/String;

    if-nez p1, :cond_1

    .line 76
    iget-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestion:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestion:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestionPhrase:Lcom/squareup/phrase/Phrase;

    const-string v2, "email"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->emailSuggestion:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public enableWorld(Z)V
    .locals 0

    .line 64
    iput-boolean p1, p0, Lcom/squareup/widgets/EmailSuggestionHandler;->worldEnabled:Z

    return-void
.end method
