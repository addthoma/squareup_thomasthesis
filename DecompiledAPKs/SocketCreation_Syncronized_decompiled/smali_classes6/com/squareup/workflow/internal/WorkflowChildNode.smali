.class public final Lcom/squareup/workflow/internal/WorkflowChildNode;
.super Ljava/lang/Object;
.source "WorkflowChildNode.kt"

# interfaces
.implements Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ChildPropsT:",
        "Ljava/lang/Object;",
        "ChildOutputT:",
        "Ljava/lang/Object;",
        "ParentStateT:",
        "Ljava/lang/Object;",
        "ParentOutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode<",
        "Lcom/squareup/workflow/internal/WorkflowChildNode<",
        "****>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowChildNode.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowChildNode.kt\ncom/squareup/workflow/internal/WorkflowChildNode\n*L\n1#1,84:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u0003*\u0004\u0008\u0002\u0010\u0004*\u0008\u0008\u0003\u0010\u0005*\u00020\u00032\u0018\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00000\u0006BW\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0001\u0012\u0002\u0008\u00030\u0008\u0012\u001e\u0010\t\u001a\u001a\u0012\u0004\u0012\u00028\u0001\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b0\n\u0012\u001a\u0010\u000c\u001a\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0001\u0012\u0002\u0008\u00030\r\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b2\u0006\u0010\u001d\u001a\u00020\u0003J\"\u0010\u001e\u001a\u00020\u001f2\u0012\u0010 \u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00082\u0006\u0010!\u001a\u00020\"J3\u0010#\u001a\u0002H$\"\u0004\u0008\u0004\u0010$2\u0016\u0010\u0007\u001a\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030%2\u0008\u0010&\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\'J<\u0010(\u001a\u00020)\"\u0004\u0008\u0004\u0010*\"\u0004\u0008\u0005\u0010+\"\u0008\u0008\u0006\u0010,*\u00020\u00032\u001e\u0010-\u001a\u001a\u0012\u0004\u0012\u0002H*\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H+\u0012\u0004\u0012\u0002H,0\u000b0\nR&\u0010\t\u001a\u001a\u0012\u0004\u0012\u00028\u0001\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000b0\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R%\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u00108F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R,\u0010\u0013\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0000X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R\u001f\u0010\u0007\u001a\u0010\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0001\u0012\u0002\u0008\u00030\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R%\u0010\u000c\u001a\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0001\u0012\u0002\u0008\u00030\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/workflow/internal/WorkflowChildNode;",
        "ChildPropsT",
        "ChildOutputT",
        "",
        "ParentStateT",
        "ParentOutputT",
        "Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "handler",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "workflowNode",
        "Lcom/squareup/workflow/internal/WorkflowNode;",
        "(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/internal/WorkflowNode;)V",
        "id",
        "Lcom/squareup/workflow/internal/WorkflowId;",
        "getId",
        "()Lcom/squareup/workflow/internal/WorkflowId;",
        "nextListNode",
        "getNextListNode",
        "()Lcom/squareup/workflow/internal/WorkflowChildNode;",
        "setNextListNode",
        "(Lcom/squareup/workflow/internal/WorkflowChildNode;)V",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "getWorkflowNode",
        "()Lcom/squareup/workflow/internal/WorkflowNode;",
        "acceptChildOutput",
        "output",
        "matches",
        "",
        "otherWorkflow",
        "key",
        "",
        "render",
        "R",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "props",
        "(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;",
        "setHandler",
        "",
        "CO",
        "S",
        "O",
        "newHandler",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private handler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TChildOutputT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TParentStateT;+TParentOutputT;>;>;"
        }
    .end annotation
.end field

.field private nextListNode:Lcom/squareup/workflow/internal/WorkflowChildNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/WorkflowChildNode<",
            "****>;"
        }
    .end annotation
.end field

.field private final workflow:Lcom/squareup/workflow/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Workflow<",
            "*TChildOutputT;*>;"
        }
    .end annotation
.end field

.field private final workflowNode:Lcom/squareup/workflow/internal/WorkflowNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/WorkflowNode<",
            "TChildPropsT;*TChildOutputT;*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/internal/WorkflowNode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "*+TChildOutputT;*>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TChildOutputT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TParentStateT;+TParentOutputT;>;>;",
            "Lcom/squareup/workflow/internal/WorkflowNode<",
            "TChildPropsT;*TChildOutputT;*>;)V"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflowNode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->workflow:Lcom/squareup/workflow/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->handler:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->workflowNode:Lcom/squareup/workflow/internal/WorkflowNode;

    return-void
.end method


# virtual methods
.method public final acceptChildOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TParentStateT;TParentOutputT;>;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->handler:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    return-object p1
.end method

.method public final getId()Lcom/squareup/workflow/internal/WorkflowId;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "TChildPropsT;TChildOutputT;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->workflowNode:Lcom/squareup/workflow/internal/WorkflowNode;

    invoke-virtual {v0}, Lcom/squareup/workflow/internal/WorkflowNode;->getId()Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getNextListNode()Lcom/squareup/workflow/internal/WorkflowChildNode;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    return-object v0
.end method

.method public getNextListNode()Lcom/squareup/workflow/internal/WorkflowChildNode;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/internal/WorkflowChildNode<",
            "****>;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->nextListNode:Lcom/squareup/workflow/internal/WorkflowChildNode;

    return-object v0
.end method

.method public final getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "*TChildOutputT;*>;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->workflow:Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public final getWorkflowNode()Lcom/squareup/workflow/internal/WorkflowNode;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/internal/WorkflowNode<",
            "TChildPropsT;*TChildOutputT;*>;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->workflowNode:Lcom/squareup/workflow/internal/WorkflowNode;

    return-object v0
.end method

.method public final matches(Lcom/squareup/workflow/Workflow;Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Workflow<",
            "***>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const-string v0, "otherWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/workflow/internal/WorkflowChildNode;->getId()Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/internal/WorkflowId;->component1$workflow_runtime()Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/workflow/internal/WorkflowId;->component2$workflow_runtime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final render(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "****>;",
            "Ljava/lang/Object;",
            ")TR;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->workflowNode:Lcom/squareup/workflow/internal/WorkflowNode;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/workflow/internal/WorkflowNode;->render(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final setHandler(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<CO:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TCO;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TS;+TO;>;>;)V"
        }
    .end annotation

    const-string v0, "newHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 60
    invoke-static {p1, v0}, Lkotlin/jvm/internal/TypeIntrinsics;->beforeCheckcastToFunctionOfArity(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->handler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public bridge synthetic setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/workflow/internal/WorkflowChildNode;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/internal/WorkflowChildNode;->setNextListNode(Lcom/squareup/workflow/internal/WorkflowChildNode;)V

    return-void
.end method

.method public setNextListNode(Lcom/squareup/workflow/internal/WorkflowChildNode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/internal/WorkflowChildNode<",
            "****>;)V"
        }
    .end annotation

    .line 42
    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkflowChildNode;->nextListNode:Lcom/squareup/workflow/internal/WorkflowChildNode;

    return-void
.end method
