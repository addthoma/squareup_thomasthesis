.class final Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowLoop.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "TPropsT;",
        "Lkotlin/coroutines/Continuation;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0000\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u0001H\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "newInput",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$1$3$1",
        "com/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$$special$$inlined$select$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic $inputsChannel$inlined:Lkotlinx/coroutines/channels/ReceiveChannel;

.field final synthetic $inputsClosed$inlined:Lkotlin/jvm/internal/Ref$BooleanRef;

.field final synthetic $rootNode$inlined:Lcom/squareup/workflow/internal/WorkflowNode;

.field final synthetic $this_coroutineScope$inlined:Lkotlinx/coroutines/CoroutineScope;

.field label:I

.field private p$0:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;Lkotlin/jvm/internal/Ref$BooleanRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/ReceiveChannel;)V
    .locals 0

    iput-object p2, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$inputsClosed$inlined:Lkotlin/jvm/internal/Ref$BooleanRef;

    iput-object p3, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$input$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p4, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$rootNode$inlined:Lcom/squareup/workflow/internal/WorkflowNode;

    iput-object p5, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    iput-object p6, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$this_coroutineScope$inlined:Lkotlinx/coroutines/CoroutineScope;

    iput-object p7, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$inputsChannel$inlined:Lkotlinx/coroutines/channels/ReceiveChannel;

    const/4 p2, 0x2

    invoke-direct {p0, p2, p1}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;

    iget-object v3, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$inputsClosed$inlined:Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-object v4, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$input$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v5, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$rootNode$inlined:Lcom/squareup/workflow/internal/WorkflowNode;

    iget-object v6, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    iget-object v7, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$this_coroutineScope$inlined:Lkotlinx/coroutines/CoroutineScope;

    iget-object v8, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$inputsChannel$inlined:Lkotlinx/coroutines/channels/ReceiveChannel;

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lkotlin/jvm/internal/Ref$BooleanRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/ReceiveChannel;)V

    iput-object p1, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->p$0:Ljava/lang/Object;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 115
    iget v0, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->label:I

    if-nez v0, :cond_2

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->p$0:Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->this$0:Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;

    iget-object v1, v0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$input$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v3, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v4, p1

    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onPropsChanged(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    if-nez p1, :cond_1

    .line 118
    iget-object p1, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$inputsClosed$inlined:Lkotlin/jvm/internal/Ref$BooleanRef;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/squareup/workflow/internal/RealWorkflowLoop$runWorkflowLoop$2$invokeSuspend$$inlined$consume$lambda$1;->$input$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    :goto_0
    const/4 p1, 0x0

    return-object p1

    .line 124
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
