.class public interface abstract Lcom/squareup/workflow/WorkflowAction;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/WorkflowAction$Mutator;,
        Lcom/squareup/workflow/WorkflowAction$Updater;,
        Lcom/squareup/workflow/WorkflowAction$DefaultImpls;,
        Lcom/squareup/workflow/WorkflowAction$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \t*\u0004\u0008\u0000\u0010\u0001*\n\u0008\u0001\u0010\u0002 \u0001*\u00020\u00032\u00020\u0003:\u0003\t\n\u000bJ\u0019\u0010\u0004\u001a\u0004\u0018\u00018\u0001*\u0008\u0012\u0004\u0012\u00028\u00000\u0005H\u0017\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0004\u001a\u00020\u0007*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0008H\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/workflow/WorkflowAction;",
        "StateT",
        "OutputT",
        "",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Companion",
        "Mutator",
        "Updater",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/WorkflowAction$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction$Companion;->$$INSTANCE:Lcom/squareup/workflow/WorkflowAction$Companion;

    sput-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    return-void
.end method


# virtual methods
.method public abstract apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "TStateT;>;)TOutputT;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation
.end method

.method public abstract apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "TStateT;-TOutputT;>;)V"
        }
    .end annotation
.end method
