.class public final Lcom/squareup/workflow/WorkflowActionKt;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowAction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,211:1\n199#1,4:212\n181#1:216\n199#1,4:217\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n181#1,4:212\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\\\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0008\u0004\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062%\u0008\u0004\u0010\u0008\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0002\u0008\u000cH\u0086\u0008\u001aV\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00072%\u0008\u0004\u0010\u0008\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0002\u0008\u000cH\u0086\u0008\u001aA\u0010\r\u001a\u0010\u0012\u0004\u0012\u0002H\u0002\u0012\u0006\u0012\u0004\u0018\u0001H\u00030\u000e\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0004*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00012\u0006\u0010\u000f\u001a\u0002H\u0002\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "StateT",
        "OutputT",
        "",
        "name",
        "Lkotlin/Function0;",
        "",
        "apply",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "applyTo",
        "Lkotlin/Pair;",
        "state",
        "(Lcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;)Lkotlin/Pair;",
        "workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final action(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "TStateT;-TOutputT;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    new-instance v0, Lcom/squareup/workflow/WorkflowActionKt$action$$inlined$action$1;

    invoke-direct {v0, p1, p0}, Lcom/squareup/workflow/WorkflowActionKt$action$$inlined$action$1;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public static final action(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "TStateT;-TOutputT;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    new-instance v0, Lcom/squareup/workflow/WorkflowActionKt$action$2;

    invoke-direct {v0, p1, p0}, Lcom/squareup/workflow/WorkflowActionKt$action$2;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public static synthetic action$default(Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p0, ""

    :cond_0
    const-string p2, "name"

    .line 179
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "apply"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    new-instance p2, Lcom/squareup/workflow/WorkflowActionKt$action$$inlined$action$2;

    invoke-direct {p2, p1, p0}, Lcom/squareup/workflow/WorkflowActionKt$action$$inlined$action$2;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/workflow/WorkflowAction;

    return-object p2
.end method

.method public static final applyTo(Lcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;)Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;TStateT;)",
            "Lkotlin/Pair<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$applyTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/WorkflowAction$Updater;-><init>(Ljava/lang/Object;)V

    .line 208
    invoke-interface {p0, v0}, Lcom/squareup/workflow/WorkflowAction;->apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    .line 209
    new-instance p0, Lkotlin/Pair;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->getOutput$workflow_core()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0
.end method
