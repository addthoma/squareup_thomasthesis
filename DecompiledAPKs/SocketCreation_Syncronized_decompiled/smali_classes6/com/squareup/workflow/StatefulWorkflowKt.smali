.class public final Lcom/squareup/workflow/StatefulWorkflowKt;
.super Ljava/lang/Object;
.source "StatefulWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStatefulWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StatefulWorkflow.kt\ncom/squareup/workflow/StatefulWorkflowKt\n*L\n1#1,295:1\n167#1:296\n186#1:297\n165#1,3:298\n186#1:301\n167#1:302\n186#1:303\n214#1,5:304\n167#1:309\n186#1:310\n219#1:311\n213#1,6:312\n167#1:318\n186#1:319\n219#1:320\n*E\n*S KotlinDebug\n*F\n+ 1 StatefulWorkflow.kt\ncom/squareup/workflow/StatefulWorkflowKt\n*L\n195#1,3:298\n195#1:301\n214#1:302\n214#1:303\n229#1,6:312\n229#1:318\n229#1:319\n229#1:320\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u001a}\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u001a\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2#\u0010\u000b\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000f\u001ay\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u001a\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\n2#\u0010\u000b\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000f\u001a\u0086\u0002\u0010\u0010\u001a)\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0012\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0007\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u00020\u00132\u0014\u0008\u0004\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00020\u000c2O\u0008\u0004\u0010\u0015\u001aI\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0017\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0012\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00060\u0016\u00a2\u0006\u0002\u0008\u000f2M\u0008\u0006\u0010\u0019\u001aG\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001a\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001b\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00020\u0016H\u0086\u0008\u001a\u0095\u0002\u0010\u0010\u001a\u001a\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0007\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u00020\u00132\u001c\u0008\u0004\u0010\u0014\u001a\u0016\u0012\u0004\u0012\u0002H\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u001d\u0012\u0004\u0012\u0002H\u00020\u001c2O\u0008\u0004\u0010\u0015\u001aI\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0017\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0012\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00060\u0016\u00a2\u0006\u0002\u0008\u000f2\u0014\u0008\u0004\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u001d0\u000c2M\u0008\u0006\u0010\u0019\u001aG\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001a\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001b\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00020\u0016H\u0086\u0008\u001a\u00a5\u0001\u0010\u0010\u001a\u001a\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0007\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006*\u00020\u00132\u0016\u0008\u0004\u0010\u0014\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001d\u0012\u0004\u0012\u0002H\u00020\u000c2:\u0008\u0004\u0010\u0015\u001a4\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0017\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00060\u001c\u00a2\u0006\u0002\u0008\u000f2\u0014\u0008\u0004\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u001d0\u000cH\u0086\u0008\u001a\u0084\u0001\u0010\u0010\u001a\u001a\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0007\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006*\u00020\u00132\u0006\u0010\u0014\u001a\u0002H\u00022:\u0008\u0004\u0010\u0015\u001a4\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0017\u0012\u0013\u0012\u0011H\u0002\u00a2\u0006\u000c\u0008\u0011\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u0018\u0012\u0004\u0012\u0002H\u00060\u001c\u00a2\u0006\u0002\u0008\u000fH\u0086\u0008\u00a2\u0006\u0002\u0010\u001f\u001a{\u0010 \u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u001a\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u001f\u0010!\u001a\u001b\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\"\u0012\u0006\u0012\u0004\u0018\u0001H\u00030\u000c\u00a2\u0006\u0002\u0008\u000fH\u0007\u001aw\u0010 \u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006*\u001a\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\n2\u001f\u0010!\u001a\u001b\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\"\u0012\u0006\u0012\u0004\u0018\u0001H\u00030\u000c\u00a2\u0006\u0002\u0008\u000fH\u0007\u00a8\u0006#"
    }
    d2 = {
        "action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "StateT",
        "OutputT",
        "PropsT",
        "",
        "RenderingT",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "name",
        "Lkotlin/Function0;",
        "",
        "update",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "stateful",
        "Lkotlin/ParameterName;",
        "props",
        "Lcom/squareup/workflow/Workflow$Companion;",
        "initialState",
        "render",
        "Lkotlin/Function3;",
        "Lcom/squareup/workflow/RenderContext;",
        "state",
        "onPropsChanged",
        "old",
        "new",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshot",
        "(Lcom/squareup/workflow/Workflow$Companion;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/StatefulWorkflow;",
        "workflowAction",
        "block",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final action(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "TStateT;-TOutputT;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$action"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "update"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    new-instance v0, Lcom/squareup/workflow/StatefulWorkflowKt$action$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action(Lcom/squareup/workflow/StatefulWorkflow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final action(Lcom/squareup/workflow/StatefulWorkflow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "TStateT;-TOutputT;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$action"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "update"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    new-instance v0, Lcom/squareup/workflow/StatefulWorkflowKt$action$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$action$2;-><init>(Lcom/squareup/workflow/StatefulWorkflow;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public static synthetic action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const-string p1, ""

    .line 244
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final stateful(Lcom/squareup/workflow/Workflow$Companion;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow$Companion;",
            "TStateT;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/workflow/RenderContext<",
            "TStateT;-TOutputT;>;-TStateT;+TRenderingT;>;)",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "Lkotlin/Unit;",
            "TStateT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$stateful"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    new-instance p0, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$5;

    invoke-direct {p0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$5;-><init>(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;)V

    check-cast p0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object p0
.end method

.method public static final stateful(Lcom/squareup/workflow/Workflow$Companion;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow$Companion;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/Snapshot;",
            "+TStateT;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/workflow/RenderContext<",
            "TStateT;-TOutputT;>;-TStateT;+TRenderingT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TStateT;",
            "Lcom/squareup/workflow/Snapshot;",
            ">;)",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "Lkotlin/Unit;",
            "TStateT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$stateful"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "initialState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "snapshot"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    new-instance p0, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$1;

    invoke-direct {p0, p3, p1, p2}, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    check-cast p0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object p0
.end method

.method public static final stateful(Lcom/squareup/workflow/Workflow$Companion;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;)Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow$Companion;",
            "Lkotlin/jvm/functions/Function1<",
            "-TPropsT;+TStateT;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/squareup/workflow/RenderContext<",
            "TStateT;-TOutputT;>;-TPropsT;-TStateT;+TRenderingT;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-TPropsT;-TPropsT;-TStateT;+TStateT;>;)",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "TPropsT;TStateT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$stateful"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "initialState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "onPropsChanged"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    new-instance p0, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$2;

    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$2;-><init>(Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V

    check-cast p0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object p0
.end method

.method public static final stateful(Lcom/squareup/workflow/Workflow$Companion;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;)Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow$Companion;",
            "Lkotlin/jvm/functions/Function2<",
            "-TPropsT;-",
            "Lcom/squareup/workflow/Snapshot;",
            "+TStateT;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/squareup/workflow/RenderContext<",
            "TStateT;-TOutputT;>;-TPropsT;-TStateT;+TRenderingT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TStateT;",
            "Lcom/squareup/workflow/Snapshot;",
            ">;",
            "Lkotlin/jvm/functions/Function3<",
            "-TPropsT;-TPropsT;-TStateT;+TStateT;>;)",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "TPropsT;TStateT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$stateful"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "initialState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "snapshot"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "onPropsChanged"

    invoke-static {p4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    new-instance p0, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$2;

    invoke-direct {p0, p1, p4, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$2;-><init>(Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V

    check-cast p0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object p0
.end method

.method public static synthetic stateful$default(Lcom/squareup/workflow/Workflow$Companion;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;ILjava/lang/Object;)Lcom/squareup/workflow/StatefulWorkflow;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 213
    sget-object p3, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$5;->INSTANCE:Lcom/squareup/workflow/StatefulWorkflowKt$stateful$5;

    check-cast p3, Lkotlin/jvm/functions/Function3;

    :cond_0
    const-string p4, "$this$stateful"

    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "initialState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "onPropsChanged"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    new-instance p0, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$3;

    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$$inlined$stateful$3;-><init>(Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V

    check-cast p0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object p0
.end method

.method public static synthetic stateful$default(Lcom/squareup/workflow/Workflow$Companion;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function3;ILjava/lang/Object;)Lcom/squareup/workflow/StatefulWorkflow;
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    .line 165
    sget-object p4, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$1;->INSTANCE:Lcom/squareup/workflow/StatefulWorkflowKt$stateful$1;

    check-cast p4, Lkotlin/jvm/functions/Function3;

    :cond_0
    const-string p5, "$this$stateful"

    invoke-static {p0, p5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "initialState"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "snapshot"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "onPropsChanged"

    invoke-static {p4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    new-instance p0, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$2;

    invoke-direct {p0, p1, p4, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt$stateful$2;-><init>(Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;)V

    check-cast p0, Lcom/squareup/workflow/StatefulWorkflow;

    return-object p0
.end method

.method public static final workflowAction(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "TStateT;>;+TOutputT;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action(name) { nextState = state }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    const-string v0, "$this$workflowAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    new-instance v0, Lcom/squareup/workflow/StatefulWorkflowKt$workflowAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$workflowAction$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction(Lcom/squareup/workflow/StatefulWorkflow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final workflowAction(Lcom/squareup/workflow/StatefulWorkflow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "TStateT;>;+TOutputT;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action(name) { nextState = state }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    const-string v0, "$this$workflowAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    new-instance v0, Lcom/squareup/workflow/StatefulWorkflowKt$workflowAction$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt$workflowAction$2;-><init>(Lcom/squareup/workflow/StatefulWorkflow;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public static synthetic workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const-string p1, ""

    .line 275
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method
