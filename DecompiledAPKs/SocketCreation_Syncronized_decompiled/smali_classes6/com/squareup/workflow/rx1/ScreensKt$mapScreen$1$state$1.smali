.class final Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1$state$1;
.super Ljava/lang/Object;
.source "Screens.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;-><init>(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0004\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0004\"\u0008\u0008\u0003\u0010\u0006*\u00020\u00042\u001a\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u0002H\u0003 \u0008*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ScreenState;",
        "R2",
        "R1",
        "",
        "E",
        "O",
        "it",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1$state$1;->this$0:Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/ScreenState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ScreenState<",
            "+TR1;>;)",
            "Lcom/squareup/workflow/ScreenState<",
            "TR2;>;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    iget-object v1, p0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1$state$1;->this$0:Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;

    iget-object v1, v1, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;->$mapper:Lkotlin/jvm/functions/Function1;

    iget-object v2, p1, Lcom/squareup/workflow/ScreenState;->screen:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/workflow/ScreenState;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/workflow/ScreenState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1$state$1;->call(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method
