.class public final Lcom/squareup/workflow/rx1/ReactorKt;
.super Ljava/lang/Object;
.source "Reactor.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Reactor.kt\ncom/squareup/workflow/rx1/ReactorKt\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u001aZ\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u00032\u0018\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\tH\u0002\u001aY\u0010\n\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u000c\u001a\u0002H\u0002\u00a2\u0006\u0002\u0010\r\u001aB\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000b\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0003*\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0007\u001aN\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u000f\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0007H\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "callOnAbandonedOnCancellation",
        "",
        "S",
        "",
        "E",
        "O",
        "reactor",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "workflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "startWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "initialState",
        "(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;",
        "toComposedReactor",
        "Lcom/squareup/workflow/rx1/ComposedReactor;",
        "pure-rx1"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final callOnAbandonedOnCancellation(Lcom/squareup/workflow/rx1/Reactor;Lcom/squareup/workflow/legacy/Workflow;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Reactor<",
            "TS;TE;+TO;>;",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;-TE;+TO;>;)V"
        }
    .end annotation

    .line 60
    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v1, 0x0

    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 64
    sget-object v2, Lkotlinx/coroutines/GlobalScope;->INSTANCE:Lkotlinx/coroutines/GlobalScope;

    move-object v3, v2

    check-cast v3, Lkotlinx/coroutines/CoroutineScope;

    sget-object v5, Lkotlinx/coroutines/CoroutineStart;->UNDISPATCHED:Lkotlinx/coroutines/CoroutineStart;

    new-instance v2, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V

    move-object v6, v2

    check-cast v6, Lkotlin/jvm/functions/Function2;

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 71
    new-instance v1, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;

    invoke-direct {v1, v0, p0}, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/squareup/workflow/rx1/Reactor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v1}, Lcom/squareup/workflow/legacy/Workflow;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    return-void
.end method

.method public static final startWorkflow(Lcom/squareup/workflow/rx1/Reactor;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Reactor<",
            "Lkotlin/Unit;",
            "TE;+TO;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lkotlin/Unit;",
            "TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$startWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p0, v0}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Reactor<",
            "TS;TE;+TO;>;TS;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$startWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p0}, Lcom/squareup/workflow/rx1/ReactorKt;->toComposedReactor(Lcom/squareup/workflow/rx1/Reactor;)Lcom/squareup/workflow/rx1/ComposedReactor;

    move-result-object v0

    .line 30
    invoke-static {v0}, Lcom/squareup/workflow/rx1/ComposedReactorKt;->toRx2Reactor(Lcom/squareup/workflow/rx1/ComposedReactor;)Lcom/squareup/workflow/legacy/rx2/Reactor;

    move-result-object v1

    .line 31
    new-instance v3, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v3}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 32
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->callOnAbandonedOnCancellation(Lcom/squareup/workflow/rx1/Reactor;Lcom/squareup/workflow/legacy/Workflow;)V

    .line 33
    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final toComposedReactor(Lcom/squareup/workflow/rx1/Reactor;)Lcom/squareup/workflow/rx1/ComposedReactor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Reactor<",
            "TS;TE;+TO;>;)",
            "Lcom/squareup/workflow/rx1/ComposedReactor<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$toComposedReactor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/workflow/rx1/ReactorKt$toComposedReactor$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/rx1/ReactorKt$toComposedReactor$1;-><init>(Lcom/squareup/workflow/rx1/Reactor;)V

    check-cast v0, Lcom/squareup/workflow/rx1/ComposedReactor;

    return-object v0
.end method
