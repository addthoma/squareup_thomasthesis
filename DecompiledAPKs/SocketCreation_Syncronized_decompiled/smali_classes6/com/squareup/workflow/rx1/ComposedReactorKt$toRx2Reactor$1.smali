.class public final Lcom/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1;
.super Ljava/lang/Object;
.source "ComposedReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/ComposedReactorKt;->toRx2Reactor(Lcom/squareup/workflow/rx1/ComposedReactor;)Lcom/squareup/workflow/legacy/rx2/Reactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "TS;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000/\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J/\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007J?\u0010\u0008\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\n0\t2\u0006\u0010\u000b\u001a\u00028\u00002\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\r2\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "com/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toRx2Reactor:Lcom/squareup/workflow/rx1/ComposedReactor;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx1/ComposedReactor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/ComposedReactor<",
            "TS;TE;+TO;>;)V"
        }
    .end annotation

    .line 36
    iput-object p1, p0, Lcom/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1;->$this_toRx2Reactor:Lcom/squareup/workflow/rx1/ComposedReactor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1;->$this_toRx2Reactor:Lcom/squareup/workflow/rx1/ComposedReactor;

    invoke-interface {v0, p1, p2}, Lcom/squareup/workflow/rx1/ComposedReactor;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "TE;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "TS;TO;>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {p2}, Lcom/squareup/workflow/rx1/EventChannelKt;->asRx1EventChannel(Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lcom/squareup/workflow/rx1/EventChannel;

    move-result-object p2

    .line 48
    iget-object v0, p0, Lcom/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1;->$this_toRx2Reactor:Lcom/squareup/workflow/rx1/ComposedReactor;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/workflow/rx1/ComposedReactor;->onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lrx/Single;

    move-result-object p1

    .line 49
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "toV2Single(reactionSingle)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
