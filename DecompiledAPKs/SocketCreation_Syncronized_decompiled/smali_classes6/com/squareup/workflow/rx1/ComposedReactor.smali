.class public interface abstract Lcom/squareup/workflow/rx1/ComposedReactor;
.super Ljava/lang/Object;
.source "ComposedReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/WorkflowLauncher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/WorkflowLauncher<",
        "TS;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008g\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0005J?\u0010\u0006\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u00080\u00072\u0006\u0010\t\u001a\u00028\u00002\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH&\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/workflow/rx1/ComposedReactor;",
        "S",
        "",
        "E",
        "O",
        "Lcom/squareup/workflow/rx1/WorkflowLauncher;",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lrx/Single;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "TE;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "TS;TO;>;>;"
        }
    .end annotation
.end method
