.class public final Lcom/squareup/workflow/rx1/ComposedReactorKt;
.super Ljava/lang/Object;
.source "ComposedReactor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001aa\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00062\u0006\u0010\u0007\u001a\u0002H\u00022\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\n\u001aN\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u000c\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006H\u0000\u00a8\u0006\r"
    }
    d2 = {
        "doLaunch",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "S",
        "E",
        "O",
        "",
        "Lcom/squareup/workflow/rx1/ComposedReactor;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Lcom/squareup/workflow/rx1/ComposedReactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;",
        "toRx2Reactor",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "pure-rx1"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final doLaunch(Lcom/squareup/workflow/rx1/ComposedReactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/ComposedReactor<",
            "TS;TE;+TO;>;TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$doLaunch"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p0}, Lcom/squareup/workflow/rx1/ComposedReactorKt;->toRx2Reactor(Lcom/squareup/workflow/rx1/ComposedReactor;)Lcom/squareup/workflow/legacy/rx2/Reactor;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/workflow/rx1/WorkflowKt;->toRx1Workflow(Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static final toRx2Reactor(Lcom/squareup/workflow/rx1/ComposedReactor;)Lcom/squareup/workflow/legacy/rx2/Reactor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/ComposedReactor<",
            "TS;TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/rx2/Reactor<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$toRx2Reactor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/rx1/ComposedReactorKt$toRx2Reactor$1;-><init>(Lcom/squareup/workflow/rx1/ComposedReactor;)V

    check-cast v0, Lcom/squareup/workflow/legacy/rx2/Reactor;

    return-object v0
.end method
