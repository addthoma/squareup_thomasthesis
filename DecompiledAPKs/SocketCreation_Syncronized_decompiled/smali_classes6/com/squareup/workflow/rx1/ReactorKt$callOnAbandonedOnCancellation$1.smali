.class final Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "Reactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/ReactorKt;->callOnAbandonedOnCancellation(Lcom/squareup/workflow/rx1/Reactor;Lcom/squareup/workflow/legacy/Workflow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Reactor.kt\ncom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1\n+ 2 Channels.common.kt\nkotlinx/coroutines/channels/ChannelsKt__Channels_commonKt\n*L\n1#1,78:1\n179#2:79\n158#2,3:80\n180#2,2:83\n165#2:85\n161#2,3:86\n*E\n*S KotlinDebug\n*F\n+ 1 Reactor.kt\ncom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1\n*L\n66#1:79\n66#1,3:80\n66#1,2:83\n66#1:85\n66#1,3:86\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003*\u00020\u0006H\u008a@\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "S",
        "",
        "E",
        "O",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.rx1.ReactorKt$callOnAbandonedOnCancellation$1"
    f = "Reactor.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0
    }
    l = {
        0x53
    }
    m = "invokeSuspend"
    n = {
        "$this$launch",
        "$this$consumeEach$iv",
        "$this$consume$iv$iv",
        "cause$iv$iv",
        "$this$consume$iv"
    }
    s = {
        "L$0",
        "L$1",
        "L$3",
        "L$4",
        "L$5"
    }
.end annotation


# instance fields
.field final synthetic $lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic $workflow:Lcom/squareup/workflow/legacy/Workflow;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field L$6:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->$lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;

    iget-object v1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    iget-object v2, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->$lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 64
    iget v1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$6:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/channels/ChannelIterator;

    iget-object v3, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$5:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v4, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$4:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Throwable;

    iget-object v5, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$3:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v6, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$2:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;

    iget-object v7, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$1:Ljava/lang/Object;

    check-cast v7, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v8, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$0:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v9, v0

    move-object v0, p0

    goto :goto_1

    .line 69
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 64
    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 65
    iget-object v1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-interface {v1}, Lcom/squareup/workflow/legacy/Workflow;->openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v5

    const/4 v1, 0x0

    .line 80
    check-cast v1, Ljava/lang/Throwable;

    .line 83
    :try_start_1
    invoke-interface {v5}, Lkotlinx/coroutines/channels/ReceiveChannel;->iterator()Lkotlinx/coroutines/channels/ChannelIterator;

    move-result-object v3

    move-object v8, p1

    move-object v6, v0

    move-object v4, v1

    move-object v1, v3

    move-object v3, v5

    move-object v7, v3

    move-object p1, p0

    move-object v0, p1

    :goto_0
    iput-object v8, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$0:Ljava/lang/Object;

    iput-object v7, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$1:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$2:Ljava/lang/Object;

    iput-object v5, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$3:Ljava/lang/Object;

    iput-object v4, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$4:Ljava/lang/Object;

    iput-object v3, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$5:Ljava/lang/Object;

    iput-object v1, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->L$6:Ljava/lang/Object;

    iput v2, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->label:I

    invoke-interface {v1, p1}, Lkotlinx/coroutines/channels/ChannelIterator;->hasNext(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v9

    if-ne v9, v6, :cond_2

    return-object v6

    :cond_2
    move-object v11, v6

    move-object v6, p1

    move-object p1, v9

    move-object v9, v11

    :goto_1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {v1}, Lkotlinx/coroutines/channels/ChannelIterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 67
    iget-object v10, v0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$1;->$lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p1, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object p1, v6

    move-object v6, v9

    goto :goto_0

    .line 84
    :cond_3
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    invoke-static {v5, v4}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    .line 69
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    :catchall_0
    move-exception p1

    .line 88
    :try_start_2
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    .line 85
    invoke-static {v5, p1}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    throw v0
.end method
