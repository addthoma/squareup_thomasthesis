.class public final Lcom/squareup/workflow/Worker$Companion;
.super Ljava/lang/Object;
.source "Worker.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 2 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,422:1\n203#1:425\n276#2:423\n276#2:424\n276#2:426\n*E\n*S KotlinDebug\n*F\n+ 1 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n254#1:425\n203#1:423\n240#1:424\n254#1:426\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JP\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\u0007\"\u0006\u0008\u0001\u0010\u0008\u0018\u00012/\u0008\t\u0010\t\u001a)\u0008\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00080\u000b\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0012\u0004\u0018\u00010\u00010\n\u00a2\u0006\u0002\u0008\u000eH\u0086\u0008\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ2\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00072\u001c\u0010\t\u001a\u0018\u0008\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0012\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u0012\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u0002H\u00150\u0007\"\u0004\u0008\u0001\u0010\u0015J?\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\u0007\"\u0006\u0008\u0001\u0010\u0008\u0018\u00012\u001e\u0008\u0008\u0010\t\u001a\u0018\u0008\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00080\u000c\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0012H\u0086\u0008\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013JE\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00080\u0007\"\n\u0008\u0001\u0010\u0008\u0018\u0001*\u00020\u00012 \u0008\u0008\u0010\t\u001a\u001a\u0008\u0001\u0012\u000c\u0012\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00080\u000c\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0012H\u0086\u0008\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u001e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00072\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u001cR\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/workflow/Worker$Companion;",
        "",
        "()V",
        "TYPE_OF_NOTHING",
        "Lkotlin/reflect/KType;",
        "TYPE_OF_NOTHING$annotations",
        "create",
        "Lcom/squareup/workflow/Worker;",
        "OutputT",
        "block",
        "Lkotlin/Function2;",
        "Lkotlinx/coroutines/flow/FlowCollector;",
        "Lkotlin/coroutines/Continuation;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Worker;",
        "createSideEffect",
        "",
        "Lkotlin/Function1;",
        "(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;",
        "finished",
        "T",
        "from",
        "fromNullable",
        "timer",
        "delayMs",
        "",
        "key",
        "",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/workflow/Worker$Companion;

.field private static final TYPE_OF_NOTHING:Lkotlin/reflect/KType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 181
    new-instance v0, Lcom/squareup/workflow/Worker$Companion;

    invoke-direct {v0}, Lcom/squareup/workflow/Worker$Companion;-><init>()V

    sput-object v0, Lcom/squareup/workflow/Worker$Companion;->$$INSTANCE:Lcom/squareup/workflow/Worker$Companion;

    .line 192
    const-class v0, Ljava/lang/Void;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    sput-object v0, Lcom/squareup/workflow/Worker$Companion;->TYPE_OF_NOTHING:Lkotlin/reflect/KType;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static synthetic TYPE_OF_NOTHING$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const-string p3, ""

    .line 266
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/Worker$Companion;->timer(JLjava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final synthetic create(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lkotlinx/coroutines/flow/FlowCollector<",
            "-TOutputT;>;-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-static {p1}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    const/4 v0, 0x6

    const-string v1, "OutputT"

    .line 423
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/Worker;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    sget-object v1, Lcom/squareup/workflow/Worker$Companion;->TYPE_OF_NOTHING:Lkotlin/reflect/KType;

    new-instance v2, Lcom/squareup/workflow/Worker$Companion$createSideEffect$1;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/squareup/workflow/Worker$Companion$createSideEffect$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final finished()Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation

    .line 228
    sget-object v0, Lcom/squareup/workflow/FinishedWorker;->INSTANCE:Lcom/squareup/workflow/FinishedWorker;

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final synthetic from(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/coroutines/Continuation<",
            "-TOutputT;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    invoke-static {p1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    const/4 v0, 0x6

    const-string v1, "OutputT"

    .line 424
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final synthetic fromNullable(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/coroutines/Continuation<",
            "-TOutputT;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/workflow/Worker$Companion$fromNullable$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/Worker$Companion$fromNullable$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 425
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    const/4 v0, 0x6

    const-string v2, "OutputT"

    .line 426
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final timer(JLjava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    new-instance v0, Lcom/squareup/workflow/TimerWorker;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/workflow/TimerWorker;-><init>(JLjava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method
