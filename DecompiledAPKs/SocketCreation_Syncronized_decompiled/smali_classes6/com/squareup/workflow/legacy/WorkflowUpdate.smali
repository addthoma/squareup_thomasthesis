.class public abstract Lcom/squareup/workflow/legacy/WorkflowUpdate;
.super Ljava/lang/Object;
.source "WorkflowUpdate.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00087\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u0002*\n\u0008\u0001\u0010\u0003 \u0001*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005\u0082\u0001\u0002\u0006\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "S",
        "",
        "E",
        "O",
        "()V",
        "Lcom/squareup/workflow/legacy/Running;",
        "Lcom/squareup/workflow/legacy/Finished;",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/workflow/legacy/WorkflowUpdate;-><init>()V

    return-void
.end method
