.class public final Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;
.super Ljava/lang/Object;
.source "EventSelectBuilder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEventSelectBuilder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,170:1\n125#1,3:171\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u00020\u0002:\u00014B#\u0008\u0000\u0012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ<\u0010\u0018\u001a\u00020\u0019\"\u0008\u0008\u0002\u0010\u001a*\u00028\u00002\u0014\u0010\u001b\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u0001H\u001a0\u001c2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u00028\u00010\u001cH\u0001J+\u0010\u001e\u001a\u00020\u0019\"\n\u0008\u0002\u0010\u001a\u0018\u0001*\u00028\u00002\u0014\u0008\u0008\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u00028\u00010\u001cH\u0086\u0008J6\u0010\u001f\u001a\u00020\u0019\"\u0008\u0008\u0002\u0010\u001a*\u00020\u00022\u000e\u0010 \u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u001a0\u00062\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u00028\u00010\u001cH\u0007JH\u0010!\u001a\u00020\u0019\"\u0004\u0008\u0002\u0010\u001a2\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u00028\u00010\u001c2\u001c\u0010\"\u001a\u0018\u0008\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u001a0#\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u001cH\u0001\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010$Jf\u0010%\u001a\u00020\u0019\"\n\u0008\u0002\u0010&\u0018\u0001*\u00020\u0002\"\n\u0008\u0003\u0010\'\u0018\u0001*\u00020\u0002*\u00020(2\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u0002H&\u0012\u0004\u0012\u0002H\'0*2\u0006\u0010+\u001a\u0002H&2\u0008\u0008\u0002\u0010,\u001a\u00020-2\u0014\u0008\u0008\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u0002H\'\u0012\u0004\u0012\u00028\u00010\u001cH\u0086\u0008\u00a2\u0006\u0002\u0010.Jh\u0010/\u001a\u00020\u0019\"\u0008\u0008\u0002\u00100*\u00020\u0002\"\u0008\u0008\u0003\u0010\u0001*\u00020\u0002\"\u0008\u0008\u0004\u0010\'*\u00020\u0002*\u00020(2\u0018\u00101\u001a\u0014\u0012\u0004\u0012\u0002H0\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\'022$\u0010\u001d\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H0\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\'03\u0012\u0004\u0012\u00028\u00010\u001cR(\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00060\u00058\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u000c\u0010\rR*\u0010\u000e\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00028\u0000\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u00010\u00100\u000fX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u00020\u00148\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0015\u0010\u000b\u001a\u0004\u0008\u0016\u0010\u0017\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "E",
        "",
        "R",
        "builder",
        "Lkotlinx/coroutines/selects/SelectBuilder;",
        "Lio/reactivex/Single;",
        "selectionJob",
        "Lkotlinx/coroutines/Job;",
        "(Lkotlinx/coroutines/selects/SelectBuilder;Lkotlinx/coroutines/Job;)V",
        "builder$annotations",
        "()V",
        "getBuilder",
        "()Lkotlinx/coroutines/selects/SelectBuilder;",
        "cases",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;",
        "getCases$legacy_workflow_rx2",
        "()Ljava/util/List;",
        "scope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "scope$annotations",
        "getScope",
        "()Lkotlinx/coroutines/CoroutineScope;",
        "addEventCase",
        "",
        "T",
        "predicateMapper",
        "Lkotlin/Function1;",
        "handler",
        "onEvent",
        "onSuccess",
        "single",
        "onSuspending",
        "block",
        "Lkotlin/coroutines/Continuation;",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "onWorkerResult",
        "I",
        "O",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "worker",
        "Lcom/squareup/workflow/legacy/Worker;",
        "input",
        "name",
        "",
        "(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V",
        "onWorkflowUpdate",
        "S",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "SelectCase",
        "legacy-workflow-rx2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final builder:Lkotlinx/coroutines/selects/SelectBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/selects/SelectBuilder<",
            "Lio/reactivex/Single<",
            "TR;>;>;"
        }
    .end annotation
.end field

.field private final cases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase<",
            "TE;*TR;>;>;"
        }
    .end annotation
.end field

.field private final scope:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/selects/SelectBuilder;Lkotlinx/coroutines/Job;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/selects/SelectBuilder<",
            "-",
            "Lio/reactivex/Single<",
            "TR;>;>;",
            "Lkotlinx/coroutines/Job;",
            ")V"
        }
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectionJob"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->builder:Lkotlinx/coroutines/selects/SelectBuilder;

    .line 71
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->cases:Ljava/util/List;

    .line 81
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p1

    check-cast p2, Lkotlin/coroutines/CoroutineContext;

    invoke-virtual {p1, p2}, Lkotlinx/coroutines/CoroutineDispatcher;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    invoke-static {p1}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->scope:Lkotlinx/coroutines/CoroutineScope;

    return-void
.end method

.method public static synthetic builder$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic onWorkerResult$default(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    const-string p4, ""

    :cond_0
    move-object v4, p4

    const-string p4, "$this$onWorkerResult"

    .line 123
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "worker"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "input"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "name"

    invoke-static {v4, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "handler"

    invoke-static {p5, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    new-instance p4, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onWorkerResult$1;

    const/4 v5, 0x0

    move-object v0, p4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onWorkerResult$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p5, p4}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static synthetic scope$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::TE;>(",
            "Lkotlin/jvm/functions/Function1<",
            "-TE;+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "predicateMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->cases:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    new-instance v1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;

    invoke-direct {v1, p1, p2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final getBuilder()Lkotlinx/coroutines/selects/SelectBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/selects/SelectBuilder<",
            "Lio/reactivex/Single<",
            "TR;>;>;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->builder:Lkotlinx/coroutines/selects/SelectBuilder;

    return-object v0
.end method

.method public final getCases$legacy_workflow_rx2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase<",
            "TE;*TR;>;>;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->cases:Ljava/util/List;

    return-object v0
.end method

.method public final getScope()Lkotlinx/coroutines/CoroutineScope;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->scope:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final synthetic onEvent(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::TE;>(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onEvent$1;->INSTANCE:Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onEvent$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final onSuccess(Lio/reactivex/Single;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Create a Worker and use onNextDelegateReaction or onWorkerResult instead."
    .end annotation

    const-string v0, "single"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onSuccess$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onSuccess$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/coroutines/Continuation<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->builder:Lkotlinx/coroutines/selects/SelectBuilder;

    .line 165
    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->scope:Lkotlinx/coroutines/CoroutineScope;

    sget-object v3, Lkotlinx/coroutines/CoroutineStart;->UNDISPATCHED:Lkotlinx/coroutines/CoroutineStart;

    new-instance v2, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onSuspending$$inlined$with$lambda$1;

    const/4 v7, 0x0

    invoke-direct {v2, v7, p0, p2, p1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onSuspending$$inlined$with$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object v4, v2

    check-cast v4, Lkotlin/jvm/functions/Function2;

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object v1

    invoke-interface {v1}, Lkotlinx/coroutines/Deferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v1

    .line 166
    new-instance v2, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onSuspending$$inlined$with$lambda$2;

    invoke-direct {v2, v7, p0, p2, p1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onSuspending$$inlined$with$lambda$2;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-interface {v0, v1, v2}, Lkotlinx/coroutines/selects/SelectBuilder;->invoke(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final synthetic onWorkerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;TI;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TO;+TR;>;)V"
        }
    .end annotation

    const-string v0, "$this$onWorkerResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "worker"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    new-instance v0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onWorkerResult$1;

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onWorkerResult$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p5, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
            "+TS;+TE;+TO;>;+TR;>;)V"
        }
    .end annotation

    const-string v0, "$this$onWorkflowUpdate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onWorkflowUpdate$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$onWorkflowUpdate$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p3, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
