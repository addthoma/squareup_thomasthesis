.class public final Lcom/squareup/workflow/legacy/WorkflowInputKt;
.super Ljava/lang/Object;
.source "WorkflowInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u001a(\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00050\u0004H\u0007\u001a8\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0001\"\u0004\u0008\u0000\u0010\u0007\"\u0004\u0008\u0001\u0010\u0008*\u0008\u0012\u0004\u0012\u0002H\u00080\u00012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00080\u0004H\u0007\u00a8\u0006\n"
    }
    d2 = {
        "WorkflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "E",
        "handler",
        "Lkotlin/Function1;",
        "",
        "adaptEvents",
        "E2",
        "E1",
        "transform",
        "legacy-workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TE;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "TE;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "handler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowInputKt$WorkflowInput$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/legacy/WorkflowInputKt$WorkflowInput$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public static final adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E2:",
            "Ljava/lang/Object;",
            "E1:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-TE1;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TE2;+TE1;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "TE2;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$adaptEvents"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowInputKt$adaptEvents$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInputKt$adaptEvents$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p0

    return-object p0
.end method
