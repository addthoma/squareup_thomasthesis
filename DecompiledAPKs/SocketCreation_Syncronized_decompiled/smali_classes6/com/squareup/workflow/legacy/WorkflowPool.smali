.class public final Lcom/squareup/workflow/legacy/WorkflowPool;
.super Ljava/lang/Object;
.source "WorkflowPool.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacy/WorkflowPool$Type;,
        Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;,
        Lcom/squareup/workflow/legacy/WorkflowPool$Id;,
        Lcom/squareup/workflow/legacy/WorkflowPool$Handle;,
        Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;,
        Lcom/squareup/workflow/legacy/WorkflowPool$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowPool.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool\n+ 2 Channels.common.kt\nkotlinx/coroutines/channels/ChannelsKt__Channels_commonKt\n+ 3 Worker.kt\ncom/squareup/workflow/legacy/WorkerKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,422:1\n317#1,8:426\n201#1:442\n317#1,9:444\n264#1:454\n158#2,3:423\n165#2:434\n161#2,6:435\n114#3:441\n114#3:443\n114#3:453\n114#3:455\n1591#4,2:456\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool\n*L\n171#1,8:426\n217#1,9:444\n171#1,3:423\n171#1:434\n171#1,6:435\n201#1:441\n264#1:453\n273#1,2:456\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u0000 02\u00020\u0001:\u0006012345B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000e\u001a\u00020\u000fJ?\u0010\u0010\u001a\u00020\u000f\"\n\u0008\u0000\u0010\u0011\u0018\u0001*\u00020\u0001\"\n\u0008\u0001\u0010\u0012\u0018\u0001*\u00020\u00012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u00120\u00142\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u0016H\u0086\u0008J\u001a\u0010\u0017\u001a\u00020\u000f2\u0012\u0010\u0018\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0019J\u001c\u0010\u0017\u001a\u00020\u000f2\u0012\u0010\u001a\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cH\u0001JO\u0010\u001b\u001a\u0002H\u0012\"\n\u0008\u0000\u0010\u0011\u0018\u0001*\u00020\u0001\"\n\u0008\u0001\u0010\u0012\u0018\u0001*\u00020\u00012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u00120\u00142\u0006\u0010\u001c\u001a\u0002H\u00112\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u0016H\u0086H\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001dJc\u0010\u001b\u001a\u0002H\u0012\"\u0008\u0008\u0000\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0012*\u00020\u00012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u00120\u00142\u0006\u0010\u001c\u001a\u0002H\u00112\u0006\u0010\u0015\u001a\u00020\u00162\u0018\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u0002H\u00120\u0005H\u0081@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J[\u0010!\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\"\"\u0008\u0008\u0000\u0010#*\u00020\u0001\"\u0008\u0008\u0001\u0010$*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0012*\u00020\u00012\u0018\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\u0019H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010%J,\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H$0&\"\u0008\u0008\u0000\u0010$*\u00020\u00012\u0014\u0010\u0018\u001a\u0010\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H$\u0012\u0002\u0008\u00030\u0019JR\u0010\'\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\u0006\"\u0008\u0008\u0000\u0010#*\u00020\u0001\"\u0008\u0008\u0001\u0010$*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0012*\u00020\u00012\u0018\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\u0005H\u0002JX\u0010(\u001a\u00020\u000f\"\u0008\u0008\u0000\u0010#*\u00020\u0001\"\u0008\u0008\u0001\u0010$*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0012*\u00020\u00012\u0018\u0010\'\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\u00062\u0018\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\u0005J:\u0010)\u001a\u0002H*\"\u0008\u0008\u0000\u0010**\u00020\u00012\u0012\u0010\u001a\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000c2\u000c\u0010+\u001a\u0008\u0012\u0004\u0012\u0002H*0,H\u0082\u0008\u00a2\u0006\u0002\u0010-JR\u0010.\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120/\"\u0008\u0008\u0000\u0010#*\u00020\u0001\"\u0008\u0008\u0001\u0010$*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0012*\u00020\u00012\u0018\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u0002H$\u0012\u0004\u0012\u0002H\u00120\u0019H\u0002R2\u0010\u0003\u001a&\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR&\u0010\u000b\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000c\u0012\u0004\u0012\u00020\r0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "",
        "()V",
        "launchers",
        "",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "peekWorkflowsCount",
        "",
        "getPeekWorkflowsCount",
        "()I",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Id;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;",
        "abandonAll",
        "",
        "abandonWorker",
        "I",
        "O",
        "worker",
        "Lcom/squareup/workflow/legacy/Worker;",
        "name",
        "",
        "abandonWorkflow",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "id",
        "awaitWorkerResult",
        "input",
        "(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "type",
        "",
        "(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "awaitWorkflowUpdate",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "S",
        "E",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "launcher",
        "register",
        "removeCompletedWorkflowAfter",
        "R",
        "block",
        "Lkotlin/Function0;",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "requireWorkflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Companion",
        "Handle",
        "Id",
        "Launcher",
        "Type",
        "WorkflowEntry",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;


# instance fields
.field private final launchers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "***>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "***>;>;"
        }
    .end annotation
.end field

.field private final workflows:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "***>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->launchers:Ljava/util/Map;

    .line 134
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    return-void
.end method

.method public static synthetic abandonWorker$default(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 2

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, ""

    :cond_0
    const-string p3, "worker"

    .line 263
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "name"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 455
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 p3, 0x4

    const-string p4, "I"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p4, Ljava/lang/Object;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p4

    const-class v0, Ljava/lang/Void;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, "O"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p3, Ljava/lang/Object;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-direct {p1, p4, v0, p3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 454
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool;->abandonWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Id;)V

    return-void
.end method

.method public static final synthetic access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    return-object p0
.end method

.method public static synthetic awaitWorkerResult$default(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 6

    const/4 p6, 0x4

    and-int/2addr p5, p6

    if-eqz p5, :cond_0

    const-string p3, ""

    :cond_0
    move-object v3, p3

    .line 443
    new-instance v4, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-string p3, "I"

    invoke-static {p6, p3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p3, Ljava/lang/Object;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-class p5, Ljava/lang/Void;

    invoke-static {p5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p5

    const-string v0, "O"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p6, Ljava/lang/Object;

    invoke-static {p6}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p6

    invoke-direct {v4, p3, p5, p6}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const/4 p3, 0x0

    invoke-static {p3}, Lkotlin/jvm/internal/InlineMarker;->mark(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    .line 442
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/workflow/legacy/WorkflowPool;->awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p0

    const/4 p1, 0x1

    invoke-static {p1}, Lkotlin/jvm/internal/InlineMarker;->mark(I)V

    return-object p0
.end method

.method private final launcher(Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TS;-TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;TE;TO;>;"
        }
    .end annotation

    .line 279
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->launchers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    return-object v0

    .line 284
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.workflow.legacy.WorkflowPool.Launcher<S, E, O>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 281
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected launcher for \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\". Did you forget to call WorkflowPool.register()?"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 280
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final removeCompletedWorkflowAfter(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "***>;",
            "Lkotlin/jvm/functions/Function0<",
            "+TR;>;)TR;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 318
    :try_start_0
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 320
    invoke-static {p0}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz v1, :cond_0

    .line 321
    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/workflow/legacy/Workflow;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    invoke-static {p0}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-object p2

    :catchall_0
    move-exception p2

    .line 325
    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 320
    invoke-static {p0}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz v1, :cond_1

    .line 321
    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/workflow/legacy/Workflow;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 322
    invoke-static {p0}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    :cond_1
    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p2
.end method

.method private final requireWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;-TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    .line 296
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 299
    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Id;->getWorkflowType()Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->launcher(Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v0

    .line 301
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    .line 303
    iget-object v2, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final abandonAll()V
    .locals 4

    .line 273
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 456
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    .line 273
    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v3, v2, v3}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final synthetic abandonWorker(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "name"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 453
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v0, 0x4

    const-string v1, "I"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Ljava/lang/Void;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "O"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {p1, v1, v2, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 264
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool;->abandonWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Id;)V

    return-void
.end method

.method public final abandonWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "***>;)V"
        }
    .end annotation

    const-string v0, "handle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool;->abandonWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Id;)V

    return-void
.end method

.method public final abandonWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Id;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Id<",
            "***>;)V"
        }
    .end annotation

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v1}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;TI;",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TI;*+TO;>;",
            "Lkotlin/coroutines/Continuation<",
            "-TO;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p5, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;

    if-eqz v0, :cond_0

    move-object v0, p5

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;

    iget v1, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p5, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    sub-int/2addr p5, v2

    iput p5, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;

    invoke-direct {v0, p0, p5}, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p5, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 207
    iget v2, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$8:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    iget-object p2, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$7:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$6:Ljava/lang/Object;

    check-cast p3, Lcom/squareup/workflow/legacy/Workflow;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$5:Ljava/lang/Object;

    check-cast p3, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$4:Ljava/lang/Object;

    check-cast p3, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$3:Ljava/lang/Object;

    check-cast p3, Ljava/lang/String;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$2:Ljava/lang/Object;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$1:Ljava/lang/Object;

    check-cast p3, Lcom/squareup/workflow/legacy/Worker;

    iget-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$0:Ljava/lang/Object;

    check-cast p3, Lcom/squareup/workflow/legacy/WorkflowPool;

    :try_start_0
    invoke-static {p5}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p3

    move-object v4, p1

    goto :goto_2

    .line 220
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 207
    :cond_2
    invoke-static {p5}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 213
    invoke-static {p1}, Lcom/squareup/workflow/legacy/WorkflowPoolKt;->access$asLauncher(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;

    move-result-object p5

    check-cast p5, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-virtual {p0, p5, p4}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 214
    new-instance p5, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {p4, p3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v2

    invoke-direct {p5, v2, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    .line 215
    invoke-direct {p0, p5}, Lcom/squareup/workflow/legacy/WorkflowPool;->requireWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v2

    .line 217
    invoke-virtual {p5}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v4

    .line 218
    :try_start_1
    iput-object p0, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$1:Ljava/lang/Object;

    iput-object p2, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$2:Ljava/lang/Object;

    iput-object p3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$3:Ljava/lang/Object;

    iput-object p4, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$4:Ljava/lang/Object;

    iput-object p5, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$5:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$6:Ljava/lang/Object;

    iput-object p0, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$7:Ljava/lang/Object;

    iput-object v4, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->L$8:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    invoke-interface {v2, v0}, Lcom/squareup/workflow/legacy/Workflow;->await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-ne p5, v1, :cond_3

    return-object v1

    :cond_3
    move-object p2, p0

    move-object p1, v4

    .line 447
    :goto_1
    invoke-static {p2}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p3

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz p3, :cond_4

    .line 448
    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p3

    invoke-interface {p3}, Lcom/squareup/workflow/legacy/Workflow;->isCompleted()Z

    move-result p3

    if-eqz p3, :cond_4

    .line 449
    invoke-static {p2}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    return-object p5

    :catchall_1
    move-exception p3

    move-object p2, p0

    .line 447
    :goto_2
    invoke-static {p2}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz p1, :cond_5

    .line 448
    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/workflow/legacy/Workflow;->isCompleted()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 449
    invoke-static {p2}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    :cond_5
    throw p3
.end method

.method public final synthetic awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;TI;",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-TO;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 441
    new-instance v4, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v0, 0x4

    const-string v1, "I"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Ljava/lang/Void;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "O"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {v4, v1, v2, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lkotlin/jvm/internal/InlineMarker;->mark(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 201
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/workflow/legacy/WorkflowPool;->awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x1

    invoke-static {p2}, Lkotlin/jvm/internal/InlineMarker;->mark(I)V

    return-object p1
.end method

.method public final awaitWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;-TE;+TO;>;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
            "+TS;+TE;+TO;>;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;

    iget v3, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget v0, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    sub-int/2addr v0, v4

    iput v0, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v0, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v3

    .line 166
    iget v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_3

    if-eq v4, v6, :cond_2

    if-ne v4, v5, :cond_1

    iget-object v3, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$8:Ljava/lang/Object;

    iget-object v3, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$7:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    iget-object v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$6:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v5, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$5:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v5, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$4:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Throwable;

    iget-object v6, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$3:Ljava/lang/Object;

    check-cast v6, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v7, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$2:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/workflow/legacy/Workflow;

    iget-object v7, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$1:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object v2, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$0:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowPool;

    :try_start_0
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v11, v5

    goto/16 :goto_4

    :catchall_0
    move-exception v0

    move-object v9, v4

    move-object v4, v6

    goto/16 :goto_6

    .line 185
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_2
    iget-object v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$8:Ljava/lang/Object;

    iget-object v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$7:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    iget-object v9, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$6:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v10, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$5:Ljava/lang/Object;

    check-cast v10, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v11, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$4:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Throwable;

    iget-object v12, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v13, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/workflow/legacy/Workflow;

    iget-object v14, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$1:Ljava/lang/Object;

    check-cast v14, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object v15, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/workflow/legacy/WorkflowPool;

    :try_start_1
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object/from16 v16, v4

    move-object v4, v3

    move-object/from16 v3, v16

    goto/16 :goto_3

    :cond_3
    iget-object v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$7:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    iget-object v9, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$6:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v10, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$5:Ljava/lang/Object;

    check-cast v10, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v11, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$4:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Throwable;

    iget-object v12, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$3:Ljava/lang/Object;

    check-cast v12, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v13, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$2:Ljava/lang/Object;

    check-cast v13, Lcom/squareup/workflow/legacy/Workflow;

    iget-object v14, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$1:Ljava/lang/Object;

    check-cast v14, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object v15, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$0:Ljava/lang/Object;

    check-cast v15, Lcom/squareup/workflow/legacy/WorkflowPool;

    :try_start_2
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v16, v10

    move-object v10, v4

    move-object/from16 v4, v16

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v3, v4

    :goto_1
    move-object v4, v12

    goto/16 :goto_6

    :cond_4
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 169
    invoke-direct/range {p0 .. p1}, Lcom/squareup/workflow/legacy/WorkflowPool;->requireWorkflow(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v13

    .line 170
    invoke-interface {v13}, Lcom/squareup/workflow/legacy/Workflow;->openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v4

    .line 423
    move-object v11, v7

    check-cast v11, Ljava/lang/Throwable;

    .line 172
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 173
    :try_start_4
    iput-object v1, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$0:Ljava/lang/Object;

    move-object/from16 v14, p1

    iput-object v14, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$1:Ljava/lang/Object;

    iput-object v13, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$2:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$3:Ljava/lang/Object;

    iput-object v11, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$4:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$5:Ljava/lang/Object;

    iput-object v1, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$6:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$7:Ljava/lang/Object;

    iput v8, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    invoke-interface {v4, v2}, Lkotlinx/coroutines/channels/ReceiveChannel;->receiveOrNull(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-ne v0, v3, :cond_5

    return-object v3

    :cond_5
    move-object v15, v1

    move-object v12, v4

    move-object v10, v9

    move-object v9, v15

    :goto_2
    move-object/from16 v16, v4

    move-object v4, v3

    move-object v3, v10

    move-object/from16 v10, v16

    .line 175
    :cond_6
    :goto_3
    :try_start_5
    invoke-virtual {v14}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v13}, Lcom/squareup/workflow/legacy/Workflow;->isActive()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 176
    iput-object v15, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$0:Ljava/lang/Object;

    iput-object v14, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$1:Ljava/lang/Object;

    iput-object v13, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$2:Ljava/lang/Object;

    iput-object v12, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$3:Ljava/lang/Object;

    iput-object v11, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$4:Ljava/lang/Object;

    iput-object v10, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$5:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$6:Ljava/lang/Object;

    iput-object v3, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$7:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$8:Ljava/lang/Object;

    iput v6, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    invoke-interface {v10, v2}, Lkotlinx/coroutines/channels/ReceiveChannel;->receiveOrNull(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v4, :cond_6

    return-object v4

    :cond_7
    if-eqz v0, :cond_8

    .line 178
    invoke-interface {v13}, Lcom/squareup/workflow/legacy/Workflow;->isActive()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 179
    new-instance v2, Lcom/squareup/workflow/legacy/Running;

    invoke-static {v14, v7, v0, v8, v7}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->copy$default(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/workflow/legacy/Running;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowUpdate;

    move-object v4, v9

    move-object v6, v12

    goto :goto_5

    .line 181
    :cond_8
    iput-object v15, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$0:Ljava/lang/Object;

    iput-object v14, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$1:Ljava/lang/Object;

    iput-object v13, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$2:Ljava/lang/Object;

    iput-object v12, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$3:Ljava/lang/Object;

    iput-object v11, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$4:Ljava/lang/Object;

    iput-object v10, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$5:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$6:Ljava/lang/Object;

    iput-object v3, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$7:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->L$8:Ljava/lang/Object;

    const/4 v0, 0x3

    iput v0, v2, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkflowUpdate$1;->label:I

    invoke-interface {v13, v2}, Lcom/squareup/workflow/legacy/Workflow;->await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    if-ne v0, v4, :cond_9

    return-object v4

    :cond_9
    move-object v4, v9

    move-object v6, v12

    .line 166
    :goto_4
    :try_start_6
    new-instance v2, Lcom/squareup/workflow/legacy/Finished;

    invoke-direct {v2, v0}, Lcom/squareup/workflow/legacy/Finished;-><init>(Ljava/lang/Object;)V

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowUpdate;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 429
    :goto_5
    :try_start_7
    invoke-static {v4}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz v0, :cond_a

    .line 430
    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/workflow/legacy/Workflow;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 431
    invoke-static {v4}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 434
    :cond_a
    invoke-static {v6, v11}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    return-object v2

    :catchall_2
    move-exception v0

    move-object v2, v0

    move-object v4, v6

    goto :goto_7

    :catchall_3
    move-exception v0

    goto/16 :goto_1

    :catchall_4
    move-exception v0

    move-object v3, v9

    move-object v9, v1

    .line 429
    :goto_6
    :try_start_8
    invoke-static {v9}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz v2, :cond_b

    .line 430
    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/workflow/legacy/Workflow;->isCompleted()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 431
    invoke-static {v9}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    :cond_b
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    :catchall_5
    move-exception v0

    move-object v2, v0

    .line 437
    :goto_7
    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    :catchall_6
    move-exception v0

    move-object v3, v0

    .line 434
    invoke-static {v4, v2}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public final getPeekWorkflowsCount()I
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->workflows:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method

.method public final input(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "*-TE;*>;)",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "TE;>;"
        }
    .end annotation

    const-string v0, "handle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$input$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$input$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;-TE;+TO;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TS;-TE;+TO;>;)V"
        }
    .end annotation

    const-string v0, "launcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool;->launchers:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
