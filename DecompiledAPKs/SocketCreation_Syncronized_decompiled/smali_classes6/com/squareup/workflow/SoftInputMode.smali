.class public final enum Lcom/squareup/workflow/SoftInputMode;
.super Ljava/lang/Enum;
.source "SoftInputMode.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/workflow/SoftInputMode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/workflow/SoftInputMode;",
        "",
        "flags",
        "",
        "(Ljava/lang/String;II)V",
        "getFlags",
        "()I",
        "UNSPECIFIED",
        "HIDDEN",
        "PAN",
        "RESIZE",
        "ALWAYS_SHOW_RESIZE",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/workflow/SoftInputMode;

.field public static final enum ALWAYS_SHOW_RESIZE:Lcom/squareup/workflow/SoftInputMode;

.field public static final enum HIDDEN:Lcom/squareup/workflow/SoftInputMode;

.field public static final enum PAN:Lcom/squareup/workflow/SoftInputMode;

.field public static final enum RESIZE:Lcom/squareup/workflow/SoftInputMode;

.field public static final enum UNSPECIFIED:Lcom/squareup/workflow/SoftInputMode;


# instance fields
.field private final flags:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/workflow/SoftInputMode;

    new-instance v1, Lcom/squareup/workflow/SoftInputMode;

    const/4 v2, 0x0

    const-string v3, "UNSPECIFIED"

    .line 15
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/workflow/SoftInputMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/workflow/SoftInputMode;->UNSPECIFIED:Lcom/squareup/workflow/SoftInputMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/workflow/SoftInputMode;

    const/4 v2, 0x1

    const/4 v3, 0x2

    const-string v4, "HIDDEN"

    .line 18
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/workflow/SoftInputMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/workflow/SoftInputMode;->HIDDEN:Lcom/squareup/workflow/SoftInputMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/workflow/SoftInputMode;

    const-string v2, "PAN"

    const/16 v4, 0x20

    .line 24
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/SoftInputMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/workflow/SoftInputMode;->PAN:Lcom/squareup/workflow/SoftInputMode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/workflow/SoftInputMode;

    const/4 v2, 0x3

    const-string v3, "RESIZE"

    const/16 v4, 0x10

    .line 29
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/workflow/SoftInputMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/workflow/SoftInputMode;->RESIZE:Lcom/squareup/workflow/SoftInputMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/workflow/SoftInputMode;

    const/4 v2, 0x4

    const-string v3, "ALWAYS_SHOW_RESIZE"

    const/16 v4, 0x15

    .line 35
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/workflow/SoftInputMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/workflow/SoftInputMode;->ALWAYS_SHOW_RESIZE:Lcom/squareup/workflow/SoftInputMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/workflow/SoftInputMode;->$VALUES:[Lcom/squareup/workflow/SoftInputMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/workflow/SoftInputMode;->flags:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    const-class v0, Lcom/squareup/workflow/SoftInputMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/SoftInputMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->$VALUES:[Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {v0}, [Lcom/squareup/workflow/SoftInputMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method


# virtual methods
.method public final getFlags()I
    .locals 1

    .line 12
    iget v0, p0, Lcom/squareup/workflow/SoftInputMode;->flags:I

    return v0
.end method
