.class public interface abstract Lcom/squareup/workflow/WorkflowViewFactory;
.super Ljava/lang/Object;
.source "WorkflowViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/WorkflowViewFactory$Orientation;,
        Lcom/squareup/workflow/WorkflowViewFactory$ViewType;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0002\u001b\u001cJH\u0010\u0008\u001a\u000c\u0012\u0006\u0008\u0001\u0012\u00020\n\u0018\u00010\t2\u0012\u0010\u000b\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00052\u0018\u0010\u000c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\r2\u0006\u0010\u0010\u001a\u00020\u0011H&JR\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0012\u0010\u000b\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00052\u0018\u0010\u000c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\r2\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H&J\u001e\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0012\u0010\u000b\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u0005H&R$\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "",
        "keys",
        "",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "getKeys",
        "()Ljava/util/Set;",
        "buildDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "screenKey",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "buildView",
        "Landroid/view/View;",
        "container",
        "Landroid/view/ViewGroup;",
        "contextForNewView",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "getHintForKey",
        "Lcom/squareup/workflow/ScreenHint;",
        "Orientation",
        "ViewType",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract buildDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/content/Context;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation
.end method

.method public abstract buildView(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract getHintForKey(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/ScreenHint;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lcom/squareup/workflow/ScreenHint;"
        }
    .end annotation
.end method

.method public abstract getKeys()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;>;"
        }
    .end annotation
.end method
