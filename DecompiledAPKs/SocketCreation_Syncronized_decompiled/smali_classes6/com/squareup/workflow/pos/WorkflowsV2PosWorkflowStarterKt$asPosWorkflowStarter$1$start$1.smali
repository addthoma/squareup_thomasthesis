.class final Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1$start$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowsV2PosWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacyintegration/LegacyState<",
        "TI;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a4\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0004\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005j\u0002`\u00060\u0003j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0004`\u00070\u00020\u0001\"\u0008\u0008\u0000\u0010\u0008*\u00020\t\"\u0008\u0008\u0001\u0010\n*\u00020\t2,\u0010\u000b\u001a(\u0012\u0004\u0012\u0002H\u0008\u0012\u001e\u0012\u001c\u0012\u0006\u0008\u0001\u0012\u00020\u0004\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005j\u0002`\u00060\u00030\u000cH\n\u00a2\u0006\u0002\u0008\r"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "I",
        "",
        "O",
        "state",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1$start$1;->this$0:Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/legacyintegration/LegacyState;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TI;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1$start$1;->this$0:Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v0, v1, p1}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->access$toScreenState(Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;Ljava/util/Map;Lcom/squareup/workflow/Snapshot;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1$start$1;->invoke(Lcom/squareup/workflow/legacyintegration/LegacyState;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
