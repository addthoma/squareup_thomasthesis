.class public final Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt;
.super Ljava/lang/Object;
.source "LegacyWorkflowAdapters.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u00a0\u0001\u0010\u0000\u001ax\u0012\u0004\u0012\u00020\u0002\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0003\u0012\u0004\u0012\u0002H\t\u0012\u0004\u0012\u0002H\n\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u00030\u0001\"\u0008\u0008\u0000\u0010\t*\u00020\u000b\"\u0008\u0008\u0001\u0010\n*\u00020\u000b*\u000e\u0012\u0004\u0012\u0002H\t\u0012\u0004\u0012\u0002H\n0\u000c\u00a8\u0006\r"
    }
    d2 = {
        "asV2Workflow",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "EventT",
        "OutputT",
        "",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "v2-pos-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asV2Workflow(Lcom/squareup/container/PosWorkflowStarter;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<EventT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TEventT;+TOutputT;>;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;TEventT;TOutputT;",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    const-string v0, "$this$asV2Workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;-><init>(Lcom/squareup/container/PosWorkflowStarter;)V

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-object v0
.end method
