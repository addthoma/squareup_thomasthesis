.class final Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RenderContext.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TEventT;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "TStateT;+TOutputT;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRenderContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RenderContext.kt\ncom/squareup/workflow/RenderContextKt$makeEventSink$1\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,202:1\n199#2,4:203\n*E\n*S KotlinDebug\n*F\n+ 1 RenderContext.kt\ncom/squareup/workflow/RenderContextKt$makeEventSink$1\n*L\n183#1,4:203\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0002\"\u0008\u0008\u0002\u0010\u0003*\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "StateT",
        "OutputT",
        "EventT",
        "",
        "event",
        "invoke",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $update:Lkotlin/jvm/functions/Function2;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;->$update:Lkotlin/jvm/functions/Function2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEventT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .line 203
    new-instance v0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;

    invoke-direct {v0, p0, p1, p1}, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;-><init>(Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;Ljava/lang/Object;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;->invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
