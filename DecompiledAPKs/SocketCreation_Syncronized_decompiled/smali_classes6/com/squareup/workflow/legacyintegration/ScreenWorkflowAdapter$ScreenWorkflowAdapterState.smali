.class public final Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;
.super Ljava/lang/Object;
.source "ScreenWorkflowAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenWorkflowAdapterState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScreenWorkflowAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScreenWorkflowAdapter.kt\ncom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,113:1\n41#2:114\n56#2,2:115\n276#3:117\n*E\n*S KotlinDebug\n*F\n+ 1 ScreenWorkflowAdapter.kt\ncom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState\n*L\n64#1:114\n64#1,2:115\n64#1:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0003\u0010\u0001*\u00020\u0002*\u0008\u0008\u0004\u0010\u0003*\u00020\u00022\u00020\u0002:\u0001$B5\u0012\u001e\u0010\u0004\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00040\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u00030\u0005\u0012\u000e\u0010\u0008\u001a\n\u0012\u0004\u0012\u00028\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\tJ!\u0010\u001a\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00040\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u00030\u0005H\u00c6\u0003J\u0011\u0010\u001b\u001a\n\u0012\u0004\u0012\u00028\u0004\u0018\u00010\u0006H\u00c6\u0003JI\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00040\u00002 \u0008\u0002\u0010\u0004\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00040\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u00030\u00052\u0010\u0008\u0002\u0010\u0008\u001a\n\u0012\u0004\u0012\u00028\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u001d\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00040\u00060\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0019\u0010\u0008\u001a\n\u0012\u0004\u0012\u00028\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R!\u0010\u0014\u001a\u00120\u0015R\u000e\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00040\u0000\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R)\u0010\u0004\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00040\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;",
        "OutputT",
        "",
        "RenderingT",
        "runningWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "lastLegacyState",
        "(Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;)V",
        "cancellationWorker",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "getCancellationWorker",
        "()Lcom/squareup/workflow/LifecycleWorker;",
        "childStateWorker",
        "Lcom/squareup/workflow/Worker;",
        "getChildStateWorker",
        "()Lcom/squareup/workflow/Worker;",
        "getLastLegacyState",
        "()Lcom/squareup/workflow/ScreenState;",
        "resultWorker",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;",
        "getResultWorker",
        "()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;",
        "getRunningWorkflow",
        "()Lcom/squareup/workflow/rx1/Workflow;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "ResultWorker",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancellationWorker:Lcom/squareup/workflow/LifecycleWorker;

.field private final childStateWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;>;"
        }
    .end annotation
.end field

.field private final lastLegacyState:Lcom/squareup/workflow/ScreenState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;"
        }
    .end annotation
.end field

.field private final resultWorker:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>.ResultWorker;"
        }
    .end annotation
.end field

.field private final runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;*TOutputT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;*+TOutputT;>;",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;)V"
        }
    .end annotation

    const-string v0, "runningWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    .line 60
    new-instance p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$cancellationWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$cancellationWorker$1;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;)V

    check-cast p1, Lcom/squareup/workflow/LifecycleWorker;

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->cancellationWorker:Lcom/squareup/workflow/LifecycleWorker;

    .line 63
    iget-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {p1}, Lcom/squareup/workflow/rx1/Workflow;->getState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 114
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 116
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 117
    const-class p2, Lcom/squareup/workflow/ScreenState;

    sget-object v0, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->nullableTypeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    .line 114
    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->childStateWorker:Lcom/squareup/workflow/Worker;

    .line 65
    new-instance p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;

    iget-object p2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {p2}, Lcom/squareup/workflow/rx1/Workflow;->getResult()Lrx/Single;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lio/reactivex/Single;)V

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->resultWorker:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;

    return-void

    .line 116
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;ILjava/lang/Object;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->copy(Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;*TOutputT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/ScreenState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;*+TOutputT;>;",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;)",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "runningWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;-><init>(Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    iget-object v1, p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    iget-object p1, p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCancellationWorker()Lcom/squareup/workflow/LifecycleWorker;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->cancellationWorker:Lcom/squareup/workflow/LifecycleWorker;

    return-object v0
.end method

.method public final getChildStateWorker()Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;>;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->childStateWorker:Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final getLastLegacyState()Lcom/squareup/workflow/ScreenState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    return-object v0
.end method

.method public final getResultWorker()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>.ResultWorker;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->resultWorker:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;

    return-object v0
.end method

.method public final getRunningWorkflow()Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;*TOutputT;>;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenWorkflowAdapterState(runningWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->runningWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastLegacyState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->lastLegacyState:Lcom/squareup/workflow/ScreenState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
