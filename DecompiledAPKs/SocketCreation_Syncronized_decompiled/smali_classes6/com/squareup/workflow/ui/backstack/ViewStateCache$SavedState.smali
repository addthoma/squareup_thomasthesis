.class public final Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ViewStateCache.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/backstack/ViewStateCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SavedState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState$CREATOR;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0019\u0008\u0016\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u000f\u0008\u0016\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;",
        "Landroid/view/View$BaseSavedState;",
        "superState",
        "Landroid/os/Parcelable;",
        "viewStateCache",
        "Lcom/squareup/workflow/ui/backstack/ViewStateCache;",
        "(Landroid/os/Parcelable;Lcom/squareup/workflow/ui/backstack/ViewStateCache;)V",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)V",
        "getViewStateCache",
        "()Lcom/squareup/workflow/ui/backstack/ViewStateCache;",
        "writeToParcel",
        "",
        "out",
        "flags",
        "",
        "CREATOR",
        "backstack-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState$CREATOR;


# instance fields
.field private final viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->CREATOR:Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState$CREATOR;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 125
    const-class v0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    iput-object p1, p0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Lcom/squareup/workflow/ui/backstack/ViewStateCache;)V
    .locals 1

    const-string v0, "viewStateCache"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 121
    iput-object p2, p0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    return-void
.end method


# virtual methods
.method public final getViewStateCache()Lcom/squareup/workflow/ui/backstack/ViewStateCache;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "out"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 135
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
