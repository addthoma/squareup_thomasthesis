.class public final Lcom/squareup/workflow/ui/HandlesBackKt;
.super Ljava/lang/Object;
.source "HandlesBack.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "setBackHandler",
        "",
        "Landroid/view/View;",
        "handler",
        "Lkotlin/Function0;",
        "container_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$setBackHandler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/squareup/workflow/ui/HandlesBackKt$setBackHandler$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/ui/HandlesBackKt$setBackHandler$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/ui/HandlesBack;

    invoke-static {p0, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setConditionalBackHandler(Landroid/view/View;Lcom/squareup/workflow/ui/HandlesBack;)V

    return-void
.end method
