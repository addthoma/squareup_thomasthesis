.class public final Lcom/squareup/workflow/ui/WorkflowLayout;
.super Landroid/widget/FrameLayout;
.source "WorkflowLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowLayout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowLayout.kt\ncom/squareup/workflow/ui/WorkflowLayout\n*L\n1#1,154:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001!B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\tH\u0014J\n\u0010\u000f\u001a\u0004\u0018\u00010\tH\u0014J\u0018\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u001e\u0010\u0015\u001a\u00020\r2\u000e\u0010\u0016\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00120\u00172\u0006\u0010\u0013\u001a\u00020\u0014J\u001e\u0010\u0015\u001a\u00020\r2\u000e\u0010\u0016\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00120\u00172\u0006\u0010\u0018\u001a\u00020\u0019J8\u0010\u001a\u001a\u00020\r\"\u0008\u0008\u0000\u0010\u001b*\u00020\u0012*\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u00172\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u0002H\u001b\u0012\u0004\u0012\u00020\r0\u001fH\u0002J\u000c\u0010 \u001a\u00020\u0014*\u00020\u0014H\u0002R\u0016\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowLayout;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attributeSet",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "restoredChildState",
        "Landroid/util/SparseArray;",
        "Landroid/os/Parcelable;",
        "showing",
        "Lcom/squareup/workflow/ui/WorkflowViewStub;",
        "onRestoreInstanceState",
        "",
        "state",
        "onSaveInstanceState",
        "show",
        "newRendering",
        "",
        "hints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "start",
        "renderings",
        "Lio/reactivex/Observable;",
        "registry",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "takeWhileAttached",
        "S",
        "Landroid/view/View;",
        "source",
        "update",
        "Lkotlin/Function1;",
        "withDefaultViewBindings",
        "SavedState",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private restoredChildState:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private final showing:Lcom/squareup/workflow/ui/WorkflowViewStub;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance p2, Lcom/squareup/workflow/ui/WorkflowViewStub;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/workflow/ui/WorkflowViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 41
    move-object p1, p2

    check-cast p1, Landroid/view/View;

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/squareup/workflow/ui/WorkflowLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->showing:Lcom/squareup/workflow/ui/WorkflowViewStub;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 38
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/ui/WorkflowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static final synthetic access$show(Lcom/squareup/workflow/ui/WorkflowLayout;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/ui/WorkflowLayout;->show(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method

.method private final show(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->showing:Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/workflow/ui/WorkflowViewStub;->update(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    .line 79
    iget-object p1, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->restoredChildState:Landroid/util/SparseArray;

    if-eqz p1, :cond_0

    const/4 p2, 0x0

    .line 80
    check-cast p2, Landroid/util/SparseArray;

    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->restoredChildState:Landroid/util/SparseArray;

    .line 81
    iget-object p2, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->showing:Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {p2}, Lcom/squareup/workflow/ui/WorkflowViewStub;->getActual()Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method private final takeWhileAttached(Landroid/view/View;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "Lio/reactivex/Observable<",
            "TS;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 139
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;

    invoke-direct {v0, p2, p3}, Lcom/squareup/workflow/ui/WorkflowLayout$takeWhileAttached$listener$1;-><init>(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)V

    .line 151
    check-cast v0, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method

.method private final withDefaultViewBindings(Lcom/squareup/workflow/ui/ContainerHints;)Lcom/squareup/workflow/ui/ContainerHints;
    .locals 3

    .line 72
    sget-object v0, Lcom/squareup/workflow/ui/ViewRegistry;->Companion:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    sget-object v1, Lcom/squareup/workflow/ui/ViewRegistry;->Companion:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    check-cast v1, Lcom/squareup/workflow/ui/ContainerHintKey;

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/ui/ContainerHints;->get(Lcom/squareup/workflow/ui/ContainerHintKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/ui/ViewRegistry;

    invoke-static {}, Lcom/squareup/workflow/ui/ViewRegistryKt;->getDefaultViewBindings()Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/ViewRegistryKt;->plus(Lcom/squareup/workflow/ui/ViewRegistry;Lcom/squareup/workflow/ui/ViewRegistry;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/ui/ContainerHints;->plus(Lkotlin/Pair;)Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 94
    instance-of v0, p1, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    check-cast v0, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;

    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {v0}, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;->getChildState()Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->restoredChildState:Landroid/util/SparseArray;

    .line 96
    check-cast p1, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1

    .line 98
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 87
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 88
    :cond_0
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iget-object v2, p0, Lcom/squareup/workflow/ui/WorkflowLayout;->showing:Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {v2}, Lcom/squareup/workflow/ui/WorkflowViewStub;->getActual()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 86
    new-instance v2, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;

    invoke-direct {v2, v0, v1}, Lcom/squareup/workflow/ui/WorkflowLayout$SavedState;-><init>(Landroid/os/Parcelable;Landroid/util/SparseArray;)V

    check-cast v2, Landroid/os/Parcelable;

    return-object v2
.end method

.method public final start(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")V"
        }
    .end annotation

    const-string v0, "renderings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0, p2}, Lcom/squareup/workflow/ui/WorkflowLayout;->withDefaultViewBindings(Lcom/squareup/workflow/ui/ContainerHints;)Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object p2

    .line 68
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/workflow/ui/WorkflowLayout$start$1;-><init>(Lcom/squareup/workflow/ui/WorkflowLayout;Lcom/squareup/workflow/ui/ContainerHints;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p0, p1, v0}, Lcom/squareup/workflow/ui/WorkflowLayout;->takeWhileAttached(Landroid/view/View;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final start(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ViewRegistry;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            ")V"
        }
    .end annotation

    const-string v0, "renderings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "registry"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lcom/squareup/workflow/ui/ContainerHints;

    invoke-direct {v0, p2}, Lcom/squareup/workflow/ui/ContainerHints;-><init>(Lcom/squareup/workflow/ui/ViewRegistry;)V

    invoke-virtual {p0, p1, v0}, Lcom/squareup/workflow/ui/WorkflowLayout;->start(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
