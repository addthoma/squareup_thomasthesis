.class public final Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RealDialogBinding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding<",
        "TD;TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0004BA\u0008\u0000\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006\u0012$\u0010\u0007\u001a \u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n0\t\u0012\u0004\u0012\u00020\u000b0\u0008\u00a2\u0006\u0002\u0010\u000cJH\u0010\u0013\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u00172\u0012\u0010\u0018\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00192\u001a\u0010\u001a\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u001b0\tH\u0016R,\u0010\u0007\u001a \u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n0\t\u0012\u0004\u0012\u00020\u000b0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000e8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R \u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;",
        "D",
        "",
        "E",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "dialogFactoryForScreens",
        "Lkotlin/Function1;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/DialogFactory;",
        "(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)V",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "promiseDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dialogFactoryForScreens:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;",
            "Lcom/squareup/workflow/DialogFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final key:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;+",
            "Lcom/squareup/workflow/DialogFactory;",
            ">;)V"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialogFactoryForScreens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p2, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;->dialogFactoryForScreens:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    .line 188
    sget-object v0, Lcom/squareup/workflow/ScreenHint;->Companion:Lcom/squareup/workflow/ScreenHint$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/ScreenHint$Companion;->forDialog()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public promiseDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "contextForNewDialog"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 195
    invoke-virtual {p0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "promiseDialog: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;->dialogFactoryForScreens:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/squareup/workflow/legacy/Screen$Key;->cast(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/squareup/workflow/legacy/ScreenKt;->ofKeyType(Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;)Lio/reactivex/Observable;

    move-result-object p2

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/DialogFactory;

    .line 197
    invoke-interface {p2, p1}, Lcom/squareup/workflow/DialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
