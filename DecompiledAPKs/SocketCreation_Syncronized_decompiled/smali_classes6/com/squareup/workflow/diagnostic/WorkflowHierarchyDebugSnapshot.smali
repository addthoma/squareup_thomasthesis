.class public final Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;
.super Ljava/lang/Object;
.source "WorkflowHierarchyDebugSnapshot.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;,
        Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowHierarchyDebugSnapshot.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowHierarchyDebugSnapshot.kt\ncom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot\n*L\n1#1,132:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002()BG\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0001\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0001\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0008\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00c6\u0003J\u000f\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J\u000f\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0008H\u00c6\u0003JW\u0010 \u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0008H\u00c6\u0001J\u0013\u0010!\u001a\u00020\"2\u0008\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\u0006\u0010&\u001a\u00020\u0003J\u0008\u0010\'\u001a\u00020\u0003H\u0016R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001b\u0010\u000f\u001a\u00020\u00038BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0015R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0011\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;",
        "",
        "workflowType",
        "",
        "props",
        "state",
        "rendering",
        "children",
        "",
        "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;",
        "workers",
        "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;",
        "(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;)V",
        "getChildren",
        "()Ljava/util/List;",
        "lazyDescription",
        "getLazyDescription",
        "()Ljava/lang/String;",
        "lazyDescription$delegate",
        "Lkotlin/Lazy;",
        "getProps",
        "()Ljava/lang/Object;",
        "getRendering",
        "getState",
        "getWorkers",
        "getWorkflowType",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toDescriptionString",
        "toString",
        "ChildWorker",
        "ChildWorkflow",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final lazyDescription$delegate:Lkotlin/Lazy;

.field private final props:Ljava/lang/Object;

.field private final rendering:Ljava/lang/Object;

.field private final state:Ljava/lang/Object;

.field private final workers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "lazyDescription"

    const-string v4, "getLazyDescription()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;",
            ">;)V"
        }
    .end annotation

    const-string v0, "workflowType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "children"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workers"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    iput-object p4, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    iput-object p5, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    .line 69
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->PUBLICATION:Lkotlin/LazyThreadSafetyMode;

    new-instance p2, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$lazyDescription$2;

    invoke-direct {p2, p0}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$lazyDescription$2;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lkotlin/LazyKt;->lazy(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->lazyDescription$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->copy(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;)Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    move-result-object p0

    return-object p0
.end method

.method private final getLazyDescription()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->lazyDescription$delegate:Lkotlin/Lazy;

    sget-object v1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    return-object v0
.end method

.method public final component3()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    return-object v0
.end method

.method public final component4()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;)Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;",
            ">;)",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;"
        }
    .end annotation

    const-string v0, "workflowType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "children"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workers"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    return-object v0
.end method

.method public final getProps()Ljava/lang/Object;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    return-object v0
.end method

.method public final getRendering()Ljava/lang/Object;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final getState()Ljava/lang/Object;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    return-object v0
.end method

.method public final getWorkers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    return-object v0
.end method

.method public final getWorkflowType()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workflowType:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->props:Ljava/lang/Object;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->state:Ljava/lang/Object;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->rendering:Ljava/lang/Object;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->children:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->workers:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final toDescriptionString()Ljava/lang/String;
    .locals 1

    .line 84
    invoke-direct {p0}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->getLazyDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n    |WorkflowHierarchyDebugSnapshot(\n    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;->toDescriptionString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->trimEnd(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "|  "

    invoke-static {v1, v2}, Lkotlin/text/StringsKt;->prependIndent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n    |)\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "|"

    .line 79
    invoke-static {v0, v1}, Lkotlin/text/StringsKt;->trimMargin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 77
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
