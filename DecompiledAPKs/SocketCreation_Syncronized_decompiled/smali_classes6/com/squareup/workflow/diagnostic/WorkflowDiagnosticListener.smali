.class public interface abstract Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;
.super Ljava/lang/Object;
.source "WorkflowDiagnosticListener.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0001H\u0017J\u0008\u0010\u0005\u001a\u00020\u0003H\u0017J\u001a\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0001H\u0017J\u0012\u0010\t\u001a\u00020\u00032\u0008\u0010\n\u001a\u0004\u0018\u00010\u0001H\u0017J\u0008\u0010\u000b\u001a\u00020\u0003H\u0017J$\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\n\u001a\u0004\u0018\u00010\u00012\u0008\u0010\r\u001a\u0004\u0018\u00010\u0001H\u0017J?\u0010\u000e\u001a\u00020\u00032\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u0017\u00a2\u0006\u0002\u0010\u0013J\u0018\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0017J\u0008\u0010\u0019\u001a\u00020\u0003H\u0017J \u0010\u001a\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u000e\u0010\u001b\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001cH\u0017J \u0010\u001d\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00082\u0006\u0010\u001f\u001a\u00020\u00082\u0006\u0010 \u001a\u00020\u0001H\u0017J(\u0010!\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00082\u0006\u0010\u001f\u001a\u00020\u00082\u0006\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020\u0018H\u0017J\u0018\u0010$\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00082\u0006\u0010\u001f\u001a\u00020\u0008H\u0017J>\u0010%\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u000e\u0010\u001b\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001c2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00012\u0008\u0010 \u001a\u0004\u0018\u00010\u0001H\u0017JK\u0010&\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\'\u001a\u0004\u0018\u00010\u00082\u0006\u0010(\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020\u00182\u0008\u0010)\u001a\u0004\u0018\u00010\u00012\u0008\u0010*\u001a\u0004\u0018\u00010\u00012\u0006\u0010+\u001a\u00020,H\u0017\u00a2\u0006\u0002\u0010-J\u0010\u0010.\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H\u0017\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "",
        "onAfterRenderPass",
        "",
        "rendering",
        "onAfterSnapshotPass",
        "onAfterWorkflowRendered",
        "workflowId",
        "",
        "onBeforeRenderPass",
        "props",
        "onBeforeSnapshotPass",
        "onBeforeWorkflowRendered",
        "state",
        "onPropsChanged",
        "oldProps",
        "newProps",
        "oldState",
        "newState",
        "(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V",
        "onRuntimeStarted",
        "workflowScope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "rootWorkflowType",
        "",
        "onRuntimeStopped",
        "onSinkReceived",
        "action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "onWorkerOutput",
        "workerId",
        "parentWorkflowId",
        "output",
        "onWorkerStarted",
        "key",
        "description",
        "onWorkerStopped",
        "onWorkflowAction",
        "onWorkflowStarted",
        "parentId",
        "workflowType",
        "initialProps",
        "initialState",
        "restoredFromSnapshot",
        "",
        "(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V",
        "onWorkflowStopped",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onAfterRenderPass(Ljava/lang/Object;)V
.end method

.method public abstract onAfterSnapshotPass()V
.end method

.method public abstract onAfterWorkflowRendered(JLjava/lang/Object;)V
.end method

.method public abstract onBeforeRenderPass(Ljava/lang/Object;)V
.end method

.method public abstract onBeforeSnapshotPass()V
.end method

.method public abstract onBeforeWorkflowRendered(JLjava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract onPropsChanged(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public abstract onRuntimeStarted(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V
.end method

.method public abstract onRuntimeStopped()V
.end method

.method public abstract onSinkReceived(JLcom/squareup/workflow/WorkflowAction;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;)V"
        }
    .end annotation
.end method

.method public abstract onWorkerOutput(JJLjava/lang/Object;)V
.end method

.method public abstract onWorkerStarted(JJLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onWorkerStopped(JJ)V
.end method

.method public abstract onWorkflowAction(JLcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onWorkflowStarted(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V
.end method

.method public abstract onWorkflowStopped(J)V
.end method
