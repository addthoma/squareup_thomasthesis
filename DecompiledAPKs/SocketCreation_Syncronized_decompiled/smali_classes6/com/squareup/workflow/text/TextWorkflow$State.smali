.class public final Lcom/squareup/workflow/text/TextWorkflow$State;
.super Ljava/lang/Object;
.source "TextWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/text/TextWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0011\u0008\u0016\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u000c\u001a\u00020\u0000J\u0008\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/workflow/text/TextWorkflow$State;",
        "",
        "sequenceNum",
        "",
        "(I)V",
        "workflowToken",
        "Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;",
        "(ILcom/squareup/workflow/text/TextWorkflow$WorkflowToken;)V",
        "getSequenceNum",
        "()I",
        "getWorkflowToken",
        "()Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;",
        "incrementSequenceNum",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final sequenceNum:I

.field private final workflowToken:Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;

    invoke-direct {v0}, Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/squareup/workflow/text/TextWorkflow$State;-><init>(ILcom/squareup/workflow/text/TextWorkflow$WorkflowToken;)V

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 37
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/workflow/text/TextWorkflow$State;-><init>(I)V

    return-void
.end method

.method private constructor <init>(ILcom/squareup/workflow/text/TextWorkflow$WorkflowToken;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->sequenceNum:I

    iput-object p2, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->workflowToken:Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;

    return-void
.end method


# virtual methods
.method public final getSequenceNum()I
    .locals 1

    .line 34
    iget v0, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->sequenceNum:I

    return v0
.end method

.method public final getWorkflowToken()Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->workflowToken:Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;

    return-object v0
.end method

.method public final incrementSequenceNum()Lcom/squareup/workflow/text/TextWorkflow$State;
    .locals 3

    .line 39
    new-instance v0, Lcom/squareup/workflow/text/TextWorkflow$State;

    iget v1, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->sequenceNum:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->workflowToken:Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;

    invoke-direct {v0, v1, v2}, Lcom/squareup/workflow/text/TextWorkflow$State;-><init>(ILcom/squareup/workflow/text/TextWorkflow$WorkflowToken;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextWorkflowState(sequenceNum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/workflow/text/TextWorkflow$State;->sequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
