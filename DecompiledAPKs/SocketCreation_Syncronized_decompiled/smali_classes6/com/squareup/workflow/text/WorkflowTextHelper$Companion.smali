.class public final Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;
.super Ljava/lang/Object;
.source "WorkflowTextHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/text/WorkflowTextHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowTextHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowTextHelper.kt\ncom/squareup/workflow/text/WorkflowTextHelper$Companion\n*L\n1#1,125:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;",
        "",
        "()V",
        "createForView",
        "Lcom/squareup/workflow/text/WorkflowTextHelper;",
        "view",
        "Landroid/widget/TextView;",
        "getForView",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createForView(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper;
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    new-instance v0, Lcom/squareup/workflow/text/WorkflowTextHelper;

    invoke-static {p1}, Lcom/squareup/workflow/text/EditTextsKt;->asTextViewLike(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/workflow/text/WorkflowTextHelper;-><init>(Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;)V

    .line 120
    move-object v1, v0

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    sget v1, Lcom/squareup/workflow/text/R$id;->workflow_text_helper:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    return-object v0
.end method

.method public final getForView(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    sget v0, Lcom/squareup/workflow/text/R$id;->workflow_text_helper:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/workflow/text/WorkflowTextHelper;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/workflow/text/WorkflowTextHelper;

    return-object p1
.end method
