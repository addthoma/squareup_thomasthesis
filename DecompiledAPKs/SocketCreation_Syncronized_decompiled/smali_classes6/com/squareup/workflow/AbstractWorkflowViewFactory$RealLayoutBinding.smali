.class public final Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RealLayoutBinding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding<",
        "TD;TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0004Ba\u0008\u0000\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012*\u0010\r\u001a&\u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00100\u000f\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u000e\u00a2\u0006\u0002\u0010\u0013JP\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0012\u0010\u001e\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u001f2\u0018\u0010 \u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`!0\u000f2\u0006\u0010\"\u001a\u00020\u0011H\u0016R2\u0010\r\u001a&\u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00100\u000f\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;",
        "D",
        "",
        "E",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "layoutId",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "inflater",
        "Lcom/squareup/workflow/InflaterDelegate;",
        "coordinatorForScreens",
        "Lkotlin/Function2;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Lcom/squareup/coordinators/Coordinator;",
        "(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function2;)V",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "inflate",
        "Landroid/view/View;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "containerHints",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final coordinatorForScreens:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lcom/squareup/coordinators/Coordinator;",
            ">;"
        }
    .end annotation
.end field

.field private final hint:Lcom/squareup/workflow/ScreenHint;

.field private final inflater:Lcom/squareup/workflow/InflaterDelegate;

.field private final key:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation
.end field

.field private final layoutId:I


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;I",
            "Lcom/squareup/workflow/ScreenHint;",
            "Lcom/squareup/workflow/InflaterDelegate;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;-",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "+",
            "Lcom/squareup/coordinators/Coordinator;",
            ">;)V"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coordinatorForScreens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    iput p2, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->layoutId:I

    iput-object p3, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->hint:Lcom/squareup/workflow/ScreenHint;

    iput-object p4, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->inflater:Lcom/squareup/workflow/InflaterDelegate;

    iput-object p5, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->coordinatorForScreens:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static final synthetic access$getCoordinatorForScreens$p(Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;)Lkotlin/jvm/functions/Function2;
    .locals 0

    .line 157
    iget-object p0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->coordinatorForScreens:Lkotlin/jvm/functions/Function2;

    return-object p0
.end method


# virtual methods
.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->hint:Lcom/squareup/workflow/ScreenHint;

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "contextForNewView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 173
    invoke-virtual {p0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "inflate: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->inflater:Lcom/squareup/workflow/InflaterDelegate;

    iget v1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->layoutId:I

    invoke-interface {v0, p1, p2, v1}, Lcom/squareup/workflow/InflaterDelegate;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    .line 175
    invoke-static {p1, p3}, Lcom/squareup/workflow/WorkflowViewFactoryKt;->setKey(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen$Key;)V

    .line 176
    new-instance p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;

    invoke-direct {p2, p0, p4, p3, p5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;-><init>(Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ui/ContainerHints;)V

    check-cast p2, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {p1, p2}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    return-object p1
.end method
