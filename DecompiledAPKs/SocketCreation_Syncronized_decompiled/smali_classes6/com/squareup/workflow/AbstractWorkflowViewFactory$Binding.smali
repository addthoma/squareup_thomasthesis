.class public interface abstract Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Binding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u00020\u0002R\u0012\u0010\u0004\u001a\u00020\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u001e\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "D",
        "",
        "E",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getHint()Lcom/squareup/workflow/ScreenHint;
.end method

.method public abstract getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation
.end method
