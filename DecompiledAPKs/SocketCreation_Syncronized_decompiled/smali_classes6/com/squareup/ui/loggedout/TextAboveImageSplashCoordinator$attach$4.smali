.class public final Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "TextAboveImageSplashCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4",
        "Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;",
        "onPageSelected",
        "",
        "position",
        "",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4;->this$0:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 1

    .line 74
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;->onPageSelected(I)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4;->this$0:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->access$getConfig$p(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$SplashPage;->getViewEvent()Lcom/squareup/analytics/RegisterViewName;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4;->this$0:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->access$getAnalytics$p(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    :cond_0
    return-void
.end method
