.class public Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
.super Ljava/lang/Object;
.source "LoggedOutScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/loggedout/LandingScreen$Runner;
.implements Lcom/squareup/loggedout/CreateAccountStarter;
.implements Lcom/squareup/ui/login/CreateAccountScreen$Runner;
.implements Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory$FinalizeLoginFailureDialogActions;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/loggedout/LoggedOutActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoggedOutScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoggedOutScopeRunner.kt\ncom/squareup/ui/loggedout/LoggedOutScopeRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,375:1\n1642#2,2:376\n*E\n*S KotlinDebug\n*F\n+ 1 LoggedOutScopeRunner.kt\ncom/squareup/ui/loggedout/LoggedOutScopeRunner\n*L\n149#1,2:376\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\u0008\u0017\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005B\u00b6\u0001\u0008\u0007\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0013\u0008\u0001\u0010(\u001a\r\u0012\t\u0012\u00070\u0001\u00a2\u0006\u0002\u0008*0)\u0012\u0006\u0010+\u001a\u00020,\u00a2\u0006\u0002\u0010-J\u0008\u0010H\u001a\u000200H\u0016J\u0008\u0010I\u001a\u00020AH\u0016J\u0008\u0010J\u001a\u000200H\u0016J\u0018\u0010K\u001a\u0002002\u000e\u0010L\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020N0MH\u0002J\u0010\u0010O\u001a\u0002002\u0006\u0010P\u001a\u00020EH\u0002J\u0006\u0010Q\u001a\u00020NJ\u0008\u0010R\u001a\u000200H\u0016J\u0008\u0010S\u001a\u000200H\u0016J\u0006\u0010T\u001a\u000200J\r\u0010U\u001a\u000200H\u0000\u00a2\u0006\u0002\u0008VJ\u0010\u0010W\u001a\u0002002\u0006\u0010X\u001a\u00020YH\u0007J\u0008\u0010Z\u001a\u000200H\u0016J\u0008\u0010[\u001a\u000200H\u0016J\u0010\u0010\\\u001a\u0002002\u0006\u0010]\u001a\u00020^H\u0016J\u0008\u0010_\u001a\u000200H\u0016J\u0010\u0010`\u001a\u0002002\u0006\u0010a\u001a\u00020bH\u0002J\u0008\u0010c\u001a\u000200H\u0016J\u000e\u0010d\u001a\u0002002\u0006\u0010e\u001a\u00020EJ\u000e\u0010f\u001a\u0002002\u0006\u0010g\u001a\u00020EJ\u0008\u0010h\u001a\u000200H\u0002J\u0010\u0010i\u001a\u0002002\u0006\u0010P\u001a\u00020EH\u0016J\u0010\u0010j\u001a\u0002002\u0006\u0010P\u001a\u00020EH\u0002J\u0016\u0010k\u001a\u0002002\u0006\u0010l\u001a\u00020m2\u0006\u0010n\u001a\u00020EJ\u0006\u0010o\u001a\u000200J\u0010\u0010o\u001a\u0002002\u0006\u0010p\u001a\u00020AH\u0016J\u0012\u0010q\u001a\u0002002\u0008\u0008\u0002\u0010r\u001a\u00020AH\u0016J\u0010\u0010s\u001a\u0002002\u0006\u0010t\u001a\u00020uH\u0016R2\u0010.\u001a&\u0012\u000c\u0012\n 1*\u0004\u0018\u00010000 1*\u0012\u0012\u000c\u0012\n 1*\u0004\u0018\u00010000\u0018\u00010/0/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010(\u001a\r\u0012\t\u0012\u00070\u0001\u00a2\u0006\u0002\u0008*0)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u00102\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u00085\u00106\u001a\u0004\u00083\u00104R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u00107\u001a\u0008\u0012\u0004\u0012\u000200088G\u00a2\u0006\u0006\u001a\u0004\u00087\u00109R\u001b\u0010:\u001a\u00020\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008=\u00106\u001a\u0004\u0008;\u0010<R\u000e\u0010>\u001a\u00020?X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020AX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020CX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010D\u001a\u0004\u0018\u00010E*\u00020\u001f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008F\u0010G\u00a8\u0006v"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/ui/loggedout/LandingScreen$Runner;",
        "Lcom/squareup/loggedout/CreateAccountStarter;",
        "Lcom/squareup/ui/login/CreateAccountScreen$Runner;",
        "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory$FinalizeLoginFailureDialogActions;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "lazyContainer",
        "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "legacyAuthenticator",
        "Lcom/squareup/account/LegacyAuthenticator;",
        "accountStatusService",
        "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
        "postInstallEncryptedEmailSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
        "onboardingStarter",
        "Lcom/squareup/onboarding/OnboardingStarter;",
        "landingScreenSelector",
        "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
        "tourPresenter",
        "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
        "tourPager",
        "Lcom/squareup/ui/tour/LearnMoreTourPager;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "singleSignOn",
        "Lcom/squareup/singlesignon/SingleSignOn;",
        "landingCanceledListener",
        "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
        "createAccountCanceledListener",
        "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
        "adAnalytics",
        "Lcom/squareup/adanalytics/AdAnalytics;",
        "finalizeLoginFailureDialogFactory",
        "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
        "additionalServices",
        "",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "finalLogInCheck",
        "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
        "(Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/badbus/BadBus;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/ui/loggedout/LandingScreenSelector;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;Lcom/squareup/ui/tour/LearnMoreTourPager;Lcom/squareup/analytics/Analytics;Lcom/squareup/singlesignon/SingleSignOn;Lcom/squareup/ui/loggedout/LandingCanceledListener;Lcom/squareup/ui/login/CreateAccountCanceledListener;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/util/Set;Lcom/squareup/ui/loggedout/FinalLogInCheck;)V",
        "_finishActivity",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "container",
        "getContainer",
        "()Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
        "container$delegate",
        "Ldagger/Lazy;",
        "finishActivity",
        "Lrx/Observable;",
        "()Lrx/Observable;",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "generalDisposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "preventPaymentActivityReset",
        "",
        "subscriptionsRequiringFlow",
        "Lrx/subscriptions/CompositeSubscription;",
        "sessionTokenOrNull",
        "",
        "getSessionTokenOrNull",
        "(Lcom/squareup/singlesignon/SingleSignOn;)Ljava/lang/String;",
        "abandonFinalizeLogin",
        "canShowLearnMore",
        "exitFinalizeLogin",
        "exitLoginFlowFrom",
        "screenClass",
        "Ljava/lang/Class;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "finalizeLogIn",
        "sessionToken",
        "getFirstScreen",
        "goToActivation",
        "goToPaymentActivity",
        "maybeSignInWithSingleSignOnSessionToken",
        "onAuthenticatorCanceled",
        "onAuthenticatorCanceled$loggedout_release",
        "onAuthenticatorResult",
        "result",
        "Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult;",
        "onBackPressed",
        "onCreateAccountCanceled",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onJailKeeperStateChanged",
        "state",
        "Lcom/squareup/jailkeeper/JailKeeper$State;",
        "onLandingScreenCanceled",
        "onReceivedAnonymousVisitorToken",
        "anonymousVisitorToken",
        "onReceivedEncryptedEmail",
        "encryptedEmail",
        "resetPaymentActivity",
        "retryFinalizeLogin",
        "showFinalizeLoginFailure",
        "showLearnMore",
        "countryType",
        "Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;",
        "callToActionText",
        "startCreateAccount",
        "cancelOnBack",
        "startLoginFlow",
        "asLandingScreen",
        "warn",
        "warning",
        "Lcom/squareup/widgets/warning/Warning;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final _finishActivity:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final additionalServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final container$delegate:Ldagger/Lazy;

.field private final createAccountCanceledListener:Lcom/squareup/ui/login/CreateAccountCanceledListener;

.field private final finalLogInCheck:Lcom/squareup/ui/loggedout/FinalLogInCheck;

.field private final finalizeLoginFailureDialogFactory:Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

.field private final flow$delegate:Ldagger/Lazy;

.field private final generalDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final landingCanceledListener:Lcom/squareup/ui/loggedout/LandingCanceledListener;

.field private final landingScreenSelector:Lcom/squareup/ui/loggedout/LandingScreenSelector;

.field private final legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

.field private final postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;"
        }
    .end annotation
.end field

.field private preventPaymentActivityReset:Z

.field private final singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

.field private final subscriptionsRequiringFlow:Lrx/subscriptions/CompositeSubscription;

.field private final tourPager:Lcom/squareup/ui/tour/LearnMoreTourPager;

.field private final tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "flow"

    const-string v5, "getFlow()Lflow/Flow;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "container"

    const-string v4, "getContainer()Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/badbus/BadBus;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/ui/loggedout/LandingScreenSelector;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;Lcom/squareup/ui/tour/LearnMoreTourPager;Lcom/squareup/analytics/Analytics;Lcom/squareup/singlesignon/SingleSignOn;Lcom/squareup/ui/loggedout/LandingCanceledListener;Lcom/squareup/ui/login/CreateAccountCanceledListener;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/util/Set;Lcom/squareup/ui/loggedout/FinalLogInCheck;)V
    .locals 16
    .param p17    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/ForLoggedOut;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/account/LegacyAuthenticator;",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            "Lcom/squareup/ui/tour/LearnMoreTourPager;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
            "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "lazyFlow"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyContainer"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "legacyAuthenticator"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusService"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postInstallEncryptedEmailSetting"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingStarter"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "landingScreenSelector"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tourPresenter"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tourPager"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "singleSignOn"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "landingCanceledListener"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createAccountCanceledListener"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adAnalytics"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "finalizeLoginFailureDialogFactory"

    move-object/from16 v2, p16

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalServices"

    move-object/from16 v2, p17

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "finalLogInCheck"

    move-object/from16 v1, p18

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p16

    iput-object v3, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    iput-object v4, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

    iput-object v5, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iput-object v6, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    iput-object v7, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    iput-object v8, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->landingScreenSelector:Lcom/squareup/ui/loggedout/LandingScreenSelector;

    iput-object v9, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    iput-object v10, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->tourPager:Lcom/squareup/ui/tour/LearnMoreTourPager;

    iput-object v11, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object v12, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    iput-object v13, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->landingCanceledListener:Lcom/squareup/ui/loggedout/LandingCanceledListener;

    iput-object v14, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->createAccountCanceledListener:Lcom/squareup/ui/login/CreateAccountCanceledListener;

    iput-object v15, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalizeLoginFailureDialogFactory:Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

    iput-object v2, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->additionalServices:Ljava/util/Set;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalLogInCheck:Lcom/squareup/ui/loggedout/FinalLogInCheck;

    move-object/from16 v1, p1

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->flow$delegate:Ldagger/Lazy;

    move-object/from16 v1, p2

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->container$delegate:Ldagger/Lazy;

    .line 104
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->_finishActivity:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 131
    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->subscriptionsRequiringFlow:Lrx/subscriptions/CompositeSubscription;

    .line 133
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->generalDisposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method public static final synthetic access$getAdAnalytics$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/adanalytics/AdAnalytics;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getFinalLogInCheck$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/ui/loggedout/FinalLogInCheck;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalLogInCheck:Lcom/squareup/ui/loggedout/FinalLogInCheck;

    return-object p0
.end method

.method public static final synthetic access$getLegacyAuthenticator$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/account/LegacyAuthenticator;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-object p0
.end method

.method public static final synthetic access$get_finishActivity$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->_finishActivity:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$onJailKeeperStateChanged(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onJailKeeperStateChanged(Lcom/squareup/jailkeeper/JailKeeper$State;)V

    return-void
.end method

.method public static final synthetic access$showFinalizeLoginFailure(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Ljava/lang/String;)V
    .locals 0

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->showFinalizeLoginFailure(Ljava/lang/String;)V

    return-void
.end method

.method private final exitLoginFlowFrom(Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 209
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exitLoginFlowFrom["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x5d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$exitLoginFlowFrom$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$exitLoginFlowFrom$1;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Ljava/lang/Class;)V

    check-cast v3, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    invoke-direct {v1, v2, v3}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final finalizeLogIn(Ljava/lang/String;)V
    .locals 3

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->generalDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v1, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->observeStatus(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 341
    new-instance v2, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "accountStatusService.obs\u2026en)\n          }\n        }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 340
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private final getContainer()Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->container$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    return-object v0
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method

.method private final getSessionTokenOrNull(Lcom/squareup/singlesignon/SingleSignOn;)Ljava/lang/String;
    .locals 1

    .line 373
    invoke-interface {p1}, Lcom/squareup/singlesignon/SingleSignOn;->hasSessionToken()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/singlesignon/SingleSignOn;->getSessionToken()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final onJailKeeperStateChanged(Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 3

    .line 152
    sget-object v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/jailkeeper/JailKeeper$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected JailKeeper.State in Login: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 153
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->goToPaymentActivity()V

    :goto_0
    return-void
.end method

.method private final resetPaymentActivity()V
    .locals 1

    .line 287
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getContainer()Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/PaymentActivity;->reset(Landroid/content/Context;)V

    return-void
.end method

.method private final showFinalizeLoginFailure(Ljava/lang/String;)V
    .locals 3

    .line 369
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalizeLoginFailureDialogFactory:Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;-><init>(Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic startLoginFlow$default(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;ZILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 181
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startLoginFlow(Z)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: startLoginFlow"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abandonFinalizeLogin()V
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->logOut()V

    return-void
.end method

.method public canShowLearnMore()Z
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->tourPager:Lcom/squareup/ui/tour/LearnMoreTourPager;

    invoke-interface {v0}, Lcom/squareup/ui/tour/LearnMoreTourPager;->getLearnMoreItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "tourPager.learnMoreItems"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public exitFinalizeLogin()V
    .locals 1

    .line 323
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->abandonFinalizeLogin()V

    .line 324
    const-class v0, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->exitLoginFlowFrom(Ljava/lang/Class;)V

    return-void
.end method

.method public final finishActivity()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->_finishActivity:Lcom/jakewharton/rxrelay/PublishRelay;

    const-string v1, "_finishActivity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public final getFirstScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->landingScreenSelector:Lcom/squareup/ui/loggedout/LandingScreenSelector;

    invoke-interface {v0}, Lcom/squareup/ui/loggedout/LandingScreenSelector;->getLandingScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public goToActivation()V
    .locals 5

    const/4 v0, 0x1

    .line 232
    iput-boolean v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->preventPaymentActivityReset:Z

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    new-instance v1, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    sget-object v2, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->START:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingStarter;->startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    return-void
.end method

.method public goToPaymentActivity()V
    .locals 1

    .line 239
    iget-boolean v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->preventPaymentActivityReset:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 240
    iput-boolean v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->preventPaymentActivityReset:Z

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->resetPaymentActivity()V

    :cond_0
    return-void
.end method

.method public final maybeSignInWithSingleSignOnSessionToken()V
    .locals 3

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    const-string v1, "Skipping Single Sign On; already logged in."

    invoke-interface {v0, v1}, Lcom/squareup/singlesignon/SingleSignOn;->logEvent(Ljava/lang/String;)V

    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    invoke-direct {p0, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getSessionTokenOrNull(Lcom/squareup/singlesignon/SingleSignOn;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 309
    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    const-string v2, "Finalizing login with found Single Sign On session token."

    invoke-interface {v1, v2}, Lcom/squareup/singlesignon/SingleSignOn;->logEvent(Ljava/lang/String;)V

    .line 310
    invoke-direct {p0, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalizeLogIn(Ljava/lang/String;)V

    goto :goto_0

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->singleSignOn:Lcom/squareup/singlesignon/SingleSignOn;

    const-string v1, "Skipping Single Sign On; no session token found."

    invoke-interface {v0, v1}, Lcom/squareup/singlesignon/SingleSignOn;->logEvent(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public final onAuthenticatorCanceled$loggedout_release()V
    .locals 4

    .line 194
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onAuthenticatorCanceled$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onAuthenticatorCanceled$1;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v3, "goBackFrom[EmailPassword]"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public final onAuthenticatorResult(Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult;)V
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onAuthenticatorResult: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$Canceled;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$Canceled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onAuthenticatorCanceled$loggedout_release()V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto :goto_0

    .line 298
    :cond_0
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$CreateAccount;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$CreateAccount;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startCreateAccount()V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    goto :goto_0

    .line 299
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$SessionTokenFetched;->getToken()Lcom/squareup/account/SecretString;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalizeLogIn(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public onBackPressed()V
    .locals 1

    .line 283
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getContainer()Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->onBackPressed()V

    return-void
.end method

.method public onCreateAccountCanceled()V
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->createAccountCanceledListener:Lcom/squareup/ui/login/CreateAccountCanceledListener;

    invoke-interface {v0}, Lcom/squareup/ui/login/CreateAccountCanceledListener;->onCreateAccountCanceled()V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->_finishActivity:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 137
    iput-boolean v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->preventPaymentActivityReset:Z

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 140
    new-instance v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "bus.events(SessionExpire\u2026 UserPasswordIncorrect) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 144
    new-instance v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "bus.events(JailKeeper.St\u2026onJailKeeperStateChanged)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->additionalServices:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 376
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 149
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->subscriptionsRequiringFlow:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->generalDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public onLandingScreenCanceled()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->landingCanceledListener:Lcom/squareup/ui/loggedout/LandingCanceledListener;

    invoke-interface {v0}, Lcom/squareup/ui/loggedout/LandingCanceledListener;->onLandingCanceled()V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->_finishActivity:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final onReceivedAnonymousVisitorToken(Ljava/lang/String;)V
    .locals 2

    const-string v0, "anonymousVisitorToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Got an avt token %s from the branch.io link"

    .line 260
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->setAnonymizedUserToken(Ljava/lang/String;)V

    return-void
.end method

.method public final onReceivedEncryptedEmail(Ljava/lang/String;)V
    .locals 2

    const-string v0, "encryptedEmail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    .line 252
    new-instance v1, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;-><init>(Ljava/lang/String;)V

    .line 251
    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 256
    invoke-static {p0, p1, v0, v1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startLoginFlow$default(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;ZILjava/lang/Object;)V

    return-void
.end method

.method public retryFinalizeLogin(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    invoke-direct {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalizeLogIn(Ljava/lang/String;)V

    return-void
.end method

.method public final showLearnMore(Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;Ljava/lang/String;)V
    .locals 2

    const-string v0, "countryType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callToActionText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->tourPresenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    .line 177
    sget-object v1, Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;->PAYMENT:Lcom/squareup/ui/loggedout/AbstractLandingScreen$CountryType;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 178
    :goto_0
    new-instance v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$showLearnMore$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$showLearnMore$1;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    check-cast v1, Ljava/lang/Runnable;

    .line 176
    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->showLearnMore(ZLjava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final startCreateAccount()V
    .locals 1

    const/4 v0, 0x0

    .line 224
    invoke-virtual {p0, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->startCreateAccount(Z)V

    return-void
.end method

.method public startCreateAccount(Z)V
    .locals 2

    .line 227
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/login/CreateAccountScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/CreateAccountScreen;-><init>(Z)V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->assertNotAndGoTo(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public startLoginFlow(Z)V
    .locals 2

    .line 182
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/AuthenticatorBootstrapScreen;-><init>(Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Lcom/squareup/widgets/warning/Warning;)V
    .locals 2

    const-string v0, "warning"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
