.class final Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onAuthenticatorCanceled$1;
.super Ljava/lang/Object;
.source "LoggedOutScopeRunner.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onAuthenticatorCanceled$loggedout_release()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onAuthenticatorCanceled$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 196
    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->assertInScopeOf(Ljava/lang/Class;)V

    .line 198
    invoke-virtual {p1}, Lflow/History;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 199
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onAuthenticatorCanceled$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$get_finishActivity$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    const/4 p1, 0x0

    goto :goto_0

    .line 203
    :cond_0
    invoke-static {p1}, Lcom/squareup/container/Command;->goBack(Lflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    :goto_0
    return-object p1
.end method
