.class public Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;
.super Lcom/squareup/container/ContainerPresenter;
.source "LoggedOutActivityScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/LoggedOutActivityScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Container"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerPresenter<",
        "Lcom/squareup/ui/loggedout/LoggedOutContainerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final navigationListener:Lcom/squareup/navigation/NavigationListener;

.field private final nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lflow/History;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

.field private final screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

.field private final softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

.field private final traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/navigation/NavigationListener;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    sget-object v0, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$fcitzhc0Xg_YDCRDhe9QD6GgELA;->INSTANCE:Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$fcitzhc0Xg_YDCRDhe9QD6GgELA;

    invoke-direct {p0, v0}, Lcom/squareup/container/ContainerPresenter;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 56
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 61
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->navigationListener:Lcom/squareup/navigation/NavigationListener;

    .line 73
    iput-object p2, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    .line 75
    iput-object p4, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    return-void
.end method

.method static synthetic lambda$new$0(Landroid/app/Activity;)Lkotlin/Unit;
    .locals 1

    const/4 v0, 0x0

    .line 68
    invoke-static {p0, v0}, Lcom/squareup/ui/PaymentActivity;->exitWithResult(Landroid/app/Activity;I)V

    .line 69
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method


# virtual methods
.method protected buildDispatchPipeline(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/DispatchStep;",
            ">;)V"
        }
    .end annotation

    .line 111
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->buildDispatchPipeline(Ljava/util/List;)V

    .line 113
    new-instance v0, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$GGItPHlc4eEGa3frylw5xzlppbU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$GGItPHlc4eEGa3frylw5xzlppbU;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected buildRedirectPipeline()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/container/RedirectStep;",
            ">;"
        }
    .end annotation

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 89
    new-instance v1, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$LYPmp3Q7QarsbZ_rtV_iwwEWzSQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$LYPmp3Q7QarsbZ_rtV_iwwEWzSQ;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v1, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$12f3XbJPoAcv1_8XFU6QQEaPgpM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$12f3XbJPoAcv1_8XFU6QQEaPgpM;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v1, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$jUR5qraPlEjLbdzJYB6OlwzZCqs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$jUR5qraPlEjLbdzJYB6OlwzZCqs;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-super {p0}, Lcom/squareup/container/ContainerPresenter;->buildRedirectPipeline()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method protected bridge synthetic extractBundleService(Lcom/squareup/container/ContainerView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/ui/loggedout/LoggedOutContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->extractBundleService(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected extractBundleService(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/ui/loggedout/LoggedOutContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->extractBundleService(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutContainerView;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutContainerView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method protected getDefaultHistory()Lflow/History;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-static {v0}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object v0

    return-object v0
.end method

.method protected getFlow()Lflow/Flow;
    .locals 1

    .line 125
    invoke-super {p0}, Lcom/squareup/container/ContainerPresenter;->getFlow()Lflow/Flow;

    move-result-object v0

    return-object v0
.end method

.method public getNextHistory()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public synthetic lambda$buildDispatchPipeline$5$LoggedOutActivityScope$Container(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
    .locals 1

    .line 114
    new-instance v0, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$AB5tl8X0g0eBHD7cVYVK2KgYD-8;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/loggedout/-$$Lambda$LoggedOutActivityScope$Container$AB5tl8X0g0eBHD7cVYVK2KgYD-8;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lflow/Traversal;Lflow/TraversalCallback;)V

    const-string p1, "LoggedInOnboardingContainer: traversalCompleting"

    invoke-static {p1, v0}, Lcom/squareup/container/DispatchStep$Result;->wrap(Ljava/lang/String;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$buildRedirectPipeline$1$LoggedOutActivityScope$Container(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->navigationListener:Lcom/squareup/navigation/NavigationListener;

    invoke-virtual {v0, p1}, Lcom/squareup/navigation/NavigationListener;->onDispatch(Lflow/Traversal;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public synthetic lambda$buildRedirectPipeline$2$LoggedOutActivityScope$Container(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v1, Lcom/squareup/workflow/SoftInputMode;->RESIZE:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/SoftInputPresenter;->prepKeyboardForScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/SoftInputMode;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public synthetic lambda$buildRedirectPipeline$3$LoggedOutActivityScope$Container(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->nextHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public synthetic lambda$null$4$LoggedOutActivityScope$Container(Lflow/Traversal;Lflow/TraversalCallback;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 117
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 129
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->screenNavigationLogger:Lcom/squareup/navigation/ScreenNavigationLogger;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->traversalCompleting:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const-class v2, Lcom/squareup/ui/loggedout/LoggedOutActivityScope;

    .line 132
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/navigation/ScreenNavigationLogger;->init(Lmortar/MortarScope;Lio/reactivex/Observable;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->runner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
