.class public Lcom/squareup/ui/loggedout/LandingLandscapeView;
.super Landroid/widget/LinearLayout;
.source "LandingLandscapeView.java"

# interfaces
.implements Lcom/squareup/ui/loggedout/LandingScreen$PaymentLandingView;


# instance fields
.field presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;->inject(Lcom/squareup/ui/loggedout/LandingLandscapeView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LandingLandscapeView;->presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 25
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LandingLandscapeView;->presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;

    invoke-static {p0, v0}, Lcom/squareup/ui/loggedout/LandingViews;->viewFinishedInflate(Landroid/view/View;Lcom/squareup/ui/loggedout/LandingScreen$Presenter;)V

    .line 28
    invoke-static {p0}, Lcom/squareup/ui/loggedout/LandingViews;->setupReader(Landroid/view/View;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LandingLandscapeView;->presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public showLearnMore()V
    .locals 2

    .line 43
    sget v0, Lcom/squareup/loggedout/R$string;->learn_about_square:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/squareup/ui/loggedout/LandingViews;->setCallToAction(Landroid/view/View;IZ)V

    return-void
.end method

.method public startReaderAnimation()V
    .locals 0

    .line 39
    invoke-static {p0}, Lcom/squareup/ui/loggedout/LandingViews;->startReaderAnimation(Landroid/view/View;)V

    return-void
.end method
