.class final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubOrderDetailsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateExternalLinkButton(Lcom/squareup/orders/model/Order;ZLandroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $order:Lcom/squareup/orders/model/Order;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Lcom/squareup/orders/model/Order;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->this$0:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->$order:Lcom/squareup/orders/model/Order;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 964
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->this$0:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->$order:Lcom/squareup/orders/model/Order;

    iget-object v1, v1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v2, "order.id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ORDER_DEEP_LINK:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    .line 965
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->this$0:Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->access$getBrowserLauncher$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getExternalDeepLink(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method
