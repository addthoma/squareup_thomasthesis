.class final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubTrackingRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->getCarriersDataSource(Ljava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)Lcom/squareup/cycler/DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "carrierName",
        "",
        "invoke",
        "com/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

.field final synthetic $selectedRowIndex$inlined:I


# direct methods
.method constructor <init>(ILcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V
    .locals 0

    iput p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;->$selectedRowIndex$inlined:I

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;->$rendering$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 2

    const-string v0, "carrierName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;->$rendering$inlined:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 293
    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$UpdateCarrier;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen$Event$UpdateCarrier;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
