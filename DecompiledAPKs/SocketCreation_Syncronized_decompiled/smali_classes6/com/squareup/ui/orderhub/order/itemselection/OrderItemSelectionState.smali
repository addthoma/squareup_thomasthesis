.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;
.super Ljava/lang/Object;
.source "OrderItemSelectionState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012*\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u0012*\u0010\u000f\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c6\u0003J-\u0010\u001c\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eH\u00c6\u0003J-\u0010\u001d\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eH\u00c6\u0003J\u0083\u0001\u0010\u001e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072,\u0008\u0002\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e2,\u0008\u0002\u0010\u000f\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eH\u00c6\u0001J\u0013\u0010\u001f\u001a\u00020\u00052\u0008\u0010 \u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\t\u0010#\u001a\u00020\nH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R5\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R5\u0010\u000f\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0014\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "isReadOnly",
        "",
        "action",
        "Lcom/squareup/protos/client/orders/Action;",
        "availableLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "selectedLineItems",
        "(Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;)V",
        "getAction",
        "()Lcom/squareup/protos/client/orders/Action;",
        "getAvailableLineItems",
        "()Ljava/util/Map;",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getSelectedLineItems",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final action:Lcom/squareup/protos/client/orders/Action;

.field private final availableLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final isReadOnly:Z

.field private final order:Lcom/squareup/orders/model/Order;

.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Z",
            "Lcom/squareup/protos/client/orders/Action;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->copy(Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    return v0
.end method

.method public final component3()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Z",
            "Lcom/squareup/protos/client/orders/Action;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;-><init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAction()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final getAvailableLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isReadOnly()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderItemSelectionState(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isReadOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->isReadOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->action:Lcom/squareup/protos/client/orders/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", availableLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->availableLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
