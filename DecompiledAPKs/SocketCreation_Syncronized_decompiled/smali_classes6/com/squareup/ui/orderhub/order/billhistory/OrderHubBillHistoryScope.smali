.class public final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;
.super Lcom/squareup/ui/orderhub/InOrderHubAppletScope;
.source "OrderHubBillHistoryScope.kt"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$BillHistoryModule;,
        Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubBillHistoryScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubBillHistoryScope.kt\ncom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,53:1\n24#2,4:54\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubBillHistoryScope.kt\ncom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope\n*L\n51#1,4:54\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u0001:\u0002\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;",
        "Lcom/squareup/ui/orderhub/InOrderHubAppletScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "BillHistoryModule",
        "Component",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    .line 54
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 57
    sput-object v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/InOrderHubAppletScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method
