.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "result",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "Lcom/squareup/orders/model/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 477
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 478
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 479
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v4

    .line 480
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 481
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    move-object v6, p1

    check-cast v6, Lcom/squareup/orders/model/Order;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x78

    const/4 v12, 0x0

    move-object v3, v13

    .line 478
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 477
    invoke-static {v0, v13, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 484
    :cond_0
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v0, :cond_1

    .line 485
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 486
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 487
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v4

    .line 488
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 489
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    .line 491
    new-instance v8, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 492
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object p1

    .line 493
    sget-object v3, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;

    check-cast v3, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 494
    sget-object v9, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$GoBackFromOrderDetails;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$GoBackFromOrderDetails;

    check-cast v9, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 491
    invoke-direct {v8, p1, v3, v9}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x60

    const/4 v12, 0x0

    move-object v3, v13

    .line 486
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 485
    invoke-static {v0, v13, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;->invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
