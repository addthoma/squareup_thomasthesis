.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;
.super Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;
.source "OrderItemSelectionState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemSelectionComplete"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012*\u0010\u0002\u001a&\u0012\u0004\u0012\u00020\u0004\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u0003j\u0002`\u00070\u0003j\u0002`\u0008\u00a2\u0006\u0002\u0010\tJ-\u0010\u000c\u001a&\u0012\u0004\u0012\u00020\u0004\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u0003j\u0002`\u00070\u0003j\u0002`\u0008H\u00c6\u0003J7\u0010\r\u001a\u00020\u00002,\u0008\u0002\u0010\u0002\u001a&\u0012\u0004\u0012\u00020\u0004\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u0003j\u0002`\u00070\u0003j\u0002`\u0008H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0004H\u00d6\u0001R5\u0010\u0002\u001a&\u0012\u0004\u0012\u00020\u0004\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00060\u0003j\u0002`\u00070\u0003j\u0002`\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "(Ljava/util/Map;)V",
        "getSelectedLineItems",
        "()Ljava/util/Map;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "selectedLineItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->copy(Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;"
        }
    .end annotation

    const-string v0, "selectedLineItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemSelectionComplete(selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$ItemSelectionComplete;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
