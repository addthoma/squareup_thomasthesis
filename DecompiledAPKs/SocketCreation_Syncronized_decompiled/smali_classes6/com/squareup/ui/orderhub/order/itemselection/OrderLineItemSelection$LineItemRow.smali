.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;
.super Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;
.source "OrderHubItemSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LineItemRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
        "identifier",
        "",
        "lineItem",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "selected",
        "",
        "(Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem;Z)V",
        "getIdentifier",
        "()Ljava/lang/String;",
        "getLineItem",
        "()Lcom/squareup/orders/model/Order$LineItem;",
        "getSelected",
        "()Z",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final identifier:Ljava/lang/String;

.field private final lineItem:Lcom/squareup/orders/model/Order$LineItem;

.field private final selected:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem;Z)V
    .locals 1

    const-string v0, "identifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 227
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->identifier:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    iput-boolean p3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->selected:Z

    return-void
.end method


# virtual methods
.method public final getIdentifier()Ljava/lang/String;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->identifier:Ljava/lang/String;

    return-object v0
.end method

.method public final getLineItem()Lcom/squareup/orders/model/Order$LineItem;
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->lineItem:Lcom/squareup/orders/model/Order$LineItem;

    return-object v0
.end method

.method public final getSelected()Z
    .locals 1

    .line 226
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection$LineItemRow;->selected:Z

    return v0
.end method
