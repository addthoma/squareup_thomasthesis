.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$6\n*L\n1#1,173:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "I",
        "",
        "S",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "invoke",
        "com/squareup/noho/dsl/RecyclerNohoKt$nohoRow$6"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $type:Lcom/squareup/noho/CheckType;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/CheckType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$1;->$type:Lcom/squareup/noho/CheckType;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/cycler/Recycler$CreatorContext;)Lcom/squareup/noho/NohoCheckableRow;
    .locals 7

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$CreatorContext;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$1;->$type:Lcom/squareup/noho/CheckType;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setType(Lcom/squareup/noho/CheckType;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/cycler/Recycler$CreatorContext;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationReasonCoordinator$$special$$inlined$nohoRow$1;->invoke(Lcom/squareup/cycler/Recycler$CreatorContext;)Lcom/squareup/noho/NohoCheckableRow;

    move-result-object p1

    return-object p1
.end method
