.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;
.super Ljava/lang/Object;
.source "OrderHubTransactionsHistoryRefundHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderHubBillHistoryCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubBillLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubRefundFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubRefundFlowStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubRefundFlowStateProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubRefundFlowStarterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubBillHistoryCreatorProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubBillLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;
    .locals 1

    .line 55
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubRefundFlowStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubRefundFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubBillHistoryCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->orderHubBillLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->newInstance(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper_Factory;->get()Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    move-result-object v0

    return-object v0
.end method
