.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;
.super Ljava/lang/Object;
.source "OrderHubRefundFlowState.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;,
        Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u0001:\u0002 !B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rJJ\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u00122*\u0010\u0013\u001a&\u0012\u0004\u0012\u00020\u0015\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0015j\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u0014j\u0002`\u00180\u0014j\u0002`\u0019J\u0006\u0010\u001a\u001a\u00020\nJ\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u001cJ\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u001cJ\u000e\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\rR\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00080\u00080\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
        "",
        "()V",
        "refundFlowStateRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState;",
        "kotlin.jvm.PlatformType",
        "refundResultsRelay",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;",
        "cancelRefundFlow",
        "",
        "initiateRefundFlowForBillHistory",
        "bill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "initiateRefundFlowForCancellation",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "cancelReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "leaveRefundFlow",
        "refundFlowState",
        "Lio/reactivex/Observable;",
        "refundResult",
        "refundedOrder",
        "result",
        "RefundFlowState",
        "RefundResult",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState;",
            ">;"
        }
    .end annotation
.end field

.field private final refundResultsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<RefundFlowState>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<RefundResult>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundResultsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method


# virtual methods
.method public final cancelRefundFlow()V
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundResultsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;-><init>(Lcom/squareup/billhistory/model/BillHistory;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final initiateRefundFlowForBillHistory(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForBillHistoryFlow;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForBillHistoryFlow;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final initiateRefundFlowForCancellation(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bill"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelReason"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 55
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    .line 54
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final leaveRefundFlow()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final refundFlowState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowStateRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final refundResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundResultsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final refundedOrder(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundResultsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
