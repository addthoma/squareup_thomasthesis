.class public final Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;
.super Lcom/squareup/container/BlockingBootstrapTreeKey;
.source "OrderHubBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;",
        "Lcom/squareup/container/BlockingBootstrapTreeKey;",
        "()V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/container/BlockingBootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->Companion:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;->getRunner(Lmortar/MortarScope;)Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->startWorkflow$orderhub_applet_release()V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/orderhub/OrderHubAppletScope;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/OrderHubBootstrapScreen;->getParentKey()Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    move-result-object v0

    return-object v0
.end method
