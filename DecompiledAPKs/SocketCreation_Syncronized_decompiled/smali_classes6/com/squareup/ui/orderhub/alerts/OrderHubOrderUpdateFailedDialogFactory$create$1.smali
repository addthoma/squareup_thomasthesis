.class final Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1;
.super Ljava/lang/Object;
.source "OrderHubOrderUpdateFailedDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 7

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;->getData()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;->getFailure()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object v1

    .line 33
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$GenericError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$GenericError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_order_update_failed_title:I

    .line 35
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_order_update_failed_message:I

    .line 36
    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    .line 37
    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 39
    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$VersionMismatchError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$VersionMismatchError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 40
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_order_version_mismatch_title:I

    .line 41
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_order_version_mismatch_message:I

    .line 42
    sget v3, Lcom/squareup/common/strings/R$string;->ok:I

    .line 43
    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 45
    :cond_1
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_order_update_network_failure_title:I

    .line 47
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_order_update_network_failure_message:I

    .line 48
    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    .line 49
    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 51
    :cond_2
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$BillRetrievalError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$BillRetrievalError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 52
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_bill_retrieval_failed_title:I

    .line 53
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_bill_retrieval_failed_message:I

    .line 54
    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    .line 55
    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 57
    :cond_3
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$OrderAlreadyRefundedError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$OrderAlreadyRefundedError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 58
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_order_already_refunded_title:I

    .line 59
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_order_already_refunded_message:I

    .line 60
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_order_already_refunded_ok:I

    const/4 v4, 0x0

    .line 61
    check-cast v4, Ljava/lang/Integer;

    goto :goto_0

    .line 63
    :cond_4
    sget-object v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$InventoryAdjustmentFailedError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$InventoryAdjustmentFailedError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 64
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_inventory_adjustment_failed_title:I

    .line 65
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_cancelation_inventory_adjustment_failed_message:I

    .line 66
    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    .line 67
    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 71
    :goto_0
    new-instance v5, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v6, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {v5, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 73
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 77
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 78
    new-instance v2, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$dialogBuilder$1;

    invoke-direct {v2, p1, v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$dialogBuilder$1;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v3, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 82
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;->getNegativeButtonEvent()Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    move-result-object v2

    if-eqz v2, :cond_5

    if-eqz v4, :cond_5

    .line 84
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-instance v3, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;

    invoke-direct {v3, p1, v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 89
    :cond_5
    invoke-virtual {v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1

    .line 67
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
