.class public final Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J8\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J \u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "orderHubApplet",
        "Lcom/squareup/ui/orderhub/OrderHubApplet;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "newOrderCount",
        "",
        "upcomingOrderCount",
        "orderNotificationAudioPlayer",
        "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
        "messageFromOrderCounts",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;IILcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;)Landroid/app/Dialog;
    .locals 4

    add-int v0, p4, p5

    const/4 v1, 0x1

    if-lez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v3, "Expected at least one new or upcoming order for new order dialog"

    .line 58
    invoke-static {v2, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 61
    invoke-virtual {p6}, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;->playOrderNotificationAudio()V

    .line 63
    new-instance p6, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {p6, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-ne v0, v1, :cond_1

    .line 67
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_title:I

    goto :goto_1

    .line 69
    :cond_1
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_orders_title:I

    .line 65
    :goto_1
    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "order_count"

    .line 71
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 64
    invoke-virtual {p6, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p6

    .line 73
    invoke-direct {p0, p1, p4, p5}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory;->messageFromOrderCounts(Landroid/content/Context;II)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p6, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 75
    sget p4, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_orders_primary_button:I

    new-instance p5, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory$createDialog$1;

    invoke-direct {p5, p3, p2}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory$createDialog$1;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ui/orderhub/OrderHubApplet;)V

    check-cast p5, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, p4, p5}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 80
    sget p2, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 79
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 83
    sget p2, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 82
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 85
    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_orders_secondary_button:I

    new-instance p4, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory$createDialog$2;

    invoke-direct {p4, p3}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory$createDialog$2;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;)V

    check-cast p4, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, p2, p4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 88
    new-instance p2, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory$createDialog$3;

    invoke-direct {p2, p3}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory$createDialog$3;-><init>(Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;)V

    check-cast p2, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026    }\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method

.method private final messageFromOrderCounts(Landroid/content/Context;II)Ljava/lang/CharSequence;
    .locals 2

    const-string v0, "order_count"

    const/4 v1, 0x1

    if-nez p2, :cond_1

    if-lez p3, :cond_1

    if-ne p3, v1, :cond_0

    .line 101
    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_upcoming_singular:I

    goto :goto_0

    .line 103
    :cond_0
    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_upcoming_plural:I

    .line 105
    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 106
    invoke-virtual {p1, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 107
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(context, phr\u2026nt)\n            .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    :cond_1
    if-nez p3, :cond_3

    if-lez p2, :cond_3

    if-ne p2, v1, :cond_2

    .line 110
    sget p3, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_new_singular:I

    goto :goto_1

    .line 112
    :cond_2
    sget p3, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_new_plural:I

    .line 114
    :goto_1
    invoke-static {p1, p3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 115
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_3

    :cond_3
    if-ne p2, v1, :cond_4

    if-ne p3, v1, :cond_4

    .line 119
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_upcoming_and_new_singular:I

    goto :goto_2

    :cond_4
    if-ne p2, v1, :cond_5

    if-le p3, v1, :cond_5

    .line 121
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_upcoming_plural_and_new_singular:I

    goto :goto_2

    :cond_5
    if-ne p3, v1, :cond_6

    if-le p2, v1, :cond_6

    .line 123
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_upcoming_singular_and_new_plural:I

    goto :goto_2

    .line 125
    :cond_6
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_alert_new_order_message_upcoming_plural_and_new_plural:I

    .line 128
    :goto_2
    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "upcoming_order_count"

    .line 129
    invoke-virtual {p1, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p3, "new_order_count"

    .line 130
    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_3
    const-string p2, "if (upcomingOrderCount =\u2026        .format()\n      }"

    .line 108
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4
    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    const-class v0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;

    .line 34
    invoke-interface {v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;->orderHubApplet()Lcom/squareup/ui/orderhub/OrderHubApplet;

    move-result-object v3

    .line 35
    invoke-interface {v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;->orderHubAnalytics()Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v4

    .line 36
    invoke-interface {v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;->orderNotificationAudioPlayer()Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;

    move-result-object v7

    .line 37
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;

    .line 38
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;->getNewOrderCount()I

    move-result v5

    .line 39
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;->getUpcomingOrderCount()I

    move-result v6

    move-object v1, p0

    move-object v2, p1

    .line 42
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;IILcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;)Landroid/app/Dialog;

    move-result-object p1

    .line 41
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n          c\u2026layer\n          )\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
