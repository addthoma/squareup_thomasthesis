.class public final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final alertsEnabledPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final alertsFrequencyPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;>;"
        }
    .end annotation
.end field

.field private final appIdlingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final computationSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final orderNotificationAudioPlayerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final ordersKnownBeforePreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->orderNotificationAudioPlayerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->alertsEnabledPreferenceProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->alertsFrequencyPreferenceProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p5, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->ordersKnownBeforePreferenceProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p7, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p8, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p9, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p10, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p11, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p12, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p13, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p14, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;)",
            "Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;"
        }
    .end annotation

    .line 100
    new-instance v15, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Ldagger/Lazy;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;)Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/ui/main/AppIdling;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ")",
            "Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;"
        }
    .end annotation

    .line 111
    new-instance v15, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;-><init>(Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Ldagger/Lazy;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;
    .locals 15

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->orderNotificationAudioPlayerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->alertsEnabledPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->alertsFrequencyPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->ordersKnownBeforePreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/main/AppIdling;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/ui/tender/TenderStarter;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->newInstance(Lcom/squareup/ui/orderhub/monitor/RealOrderNotificationAudioPlayer;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Ldagger/Lazy;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;)Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor_Factory;->get()Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    move-result-object v0

    return-object v0
.end method
