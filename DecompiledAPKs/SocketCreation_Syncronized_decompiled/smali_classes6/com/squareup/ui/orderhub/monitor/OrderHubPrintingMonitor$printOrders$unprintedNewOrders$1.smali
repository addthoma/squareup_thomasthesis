.class final Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;
.super Ljava/lang/Object;
.source "OrderHubPrintingMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->printOrders()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u00010\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/orders/model/Order;",
        "it",
        "Lcom/squareup/ordermanagerdata/ResultState$Success;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;->apply(Lcom/squareup/ordermanagerdata/ResultState$Success;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/ordermanagerdata/ResultState$Success;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState$Success<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1
.end method
