.class final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/ordermanagerdata/ResultState$Success<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubNewOrdersDialogMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubNewOrdersDialogMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1\n*L\n1#1,244:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ordermanagerdata/ResultState$Success;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/ordermanagerdata/ResultState$Success;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState$Success<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$onEnterScope$1;->test(Lcom/squareup/ordermanagerdata/ResultState$Success;)Z

    move-result p1

    return p1
.end method
