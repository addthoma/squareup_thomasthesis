.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$10;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/connectivity/InternetState;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "it",
        "Lcom/squareup/connectivity/InternetState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/connectivity/InternetState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/InternetState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "it"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 245
    :goto_0
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    move-object/from16 v2, p0

    .line 246
    iget-object v3, v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$10;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    xor-int/lit8 v4, v0, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 248
    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x0

    if-eqz v0, :cond_1

    move-object v0, v15

    goto :goto_1

    .line 249
    :cond_1
    sget-object v0, Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;

    :goto_1
    move-object v13, v0

    check-cast v13, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    const/4 v14, 0x0

    const/4 v0, 0x0

    const/16 v16, 0x6f

    const/16 v17, 0x0

    move-object v15, v0

    .line 248
    invoke-static/range {v8 .. v17}, Lcom/squareup/ui/orderhub/DetailState;->copy$default(Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x6e

    const/4 v12, 0x0

    .line 246
    invoke-static/range {v3 .. v12}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 245
    invoke-static {v1, v0, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$10;->invoke(Lcom/squareup/connectivity/InternetState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
