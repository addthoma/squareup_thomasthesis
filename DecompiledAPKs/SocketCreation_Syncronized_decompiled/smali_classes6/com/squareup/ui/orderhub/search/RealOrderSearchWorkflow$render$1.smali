.class final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SearchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->render(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/SearchState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/search/SearchState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/search/SearchState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/search/SearchState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/search/SearchState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/search/SearchState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 123
    sget-object p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$DispatchSearch;->INSTANCE:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$DispatchSearch;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 125
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
