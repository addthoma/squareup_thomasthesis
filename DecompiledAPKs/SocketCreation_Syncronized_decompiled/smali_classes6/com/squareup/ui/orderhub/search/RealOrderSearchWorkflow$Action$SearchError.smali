.class public final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;
.super Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchError"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u0004*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 235
    new-instance v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;->INSTANCE:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$SearchError;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 235
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "*>;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/search/SearchState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/16 v9, 0x2f

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/search/SearchState;->copy$default(Lcom/squareup/ui/orderhub/search/SearchState;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
