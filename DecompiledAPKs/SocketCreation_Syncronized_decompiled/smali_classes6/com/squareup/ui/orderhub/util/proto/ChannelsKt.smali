.class public final Lcom/squareup/ui/orderhub/util/proto/ChannelsKt;
.super Ljava/lang/Object;
.source "Channels.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChannels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Channels.kt\ncom/squareup/ui/orderhub/util/proto/ChannelsKt\n*L\n1#1,65:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028G\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u0018\u0010\t\u001a\u00020\n*\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "iconRes",
        "",
        "Lcom/squareup/protos/client/orders/Channel;",
        "getIconRes",
        "(Lcom/squareup/protos/client/orders/Channel;)I",
        "knownChannel",
        "Lcom/squareup/ui/orderhub/util/proto/KnownChannel;",
        "getKnownChannel",
        "(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;",
        "normalized",
        "",
        "getNormalized",
        "(Ljava/lang/String;)Ljava/lang/String;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getNormalized$p(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/ChannelsKt;->getNormalized(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getIconRes(Lcom/squareup/protos/client/orders/Channel;)I
    .locals 1

    const-string v0, "$this$iconRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->Companion:Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;->fromProtoChannel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->getIconRes()I

    move-result p0

    return p0
.end method

.method public static final getKnownChannel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;
    .locals 1

    const-string v0, "$this$knownChannel"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->Companion:Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;->fromProtoChannel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    move-result-object p0

    return-object p0
.end method

.method private static final getNormalized(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    .line 64
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "(this as java.lang.String).toLowerCase()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lcom/squareup/util/Strings;->removeSpaces(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
