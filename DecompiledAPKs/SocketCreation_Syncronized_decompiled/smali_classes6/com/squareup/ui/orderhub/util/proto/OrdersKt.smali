.class public final Lcom/squareup/ui/orderhub/util/proto/OrdersKt;
.super Ljava/lang/Object;
.source "Orders.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Orders.kt\ncom/squareup/ui/orderhub/util/proto/OrdersKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n+ 4 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,195:1\n1099#2,2:196\n1127#2,4:198\n132#3,3:202\n469#4,7:205\n*E\n*S KotlinDebug\n*F\n+ 1 Orders.kt\ncom/squareup/ui/orderhub/util/proto/OrdersKt\n*L\n118#1,2:196\n118#1,4:198\n136#1,3:202\n153#1,7:205\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u001a\u0016\u0010*\u001a\u00020\'*\u00020\'2\u0008\u0010+\u001a\u0004\u0018\u00010,H\u0002\"\u001b\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\"!\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00020\u0007*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\"\u0015\u0010\u000b\u001a\u00020\u0008*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\"\u0018\u0010\u000e\u001a\u00020\u000f*\u00020\u00038@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\"\u0017\u0010\u0012\u001a\u0004\u0018\u00010\u0013*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015\"\u0018\u0010\u0016\u001a\u00020\u0008*\u00020\u00038@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\r\"\u0017\u0010\u0018\u001a\u0004\u0018\u00010\u0008*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\r\"\u0017\u0010\u001a\u001a\u0004\u0018\u00010\u0008*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\r\"\u001b\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u0001*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u0005\"\u0015\u0010\u001f\u001a\u00020\u000f*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010\u0011\"\u0017\u0010 \u001a\u0004\u0018\u00010!*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010#\"\u001b\u0010$\u001a\u0008\u0012\u0004\u0012\u00020!0\u0001*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010\u0005\"\u0017\u0010&\u001a\u0004\u0018\u00010\'*\u00020\u00038F\u00a2\u0006\u0006\u001a\u0004\u0008(\u0010)\u00a8\u0006-"
    }
    d2 = {
        "availableLineItems",
        "",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/orders/model/Order;",
        "getAvailableLineItems",
        "(Lcom/squareup/orders/model/Order;)Ljava/util/List;",
        "availableLineItemsByUid",
        "",
        "",
        "getAvailableLineItemsByUid",
        "(Lcom/squareup/orders/model/Order;)Ljava/util/Map;",
        "billServerToken",
        "getBillServerToken",
        "(Lcom/squareup/orders/model/Order;)Ljava/lang/String;",
        "canAdjustFulfillmentTime",
        "",
        "getCanAdjustFulfillmentTime",
        "(Lcom/squareup/orders/model/Order;)Z",
        "channel",
        "Lcom/squareup/protos/client/orders/Channel;",
        "getChannel",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;",
        "displayDateTime",
        "getDisplayDateTime",
        "displayId",
        "getDisplayId",
        "externalDeepLink",
        "getExternalDeepLink",
        "groups",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        "getGroups",
        "isFinished",
        "primaryAction",
        "Lcom/squareup/protos/client/orders/Action;",
        "getPrimaryAction",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;",
        "secondaryActions",
        "getSecondaryActions",
        "subtotal",
        "Lcom/squareup/protos/common/Money;",
        "getSubtotal",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/common/Money;",
        "subtractNullSafe",
        "money",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getAvailableLineItems(Lcom/squareup/orders/model/Order;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$availableLineItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getAvailableLineItemsByUid(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final getAvailableLineItemsByUid(Lcom/squareup/orders/model/Order;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$availableLineItemsByUid"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    const-string v1, "line_items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    const/16 v1, 0xa

    .line 196
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 197
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 198
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 199
    move-object v3, v1

    check-cast v3, Lcom/squareup/orders/model/Order$LineItem;

    .line 118
    iget-object v3, v3, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 119
    :cond_0
    invoke-static {v2}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orders/model/Order$Fulfillment;

    const-string v3, "fulfillment"

    .line 122
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->isFinished(Lcom/squareup/orders/model/Order$Fulfillment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    iget-object v3, v2, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/4 v4, 0x1

    if-nez v3, :cond_2

    goto/16 :goto_3

    :cond_2
    sget-object v5, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v3}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ordinal()I

    move-result v3

    aget v3, v5, v3

    if-eq v3, v4, :cond_7

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    const/4 v2, 0x3

    if-eq v3, v2, :cond_3

    goto :goto_1

    .line 145
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Should not receive UNKNOWN_APPLICATION enum value for fulfillment line_item_application"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 131
    :cond_4
    iget-object v2, v2, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;

    .line 136
    iget-object v4, v3, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_6

    check-cast v4, Lcom/squareup/wire/Message;

    .line 203
    invoke-virtual {v4}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v4

    if-eqz v4, :cond_5

    move-object v5, v4

    check-cast v5, Lcom/squareup/orders/model/Order$LineItem$Builder;

    .line 138
    iget-object v6, v5, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity:Ljava/lang/String;

    const-string v7, "quantity"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/math/BigDecimal;

    invoke-direct {v7, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    const-string v8, "entry.quantity"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Ljava/math/BigDecimal;

    invoke-direct {v8, v6}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v6

    const-string v7, "this.subtract(other)"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v6

    .line 137
    invoke-virtual {v5, v6}, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;

    .line 204
    invoke-virtual {v4}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v4

    .line 135
    check-cast v4, Lcom/squareup/orders/model/Order$LineItem;

    .line 142
    iget-object v3, v3, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    const-string v5, "entry.line_item_uid"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "updatedLineItem"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 203
    :cond_5
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 136
    :cond_6
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 125
    :cond_7
    :goto_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-ne p0, v4, :cond_8

    goto :goto_4

    :cond_8
    const/4 v4, 0x0

    :goto_4
    if-eqz v4, :cond_9

    goto :goto_5

    :cond_9
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Fulfillment type ALL is not compatible with multiple fulfillments."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 205
    :cond_a
    :goto_5
    new-instance p0, Ljava/util/LinkedHashMap;

    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 206
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_b
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 207
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orders/model/Order$LineItem;

    .line 154
    iget-object v2, v2, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string v3, "it.quantity"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/math/BigDecimal;

    invoke-direct {v3, v2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    .line 155
    invoke-static {v3, v4, v5}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;J)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 208
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 211
    :cond_c
    check-cast p0, Ljava/util/Map;

    .line 157
    invoke-static {p0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final getBillServerToken(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$billServerToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v0, "id"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getCanAdjustFulfillmentTime(Lcom/squareup/orders/model/Order;)Z
    .locals 5

    const-string v0, "$this$canAdjustFulfillmentTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    sget-object v3, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    if-eq v0, v2, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    goto :goto_1

    .line 188
    :cond_1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    goto :goto_0

    :cond_2
    move-object v0, v3

    :goto_0
    sget-object v4, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    if-ne v0, v4, :cond_4

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/ChannelsKt;->getKnownChannel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    move-result-object v3

    :cond_3
    sget-object p0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->CAVIAR:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    if-ne v3, p0, :cond_4

    const/4 v1, 0x1

    :cond_4
    :goto_1
    return v1
.end method

.method public static final getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;
    .locals 1

    const-string v0, "$this$channel"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getDisplayDateTime(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$displayDateTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/ui/orderhub/util/proto/OrdersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 79
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 78
    :pswitch_0
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getClosedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 77
    :pswitch_1
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getClosedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 76
    :pswitch_2
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getClosedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 75
    :pswitch_3
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getClosedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 74
    :pswitch_4
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 73
    :pswitch_5
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 72
    :pswitch_6
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 71
    :pswitch_7
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    .line 79
    :cond_0
    :pswitch_8
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Did not expect DO_NOT_USE display state."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public static final getDisplayId(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$displayId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public static final getExternalDeepLink(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$externalDeepLink"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getGroups(Lcom/squareup/orders/model/Order;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$groups"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method public static final getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;
    .locals 1

    const-string v0, "$this$primaryAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/orders/Action;

    return-object p0
.end method

.method public static final getSecondaryActions(Lcom/squareup/orders/model/Order;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$secondaryActions"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    check-cast p0, Ljava/lang/Iterable;

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final getSubtotal(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "$this$subtotal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 178
    iget-object p0, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-static {v0, p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static final isFinished(Lcom/squareup/orders/model/Order;)Z
    .locals 2

    const-string v0, "$this$isFinished"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    sget-object v1, Lcom/squareup/orders/model/Order$State;->CANCELED:Lcom/squareup/orders/model/Order$State;

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    sget-object v0, Lcom/squareup/orders/model/Order$State;->COMPLETED:Lcom/squareup/orders/model/Order$State;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static final subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 0

    if-eqz p1, :cond_0

    .line 193
    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string p1, "MoneyMath.subtractNullSa\u2026this, money?.toMoneyV1())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
