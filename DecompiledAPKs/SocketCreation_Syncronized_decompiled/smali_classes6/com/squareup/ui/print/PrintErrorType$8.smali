.class final enum Lcom/squareup/ui/print/PrintErrorType$8;
.super Lcom/squareup/ui/print/PrintErrorType;
.source "PrintErrorType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 76
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/print/PrintErrorType;-><init>(Ljava/lang/String;ILcom/squareup/ui/print/PrintErrorType$1;)V

    return-void
.end method


# virtual methods
.method canShowSpinner()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getMessage()I
    .locals 1

    .line 82
    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_issue_body_network:I

    return v0
.end method

.method getTitle()I
    .locals 1

    .line 78
    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_unavailable_title:I

    return v0
.end method
