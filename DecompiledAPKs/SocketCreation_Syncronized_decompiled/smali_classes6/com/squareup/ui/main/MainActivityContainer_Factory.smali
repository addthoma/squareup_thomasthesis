.class public final Lcom/squareup/ui/main/MainActivityContainer_Factory;
.super Ljava/lang/Object;
.source "MainActivityContainer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/MainActivityContainer;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityBackHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MainActivityBackHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalContainerLayerSetupProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/AdditionalContainerLayerSetup;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final appletsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinkHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final glassConfirmControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final intentParserProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/IntentParser;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final lockScreenMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final redirectPipelineDecoratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
            ">;"
        }
    .end annotation
.end field

.field private final registrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field private final rootViewSetupProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;"
        }
    .end annotation
.end field

.field private final salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final screenNavigationLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final systemPermissionsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final unsyncedOpenTicketsSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MainActivityBackHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lmortar/Scoped;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/IntentParser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/AdditionalContainerLayerSetup;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->activityBackHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->registrarProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->glassConfirmControllerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->intentParserProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->appletsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 129
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->systemPermissionsPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->screenNavigationLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->navigationListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 133
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->homeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 135
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->deepLinkHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 136
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->rootViewSetupProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->unsyncedOpenTicketsSpinnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->redirectPipelineDecoratorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->additionalContainerLayerSetupProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/MainActivityContainer_Factory;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MainActivityBackHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lmortar/Scoped;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/IntentParser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/AdditionalContainerLayerSetup;",
            ">;)",
            "Lcom/squareup/ui/main/MainActivityContainer_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    .line 174
    new-instance v27, Lcom/squareup/ui/main/MainActivityContainer_Factory;

    move-object/from16 v0, v27

    invoke-direct/range {v0 .. v26}, Lcom/squareup/ui/main/MainActivityContainer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v27
.end method

.method public static newInstance(Lcom/squareup/ui/MainActivityBackHandler;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/settings/server/Features;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/ui/main/TransactionMetrics;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/navigation/NavigationListener;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/analytics/DeepLinkHelper;Ldagger/Lazy;Lcom/squareup/rootview/RootViewSetup;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Ldagger/Lazy;Lcom/squareup/container/AdditionalContainerLayerSetup;)Lcom/squareup/ui/main/MainActivityContainer;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/MainActivityBackHandler;",
            "Ldagger/Lazy<",
            "Lmortar/Scoped;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/IntentParser;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            "Lcom/squareup/navigation/NavigationListener;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/ui/main/Home;",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            "Ldagger/Lazy<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Lcom/squareup/rootview/RootViewSetup;",
            "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;",
            ">;",
            "Lcom/squareup/container/AdditionalContainerLayerSetup;",
            ")",
            "Lcom/squareup/ui/main/MainActivityContainer;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    .line 192
    new-instance v27, Lcom/squareup/ui/main/MainActivityContainer;

    move-object/from16 v0, v27

    invoke-direct/range {v0 .. v26}, Lcom/squareup/ui/main/MainActivityContainer;-><init>(Lcom/squareup/ui/MainActivityBackHandler;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/settings/server/Features;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/ui/main/TransactionMetrics;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/navigation/NavigationListener;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/analytics/DeepLinkHelper;Ldagger/Lazy;Lcom/squareup/rootview/RootViewSetup;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Ldagger/Lazy;Lcom/squareup/container/AdditionalContainerLayerSetup;)V

    return-object v27
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/MainActivityContainer;
    .locals 28

    move-object/from16 v0, p0

    .line 146
    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->activityBackHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/MainActivityBackHandler;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->registrarProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->glassConfirmControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/widgets/glass/GlassConfirmController;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/jailkeeper/JailKeeper;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->intentParserProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->appletsProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/api/ApiTransactionController;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->systemPermissionsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->screenNavigationLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/navigation/ScreenNavigationLogger;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->navigationListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/navigation/NavigationListener;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/SoftInputPresenter;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/main/Home;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->deepLinkHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/analytics/DeepLinkHelper;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v22

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->rootViewSetupProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/rootview/RootViewSetup;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->unsyncedOpenTicketsSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->redirectPipelineDecoratorProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v26

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityContainer_Factory;->additionalContainerLayerSetupProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/container/AdditionalContainerLayerSetup;

    invoke-static/range {v2 .. v27}, Lcom/squareup/ui/main/MainActivityContainer_Factory;->newInstance(Lcom/squareup/ui/MainActivityBackHandler;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/settings/server/Features;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/ui/main/TransactionMetrics;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/navigation/NavigationListener;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/analytics/DeepLinkHelper;Ldagger/Lazy;Lcom/squareup/rootview/RootViewSetup;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Ldagger/Lazy;Lcom/squareup/container/AdditionalContainerLayerSetup;)Lcom/squareup/ui/main/MainActivityContainer;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer_Factory;->get()Lcom/squareup/ui/main/MainActivityContainer;

    move-result-object v0

    return-object v0
.end method
