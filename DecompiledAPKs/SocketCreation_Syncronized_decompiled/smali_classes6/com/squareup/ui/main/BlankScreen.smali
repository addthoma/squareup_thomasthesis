.class public final Lcom/squareup/ui/main/BlankScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "BlankScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/BlankScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/BlankScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/ui/main/BlankScreen;

    invoke-direct {v0}, Lcom/squareup/ui/main/BlankScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/BlankScreen;->INSTANCE:Lcom/squareup/ui/main/BlankScreen;

    .line 15
    sget-object v0, Lcom/squareup/ui/main/BlankScreen;->INSTANCE:Lcom/squareup/ui/main/BlankScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/BlankScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 18
    sget v0, Lcom/squareup/pos/container/R$layout;->transparent_container:I

    return v0
.end method
