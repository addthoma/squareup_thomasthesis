.class public final Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;
.super Ljava/lang/Object;
.source "EmvSwipePassthroughEnabler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;-><init>(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHub;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v2, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->newInstance(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler_Factory;->get()Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    move-result-object v0

    return-object v0
.end method
