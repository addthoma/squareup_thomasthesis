.class final Lcom/squareup/ui/main/MainActivityContainer$1;
.super Lkotlin/jvm/internal/Lambda;
.source "MainActivityContainer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;-><init>(Lcom/squareup/ui/MainActivityBackHandler;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/settings/server/Features;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/ui/main/TransactionMetrics;Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/navigation/ScreenNavigationLogger;Lcom/squareup/navigation/NavigationListener;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/ui/main/Home;Lcom/squareup/analytics/DeepLinkHelper;Ldagger/Lazy;Lcom/squareup/rootview/RootViewSetup;Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Ldagger/Lazy;Lcom/squareup/container/AdditionalContainerLayerSetup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/app/Activity;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/app/Activity;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activityBackHandler:Lcom/squareup/ui/MainActivityBackHandler;

.field final synthetic $lazyX2ScreenRunner:Ldagger/Lazy;


# direct methods
.method constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/MainActivityBackHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$1;->$lazyX2ScreenRunner:Ldagger/Lazy;

    iput-object p2, p0, Lcom/squareup/ui/main/MainActivityContainer$1;->$activityBackHandler:Lcom/squareup/ui/MainActivityBackHandler;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer$1;->invoke(Landroid/app/Activity;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$1;->$lazyX2ScreenRunner:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$1;->$activityBackHandler:Lcom/squareup/ui/MainActivityBackHandler;

    invoke-interface {v0, p1}, Lcom/squareup/ui/MainActivityBackHandler;->onMainActivityBackPressed(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
