.class public final Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$10;
.super Ljava/lang/Object;
.source "MainActivityScopeRegistrar.kt"

# interfaces
.implements Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$10",
        "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;",
        "activityNoLongerVisible",
        "",
        "activityVisible",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 175
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$10;->this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public activityNoLongerVisible()V
    .locals 0

    return-void
.end method

.method public activityVisible()V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$10;->this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->access$getJailKeeper$p(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lcom/squareup/jailkeeper/JailKeeper;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->backgroundSync()V

    return-void
.end method
