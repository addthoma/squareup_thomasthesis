.class public Lcom/squareup/ui/main/LockOrClockButtonHelper;
.super Ljava/lang/Object;
.source "LockOrClockButtonHelper.java"


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 27
    iput-object p3, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 28
    iput-object p4, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    return-void
.end method


# virtual methods
.method public employeeIsGuest()Z
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    return v0
.end method

.method public getEmployeeInitials()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->initials:Ljava/lang/String;

    return-object v0
.end method

.method public lockOrClockButtonClicked()V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->shouldShowClockInButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-interface {v0}, Lcom/squareup/ui/timecards/api/TimecardsLauncher;->showClockInOut()V

    goto :goto_0

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->attemptScreenLock()V

    :goto_0
    return-void
.end method

.method public shouldShowLockButton()Z
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->shouldShowLockButtonWithoutClock()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->shouldShowClockInButton()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public shouldShowLockButtonWithoutClock()Z
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
