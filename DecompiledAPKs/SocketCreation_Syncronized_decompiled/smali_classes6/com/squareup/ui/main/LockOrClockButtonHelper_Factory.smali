.class public final Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;
.super Ljava/lang/Object;
.source "LockOrClockButtonHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/LockOrClockButtonHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->timecardsLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;)",
            "Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)Lcom/squareup/ui/main/LockOrClockButtonHelper;
    .locals 1

    .line 54
    new-instance v0, Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/LockOrClockButtonHelper;-><init>(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/LockOrClockButtonHelper;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v2, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v3, p0, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->timecardsLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->newInstance(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)Lcom/squareup/ui/main/LockOrClockButtonHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/main/LockOrClockButtonHelper_Factory;->get()Lcom/squareup/ui/main/LockOrClockButtonHelper;

    move-result-object v0

    return-object v0
.end method
