.class public interface abstract Lcom/squareup/ui/main/ForceableContentLauncher;
.super Ljava/lang/Object;
.source "ForceableContentLauncher.java"

# interfaces
.implements Lcom/squareup/ui/main/ContentLauncher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/ui/main/ContentLauncher<",
        "TT;>;"
    }
.end annotation


# virtual methods
.method public abstract isContentShowing()Z
.end method

.method public abstract showContent(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
