.class public Lcom/squareup/ui/main/r12education/R12EducationView;
.super Landroid/widget/RelativeLayout;
.source "R12EducationView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;,
        Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;,
        Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;,
        Lcom/squareup/ui/main/r12education/R12EducationView$Element;
    }
.end annotation


# static fields
.field private static final OVERTWEEN_DURATION_MSEC:I = 0x2ee


# instance fields
.field private activePanelOverTweenAnimator:Landroid/animation/ValueAnimator;

.field private cable:Landroid/view/View;

.field private chipCard:Landroid/view/View;

.field countryCode:Lcom/squareup/CountryCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dotsGreen:Landroid/view/View;

.field private dotsOrange:Landroid/view/View;

.field private final elementViews:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Lcom/squareup/ui/main/r12education/R12EducationView$Element;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private magCard:Landroid/view/View;

.field private nextButton:Landroid/widget/TextView;

.field private pagerAdapter:Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;

.field private pagerIndicator:Lcom/squareup/viewpagerindicator/PageIndicator;

.field private panelTransitions:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

.field private phoneApplePay:Landroid/view/View;

.field private phoneR4:Landroid/view/View;

.field presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r12:Landroid/view/View;

.field private sticker:Landroid/view/View;

.field private tapCardWithHand:Landroid/view/View;

.field private tweener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

.field private viewPager:Landroidx/viewpager/widget/ViewPager;

.field private xButtonPhone:Lcom/squareup/glyph/SquareGlyphView;

.field private xButtonTablet:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 420
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 412
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    .line 421
    const-class p2, Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;->inject(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    .line 422
    new-instance p2, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;-><init>(Landroid/content/res/Resources;)V

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->panelTransitions:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationView;->getVariant()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroidx/viewpager/widget/ViewPager;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->panelTransitions:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/main/r12education/R12EducationView;)Ljava/util/LinkedHashMap;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/widget/TextView;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->nextButton:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->activePanelOverTweenAnimator:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method static synthetic access$802(Lcom/squareup/ui/main/r12education/R12EducationView;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->activePanelOverTweenAnimator:Landroid/animation/ValueAnimator;

    return-object p1
.end method

.method private bindViews()V
    .locals 1

    .line 623
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_next:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->nextButton:Landroid/widget/TextView;

    .line 624
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_pager:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    .line 625
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_pager_indicator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/viewpagerindicator/PageIndicator;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->pagerIndicator:Lcom/squareup/viewpagerindicator/PageIndicator;

    .line 626
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_r12:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->r12:Landroid/view/View;

    .line 627
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_cable:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->cable:Landroid/view/View;

    .line 628
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_dots_green:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->dotsGreen:Landroid/view/View;

    .line 629
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_dots_orange:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->dotsOrange:Landroid/view/View;

    .line 630
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_chip_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->chipCard:Landroid/view/View;

    .line 631
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_phone_apple_pay:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->phoneApplePay:Landroid/view/View;

    .line 632
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_phone_r4:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->phoneR4:Landroid/view/View;

    .line 633
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_mag_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->magCard:Landroid/view/View;

    .line 634
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_sticker:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->sticker:Landroid/view/View;

    .line 635
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_tap_card_with_hand:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->tapCardWithHand:Landroid/view/View;

    .line 636
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_x_phone:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->xButtonPhone:Lcom/squareup/glyph/SquareGlyphView;

    .line 637
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_x_tablet:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->xButtonTablet:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method private getVariant()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;
    .locals 2

    .line 619
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->variant:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    const-string v1, "R12 education view variant must already be set"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    return-object v0
.end method

.method private initButtonText(I)V
    .locals 1

    .line 641
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationView;->getVariant()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object p1

    .line 642
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->nextButton:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->getButtonText()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private retreatPager()Z
    .locals 3

    .line 611
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    if-lez v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    return v2

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method advancePager()Z
    .locals 3

    .line 601
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    .line 602
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationView;->getVariant()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->getPanelCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 603
    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 592
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 431
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 432
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 436
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 450
    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_sticker:I

    goto :goto_0

    .line 447
    :cond_0
    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_sticker_jp:I

    goto :goto_0

    .line 444
    :cond_1
    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_sticker_gb:I

    goto :goto_0

    .line 438
    :cond_2
    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_sticker_ca:I

    goto :goto_0

    .line 441
    :cond_3
    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_sticker_au:I

    .line 452
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->sticker:Landroid/view/View;

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationView;->getVariant()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->getPanelCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->xButtonPhone:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->xButtonTablet:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 461
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->xButtonTablet:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/r12education/R12EducationView$1;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 469
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->xButtonPhone:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/r12education/R12EducationView$2;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 478
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->nextButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/r12education/R12EducationView$3;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->R12:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->r12:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->CABLE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->cable:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->CHIP_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->chipCard:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_GREEN:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->dotsGreen:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->DOTS_ORANGE:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->dotsOrange:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->PHONE_APPLE_PAY:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->phoneApplePay:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->PHONE_R4:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->phoneR4:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->MAG_CARD:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->magCard:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->STICKER:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->sticker:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->elementViews:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->TAP_CARD_WITH_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->tapCardWithHand:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/ui/main/r12education/R12EducationView$1;)V

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->pagerAdapter:Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;

    .line 496
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->pagerAdapter:Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 497
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->pagerIndicator:Lcom/squareup/viewpagerindicator/PageIndicator;

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-interface {v0, v1}, Lcom/squareup/viewpagerindicator/PageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 499
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationView$4;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->tweener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    .line 581
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->initButtonText(I)V

    .line 583
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->pagerIndicator:Lcom/squareup/viewpagerindicator/PageIndicator;

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->tweener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    invoke-interface {v0, v1}, Lcom/squareup/viewpagerindicator/PageIndicator;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 596
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationView;->retreatPager()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 587
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 588
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 426
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 427
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationView;->bindViews()V

    return-void
.end method
