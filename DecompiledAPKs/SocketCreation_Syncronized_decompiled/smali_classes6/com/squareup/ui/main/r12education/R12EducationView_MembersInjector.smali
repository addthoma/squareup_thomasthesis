.class public final Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;
.super Ljava/lang/Object;
.source "R12EducationView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/main/r12education/R12EducationView;",
        ">;"
    }
.end annotation


# instance fields
.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/main/r12education/R12EducationView;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCountryCode(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView;->presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/main/r12education/R12EducationView;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->injectPresenter(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->injectDevice(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/util/Device;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/CountryCode;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->injectCountryCode(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/CountryCode;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView_MembersInjector;->injectMembers(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    return-void
.end method
