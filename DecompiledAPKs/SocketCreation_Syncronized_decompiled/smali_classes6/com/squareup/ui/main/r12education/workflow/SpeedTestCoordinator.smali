.class public final Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SpeedTestCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J$\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00150\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0002J>\u0010 \u001a\u00020\u00152\u0006\u0010!\u001a\u00020\"2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020\u001e2\u0008\u0008\u0001\u0010$\u001a\u00020%2\u0008\u0008\u0001\u0010&\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020%H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "messageView",
        "Lcom/squareup/widgets/MessageView;",
        "primaryButton",
        "Lcom/squareup/noho/NohoButton;",
        "secondaryButton",
        "spinnerGlyph",
        "Lcom/squareup/marin/widgets/MarinSpinnerGlyph;",
        "titleView",
        "Landroid/widget/TextView;",
        "upButton",
        "Landroid/widget/ImageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "setViewClickListener",
        "callback",
        "Lkotlin/Function1;",
        "updateTexts",
        "title",
        "",
        "body",
        "updateTextsWithHyperlink",
        "context",
        "Landroid/content/Context;",
        "key",
        "pattern",
        "",
        "url",
        "clickableText",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private messageView:Lcom/squareup/widgets/MessageView;

.field private primaryButton:Lcom/squareup/noho/NohoButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private secondaryButton:Lcom/squareup/noho/NohoButton;

.field private spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private titleView:Landroid/widget/TextView;

.field private upButton:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->primaryButton:Lcom/squareup/noho/NohoButton;

    if-nez p0, :cond_0

    const-string v0, "primaryButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSecondaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/noho/NohoButton;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez p0, :cond_0

    const-string v0, "secondaryButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSpinnerGlyph$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Lcom/squareup/marin/widgets/MarinSpinnerGlyph;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p0, :cond_0

    const-string v0, "spinnerGlyph"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getUpButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->upButton:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "upButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setPrimaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Lcom/squareup/noho/NohoButton;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->primaryButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method public static final synthetic access$setSecondaryButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Lcom/squareup/noho/NohoButton;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->secondaryButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method public static final synthetic access$setSpinnerGlyph$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Lcom/squareup/marin/widgets/MarinSpinnerGlyph;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    return-void
.end method

.method public static final synthetic access$setUpButton$p(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->upButton:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setViewClickListener(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->setViewClickListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final synthetic access$updateTexts(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->updateTexts(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$updateTextsWithHyperlink(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 0

    .line 25
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->updateTextsWithHyperlink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 125
    sget v0, Lcom/squareup/readertutorial/R$id;->spinner_glyph:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.spinner_glyph)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    .line 126
    sget v0, Lcom/squareup/readertutorial/R$id;->glyph_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.glyph_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->titleView:Landroid/widget/TextView;

    .line 127
    sget v0, Lcom/squareup/readertutorial/R$id;->glyph_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.glyph_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    .line 128
    sget v0, Lcom/squareup/readertutorial/R$id;->speed_test_primary_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.speed_test_primary_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->primaryButton:Lcom/squareup/noho/NohoButton;

    .line 129
    sget v0, Lcom/squareup/readertutorial/R$id;->speed_test_secondary_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.s\u2026ed_test_secondary_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->secondaryButton:Lcom/squareup/noho/NohoButton;

    .line 130
    sget v0, Lcom/squareup/readertutorial/R$id;->speedtest_up_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.speedtest_up_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->upButton:Landroid/widget/ImageView;

    return-void
.end method

.method private final setViewClickListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$setViewClickListener$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$setViewClickListener$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updateTexts(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->titleView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_1

    const-string v0, "messageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateTextsWithHyperlink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->titleView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->messageView:Lcom/squareup/widgets/MessageView;

    if-nez p2, :cond_1

    const-string v0, "messageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 116
    :cond_1
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 117
    invoke-virtual {v0, p4, p3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 118
    invoke-virtual {p1, p5}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 119
    sget p3, Lcom/squareup/readertutorial/R$color;->speedtest_link_color:I

    invoke-virtual {p1, p3}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 120
    invoke-virtual {p1, p6}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->bindViews(Landroid/view/View;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe {\n    \u2026)\n        }\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
