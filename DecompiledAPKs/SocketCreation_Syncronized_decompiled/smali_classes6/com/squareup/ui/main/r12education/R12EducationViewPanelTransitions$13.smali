.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$13;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateCountryPrefersContactlessCardsTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V
    .locals 0

    .line 586
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$13;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 3

    .line 590
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/16 p4, 0x8

    const/high16 p5, 0x3f400000    # 0.75f

    if-eq p1, p4, :cond_1

    const/16 p4, 0x9

    if-eq p1, p4, :cond_0

    .line 630
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    .line 621
    invoke-static {p3, p5, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 622
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 623
    invoke-static {p2, p7}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToBottomY(Landroid/view/View;Landroid/view/View;)V

    .line 624
    invoke-static {p2, p7, p6, v1, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideInRightToAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    :cond_1
    const/high16 p1, 0x3e800000    # 0.25f

    .line 613
    invoke-static {p3, p1, p5}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 614
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 615
    invoke-static {p2, p7}, Lcom/squareup/ui/main/r12education/Tweens;->centerX(Landroid/view/View;Landroid/view/View;)V

    .line 616
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$13;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneR4BottomMargin:I

    invoke-static {p2, p7, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideInToAlignBottom(Landroid/view/View;Landroid/view/View;IF)V

    goto :goto_0

    :cond_2
    const p1, 0x3ea8f5c3    # 0.33f

    .line 602
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$400()Landroid/view/animation/Interpolator;

    move-result-object p6

    invoke-static {p3, v2, p1, p6}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 604
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 605
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->centerY(Landroid/view/View;Landroid/view/View;)V

    .line 606
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$13;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 607
    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->chipCardMargin:I

    .line 606
    invoke-static {p2, p5, p4, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutRightFromAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    .line 594
    :cond_3
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    const/high16 p1, 0x3f000000    # 0.5f

    .line 595
    invoke-static {p3, v2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 596
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->centerY(Landroid/view/View;Landroid/view/View;)V

    .line 597
    invoke-static {p2, p5, v1, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutLeftFromAlignCenter(Landroid/view/View;Landroid/view/View;IF)V

    :goto_0
    return-void
.end method
