.class final Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "RealSpeedTestWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SpeedTestWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\u0006\u001a\u00020\u00072\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\tH\u0016J\u0010\u0010\n\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u000bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "",
        "workerUuid",
        "",
        "(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Ljava/lang/String;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lorg/reactivestreams/Publisher;",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

.field private final workerUuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "workerUuid"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->workerUuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;

    iget-object p1, p1, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->workerUuid:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->workerUuid:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public runPublisher()Lorg/reactivestreams/Publisher;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/reactivestreams/Publisher<",
            "+",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->access$getClock$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 137
    iget-object v2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-static {v2}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->access$getFelicaSpeedTestService$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/server/felica/FelicaSpeedTestService;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/server/felica/FelicaSpeedTestService;->ping()Lio/reactivex/Single;

    move-result-object v2

    .line 138
    new-instance v3, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker$runPublisher$1;-><init>(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;J)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "felicaSpeedTestService.p\u2026}\n          .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
