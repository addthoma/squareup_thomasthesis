.class final Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$3;
.super Lkotlin/jvm/internal/Lambda;
.source "SpeedTestCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$3;->$screen:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$3;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/SpeedTestCoordinator$attach$1$3;->$screen:Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    check-cast p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Success;

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Success;->getOnContinuePress()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
