.class public Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;
.super Ljava/lang/Object;
.source "EmvSwipePassthroughEnabler.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/EmvSwipePassthroughEnabler$SwipePassthrough;
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private onScreenWithPassthrough:Z


# direct methods
.method public constructor <init>(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->badBus:Lcom/squareup/badbus/BadBus;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method


# virtual methods
.method public enableSwipePassthroughOnOtherReaders(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 67
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v2, v3, :cond_0

    .line 68
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    .line 69
    invoke-interface {v1, v2}, Lcom/squareup/cardreader/CardReader;->enableSwipePassthrough(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$EmvSwipePassthroughEnabler(Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->refreshSwipePassthrough()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$EmvSwipePassthroughEnabler$__z1X8osBonh5fjuXAbcAVop3RA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/-$$Lambda$EmvSwipePassthroughEnabler$__z1X8osBonh5fjuXAbcAVop3RA;-><init>(Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 57
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public refreshSwipePassthrough()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 95
    iget-object v2, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 96
    invoke-interface {v2}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->onScreenWithPassthrough:Z

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v2, 0x1

    .line 95
    :goto_2
    invoke-interface {v1, v2}, Lcom/squareup/cardreader/CardReader;->enableSwipePassthrough(Z)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method refreshSwipePassthrough(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 81
    const-class v0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler$SwipePassthrough;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->onScreenWithPassthrough:Z

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->refreshSwipePassthrough()V

    return-void
.end method
