.class public Lcom/squareup/ui/main/errors/ReaderWarningView;
.super Lcom/squareup/widgets/ResponsiveScrollView;
.source "ReaderWarningView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/ReaderWarningView$Component;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private bottomDefaultButton:Landroid/widget/Button;

.field private cancelButton:Lcom/squareup/glyph/SquareGlyphView;

.field private glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private importantMessage:Landroid/widget/TextView;

.field presenter:Lcom/squareup/ui/main/errors/ReaderWarningPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private topAlternativeButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/ui/main/errors/ReaderWarningView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/ReaderWarningView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/main/errors/ReaderWarningView$Component;->inject(Lcom/squareup/ui/main/errors/ReaderWarningView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 111
    sget v0, Lcom/squareup/cardreader/ui/R$id;->cancel_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 112
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_glyph_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 113
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_important_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->importantMessage:Landroid/widget/TextView;

    .line 114
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_top_alternative_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->topAlternativeButton:Landroid/widget/Button;

    .line 115
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_bottom_default_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->bottomDefaultButton:Landroid/widget/Button;

    return-void
.end method

.method private static showButton(Landroid/widget/Button;Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V
    .locals 1

    const/4 v0, 0x0

    .line 100
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 101
    iget-boolean v0, p1, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->enabled:Z

    invoke-virtual {p0, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 102
    iget-object v0, p1, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->localizedText:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p1, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->localizedText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 105
    :cond_0
    iget v0, p1, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->textId:I

    invoke-virtual {p0, v0}, Landroid/widget/Button;->setText(I)V

    .line 107
    :goto_0
    iget-object p1, p1, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->callback:Lcom/squareup/debounce/DebouncedOnClickListener;

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public hideImportantMessage()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->importantMessage:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->presenter:Lcom/squareup/ui/main/errors/ReaderWarningPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->dropView(Ljava/lang/Object;)V

    .line 59
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 46
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onFinishInflate()V

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/ReaderWarningView;->bindViews()V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/main/errors/ReaderWarningView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/ReaderWarningView$1;-><init>(Lcom/squareup/ui/main/errors/ReaderWarningView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->presenter:Lcom/squareup/ui/main/errors/ReaderWarningPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVector(I)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVector(I)V

    return-void
.end method

.method public showBottomDefaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->bottomDefaultButton:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showButton(Landroid/widget/Button;Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V

    return-void
.end method

.method public showCancelButton(Z)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method

.method public showImportantMessage(I)V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->importantMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->importantMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public showTopAlternativeButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningView;->topAlternativeButton:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showButton(Landroid/widget/Button;Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V

    return-void
.end method
