.class public Lcom/squareup/ui/main/errors/RootReaderWarningScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "RootReaderWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/main/errors/ReaderWarningScreen;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/spot/ModalBodyScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/main/errors/RootReaderWarningScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/RootReaderWarningScreen$Component;,
        Lcom/squareup/ui/main/errors/RootReaderWarningScreen$ParentComponent;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/RootReaderWarningScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 199
    sget-object v0, Lcom/squareup/ui/main/errors/-$$Lambda$RootReaderWarningScreen$jXVTe8wYmNITJz6gZ8wMVNRIevg;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$RootReaderWarningScreen$jXVTe8wYmNITJz6gZ8wMVNRIevg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V
    .locals 0

    .line 137
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V
    .locals 1

    .line 64
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 65
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V
    .locals 1

    .line 72
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 73
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 75
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 76
    invoke-virtual {p1, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 82
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 83
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 84
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 85
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p5}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;IILjava/lang/String;Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)V
    .locals 1

    .line 111
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 112
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 113
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 114
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 115
    invoke-virtual {p1, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 116
    invoke-virtual {p1, p6}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->userInteractionMessage(Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 117
    invoke-virtual {p1, p5}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 94
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 124
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 125
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 126
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 127
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 128
    invoke-virtual {p1, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 129
    invoke-virtual {p1, p5}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 124
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)V
    .locals 1

    .line 99
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 100
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 101
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 102
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 103
    invoke-virtual {p1, p4}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p1, p6}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->userInteractionMessage(Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 105
    invoke-virtual {p1, p5}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->url(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/RootReaderWarningScreen;
    .locals 1

    .line 200
    const-class v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    .line 201
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    .line 202
    new-instance v0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{warningType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;->initialParameters:Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    iget-object v1, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 148
    invoke-virtual {v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 59
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->reader_warning_view:I

    return v0
.end method
