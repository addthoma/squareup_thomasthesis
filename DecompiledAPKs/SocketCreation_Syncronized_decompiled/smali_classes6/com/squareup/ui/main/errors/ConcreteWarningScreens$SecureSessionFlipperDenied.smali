.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;
.super Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SecureSessionFlipperDenied"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final localizedMessage:Ljava/lang/String;

.field private final localizedTitle:Ljava/lang/String;

.field private final uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 373
    sget-object v0, Lcom/squareup/ui/main/errors/-$$Lambda$ConcreteWarningScreens$SecureSessionFlipperDenied$Vh1GOiBXxZ0B9jJcY02M-4O0kv0;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$ConcreteWarningScreens$SecureSessionFlipperDenied$Vh1GOiBXxZ0B9jJcY02M-4O0kv0;

    .line 374
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 0

    .line 338
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;-><init>()V

    .line 339
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->localizedTitle:Ljava/lang/String;

    .line 340
    iput-object p2, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->localizedMessage:Ljava/lang/String;

    .line 341
    iput-object p3, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;
    .locals 3

    .line 375
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 376
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 377
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object p0

    .line 378
    new-instance v2, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;

    invoke-direct {v2, v0, v1, p0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    return-object v2
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 382
    iget-object p2, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->localizedTitle:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 383
    iget-object p2, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->localizedMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 384
    iget-object p2, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    invoke-virtual {p2}, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->swigValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method protected getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 4

    .line 347
    sget-object p1, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureSessionUxHint:[I

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    const/4 v0, 0x0

    goto :goto_0

    .line 353
    :cond_0
    sget-object p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->START_ACTIVATION:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 354
    sget v0, Lcom/squareup/cardreader/ui/R$string;->get_started:I

    goto :goto_0

    .line 349
    :cond_1
    sget-object p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->CONTACT_SUPPORT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 350
    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_contact_support:I

    .line 361
    :goto_0
    new-instance v1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    .line 362
    sget-object v2, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    sget v3, Lcom/squareup/common/strings/R$string;->ok:I

    .line 363
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v2

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 364
    invoke-virtual {v2, v3}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->localizedTitle:Ljava/lang/String;

    .line 365
    invoke-virtual {v2, v3}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;->localizedMessage:Ljava/lang/String;

    .line 366
    invoke-virtual {v2, v3}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    if-eqz p1, :cond_2

    .line 368
    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->topAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    .line 370
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
