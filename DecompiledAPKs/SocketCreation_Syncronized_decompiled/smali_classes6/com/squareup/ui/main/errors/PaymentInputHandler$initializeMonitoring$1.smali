.class final Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;
.super Ljava/lang/Object;
.source "PaymentInputHandler.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeMonitoring(JLkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $startMonitoringAction:Lkotlin/jvm/functions/Function0;

.field final synthetic this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->$startMonitoringAction:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->access$getTransaction$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->access$getDippedCardTracker$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/DippedCardTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->access$getReaderHudManager$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReinsertChipCardToCharge()Z

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->access$getCardReaderHubUtils$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/CardReaderHubUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isCardInsertedOnAnyContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->this$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->access$getReaderHudManager$p(Lcom/squareup/ui/main/errors/PaymentInputHandler;)Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastRemoveChipCard()Z

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentInputHandler$initializeMonitoring$1;->$startMonitoringAction:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_0
    return-void
.end method
