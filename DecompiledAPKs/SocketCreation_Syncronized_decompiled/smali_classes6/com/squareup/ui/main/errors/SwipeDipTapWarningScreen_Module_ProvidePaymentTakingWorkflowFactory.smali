.class public final Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;
.super Ljava/lang/Object;
.source "SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final buttonFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final initialViewDataProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;

.field private final paymentInputHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->module:Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->buttonFlowStarterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->initialViewDataProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;)",
            "Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;-><init>(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static providePaymentTakingWorkflow(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
    .locals 0

    .line 63
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;->providePaymentTakingWorkflow(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
    .locals 6

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->module:Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->buttonFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    iget-object v2, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->initialViewDataProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/errors/WarningScreenData;

    iget-object v3, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iget-object v4, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v5, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-static/range {v0 .. v5}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->providePaymentTakingWorkflow(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen_Module_ProvidePaymentTakingWorkflowFactory;->get()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v0

    return-object v0
.end method
