.class public final synthetic Lcom/squareup/ui/main/errors/-$$Lambda$j72C0BidDZmSnBCKL_PCGEuDIgw;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/errors/-$$Lambda$j72C0BidDZmSnBCKL_PCGEuDIgw;->f$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/-$$Lambda$j72C0BidDZmSnBCKL_PCGEuDIgw;->f$0:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    check-cast p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method
