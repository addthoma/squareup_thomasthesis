.class public final Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;
.super Ljava/lang/Object;
.source "WarningWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/errors/WarningWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final buttonFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final initialViewDataProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;->buttonFlowStarterProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;->initialViewDataProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;)",
            "Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;)Lcom/squareup/ui/main/errors/WarningWorkflow;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/main/errors/WarningWorkflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/errors/WarningWorkflow;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/errors/WarningWorkflow;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;->buttonFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;->initialViewDataProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/errors/WarningScreenData;

    invoke-static {v0, v1}, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;->newInstance(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;)Lcom/squareup/ui/main/errors/WarningWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/WarningWorkflow_Factory;->get()Lcom/squareup/ui/main/errors/WarningWorkflow;

    move-result-object v0

    return-object v0
.end method
