.class public abstract Lcom/squareup/ui/main/errors/PaymentEvent;
.super Ljava/lang/Object;
.source "PaymentEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\u0003\u0004\u0005\u0006\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        "",
        "()V",
        "Lcom/squareup/ui/main/errors/ReportReaderIssue;",
        "Lcom/squareup/ui/main/errors/TakeSwipePayment;",
        "Lcom/squareup/ui/main/errors/TakeDipPayment;",
        "Lcom/squareup/ui/main/errors/TakeTapPayment;",
        "Lcom/squareup/ui/main/errors/CardFailed;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/PaymentEvent;-><init>()V

    return-void
.end method
