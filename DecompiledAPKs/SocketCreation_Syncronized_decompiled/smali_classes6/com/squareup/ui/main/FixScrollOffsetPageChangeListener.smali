.class public Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;
.super Ljava/lang/Object;
.source "FixScrollOffsetPageChangeListener.java"

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;


# instance fields
.field private final delegate:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

.field private final viewPager:Landroidx/viewpager/widget/ViewPager;


# direct methods
.method public constructor <init>(Landroidx/viewpager/widget/ViewPager;Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->viewPager:Landroidx/viewpager/widget/ViewPager;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->delegate:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    return-void
.end method

.method private fixScroll()V
    .locals 3

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getScrollX()I

    move-result v0

    .line 53
    iget-object v1, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->viewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getPageMargin()I

    move-result v2

    add-int/2addr v1, v2

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-ne v0, v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->viewPager:Landroidx/viewpager/widget/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroidx/viewpager/widget/ViewPager;->scrollBy(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1

    if-nez p1, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->fixScroll()V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->delegate:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_1

    .line 45
    invoke-interface {v0, p1}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    :cond_1
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->fixScroll()V

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->delegate:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_1

    .line 30
    invoke-interface {v0, p1, p2, p3}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    :cond_1
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/main/FixScrollOffsetPageChangeListener;->delegate:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 36
    invoke-interface {v0, p1}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    :cond_0
    return-void
.end method
