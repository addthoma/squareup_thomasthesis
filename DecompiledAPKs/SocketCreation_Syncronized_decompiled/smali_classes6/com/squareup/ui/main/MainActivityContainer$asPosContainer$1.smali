.class public final Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;
.super Ljava/lang/Object;
.source "MainActivityContainer.kt"

# interfaces
.implements Lcom/squareup/ui/main/PosContainer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;->asPosContainer()Lcom/squareup/ui/main/PosContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMainActivityContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MainActivityContainer.kt\ncom/squareup/ui/main/MainActivityContainer$asPosContainer$1\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,532:1\n57#2,4:533\n*E\n*S KotlinDebug\n*F\n+ 1 MainActivityContainer.kt\ncom/squareup/ui/main/MainActivityContainer$asPosContainer$1\n*L\n215#1,4:533\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000g\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001e\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u0005H\u0016J)\u0010\u0006\u001a\u00020\u00072\u001a\u0010\u0008\u001a\u000e\u0012\n\u0008\u0001\u0012\u0006\u0012\u0002\u0008\u00030\u00050\t\"\u0006\u0012\u0002\u0008\u00030\u0005H\u0016\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0016\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00030\u00030\u000fH\u0016J,\u0010\u0011\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00130\u0013 \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00130\u0013\u0018\u00010\u00120\u0012H\u0016J\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0008\u0010\u0017\u001a\u00020\u0007H\u0016J\u0010\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0008\u0010\u0019\u001a\u00020\u0007H\u0016J\u0016\u0010\u001a\u001a\u00020\u00072\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001cH\u0016J\u0008\u0010\u001e\u001a\u00020\u0007H\u0016J\u0018\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J,\u0010#\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u001d0\u001d \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u001d0\u001d\u0018\u00010\u00120\u0012H\u0016J\u0014\u0010$\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0%0\u0012H\u0016\u00a8\u0006&"
    }
    d2 = {
        "com/squareup/ui/main/MainActivityContainer$asPosContainer$1",
        "Lcom/squareup/ui/main/PosContainer;",
        "currentPathIncludes",
        "",
        "pathType",
        "Ljava/lang/Class;",
        "goBackPast",
        "",
        "screenTypes",
        "",
        "([Ljava/lang/Class;)V",
        "goBackPastWorkflow",
        "runnerServiceName",
        "",
        "hasViewNow",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "kotlin.jvm.PlatformType",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "onActivityCreate",
        "intent",
        "Landroid/content/Intent;",
        "onActivityFinish",
        "onActivityNewIntent",
        "onBackPressed",
        "pushStack",
        "newTop",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "resetHistory",
        "temporaryStartHistory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "direction",
        "Lflow/Direction;",
        "topOfTraversalCompleting",
        "traversalByVisibleScreens",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityContainer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 198
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public currentPathIncludes(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "pathType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->access$currentPathIncludes(Lcom/squareup/ui/main/MainActivityContainer;Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public varargs goBackPast([Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "screenTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object v0

    array-length v1, p1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Class;

    invoke-static {v0, p1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public goBackPastWorkflow(Ljava/lang/String;)V
    .locals 1

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/container/ContainerKt;->goBackPastWorkflow(Lflow/Flow;Ljava/lang/String;)V

    return-void
.end method

.method public hasViewNow()Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getHasView$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hasViewNow()Lio/reactivex/Observable;
    .locals 1

    .line 198
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->hasViewNow()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public nextHistory()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;"
        }
    .end annotation

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getNextHistory$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreate(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->access$onActivityCreate(Lcom/squareup/ui/main/MainActivityContainer;Landroid/content/Intent;)V

    return-void
.end method

.method public onActivityFinish()V
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/main/MainActivityContainer;->onActivityFinish()V

    return-void
.end method

.method public onActivityNewIntent(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/MainActivityContainer;->access$onActivityNewIntent(Lcom/squareup/ui/main/MainActivityContainer;Landroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/main/MainActivityContainer;->onBackPressed()V

    return-void
.end method

.method public pushStack(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newTop"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/container/Flows;->pushStack(Lflow/Flow;Ljava/util/List;)V

    return-void
.end method

.method public resetHistory()V
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$resetHistory(Lcom/squareup/ui/main/MainActivityContainer;)V

    return-void
.end method

.method public resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V
    .locals 1

    const-string v0, "temporaryStartHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/main/MainActivityContainer;->access$resetHistory(Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method public topOfTraversalCompleting()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getTraversalCompleting$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1$topOfTraversalCompleting$1;->INSTANCE:Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1$topOfTraversalCompleting$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public traversalByVisibleScreens()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Collection<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;>;"
        }
    .end annotation

    .line 215
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getTraversalCompleting$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    check-cast v0, Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v1}, Lcom/squareup/ui/main/MainActivityContainer;->access$getNextHistory$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    check-cast v1, Lio/reactivex/Observable;

    .line 534
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 535
    new-instance v2, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1$traversalByVisibleScreens$$inlined$combineLatest$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1$traversalByVisibleScreens$$inlined$combineLatest$1;-><init>(Lcom/squareup/ui/main/MainActivityContainer$asPosContainer$1;)V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 533
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
