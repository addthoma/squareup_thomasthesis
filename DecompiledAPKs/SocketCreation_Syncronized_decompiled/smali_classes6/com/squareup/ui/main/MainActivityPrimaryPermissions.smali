.class public final Lcom/squareup/ui/main/MainActivityPrimaryPermissions;
.super Ljava/lang/Object;
.source "MainActivityPrimaryPermissions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\"\u0018\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "PRIMARY_SYSTEM_PERMISSIONS",
        "",
        "Lcom/squareup/systempermissions/SystemPermission;",
        "[Lcom/squareup/systempermissions/SystemPermission;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final PRIMARY_SYSTEM_PERMISSIONS:[Lcom/squareup/systempermissions/SystemPermission;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/systempermissions/SystemPermission;

    .line 8
    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->STORAGE:Lcom/squareup/systempermissions/SystemPermission;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/main/MainActivityPrimaryPermissions;->PRIMARY_SYSTEM_PERMISSIONS:[Lcom/squareup/systempermissions/SystemPermission;

    return-void
.end method
