.class public final Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "MainActivityContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;->nextHistoryToShowWhileResetting(Lflow/History;)Lflow/History;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newHistory:Lflow/History;

.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityContainer;Lflow/History;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")V"
        }
    .end annotation

    .line 294
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    iput-object p2, p0, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;->$newHistory:Lflow/History;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 0

    return-void
.end method

.method public success()V
    .locals 3

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getSalesReportDetailLevelHolder$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->grantPermissionToPinnedEmployee(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/main/MainActivityContainer;->requireFlow()Lflow/Flow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer$nextHistoryToShowWhileResetting$1;->$newHistory:Lflow/History;

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {v0, v1, v2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method
