.class public final Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;
.super Ljava/lang/Object;
.source "PaymentFlowHistoryFactoryStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PaymentFlowHistoryFactory;Ldagger/Lazy;)Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;-><init>(Lcom/squareup/ui/main/PaymentFlowHistoryFactory;Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PaymentFlowHistoryFactory;

    iget-object v1, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;->newInstance(Lcom/squareup/ui/main/PaymentFlowHistoryFactory;Ldagger/Lazy;)Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter_Factory;->get()Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    move-result-object v0

    return-object v0
.end method
