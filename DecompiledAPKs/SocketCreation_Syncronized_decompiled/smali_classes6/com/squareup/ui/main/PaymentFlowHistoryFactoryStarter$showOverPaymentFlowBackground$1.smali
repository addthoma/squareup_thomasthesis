.class final Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;
.super Ljava/lang/Object;
.source "PaymentFlowHistoryFactory.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->showOverPaymentFlowBackground(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "currentHistory",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $stack:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;->this$0:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    iput-object p2, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;->$stack:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    const-string v0, "currentHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;->this$0:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    invoke-static {v0}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->access$getPaymentFlowHistoryFactory$p(Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;)Lcom/squareup/ui/main/PaymentFlowHistoryFactory;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactory;->historyToPaymentFlowBackground(Lflow/History;)Lflow/History;

    move-result-object p1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;->$stack:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->pushStack(Lflow/History;Ljava/util/List;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    .line 44
    check-cast v0, Lflow/History;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Direction;

    .line 48
    invoke-static {v0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
