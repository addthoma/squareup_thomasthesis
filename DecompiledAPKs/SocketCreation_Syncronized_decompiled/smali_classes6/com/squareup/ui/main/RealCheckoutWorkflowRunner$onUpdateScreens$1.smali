.class final Lcom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1;
.super Ljava/lang/Object;
.source "RealCheckoutWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckoutWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckoutWorkflowRunner.kt\ncom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,67:1\n1360#2:68\n1429#2,3:69\n*E\n*S KotlinDebug\n*F\n+ 1 RealCheckoutWorkflowRunner.kt\ncom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1\n*L\n50#1:68\n50#1,3:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "stack",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1;->this$0:Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1;->apply(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;"
        }
    .end annotation

    const-string v0, "stack"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    check-cast p1, Ljava/lang/Iterable;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 69
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 70
    check-cast v1, Lcom/squareup/container/WorkflowTreeKey;

    .line 50
    iget-object v2, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1;->this$0:Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;

    invoke-static {v2}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->access$getCheckoutScopeParentProvider$p(Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;)Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;->getCheckoutScopeParent()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v1, v2}, Lcom/squareup/container/WorkflowTreeKey;->reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
