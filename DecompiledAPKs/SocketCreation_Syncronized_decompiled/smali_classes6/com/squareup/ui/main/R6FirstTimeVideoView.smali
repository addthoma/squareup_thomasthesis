.class public Lcom/squareup/ui/main/R6FirstTimeVideoView;
.super Landroid/widget/FrameLayout;
.source "R6FirstTimeVideoView.java"


# instance fields
.field private cancelButton:Lcom/squareup/glyph/SquareGlyphView;

.field private fallbackImage:Landroid/widget/ImageView;

.field presenter:Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private textContainer:Landroid/widget/LinearLayout;

.field private textLayer:Landroid/widget/LinearLayout;

.field private videoView:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-class p2, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Component;->inject(Lcom/squareup/ui/main/R6FirstTimeVideoView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 109
    sget v0, Lcom/squareup/readertutorial/R$id;->r6_first_time_video_text_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->textContainer:Landroid/widget/LinearLayout;

    .line 110
    sget v0, Lcom/squareup/readertutorial/R$id;->r6_first_time_video_surface_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    .line 111
    sget v0, Lcom/squareup/readertutorial/R$id;->r6_first_time_video_done:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 112
    sget v0, Lcom/squareup/readertutorial/R$id;->r6_first_time_video_text_layer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->textLayer:Landroid/widget/LinearLayout;

    .line 113
    sget v0, Lcom/squareup/readertutorial/R$id;->r6_first_time_video_fallback_background:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->fallbackImage:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method displayFallbackImage()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->fallbackImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 99
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v2, :cond_0

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v2, :cond_0

    .line 100
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 101
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->fallbackImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->fallbackImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->textLayer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$prepareMovie$0$R6FirstTimeVideoView(Landroid/media/MediaPlayer;II)Z
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->presenter:Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->onPlaybackError(II)V

    const/4 p1, 0x1

    return p1
.end method

.method public synthetic lambda$prepareMovie$1$R6FirstTimeVideoView(Landroid/media/MediaPlayer;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->presenter:Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->onPrepared(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->stopPlayback()V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->presenter:Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 45
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 37
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->bindViews()V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->presenter:Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method prepareMovie(Landroid/net/Uri;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$R6FirstTimeVideoView$rFvjMLiHoVYFYegxCdqB3tUNV-I;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/-$$Lambda$R6FirstTimeVideoView$rFvjMLiHoVYFYegxCdqB3tUNV-I;-><init>(Lcom/squareup/ui/main/R6FirstTimeVideoView;)V

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$R6FirstTimeVideoView$s1d_KSqO3F4dD8c5fqlcV0um6BM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/-$$Lambda$R6FirstTimeVideoView$s1d_KSqO3F4dD8c5fqlcV0um6BM;-><init>(Lcom/squareup/ui/main/R6FirstTimeVideoView;)V

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v0, Lcom/squareup/ui/main/R6FirstTimeVideoView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/R6FirstTimeVideoView$1;-><init>(Lcom/squareup/ui/main/R6FirstTimeVideoView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showTextAndCancelButton(Landroid/graphics/Point;)V
    .locals 4

    .line 66
    iget v0, p1, Landroid/graphics/Point;->x:I

    div-int/lit8 v0, v0, 0x2

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 70
    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 71
    iget p1, p1, Landroid/graphics/Point;->y:I

    div-int/lit8 p1, p1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, v1}, Lcom/squareup/glyph/SquareGlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->textContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 76
    iget v1, p1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v3, p1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {p1, v0, v1, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->textContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->textLayer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method startPlayback()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    return-void
.end method

.method stopPlayback()V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoView;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    return-void
.end method
