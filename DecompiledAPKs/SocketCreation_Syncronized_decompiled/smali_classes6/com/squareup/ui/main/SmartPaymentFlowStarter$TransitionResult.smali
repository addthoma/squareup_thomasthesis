.class public final enum Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
.super Ljava/lang/Enum;
.source "SmartPaymentFlowStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentFlowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TransitionResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum BATTERY_DEAD:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum BLOCKING_FIRMWARE_UPDATE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum NOT_ONLINE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum NOT_SINGLE_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum NOT_SPLIT_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum NO_ACTIVE_CARD_READER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum NO_SECURE_SESSION:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum NO_VALID_TMS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum REINSERT_CHIP:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

.field public static final enum SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 57
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 58
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v2, 0x1

    const-string v3, "BATTERY_DEAD"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->BATTERY_DEAD:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 59
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v3, 0x2

    const-string v4, "BLOCKING_FIRMWARE_UPDATE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->BLOCKING_FIRMWARE_UPDATE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 60
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v4, 0x3

    const-string v5, "NO_ACTIVE_CARD_READER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_ACTIVE_CARD_READER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 61
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v5, 0x4

    const-string v6, "NO_SECURE_SESSION"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_SECURE_SESSION:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 62
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v6, 0x5

    const-string v7, "NO_VALID_TMS"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_VALID_TMS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 63
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v7, 0x6

    const-string v8, "NOT_ONLINE"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_ONLINE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 64
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/4 v8, 0x7

    const-string v9, "NOT_SINGLE_TENDER"

    invoke-direct {v0, v9, v8}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SINGLE_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 65
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/16 v9, 0x8

    const-string v10, "NOT_SPLIT_TENDER"

    invoke-direct {v0, v10, v9}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SPLIT_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 66
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/16 v10, 0x9

    const-string v11, "REINSERT_CHIP"

    invoke-direct {v0, v11, v10}, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->REINSERT_CHIP:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    .line 56
    sget-object v11, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->BATTERY_DEAD:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->BLOCKING_FIRMWARE_UPDATE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_ACTIVE_CARD_READER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_SECURE_SESSION:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NO_VALID_TMS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_ONLINE:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SINGLE_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->NOT_SPLIT_TENDER:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->REINSERT_CHIP:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->$VALUES:[Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->$VALUES:[Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    invoke-virtual {v0}, [Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    return-object v0
.end method
