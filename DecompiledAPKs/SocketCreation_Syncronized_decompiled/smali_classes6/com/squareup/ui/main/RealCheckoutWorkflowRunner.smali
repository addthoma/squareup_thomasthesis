.class public final Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealCheckoutWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/ui/main/CheckoutWorkflowRunner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        ">;",
        "Lcom/squareup/ui/main/CheckoutWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B/\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0012\u0010\u0016\u001a\u00020\u00132\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0014\u0010\u0019\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001c0\u001b0\u001aH\u0016J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0003H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;",
        "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        "workflow",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow;",
        "paymentViewFactory",
        "Lcom/squareup/tenderpayment/PaymentViewFactory;",
        "checkoutScopeParentProvider",
        "Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "paymentProcessingEventSink",
        "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
        "(Lcom/squareup/checkoutflow/CheckoutWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V",
        "getWorkflow",
        "()Lcom/squareup/checkoutflow/CheckoutWorkflow;",
        "isPaymentWorkflowScreen",
        "",
        "key",
        "Lflow/path/Path;",
        "isSelectMethodScreen",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "onUpdateScreens",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "start",
        "",
        "startArg",
        "pos-main-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final checkoutScopeParentProvider:Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final paymentViewFactory:Lcom/squareup/tenderpayment/PaymentViewFactory;

.field private final workflow:Lcom/squareup/checkoutflow/CheckoutWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/CheckoutWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutScopeParentProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentProcessingEventSink"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->Companion:Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 28
    invoke-interface {p4}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 29
    move-object v4, p2

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    .line 26
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->workflow:Lcom/squareup/checkoutflow/CheckoutWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->paymentViewFactory:Lcom/squareup/tenderpayment/PaymentViewFactory;

    iput-object p3, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->checkoutScopeParentProvider:Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;

    iput-object p5, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    return-void
.end method

.method public static final synthetic access$getCheckoutScopeParentProvider$p(Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;)Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->checkoutScopeParentProvider:Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/checkoutflow/CheckoutWorkflow;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->workflow:Lcom/squareup/checkoutflow/CheckoutWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->getWorkflow()Lcom/squareup/checkoutflow/CheckoutWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public isPaymentWorkflowScreen(Lflow/path/Path;)Z
    .locals 2

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    instance-of v0, p1, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->paymentViewFactory:Lcom/squareup/tenderpayment/PaymentViewFactory;

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    iget-object v1, p1, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/PaymentViewFactory;->isInSelectMethodWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->paymentViewFactory:Lcom/squareup/tenderpayment/PaymentViewFactory;

    iget-object p1, p1, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/PaymentViewFactory;->isInBuyerCheckoutWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isSelectMethodScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 36
    :cond_0
    instance-of v1, p1, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->paymentViewFactory:Lcom/squareup/tenderpayment/PaymentViewFactory;

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    iget-object p1, p1, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-interface {v1, p1}, Lcom/squareup/tenderpayment/PaymentViewFactory;->matchesSelectMethodKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public onUpdateScreens()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation

    .line 48
    invoke-super {p0}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner$onUpdateScreens$1;-><init>(Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "super.onUpdateScreens()\n\u2026copeParent()) }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public start(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;)V
    .locals 1

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentStarted()V

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
