.class public interface abstract Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;
.super Ljava/lang/Object;
.source "PermissionDeniedScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/PermissionDeniedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/permissions/PermissionDeniedView;)V
.end method

.method public abstract presenter()Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;
.end method
