.class public Lcom/squareup/ui/onboarding/SilentExitView;
.super Landroid/widget/FrameLayout;
.source "SilentExitView.java"


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const-class p2, Lcom/squareup/ui/onboarding/SilentExitScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/SilentExitScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/SilentExitScreen$Component;->inject(Lcom/squareup/ui/onboarding/SilentExitView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/onboarding/SilentExitView;->presenter:Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 29
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 23
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/onboarding/SilentExitView;->presenter:Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/SilentExitScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
