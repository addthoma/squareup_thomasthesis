.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->doStart(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0001j\u0002`\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "nonDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "state",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $depositSpeedInput:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic $workflow:Lcom/squareup/workflow/rx1/Workflow;

.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$depositSpeedInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$depositSpeedScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;)Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$depositSpeedInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneScreenKt;->DepositSpeedPhoneScreen(Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_2

    .line 156
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$depositSpeedScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;)Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$depositSpeedInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletScreenKt;->DepositSpeedTabletScreen(Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_2

    .line 158
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 159
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    if-eqz v0, :cond_3

    goto :goto_0

    .line 160
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    if-eqz v0, :cond_4

    .line 161
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$depositCardLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;

    move-result-object p1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 160
    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingScreenKt;->DepositCardLinkingScreen(Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_2

    .line 170
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    if-eqz v0, :cond_5

    goto :goto_1

    .line 171
    :cond_5
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    if-eqz v0, :cond_6

    goto :goto_1

    .line 172
    :cond_6
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;

    if-eqz v0, :cond_7

    goto :goto_1

    .line 173
    :cond_7
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    if-eqz v0, :cond_8

    goto :goto_1

    .line 174
    :cond_8
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    if-eqz v0, :cond_9

    goto :goto_1

    .line 175
    :cond_9
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    if-eqz v0, :cond_a

    .line 176
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$depositBankLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    move-result-object p1

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 175
    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingScreenKt;->DepositBankLinkingScreen(Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_2

    .line 189
    :cond_a
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    invoke-static {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->access$depositsSettingsResultScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    move-result-object p1

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->$workflow:Lcom/squareup/workflow/rx1/Workflow;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 188
    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultScreenKt;->DepositLinkingResultScreen(Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
