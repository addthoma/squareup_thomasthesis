.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;
.super Ljava/lang/Object;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnboardingFactory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;",
        "reactor",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
        "model",
        "Lcom/squareup/ui/onboarding/OnboardingModel;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "rateFormatter",
        "Lcom/squareup/text/RateFormatter;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/Features;)V",
        "create",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rateFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->rateFormatter:Lcom/squareup/text/RateFormatter;

    iput-object p6, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public create()Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;
    .locals 9

    .line 114
    new-instance v7, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "model.personalName"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->rateFormatter:Lcom/squareup/text/RateFormatter;

    .line 116
    new-instance v6, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_CHOOSE_DEFAULT_DEPOSIT_METHOD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v8, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->isBankingOnly()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 116
    :goto_0
    invoke-direct {v6, v8, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;-><init>(ZZ)V

    move-object v0, v7

    .line 114
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;)V

    return-object v7
.end method
