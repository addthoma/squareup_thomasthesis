.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;
.super Ljava/lang/Object;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Reactor<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositOptionsReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositOptionsReactor.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsReactor\n*L\n1#1,413:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0003012B\'\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0018\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000fH\u0002J\u0008\u0010\u001a\u001a\u00020\u0002H\u0002J\u0016\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u001c2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0016\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u001c2\u0006\u0010\u0017\u001a\u00020\u001eH\u0002J2\u0010\u001f\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040!0 2\u0006\u0010\u0017\u001a\u00020\u00022\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00030#H\u0016J\u001c\u0010$\u001a\u00020%2\u0006\u0010\u0017\u001a\u00020\u00022\n\u0008\u0002\u0010&\u001a\u0004\u0018\u00010\'H\u0002J\u0008\u0010(\u001a\u00020\u0002H\u0002J\u0016\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u001c2\u0006\u0010\u0017\u001a\u00020*H\u0002J0\u0010+\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040,j\u0002`-2\n\u0008\u0002\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u00020\u000f8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "device",
        "Lcom/squareup/util/Device;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "debitCardSettings",
        "Lcom/squareup/debitcard/DebitCardSettings;",
        "depositScheduleSettings",
        "Lcom/squareup/depositschedule/DepositScheduleSettings;",
        "(Lcom/squareup/util/Device;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/debitcard/DebitCardSettings;Lcom/squareup/depositschedule/DepositScheduleSettings;)V",
        "canSelectDepositSpeed",
        "",
        "canSelectDepositSpeed$annotations",
        "()V",
        "getCanSelectDepositSpeed$onboarding_release",
        "()Z",
        "setCanSelectDepositSpeed$onboarding_release",
        "(Z)V",
        "bankSuccessOrPending",
        "state",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;",
        "bankSuccess",
        "initialState",
        "linkBankAccount",
        "Lio/reactivex/Single;",
        "linkDebitCard",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "prepareToLinkBankAccountState",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;",
        "bankAccountType",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "selectingDepositSpeedState",
        "setUpSameDayDeposit",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;",
        "startWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "DepositOptionsEvent",
        "DepositOptionsResult",
        "DepositOptionsState",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private canSelectDepositSpeed:Z

.field private final debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

.field private final depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/debitcard/DebitCardSettings;Lcom/squareup/depositschedule/DepositScheduleSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccountSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "debitCardSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositScheduleSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    return-void
.end method

.method public static final synthetic access$bankSuccessOrPending(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;Z)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 0

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->bankSuccessOrPending(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;Z)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$linkBankAccount(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;)Lio/reactivex/Single;
    .locals 0

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->linkBankAccount(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$linkDebitCard(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;)Lio/reactivex/Single;
    .locals 0

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->linkDebitCard(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$prepareToLinkBankAccountState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;
    .locals 0

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->prepareToLinkBankAccountState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$selectingDepositSpeedState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->selectingDepositSpeedState()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setUpSameDayDeposit(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;)Lio/reactivex/Single;
    .locals 0

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->setUpSameDayDeposit(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final bankSuccessOrPending(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;Z)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 0

    .line 373
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;->getSameDayDepositSelected()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;

    invoke-direct {p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;-><init>(Z)V

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 374
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 375
    :cond_1
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    :goto_0
    return-object p1
.end method

.method public static synthetic canSelectDepositSpeed$annotations()V
    .locals 0

    return-void
.end method

.method private final initialState()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 4

    .line 332
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->canSelectDepositSpeed:Z

    if-eqz v0, :cond_0

    .line 333
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->selectingDepositSpeedState()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object v0

    goto :goto_0

    .line 335
    :cond_0
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    :goto_0
    return-object v0
.end method

.method private final linkBankAccount(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            ">;"
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/squareup/banklinking/BankAccountSettings$DefaultImpls;->linkBankAccount$default(Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    .line 355
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkBankAccount$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bankAccountSettings.link\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final linkDebitCard(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            ">;"
        }
    .end annotation

    .line 380
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;-><init>()V

    .line 381
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object v0

    .line 382
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 383
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->skip_verification_email(Ljava/lang/Boolean;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object v0

    .line 384
    invoke-virtual {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object v0

    .line 386
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->debitCardSettings:Lcom/squareup/debitcard/DebitCardSettings;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/debitcard/DebitCardSettings;->linkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lio/reactivex/Single;

    move-result-object v0

    .line 387
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$linkDebitCard$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "debitCardSettings.linkCa\u2026ure\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final prepareToLinkBankAccountState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;
    .locals 2

    if-eqz p1, :cond_1

    .line 347
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;

    .line 348
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    .line 349
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;->getSameDayDepositSelected()Z

    move-result v1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object p2

    .line 348
    :goto_0
    invoke-direct {v0, v1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;)V

    return-object v0

    .line 347
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.onboarding.bank.DepositOptionsReactor.DepositOptionsState.BankLinking"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic prepareToLinkBankAccountState$default(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/protos/client/bankaccount/BankAccountType;ILjava/lang/Object;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 345
    check-cast p2, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->prepareToLinkBankAccountState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    move-result-object p0

    return-object p0
.end method

.method private final selectingDepositSpeedState()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;

    :goto_0
    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    return-object v0
.end method

.method private final setUpSameDayDeposit(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            ">;"
        }
    .end annotation

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->depositScheduleSettings:Lcom/squareup/depositschedule/DepositScheduleSettings;

    invoke-interface {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings;->setAllToSameDay()Lio/reactivex/Single;

    move-result-object v0

    .line 402
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "depositScheduleSettings.\u2026ure\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static synthetic startWorkflow$default(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/workflow/Snapshot;ZILjava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 211
    check-cast p1, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->startWorkflow(Lcom/squareup/workflow/Snapshot;Z)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getCanSelectDepositSpeed$onboarding_release()Z
    .locals 1

    .line 208
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->canSelectDepositSpeed:Z

    return v0
.end method

.method public onAbandoned(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/Reactor$DefaultImpls;->onAbandoned(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onAbandoned(Ljava/lang/Object;)V
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->onAbandoned(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    return-void
.end method

.method public onReact(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
            ">;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_3

    .line 234
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$2;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_3

    .line 242
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 243
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;

    if-eqz v0, :cond_3

    :goto_0
    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3;

    invoke-direct {p1, p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_3

    .line 249
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;

    if-eqz v0, :cond_4

    goto :goto_1

    .line 250
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;

    if-eqz v0, :cond_5

    :goto_1
    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$4;

    invoke-direct {p1, p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$4;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_3

    .line 258
    :cond_5
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$5;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_3

    .line 287
    :cond_6
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$6;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_3

    .line 293
    :cond_7
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$7;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_3

    .line 301
    :cond_8
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    if-eqz v0, :cond_9

    goto :goto_2

    .line 302
    :cond_9
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    if-eqz v0, :cond_a

    :goto_2
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$8;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_3

    .line 307
    :cond_a
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$9;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$9;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_3

    .line 310
    :cond_b
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;

    if-eqz v0, :cond_c

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$10;

    invoke-direct {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$10;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_3

    .line 316
    :cond_c
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    if-eqz v0, :cond_d

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$11;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$11;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_3

    .line 319
    :cond_d
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    if-eqz v0, :cond_e

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$12;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$12;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_3

    .line 322
    :cond_e
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    if-eqz v0, :cond_f

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$13;

    invoke-direct {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$13;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    :goto_3
    return-object p1

    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->onReact(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final setCanSelectDepositSpeed$onboarding_release(Z)V
    .locals 0

    .line 208
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->canSelectDepositSpeed:Z

    return-void
.end method

.method public final startWorkflow(Lcom/squareup/workflow/Snapshot;Z)Lcom/squareup/workflow/rx1/Workflow;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            "Z)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
            ">;"
        }
    .end annotation

    .line 214
    iput-boolean p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->canSelectDepositSpeed:Z

    if-eqz p1, :cond_0

    .line 216
    sget-object p2, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 217
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->initialState()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    move-result-object p1

    .line 218
    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
