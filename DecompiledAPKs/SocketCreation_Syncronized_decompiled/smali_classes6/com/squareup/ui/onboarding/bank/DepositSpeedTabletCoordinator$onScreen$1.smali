.class final Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$1;
.super Ljava/lang/Object;
.source "DepositSpeedTabletCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedTabletCoordinator$onScreen$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/NextBusinessDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/NextBusinessDayDepositSelected;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
