.class final Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$2;
.super Ljava/lang/Object;
.source "DepositBankLinkingCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->setUpAccountTypeField(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "v",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "hasFocus",
        "",
        "onFocusChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$2;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    const-string p2, "v"

    .line 170
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$2;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p2, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$AccountTypeClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$AccountTypeClicked;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
