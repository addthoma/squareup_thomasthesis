.class public interface abstract Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;
.super Ljava/lang/Object;
.source "DepositOptionsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ResultHandler"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;",
        "",
        "onDepositOptionsResult",
        "Lio/reactivex/Completable;",
        "result",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "parentTreeKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onDepositOptionsResult(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;Lcom/squareup/ui/main/RegisterTreeKey;)Lio/reactivex/Completable;
.end method
