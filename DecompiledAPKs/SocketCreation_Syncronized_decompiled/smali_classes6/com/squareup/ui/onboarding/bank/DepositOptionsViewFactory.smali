.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "DepositOptionsViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "depositBankLinkingFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;",
        "depositCardLinkingFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;",
        "(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;)V",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "depositBankLinkingFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositCardLinkingFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 13
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 14
    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhone;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositSpeedPhone;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhone;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/onboarding/flow/R$layout;->deposit_speed_view:I

    .line 15
    sget-object v4, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 13
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 17
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 18
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositSpeedTablet;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositSpeedTablet;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedTablet;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    sget v5, Lcom/squareup/onboarding/flow/R$layout;->deposit_speed_view:I

    .line 19
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$2;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    .line 17
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 21
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialog;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 22
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositBankLinking;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    sget v5, Lcom/squareup/onboarding/flow/R$layout;->deposit_bank_linking_view:I

    .line 24
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$4;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$4;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 22
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x3

    aput-object p1, v0, v1

    .line 26
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 27
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositAccountTypeDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 28
    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$5;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 26
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x4

    aput-object p1, v0, v1

    .line 30
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 31
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 32
    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$6;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 30
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x5

    aput-object p1, v0, v1

    .line 34
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 35
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositCardLinking;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositCardLinking;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinking;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    sget v4, Lcom/squareup/onboarding/flow/R$layout;->deposit_card_linking_view:I

    .line 36
    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$7;

    invoke-direct {p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$7;-><init>(Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 34
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x6

    aput-object p1, v0, p2

    .line 38
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 39
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositLinkingResult;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/onboarding/flow/R$layout;->deposit_linking_result_view:I

    .line 40
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$8;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory$8;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 38
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x7

    aput-object p1, v0, p2

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
