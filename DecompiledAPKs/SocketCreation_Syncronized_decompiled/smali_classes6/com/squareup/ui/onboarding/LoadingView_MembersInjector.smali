.class public final Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;
.super Ljava/lang/Object;
.source "LoadingView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/LoadingView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/LoadingView;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/onboarding/LoadingView;Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/onboarding/LoadingView;->presenter:Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/LoadingView;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/LoadingView;Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/onboarding/LoadingView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/LoadingView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/LoadingView;)V

    return-void
.end method
