.class public interface abstract Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;
.super Ljava/lang/Object;
.source "ConfirmMagstripeAddressScreen.java"

# interfaces
.implements Lcom/squareup/address/AddressLayout$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V
.end method
