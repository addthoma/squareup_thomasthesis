.class Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;
.super Ljava/lang/Object;
.source "ConfirmMagstripeAddressScreen.java"

# interfaces
.implements Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/FailureMessageFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener<",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

.field final synthetic val$runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;->val$runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailureViewDismissed(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;->this$0:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->verifyAndSendShippingAddress()V

    :cond_0
    return-void
.end method

.method public onProgressViewDismissed(Lcom/squareup/server/shipping/UpdateAddressResponse;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;->val$runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onShippedOrSkippedMagstripeReader()V

    :cond_0
    return-void
.end method

.method public bridge synthetic onProgressViewDismissed(Ljava/lang/Object;)V
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/server/shipping/UpdateAddressResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;->onProgressViewDismissed(Lcom/squareup/server/shipping/UpdateAddressResponse;)V

    return-void
.end method
