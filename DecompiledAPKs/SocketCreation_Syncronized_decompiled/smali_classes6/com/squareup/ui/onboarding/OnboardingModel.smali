.class public Lcom/squareup/ui/onboarding/OnboardingModel;
.super Ljava/lang/Object;
.source "OnboardingModel.java"


# instance fields
.field private activationErrorState:Lcom/squareup/server/activation/ActivationStatus$State;

.field private final activationLaunchMode:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

.field private bankingOnly:Z

.field private birthDate:Lcom/squareup/ui/SquareDate;

.field private businessCategory:Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

.field private businessName:Ljava/lang/String;

.field private businessSubcategory:Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;

.field private destinationAfterOnboarding:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

.field private ein:Ljava/lang/String;

.field private isRestart:Z

.field private lastSsn:Ljava/lang/String;

.field private personalAddress:Lcom/squareup/address/Address;

.field private personalFirstName:Ljava/lang/String;

.field private personalLastName:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private quizQuestions:[Lcom/squareup/server/activation/QuizQuestion;

.field private revenue:Lcom/squareup/server/activation/ActivationResources$RevenueEntry;

.field private salePrice:Lcom/squareup/protos/common/Money;

.field private shippingAddress:Lcom/squareup/address/Address;

.field private shippingName:Ljava/lang/String;

.field private shouldPromptActivate:Z

.field private showLaterInPersonalInfo:Z

.field private tax:Lcom/squareup/protos/common/Money;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->activationLaunchMode:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->destinationAfterOnboarding:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    return-void
.end method

.method public static create(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)Lcom/squareup/ui/onboarding/OnboardingModel;
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/OnboardingModel;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V

    .line 61
    sget-object p1, Lcom/squareup/ui/onboarding/OnboardingModel$1;->$SwitchMap$com$squareup$onboarding$OnboardingStarter$ActivationLaunchMode:[I

    invoke-virtual {p0}, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->ordinal()I

    move-result p0

    aget p0, p1, p0

    const/4 p1, 0x1

    if-eq p0, p1, :cond_2

    const/4 p1, 0x2

    if-eq p0, p1, :cond_1

    const/4 p1, 0x3

    if-eq p0, p1, :cond_1

    const/4 p1, 0x4

    if-eq p0, p1, :cond_0

    goto :goto_0

    .line 69
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setShouldPromptActivate()V

    .line 70
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setRestart()V

    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setRestart()V

    :cond_2
    :goto_0
    return-object v0
.end method


# virtual methods
.method public getActivationErrorState()Lcom/squareup/server/activation/ActivationStatus$State;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->activationErrorState:Lcom/squareup/server/activation/ActivationStatus$State;

    return-object v0
.end method

.method public getActivationLaunchMode()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->activationLaunchMode:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    return-object v0
.end method

.method public getBirthDate()Lcom/squareup/ui/SquareDate;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->birthDate:Lcom/squareup/ui/SquareDate;

    return-object v0
.end method

.method public getBusinessCategory()Lcom/squareup/server/activation/ActivationResources$BusinessCategory;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessCategory:Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    return-object v0
.end method

.method public getBusinessKey()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessSubcategory:Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;->key:Ljava/lang/String;

    return-object v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessCategory:Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->key:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getBusinessName()Ljava/lang/String;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingModel;->skipBusinessInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDestinationAfterOnboarding()Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->destinationAfterOnboarding:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    return-object v0
.end method

.method public getEin()Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->ein:Ljava/lang/String;

    return-object v0
.end method

.method public getLastSsn()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->lastSsn:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonalAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public getPersonalFirstName()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonalLastName()Ljava/lang/String;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonalName()Ljava/lang/String;
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalFirstName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalLastName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 159
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalFirstName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalLastName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getQuizQuestions()[Lcom/squareup/server/activation/QuizQuestion;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->quizQuestions:[Lcom/squareup/server/activation/QuizQuestion;

    return-object v0
.end method

.method public getRevenue()Lcom/squareup/server/activation/ActivationResources$RevenueEntry;
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->revenue:Lcom/squareup/server/activation/ActivationResources$RevenueEntry;

    return-object v0
.end method

.method public getSalePrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->salePrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getShippingAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->shippingAddress:Lcom/squareup/address/Address;

    if-eqz v0, :cond_0

    return-object v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalAddress:Lcom/squareup/address/Address;

    if-eqz v0, :cond_1

    return-object v0

    .line 190
    :cond_1
    sget-object v0, Lcom/squareup/address/Address;->EMPTY:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public getShippingName()Ljava/lang/String;
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->shippingName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalName()Ljava/lang/String;

    move-result-object v0

    .line 152
    :cond_0
    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTax()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->tax:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public isBankingOnly()Z
    .locals 1

    .line 231
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->bankingOnly:Z

    return v0
.end method

.method public isRestart()Z
    .locals 1

    .line 255
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->isRestart:Z

    return v0
.end method

.method public setActivationErrorState(Lcom/squareup/server/activation/ActivationStatus$State;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->activationErrorState:Lcom/squareup/server/activation/ActivationStatus$State;

    return-void
.end method

.method public setBankingOnly()V
    .locals 1

    const/4 v0, 0x1

    .line 235
    iput-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->bankingOnly:Z

    return-void
.end method

.method public setBirthDate(Lcom/squareup/ui/SquareDate;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->birthDate:Lcom/squareup/ui/SquareDate;

    return-void
.end method

.method public setBusinessCategory(Lcom/squareup/server/activation/ActivationResources$BusinessCategory;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessCategory:Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    return-void
.end method

.method public setBusinessName(Ljava/lang/String;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessName:Ljava/lang/String;

    return-void
.end method

.method public setBusinessSubcategory(Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessSubcategory:Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;

    return-void
.end method

.method public setDestinationAfterOnboarding(Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->destinationAfterOnboarding:Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    return-void
.end method

.method public setEin(Ljava/lang/String;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->ein:Ljava/lang/String;

    return-void
.end method

.method public setLastSsn(Ljava/lang/String;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->lastSsn:Ljava/lang/String;

    return-void
.end method

.method public setPersonalAddress(Lcom/squareup/address/Address;)V
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalAddress:Lcom/squareup/address/Address;

    return-void
.end method

.method public setPersonalFirstName(Ljava/lang/String;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalFirstName:Ljava/lang/String;

    return-void
.end method

.method public setPersonalLastName(Ljava/lang/String;)V
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->personalLastName:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->phoneNumber:Ljava/lang/String;

    return-void
.end method

.method public setQuizQuestions([Lcom/squareup/server/activation/QuizQuestion;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->quizQuestions:[Lcom/squareup/server/activation/QuizQuestion;

    return-void
.end method

.method public setRestart()V
    .locals 1

    const/4 v0, 0x1

    .line 259
    iput-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->isRestart:Z

    return-void
.end method

.method public setRevenue(Lcom/squareup/server/activation/ActivationResources$RevenueEntry;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->revenue:Lcom/squareup/server/activation/ActivationResources$RevenueEntry;

    return-void
.end method

.method public setSalePrice(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->salePrice:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public setShippingAddress(Lcom/squareup/address/Address;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->shippingAddress:Lcom/squareup/address/Address;

    return-void
.end method

.method public setShippingName(Ljava/lang/String;)V
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->shippingName:Ljava/lang/String;

    return-void
.end method

.method public setShouldPromptActivate()V
    .locals 1

    const/4 v0, 0x1

    .line 239
    iput-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->shouldPromptActivate:Z

    return-void
.end method

.method public setShowLaterInPersonalInfo()V
    .locals 1

    const/4 v0, 0x1

    .line 251
    iput-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->showLaterInPersonalInfo:Z

    return-void
.end method

.method public setTax(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->tax:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public shouldPromptActivate()Z
    .locals 1

    .line 243
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->shouldPromptActivate:Z

    return v0
.end method

.method public showLaterInPersonalInfo()Z
    .locals 1

    .line 247
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->showLaterInPersonalInfo:Z

    return v0
.end method

.method public skipBusinessInfo()Z
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessCategory:Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->skip_business_info:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingModel;->businessSubcategory:Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;->skip_business_info:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
