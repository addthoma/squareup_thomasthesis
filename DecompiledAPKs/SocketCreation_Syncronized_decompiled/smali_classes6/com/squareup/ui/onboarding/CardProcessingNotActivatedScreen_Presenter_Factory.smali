.class public final Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CardProcessingNotActivatedScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
