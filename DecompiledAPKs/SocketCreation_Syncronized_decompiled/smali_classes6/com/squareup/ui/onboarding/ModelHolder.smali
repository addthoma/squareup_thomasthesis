.class public Lcom/squareup/ui/onboarding/ModelHolder;
.super Ljava/lang/Object;
.source "ModelHolder.java"


# instance fields
.field private model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final onboardingModelBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ModelHolder;->onboardingModelBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method private readOrCreate(Landroid/os/Bundle;Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)Lcom/squareup/ui/onboarding/OnboardingModel;
    .locals 1

    if-eqz p1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ModelHolder;->onboardingModelBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingModel;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    .line 49
    invoke-static {p2, p3}, Lcom/squareup/ui/onboarding/OnboardingModel;->create(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object p1

    :cond_1
    return-object p1
.end method


# virtual methods
.method public getModel()Lcom/squareup/ui/onboarding/OnboardingModel;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ModelHolder;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    return-object v0
.end method

.method public maybeRestoreFromBundle(Landroid/os/Bundle;Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ModelHolder;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    if-nez v0, :cond_0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/ModelHolder;->readOrCreate(Landroid/os/Bundle;Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;)Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/onboarding/ModelHolder;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    :cond_0
    return-void
.end method

.method public toBundle(Landroid/os/Bundle;)V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ModelHolder;->onboardingModelBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ModelHolder;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method
