.class public Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;
.super Lcom/squareup/widgets/ResponsiveScrollView;
.source "CardProcessingNotActivatedView.java"


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field settings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-class p2, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Component;->inject(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;->presenter:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 39
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 26
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onFinishInflate()V

    .line 27
    sget v0, Lcom/squareup/onboarding/flow/R$id;->instructional_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 28
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/onboarding/flow/R$string;->card_processing_not_activated_text:I

    const-string v3, "support_center"

    .line 29
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/onboarding/flow/R$string;->onboarding_help_url:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/checkout/R$string;->support_center:I

    .line 31
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;->presenter:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
