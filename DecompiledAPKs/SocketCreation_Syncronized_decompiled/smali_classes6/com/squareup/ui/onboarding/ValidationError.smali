.class public Lcom/squareup/ui/onboarding/ValidationError;
.super Ljava/lang/Object;
.source "ValidationError.java"


# instance fields
.field private final childViewId:I

.field private final message:I

.field private final parentViewId:I

.field private final title:I


# direct methods
.method public constructor <init>(III)V
    .locals 1

    const/4 v0, -0x1

    .line 12
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/onboarding/ValidationError;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/squareup/ui/onboarding/ValidationError;->title:I

    .line 17
    iput p2, p0, Lcom/squareup/ui/onboarding/ValidationError;->message:I

    .line 18
    iput p3, p0, Lcom/squareup/ui/onboarding/ValidationError;->parentViewId:I

    .line 19
    iput p4, p0, Lcom/squareup/ui/onboarding/ValidationError;->childViewId:I

    return-void
.end method


# virtual methods
.method public getChildViewId()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/ui/onboarding/ValidationError;->childViewId:I

    return v0
.end method

.method public getMessageId()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/ui/onboarding/ValidationError;->message:I

    return v0
.end method

.method public getParentViewId()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/squareup/ui/onboarding/ValidationError;->parentViewId:I

    return v0
.end method

.method public getTitleId()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/ui/onboarding/ValidationError;->title:I

    return v0
.end method
