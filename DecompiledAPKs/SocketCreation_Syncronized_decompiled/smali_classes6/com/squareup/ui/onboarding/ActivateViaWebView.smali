.class public Lcom/squareup/ui/onboarding/ActivateViaWebView;
.super Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;
.source "ActivateViaWebView.java"


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const-class p2, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Component;->inject(Lcom/squareup/ui/onboarding/ActivateViaWebView;)V

    .line 24
    new-instance p2, Lcom/squareup/caller/ProgressAndFailureView;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressAndFailureView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    return-void
.end method


# virtual methods
.method protected configurePayment(Ljava/lang/CharSequence;)V
    .locals 1

    .line 62
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_activate_on_web:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setTitleText(I)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setMessageText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected configureRegisterWorld(Ljava/lang/CharSequence;)V
    .locals 2

    .line 55
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECKLIST:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V

    .line 56
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_finalize_account_setup:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setTitleText(I)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setMessageText(Ljava/lang/CharSequence;)V

    const/4 p1, -0x1

    .line 58
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setBottomButton(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->presenter:Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->urlHelper:Lcom/squareup/onboarding/ActivationUrlHelper;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/ActivationUrlHelper;->dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->presenter:Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 51
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 28
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onFinishInflate()V

    .line 30
    sget v0, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setTopButton(I)V

    .line 31
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_120:I

    sget v1, Lcom/squareup/ui/onboarding/ActivateViaWebView;->ROTATE_LEFT:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setVector(IF)V

    .line 32
    sget v0, Lcom/squareup/onboarding/flow/R$string;->later:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setBottomButton(I)V

    .line 33
    new-instance v0, Lcom/squareup/ui/onboarding/ActivateViaWebView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivateViaWebView$1;-><init>(Lcom/squareup/ui/onboarding/ActivateViaWebView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setBottomButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 38
    new-instance v0, Lcom/squareup/ui/onboarding/ActivateViaWebView$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivateViaWebView$2;-><init>(Lcom/squareup/ui/onboarding/ActivateViaWebView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivateViaWebView;->setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->presenter:Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->presenter:Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;->urlHelper:Lcom/squareup/onboarding/ActivationUrlHelper;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/ActivationUrlHelper;->takeView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    return-void
.end method
