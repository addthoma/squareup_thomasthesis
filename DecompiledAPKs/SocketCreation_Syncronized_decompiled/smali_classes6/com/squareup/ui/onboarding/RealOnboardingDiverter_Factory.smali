.class public final Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;
.super Ljava/lang/Object;
.source "RealOnboardingDiverter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/RealOnboardingDiverter;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final starterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->starterProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)",
            "Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/settings/server/AccountStatusSettings;Ldagger/Lazy;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/ui/onboarding/RealOnboardingDiverter;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ")",
            "Lcom/squareup/ui/onboarding/RealOnboardingDiverter;"
        }
    .end annotation

    .line 73
    new-instance v8, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter;-><init>(Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/settings/server/AccountStatusSettings;Ldagger/Lazy;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/RealOnboardingDiverter;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/onboarding/OnboardingStarter;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/api/ApiTransactionController;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->newInstance(Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/settings/server/AccountStatusSettings;Ldagger/Lazy;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/ui/onboarding/RealOnboardingDiverter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter_Factory;->get()Lcom/squareup/ui/onboarding/RealOnboardingDiverter;

    move-result-object v0

    return-object v0
.end method
