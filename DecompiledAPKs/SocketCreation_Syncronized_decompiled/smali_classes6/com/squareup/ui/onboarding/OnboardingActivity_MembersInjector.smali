.class public final Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;
.super Ljava/lang/Object;
.source "OnboardingActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/OnboardingActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationResourcesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            ">;"
        }
    .end annotation
.end field

.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final containerActivityDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final contentViewInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featureFlagFeaturesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final focusedActivityScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaButtonDisablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final modelHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->containerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->activationResourcesServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->onboardingFinisherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->modelHolderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->cameraHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->featureFlagFeaturesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationResourcesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingFinisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ModelHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/OnboardingActivity;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 150
    new-instance v24, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v24
.end method

.method public static injectActivationResourcesService(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/onboarding/ActivationResourcesService;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

    return-void
.end method

.method public static injectBrowserLauncher(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/util/BrowserLauncher;)V
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public static injectCameraHelper(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/camerahelper/CameraHelper;)V
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    return-void
.end method

.method public static injectContainer(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/onboarding/OnboardingContainer;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    return-void
.end method

.method public static injectContainerActivityDelegate(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/container/ContainerActivityDelegate;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    return-void
.end method

.method public static injectFeatureFlagFeatures(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->featureFlagFeatures:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectModelHolder(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/onboarding/ModelHolder;)V
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->modelHolder:Lcom/squareup/ui/onboarding/ModelHolder;

    return-void
.end method

.method public static injectOnboardingFinisher(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/onboarding/OnboardingFinisher;)V
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->onboardingFinisher:Lcom/squareup/ui/onboarding/OnboardingFinisher;

    return-void
.end method

.method public static injectSoftInputPresenter(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/SoftInputPresenter;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    return-void
.end method

.method public static injectToastFactory(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/util/ToastFactory;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingActivity;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/OnboardingActivity;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectContainerActivityDelegate(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/container/ContainerActivityDelegate;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SoftInputPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectSoftInputPresenter(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/SoftInputPresenter;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectContainer(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->activationResourcesServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/ActivationResourcesService;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectActivationResourcesService(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/onboarding/ActivationResourcesService;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->onboardingFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingFinisher;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectOnboardingFinisher(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/onboarding/OnboardingFinisher;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->modelHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ModelHolder;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectModelHolder(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/ui/onboarding/ModelHolder;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectBrowserLauncher(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/util/BrowserLauncher;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->cameraHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/camerahelper/CameraHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectCameraHelper(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/camerahelper/CameraHelper;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/ToastFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectToastFactory(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/util/ToastFactory;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->featureFlagFeaturesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectFeatureFlagFeatures(Lcom/squareup/ui/onboarding/OnboardingActivity;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingActivity_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/OnboardingActivity;)V

    return-void
.end method
