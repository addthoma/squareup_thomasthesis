.class public final Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "EmployeeJobsListScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/EmployeeJobsListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final isEmployeeCurrentlyClockedIn:Z

.field public final jobs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Ljava/util/List;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/connectivity/InternetState;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;Z)V"
        }
    .end annotation

    move-object v8, p0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    .line 30
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    move-object/from16 v0, p7

    .line 32
    iput-object v0, v8, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->jobs:Ljava/util/List;

    move/from16 v0, p8

    .line 33
    iput-boolean v0, v8, Lcom/squareup/ui/timecards/EmployeeJobsListScreen$Data;->isEmployeeCurrentlyClockedIn:Z

    return-void
.end method
