.class public Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;
.super Ljava/lang/Object;
.source "TimecardsActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/TimecardsActionBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    }
.end annotation


# instance fields
.field public final actionBarVisible:Z

.field public final primaryButtonCommand:Ljava/lang/Runnable;

.field public final primaryButtonEnabled:Z

.field public final primaryButtonText:Ljava/lang/CharSequence;

.field public final primaryButtonVisible:Z

.field public final titleTextPrimary:Ljava/lang/CharSequence;

.field public final titleTextSecondary:Ljava/lang/CharSequence;

.field public final titleTextVisible:Z

.field public final upButtonCommand:Ljava/lang/Runnable;

.field public final upButtonContentDescription:Ljava/lang/CharSequence;

.field public final upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final upButtonText:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)V
    .locals 1

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$000(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->actionBarVisible:Z

    .line 186
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$100(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 187
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$200(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonContentDescription:Ljava/lang/CharSequence;

    .line 188
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonCommand:Ljava/lang/Runnable;

    .line 189
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$300(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonText:Ljava/lang/CharSequence;

    .line 190
    iget-boolean v0, p1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextVisible:Z

    iput-boolean v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextVisible:Z

    .line 191
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextPrimary:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextPrimary:Ljava/lang/CharSequence;

    .line 192
    iget-object v0, p1, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextSecondary:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextSecondary:Ljava/lang/CharSequence;

    .line 193
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$400(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonVisible:Z

    .line 194
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$500(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonEnabled:Z

    .line 195
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$600(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonText:Ljava/lang/CharSequence;

    .line 196
    invoke-static {p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$700(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/Runnable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonCommand:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Lcom/squareup/ui/timecards/TimecardsActionBarView$1;)V
    .locals 0

    .line 170
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;-><init>(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 2

    .line 200
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;-><init>()V

    .line 201
    iget-boolean v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->actionBarVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$002(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Z)Z

    .line 202
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$102(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 203
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonContentDescription:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$202(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonCommand:Ljava/lang/Runnable;

    iput-object v1, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->upButtonText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$302(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 206
    iget-boolean v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextVisible:Z

    iput-boolean v1, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextVisible:Z

    .line 207
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextPrimary:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextPrimary:Ljava/lang/CharSequence;

    .line 208
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->titleTextSecondary:Ljava/lang/CharSequence;

    iput-object v1, v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextSecondary:Ljava/lang/CharSequence;

    .line 209
    iget-boolean v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$402(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Z)Z

    .line 210
    iget-boolean v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonEnabled:Z

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$502(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Z)Z

    .line 211
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$602(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 212
    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;->primaryButtonCommand:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->access$702(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object v0
.end method
