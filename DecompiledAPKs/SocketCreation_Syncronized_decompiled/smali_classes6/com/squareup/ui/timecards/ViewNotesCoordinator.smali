.class public final Lcom/squareup/ui/timecards/ViewNotesCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ViewNotesCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewNotesCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewNotesCoordinator.kt\ncom/squareup/ui/timecards/ViewNotesCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,68:1\n1642#2,2:69\n*E\n*S KotlinDebug\n*F\n+ 1 ViewNotesCoordinator.kt\ncom/squareup/ui/timecards/ViewNotesCoordinator\n*L\n40#1,2:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/ViewNotesCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
        "flow",
        "Lflow/Flow;",
        "(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "notesContainer",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "getActionBarConfig",
        "Lcom/squareup/noho/NohoActionBar$Config;",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final flow:Lflow/Flow;

.field private notesContainer:Lcom/squareup/noho/NohoLinearLayout;

.field private final scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->flow:Lflow/Flow;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/timecards/ViewNotesCoordinator;)Lflow/Flow;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->flow:Lflow/Flow;

    return-object p0
.end method

.method private final getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 5

    .line 57
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 58
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/ui/timecards/ViewNotesCoordinator$getActionBarConfig$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/ViewNotesCoordinator$getActionBarConfig$1;-><init>(Lcom/squareup/ui/timecards/ViewNotesCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_view_notes:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 63
    sget-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/ui/timecards/R$string;->timecard_view_notes_save_button:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 64
    sget-object v3, Lcom/squareup/ui/timecards/ViewNotesCoordinator$getActionBarConfig$2;->INSTANCE:Lcom/squareup/ui/timecards/ViewNotesCoordinator$getActionBarConfig$2;

    check-cast v3, Lkotlin/jvm/functions/Function0;

    const/4 v4, 0x0

    .line 62
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 8

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 31
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_view_notes_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 34
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_view_notes_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->notesContainer:Lcom/squareup/noho/NohoLinearLayout;

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_9

    check-cast p1, Landroid/view/LayoutInflater;

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->notesContainer:Lcom/squareup/noho/NohoLinearLayout;

    const-string v1, "notesContainer"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoLinearLayout;->removeAllViews()V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->viewNotesScreenData()Lcom/squareup/ui/timecards/ViewNotesScreen$Data;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ViewNotesScreen$Data;->getNotes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 69
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;

    .line 42
    sget v3, Lcom/squareup/ui/timecards/R$layout;->view_notes_entry:I

    iget-object v4, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->notesContainer:Lcom/squareup/noho/NohoLinearLayout;

    if-nez v4, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v4, Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_7

    check-cast v3, Landroid/widget/LinearLayout;

    .line 43
    check-cast v3, Landroid/view/View;

    sget v4, Lcom/squareup/ui/timecards/R$id;->view_notes_entry_job_title:I

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/noho/NohoLabel;

    .line 44
    sget v6, Lcom/squareup/ui/timecards/R$id;->view_notes_entry_note_content:I

    invoke-static {v3, v6}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/squareup/noho/NohoLabel;

    .line 46
    invoke-virtual {v2}, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->getJobTitle()Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-nez v7, :cond_4

    :cond_3
    const/4 v5, 0x1

    :cond_4
    if-eqz v5, :cond_5

    const/16 v5, 0x8

    .line 47
    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoLabel;->setVisibility(I)V

    goto :goto_1

    .line 49
    :cond_5
    invoke-virtual {v2}, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->getJobTitle()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :goto_1
    invoke-virtual {v2}, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->getNote()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v6, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v2, p0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;->notesContainer:Lcom/squareup/noho/NohoLinearLayout;

    if-nez v2, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoLinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 42
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.widget.LinearLayout"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    return-void

    .line 36
    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.LayoutInflater"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
