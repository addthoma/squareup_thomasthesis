.class public final Lcom/squareup/ui/timecards/EditNotesCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditNotesCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/EditNotesCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
        "flow",
        "Lflow/Flow;",
        "(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "notes",
        "Lcom/squareup/noho/NohoMessageText;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "getActionBarConfig",
        "Lcom/squareup/noho/NohoActionBar$Config;",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final flow:Lflow/Flow;

.field private notes:Lcom/squareup/noho/NohoMessageText;

.field private final scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->flow:Lflow/Flow;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/timecards/EditNotesCoordinator;)Lflow/Flow;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getNotes$p(Lcom/squareup/ui/timecards/EditNotesCoordinator;)Lcom/squareup/noho/NohoMessageText;
    .locals 1

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->notes:Lcom/squareup/noho/NohoMessageText;

    if-nez p0, :cond_0

    const-string v0, "notes"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/ui/timecards/EditNotesCoordinator;)Lcom/squareup/ui/timecards/TimecardsScopeRunner;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$setNotes$p(Lcom/squareup/ui/timecards/EditNotesCoordinator;Lcom/squareup/noho/NohoMessageText;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->notes:Lcom/squareup/noho/NohoMessageText;

    return-void
.end method

.method private final getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 5

    .line 34
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 35
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/ui/timecards/EditNotesCoordinator$getActionBarConfig$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/EditNotesCoordinator$getActionBarConfig$1;-><init>(Lcom/squareup/ui/timecards/EditNotesCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_edit_notes:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 40
    sget-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/ui/timecards/R$string;->timecard_add_or_edit_notes_save_button:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 41
    new-instance v3, Lcom/squareup/ui/timecards/EditNotesCoordinator$getActionBarConfig$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/EditNotesCoordinator$getActionBarConfig$2;-><init>(Lcom/squareup/ui/timecards/EditNotesCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    const/4 v4, 0x1

    .line 39
    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 26
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_add_or_edit_notes_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EditNotesCoordinator;->getActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 29
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_add_or_edit_notes_text_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageText;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->notes:Lcom/squareup/noho/NohoMessageText;

    .line 30
    iget-object p1, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->notes:Lcom/squareup/noho/NohoMessageText;

    if-nez p1, :cond_1

    const-string v0, "notes"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/timecards/EditNotesCoordinator;->scopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->editNotesScreenData()Lcom/squareup/ui/timecards/EditNotesScreen$Data;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/EditNotesScreen$Data;->getNotes()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
