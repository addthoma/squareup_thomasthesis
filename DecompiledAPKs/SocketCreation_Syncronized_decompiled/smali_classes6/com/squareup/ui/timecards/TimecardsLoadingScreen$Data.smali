.class public final Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;
.super Ljava/lang/Object;
.source "TimecardsLoadingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/TimecardsLoadingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public employeeHasOpenBreak:Z

.field public employeeHasOpenTimecard:Z

.field public requestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;ZZ)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->requestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    .line 40
    iput-boolean p2, p0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->employeeHasOpenTimecard:Z

    .line 41
    iput-boolean p3, p0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen$Data;->employeeHasOpenBreak:Z

    return-void
.end method
