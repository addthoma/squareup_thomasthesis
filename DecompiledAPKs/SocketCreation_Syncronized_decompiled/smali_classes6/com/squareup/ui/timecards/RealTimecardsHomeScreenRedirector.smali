.class public final Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;
.super Ljava/lang/Object;
.source "RealTimecardsHomeScreenRedirector.kt"

# interfaces
.implements Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;",
        "Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V",
        "shouldNotRedirectForOpenTicketsHomeScreen",
        "",
        "traversal",
        "Lflow/Traversal;",
        "leavingOrderEntryScreen",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-void
.end method


# virtual methods
.method public shouldNotRedirectForOpenTicketsHomeScreen(Lflow/Traversal;Z)Z
    .locals 2

    const-string v0, "traversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "EnterPasscodeToUnlockScreen.INSTANCE"

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    .line 29
    sget-object p2, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->enteringScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 30
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->enteringScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result p2

    if-eqz p2, :cond_1

    :cond_0
    return v1

    .line 36
    :cond_1
    sget-object p2, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 37
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->enteringScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result p2

    if-nez p2, :cond_3

    .line 38
    :cond_2
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->leavingScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 39
    sget-object p2, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->enteringScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p2

    if-eqz p2, :cond_4

    :cond_3
    return v1

    .line 45
    :cond_4
    sget-object p2, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p2

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p2

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isRepeatedLogin()Z

    move-result p2

    if-eqz p2, :cond_5

    return v1

    .line 53
    :cond_5
    sget-object p2, Lcom/squareup/ui/timecards/ClockInOutScreen;->NORMAL:Lcom/squareup/ui/timecards/ClockInOutScreen;

    const-string v0, "ClockInOutScreen.NORMAL"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p2

    if-eqz p2, :cond_6

    return v1

    .line 58
    :cond_6
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->leavingScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 59
    const-class p2, Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-static {p1, p2}, Lcom/squareup/container/Traversals;->enteringScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_7

    return v1

    :cond_7
    const/4 p1, 0x0

    return p1
.end method
