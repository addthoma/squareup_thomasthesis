.class public abstract Lcom/squareup/ui/timecards/TimecardsModule;
.super Ljava/lang/Object;
.source "TimecardsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindTimecardsLauncher(Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;)Lcom/squareup/ui/timecards/api/TimecardsLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindTimecardsRedirector(Lcom/squareup/ui/timecards/RealTimecardsHomeScreenRedirector;)Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
