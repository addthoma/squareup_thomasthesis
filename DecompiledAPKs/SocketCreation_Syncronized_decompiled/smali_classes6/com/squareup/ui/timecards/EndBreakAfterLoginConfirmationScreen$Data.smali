.class public final Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "EndBreakAfterLoginConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    .line 24
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    return-void
.end method
