.class public Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "ClockOutConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/ClockOutConfirmationScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

.field public static final NOT_FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;


# instance fields
.field final isFirstScreenInLayer:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 17
    new-instance v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    .line 19
    new-instance v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->NOT_FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    .line 57
    sget-object v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutConfirmationScreen$y7Aw_rvlLfEbsPuql4DoSdVKasw;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockOutConfirmationScreen$y7Aw_rvlLfEbsPuql4DoSdVKasw;

    .line 58
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    .line 40
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->isFirstScreenInLayer:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;
    .locals 1

    .line 59
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 60
    sget-object p0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    goto :goto_1

    :cond_1
    sget-object p0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->NOT_FIRST_SCREEN_IN_LAYER:Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;

    :goto_1
    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 53
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/timecards/InTimecardsScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 54
    iget-boolean p2, p0, Lcom/squareup/ui/timecards/ClockOutConfirmationScreen;->isFirstScreenInLayer:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    .line 49
    invoke-interface {p1}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->clockOutConfirmationCoordinator()Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 44
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards:I

    return v0
.end method
