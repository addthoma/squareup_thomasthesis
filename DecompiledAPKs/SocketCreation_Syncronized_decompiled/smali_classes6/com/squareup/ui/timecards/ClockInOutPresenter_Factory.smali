.class public final Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;
.super Ljava/lang/Object;
.source "ClockInOutPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/timecards/ClockInOutPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeesServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final featureReleaseHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/TimecardsService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeesServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/TimecardsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 75
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->employeesServiceProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 76
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 77
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->timecardsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->timecardsServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeesServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/TimecardsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 112
    new-instance v17, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/permissions/EmployeesServiceHelper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/employees/TimecardsService;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/ClockInOutPresenter;
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 122
    new-instance v17, Lcom/squareup/ui/timecards/ClockInOutPresenter;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/timecards/ClockInOutPresenter;-><init>(Lcom/squareup/permissions/EmployeesServiceHelper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/employees/TimecardsService;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/analytics/Analytics;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/ui/timecards/ClockInOutPresenter;
    .locals 18

    move-object/from16 v0, p0

    .line 95
    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->employeesServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/permissions/EmployeesServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->timecardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/timecards/Timecards;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->timecardsServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/server/employees/TimecardsService;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lrx/Scheduler;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/receiving/StandardReceiver;

    iget-object v1, v0, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v2 .. v17}, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->newInstance(Lcom/squareup/permissions/EmployeesServiceHelper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/employees/TimecardsService;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/ClockInOutPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter_Factory;->get()Lcom/squareup/ui/timecards/ClockInOutPresenter;

    move-result-object v0

    return-object v0
.end method
