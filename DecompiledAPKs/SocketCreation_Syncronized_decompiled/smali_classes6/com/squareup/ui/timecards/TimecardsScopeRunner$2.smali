.class Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "TimecardsScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakEarlyAfterLoginClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/TimecardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V
    .locals 0

    .line 660
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;->this$0:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 662
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;->this$0:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    .line 663
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner$2;->getAuthorizedEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v2

    sget-object v3, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 662
    :goto_0
    invoke-static {v0, v1, v2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->access$100(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Ljava/lang/String;Z)V

    return-void
.end method
