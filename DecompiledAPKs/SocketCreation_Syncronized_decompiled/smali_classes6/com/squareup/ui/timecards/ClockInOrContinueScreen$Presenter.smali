.class public Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ClockInOrContinueScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/ClockInOrContinueScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/timecards/ClockInOrContinueView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private clockInSubscription:Lrx/Subscription;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final res:Lcom/squareup/util/Res;

.field private final timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/analytics/Analytics;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 43
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->clockInSubscription:Lrx/Subscription;

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 53
    invoke-interface {p5}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->internetState:Lio/reactivex/Observable;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 79
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/connectivity/InternetState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showNoInternet()V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/connectivity/InternetState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 86
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq p1, v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showNoInternet()V

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showClockInPrompt()V

    :goto_0
    return-void
.end method


# virtual methods
.method public clockIn()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onClockInConfirmationClicked(Z)V

    return-void
.end method

.method public finish()Z
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    const/4 v0, 0x1

    return v0
.end method

.method public getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public synthetic lambda$onLoad$2$ClockInOrContinueScreen$Presenter(Lcom/squareup/ui/timecards/ClockInOrContinueView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 78
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$QPbub5gCCLUk4iaN-b5fKTbhGds;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$QPbub5gCCLUk4iaN-b5fKTbhGds;

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$suCITylV0wkeoL0TNjIaZX5zYuo;

    invoke-direct {v1, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$suCITylV0wkeoL0TNjIaZX5zYuo;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    .line 80
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$ClockInOrContinueScreen$Presenter(Lcom/squareup/ui/timecards/ClockInOrContinueView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 84
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$s9q5UYMAjrvaZaBQlTS2Bmg8k84;

    invoke-direct {v1, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$s9q5UYMAjrvaZaBQlTS2Bmg8k84;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->clockInSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 65
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOrContinueView;

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 70
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 69
    invoke-static {v0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 70
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo;->getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$-FcCZk9OsD0ClZ_77pXIPgEt6r8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$-FcCZk9OsD0ClZ_77pXIPgEt6r8;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;)V

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->setActionBarConfig(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showClockInPrompt()V

    .line 76
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$WDbZp1m3GkR4ejR96ikw_dldxlY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$WDbZp1m3GkR4ejR96ikw_dldxlY;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 82
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$PehU-FqkbimfjmIn7g6ppofFGKI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueScreen$Presenter$PehU-FqkbimfjmIn7g6ppofFGKI;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
