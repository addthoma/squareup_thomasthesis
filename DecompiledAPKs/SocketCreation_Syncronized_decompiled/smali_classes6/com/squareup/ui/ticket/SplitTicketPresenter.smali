.class Lcom/squareup/ui/ticket/SplitTicketPresenter;
.super Lmortar/ViewPresenter;
.source "SplitTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/SplitTicketView;",
        ">;"
    }
.end annotation


# static fields
.field private static final EDITING_SPLIT_TICKET_ID_KEY:Ljava/lang/String; = "editingSplitTicketId"


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

.field private final coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private editingSplitTicketId:Ljava/lang/String;

.field private final flow:Lflow/Flow;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private final usePrinters:Z

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ticket/SplitTicketCoordinator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/crm/CustomerManagementSettings;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 72
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    .line 74
    iput-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 75
    iput-object p3, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->res:Lcom/squareup/util/Res;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 77
    iput-object p6, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    .line 78
    iput-object p7, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 79
    iput-object p8, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 80
    iput-object p9, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/squareup/print/PrinterStation$Role;

    .line 83
    sget-object p2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 p3, 0x0

    aput-object p2, p1, p3

    invoke-interface {p4, p1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->usePrinters:Z

    .line 84
    iput-object p10, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/SplitTicketPresenter;)Lcom/squareup/ui/ticket/SplitTicketCoordinator;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/SplitTicketPresenter;)Lflow/Flow;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method private addTicketView(Lcom/squareup/splitticket/SplitState;)V
    .locals 10

    .line 317
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result v0

    xor-int/lit8 v5, v0, 0x1

    .line 318
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->hasItems()Z

    move-result v7

    .line 319
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->isAddCustomerVisible(Lcom/squareup/splitticket/SplitState;)Z

    move-result v8

    .line 320
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->isViewCustomerVisible(Lcom/squareup/splitticket/SplitState;)Z

    move-result v9

    .line 322
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/ticket/SplitTicketView;

    .line 323
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v2

    .line 324
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getName()Ljava/lang/String;

    move-result-object v3

    .line 325
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getViewIndex()I

    move-result v4

    iget-boolean v6, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->usePrinters:Z

    .line 322
    invoke-virtual/range {v1 .. v9}, Lcom/squareup/ui/ticket/SplitTicketView;->addSplitTicket(Ljava/lang/String;Ljava/lang/String;IZZZZZ)Lcom/squareup/ui/ticket/TicketView;

    move-result-object v0

    .line 333
    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$SplitTicketPresenter$jhbdRcw6xNrXdY0FNn5e0Xvq3g8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$SplitTicketPresenter$jhbdRcw6xNrXdY0FNn5e0Xvq3g8;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Lcom/squareup/splitticket/SplitState;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private isAddCustomerVisible(Lcom/squareup/splitticket/SplitState;)Z
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isViewCustomerVisible(Lcom/squareup/splitticket/SplitState;)Z
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static synthetic lambda$I4kSfv15jRlzRlu04ib91fvzxz0(Lcom/squareup/ui/ticket/SplitTicketPresenter;Lcom/squareup/splitticket/SplitState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshTicketView(Lcom/squareup/splitticket/SplitState;)V

    return-void
.end method

.method private refreshAllTicketFooterButtons()V
    .locals 8

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getSelectedEntriesCountAcrossAllTickets()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 257
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/ticket/SplitTicketView;

    .line 258
    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v4}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->splitStates()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/splitticket/SplitState;

    .line 260
    invoke-interface {v5}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 262
    invoke-interface {v5}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/ui/ticket/SplitTicketView;->showDismissButton(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    if-eqz v2, :cond_2

    .line 269
    invoke-interface {v5}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/ui/ticket/SplitTicketView;->showSaveButton(Ljava/lang/String;)V

    goto :goto_1

    .line 274
    :cond_2
    invoke-interface {v5}, Lcom/squareup/splitticket/SplitState;->getSelectedEntriesCount()I

    move-result v6

    if-lez v6, :cond_3

    .line 276
    invoke-interface {v5}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/ui/ticket/SplitTicketView;->showNewTicketButton(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    if-ne v0, v1, :cond_4

    .line 282
    iget-object v6, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/orderentry/R$string;->split_ticket_move_item_singular:I

    .line 283
    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/orderentry/R$string;->split_ticket_move_item_plural:I

    .line 284
    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    const-string v7, "count"

    .line 285
    invoke-virtual {v6, v7, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    .line 286
    invoke-virtual {v6}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v6

    .line 287
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 288
    :goto_2
    invoke-interface {v5}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v6}, Lcom/squareup/ui/ticket/SplitTicketView;->showMoveItemsButton(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method private refreshAllTicketViewsAndButtons()V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->splitStates()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/splitticket/SplitState;

    .line 294
    invoke-direct {p0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshTicketView(Lcom/squareup/splitticket/SplitState;)V

    goto :goto_0

    .line 296
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshAllTicketFooterButtons()V

    return-void
.end method

.method private refreshTicketView(Lcom/squareup/splitticket/SplitState;)V
    .locals 9

    .line 300
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result v0

    xor-int/lit8 v4, v0, 0x1

    .line 301
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->hasItems()Z

    move-result v6

    .line 302
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->isAddCustomerVisible(Lcom/squareup/splitticket/SplitState;)Z

    move-result v7

    .line 303
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->isViewCustomerVisible(Lcom/squareup/splitticket/SplitState;)Z

    move-result v8

    .line 305
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/ticket/SplitTicketView;

    .line 306
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v2

    .line 307
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getName()Ljava/lang/String;

    move-result-object v3

    iget-boolean v5, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->usePrinters:Z

    .line 305
    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/ticket/SplitTicketView;->refreshTicketView(Ljava/lang/String;Ljava/lang/String;ZZZZZ)V

    return-void
.end method

.method private removeTicketViewOrExit(Ljava/lang/String;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 353
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/SplitTicketView;->removeTicket(Ljava/lang/String;)V

    goto :goto_0

    .line 356
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method


# virtual methods
.method getAdditionalTaxAmount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getAdditionalTaxAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method getCartDiningOption(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getCartDiningOptionText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getCartEntryViewModelFactory()Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    return-object v0
.end method

.method getCartLevelCashDiscount(Ljava/lang/String;I)Lcom/squareup/checkout/Discount;
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/Discount;

    return-object p1
.end method

.method getCartLevelCashDiscountCount(Ljava/lang/String;)I
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getCartAmountDiscounts()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    return p1
.end method

.method getCompTotal(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getCompAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method getEditingSplitTicketId()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->editingSplitTicketId:Ljava/lang/String;

    return-object v0
.end method

.method getItem(Ljava/lang/String;I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getItems()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    return-object p1
.end method

.method getItemCount(Ljava/lang/String;)I
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getItems()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    return p1
.end method

.method getNegativePerItemDiscountsAmount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 6

    .line 442
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/splitticket/SplitState;->getNegativePerItemDiscountsAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 443
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0

    .line 445
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/protos/common/Money;->newBuilder()Lcom/squareup/protos/common/Money$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 446
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getCompTotal(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p1

    .line 447
    invoke-virtual {p1}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method getOrderTotal(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getTotalAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method getTaxTypeLabelId(Ljava/lang/String;)I
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getTaxTypeId()I

    move-result p1

    return p1
.end method

.method getTicketGroup(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup;
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 391
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method getTicketName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getTicketNote(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 386
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getNote()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getTicketTemplate(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate;
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 396
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem(Ljava/lang/String;)Z
    .locals 1

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z

    move-result p1

    return p1
.end method

.method isEntrySelected(Ljava/lang/String;IZ)Z
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 371
    invoke-interface {p1, p2}, Lcom/squareup/splitticket/SplitState;->isDiscountSelected(I)Z

    move-result p1

    return p1

    .line 373
    :cond_0
    invoke-interface {p1, p2}, Lcom/squareup/splitticket/SplitState;->isItemSelected(I)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$addTicketView$1$SplitTicketPresenter(Lcom/squareup/splitticket/SplitState;)Lrx/Subscription;
    .locals 2

    .line 334
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$SplitTicketPresenter$3zs_pSHL28ob5Vk_ySswJTSyrfc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$SplitTicketPresenter$3zs_pSHL28ob5Vk_ySswJTSyrfc;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Lcom/squareup/splitticket/SplitState;)V

    .line 335
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$SplitTicketPresenter(Lcom/squareup/splitticket/SplitState;Lkotlin/Unit;)V
    .locals 0

    .line 335
    iget-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshTicketView(Lcom/squareup/splitticket/SplitState;)V

    return-void
.end method

.method onAddCustomerClicked(Ljava/lang/String;)V
    .locals 3

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 141
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketView;->closeDropDown(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/ticket/SplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/SplitTicketScreen;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    .line 143
    invoke-virtual {v2, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;

    move-result-object p1

    .line 142
    invoke-static {v1, p1}, Lcom/squareup/ui/crm/flow/CrmScope;->forAddingCustomerInSplitTicketScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onCancelClicked()Z
    .locals 4

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getSavedTicketsCount()I

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/ui/ticket/analytics/SplitTicketCancelEvent;

    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    .line 161
    invoke-virtual {v3}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getSplitTicketsCount()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-direct {v2, v0, v3}, Lcom/squareup/ui/ticket/analytics/SplitTicketCancelEvent;-><init>(II)V

    .line 160
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    const/4 v0, 0x1

    return v0
.end method

.method onCreateNewTicketClicked()V
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->createNewSplitTicket()Lcom/squareup/splitticket/SplitState;

    move-result-object v0

    .line 231
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->addTicketView(Lcom/squareup/splitticket/SplitState;)V

    .line 233
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshAllTicketViewsAndButtons()V

    .line 234
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-interface {v0}, Lcom/squareup/splitticket/SplitState;->getViewIndex()I

    move-result v2

    invoke-interface {v0}, Lcom/squareup/splitticket/SplitState;->getTicketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/ticket/SplitTicketView;->scrollTicketIntoView(ILjava/lang/String;)V

    return-void
.end method

.method onDismissClicked(Ljava/lang/String;)V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->removeOneTicket(Ljava/lang/String;)Z

    move-result v0

    .line 197
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->removeTicketViewOrExit(Ljava/lang/String;Z)V

    return-void
.end method

.method onEditTicketClicked(Ljava/lang/String;)V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Should not be allowed to edit a ticket after printing it!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_EDIT:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 204
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketView;->closeDropDown(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->setEditingSplitTicketId(Ljava/lang/String;)V

    .line 206
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/ticket/EditSplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/EditSplitTicketScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 92
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 93
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->onAsyncStateChange()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$SplitTicketPresenter$I4kSfv15jRlzRlu04ib91fvzxz0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$SplitTicketPresenter$I4kSfv15jRlzRlu04ib91fvzxz0;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method onEntryClicked(Ljava/lang/String;IZ)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 214
    invoke-interface {p1, p2}, Lcom/squareup/splitticket/SplitState;->toggleDiscountSelection(I)Z

    goto :goto_0

    .line 216
    :cond_0
    invoke-interface {p1, p2}, Lcom/squareup/splitticket/SplitState;->toggleItemSelection(I)Z

    .line 219
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshAllTicketFooterButtons()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 98
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "editingSplitTicketId"

    .line 100
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->editingSplitTicketId:Ljava/lang/String;

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/SplitTicketView;

    .line 104
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketView;->removeAllTicketViews()V

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->splitStatesByViewIndex()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/splitticket/SplitState;

    .line 106
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->addTicketView(Lcom/squareup/splitticket/SplitState;)V

    goto :goto_0

    .line 108
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshAllTicketFooterButtons()V

    .line 110
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$psvAfrbOkXVlqycOyK0TpM3ssNE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$psvAfrbOkXVlqycOyK0TpM3ssNE;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;)V

    .line 111
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->split_ticket_ticket_name_action_bar:I

    .line 112
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    .line 113
    invoke-virtual {v2}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getParentTicketBaseName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ticket_name"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 112
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->split_ticket_save_all:I

    .line 115
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$288xFNGrAE0smTkXyPOmkMogDpE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$288xFNGrAE0smTkXyPOmkMogDpE;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;)V

    .line 116
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 118
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->usePrinters:Z

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->split_ticket_print_all:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$q20aksYhkLW58KIy7dP9PBiWOww;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$q20aksYhkLW58KIy7dP9PBiWOww;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;)V

    .line 120
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method onMoveSelectedItemsToTicketClicked(Ljava/lang/String;)V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object v0

    .line 225
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshAllTicketViewsAndButtons()V

    .line 226
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-interface {v0}, Lcom/squareup/splitticket/SplitState;->getViewIndex()I

    move-result v0

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/ticket/SplitTicketView;->scrollTicketIntoView(ILjava/lang/String;)V

    return-void
.end method

.method onPrintAllClicked()V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->printAllTickets()V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method onPrintClicked(Ljava/lang/String;)V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->printOneTickets(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketView;->closeDropDown(Ljava/lang/String;)V

    .line 186
    invoke-direct {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshAllTicketViewsAndButtons()V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 127
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->editingSplitTicketId:Ljava/lang/String;

    const-string v1, "editingSplitTicketId"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onSaveAllClicked()V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getSavedTicketsCount()I

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->saveAllTickets()I

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method onSaveClicked(Ljava/lang/String;)V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->saveOneTicket(Ljava/lang/String;Z)Z

    move-result v0

    .line 192
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->removeTicketViewOrExit(Ljava/lang/String;Z)V

    return-void
.end method

.method onViewCustomerClicked(Ljava/lang/String;)V
    .locals 3

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 148
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/SplitTicketView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketView;->closeDropDown(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method setEditingSplitTicketId(Ljava/lang/String;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->editingSplitTicketId:Ljava/lang/String;

    return-void
.end method

.method shouldShowComps(Ljava/lang/String;)Z
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->shouldShowComps()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method shouldShowPerItemDiscounts(Ljava/lang/String;)Z
    .locals 2

    .line 408
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 409
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/splitticket/SplitState;->shouldShowPerItemDiscounts(Z)Z

    move-result p1

    return p1
.end method

.method shouldShowTaxes(Ljava/lang/String;)Z
    .locals 1

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->shouldShowTaxRow()Z

    move-result p1

    return p1
.end method

.method shouldShowTicketNote(Ljava/lang/String;)Z
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getNote()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method shouldShowTotal(Ljava/lang/String;)Z
    .locals 1

    .line 425
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    .line 426
    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->hasItems()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->hasCartAmountDiscounts()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method ticketMutable(Ljava/lang/String;)Z
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/splitticket/SplitState;->getHasBeenSaved()Z

    move-result p1

    return p1
.end method

.method updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 244
    iget-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter;->coordinator:Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->refreshTicketView(Lcom/squareup/splitticket/SplitState;)V

    :cond_0
    return-void
.end method
