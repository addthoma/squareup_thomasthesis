.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideTicketListPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final configProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketListListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketSortSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ticketsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    .line 71
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->configProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketSelectionProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p6, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketScopeRunnerProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p7, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketSortSettingProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p8, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketListListenerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p9, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p10, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p11, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->cogsProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p12, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p13, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p14, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p15, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;"
        }
    .end annotation

    .line 104
    new-instance v16, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static provideTicketListPresenter(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljava/lang/Object;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Ljava/lang/Object;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljava/lang/Object;",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/ui/ticket/TicketListPresenter;"
        }
    .end annotation

    .line 113
    move-object/from16 v1, p1

    check-cast v1, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-object/from16 v7, p7

    check-cast v7, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    move-object v0, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-virtual/range {v0 .. v14}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideTicketListPresenter(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListPresenter;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketListPresenter;
    .locals 15

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->configProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/opentickets/PredefinedTickets;

    iget-object v3, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v4, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/ticket/TicketSelection;

    iget-object v5, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/ticket/TicketScopeRunner;

    iget-object v6, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketSortSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/settings/LocalSetting;

    iget-object v7, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->ticketListListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v9, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v10, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v10}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/cogs/Cogs;

    iget-object v11, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/util/Device;

    iget-object v12, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v12}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/text/Formatter;

    iget-object v13, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v13}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v14, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v14}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/squareup/util/Res;

    invoke-static/range {v0 .. v14}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->provideTicketListPresenter(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljava/lang/Object;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Ljava/lang/Object;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketListPresenterFactory;->get()Lcom/squareup/ui/ticket/TicketListPresenter;

    move-result-object v0

    return-object v0
.end method
