.class public Lcom/squareup/ui/ticket/MasterDetailTicketScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "MasterDetailTicketScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/ui/ticket/TicketActionScope$Component;

    .line 74
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketActionScope$Component;

    .line 75
    check-cast p2, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    .line 76
    new-instance v0, Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketScreen;)V

    .line 77
    invoke-interface {p1, v0}, Lcom/squareup/ui/ticket/TicketActionScope$Component;->masterDetailTicket(Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Module;)Lcom/squareup/ui/ticket/MasterDetailTicketScreen$Component;

    move-result-object p1

    return-object p1
.end method
