.class Lcom/squareup/ui/ticket/TicketView$5;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketView;->showSaveButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketView;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketView$5;->this$0:Lcom/squareup/ui/ticket/TicketView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketView$5;->this$0:Lcom/squareup/ui/ticket/TicketView;

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketView;->presenter:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketView$5;->this$0:Lcom/squareup/ui/ticket/TicketView;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketView;->access$100(Lcom/squareup/ui/ticket/TicketView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->onSaveClicked(Ljava/lang/String;)V

    return-void
.end method
