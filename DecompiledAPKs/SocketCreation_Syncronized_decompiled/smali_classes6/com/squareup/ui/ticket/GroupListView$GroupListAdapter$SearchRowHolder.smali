.class Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;
.super Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchRowHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

.field private final title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;->this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    .line 115
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)V

    .line 116
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->open_tickets_search_tickets:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;->title:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V
    .locals 0

    .line 121
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 110
    check-cast p1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    return-void
.end method

.method protected onRowClicked(Landroid/view/View;)V
    .locals 0

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$SearchRowHolder;->this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->access$100(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)Lcom/squareup/ui/ticket/GroupListPresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/GroupListPresenter;->selectSearchSection()V

    return-void
.end method
