.class public Lcom/squareup/ui/ticket/TicketTransferEmployeesView;
.super Landroid/widget/LinearLayout;
.source "TicketTransferEmployeesView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;
    }
.end annotation


# instance fields
.field private final gutterHalf:I

.field presenter:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Landroid/widget/ProgressBar;

.field private recyclerAdapter:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private searchBar:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 127
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 128
    const-class p2, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Component;->inject(Lcom/squareup/ui/ticket/TicketTransferEmployeesView;)V

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->gutterHalf:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketTransferEmployeesView;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->gutterHalf:I

    return p0
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->presenter:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->presenter:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 151
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 133
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 134
    sget v0, Lcom/squareup/orderentry/R$id;->tickets_transfer_employee_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 135
    sget v0, Lcom/squareup/orderentry/R$id;->tickets_transfer_employee_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->progress:Landroid/widget/ProgressBar;

    .line 136
    sget v0, Lcom/squareup/orderentry/R$id;->tickets_transfer_employee_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$1;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 143
    new-instance v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerAdapter:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerAdapter:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->presenter:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public refreshList()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerAdapter:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method scrollListToTop()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    return-void
.end method

.method showList()V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->progress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    return-void
.end method

.method updateEmployees(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;)V"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->recyclerAdapter:Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->setList(Ljava/util/List;)V

    return-void
.end method
