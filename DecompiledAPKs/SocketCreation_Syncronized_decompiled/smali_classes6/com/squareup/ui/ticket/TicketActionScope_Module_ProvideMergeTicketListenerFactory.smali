.class public final Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory;
.super Ljava/lang/Object;
.source "TicketActionScope_Module_ProvideMergeTicketListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory$InstanceHolder;->access$000()Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideMergeTicketListener()Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/ui/ticket/TicketActionScope$Module;->provideMergeTicketListener()Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory;->provideMergeTicketListener()Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideMergeTicketListenerFactory;->get()Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    move-result-object v0

    return-object v0
.end method
