.class public final Lcom/squareup/ui/ticket/MoveTicketScreen;
.super Lcom/squareup/ui/ticket/InTicketScope;
.source "MoveTicketScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/ticket/TicketScreen;
.implements Lcom/squareup/ui/ticket/TicketModeScreen;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/ticket/MoveTicketScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MoveTicketScreen$Component;,
        Lcom/squareup/ui/ticket/MoveTicketScreen$ComponentFactory;,
        Lcom/squareup/ui/ticket/MoveTicketScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/MoveTicketScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

.field private final ticketsToMove:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 211
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketScreen$OiQlM9ioL1J25FW1elUQzXIWEDk;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketScreen$OiQlM9ioL1J25FW1elUQzXIWEDk;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/MoveTicketScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketScope;-><init>()V

    if-eqz p1, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen;->ticketsToMove:Ljava/util/List;

    .line 69
    sget-object p1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;->MOVE_TICKET:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MoveTicketScreen;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/MoveTicketScreen;)Ljava/util/List;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen;->ticketsToMove:Ljava/util/List;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/MoveTicketScreen;
    .locals 2

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 213
    const-class v1, Lcom/squareup/ui/ticket/MoveTicketScreen;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 214
    new-instance p0, Lcom/squareup/ui/ticket/MoveTicketScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketScreen;-><init>(Ljava/util/List;)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 207
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 208
    iget-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen;->ticketsToMove:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    return-void
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 3

    .line 77
    const-class v0, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;

    .line 78
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v1

    .line 79
    invoke-interface {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;->ticketSelectionSession()Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 80
    invoke-interface {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;->ticketActionSession()Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 81
    invoke-interface {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;->ticketsLoader()Lcom/squareup/ui/ticket/TicketsLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 218
    sget v0, Lcom/squareup/orderentry/R$layout;->move_ticket_view:I

    return v0
.end method

.method public setTicketMode(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen;->ticketMode:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    return-void
.end method
