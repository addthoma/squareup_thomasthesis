.class public final Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;
.super Ljava/lang/Object;
.source "TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketActionScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final voidReasonsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->voidReasonsCacheProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->ticketActionScopeRunnerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->openTicketSettingsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;"
        }
    .end annotation

    .line 83
    new-instance v12, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;
    .locals 13

    .line 91
    new-instance v12, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;
    .locals 12

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->voidReasonsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->ticketActionScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->openTicketSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketBulkVoidScreen_TicketBulkVoidPresenter_Factory;->get()Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;

    move-result-object v0

    return-object v0
.end method
