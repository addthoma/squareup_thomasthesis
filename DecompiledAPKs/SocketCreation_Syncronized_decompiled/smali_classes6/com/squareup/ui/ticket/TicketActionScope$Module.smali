.class public Lcom/squareup/ui/ticket/TicketActionScope$Module;
.super Ljava/lang/Object;
.source "TicketActionScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketActionScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideInEditModeBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 88
    const-class v0, Ljava/lang/Boolean;

    const-string v1, "ticket-action-path-in-edit-mode"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideMergeTicketListener()Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 74
    new-instance v0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;-><init>()V

    return-object v0
.end method

.method static provideSelectedTicketsInfoBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/ui/ticket/TicketActionScope$Module$1;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/TicketActionScope$Module$1;-><init>()V

    .line 94
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScope$Module$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "ticket-action-path-selected-tickets-info"

    .line 92
    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideTicketListListener()Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 84
    new-instance v0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;-><init>()V

    return-object v0
.end method

.method static provideTicketSort(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation

    .line 79
    new-instance v0, Lcom/squareup/settings/EnumLocalSetting;

    const-class v1, Lcom/squareup/tickets/TicketSort;

    const-string v2, "ticket-action-path-sort-style"

    invoke-direct {v0, p0, v2, v1}, Lcom/squareup/settings/EnumLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method
