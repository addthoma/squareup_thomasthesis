.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindTemplate(Lcom/squareup/tickets/TicketSort;Lcom/squareup/ui/ticket/TicketInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;

.field final synthetic val$templateInfo:Lcom/squareup/ui/ticket/TicketInfo;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;->this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;

    iput-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;->val$templateInfo:Lcom/squareup/ui/ticket/TicketInfo;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 262
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;->this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;->this$2:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;

    invoke-static {v0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->access$300(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;)Lcom/squareup/marin/widgets/MarinCheckBox;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;->val$templateInfo:Lcom/squareup/ui/ticket/TicketInfo;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/ticket/BaseTicketListView;->onTemplateClicked(Landroid/widget/CompoundButton;Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method
