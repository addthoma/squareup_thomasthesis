.class final enum Lcom/squareup/ui/ticket/GroupListView$GroupRow;
.super Ljava/lang/Enum;
.source "GroupListView.java"

# interfaces
.implements Lcom/squareup/ui/Ranger$RowType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "GroupRow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
        ">;",
        "Lcom/squareup/ui/Ranger$RowType<",
        "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/GroupListView$GroupRow;

.field public static final enum ALL_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

.field public static final enum CUSTOM_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

.field public static final enum GROUP_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

.field public static final enum SEARCH_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;


# instance fields
.field private final holderType:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 48
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->SEARCH_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v2, 0x0

    const-string v3, "SEARCH_ROW"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/ticket/GroupListView$GroupRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->SEARCH_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    .line 49
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->ALL_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v3, 0x1

    const-string v4, "ALL_TICKETS_ROW"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/ticket/GroupListView$GroupRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->ALL_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    .line 50
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->CUSTOM_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v4, 0x2

    const-string v5, "CUSTOM_TICKETS_ROW"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/ticket/GroupListView$GroupRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->CUSTOM_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    .line 51
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->GROUP_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v5, 0x3

    const-string v6, "GROUP_ROW"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/ticket/GroupListView$GroupRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->GROUP_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    .line 47
    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->SEARCH_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->ALL_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->CUSTOM_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->GROUP_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->$VALUES:[Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/ui/ticket/GroupListView$GroupViewHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
            ")V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->holderType:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/GroupListView$GroupRow;
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/GroupListView$GroupRow;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->$VALUES:[Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/GroupListView$GroupRow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    return-object v0
.end method


# virtual methods
.method public getHolderType()Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->holderType:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    return-object v0
.end method

.method public bridge synthetic getHolderType()Ljava/lang/Enum;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->getHolderType()Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    move-result-object v0

    return-object v0
.end method
