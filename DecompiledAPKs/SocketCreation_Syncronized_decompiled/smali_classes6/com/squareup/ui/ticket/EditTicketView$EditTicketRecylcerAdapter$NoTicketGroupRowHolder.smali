.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoTicketGroupRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
        "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field final checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private final groupNameNone:Ljava/lang/String;

.field final synthetic this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 199
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    .line 200
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_detail_checkable_ticket_group:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->checkable_ticket_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 202
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->predefined_tickets_none:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->groupNameNone:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V
    .locals 3

    .line 207
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketState;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object p1

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 208
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    if-eqz p1, :cond_1

    move v2, p3

    goto :goto_1

    :cond_1
    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$200(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)I

    move-result v2

    :goto_1
    invoke-static {v1, v2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$202(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;I)I

    .line 209
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    iget-object v2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->groupNameNone:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v1, p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setChecked(Z)V

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    new-instance v1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder$1;

    invoke-direct {v1, p0, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder$1;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;I)V

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p2, 0x0

    :goto_2
    invoke-virtual {p0, p2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->setExtraBottomMargin(Z)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V

    return-void
.end method

.method setExtraBottomMargin(Z)V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz p1, :cond_0

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->itemView:Landroid/view/View;

    .line 226
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 227
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 229
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
