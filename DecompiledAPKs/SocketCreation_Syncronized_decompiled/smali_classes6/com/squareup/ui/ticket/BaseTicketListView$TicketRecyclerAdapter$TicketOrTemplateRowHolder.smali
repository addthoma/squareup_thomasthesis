.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketOrTemplateRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field private amountView:Landroid/widget/TextView;

.field private checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private employeeBlock:Landroid/view/View;

.field private employeeName:Landroid/widget/TextView;

.field private phoneTicketRow:Lcom/squareup/noho/NohoRow;

.field private tabletTicketOrTemplateNameView:Landroid/widget/TextView;

.field final synthetic this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

.field private timeView:Landroid/widget/TextView;

.field private timeViewWithEmployees:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 124
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 125
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_list_ticket_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 126
    invoke-direct {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindViews()V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;)Lcom/squareup/marin/widgets/MarinCheckBox;
    .locals 0

    .line 113
    iget-object p0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-object p0
.end method

.method private adjustParams(I)V
    .locals 4

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 274
    iget-object v2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/tickets/TicketRowCursorList;->hasSearch()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NEW_TICKET_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 275
    invoke-static {v2, p1, v3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;ILcom/squareup/ui/ticket/BaseTicketListView$TicketRow;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 280
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {v2}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result v2

    goto :goto_0

    .line 281
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->SORT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-static {v2, p1, v3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;ILcom/squareup/ui/ticket/BaseTicketListView$TicketRow;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 283
    iget-object v2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {v2}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$700(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result v2

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    .line 287
    :goto_0
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 290
    iget-object v2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$800(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/ui/Ranger;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/Ranger;->count()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_4

    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result v1

    :cond_4
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private bindTemplate(Lcom/squareup/tickets/TicketSort;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 2

    .line 246
    iget-object v0, p2, Lcom/squareup/ui/ticket/TicketInfo;->name:Ljava/lang/String;

    .line 248
    sget-object v1, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    if-ne p1, v1, :cond_0

    .line 249
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 251
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->tabletTicketOrTemplateNameView:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->isTicketCheckboxSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinCheckBox;->setChecked(Z)V

    .line 254
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinCheckBox;->setVisibility(I)V

    .line 256
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->amountView:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeView:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeBlock:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$2;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;Lcom/squareup/ui/ticket/TicketInfo;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private bindTicket(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Lcom/squareup/tickets/TicketSort;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindTicketPhone(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Lcom/squareup/tickets/TicketSort;)V

    goto :goto_0

    .line 170
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindTicketTablet(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Lcom/squareup/tickets/TicketSort;)V

    .line 172
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    new-instance p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder$1;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;Lcom/squareup/ui/ticket/TicketInfo;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private bindTicketPhone(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Lcom/squareup/tickets/TicketSort;)V
    .locals 3

    .line 180
    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getName()Ljava/lang/String;

    move-result-object v0

    .line 182
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    invoke-virtual {p2}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_2

    const/4 v1, 0x3

    if-eq p2, v1, :cond_1

    const/4 v1, 0x4

    if-eq p2, v1, :cond_0

    .line 185
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 186
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getTimeUpdated()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTimeSinceString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 195
    :cond_0
    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getEmployeeName()Lcom/squareup/resources/TextModel;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v1, v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/BaseTicketListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object p2

    goto :goto_0

    .line 192
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getTimeUpdated()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTimeSinceString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object p2

    goto :goto_0

    .line 189
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getPriceAmount()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getPriceString(J)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object p2

    .line 199
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->phoneTicketRow:Lcom/squareup/noho/NohoRow;

    invoke-static {v1, v0, p2}, Lcom/squareup/ui/ticket/NohoRowsKt;->setLabelAndValueForTicketRow(Lcom/squareup/noho/NohoRow;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 201
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/BaseTicketListView;->onBindTicketRowPhone(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;)V

    return-void
.end method

.method private bindTicketTablet(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Lcom/squareup/tickets/TicketSort;)V
    .locals 5

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getPriceAmount()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getPriceString(J)Ljava/lang/String;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v1, v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v1, v1, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getTimeUpdated()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTimeSinceString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getEmployeeName()Lcom/squareup/resources/TextModel;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v3, v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/BaseTicketListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 208
    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getName()Ljava/lang/String;

    move-result-object v3

    .line 210
    sget-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    invoke-virtual {p2}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result p2

    aget p2, v4, p2

    const/4 v4, 0x2

    if-eq p2, v4, :cond_2

    const/4 v4, 0x3

    if-eq p2, v4, :cond_1

    const/4 v4, 0x4

    if-eq p2, v4, :cond_0

    .line 213
    invoke-direct {p0, v3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v3

    goto :goto_0

    .line 222
    :cond_0
    invoke-direct {p0, v2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v2

    goto :goto_0

    .line 219
    :cond_1
    invoke-direct {p0, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v1

    goto :goto_0

    .line 216
    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 226
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->amountView:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->tabletTicketOrTemplateNameView:Landroid/widget/TextView;

    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$400(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Z

    move-result p2

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz p2, :cond_3

    .line 230
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeBlock:Landroid/view/View;

    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeView:Landroid/widget/TextView;

    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeView:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeViewWithEmployees:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeName:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 236
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeBlock:Landroid/view/View;

    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeView:Landroid/widget/TextView;

    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 238
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeView:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeName:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    :goto_1
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->onBindTicketRowTablet(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Landroid/widget/CompoundButton;)V

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_check_box:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_row_name:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->tabletTicketOrTemplateNameView:Landroid/widget/TextView;

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_row_amount:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->amountView:Landroid/widget/TextView;

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_row_time:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeView:Landroid/widget/TextView;

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_row_block_employee:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeBlock:Landroid/view/View;

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_row_time_with_employee:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->timeViewWithEmployees:Landroid/widget/TextView;

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_row_employee:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->employeeName:Landroid/widget/TextView;

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->phone_ticket_row:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->phoneTicketRow:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method private spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .locals 2

    .line 268
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v1}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketRowCursorList;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    .line 141
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    if-ne p1, v1, :cond_1

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/tickets/TicketRowCursorList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    .line 143
    invoke-static {p1}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromRow(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object p2

    .line 144
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindTicket(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Lcom/squareup/tickets/TicketSort;Lcom/squareup/ui/ticket/TicketInfo;)V

    goto :goto_0

    .line 146
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 147
    invoke-static {p1}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromTemplate(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object p1

    .line 148
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindTemplate(Lcom/squareup/tickets/TicketSort;Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 151
    :goto_0
    invoke-direct {p0, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->adjustParams(I)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 113
    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$TicketOrTemplateRowHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V

    return-void
.end method
