.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->bind()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;I)V
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;

    iput p2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;->val$position:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 207
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;->val$position:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->onEntryClicked(Ljava/lang/String;IZ)V

    .line 208
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;->access$400(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ItemViewHolder;)Lcom/squareup/marin/widgets/MarinCheckBox;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->toggle()V

    return-void
.end method
