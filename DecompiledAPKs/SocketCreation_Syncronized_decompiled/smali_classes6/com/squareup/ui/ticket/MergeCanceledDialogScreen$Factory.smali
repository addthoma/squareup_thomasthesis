.class public Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Factory;
.super Ljava/lang/Object;
.source "MergeCanceledDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getTitle(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)I
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->BULK_MERGE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    if-ne p1, v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 p1, 0x1

    if-le p2, p1, :cond_1

    .line 110
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_move_canceled_title_many:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_move_canceled_title_one:I

    :goto_0
    return p1

    .line 108
    :cond_2
    :goto_1
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_merge_canceled_title:I

    return p1
.end method

.method static synthetic lambda$create$0(Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 93
    invoke-virtual {p0}, Lflow/Flow;->goBack()Z

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 86
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    .line 87
    invoke-static {p1}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object v1

    .line 88
    invoke-static {v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->access$000(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    move-result-object v2

    .line 89
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_merge_canceled_confirmation:I

    new-instance v4, Lcom/squareup/ui/ticket/-$$Lambda$MergeCanceledDialogScreen$Factory$rx22i9EaX3FNSqlHBgDkd_AQDYc;

    invoke-direct {v4, v1}, Lcom/squareup/ui/ticket/-$$Lambda$MergeCanceledDialogScreen$Factory$rx22i9EaX3FNSqlHBgDkd_AQDYc;-><init>(Lflow/Flow;)V

    .line 90
    invoke-virtual {v3, p1, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 95
    invoke-static {v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->access$100(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;)I

    move-result v0

    invoke-direct {p0, v2, v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Factory;->getTitle(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_merge_canceled_message:I

    .line 96
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 100
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$DismissalNavigator;-><init>(Lflow/Flow;Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    const/4 v0, 0x0

    .line 102
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 103
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
