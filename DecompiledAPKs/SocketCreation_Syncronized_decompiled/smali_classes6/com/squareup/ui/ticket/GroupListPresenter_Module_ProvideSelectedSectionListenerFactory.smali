.class public final Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;
.super Ljava/lang/Object;
.source "GroupListPresenter_Module_ProvideSelectedSectionListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final ticketGroupSelectedSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;->ticketGroupSelectedSectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSelectedSectionListener(Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/ui/ticket/GroupListPresenter$Module;->provideSelectedSectionListener(Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;->ticketGroupSelectedSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;->provideSelectedSectionListener(Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter_Module_ProvideSelectedSectionListenerFactory;->get()Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    move-result-object v0

    return-object v0
.end method
