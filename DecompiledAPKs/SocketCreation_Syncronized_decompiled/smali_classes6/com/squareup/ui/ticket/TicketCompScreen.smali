.class public final Lcom/squareup/ui/ticket/TicketCompScreen;
.super Lcom/squareup/ui/ticket/InTicketScope;
.source "TicketCompScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/ticket/TicketScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketCompScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketCompScreen$Component;,
        Lcom/squareup/ui/ticket/TicketCompScreen$Module;,
        Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketCompScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final authorizedEmployeeToken:Ljava/lang/String;

.field private final predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

.field private final ticketName:Ljava/lang/String;

.field private final ticketNote:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 153
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketCompScreen$pSHL86ab9zGoTz0kYCQnKxONOBQ;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketCompScreen$pSHL86ab9zGoTz0kYCQnKxONOBQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketCompScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketScope;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->ticketName:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->ticketNote:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketCompScreen;)Ljava/lang/String;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/TicketCompScreen;)Ljava/lang/String;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->ticketName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/TicketCompScreen;)Ljava/lang/String;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->ticketNote:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/TicketCompScreen;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/TicketCompScreen;
    .locals 4

    .line 154
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 156
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 157
    invoke-virtual {p0}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 158
    new-instance v3, Lcom/squareup/ui/ticket/TicketCompScreen;

    invoke-direct {v3, v0, v1, v2, p0}, Lcom/squareup/ui/ticket/TicketCompScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    return-object v3
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 146
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 147
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->ticketName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->ticketNote:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketCompScreen;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_TICKET_COMP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 162
    sget v0, Lcom/squareup/configure/item/R$layout;->void_comp_view:I

    return v0
.end method
