.class synthetic Lcom/squareup/ui/ticket/NewTicketPresenter$1;
.super Ljava/lang/Object;
.source "NewTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

.field static final synthetic $SwitchMap$com$squareup$ui$ticket$NewTicketScreen$DisplayMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 151
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->values()[Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    sget-object v2, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->CREATE_NEW_EMPTY_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    sget-object v3, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 111
    :catch_1
    invoke-static {}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->values()[Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$NewTicketScreen$DisplayMode:[I

    :try_start_2
    sget-object v2, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$NewTicketScreen$DisplayMode:[I

    sget-object v3, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$NewTicketScreen$DisplayMode:[I

    sget-object v2, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$NewTicketScreen$DisplayMode:[I

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    return-void
.end method
