.class public Lcom/squareup/ui/ticket/NewTicketPresenter;
.super Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;
.source "NewTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter<",
        "Lcom/squareup/ui/ticket/NewTicketView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SEARCH_TEXT_KEY:Ljava/lang/String; = "newTicketScreenSearchText"


# instance fields
.field private final availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

.field private displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

.field private final filteredTicketTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private hasLoadedData:Z

.field private final predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

.field private final res:Lcom/squareup/util/Res;

.field private searchText:Ljava/lang/String;

.field private selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

.field private final ticketGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/util/Res;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 69
    invoke-direct {p0, p1, p5}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lflow/Flow;)V

    .line 57
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketGroups:Ljava/util/List;

    .line 58
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketTemplates:Ljava/util/List;

    .line 59
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->filteredTicketTemplates:Ljava/util/List;

    const-string p1, ""

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    const/4 p1, 0x0

    .line 64
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->hasLoadedData:Z

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->res:Lcom/squareup/util/Res;

    .line 72
    iput-object p4, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

    return-void
.end method

.method private buildActionBar()V
    .locals 2

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/NewTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/NewTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 208
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private canSearch()Z
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    iget-boolean v0, v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->hasSearchBar:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    .line 249
    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private filterTemplatesBySearchText()V
    .locals 3

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 289
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketTemplates:Ljava/util/List;

    invoke-static {v1}, Lrx/Observable;->from(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$JB1qpL5Y4npVDvvoXESjcmqZ42k;

    invoke-direct {v2, v0}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$JB1qpL5Y4npVDvvoXESjcmqZ42k;-><init>(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v1, v2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 293
    invoke-virtual {v0}, Lrx/Observable;->toList()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$jTSduhORy29kcaX87RYHjDBpM6E;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$jTSduhORy29kcaX87RYHjDBpM6E;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;)V

    .line 294
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method private getActionBarGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object v0
.end method

.method private getActionBarText()Ljava/lang/String;
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_new_ticket:I

    .line 229
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_new_group_ticket:I

    .line 230
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v1, v1, Lcom/squareup/api/items/TicketGroup;->name:Ljava/lang/String;

    const-string v2, "ticket_group"

    .line 231
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 233
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 3

    .line 212
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 213
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 215
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 216
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getActionBarGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getActionBarText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$AjQSv9PJL45tyydk4rbuPkcHcpA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$AjQSv9PJL45tyydk4rbuPkcHcpA;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;)V

    .line 217
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$filterTemplatesBySearchText$5(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Ljava/lang/Boolean;
    .locals 1

    .line 290
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object p1

    .line 291
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 292
    invoke-virtual {p1, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    .line 290
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private loadAvailableTicketTemplates(Ljava/lang/String;)V
    .locals 2

    .line 277
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$g3c3Y_BlOi_LRMCfecwQCt7p0yg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$g3c3Y_BlOi_LRMCfecwQCt7p0yg;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private loadTicketGroups()V
    .locals 2

    .line 262
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$kGPu-jSE7D3bhtr4Sf2KYvOugGg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$kGPu-jSE7D3bhtr4Sf2KYvOugGg;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private preselectSoleTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 3

    .line 179
    invoke-static {p1}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroupOrNull(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object p1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$cBVWgHUoUig92yD0FzLtmhV-X10;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$cBVWgHUoUig92yD0FzLtmhV-X10;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;Lcom/squareup/api/items/TicketGroup;)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private updateTicketGroupsOrTemplates()V
    .locals 3

    .line 253
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/NewTicketView;

    .line 254
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    sget-object v2, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    if-ne v1, v2, :cond_0

    .line 255
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketGroups:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/NewTicketView;->setTicketGroups(Ljava/util/List;)V

    goto :goto_0

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->filteredTicketTemplates:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/ticket/NewTicketView;->setTicketTemplates(Ljava/util/List;Z)V

    :goto_0
    return-void
.end method

.method private updateView()V
    .locals 2

    .line 237
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->updateTicketGroupsOrTemplates()V

    .line 238
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/NewTicketView;

    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->canSearch()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/NewTicketView;->setSearchBarVisible(Z)V

    .line 239
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/NewTicketView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    .line 240
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    iget-boolean v1, v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->hasCustomTicketButton:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 239
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/NewTicketView;->setCustomTicketButtonVisible(Z)V

    return-void
.end method


# virtual methods
.method getAvailableTemplateCount(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)I
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

    invoke-interface {v0, p1}, Lcom/squareup/opentickets/AvailableTemplateCountCache;->getAvailableTemplateCountForGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)I

    move-result p1

    return p1
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method getSelectedTicketGroupName()Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup;->name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 3

    .line 110
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/NewTicketScreen;

    .line 111
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$NewTicketScreen$DisplayMode:[I

    iget-object v1, p1, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 116
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    return-object p1

    .line 118
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a valid display mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_1
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$filterTemplatesBySearchText$6$NewTicketPresenter(Ljava/util/List;)V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->filteredTicketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->filteredTicketTemplates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 297
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 298
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->updateView()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$loadAvailableTicketTemplates$4$NewTicketPresenter(Ljava/lang/String;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0, p1}, Lcom/squareup/opentickets/PredefinedTickets;->getAvailableTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$QSKo-i1JLj4hkLIdqVKRxxrb1OI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$QSKo-i1JLj4hkLIdqVKRxxrb1OI;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;)V

    .line 279
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$loadTicketGroups$2$NewTicketPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->getAllTicketGroups()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$W3heB4K7QI8yFSl8RsBxKJjrd8g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$NewTicketPresenter$W3heB4K7QI8yFSl8RsBxKJjrd8g;-><init>(Lcom/squareup/ui/ticket/NewTicketPresenter;)V

    .line 264
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$NewTicketPresenter(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 265
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->hasLoadedData:Z

    .line 266
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketGroups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 267
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketGroups:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 268
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketGroups:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v0, :cond_0

    .line 269
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketGroups:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/NewTicketPresenter;->preselectSoleTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    goto :goto_0

    .line 270
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 271
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->updateView()V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$null$3$NewTicketPresenter(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 280
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->hasLoadedData:Z

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 283
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->filterTemplatesBySearchText()V

    return-void
.end method

.method public synthetic lambda$preselectSoleTicketGroup$0$NewTicketPresenter(Lcom/squareup/api/items/TicketGroup;Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 181
    invoke-virtual {p2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 191
    invoke-virtual {p2}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/ui/ticket/NewTicketScreen;

    if-eqz p2, :cond_0

    .line 192
    invoke-virtual {v0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 194
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 195
    invoke-static {p2, v1, p1}, Lcom/squareup/ui/ticket/NewTicketScreen;->forSelectingTicketTemplateFromSoleGroup(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/ui/ticket/NewTicketScreen;

    move-result-object p1

    .line 194
    invoke-virtual {v0, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 197
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method onClickCustomTicket()V
    .locals 6

    .line 151
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketPresenter$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 157
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    goto :goto_1

    .line 160
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a valid ticket action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    sget-object v3, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 154
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forCreatingEmptyNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    .line 163
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction(Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    .line 164
    iget-object v3, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v4, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    sget-object v5, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SOLE_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    if-ne v4, v5, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-virtual {v0, v3, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup(Lcom/squareup/api/items/TicketGroup;Z)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    .line 166
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildTicketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 76
    invoke-super {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 77
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/NewTicketScreen;

    .line 78
    iget-object v0, p1, Lcom/squareup/ui/ticket/NewTicketScreen;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    iput-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    .line 79
    iget-object p1, p1, Lcom/squareup/ui/ticket/NewTicketScreen;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 83
    invoke-super {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 84
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->buildActionBar()V

    if-eqz p1, :cond_0

    const-string v0, "newTicketScreenSearchText"

    .line 87
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    .line 91
    :cond_0
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->hasLoadedData:Z

    if-eqz p1, :cond_1

    .line 92
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->updateView()V

    return-void

    .line 96
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    sget-object v0, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_GROUPS:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    if-ne p1, v0, :cond_2

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->loadTicketGroups()V

    goto :goto_0

    .line 99
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object p1, p1, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/NewTicketPresenter;->loadAvailableTicketTemplates(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method onPressUp()Z
    .locals 7

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->displayMode:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;->SHOW_TICKET_TEMPLATES_FOR_SELECTED_GROUP:Lcom/squareup/ui/ticket/NewTicketScreen$DisplayMode;

    if-ne v0, v1, :cond_1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne v0, v1, :cond_0

    .line 133
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    goto :goto_0

    .line 134
    :cond_0
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forCreatingEmptyNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    .line 135
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->flow:Lflow/Flow;

    sget-object v2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildNewTicketScreen()Lcom/squareup/ui/ticket/NewTicketScreen;

    move-result-object v0

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Lcom/squareup/ui/ticket/NewTicketScreen;

    aput-object v6, v4, v5

    invoke-static {v1, v2, v0, v4}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return v3

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    move-result v0

    return v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 104
    invoke-super {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->onSave(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    const-string v1, "newTicketScreenSearchText"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onSearchTextChanged(Ljava/lang/String;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->searchText:Ljava/lang/String;

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketTemplates:Ljava/util/List;

    if-eqz p1, :cond_0

    .line 145
    invoke-direct {p0}, Lcom/squareup/ui/ticket/NewTicketPresenter;->filterTemplatesBySearchText()V

    :cond_0
    return-void
.end method

.method onSelectTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 3

    .line 170
    invoke-static {p1}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroupOrNull(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object p1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    iget-object v2, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    invoke-static {v1, v2, p1}, Lcom/squareup/ui/ticket/NewTicketScreen;->forSelectingTicketTemplate(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/ui/ticket/NewTicketScreen;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onSelectTicketTemplate(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)V
    .locals 3

    .line 202
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/NewTicketPresenter;->selectedTicketGroup:Lcom/squareup/api/items/TicketGroup;

    .line 203
    invoke-static {p1}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketTemplateOrNull(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/api/items/TicketTemplate;

    move-result-object p1

    const/4 v2, 0x0

    .line 202
    invoke-virtual {p0, v0, v2, v1, p1}, Lcom/squareup/ui/ticket/NewTicketPresenter;->saveTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    return-void
.end method
