.class public Lcom/squareup/ui/ticket/GroupListPresenter;
.super Lmortar/ViewPresenter;
.source "GroupListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/GroupListPresenter$Module;,
        Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;,
        Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/GroupListView;",
        ">;"
    }
.end annotation


# static fields
.field public static final ALL_TICKETS:Ljava/lang/String; = "group-list-presenter-selected-section-all-tickets"

.field static final CUSTOM_TICKETS:Ljava/lang/String; = "group-list-presenter-selected-section-custom-tickets"

.field public static final SEARCH:Ljava/lang/String; = "group-list-presenter-selected-section-search"

.field private static final TICKET_GROUP_SELECTED_SECTION:Ljava/lang/String; = "group-list-presenter-selected-section-ticket-group"


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field final blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private firstTimeLoadingGroups:Z

.field private final groupCache:Lcom/squareup/tickets/TicketGroupsCache;

.field private final groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

.field private groupQueryCallback:Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field final loadingGroupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field

.field private final lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final res:Lcom/squareup/util/Res;

.field private final selectedSection:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/main/LockOrClockButtonHelper;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            "Lcom/squareup/tickets/TicketCountsCache;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/ui/main/LockOrClockButtonHelper;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 89
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    .line 91
    iput-object p2, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    .line 92
    iput-object p3, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->selectedSection:Lcom/squareup/settings/LocalSetting;

    .line 93
    iput-object p4, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    .line 94
    iput-object p5, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 95
    iput-object p6, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    .line 96
    iput-object p8, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    .line 97
    iput-object p9, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->res:Lcom/squareup/util/Res;

    .line 98
    new-instance p1, Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-direct {p1, p7}, Lcom/squareup/caller/BlockingPopupPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    .line 99
    iput-object p11, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 100
    new-instance p1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->loadingGroupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 101
    invoke-interface {p10}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->internetState:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/GroupListPresenter;)Lcom/squareup/tickets/TicketGroupsCache;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/GroupListPresenter;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->updateView()V

    return-void
.end method

.method private buildRangerForGroupList(I)Lcom/squareup/ui/Ranger;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/ui/Ranger<",
            "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
            "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
            ">;"
        }
    .end annotation

    .line 230
    new-instance v0, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    .line 231
    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->SEARCH_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 232
    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->ALL_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    if-lez p1, :cond_0

    .line 234
    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->GROUP_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 236
    :cond_0
    sget-object p1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;->CUSTOM_TICKETS_ROW:Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 237
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object p1

    return-object p1
.end method

.method private cancelGroupQueryCallback()V
    .locals 1

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->hasPendingGroupQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupQueryCallback:Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;->cancel()V

    const/4 v0, 0x0

    .line 247
    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupQueryCallback:Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

    :cond_0
    return-void
.end method

.method private fetchGroups()V
    .locals 3

    .line 210
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->cancelGroupQueryCallback()V

    .line 211
    new-instance v0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;Lcom/squareup/ui/ticket/GroupListPresenter$1;)V

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupQueryCallback:Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$fjpSUamodk9X3HNJ2rP_k_uYwNw;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$fjpSUamodk9X3HNJ2rP_k_uYwNw;

    iget-object v2, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupQueryCallback:Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private hasPendingGroupQuery()Z
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupQueryCallback:Lcom/squareup/ui/ticket/GroupListPresenter$GroupQueryCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$fetchGroups$6(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 213
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 214
    invoke-interface {p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$nIV5WNl0Bzelu0cIsJzc0K-SHDA(Lcom/squareup/ui/ticket/GroupListPresenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/GroupListPresenter;->onCogsUpdated(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private onCogsUpdated(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 144
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 145
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->fetchGroups()V

    :cond_0
    return-void
.end method

.method private updateView()V
    .locals 3

    .line 220
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/GroupListView;

    .line 222
    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/ticket/GroupListPresenter;->buildRangerForGroupList(I)Lcom/squareup/ui/Ranger;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    .line 223
    invoke-virtual {v2}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupEntries()Ljava/util/List;

    move-result-object v2

    .line 222
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/ticket/GroupListView;->setList(Lcom/squareup/ui/Ranger;Ljava/util/List;)V

    .line 224
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListView;->ensureSelectedSectionIsVisible()V

    .line 225
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListView;->hideSpinner()V

    :cond_0
    return-void
.end method


# virtual methods
.method getAllTicketsCount()I
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->getAllTicketsCount()I

    move-result v0

    return v0
.end method

.method getCustomTicketsCount()I
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->getCustomTicketsCount()I

    move-result v0

    return v0
.end method

.method getGroupCount()I
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupCount()I

    move-result v0

    return v0
.end method

.method getGroupEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupEntries()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getLogOutButtonText()Ljava/lang/String;
    .locals 3

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->employeeIsGuest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_guest:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_log_out:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    .line 264
    invoke-virtual {v1}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->getEmployeeInitials()Ljava/lang/String;

    move-result-object v1

    const-string v2, "employee_initials"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 266
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSelectedGroup()Lcom/squareup/api/items/TicketGroup;
    .locals 5

    .line 154
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getSelectedSection()Ljava/lang/String;

    move-result-object v0

    const-string v1, "group-list-presenter-selected-section-search"

    .line 155
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_2

    const-string v1, "group-list-presenter-selected-section-all-tickets"

    .line 156
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "group-list-presenter-selected-section-custom-tickets"

    .line 157
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupEntries()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 162
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 163
    invoke-static {v3}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroupOrNull(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    return-object v0

    :cond_2
    :goto_0
    return-object v2
.end method

.method getSelectedSection()Ljava/lang/String;
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->selectedSection:Lcom/squareup/settings/LocalSetting;

    const-string v1, "group-list-presenter-selected-section-all-tickets"

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method getTicketCountForGroup(Ljava/lang/String;)I
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0, p1}, Lcom/squareup/tickets/TicketCountsCache;->getGroupTicketCount(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public synthetic lambda$null$2$GroupListPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->updateView()V

    return-void
.end method

.method public synthetic lambda$null$4$GroupListPresenter(Lcom/squareup/permissions/Employee;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/GroupListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/GroupListView;->fadeInLogOutButton()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$GroupListPresenter(Lcom/squareup/connectivity/InternetState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$GroupListPresenter()Lrx/Subscription;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->onTicketCountsRefreshed()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$BAtq4wx1aWsSlW2CI4wMp5PEv4Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$BAtq4wx1aWsSlW2CI4wMp5PEv4Q;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    .line 125
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$5$GroupListPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentEmployee()Lio/reactivex/Observable;

    move-result-object v0

    .line 133
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$hRxWOLB_Qy-7tfnhZky6SqGaja4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$hRxWOLB_Qy-7tfnhZky6SqGaja4;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    .line 134
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    .line 106
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$nIV5WNl0Bzelu0cIsJzc0K-SHDA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$nIV5WNl0Bzelu0cIsJzc0K-SHDA;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 105
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->internetState:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$hRBm2ZVk-lF62k7v7bdw5u9p_CY;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$hRBm2ZVk-lF62k7v7bdw5u9p_CY;

    .line 110
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$ClYLf2oSm4q3TuBsB4oWPTimzbg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$ClYLf2oSm4q3TuBsB4oWPTimzbg;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    .line 111
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 108
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->cancelGroupQueryCallback()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 119
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 121
    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->firstTimeLoadingGroups:Z

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$bhTtdTqTDvvq6HA_7qxRiMoWFo8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$bhTtdTqTDvvq6HA_7qxRiMoWFo8;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$6qAw1qDkvJn1QmlVQdr1NcLUIVo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$GroupListPresenter$6qAw1qDkvJn1QmlVQdr1NcLUIVo;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getSelectedSection()Ljava/lang/String;

    move-result-object p1

    const-string v0, "group-list-presenter-selected-section-search"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->selectAllTicketsSection()V

    .line 140
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->fetchGroups()V

    return-void
.end method

.method onLogOutButtonClicked()V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->lockOrClockButtonClicked()V

    return-void
.end method

.method onProgressHidden()V
    .locals 2

    .line 252
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/GroupListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/GroupListView;

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->firstTimeLoadingGroups:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/GroupListView;->showList(Z)V

    const/4 v0, 0x0

    .line 254
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->firstTimeLoadingGroups:Z

    :cond_0
    return-void
.end method

.method selectAllTicketsSection()V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->selectedSection:Lcom/squareup/settings/LocalSetting;

    const-string v1, "group-list-presenter-selected-section-all-tickets"

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->access$000(Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Ljava/lang/String;)V

    return-void
.end method

.method selectCustomTicketsSection()V
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->selectedSection:Lcom/squareup/settings/LocalSetting;

    const-string v1, "group-list-presenter-selected-section-custom-tickets"

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->access$000(Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Ljava/lang/String;)V

    return-void
.end method

.method selectGroupSection(Ljava/lang/String;)V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->selectedSection:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    invoke-static {v0, p1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->access$000(Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Ljava/lang/String;)V

    return-void
.end method

.method selectSearchSection()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->selectedSection:Lcom/squareup/settings/LocalSetting;

    const-string v1, "group-list-presenter-selected-section-search"

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter;->groupListListener:Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->access$000(Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Ljava/lang/String;)V

    return-void
.end method
