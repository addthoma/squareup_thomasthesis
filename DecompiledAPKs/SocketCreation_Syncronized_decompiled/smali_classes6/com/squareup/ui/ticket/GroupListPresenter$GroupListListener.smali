.class Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;
.super Ljava/lang/Object;
.source "GroupListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GroupListListener"
.end annotation


# instance fields
.field private final sectionSelected:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "group-list-presenter-selected-section-all-tickets"

    .line 304
    :goto_0
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->sectionSelected:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Ljava/lang/String;)V
    .locals 0

    .line 298
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->publishSectionSelected(Ljava/lang/String;)V

    return-void
.end method

.method private publishSectionSelected(Ljava/lang/String;)V
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->sectionSelected:Lcom/jakewharton/rxrelay/BehaviorRelay;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "group-list-presenter-selected-section-all-tickets"

    :goto_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method onSectionSelected()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;->sectionSelected:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
