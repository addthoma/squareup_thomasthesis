.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->bind()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;I)V
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;

    iput p2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;->val$position:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;->val$position:I

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->onEntryClicked(Ljava/lang/String;IZ)V

    .line 253
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder$1;->this$1:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;->access$700(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$CashDiscountViewHolder;)Lcom/squareup/marin/widgets/MarinCheckBox;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->toggle()V

    return-void
.end method
