.class public final Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;
.super Ljava/lang/Object;
.source "RealOpenTicketsHomeScreenSelector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;",
        ">;"
    }
.end annotation


# instance fields
.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v2, p0, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector_Factory;->get()Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;

    move-result-object v0

    return-object v0
.end method
