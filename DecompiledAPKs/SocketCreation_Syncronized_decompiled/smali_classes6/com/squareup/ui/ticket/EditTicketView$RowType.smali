.class final enum Lcom/squareup/ui/ticket/EditTicketView$RowType;
.super Ljava/lang/Enum;
.source "EditTicketView.java"

# interfaces
.implements Lcom/squareup/ui/Ranger$RowType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
        ">;",
        "Lcom/squareup/ui/Ranger$RowType<",
        "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/EditTicketView$RowType;

.field public static final enum BUTTON_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

.field public static final enum CONVERT_TO_CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

.field public static final enum HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

.field public static final enum NO_TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

.field public static final enum TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

.field public static final enum TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;


# instance fields
.field final holderType:Lcom/squareup/ui/ticket/EditTicketView$HolderType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 66
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    const/4 v2, 0x0

    const-string v3, "HEADER_ROW"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/ticket/EditTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    .line 67
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->CONVERT_TO_CUSTOM_TICKET_ROW_BUTTON_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    const/4 v3, 0x1

    const-string v4, "CONVERT_TO_CUSTOM_TICKET_BUTTON_ROW"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/ticket/EditTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->CONVERT_TO_CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    .line 68
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->TICKET_GROUP_HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    const/4 v4, 0x2

    const-string v5, "TICKET_GROUP_HEADER_ROW"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/ticket/EditTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    .line 69
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->NO_TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    const/4 v5, 0x3

    const-string v6, "NO_TICKET_GROUP_ROW"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/ticket/EditTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->NO_TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    .line 70
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    const/4 v6, 0x4

    const-string v7, "TICKET_GROUP_ROW"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/ui/ticket/EditTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    .line 71
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->BUTTON_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    const/4 v7, 0x5

    const-string v8, "BUTTON_GROUP_ROW"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/ui/ticket/EditTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->BUTTON_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/ticket/EditTicketView$RowType;

    .line 65
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->CONVERT_TO_CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->NO_TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->BUTTON_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->$VALUES:[Lcom/squareup/ui/ticket/EditTicketView$RowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/ui/ticket/EditTicketView$HolderType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
            ")V"
        }
    .end annotation

    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->holderType:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/EditTicketView$RowType;
    .locals 1

    .line 65
    const-class v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/EditTicketView$RowType;
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->$VALUES:[Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/EditTicketView$RowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/EditTicketView$RowType;

    return-object v0
.end method


# virtual methods
.method public getHolderType()Lcom/squareup/ui/ticket/EditTicketView$HolderType;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->holderType:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    return-object v0
.end method

.method public bridge synthetic getHolderType()Ljava/lang/Enum;
    .locals 1

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketView$RowType;->getHolderType()Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    move-result-object v0

    return-object v0
.end method
