.class public final synthetic Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/tickets/TicketsCallback;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

.field private final synthetic f$1:Ljava/lang/String;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Lcom/squareup/api/items/TicketGroup;

.field private final synthetic f$4:Lcom/squareup/api/items/TicketTemplate;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$0:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iput-object p2, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$1:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$3:Lcom/squareup/api/items/TicketGroup;

    iput-object p5, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$4:Lcom/squareup/api/items/TicketTemplate;

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/tickets/TicketsResult;)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$0:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iget-object v1, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$1:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$3:Lcom/squareup/api/items/TicketGroup;

    iget-object v4, p0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketScreen$EditTicketController$kDDfrL2sBAxMJiBe3nChaPCWMFA;->f$4:Lcom/squareup/api/items/TicketTemplate;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->lambda$editExistingTicketAndExit$0$EditTicketScreen$EditTicketController(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/tickets/TicketsResult;)V

    return-void
.end method
