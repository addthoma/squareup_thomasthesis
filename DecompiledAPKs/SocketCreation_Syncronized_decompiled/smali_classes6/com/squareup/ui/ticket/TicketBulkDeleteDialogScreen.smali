.class public final Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "TicketBulkDeleteDialogScreen.java"

# interfaces
.implements Lcom/squareup/ui/ticket/TicketScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Component;,
        Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Factory;,
        Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

.field private final authorizedEmployeeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 174
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$49osEA1B9r4G7vfb9972PMT2faw;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketBulkDeleteDialogScreen$49osEA1B9r4G7vfb9972PMT2faw;

    .line 175
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;Ljava/lang/String;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Landroid/content/Context;I)Ljava/lang/String;
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildTitleText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Landroid/content/Context;I)Ljava/lang/String;
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildMessageText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)I
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->getNegativeButtonResouceId()I

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)I
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->getPositiveButtonResourceId()I

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;Landroid/content/Context;I)Ljava/lang/String;
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildToastText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;)Ljava/lang/String;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->authorizedEmployeeToken:Ljava/lang/String;

    return-object p0
.end method

.method private static buildDeleteMessageText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 146
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_delete_ticket_warning_one:I

    .line 147
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_delete_ticket_warning_many:I

    .line 148
    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "number"

    .line 149
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 150
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static buildDeleteTitleText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 154
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_delete_ticket:I

    .line 155
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_delete_tickets:I

    .line 156
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static buildDeleteToastText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 138
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_deleted_one:I

    .line 139
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_deleted_many:I

    .line 140
    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "number"

    .line 141
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 142
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private buildMessageText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    if-ne v0, v1, :cond_0

    .line 105
    invoke-static {p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildDeleteMessageText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 106
    :cond_0
    invoke-static {p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildVoidMessageText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private buildTitleText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    if-ne v0, v1, :cond_0

    .line 111
    invoke-static {p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildDeleteTitleText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 112
    :cond_0
    invoke-static {p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildVoidTitleText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private buildToastText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    if-ne v0, v1, :cond_0

    .line 99
    invoke-static {p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildDeleteToastText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 100
    :cond_0
    invoke-static {p1, p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->buildVoidToastText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private static buildVoidMessageText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 124
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_void_ticket_warning_one:I

    .line 125
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_void_ticket_warning_many:I

    .line 126
    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "number"

    .line 127
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 128
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static buildVoidTitleText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 132
    sget p1, Lcom/squareup/orderentry/R$string;->void_ticket:I

    .line 133
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/orderentry/R$string;->void_tickets:I

    .line 134
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static buildVoidToastText(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 116
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_voided_one:I

    .line 117
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_voided_many:I

    .line 118
    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "number"

    .line 119
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 120
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static forDelete()Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
    .locals 3

    .line 40
    new-instance v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;-><init>(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forVoid(Ljava/lang/String;)Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->VOID:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;-><init>(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;Ljava/lang/String;)V

    return-object v0
.end method

.method private getNegativeButtonResouceId()I
    .locals 1

    .line 94
    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_delete_ticket_warning_keep:I

    return v0
.end method

.method private getPositiveButtonResourceId()I
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    if-ne v0, v1, :cond_0

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_delete:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/configure/item/R$string;->void_initial:I

    :goto_0
    return v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
    .locals 2

    .line 176
    invoke-static {}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->values()[Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    .line 177
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 178
    new-instance v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;-><init>(Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 169
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/InTicketActionScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 170
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->action:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-object p2, p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
