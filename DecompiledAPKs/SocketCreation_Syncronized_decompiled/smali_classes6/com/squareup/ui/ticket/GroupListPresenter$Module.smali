.class public Lcom/squareup/ui/ticket/GroupListPresenter$Module;
.super Ljava/lang/Object;
.source "GroupListPresenter.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideSelectedSectionListener(Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;"
        }
    .end annotation

    .line 326
    new-instance v0, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    invoke-interface {p0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static provideTicketGroupSelectedSection(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 320
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "group-list-presenter-selected-section-ticket-group"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method
