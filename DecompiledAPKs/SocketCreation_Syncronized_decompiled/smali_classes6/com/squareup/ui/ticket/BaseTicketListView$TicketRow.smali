.class final enum Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;
.super Ljava/lang/Enum;
.source "BaseTicketListView.java"

# interfaces
.implements Lcom/squareup/ui/Ranger$RowType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TicketRow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        ">;",
        "Lcom/squareup/ui/Ranger$RowType<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum AVAILABLE_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum ERROR:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum NEW_TICKET_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum NO_RESULTS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum NO_TICKETS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum OPEN_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum SORT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum TEMPLATE:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum TEXT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum VIEW_ALL_TICKETS_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

.field public static final enum YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;


# instance fields
.field private final holderType:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 74
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TICKET_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v2, 0x0

    const-string v3, "TICKET"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 75
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEMPLATE_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v3, 0x1

    const-string v4, "TEMPLATE"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEMPLATE:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 76
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEXT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v4, 0x2

    const-string v5, "TEXT"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEXT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 77
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SORT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v5, 0x3

    const-string v6, "SORT"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->SORT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 78
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->BUTTON_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v6, 0x4

    const-string v7, "NEW_TICKET_BUTTON"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NEW_TICKET_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 79
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->BUTTON_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v7, 0x5

    const-string v8, "VIEW_ALL_TICKETS_BUTTON"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->VIEW_ALL_TICKETS_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 80
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v8, 0x6

    const-string v9, "OPEN_TICKETS_HEADER"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OPEN_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 81
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/4 v9, 0x7

    const-string v10, "YOUR_TICKETS_HEADER"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 82
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v10, 0x8

    const-string v11, "AVAILABLE_TICKETS_HEADER"

    invoke-direct {v0, v11, v10, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->AVAILABLE_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 83
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v11, 0x9

    const-string v12, "OTHER_TICKETS_HEADER"

    invoke-direct {v0, v12, v11, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 84
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_TICKETS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v12, 0xa

    const-string v13, "NO_TICKETS"

    invoke-direct {v0, v13, v12, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NO_TICKETS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 85
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_RESULTS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v13, 0xb

    const-string v14, "NO_RESULTS"

    invoke-direct {v0, v14, v13, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NO_RESULTS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 86
    new-instance v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ERROR_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    const/16 v14, 0xc

    const-string v15, "ERROR"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ERROR:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    .line 73
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TICKET:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEMPLATE:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->TEXT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->SORT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NEW_TICKET_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->VIEW_ALL_TICKETS_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OPEN_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->AVAILABLE_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NO_TICKETS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NO_RESULTS:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ERROR:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->$VALUES:[Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
            ")V"
        }
    .end annotation

    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 91
    iput-object p3, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->holderType:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->$VALUES:[Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    return-object v0
.end method


# virtual methods
.method public getHolderType()Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->holderType:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    return-object v0
.end method

.method public bridge synthetic getHolderType()Ljava/lang/Enum;
    .locals 1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->getHolderType()Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    move-result-object v0

    return-object v0
.end method
