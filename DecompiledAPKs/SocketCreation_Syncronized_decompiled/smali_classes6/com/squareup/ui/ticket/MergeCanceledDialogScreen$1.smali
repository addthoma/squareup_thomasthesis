.class final Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;
.super Lcom/squareup/container/ContainerTreeKey$PathCreator;
.source "MergeCanceledDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
        "Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;->doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    move-result-object p1

    return-object p1
.end method

.method protected doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
    .locals 3

    .line 125
    invoke-static {}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->values()[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    .line 127
    new-instance v1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, v2}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;-><init>(Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;ILcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;)V

    return-object v1
.end method

.method public newArray(I)[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
    .locals 0

    .line 131
    new-array p1, p1, [Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$1;->newArray(I)[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    move-result-object p1

    return-object p1
.end method
