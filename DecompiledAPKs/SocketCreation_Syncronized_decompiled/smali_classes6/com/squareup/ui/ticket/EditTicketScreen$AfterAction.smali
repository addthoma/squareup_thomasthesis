.class public final enum Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;
.super Ljava/lang/Enum;
.source "EditTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AfterAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

.field public static final enum EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

.field public static final enum PRINT_BILL:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

.field public static final enum SPLIT_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 187
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    const/4 v1, 0x0

    const-string v2, "EXIT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 188
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    const/4 v2, 0x1

    const-string v3, "PRINT_BILL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->PRINT_BILL:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 189
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    const/4 v3, 0x2

    const-string v4, "SPLIT_TICKET"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->SPLIT_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 185
    sget-object v4, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->PRINT_BILL:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->SPLIT_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->$VALUES:[Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;
    .locals 1

    .line 185
    const-class v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;
    .locals 1

    .line 185
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->$VALUES:[Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-object v0
.end method
