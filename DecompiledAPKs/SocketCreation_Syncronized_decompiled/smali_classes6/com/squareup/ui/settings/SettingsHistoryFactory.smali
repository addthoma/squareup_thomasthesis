.class public Lcom/squareup/ui/settings/SettingsHistoryFactory;
.super Ljava/lang/Object;
.source "SettingsHistoryFactory.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# instance fields
.field private final permissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field

.field private final screens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsHistoryFactory;->screens:Ljava/util/List;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsHistoryFactory;->permissions:Ljava/util/Set;

    return-void
.end method

.method public static noPermissions(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 3

    .line 68
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v1, Lcom/squareup/ui/settings/SettingsHistoryFactory;

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/settings/SettingsHistoryFactory;-><init>(Ljava/util/List;Ljava/util/Set;)V

    invoke-direct {v0, v1}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object v0
.end method

.method public static varargs withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 0

    .line 53
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Ljava/util/List;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p0

    return-object p0
.end method

.method public static varargs withPermissions(Ljava/util/List;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;[",
            "Lcom/squareup/permissions/Permission;",
            ")",
            "Lcom/squareup/deeplinks/DeepLinkResult;"
        }
    .end annotation

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 63
    sget-object p1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v1, Lcom/squareup/ui/settings/SettingsHistoryFactory;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;-><init>(Ljava/util/List;Ljava/util/Set;)V

    invoke-direct {p1, v1}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    .line 33
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/SettingsSectionsScreen;->INSTANCE:Lcom/squareup/ui/settings/SettingsSectionsScreen;

    .line 34
    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 36
    iget-object p2, p0, Lcom/squareup/ui/settings/SettingsHistoryFactory;->screens:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 37
    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsHistoryFactory;->permissions:Ljava/util/Set;

    return-object v0
.end method
