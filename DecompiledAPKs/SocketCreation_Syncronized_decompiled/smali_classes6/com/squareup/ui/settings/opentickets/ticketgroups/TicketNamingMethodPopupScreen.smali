.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;
.super Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;
.source "TicketNamingMethodPopupScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;,
        Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;


# instance fields
.field private final onChangeTicketNamingMethodConfirmed:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    .line 58
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    .line 59
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;-><init>()V

    .line 20
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->onChangeTicketNamingMethodConfirmed:Lio/reactivex/subjects/PublishSubject;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;)Lio/reactivex/subjects/PublishSubject;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->onChangeTicketNamingMethodConfirmed:Lio/reactivex/subjects/PublishSubject;

    return-object p0
.end method


# virtual methods
.method public onChangeTicketNamingMethodConfirmed()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->onChangeTicketNamingMethodConfirmed:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method
