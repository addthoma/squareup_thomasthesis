.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;
.super Ljava/lang/Object;
.source "OpenTicketsSettingsRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final flow:Lflow/Flow;

.field private openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final res:Lcom/squareup/util/Res;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

.field private final ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/opentickets/TicketsSweeperManager;Lflow/Flow;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->tickets:Lcom/squareup/tickets/Tickets;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

    .line 41
    iput-object p6, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->flow:Lflow/Flow;

    .line 42
    iput-object p7, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public isOpenTicketsEnabled()Z
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method maybeDisableTickets(I)V
    .locals 3

    if-lez p1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v2}, Lcom/squareup/tickets/UnsyncedTicketConfirmations;->disableConfirmation(ILcom/squareup/util/Res;)Lcom/squareup/register/widgets/ConfirmationStrings;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;-><init>(Lcom/squareup/register/widgets/Confirmation;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 64
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->setTicketsEnabled(Z)V

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public openTicketsEnabled()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method setTicketsEnabled(Z)V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/OpenTicketsSettings;->setEnabled(Z)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    invoke-interface {p1}, Lcom/squareup/opentickets/TicketsListScheduler;->syncOnce()V

    goto :goto_0

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tickets/Tickets;->unlock(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/OpenTicketsSettings;->setEnabled(Z)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->openTicketsEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->ticketsListScheduler:Lcom/squareup/opentickets/TicketsListScheduler;

    invoke-interface {p1}, Lcom/squareup/opentickets/TicketsListScheduler;->stopSyncing()V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

    invoke-interface {p1}, Lcom/squareup/opentickets/TicketsSweeperManager;->stopSyncing()V

    :goto_0
    return-void
.end method
