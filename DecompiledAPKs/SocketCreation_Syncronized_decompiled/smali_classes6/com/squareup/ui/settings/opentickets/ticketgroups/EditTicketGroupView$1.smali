.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "EditTicketGroupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->onHeaderViewInflated(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 91
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->getCurrentGroupName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    iget-object v1, v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onNameChanged(Ljava/lang/String;)V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-static {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->access$100(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
