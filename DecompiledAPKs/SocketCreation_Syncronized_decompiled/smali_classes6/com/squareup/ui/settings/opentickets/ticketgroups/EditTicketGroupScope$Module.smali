.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Module;
.super Ljava/lang/Object;
.source "EditTicketGroupScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static onChangeTicketNamingMethodConfirmed()Lrx/Observable;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 36
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    .line 37
    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;->onChangeTicketNamingMethodConfirmed()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    .line 36
    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
