.class public final Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;
.super Ljava/lang/Object;
.source "OpenTicketsSettingsRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsListSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsSweeperManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->ticketsListSchedulerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->ticketsSweeperManagerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/opentickets/TicketsSweeperManager;Lflow/Flow;Lcom/squareup/util/Res;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;
    .locals 9

    .line 69
    new-instance v8, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;-><init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/opentickets/TicketsSweeperManager;Lflow/Flow;Lcom/squareup/util/Res;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/Tickets;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->ticketsListSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/opentickets/TicketsListScheduler;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->ticketsSweeperManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/opentickets/TicketsSweeperManager;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->newInstance(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/opentickets/TicketsSweeperManager;Lflow/Flow;Lcom/squareup/util/Res;)Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner_Factory;->get()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    move-result-object v0

    return-object v0
.end method
