.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;
.super Lcom/squareup/cogs/CatalogUpdateTask;
.source "EditTicketGroupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->deleteTicketGroup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-direct {p0}, Lcom/squareup/cogs/CatalogUpdateTask;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/squareup/shared/catalog/Catalog$Local;)V
    .locals 6

    .line 391
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 392
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 394
    const-class v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 395
    invoke-interface {p1, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 396
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v2

    .line 399
    :try_start_0
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-static {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getOrdinal()I

    move-result v3

    .line 400
    :cond_0
    :goto_0
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 401
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v4

    .line 402
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getOrdinal()I

    move-result v5

    if-le v5, v3, :cond_0

    .line 403
    invoke-static {v4}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object v4

    .line 404
    invoke-virtual {v4}, Lcom/squareup/api/items/TicketGroup;->newBuilder()Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v5

    iget-object v4, v4, Lcom/squareup/api/items/TicketGroup;->ordinal:Ljava/lang/Integer;

    .line 405
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v4

    .line 406
    invoke-virtual {v4}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object v4

    .line 404
    invoke-static {v4}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->create(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 409
    :cond_1
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-static {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->buildTicketGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;->create(Lcom/squareup/api/items/TicketGroup;)Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-static {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getTicketTemplates()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/TicketTemplate;

    .line 411
    invoke-static {v4}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 413
    :cond_2
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 416
    throw p1
.end method
