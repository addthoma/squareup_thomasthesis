.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditTicketGroupRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

.field final synthetic val$presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;->val$presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;->val$presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$1;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onTicketTemplateDeleted(Lcom/squareup/api/items/TicketTemplate$Builder;I)V

    return-void
.end method
