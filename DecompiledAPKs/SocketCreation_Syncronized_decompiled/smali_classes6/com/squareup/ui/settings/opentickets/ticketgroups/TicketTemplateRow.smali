.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "TicketTemplateRow.java"


# instance fields
.field private delete:Landroid/view/View;

.field private dragHandle:Landroid/view/View;

.field private nameView:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 25
    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    .line 26
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 25
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setBorderWidth(I)V

    .line 27
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 28
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->setHorizontalInsets(I)V

    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public getDragHandle()Landroid/view/View;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->dragHandle:Landroid/view/View;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNameView()Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    return-object v0
.end method

.method public hideControls()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->dragHandle:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->delete:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 32
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    .line 33
    sget v0, Lcom/squareup/settingsapplet/R$id;->ticket_template_drag_handle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->dragHandle:Landroid/view/View;

    .line 34
    sget v0, Lcom/squareup/settingsapplet/R$id;->ticket_template_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    .line 35
    sget v0, Lcom/squareup/settingsapplet/R$id;->ticket_template_delete:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->delete:Landroid/view/View;

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v2, Lcom/squareup/text/TrimLeadingSpacesScrubber;

    invoke-direct {v2}, Lcom/squareup/text/TrimLeadingSpacesScrubber;-><init>()V

    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v1, v2, v3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setContent(Ljava/lang/CharSequence;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    sget v0, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_template_name_hint:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    return-void
.end method

.method public setDeleteClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->delete:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setFocusChangedListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setInputEnabled(Z)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->nameView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    return-void
.end method

.method public showControls()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->dragHandle:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->delete:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
