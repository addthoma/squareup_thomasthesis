.class public final Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;
.super Ljava/lang/Object;
.source "DepositScheduleCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Clock;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;->newInstance(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Clock;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator_Factory;->get()Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    move-result-object v0

    return-object v0
.end method
