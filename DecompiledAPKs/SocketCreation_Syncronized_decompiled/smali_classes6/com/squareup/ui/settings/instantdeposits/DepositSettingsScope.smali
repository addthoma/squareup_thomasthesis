.class public final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "DepositSettingsScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;,
        Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositSettingsScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositSettingsScope.kt\ncom/squareup/ui/settings/instantdeposits/DepositSettingsScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,44:1\n35#2:45\n24#3,4:46\n*E\n*S KotlinDebug\n*F\n+ 1 DepositSettingsScope.kt\ncom/squareup/ui/settings/instantdeposits/DepositSettingsScope\n*L\n20#1:45\n42#1,4:46\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;",
        "Lcom/squareup/ui/settings/InSettingsAppletScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "register",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "Component",
        "Module",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 49
    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    const-class v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 20
    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;

    .line 21
    invoke-interface {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
