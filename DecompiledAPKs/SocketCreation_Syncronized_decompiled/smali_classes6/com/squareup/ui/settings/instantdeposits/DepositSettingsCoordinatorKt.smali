.class public final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt;
.super Ljava/lang/Object;
.source "DepositSettingsCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0080\u0004\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "CLOSE_OF_DAY_FORMAT",
        "",
        "DEFAULT_CLOSE_OF_DAY_FORMAT",
        "NO_DAY_OF_WEEK",
        "",
        "getNO_DAY_OF_WEEK",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "settings-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CLOSE_OF_DAY_FORMAT:Ljava/lang/String; = "h:mm a"

.field public static final DEFAULT_CLOSE_OF_DAY_FORMAT:Ljava/lang/String; = "hh:mm a z"

.field private static final NO_DAY_OF_WEEK:Ljava/lang/Integer;


# direct methods
.method public static final getNO_DAY_OF_WEEK()Ljava/lang/Integer;
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsCoordinatorKt;->NO_DAY_OF_WEEK:Ljava/lang/Integer;

    return-object v0
.end method
