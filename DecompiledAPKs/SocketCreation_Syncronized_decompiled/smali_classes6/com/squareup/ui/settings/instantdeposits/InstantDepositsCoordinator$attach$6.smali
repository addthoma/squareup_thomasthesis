.class final Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;
.super Ljava/lang/Object;
.source "DepositSettingsCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dayOfWeek:I

.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;->this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    iput p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;->$dayOfWeek:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;->this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->access$getDepositSpeedCustom$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)Lcom/squareup/noho/NohoCheckableRow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;->this$0:Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;->access$getScopeRunner$p(Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$attach$6;->$dayOfWeek:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->goToDepositScheduleScreen(I)V

    :cond_0
    return-void
.end method
