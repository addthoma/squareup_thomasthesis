.class public Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "CreateOrEditTeamPasscodeSuccessScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;

    .line 33
    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;

    .line 34
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 29
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 30
    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->createOrEditTeamPasscodeSuccessCoordinator()Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 24
    sget v0, Lcom/squareup/settingsapplet/R$layout;->passcodes_settings_create_or_edit_passcode_success:I

    return v0
.end method
