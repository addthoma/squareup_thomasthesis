.class public final Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;
.super Ljava/lang/Object;
.source "CreateOrEditTeamPasscodeSuccessScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field final passcodePreviouslyCreated:Z

.field final status:Lcom/squareup/permissions/PasscodesSettings$RequestState;


# direct methods
.method public constructor <init>(ZLcom/squareup/permissions/PasscodesSettings$RequestState;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->passcodePreviouslyCreated:Z

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;->status:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    return-void
.end method
