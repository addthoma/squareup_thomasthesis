.class public final synthetic Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;

    invoke-direct {v0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Ljava/lang/Object;)Z
    .locals 0

    check-cast p1, Lcom/squareup/permissions/Employee;

    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->is_account_owner()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method
