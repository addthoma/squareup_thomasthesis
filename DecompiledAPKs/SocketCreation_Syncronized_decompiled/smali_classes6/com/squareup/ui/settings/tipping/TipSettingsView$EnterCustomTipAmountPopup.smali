.class public final Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;
.super Lcom/squareup/flowlegacy/EditTextDialogPopup;
.source "TipSettingsView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnterCustomTipAmountPopup"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J4\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0018\u0010\u000e\u001a\u0014\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0018\u00010\u000fH\u0014J\u0008\u0010\u0011\u001a\u00020\u0012H\u0014R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;",
        "Lcom/squareup/flowlegacy/EditTextDialogPopup;",
        "context",
        "Landroid/content/Context;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Landroid/content/Context;Ljavax/inject/Provider;)V",
        "createDialog",
        "Landroid/app/Dialog;",
        "info",
        "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
        "withFlourish",
        "",
        "presenter",
        "Lcom/squareup/mortar/PopupPresenter;",
        "",
        "getLayout",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/EditTextDialogPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static final synthetic access$getEditor$p(Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 151
    iget-object p0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method public static final synthetic access$setEditor$p(Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method


# virtual methods
.method public bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 151
    check-cast p1, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->createDialog(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 160
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->createDialog(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    const-string p2, "super.createDialog(info, withFlourish, presenter)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 161
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 162
    iget-object p3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->root:Landroid/view/View;

    sget v0, Lcom/squareup/settingsapplet/R$id;->editor_wrapper:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "root.findViewById(R.id.editor_wrapper)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "editor"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {p3, v0}, Lcom/squareup/util/Views;->expandTouchArea(Landroid/view/View;Landroid/view/View;)V

    .line 164
    iget-object p3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    .line 165
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v1, Lcom/squareup/text/PercentageScrubber;

    iget-object v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->localeProvider:Ljavax/inject/Provider;

    invoke-direct {v1, p2, v2}, Lcom/squareup/text/PercentageScrubber;-><init>(ILjavax/inject/Provider;)V

    check-cast v1, Lcom/squareup/text/Scrubber;

    iget-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    check-cast p2, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v0, v1, p2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v0, Landroid/text/TextWatcher;

    .line 164
    invoke-virtual {p3, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 167
    iget-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    new-instance p3, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup$createDialog$1;

    invoke-direct {p3, p0}, Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup$createDialog$1;-><init>(Lcom/squareup/ui/settings/tipping/TipSettingsView$EnterCustomTipAmountPopup;)V

    check-cast p3, Ljava/lang/Runnable;

    invoke-virtual {p2, p3}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-object p1
.end method

.method protected getLayout()I
    .locals 1

    .line 171
    sget v0, Lcom/squareup/settingsapplet/R$layout;->editor_dialog_percentage:I

    return v0
.end method
