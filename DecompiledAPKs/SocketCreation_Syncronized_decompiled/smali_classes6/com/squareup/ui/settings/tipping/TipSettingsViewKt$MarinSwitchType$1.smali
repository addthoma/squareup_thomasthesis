.class final Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TipSettingsView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsViewKt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Landroid/content/Context;",
        "Lkotlin/jvm/functions/Function1<",
        "-",
        "Ljava/lang/Boolean;",
        "+",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/marin/widgets/MarinSwitch;",
        "+",
        "Lkotlin/jvm/functions/Function1<",
        "-",
        "Ljava/lang/Boolean;",
        "+",
        "Lkotlin/Unit;",
        ">;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTipSettingsView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TipSettingsView.kt\ncom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1\n*L\n1#1,370:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00030\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Pair;",
        "Lcom/squareup/marin/widgets/MarinSwitch;",
        "Lkotlin/Function1;",
        "",
        "",
        "context",
        "Landroid/content/Context;",
        "callback",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/Context;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1;->invoke(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Lkotlin/Pair;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlin/Pair<",
            "Lcom/squareup/marin/widgets/MarinSwitch;",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 356
    new-instance v0, Lcom/squareup/marin/widgets/MarinSwitch;

    invoke-direct {v0, p1}, Lcom/squareup/marin/widgets/MarinSwitch;-><init>(Landroid/content/Context;)V

    .line 357
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSwitch;->removePadding()V

    .line 358
    new-instance p1, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1$$special$$inlined$apply$lambda$1;

    invoke-direct {p1, p2}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1$$special$$inlined$apply$lambda$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinSwitch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 360
    new-instance p1, Lkotlin/Pair;

    new-instance p2, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1$1;

    invoke-direct {p2, v0}, Lcom/squareup/ui/settings/tipping/TipSettingsViewKt$MarinSwitchType$1$1;-><init>(Lcom/squareup/marin/widgets/MarinSwitch;)V

    invoke-direct {p1, v0, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method
