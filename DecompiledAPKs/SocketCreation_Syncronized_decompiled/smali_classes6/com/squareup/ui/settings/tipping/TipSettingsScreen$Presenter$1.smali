.class Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "TipSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->onPopupResult(Ljava/lang/String;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 67
    invoke-static {p1, v0}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->customTips:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)I

    move-result v1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->saveSettings()V

    .line 73
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->access$200(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/tipping/TipSettingsView;

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;->access$100(Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;)Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/tipping/TipSettingsView;->update(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V

    return-void
.end method
