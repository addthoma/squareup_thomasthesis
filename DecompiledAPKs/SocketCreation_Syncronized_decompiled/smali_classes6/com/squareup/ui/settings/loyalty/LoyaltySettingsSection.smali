.class public Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;
.super Lcom/squareup/applet/AppletSection;
.source "LoyaltySettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$Access;,
        Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;
    }
.end annotation


# static fields
.field public static final TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    sget v0, Lcom/squareup/settingsapplet/R$string;->loyalty_settings_title:I

    sput v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;

    return-object v0
.end method
