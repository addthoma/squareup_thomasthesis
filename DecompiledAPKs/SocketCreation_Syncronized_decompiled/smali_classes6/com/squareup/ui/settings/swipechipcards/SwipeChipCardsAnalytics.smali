.class public Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;
.super Ljava/lang/Object;
.source "SwipeChipCardsAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics$SwipeChipCardsToggleAction;
    }
.end annotation


# static fields
.field private static final OFF:Ljava/lang/String; = "OFF"

.field private static final ON:Ljava/lang/String; = "ON"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public swipeChipCardsEnabled()V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SWIPE_CHIP_CARDS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public swipeChipCardsToggled(Z)V
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics$SwipeChipCardsToggleAction;

    if-eqz p1, :cond_0

    const-string p1, "ON"

    goto :goto_0

    :cond_0
    const-string p1, "OFF"

    :goto_0
    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics$SwipeChipCardsToggleAction;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
