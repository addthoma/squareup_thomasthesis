.class public final Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "SwipeChipCardsSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Component;,
        Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;

    .line 111
    sget-object v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;

    .line 112
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SWIPE_CHIP_CARDS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 35
    const-class v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 115
    sget v0, Lcom/squareup/settingsapplet/R$layout;->swipe_chip_cards_settings_view:I

    return v0
.end method
