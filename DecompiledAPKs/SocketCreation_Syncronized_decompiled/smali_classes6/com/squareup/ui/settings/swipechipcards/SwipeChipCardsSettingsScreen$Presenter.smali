.class public Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "SwipeChipCardsSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/SwipeChipCardsSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->analytics:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    .line 56
    invoke-virtual {p2}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 66
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->setSwipeChipCardsEnabled(ZZ)V

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$1$SwipeChipCardsSettingsScreen$Presenter(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 64
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isEnabled()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/swipechipcards/-$$Lambda$SwipeChipCardsSettingsScreen$Presenter$fL-JsT3yUzZnmqJ7_WgIPx2SXBA;

    invoke-direct {v2, p1, v0}, Lcom/squareup/ui/settings/swipechipcards/-$$Lambda$SwipeChipCardsSettingsScreen$Presenter$fL-JsT3yUzZnmqJ7_WgIPx2SXBA;-><init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 66
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v1, 0x1

    .line 67
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method onEnabledToggled(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;Z)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->analytics:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsAnalytics;->swipeChipCardsToggled(Z)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 91
    invoke-virtual {p1, v0, v0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->setSwipeChipCardsEnabled(ZZ)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen;->INSTANCE:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    invoke-virtual {p1, v0}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;

    .line 63
    new-instance v0, Lcom/squareup/ui/settings/swipechipcards/-$$Lambda$SwipeChipCardsSettingsScreen$Presenter$HN0WsFCXnE82gx_FG4XQJQWrbpo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/swipechipcards/-$$Lambda$SwipeChipCardsSettingsScreen$Presenter$HN0WsFCXnE82gx_FG4XQJQWrbpo;-><init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onUpPressed()Z
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->saveSettings()V

    const/4 v0, 0x0

    return v0
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 73
    const-class v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;

    return-object v0
.end method
