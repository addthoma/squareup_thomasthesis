.class public Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "SwipeChipCardsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 6

    .line 45
    sget v3, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isEnabledBlocking()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_toggle_on:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_toggle_off:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
