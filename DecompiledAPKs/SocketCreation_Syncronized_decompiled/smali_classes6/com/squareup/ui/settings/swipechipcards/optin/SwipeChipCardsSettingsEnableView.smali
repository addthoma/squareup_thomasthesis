.class public Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;
.super Lcom/squareup/marin/widgets/DetailConfirmationView;
.source "SwipeChipCardsSettingsEnableView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-class p2, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Component;->inject(Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 62
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 58
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 32
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->onAttachedToWindow()V

    .line 34
    new-instance v0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView$1;-><init>(Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->setOnConfirmListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_popup_liability_warning_text:I

    sget v2, Lcom/squareup/common/bootstrap/R$dimen;->message_new_line_spacing:I

    invoke-static {v0, v1, v2}, Lcom/squareup/text/Fonts;->addSectionBreaks(Landroid/content/res/Resources;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->setMessage(Ljava/lang/CharSequence;)V

    .line 44
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_popup_for_more_information:I

    const-string v2, "support_center"

    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->swipe_chip_cards_url:I

    .line 46
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 44
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->setHelperText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->presenter:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableView;->presenter:Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 54
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->onDetachedFromWindow()V

    return-void
.end method
