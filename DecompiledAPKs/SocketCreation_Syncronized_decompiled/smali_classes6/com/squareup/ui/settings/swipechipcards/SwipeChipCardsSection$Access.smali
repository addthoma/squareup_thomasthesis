.class public final Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "SwipeChipCardsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private features:Lcom/squareup/settings/server/Features;

.field private final r12HasConnected:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 63
    invoke-direct {p0, p3, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;->r12HasConnected:Lcom/squareup/settings/LocalSetting;

    .line 65
    iput-object p2, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    .line 66
    iput-object p3, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/SwipeChipCardsSettings;->isAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;->r12HasConnected:Lcom/squareup/settings/LocalSetting;

    .line 71
    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 72
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
