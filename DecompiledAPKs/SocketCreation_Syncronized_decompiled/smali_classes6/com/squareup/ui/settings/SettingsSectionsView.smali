.class public Lcom/squareup/ui/settings/SettingsSectionsView;
.super Landroid/widget/LinearLayout;
.source "SettingsSectionsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;,
        Lcom/squareup/ui/settings/SettingsSectionsView$RowType;,
        Lcom/squareup/ui/settings/SettingsSectionsView$Component;
    }
.end annotation


# static fields
.field private static final APPLET_SECTION:I = 0x1

.field private static final BANNER:I = 0x0

.field private static final NO_HEADER:I = -0x1

.field private static final SIGN_OUT_BUTTON:I = 0x2

.field private static final SIGN_OUT_HEADER_ID:I = 0x2


# instance fields
.field private adapter:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final displayedAsSidebar:Z

.field private listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

.field presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    const-class p2, Lcom/squareup/ui/settings/SettingsSectionsView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsSectionsView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/SettingsSectionsView$Component;->inject(Lcom/squareup/ui/settings/SettingsSectionsView;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->displayedAsSidebar:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/SettingsSectionsView;)Z
    .locals 0

    .line 42
    iget-boolean p0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->displayedAsSidebar:Z

    return p0
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 110
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    return-object v0
.end method

.method initializeList()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->displayedAsSidebar:Z

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setAreHeadersSticky(Z)V

    .line 101
    new-instance v0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->adapter:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->adapter:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$SettingsSectionsView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->onSectionClicked(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 83
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$0A0oVXHXaUGQibg2tYFHpBcesAo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$0A0oVXHXaUGQibg2tYFHpBcesAo;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->dropView(Ljava/lang/Object;)V

    .line 92
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 74
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 75
    sget v0, Lcom/squareup/settingsapplet/R$id;->section_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    iput-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    .line 76
    iget-boolean v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->displayedAsSidebar:Z

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setSelectorPadding(I)V

    :cond_0
    return-void
.end method

.method scrollToSection(I)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->listView:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setSelection(I)V

    return-void
.end method

.method updateList()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView;->adapter:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->notifyDataSetChanged()V

    return-void
.end method
