.class public final Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "EmailCollectionConfirmationDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Factory;,
        Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;,
        Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$ERuB0Uc5_8rHt8dYVoP4ECMNTKs;->INSTANCE:Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionConfirmationDialog$ERuB0Uc5_8rHt8dYVoP4ECMNTKs;

    .line 57
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;
    .locals 0

    .line 57
    new-instance p0, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;

    invoke-direct {p0}, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;-><init>()V

    return-object p0
.end method
