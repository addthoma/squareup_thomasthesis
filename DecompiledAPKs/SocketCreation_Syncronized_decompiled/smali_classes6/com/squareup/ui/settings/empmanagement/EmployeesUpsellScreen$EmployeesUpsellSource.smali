.class public final enum Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;
.super Ljava/lang/Enum;
.source "EmployeesUpsellScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EmployeesUpsellSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

.field public static final enum LEARN_MORE_BUTTON:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

.field public static final enum TRACK_TIME_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    const/4 v1, 0x0

    const-string v2, "LEARN_MORE_BUTTON"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->LEARN_MORE_BUTTON:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    .line 41
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    const/4 v2, 0x1

    const-string v3, "TRACK_TIME_TOGGLE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->TRACK_TIME_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    .line 39
    sget-object v3, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->LEARN_MORE_BUTTON:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->TRACK_TIME_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->$VALUES:[Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->$VALUES:[Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    return-object v0
.end method
