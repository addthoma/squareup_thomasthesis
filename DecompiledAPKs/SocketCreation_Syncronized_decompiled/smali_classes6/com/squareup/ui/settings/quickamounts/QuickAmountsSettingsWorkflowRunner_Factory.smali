.class public final Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final createItemTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->createItemTutorialRunnerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->quickAmountsSettingsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;",
            ">;)",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;Lcom/squareup/items/tutorial/CreateItemTutorialRunner;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;)Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;
    .locals 7

    .line 60
    new-instance v6, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;Lcom/squareup/items/tutorial/CreateItemTutorialRunner;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->createItemTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

    iget-object v3, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->quickAmountsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v4, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsWorkflow;Lcom/squareup/items/tutorial/CreateItemTutorialRunner;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsViewFactory;)Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner_Factory;->get()Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
