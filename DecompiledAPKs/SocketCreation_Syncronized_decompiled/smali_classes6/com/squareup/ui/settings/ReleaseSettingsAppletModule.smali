.class public abstract Lcom/squareup/ui/settings/ReleaseSettingsAppletModule;
.super Ljava/lang/Object;
.source "ReleaseSettingsAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/settings/CommonSettingsAppletModule;,
        Lcom/squareup/ui/settings/DefaultSettingsAppletServicesModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract settingsAppletEntryPoint(Lcom/squareup/ui/settings/PosSettingsAppletEntryPoint;)Lcom/squareup/ui/settings/SettingsAppletEntryPoint;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract settingsAppletSections(Lcom/squareup/ui/settings/PosSettingsAppletSections;)Lcom/squareup/ui/settings/SettingsAppletSections;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
