.class public Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;
.super Lcom/squareup/marin/widgets/DetailConfirmationView;
.source "StoreAndForwardSettingsEnableView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const-class p2, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Component;->inject(Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 64
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 60
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 33
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->onAttachedToWindow()V

    .line 35
    new-instance v0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView$1;-><init>(Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->setOnConfirmListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/common/bootstrap/R$string;->offline_mode_warning:I

    sget v2, Lcom/squareup/common/bootstrap/R$dimen;->message_new_line_spacing:I

    invoke-static {v0, v1, v2}, Lcom/squareup/text/Fonts;->addSectionBreaks(Landroid/content/res/Resources;II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->setMessage(Ljava/lang/CharSequence;)V

    .line 45
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->offline_mode_for_more_information:I

    const-string v2, "support_center"

    .line 46
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$string;->offline_mode_url:I

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 45
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->setHelperText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->presenter:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->presenter:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 56
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->onDetachedFromWindow()V

    return-void
.end method
