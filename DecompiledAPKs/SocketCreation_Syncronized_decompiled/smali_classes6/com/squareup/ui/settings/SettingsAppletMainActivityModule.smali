.class public abstract Lcom/squareup/ui/settings/SettingsAppletMainActivityModule;
.super Ljava/lang/Object;
.source "SettingsAppletMainActivityModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/SettingsAppletMainActivityModule;",
        "",
        "()V",
        "bindMerchantProfileBadge",
        "Lmortar/Scoped;",
        "merchantProfileBadge",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;",
        "bindMerchantProfileBadge$settings_applet_release",
        "bindSettingsBadge",
        "settingsBadge",
        "Lcom/squareup/ui/settings/SettingsBadge;",
        "bindSettingsBadge$settings_applet_release",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindMerchantProfileBadge$settings_applet_release(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindSettingsBadge$settings_applet_release(Lcom/squareup/ui/settings/SettingsBadge;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
