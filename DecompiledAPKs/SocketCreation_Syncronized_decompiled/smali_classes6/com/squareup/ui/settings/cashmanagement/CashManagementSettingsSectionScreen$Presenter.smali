.class Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "CashManagementSettingsSectionScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

.field private final flow:Lflow/Flow;

.field private final helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->controller:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->flow:Lflow/Flow;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    return-void
.end method


# virtual methods
.method enableAutoEmailToggled(Z)V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->enableAutoEmailToggled(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Z)V

    return-void
.end method

.method enableCashManagementToggled(Z)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->controller:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->enableCashManagementToggled(Z)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->getPriceLocaleHelper()Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$0$CashManagementSettingsSectionScreen$Presenter(Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;)Lrx/Subscription;
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->controller:Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementEnabled()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$xLpfsCQENPKDU56ZEllmXt3JAMs;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$xLpfsCQENPKDU56ZEllmXt3JAMs;-><init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;)V

    .line 69
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method onBackPressed()Z
    .locals 3

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    iget-object v2, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->handleInvalidEmail(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Lflow/Flow;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->hasView()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->handleBackPressed(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 64
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;

    .line 67
    new-instance v0, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CashManagementSettingsSectionScreen$Presenter$9gl2MiU96FLplvhG1F7q-ao3-QY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CashManagementSettingsSectionScreen$Presenter$9gl2MiU96FLplvhG1F7q-ao3-QY;-><init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->onLoad(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    return-void
.end method

.method protected onUpPressed()Z
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected saveSettings()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->helper:Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsPresenterHelper;->saveSettings(Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;)V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 74
    const-class v0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;

    return-object v0
.end method
