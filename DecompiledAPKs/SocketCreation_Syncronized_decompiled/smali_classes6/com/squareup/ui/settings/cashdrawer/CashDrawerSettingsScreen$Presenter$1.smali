.class Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CashDrawerSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->testCashDrawersClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->access$100(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CASH_DRAWER_TEST:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
