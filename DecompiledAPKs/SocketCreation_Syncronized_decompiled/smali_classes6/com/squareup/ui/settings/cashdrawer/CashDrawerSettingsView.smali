.class public Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;
.super Landroid/widget/LinearLayout;
.source "CashDrawerSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private adapter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;

.field presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 47
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    new-instance v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;

    iget-object v1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;-><init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->adapter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;

    .line 32
    sget v0, Lcom/squareup/settingsapplet/R$id;->cash_drawer_settings_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 33
    iget-object v1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->adapter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method updateView(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cashdrawer/CashDrawer;",
            ">;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->adapter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->setCashDrawerList(Ljava/util/Collection;)V

    return-void
.end method
