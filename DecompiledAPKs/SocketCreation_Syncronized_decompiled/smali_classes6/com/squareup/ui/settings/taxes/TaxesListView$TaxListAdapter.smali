.class Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;
.super Landroid/widget/BaseAdapter;
.source "TaxesListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/TaxesListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaxListAdapter"
.end annotation


# static fields
.field private static final BUTTON_ROW:I = 0x0

.field private static final CONDITIONAL_TAXES_HELP_ROW:I = 0x2

.field private static final ROW_TYPE_COUNT:I = 0x3

.field private static final TAX_ROW:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/taxes/TaxesListView;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/taxes/TaxesListView;Lcom/squareup/ui/settings/taxes/TaxesListView$1;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListView;)V

    return-void
.end method

.method private buildAndBindButtonRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 141
    sget p1, Lcom/squareup/settingsapplet/R$layout;->tax_settings_add_tax_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 142
    sget p2, Lcom/squareup/settingsapplet/R$id;->add_tax_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 143
    new-instance v0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter$1;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object p1
.end method

.method private buildAndBindCondtionalTaxesHelpTextRow(Lcom/squareup/register/widgets/list/HelpRow;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p1, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    .line 173
    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/TaxesListView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 174
    new-instance v0, Lcom/squareup/register/widgets/list/HelpRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/taxes/TaxesListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->conditional_taxes_help_text_for_taxes_settings:I

    const-string v3, "dashboard"

    .line 175
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    .line 176
    invoke-virtual {v2}, Lcom/squareup/ui/settings/taxes/TaxesListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/configure/item/R$string;->dashboard_taxes_url:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/configure/item/R$string;->dashboard:I

    .line 177
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 178
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/squareup/register/widgets/list/HelpRow;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    const/4 p2, 0x0

    .line 179
    invoke-virtual {v0, p1, p2, p1, p2}, Lcom/squareup/register/widgets/list/HelpRow;->setPadding(IIII)V

    move-object p1, v0

    :cond_0
    return-object p1
.end method

.method private buildAndBindTaxRow(Lcom/squareup/ui/account/view/LineRow;I)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 156
    new-instance p1, Lcom/squareup/ui/account/view/LineRow$Builder;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 157
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->displayValueAsPercent(Z)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->isTaxEnabled(I)Z

    move-result v0

    .line 162
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setDisplayValueAsPercent(Z)V

    .line 163
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValueEnabled(Z)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTaxStatusText(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTaxNameText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-object p1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTaxesCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 90
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object v1, v1, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->showConditionalTaxesHelpText()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->showConditionalTaxesHelpText()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->getCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x2

    return p1

    :cond_1
    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 131
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 132
    check-cast p2, Landroid/view/ViewGroup;

    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->buildAndBindButtonRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 133
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 134
    check-cast p2, Lcom/squareup/register/widgets/list/HelpRow;

    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->buildAndBindCondtionalTaxesHelpTextRow(Lcom/squareup/register/widgets/list/HelpRow;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 136
    :cond_1
    check-cast p2, Lcom/squareup/ui/account/view/LineRow;

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->buildAndBindTaxRow(Lcom/squareup/ui/account/view/LineRow;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->getItemViewType(I)I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method
