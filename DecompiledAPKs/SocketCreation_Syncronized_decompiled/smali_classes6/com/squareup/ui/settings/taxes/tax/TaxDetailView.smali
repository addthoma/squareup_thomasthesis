.class public Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;
.super Landroid/widget/LinearLayout;
.source "TaxDetailView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/items/BaseEditObjectView;


# instance fields
.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private applicableItemsRow:Lcom/squareup/ui/account/view/LineRow;

.field private applicableServicesRow:Lcom/squareup/ui/account/view/LineRow;

.field private banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

.field private content:Landroid/view/View;

.field private customCheck:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private itemPricingRow:Lcom/squareup/ui/account/view/LineRow;

.field private nameRow:Lcom/squareup/ui/account/view/LineRow;

.field private percentageRow:Lcom/squareup/ui/account/view/LineRow;

.field presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

.field private removeView:Lcom/squareup/ui/ConfirmButton;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private taxEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private typeRow:Lcom/squareup/ui/account/view/LineRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class p2, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;->inject(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    return-void
.end method


# virtual methods
.method hideProgressBar()V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method hideRemoveTax()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->removeView:Lcom/squareup/ui/ConfirmButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$TaxDetailView()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onProgressHidden()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$TaxDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onTaxEnabledClicked(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$TaxDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onCustomAmountRowClicked(Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->dropView(Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 53
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_detail_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailView$4zpkDDx-hxy4oS37LIEAlKz8qlA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailView$4zpkDDx-hxy4oS37LIEAlKz8qlA;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 56
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_detail_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->content:Landroid/view/View;

    .line 58
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_enabled_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->taxEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->taxEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailView$NU7oqOwVD5vrQPdx14-DQ64fIQg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailView$NU7oqOwVD5vrQPdx14-DQ64fIQg;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 62
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_type_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->typeRow:Lcom/squareup/ui/account/view/LineRow;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->typeRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$1;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_name_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->nameRow:Lcom/squareup/ui/account/view/LineRow;

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->nameRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$2;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_percentage_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->percentageRow:Lcom/squareup/ui/account/view/LineRow;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->percentageRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$3;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    sget v0, Lcom/squareup/settingsapplet/R$id;->custom_tax_enabled_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->customCheck:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->customCheck:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailView$NkUgdwLEJgTrldHYpGJ6a8K0--8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailView$NkUgdwLEJgTrldHYpGJ6a8K0--8;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 87
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_applicable_items_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableItemsRow:Lcom/squareup/ui/account/view/LineRow;

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableItemsRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$4;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_applicable_services_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableServicesRow:Lcom/squareup/ui/account/view/LineRow;

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableServicesRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->supportsServices()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableServicesRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$5;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_item_pricing_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->itemPricingRow:Lcom/squareup/ui/account/view/LineRow;

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->itemPricingRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView$6;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    sget v0, Lcom/squareup/settingsapplet/R$id;->delete_tax_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->removeView:Lcom/squareup/ui/ConfirmButton;

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->removeView:Lcom/squareup/ui/ConfirmButton;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$eBs9r3KQrjSGxigd2h3MubqPWBg;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$eBs9r3KQrjSGxigd2h3MubqPWBg;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 113
    sget v0, Lcom/squareup/settingsapplet/R$id;->banner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBanner;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setApplicableItemsRowValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableItemsRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setApplicableServicesRowValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->applicableServicesRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setCustomTaxEnabled(Z)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->customCheck:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setItemPricingRowValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->itemPricingRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setNameRowValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->nameRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPercentageRowValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->percentageRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTaxEnabledRowChecked(Z)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->taxEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    return-void
.end method

.method setTypeRowValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->typeRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showContent()V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->content:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method showFeeTypes()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->typeRow:Lcom/squareup/ui/account/view/LineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public showMultiUnitContent()V
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->removeView:Lcom/squareup/ui/ConfirmButton;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->item_editing_delete_from_location_tax:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/AppliedLocationsBanner;->setVisibility(I)V

    return-void
.end method
