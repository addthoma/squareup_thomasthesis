.class public final Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;
.super Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.source "TaxItemPricingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Component;,
        Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxItemPricingScreen$nGsv-7cUsbN-tUSDzSql5DzO_18;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxItemPricingScreen$nGsv-7cUsbN-tUSDzSql5DzO_18;

    .line 88
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;
    .locals 1

    .line 88
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 83
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 84
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 33
    const-class v0, Lcom/squareup/ui/settings/taxes/TaxesSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 91
    sget v0, Lcom/squareup/settingsapplet/R$layout;->tax_item_pricing_view:I

    return v0
.end method
