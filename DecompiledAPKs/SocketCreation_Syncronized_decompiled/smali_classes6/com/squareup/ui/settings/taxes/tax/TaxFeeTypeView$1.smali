.class Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TaxFeeTypeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->createFeeTypeRow(Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;I)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;

    iput p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;->val$index:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;

    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeView$1;->val$index:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;->feeTypeRowClicked(I)V

    return-void
.end method
