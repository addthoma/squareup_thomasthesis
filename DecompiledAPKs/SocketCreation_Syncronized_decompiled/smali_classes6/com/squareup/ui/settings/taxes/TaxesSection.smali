.class public Lcom/squareup/ui/settings/taxes/TaxesSection;
.super Lcom/squareup/applet/AppletSection;
.source "TaxesSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/TaxesSection$ListEntry;,
        Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/permissions/Permission;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/TaxesSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/ui/settings/taxes/TaxesListScreen;->INSTANCE:Lcom/squareup/ui/settings/taxes/TaxesListScreen;

    return-object v0
.end method
