.class Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "TaxesListScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/TaxesListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/taxes/TaxesListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private cogsUpToDate:Z

.field private final collator:Ljava/text/Collator;

.field private final feesEditor:Lcom/squareup/settings/server/FeesEditor;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

.field private final taxListCache:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/FeesEditor;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 61
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->collator:Ljava/text/Collator;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->taxListCache:Ljava/util/List;

    const/4 v0, 0x0

    .line 69
    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->cogsUpToDate:Z

    .line 75
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getBadBus()Lcom/squareup/badbus/BadBus;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    .line 77
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 78
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 79
    iput-object p5, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 80
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private getTax(I)Lcom/squareup/shared/catalog/models/CatalogTax;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->taxListCache:Ljava/util/List;

    add-int/lit8 p1, p1, -0x1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogTax;

    return-object p1
.end method

.method private launchTax(Ljava/lang/String;)V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private refreshTaxesCacheAndShowList()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->taxListCache:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->taxListCache:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    invoke-interface {v1}, Lcom/squareup/settings/server/FeesEditor;->getTaxes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->taxListCache:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListScreen$Presenter$VXUQoFqSO4UzO2fqk2ItZgHU9_0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListScreen$Presenter$VXUQoFqSO4UzO2fqk2ItZgHU9_0;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 167
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/TaxesListView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListView;->hideProgressBarToShowList()V

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->tax_taxes:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getTaxNameText(I)Ljava/lang/CharSequence;
    .locals 0

    .line 156
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTax(I)Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getTaxStatusText(I)Ljava/lang/String;
    .locals 1

    .line 149
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTax(I)Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object p1

    .line 150
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getPercentage()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {p1, v0}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->tax_off:I

    .line 152
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method getTaxesCount()I
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->taxListCache:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method isTaxEnabled(I)Z
    .locals 0

    .line 145
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTax(I)Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->isEnabled()Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$0$TaxesListScreen$Presenter()V
    .locals 1

    const/4 v0, 0x1

    .line 89
    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->cogsUpToDate:Z

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->refreshTaxesCacheAndShowList()V

    return-void
.end method

.method public synthetic lambda$refreshTaxesCacheAndShowList$1$TaxesListScreen$Presenter(Lcom/squareup/shared/catalog/models/CatalogTax;Lcom/squareup/shared/catalog/models/CatalogTax;)I
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTax;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 84
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/settings/server/FeesUpdate;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/taxes/-$$Lambda$Gzzh0qt0KkjO1Vm6yOTnRmw_nL4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/-$$Lambda$Gzzh0qt0KkjO1Vm6yOTnRmw_nL4;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    new-instance v0, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListScreen$Presenter$vUsVJCGwvF8uAWmp23d07SVj-7A;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/-$$Lambda$TaxesListScreen$Presenter$vUsVJCGwvF8uAWmp23d07SVj-7A;-><init>(Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;)V

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/FeesEditor;->read(Ljava/lang/Runnable;)V

    return-void
.end method

.method onFeesUpdated(Lcom/squareup/settings/server/FeesUpdate;)V
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->cogsUpToDate:Z

    if-eqz v0, :cond_0

    iget-boolean p1, p1, Lcom/squareup/settings/server/FeesUpdate;->inForeground:Z

    if-eqz p1, :cond_0

    .line 120
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->refreshTaxesCacheAndShowList()V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 99
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 100
    iget-boolean p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->cogsUpToDate:Z

    if-eqz p1, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->refreshTaxesCacheAndShowList()V

    :cond_0
    return-void
.end method

.method onNewTaxClicked()V
    .locals 1

    .line 129
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->NEW_TAX:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->launchTax(Ljava/lang/String;)V

    return-void
.end method

.method onProgressHidden()V
    .locals 1

    .line 160
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/TaxesListView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/TaxesListView;->refreshList()V

    return-void
.end method

.method onTaxClicked(I)V
    .locals 0

    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->getTax(I)Lcom/squareup/shared/catalog/models/CatalogTax;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->launchTax(Ljava/lang/String;)V

    return-void
.end method

.method protected saveSettings()V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 106
    const-class v0, Lcom/squareup/ui/settings/taxes/TaxesListScreen;

    return-object v0
.end method

.method showConditionalTaxesHelpText()Z
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseConditionalTaxes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->feesEditor:Lcom/squareup/settings/server/FeesEditor;

    invoke-interface {v0}, Lcom/squareup/settings/server/FeesEditor;->getTaxRules()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
