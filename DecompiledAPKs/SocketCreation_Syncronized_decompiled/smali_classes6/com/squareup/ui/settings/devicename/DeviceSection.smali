.class public Lcom/squareup/ui/settings/devicename/DeviceSection;
.super Lcom/squareup/applet/AppletSection;
.source "DeviceSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/devicename/DeviceSection$Access;,
        Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    sget v0, Lcom/squareup/registerlib/R$string;->device_name:I

    sput v0, Lcom/squareup/ui/settings/devicename/DeviceSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/settings/devicename/DeviceSection$Access;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;->INSTANCE:Lcom/squareup/ui/settings/devicename/DeviceNameScreen;

    return-object v0
.end method
