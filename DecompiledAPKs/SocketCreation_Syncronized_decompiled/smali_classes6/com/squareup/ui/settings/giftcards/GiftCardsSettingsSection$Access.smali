.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "GiftCardsSettingsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Access"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$Access;",
        "Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "determineVisibility",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 32
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$Access;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_SETTINGS_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
