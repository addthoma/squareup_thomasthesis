.class final Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$addDesigns$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewDesignsSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/noho/NohoButton;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/NohoButton;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$addDesigns$2;->this$0:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/noho/NohoButton;
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$addDesigns$2;->this$0:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->access$getView$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$id;->egiftcard_design_settings_add:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$addDesigns$2;->invoke()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    return-object v0
.end method
