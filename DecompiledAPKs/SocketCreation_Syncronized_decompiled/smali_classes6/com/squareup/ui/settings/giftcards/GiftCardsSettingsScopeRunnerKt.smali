.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunnerKt;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\t\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "SAVE_DEBOUNCE_MS",
        "",
        "settings-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final SAVE_DEBOUNCE_MS:J = 0xc8L
