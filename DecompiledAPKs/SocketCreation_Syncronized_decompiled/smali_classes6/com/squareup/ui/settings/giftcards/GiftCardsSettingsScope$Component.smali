.class public interface abstract Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScope.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract addDesignsSettingsCoordinator()Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;
.end method

.method public abstract cropCustomDesignSettingsCoordinator()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;
.end method

.method public abstract giftCardsSettingsCoordinator()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;
.end method

.method public abstract viewDesignsSettingsCoordinator()Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;
.end method
