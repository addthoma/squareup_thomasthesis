.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;
.super Ljava/lang/Object;
.source "GiftCardsSettingsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ")",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;"
        }
    .end annotation

    .line 58
    new-instance v6, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->newInstance(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator_Factory;->get()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method
