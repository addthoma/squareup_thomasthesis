.class final Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$3;
.super Ljava/lang/Object;
.source "EGiftCardImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->uploadImage(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Landroid/graphics/Bitmap;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002 \u0004*.\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
        "kotlin.jvm.PlatformType",
        "it",
        "Ljava/io/File;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$3;->this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/io/File;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$3;->this$0:Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->access$getGiftCardServiceHelper$p(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;)Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCardServiceHelper;->uploadEGiftCardImage(Ljava/io/File;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$uploadImage$3;->apply(Ljava/io/File;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
