.class public interface abstract Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;
.super Ljava/lang/Object;
.source "AddDesignsSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\u0006H&J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH&J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0010\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0008\u0010\u0011\u001a\u00020\u0006H&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;",
        "",
        "addDesignsScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        "addTheme",
        "",
        "theme",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "exitAddDesignsScreen",
        "gridColumnCount",
        "Lio/reactivex/Single;",
        "",
        "removeTheme",
        "saveAddThemes",
        "oldState",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "showUploadCustom",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addDesignsScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract addTheme(Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
.end method

.method public abstract exitAddDesignsScreen()V
.end method

.method public abstract gridColumnCount()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeTheme(Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
.end method

.method public abstract saveAddThemes(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)V
.end method

.method public abstract showUploadCustom()V
.end method
