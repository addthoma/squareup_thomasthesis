.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;->invoke(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;)V
    .locals 5

    .line 145
    instance-of p1, p1, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult$Success;

    if-eqz p1, :cond_0

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    const/4 v0, 0x0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->setCustomImageUri(Landroid/net/Uri;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getFlow$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lflow/Flow;

    move-result-object p1

    .line 149
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    .line 150
    sget-object v1, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->Companion:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    .line 151
    const-class v4, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 152
    const-class v4, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;

    aput-object v4, v2, v3

    .line 148
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getThemesToBeAdded$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 157
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getFlow$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lflow/Flow;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;->Companion:Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
