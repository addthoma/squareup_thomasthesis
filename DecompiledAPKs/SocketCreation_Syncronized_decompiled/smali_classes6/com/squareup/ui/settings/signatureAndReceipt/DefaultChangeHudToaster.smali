.class public Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;
.super Ljava/lang/Object;
.source "DefaultChangeHudToaster.java"

# interfaces
.implements Lcom/squareup/tenderpayment/ChangeHudToaster;


# instance fields
.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private message:Ljava/lang/CharSequence;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final res:Lcom/squareup/util/Res;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/papersignature/PaperSignatureSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->res:Lcom/squareup/util/Res;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    return-void
.end method

.method private fastCheckoutEnabled()Z
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v0

    return v0
.end method

.method private prepareChangeToast(Lcom/squareup/payment/tender/BaseTender$ReturnsChange;)V
    .locals 3

    .line 89
    invoke-interface {p1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/payment/R$string;->change_hud_amount_change:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 91
    invoke-interface {p1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/payment/R$string;->change_hud_out_of_total:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 94
    invoke-interface {p1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "total"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    goto :goto_0

    .line 97
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/payment/R$string;->change_hud_no_change:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    const/4 p1, 0x0

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    :goto_0
    return-void
.end method

.method private preparePaymentCompleteToast()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/payment/R$string;->change_hud_payment_complete:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    const/4 v0, 0x0

    .line 104
    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 84
    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    .line 85
    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    return-void
.end method

.method public prepare(Lcom/squareup/payment/CompletedPayment;)V
    .locals 1

    .line 51
    invoke-interface {p1}, Lcom/squareup/payment/CompletedPayment;->isLocalPayment()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 52
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->fastCheckoutEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 54
    :cond_1
    invoke-interface {p1}, Lcom/squareup/payment/CompletedPayment;->requireLastAddedTender()Ljava/lang/Object;

    move-result-object p1

    .line 55
    instance-of v0, p1, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    if-eqz v0, :cond_2

    .line 56
    check-cast p1, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->prepareChangeToast(Lcom/squareup/payment/tender/BaseTender$ReturnsChange;)V

    goto :goto_0

    .line 57
    :cond_2
    instance-of p1, p1, Lcom/squareup/payment/tender/OtherTender;

    if-eqz p1, :cond_3

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->preparePaymentCompleteToast()V

    :cond_3
    :goto_0
    return-void
.end method

.method public prepareRemainingBalanceToast(Ljava/lang/String;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    const/4 p1, 0x0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    return-void
.end method

.method public toastIfAvailable()V
    .locals 4

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    const/4 v0, 0x0

    .line 74
    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->title:Ljava/lang/CharSequence;

    .line 75
    iput-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/DefaultChangeHudToaster;->message:Ljava/lang/CharSequence;

    :cond_0
    return-void
.end method
