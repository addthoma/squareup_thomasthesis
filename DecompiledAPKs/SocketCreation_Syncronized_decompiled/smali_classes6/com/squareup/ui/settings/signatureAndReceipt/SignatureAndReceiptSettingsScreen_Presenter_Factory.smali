.class public final Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "SignatureAndReceiptSettingsScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final sidebarRefresherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;"
        }
    .end annotation

    .line 81
    new-instance v11, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;"
        }
    .end annotation

    .line 90
    new-instance v11, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;
    .locals 11

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/settings/SidebarRefresher;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
