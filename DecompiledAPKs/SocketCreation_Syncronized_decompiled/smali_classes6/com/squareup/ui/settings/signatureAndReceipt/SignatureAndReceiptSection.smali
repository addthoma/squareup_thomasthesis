.class public Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;
.super Lcom/squareup/applet/AppletSection;
.source "SignatureAndReceiptSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$Access;,
        Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;
    }
.end annotation


# static fields
.field public static RECEIPT_ID:I

.field public static SIGNATURE_AND_RECEIPT_ID:I

.field public static SIGNATURE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    sget v0, Lcom/squareup/settingsapplet/R$string;->signature_title:I

    sput v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->SIGNATURE_ID:I

    .line 25
    sget v0, Lcom/squareup/registerlib/R$string;->receipt_title:I

    sput v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->RECEIPT_ID:I

    .line 26
    sget v0, Lcom/squareup/settingsapplet/R$string;->signature_and_receipt_title:I

    sput v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->SIGNATURE_AND_RECEIPT_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$Access;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$Access;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method

.method static getSectionNameResId(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)I
    .locals 0

    .line 44
    invoke-interface {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->canSkipReceiptScreen()Z

    move-result p1

    .line 45
    invoke-static {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->shouldShowSignatureSettings(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 48
    sget p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->SIGNATURE_AND_RECEIPT_ID:I

    return p0

    :cond_0
    if-eqz p0, :cond_1

    .line 50
    sget p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->SIGNATURE_ID:I

    return p0

    :cond_1
    if-eqz p1, :cond_2

    .line 52
    sget p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->RECEIPT_ID:I

    return p0

    .line 55
    :cond_2
    sget p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->RECEIPT_ID:I

    return p0
.end method

.method static shouldShowSignatureSettings(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 2

    .line 33
    sget-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;

    return-object v0
.end method
