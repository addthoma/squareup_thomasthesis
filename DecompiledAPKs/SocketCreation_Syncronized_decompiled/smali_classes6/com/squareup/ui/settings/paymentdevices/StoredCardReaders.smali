.class public Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;
.super Ljava/lang/Object;
.source "StoredCardReaders.java"


# instance fields
.field private final cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

.field private final savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    .line 43
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 44
    invoke-interface {p2, p1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$s90lDUTHScBJf_CN-ASVKiMYRHY;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$s90lDUTHScBJf_CN-ASVKiMYRHY;-><init>(Lcom/squareup/settings/LocalSetting;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method private lookupLastConnectionSuccessUtcMillis(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/SavedCardReader;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 216
    :cond_0
    iget-object p1, p1, Lcom/squareup/cardreader/SavedCardReader;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    return-object p1
.end method

.method private lookupSerialNumberLast4(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 203
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SavedCardReader;

    if-eqz v0, :cond_0

    .line 205
    iget-object p1, v0, Lcom/squareup/cardreader/SavedCardReader;->serialNumberLast4:Ljava/lang/String;

    return-object p1

    .line 208
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/LastFourDigits;->getLastDigitsAsSerialNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method addOrUpdateStoredReader(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 6

    .line 128
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->lookupNickname(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->lookupSerialNumberLast4(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    .line 137
    iget-object v4, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/SavedCardReader;

    if-eqz v4, :cond_0

    .line 140
    invoke-virtual {v4}, Lcom/squareup/cardreader/SavedCardReader;->buildUpon()Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v4

    goto :goto_0

    .line 142
    :cond_0
    new-instance v4, Lcom/squareup/cardreader/SavedCardReader$Builder;

    invoke-direct {v4}, Lcom/squareup/cardreader/SavedCardReader$Builder;-><init>()V

    .line 143
    invoke-virtual {v4, v2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->macAddress(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    .line 146
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->firmwareVersion(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    .line 151
    :cond_1
    invoke-virtual {v4, v0}, Lcom/squareup/cardreader/SavedCardReader$Builder;->name(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p1

    .line 152
    invoke-virtual {p1, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->serialNumberLast4(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 153
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/SavedCardReader$Builder;->isBluetoothClassic(Z)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p1

    .line 154
    invoke-virtual {p1, v3}, Lcom/squareup/cardreader/SavedCardReader$Builder;->hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->build()Lcom/squareup/cardreader/SavedCardReader;

    move-result-object p1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 157
    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void

    .line 134
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Can\'t store a connected audio reader"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method addOrUpdateStoredWirelessReader(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 4

    .line 60
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 61
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    sget-object v2, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getDefaultCardReaderName(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/LastFourDigits;->getLastDigitsAsSerialNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    .line 66
    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 67
    invoke-virtual {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/SavedCardReader;

    if-eqz v3, :cond_1

    return-void

    .line 72
    :cond_1
    new-instance v3, Lcom/squareup/cardreader/SavedCardReader$Builder;

    invoke-direct {v3}, Lcom/squareup/cardreader/SavedCardReader$Builder;-><init>()V

    .line 73
    invoke-virtual {v3, v0}, Lcom/squareup/cardreader/SavedCardReader$Builder;->name(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->serialNumberLast4(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->macAddress(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->isBluetoothClassic(Z)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/squareup/cardreader/SavedCardReader$Builder;->build()Lcom/squareup/cardreader/SavedCardReader;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 80
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public getSavedCardReaders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public lookupNickname(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/SavedCardReader;

    if-eqz p1, :cond_0

    .line 195
    iget-object p1, p1, Lcom/squareup/cardreader/SavedCardReader;->name:Ljava/lang/String;

    return-object p1

    .line 198
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getDefaultCardReaderName(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method removeStoredReader(Ljava/lang/String;)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 188
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method savedCardReaders()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method updateFirmwareVersionIfDifferent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SavedCardReader;

    if-nez v0, :cond_0

    return-void

    .line 102
    :cond_0
    iget-object v1, v0, Lcom/squareup/cardreader/SavedCardReader;->firmwareVersion:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v1

    if-nez v1, :cond_2

    if-nez p2, :cond_1

    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cardreader/SavedCardReader;->buildUpon()Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->firmwareVersion(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p2

    .line 108
    invoke-virtual {p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->build()Lcom/squareup/cardreader/SavedCardReader;

    move-result-object p2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 110
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method updateHardwareSerialNumberIfNull(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SavedCardReader;

    if-eqz v0, :cond_1

    .line 116
    iget-object v1, v0, Lcom/squareup/cardreader/SavedCardReader;->hardwareSerialNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 119
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardreader/SavedCardReader;->buildUpon()Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 120
    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p2

    .line 121
    invoke-virtual {p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->build()Lcom/squareup/cardreader/SavedCardReader;

    move-result-object p2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 123
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method updateLastConnectionSuccess(Ljava/lang/String;J)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SavedCardReader;

    if-nez v0, :cond_0

    return-void

    .line 89
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardreader/SavedCardReader;->buildUpon()Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    .line 90
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->lastConnectionSuccessUtcMillis(Ljava/lang/Long;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p2

    .line 91
    invoke-virtual {p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->build()Lcom/squareup/cardreader/SavedCardReader;

    move-result-object p2

    .line 92
    iget-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map;

    .line 93
    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, p3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method updateStoredReaderName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 166
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 173
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/SavedCardReader;

    .line 174
    iget-object v2, v1, Lcom/squareup/cardreader/SavedCardReader;->name:Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p1, 0x0

    return p1

    .line 178
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/cardreader/SavedCardReader;->buildUpon()Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v1

    .line 179
    invoke-virtual {v1, p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->name(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object p2

    .line 180
    invoke-virtual {p2}, Lcom/squareup/cardreader/SavedCardReader$Builder;->build()Lcom/squareup/cardreader/SavedCardReader;

    move-result-object p2

    .line 181
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaderRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    .line 167
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot find a SavedCardReader with address "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
