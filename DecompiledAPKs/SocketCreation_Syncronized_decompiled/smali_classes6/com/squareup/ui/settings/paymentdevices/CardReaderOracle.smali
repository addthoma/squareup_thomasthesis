.class public Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
.super Ljava/lang/Object;
.source "CardReaderOracle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;,
        Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;,
        Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;
    }
.end annotation


# instance fields
.field private final branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final clock:Lcom/squareup/util/Clock;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

.field private final internalCardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

.field private final internalFirmwareUpdateListener:Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

.field private final internalWirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

.field private final mainScheduler:Lrx/Scheduler;

.field private final readerBleStatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;"
        }
    .end annotation
.end field

.field private final readerFirmwareUpdatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;"
        }
    .end annotation
.end field

.field private readerStates:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;"
        }
    .end annotation
.end field

.field private readerStatesSubscription:Lrx/Subscription;

.field private final readersWithFwupCheckInProgress:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/BranRemoteCardReader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/util/Clock;Lrx/Scheduler;)V
    .locals 1
    .param p8    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readersWithFwupCheckInProgress:Ljava/util/Set;

    .line 91
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 92
    invoke-static {}, Lrx/subscriptions/Subscriptions;->unsubscribed()Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStatesSubscription:Lrx/Subscription;

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 100
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    .line 101
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 102
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

    .line 103
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    .line 104
    iput-object p8, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->mainScheduler:Lrx/Scheduler;

    .line 106
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalCardReaderAttachListener;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$1;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalCardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    .line 107
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalFirmwareUpdateListener;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$1;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalFirmwareUpdateListener:Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 108
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$InternalWirelessPairingEventListener;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle$1;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalWirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    .line 109
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    .line 110
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerBleStatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 111
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    .line 112
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p1, 0x0

    .line 113
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerFirmwareUpdatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 114
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 115
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerBleStatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Ljava/util/Set;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readersWithFwupCheckInProgress:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerFirmwareUpdatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;II)V
    .locals 0

    .line 74
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReaderForFwupProgress(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;II)V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    return-object p0
.end method

.method static synthetic access$900(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    .line 74
    invoke-static {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    return-void
.end method

.method private static applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ")V"
        }
    .end annotation

    .line 224
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 227
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 228
    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 229
    invoke-static {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->createReaderStateByMergingInfoFrom(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    .line 228
    invoke-interface {p0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method private interpretCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_0

    .line 247
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 251
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readersWithFwupCheckInProgress:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_CHECKING_SERVER_FOR_FWUP:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 259
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 263
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 264
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateInProgress()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 265
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 268
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_TAMPERED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    if-ne v0, v1, :cond_5

    .line 269
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TAMPERED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 272
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getSecureSessionState()Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->DENIED:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    if-ne v0, v1, :cond_6

    .line 273
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 276
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v0

    if-nez v0, :cond_7

    .line 277
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_ESTABLISHING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 280
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v0

    if-nez v0, :cond_8

    .line 281
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TMS_INVALID:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 284
    :cond_8
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSystemInfoAcquired()Z

    move-result v0

    if-nez v0, :cond_9

    .line 285
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_COMMS_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 289
    :cond_9
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq p2, v0, :cond_a

    .line 290
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 293
    :cond_a
    invoke-static {p1}, Lcom/squareup/cardreader/BatteryLevel;->isBatteryLow(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result p2

    if-eqz p2, :cond_b

    .line 294
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 297
    :cond_b
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isBatteryDead()Z

    move-result p1

    if-eqz p1, :cond_c

    .line 298
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_DEAD:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1

    .line 301
    :cond_c
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p1
.end method

.method private startEventOverrideForReaderForFwupProgress(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;II)V
    .locals 15

    move-object v0, p0

    .line 316
    new-instance v1, Ljava/util/LinkedHashMap;

    iget-object v2, v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 317
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 318
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    new-instance v14, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    .line 319
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v3, v14

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move/from16 v12, p3

    move/from16 v13, p4

    invoke-direct/range {v3 .. v13}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;II)V

    .line 318
    invoke-interface {v1, v2, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v2, v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStatesSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method getInternalFirmwareUpdateListener()Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalFirmwareUpdateListener:Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    return-object v0
.end method

.method public getPairingEventListener()Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalWirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    return-object v0
.end method

.method public initialize()V
    .locals 9

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalCardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->internalFirmwareUpdateListener:Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->addFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;

    .line 122
    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracle$FJUzD9g9XaCkstsR9syG9IHfT1M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracle$FJUzD9g9XaCkstsR9syG9IHfT1M;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    .line 123
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderInfos()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracle$abedusUMNn4QUQSfSEbE72ZIxJA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracle$abedusUMNn4QUQSfSEbE72ZIxJA;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/BranRemoteCardReader;->onConnectionStatusChangedRx1()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    .line 138
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->savedCardReaders()Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerBleStatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 139
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderInfos()Lrx/Observable;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerFirmwareUpdatesRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 141
    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v7, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v7}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v7

    new-instance v8, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracle$qEzqxW8SimHr8ZRMWSgXvChrvAU;

    invoke-direct {v8, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReaderOracle$qEzqxW8SimHr8ZRMWSgXvChrvAU;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    .line 137
    invoke-static/range {v1 .. v8}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->mainScheduler:Lrx/Scheduler;

    .line 206
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 207
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStatesSubscription:Lrx/Subscription;

    return-void
.end method

.method public synthetic lambda$initialize$0$CardReaderOracle(Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;)V
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    .line 124
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    .line 123
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->updateLastConnectionSuccess(Ljava/lang/String;J)V

    return-void
.end method

.method public synthetic lambda$initialize$1$CardReaderOracle(Ljava/util/Collection;)V
    .locals 4

    .line 126
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderInfo;

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 128
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v3

    .line 127
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->updateFirmwareVersionIfDifferent(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 130
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->updateHardwareSerialNumberIfNull(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$initialize$2$CardReaderOracle(Ljava/lang/Boolean;Ljava/util/Map;Ljava/util/Map;Ljava/util/Collection;Ljava/util/Map;Ljava/lang/Integer;Lcom/squareup/connectivity/InternetState;)Ljava/util/Collection;
    .locals 14

    move-object v0, p0

    .line 144
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 153
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    iget-object v2, v0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

    invoke-virtual {v2}, Lcom/squareup/cardreader/BranRemoteCardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v7

    .line 155
    new-instance v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 156
    invoke-virtual {v7}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    const/4 v9, 0x0

    .line 157
    invoke-virtual {v7}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v10

    .line 158
    invoke-virtual {v7}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v11

    move-object v3, v2

    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-static {v1, v2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 165
    :cond_0
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/SavedCardReader;

    .line 166
    iget-boolean v4, v3, Lcom/squareup/cardreader/SavedCardReader;->isBluetoothClassic:Z

    if-eqz v4, :cond_1

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLUETOOTH_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    goto :goto_1

    :cond_1
    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    :goto_1
    move-object v10, v4

    .line 169
    new-instance v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v6, v3, Lcom/squareup/cardreader/SavedCardReader;->name:Ljava/lang/String;

    iget-object v7, v3, Lcom/squareup/cardreader/SavedCardReader;->serialNumberLast4:Ljava/lang/String;

    iget-object v8, v3, Lcom/squareup/cardreader/SavedCardReader;->macAddress:Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v11, v3, Lcom/squareup/cardreader/SavedCardReader;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    iget-object v12, v3, Lcom/squareup/cardreader/SavedCardReader;->firmwareVersion:Ljava/lang/String;

    iget-object v13, v3, Lcom/squareup/cardreader/SavedCardReader;->hardwareSerialNumber:Ljava/lang/String;

    move-object v5, v4

    invoke-direct/range {v5 .. v13}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v4}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    goto :goto_0

    .line 182
    :cond_2
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    goto :goto_2

    .line 190
    :cond_3
    invoke-interface/range {p4 .. p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Lcom/squareup/cardreader/CardReaderInfo;

    .line 191
    new-instance v3, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 192
    invoke-virtual {v8}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v13, p7

    .line 193
    invoke-direct {p0, v8, v13}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->interpretCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    move-result-object v9

    const/4 v10, 0x0

    .line 194
    invoke-virtual {v8}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v12

    move-object v4, v3

    invoke-direct/range {v4 .. v12}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    goto :goto_3

    .line 201
    :cond_4
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->applyReaderState(Ljava/util/Map;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    goto :goto_4

    .line 204
    :cond_5
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    return-object v1
.end method

.method public readerStates()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;"
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V
    .locals 12

    .line 306
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 307
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 308
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    new-instance v11, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 309
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, v11

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v2 .. v10}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-interface {v0, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 325
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 326
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 327
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerOverrideEventsRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
