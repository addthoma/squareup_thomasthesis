.class public final enum Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;
.super Ljava/lang/Enum;
.source "ReaderState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/ReaderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Level"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

.field public static final enum READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

.field public static final enum READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

.field public static final enum READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

.field public static final enum READER_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

.field public static final enum READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

.field public static final enum READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 30
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v1, 0x0

    const-string v2, "READER_UPDATING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    .line 44
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v2, 0x1

    const-string v3, "READER_FAILED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    .line 48
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v3, 0x2

    const-string v4, "READER_PERMISSION_MISSING"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    .line 54
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v4, 0x3

    const-string v5, "READER_CONNECTING"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    .line 61
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v5, 0x4

    const-string v6, "READER_READY"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    .line 66
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v6, 0x5

    const-string v7, "READER_DISCONNECTED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    .line 24
    sget-object v7, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    return-object v0
.end method
