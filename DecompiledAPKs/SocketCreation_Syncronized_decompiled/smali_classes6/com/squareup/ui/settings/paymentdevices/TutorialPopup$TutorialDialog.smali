.class Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;
.super Lcom/squareup/dialog/GlassDialog;
.source "TutorialPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/TutorialPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TutorialDialog"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;Z)V
    .locals 5

    .line 78
    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 79
    invoke-interface {p2}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;->getLayoutResId()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->setContentView(I)V

    .line 80
    invoke-virtual {p0, p4}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->setCancelable(Z)V

    .line 81
    invoke-virtual {p0, p4}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->setCanceledOnTouchOutside(Z)V

    .line 82
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$TutorialPopup$TutorialDialog$feS1IMOnm7ZR7r3yPwso7RsQv1A;

    invoke-direct {p1, p3}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$TutorialPopup$TutorialDialog$feS1IMOnm7ZR7r3yPwso7RsQv1A;-><init>(Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p1

    const/4 p4, -0x1

    iput p4, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    .line 87
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_content:I

    .line 88
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 89
    sget v1, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_primary:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 90
    sget v2, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_secondary:I

    .line 91
    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 95
    sget v4, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_title:I

    .line 96
    invoke-static {p1, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    .line 97
    invoke-virtual {p1, v3}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;->getContent(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 102
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_1
    invoke-interface {p2}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;->getPrimaryButton()I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(I)V

    .line 106
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$1;

    invoke-direct {p1, p0, p3}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;)V

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    invoke-interface {p2}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;->getSecondaryButton()I

    move-result p1

    if-ne p1, p4, :cond_2

    const/16 p1, 0x8

    .line 114
    invoke-virtual {v2, p1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v2, p1}, Landroid/widget/Button;->setText(I)V

    .line 117
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$2;

    invoke-direct {p1, p0, p3}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;)V

    invoke-virtual {v2, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;ZLcom/squareup/ui/settings/paymentdevices/TutorialPopup$1;)V
    .locals 0

    .line 74
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$TutorialDialog;-><init>(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;Z)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;->dismiss()V

    return-void
.end method
