.class public Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
.super Ljava/lang/Object;
.source "BleGattConnectionEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public characteristic:Ljava/lang/String;

.field public descriptor:Ljava/lang/String;

.field public gattStatus:I

.field public mtu:I

.field public final name:Ljava/lang/String;

.field public newState:I


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/GattConnectionEventName;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->name:Ljava/lang/String;

    const/4 p1, -0x1

    .line 43
    iput p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->newState:I

    .line 44
    iput p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->mtu:I

    .line 45
    iput p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->gattStatus:I

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;)V

    return-object v0
.end method

.method public characteristic(Ljava/util/UUID;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 53
    :cond_0
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->characteristic:Ljava/lang/String;

    return-object p0
.end method

.method public descriptor(Ljava/util/UUID;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 58
    :cond_0
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->descriptor:Ljava/lang/String;

    return-object p0
.end method

.method public gattStatus(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
    .locals 0

    .line 73
    iput p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->gattStatus:I

    return-object p0
.end method

.method public mtu(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
    .locals 0

    .line 68
    iput p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->mtu:I

    return-object p0
.end method

.method public newState(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;
    .locals 0

    .line 63
    iput p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleGattConnectionEvent$Builder;->newState:I

    return-object p0
.end method
