.class final Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails$1;
.super Ljava/lang/Object;
.source "PairingConfirmationPopupDetails.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;
    .locals 2

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    .line 51
    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;
    .locals 0

    .line 55
    new-array p1, p1, [Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails$1;->newArray(I)[Lcom/squareup/ui/settings/paymentdevices/pairing/PairingConfirmationPopupDetails;

    move-result-object p1

    return-object p1
.end method
