.class public final Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;
.super Ljava/lang/Object;
.source "CardReadersScreenView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderMessagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;",
            ">;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAccountStatusSettings(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static injectCardReaderMessages(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;Ljava/lang/Object;)V
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->injectCardReaderMessages(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->injectAccountStatusSettings(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V

    return-void
.end method
