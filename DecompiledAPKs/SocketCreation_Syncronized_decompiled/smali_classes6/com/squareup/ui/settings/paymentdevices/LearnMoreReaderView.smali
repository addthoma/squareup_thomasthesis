.class public Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;
.super Landroid/widget/LinearLayout;
.source "LearnMoreReaderView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private checkCompatibility:Landroid/view/View;

.field private connectReader:Landroid/view/View;

.field private contactless:Landroid/view/View;

.field private magstripe:Landroid/view/View;

.field presenter:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private videoTutorialLinkR12:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Component;->inject(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->onOrderReaderClicked()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 112
    sget v0, Lcom/squareup/settingsapplet/R$id;->contactless_learn_more_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->contactless:Landroid/view/View;

    .line 113
    sget v0, Lcom/squareup/settingsapplet/R$id;->magstripe_learn_more_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->magstripe:Landroid/view/View;

    .line 114
    sget v0, Lcom/squareup/settingsapplet/R$id;->tutorial_video_link_r12:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->videoTutorialLinkR12:Landroid/view/View;

    .line 115
    sget v0, Lcom/squareup/settingsapplet/R$id;->connect_reader_wirelessly:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->connectReader:Landroid/view/View;

    .line 116
    sget v0, Lcom/squareup/settingsapplet/R$id;->check_compatibility_reader_button:I

    .line 117
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->checkCompatibility:Landroid/view/View;

    return-void
.end method

.method private onOrderReaderClicked()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->presenter:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->onOrderReaderClicked()V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 121
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->presenter:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 75
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->bindViews()V

    .line 40
    sget v0, Lcom/squareup/settingsapplet/R$id;->connect_reader_wirelessly:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    sget v0, Lcom/squareup/settingsapplet/R$id;->check_compatibility_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    .line 47
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->videoTutorialLinkR12:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$3;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    .line 53
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    sget v0, Lcom/squareup/settingsapplet/R$id;->order_contactless_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$4;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    sget v0, Lcom/squareup/settingsapplet/R$id;->order_magstripe_reader_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$5;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    .line 65
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->presenter:Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setCheckCompatibilityVisible(Z)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->checkCompatibility:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setConnectReaderVisible(Z)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->connectReader:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setVideoTutorialLinkVisible(Z)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->videoTutorialLinkR12:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public show(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->contactless:Landroid/view/View;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->magstripe:Landroid/view/View;

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showBluetoothRequiredDialog()V
    .locals 2

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView$6;-><init>(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderView;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog;->show(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;)V

    return-void
.end method
