.class public final Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "CardReaderDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Presenter;
    }
.end annotation


# static fields
.field private static final AUDIO_COMMS_LONG_CONNECTION_INTERVAL_SECONDS:J = 0xfL


# instance fields
.field private final initialReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;->initialReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen;->initialReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-object p0
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 78
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 267
    sget v0, Lcom/squareup/settingsapplet/R$layout;->card_reader_detail_screen_view:I

    return v0
.end method
