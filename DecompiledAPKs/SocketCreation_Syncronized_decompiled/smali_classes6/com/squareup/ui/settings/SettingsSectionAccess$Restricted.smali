.class public Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;
.super Lcom/squareup/ui/settings/SettingsSectionAccess;
.source "SettingsSectionAccess.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsSectionAccess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Restricted"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSettingsSectionAccess.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SettingsSectionAccess.kt\ncom/squareup/ui/settings/SettingsSectionAccess$Restricted\n*L\n1#1,40:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u0003\"\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0007H\u0016R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;",
        "Lcom/squareup/ui/settings/SettingsSectionAccess;",
        "alternatePermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "([Lcom/squareup/permissions/Permission;)V",
        "acceptablePermissions",
        "",
        "getPermissions",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final acceptablePermissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lcom/squareup/permissions/Permission;)V
    .locals 2

    const-string v0, "alternatePermissions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/SettingsSectionAccess;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 29
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 30
    sget-object v1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 32
    iput-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;->acceptablePermissions:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;->acceptablePermissions:Ljava/util/Set;

    return-object v0
.end method
