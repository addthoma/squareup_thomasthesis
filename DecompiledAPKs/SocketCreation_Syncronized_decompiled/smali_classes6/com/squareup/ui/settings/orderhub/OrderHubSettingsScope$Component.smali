.class public interface abstract Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;
.super Ljava/lang/Object;
.source "OrderHubSettingsScope.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;",
        "",
        "orderHubNotificationSettingsCoordinator",
        "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;",
        "orderHubPrintingSettingsCoordinator",
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;",
        "orderHubQuickActionsSettingsCoordinator",
        "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsCoordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract orderHubNotificationSettingsCoordinator()Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;
.end method

.method public abstract orderHubPrintingSettingsCoordinator()Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;
.end method

.method public abstract orderHubQuickActionsSettingsCoordinator()Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsCoordinator;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;
.end method
