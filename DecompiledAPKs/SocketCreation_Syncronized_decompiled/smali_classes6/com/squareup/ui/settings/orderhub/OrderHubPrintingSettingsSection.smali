.class public final Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;
.super Lcom/squareup/applet/AppletSection;
.source "OrderHubPrintingSettingsSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;,
        Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Access;,
        Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u00082\u00020\u0001:\u0003\u0007\u0008\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;",
        "Lcom/squareup/applet/AppletSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen;",
        "Access",
        "Companion",
        "ListEntry",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Companion;

.field public static final TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;->Companion:Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Companion;

    .line 44
    sget v0, Lcom/squareup/settingsapplet/R$string;->orderhub_printing_settings_section_label:I

    sput v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Access;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$Access;-><init>(Lcom/squareup/settings/server/Features;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection;->getInitialScreen()Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen;->Companion:Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsScreen;

    move-result-object v0

    return-object v0
.end method
