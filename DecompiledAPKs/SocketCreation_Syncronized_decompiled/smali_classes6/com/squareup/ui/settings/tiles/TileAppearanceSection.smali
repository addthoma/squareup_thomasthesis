.class public Lcom/squareup/ui/settings/tiles/TileAppearanceSection;
.super Lcom/squareup/applet/AppletSection;
.source "TileAppearanceSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tiles/TileAppearanceSection$Access;,
        Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    sget v0, Lcom/squareup/settingsapplet/R$string;->item_appeareance_settings_label:I

    sput v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$Access;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$Access;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen;->INSTANCE:Lcom/squareup/ui/settings/tiles/TileAppearanceScreen;

    return-object v0
.end method
