.class public Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
.super Ljava/lang/Object;
.source "TileAppearanceSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY:Ljava/lang/String; = "COGS_CONFIG_CACHE"


# instance fields
.field private final cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/shared/catalog/models/CatalogConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final isTablet:Z

.field private final nonNullAndDebounced:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/shared/catalog/models/CatalogConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$9YAjvXoO1fcG12958mZRlvHMvS0;->INSTANCE:Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$9YAjvXoO1fcG12958mZRlvHMvS0;

    .line 38
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->nonNullAndDebounced:Lrx/Observable;

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cogs:Lcom/squareup/cogs/Cogs;

    .line 47
    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTablet:Z

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->nonNullAndDebounced:Lrx/Observable;

    const/4 p2, 0x1

    .line 52
    invoke-virtual {p1, p2}, Lrx/Observable;->skip(I)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$mLnLhsJThvbGsYUQCmjpuNwjf2I;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$mLnLhsJThvbGsYUQCmjpuNwjf2I;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    .line 56
    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method private isTextTileEnabled()Z
    .locals 2

    .line 153
    invoke-direct {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->requireCachedConfig()Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->getPageLayout()Lcom/squareup/api/items/PageLayout;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/PageLayout;->TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$mLnLhsJThvbGsYUQCmjpuNwjf2I(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->writeToCogs(Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/shared/catalog/models/CatalogConfiguration;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 38
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$observeTileType$1(Lcom/squareup/shared/catalog/models/CatalogConfiguration;)Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->getPageLayout()Lcom/squareup/api/items/PageLayout;

    move-result-object p0

    sget-object v0, Lcom/squareup/api/items/PageLayout;->TEXT_TILES_3X9:Lcom/squareup/api/items/PageLayout;

    if-ne p0, v0, :cond_0

    sget-object p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->TEXT:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->IMAGE:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    :goto_0
    return-object p0
.end method

.method private requireCachedConfig()Lcom/squareup/shared/catalog/models/CatalogConfiguration;
    .locals 3

    .line 157
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "Must not be called before initialized by JailKeeper"

    .line 159
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-object v0
.end method

.method private writeToCogs(Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V
    .locals 2

    const-string v0, "catalogConfiguration"

    .line 164
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$2;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    .line 175
    invoke-virtual {p1}, Lrx/Single;->subscribe()Lrx/Subscription;

    return-void
.end method


# virtual methods
.method public getBundler()Lmortar/bundler/Bundler;
    .locals 1

    .line 68
    new-instance v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$1;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    return-object v0
.end method

.method public isTextTileAllowed()Z
    .locals 1

    .line 104
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTablet:Z

    return v0
.end method

.method public isTextTileMode()Z
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$loadTask$2$TileAppearanceSettings(Lcom/squareup/shared/catalog/models/CatalogConfiguration;)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "catalogConfiguration"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public loadTask()Lrx/Completable;
    .locals 2

    .line 139
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-static {}, Lrx/Completable;->complete()Lrx/Completable;

    move-result-object v0

    return-object v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cogs:Lcom/squareup/cogs/Cogs;

    sget-object v1, Lcom/squareup/ui/settings/tiles/-$$Lambda$8qNBySWbpALDZt9gIK2Di27DrnY;->INSTANCE:Lcom/squareup/ui/settings/tiles/-$$Lambda$8qNBySWbpALDZt9gIK2Di27DrnY;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$k8q_nkXGFuo8fFx98iKNFCx8AY4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$k8q_nkXGFuo8fFx98iKNFCx8AY4;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    .line 145
    invoke-virtual {v0, v1}, Lrx/Single;->doOnSuccess(Lrx/functions/Action1;)Lrx/Single;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lrx/Single;->toCompletable()Lrx/Completable;

    move-result-object v0

    return-object v0
.end method

.method public observeTileType()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;",
            ">;"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->nonNullAndDebounced:Lrx/Observable;

    sget-object v1, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$5IQYw-UNwz24Fum8TtWcobHN3Xw;->INSTANCE:Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceSettings$5IQYw-UNwz24Fum8TtWcobHN3Xw;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;)V
    .locals 3

    const-string v0, "type"

    .line 120
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->cacheRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->requireCachedConfig()Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->TEXT:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    if-ne p1, v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogConfiguration;->from(Lcom/squareup/shared/catalog/models/CatalogConfiguration;Z)Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
