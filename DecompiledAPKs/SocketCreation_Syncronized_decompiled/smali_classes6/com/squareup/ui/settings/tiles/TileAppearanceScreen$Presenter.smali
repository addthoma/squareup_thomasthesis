.class public Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "TileAppearanceScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

.field private final settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;Lcom/squareup/ui/settings/SettingsAppletScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/tiles/TileAppearanceSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$0$TileAppearanceScreen$Presenter(Lcom/squareup/ui/settings/tiles/TileAppearanceView;)Lrx/Subscription;
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->observeTileType()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/tiles/-$$Lambda$pTY5rBITKXX79ybiK1AAvvVXMR0;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/tiles/-$$Lambda$pTY5rBITKXX79ybiK1AAvvVXMR0;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceView;)V

    .line 68
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method maybeConfirmChange(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;)V
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->IMAGE:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->confirmTileAppearanceChangedToImage()V

    goto :goto_0

    .line 87
    :cond_0
    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->TEXT:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result p1

    if-nez p1, :cond_1

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->TEXT:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->set(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->textTileSelected()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/tiles/TileAppearanceView;

    .line 66
    new-instance v0, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceScreen$Presenter$qKxG9fpEsDwuK6CeSrAxITyfVhg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/tiles/-$$Lambda$TileAppearanceScreen$Presenter$qKxG9fpEsDwuK6CeSrAxITyfVhg;-><init>(Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;Lcom/squareup/ui/settings/tiles/TileAppearanceView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected saveSettings()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 72
    const-class v0, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen;

    return-object v0
.end method
