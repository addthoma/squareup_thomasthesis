.class public final Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;
.super Ljava/lang/Object;
.source "TileAppearanceSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;)Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;-><init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;->newInstance(Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;)Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings_Factory;->get()Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-result-object v0

    return-object v0
.end method
