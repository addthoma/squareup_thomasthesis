.class public final Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;
.super Ljava/lang/Object;
.source "PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTenderSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderSettingsManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;)",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;
    .locals 8

    .line 66
    new-instance v7, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->sectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tenderpayment/TenderSettingsManager;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->newInstance(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection_PaymentTypesSettingsCheckoutListEntry_Factory;->get()Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

    move-result-object v0

    return-object v0
.end method
