.class public interface abstract Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;
.super Ljava/lang/Object;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onTenderDragFinished(IZ)V
.end method

.method public abstract onTenderMovedToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
.end method

.method public abstract onTenderMovedToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V
.end method
