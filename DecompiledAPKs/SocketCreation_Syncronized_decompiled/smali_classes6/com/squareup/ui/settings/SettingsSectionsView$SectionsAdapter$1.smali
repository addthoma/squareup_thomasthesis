.class Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SettingsSectionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->bindSignOutButton(Landroid/widget/FrameLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;)V
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter$1;->this$1:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 297
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter$1;->this$1:Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;

    iget-object p1, p1, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object p1, p1, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->onSignOutClicked()V

    return-void
.end method
