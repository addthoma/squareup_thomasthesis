.class public abstract Lcom/squareup/ui/settings/x2/X2SettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "X2SettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# instance fields
.field screenLayoutId:I

.field protected final screenType:Lcom/squareup/x2/settings/ScreenType;


# direct methods
.method protected constructor <init>(Lcom/squareup/x2/settings/ScreenType;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenType:Lcom/squareup/x2/settings/ScreenType;

    const/4 p1, 0x0

    .line 26
    iput p1, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenLayoutId:I

    return-void
.end method


# virtual methods
.method protected baseComponent(Landroid/view/View;)Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;
    .locals 1

    .line 37
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 3

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->baseComponent(Landroid/view/View;)Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->x2SettingsCoordinators()Ljava/util/Map;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenType:Lcom/squareup/x2/settings/ScreenType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    if-eqz p1, :cond_0

    return-object p1

    .line 52
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenType:Lcom/squareup/x2/settings/ScreenType;

    .line 54
    invoke-virtual {v2}, Lcom/squareup/x2/settings/ScreenType;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Missing coordinator configuration for %s in X2SettingsModule"

    .line 53
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public screenLayout()I
    .locals 4

    .line 41
    iget v0, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenLayoutId:I

    if-eqz v0, :cond_0

    return v0

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenType:Lcom/squareup/x2/settings/ScreenType;

    .line 44
    invoke-virtual {v3}, Lcom/squareup/x2/settings/ScreenType;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Missing layout configuration for %s in X2SettingsAppletModule"

    .line 43
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setScreenLayoutId(I)V
    .locals 0

    .line 33
    iput p1, p0, Lcom/squareup/ui/settings/x2/X2SettingsScreen;->screenLayoutId:I

    return-void
.end method
