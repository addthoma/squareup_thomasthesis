.class public Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "TimeTrackingSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;

    .line 36
    sget-object v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;

    .line 37
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 31
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 32
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 33
    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->timeTrackingSettingsCoordinator()Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 23
    sget v0, Lcom/squareup/settingsapplet/R$layout;->time_tracking_settings:I

    return v0
.end method
