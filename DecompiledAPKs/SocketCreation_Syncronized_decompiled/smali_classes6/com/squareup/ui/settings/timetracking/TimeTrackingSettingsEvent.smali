.class public Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "TimeTrackingSettingsEvent.java"


# static fields
.field private static final EVENT_DETAIL_OFF:Ljava/lang/String; = "OFF"

.field private static final EVENT_DETAIL_ON:Ljava/lang/String; = "ON"

.field private static final TIME_TRACKING_EVENT:Ljava/lang/String; = "Employee Management: Time Tracking: Toggle"


# instance fields
.field public final detail:Ljava/lang/String;

.field public final version:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    const-string p1, "v3"

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsEvent;->version:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsEvent;->detail:Ljava/lang/String;

    return-void
.end method

.method public static forTimeTrackingToggle(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 23
    :goto_0
    new-instance v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v2, "Employee Management: Time Tracking: Toggle"

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
