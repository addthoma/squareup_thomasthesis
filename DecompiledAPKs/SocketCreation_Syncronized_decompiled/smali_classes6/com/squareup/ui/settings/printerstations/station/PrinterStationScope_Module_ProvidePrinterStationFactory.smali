.class public final Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;
.super Ljava/lang/Object;
.source "PrinterStationScope_Module_ProvidePrinterStationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/PrinterStation;",
        ">;"
    }
.end annotation


# instance fields
.field private final isNewPrinterStationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;

.field private final printerStationFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStationFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStationFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->module:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->printerStationFactoryProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->isNewPrinterStationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStationFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePrinterStation(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrinterStationFactory;Z)Lcom/squareup/print/PrinterStation;
    .locals 0

    .line 53
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;->providePrinterStation(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrinterStationFactory;Z)Lcom/squareup/print/PrinterStation;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrinterStation;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/PrinterStation;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->module:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStations;

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->printerStationFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStationFactory;

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->isNewPrinterStationProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->providePrinterStation(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrinterStationFactory;Z)Lcom/squareup/print/PrinterStation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvidePrinterStationFactory;->get()Lcom/squareup/print/PrinterStation;

    move-result-object v0

    return-object v0
.end method
