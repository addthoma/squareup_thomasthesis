.class public final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;
.super Ljava/lang/Object;
.source "HardwarePrinterSelectView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->recyclerFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->presenter:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    return-void
.end method

.method public static injectRecyclerFactory(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->recyclerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/recycler/RecyclerFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->injectRecyclerFactory(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/recycler/RecyclerFactory;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V

    return-void
.end method
