.class final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "HardwarePrinterSelectView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->createRecycler()Lcom/squareup/cycler/Recycler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
        "Lcom/squareup/ui/RadioButtonListRow;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
        "Lcom/squareup/ui/RadioButtonListRow;",
        "it",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$1$3$1",
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$$special$$inlined$row$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;",
            "Lcom/squareup/ui/RadioButtonListRow;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lcom/squareup/ui/RadioButtonListRow;

    invoke-direct {v0, p2}, Lcom/squareup/ui/RadioButtonListRow;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 124
    new-instance p2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$createRecycler$$inlined$adopt$lambda$1;Lcom/squareup/cycler/StandardRowSpec$Creator;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
