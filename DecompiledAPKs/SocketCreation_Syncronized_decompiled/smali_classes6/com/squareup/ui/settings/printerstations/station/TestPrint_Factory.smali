.class public final Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;
.super Ljava/lang/Object;
.source "TestPrint_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final printSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final testReceiptPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TestReceiptPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TestReceiptPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->testReceiptPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->ticketPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->resProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TestReceiptPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;"
        }
    .end annotation

    .line 68
    new-instance v9, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/print/payload/TestReceiptPayloadFactory;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/settings/printerstations/station/TestPrint;
    .locals 10

    .line 75
    new-instance v9, Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/printerstations/station/TestPrint;-><init>(Lcom/squareup/print/payload/TestReceiptPayloadFactory;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/printerstations/station/TestPrint;
    .locals 9

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->testReceiptPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/print/payload/TestReceiptPayloadFactory;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/PrintSpooler;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->ticketPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/payload/TicketPayload$Factory;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->newInstance(Lcom/squareup/print/payload/TestReceiptPayloadFactory;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/TestPrint_Factory;->get()Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    move-result-object v0

    return-object v0
.end method
