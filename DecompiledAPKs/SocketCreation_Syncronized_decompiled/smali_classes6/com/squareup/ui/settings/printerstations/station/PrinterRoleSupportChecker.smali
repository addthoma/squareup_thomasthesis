.class public Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;
.super Ljava/lang/Object;
.source "PrinterRoleSupportChecker.java"


# instance fields
.field private final canPrintTextReceipts:Z

.field private final canPrintTextTicketStubs:Z


# direct methods
.method private constructor <init>(ZZ)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTextTicketStubs:Z

    .line 13
    iput-boolean p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTextReceipts:Z

    return-void
.end method

.method public static allRolesSupported()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;-><init>(ZZ)V

    return-object v0
.end method

.method public static rasterOnlyForTicketStubsAndReceipts()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;-><init>(ZZ)V

    return-object v0
.end method


# virtual methods
.method public canPrintReceipts(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTextReceipts:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    iget-boolean p1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public canPrintTicketStubs(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTextTicketStubs:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    iget-boolean p1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
