.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;
.super Lmortar/ViewPresenter;
.source "MerchantProfileBusinessAddressPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;",
        ">;"
    }
.end annotation


# instance fields
.field private hasLoadedState:Z

.field private final monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

.field private final runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

.field private showExitDialog:Z

.field private final state:Lcom/squareup/merchantprofile/MerchantProfileState;


# direct methods
.method constructor <init>(Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    return-void
.end method

.method private onAddressValidationRequired()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->showRequestBusinessAddressDialogScreen()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$MerchantProfileBusinessAddressPresenter(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 38
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 39
    iput-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->showExitDialog:Z

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->onAddressValidationRequired()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 42
    iput-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->showExitDialog:Z

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$MerchantProfileBusinessAddressPresenter(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->setMobileBusinessContent(Z)V

    .line 53
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/address/Address;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->setAddress(Lcom/squareup/address/Address;)V

    const/4 p1, 0x1

    .line 54
    iput-boolean p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->hasLoadedState:Z

    :cond_0
    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->onCloseButtonTapped()V

    const/4 v0, 0x1

    return v0
.end method

.method onCloseButtonTapped()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->showExitDialog:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->onExitEditScreen(Z)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 34
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    .line 36
    invoke-interface {v0}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->addressRequiresValidation()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressPresenter$FqUM8CRFx7w__wQfXhG0gneBhmM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressPresenter$FqUM8CRFx7w__wQfXhG0gneBhmM;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;)V

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 35
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 46
    iget-boolean v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->hasLoadedState:Z

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->businessAddress()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressPresenter$g4jZa2HXqFEeZqHLFAHF0icIO2k;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressPresenter$g4jZa2HXqFEeZqHLFAHF0icIO2k;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method onSave(ZLcom/squareup/address/Address;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->runner:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->onBusinessAddressSave(ZLcom/squareup/address/Address;)V

    return-void
.end method
