.class public final Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory;
.super Ljava/lang/Object;
.source "RequestBusinessAddressDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRequestBusinessAddressDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RequestBusinessAddressDialogScreen.kt\ncom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,101:1\n17#2,2:102\n*E\n*S KotlinDebug\n*F\n+ 1 RequestBusinessAddressDialogScreen.kt\ncom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory\n*L\n66#1,2:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J \u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "runner",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
        "data",
        "Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;)Landroid/app/Dialog;
    .locals 5

    .line 64
    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_dialog_message:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 63
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 66
    invoke-virtual {p3}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;->getAddress()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 103
    new-instance v3, Lcom/squareup/fonts/FontSpan;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v2

    invoke-direct {v3, p1, v2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v3, Landroid/text/style/CharacterStyle;

    .line 66
    invoke-static {v1, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "address"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 70
    sget p1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_dialog_title:I

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 73
    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_dialog_button:I

    .line 74
    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;

    invoke-direct {v1, p2, p3}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory$createDialog$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    .line 72
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 78
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 79
    sget p2, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026hite)\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p1}, Lcom/squareup/ui/settings/InSettingsAppletScope;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    const-string v1, "get(context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen;

    .line 53
    const-class v1, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Component;

    .line 54
    invoke-interface {v1}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Component;->runner()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    move-result-object v1

    .line 55
    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen;->getData()Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(createDialog\u2026xt, runner, screen.data))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
