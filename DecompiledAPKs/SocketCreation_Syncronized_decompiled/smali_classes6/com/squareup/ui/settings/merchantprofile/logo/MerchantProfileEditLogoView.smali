.class public Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;
.super Landroid/widget/LinearLayout;
.source "MerchantProfileEditLogoView.java"

# interfaces
.implements Lcom/squareup/picasso/Target;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private attacher:Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

.field private hint:Landroid/widget/TextView;

.field private image:Landroid/widget/ImageView;

.field picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final progressPopup:Lcom/squareup/caller/ProgressPopup;

.field private final targetHeight:I

.field private final targetWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const-class p2, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Component;->inject(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;)V

    .line 48
    new-instance p2, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/registerlib/R$dimen;->merchant_logo_width:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->targetWidth:I

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/registerlib/R$dimen;->merchant_logo_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->targetHeight:I

    return-void
.end method


# virtual methods
.method createBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->image:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/squareup/ui/Bitmaps;->createBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 102
    iget v1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->targetWidth:I

    iget v2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->targetHeight:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/Bitmaps;->scaleBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 88
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getProgressPopup()Lcom/squareup/caller/ProgressPopup;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 110
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$loadPhoto$0$MerchantProfileEditLogoView(Landroid/net/Uri;Landroid/view/View;II)V
    .locals 0

    .line 92
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 93
    invoke-virtual {p1, p3, p4}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->skipMemoryCache()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 97
    invoke-virtual {p1, p0}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    return-void
.end method

.method loadPhoto(Landroid/net/Uri;)V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoView$Qv1viUALuLd1GbuR_oImnmwDNZg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoView$Qv1viUALuLd1GbuR_oImnmwDNZg;-><init>(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;Landroid/net/Uri;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_error_message:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->presenter:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->bitmapLoadFailed()V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->hint:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 0

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->image:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 71
    new-instance p1, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->image:Landroid/widget/ImageView;

    invoke-direct {p1, p2}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;-><init>(Landroid/widget/ImageView;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->attacher:Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->image:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getId()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->presenter:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->bitmapLoaded()V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->hint:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->presenter:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->dropView(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0, p0}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    .line 66
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 58
    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->image:Landroid/widget/ImageView;

    .line 59
    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->hint:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->presenter:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method
