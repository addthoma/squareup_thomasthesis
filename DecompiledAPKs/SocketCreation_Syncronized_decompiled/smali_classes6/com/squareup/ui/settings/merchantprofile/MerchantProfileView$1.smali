.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "MerchantProfileView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->access$300(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)Lcom/squareup/widgets/PersistentViewAnimator;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$id;->merchant_profile_content_progress:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/PersistentViewAnimator;->setDisplayedChildById(I)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    iget-object p1, p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->retryClicked()V

    return-void
.end method
