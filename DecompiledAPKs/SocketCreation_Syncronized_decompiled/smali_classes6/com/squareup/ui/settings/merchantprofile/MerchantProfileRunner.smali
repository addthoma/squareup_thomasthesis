.class public final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;
.super Ljava/lang/Object;
.source "MerchantProfileRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/SettingsAppletScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0007\u0018\u00002\u00020\u0001BG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0006\u0010\u0013\u001a\u00020\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\tH\u0002J\u0008\u0010\u0018\u001a\u00020\u0014H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0006\u0010\u001d\u001a\u00020\u0014J\u0016\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u001f\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cJ\u0010\u0010 \u001a\u00020\u00142\u0006\u0010!\u001a\u00020\"H\u0016J\u000e\u0010#\u001a\u00020\u00142\u0006\u0010$\u001a\u00020\u001aJ\u0008\u0010%\u001a\u00020\u0014H\u0016J\u000e\u0010&\u001a\u00020\u00142\u0006\u0010\'\u001a\u00020(J\u0018\u0010)\u001a\u00020\u00142\u0006\u0010*\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0008\u0010+\u001a\u00020\u0014H\u0002J\u0008\u0010,\u001a\u00020\u0014H\u0002J\u0008\u0010-\u001a\u00020\u0014H\u0002J\u0008\u0010.\u001a\u00020\u0014H\u0002J\u0006\u0010/\u001a\u00020\u0014R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
        "Lmortar/Scoped;",
        "flow",
        "Lflow/Flow;",
        "monitor",
        "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
        "resources",
        "Landroid/content/res/Resources;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "state",
        "Lcom/squareup/merchantprofile/MerchantProfileState;",
        "updater",
        "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lflow/Flow;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Landroid/content/res/Resources;Lcom/squareup/CountryCode;Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V",
        "addressConfirmedButtonClicked",
        "",
        "getMissingFieldsMessage",
        "",
        "country",
        "goBack",
        "hasPoBox",
        "",
        "address",
        "Lcom/squareup/address/Address;",
        "mobileBusinessConfirmedButtonClicked",
        "onBusinessAddressSave",
        "isMobileBusiness",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitEditScreen",
        "showConfirmAddressDialog",
        "onExitScope",
        "onVerifyBusinessAddressDialogContinue",
        "dialogType",
        "Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;",
        "saveAddress",
        "mobileBusiness",
        "showBusinessAddressPoBoxDisallowedError",
        "showConfirmBusinessAddressDialogScreen",
        "showConfirmMobileBusinessDialogScreen",
        "showMissingRequiredFields",
        "showRequestBusinessAddressDialogScreen",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

.field private final resources:Landroid/content/res/Resources;

.field private final state:Lcom/squareup/merchantprofile/MerchantProfileState;

.field private final updater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Landroid/content/res/Resources;Lcom/squareup/CountryCode;Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/merchantprofile/MerchantProfileUpdater;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "monitor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updater"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    iput-object p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->countryCode:Lcom/squareup/CountryCode;

    iput-object p5, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    iput-object p6, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->updater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    iput-object p7, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p8, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getMonitor$p(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    return-object p0
.end method

.method private final getMissingFieldsMessage(Lcom/squareup/CountryCode;)Ljava/lang/String;
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/settingsapplet/R$string;->missing_address_fields_message:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->stateHint(Lcom/squareup/CountryCode;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "state"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->postalHint(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "postalcode"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 191
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 192
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final goBack()V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method private final hasPoBox(Lcom/squareup/address/Address;)Z
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 199
    :cond_0
    invoke-static {p1}, Lcom/squareup/address/PoBoxChecker;->isPoBox(Lcom/squareup/address/Address;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method private final saveAddress(ZLcom/squareup/address/Address;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->updater:Lcom/squareup/merchantprofile/MerchantProfileUpdater;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$saveAddress$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$saveAddress$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1, p2, v1}, Lcom/squareup/merchantprofile/MerchantProfileUpdater;->updateBusinessAddressAndSubscribe(ZLcom/squareup/address/Address;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final showBusinessAddressPoBoxDisallowedError()V
    .locals 3

    .line 172
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 173
    sget v1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_warning_no_po_box_allowed_title:I

    .line 174
    sget v2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_warning_no_po_box_allowed_message:I

    .line 172
    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final showConfirmBusinessAddressDialogScreen()V
    .locals 5

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    .line 156
    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;

    .line 157
    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;

    .line 158
    iget-object v3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v3}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object v3

    const-string v4, "state.address"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/address/AddressFormatter;->toOneLineDisplayString(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 157
    invoke-direct {v2, v3, v4}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;-><init>(Ljava/lang/String;Z)V

    .line 156
    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;-><init>(Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;)V

    .line 155
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final showConfirmMobileBusinessDialogScreen()V
    .locals 5

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    .line 167
    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;

    const-string v3, ""

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;-><init>(Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;)V

    .line 166
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final showMissingRequiredFields()V
    .locals 3

    .line 180
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/settingsapplet/R$string;->missing_address_fields_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 182
    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->countryCode:Lcom/squareup/CountryCode;

    invoke-direct {p0, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->getMissingFieldsMessage(Lcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v2

    .line 180
    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final addressConfirmedButtonClicked()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {v0}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->setBusinessAddressVerified()V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public final mobileBusinessConfirmedButtonClicked()V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->setMobileBusiness(Ljava/lang/Boolean;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {v0}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->setBusinessAddressVerified()V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public final onBusinessAddressSave(ZLcom/squareup/address/Address;)V
    .locals 2

    const-string v0, "address"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->setMobileBusiness(Ljava/lang/Boolean;)V

    if-eqz p1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logMobileBusiness(Lcom/squareup/analytics/Analytics;)V

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->saveAddress(ZLcom/squareup/address/Address;)V

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->goBack()V

    return-void

    .line 62
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/address/Address;->isComplete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logInvalidAddress(Lcom/squareup/analytics/Analytics;)V

    .line 64
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->showMissingRequiredFields()V

    goto :goto_0

    .line 65
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->hasPoBox(Lcom/squareup/address/Address;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logInvalidAddress(Lcom/squareup/analytics/Analytics;)V

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->showBusinessAddressPoBoxDisallowedError()V

    goto :goto_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logSaveAddress(Lcom/squareup/analytics/Analytics;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0, p2}, Lcom/squareup/merchantprofile/MerchantProfileState;->setAddress(Lcom/squareup/address/Address;)V

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->saveAddress(ZLcom/squareup/address/Address;)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->goBack()V

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final onExitEditScreen(Z)V
    .locals 1

    if-eqz p1, :cond_3

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->isMobileBusiness()Ljava/lang/Boolean;

    move-result-object p1

    const-string v0, "state.isMobileBusiness"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_1

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/address/Address;->isComplete()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {p1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    const-string v0, "state.address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->hasPoBox(Lcom/squareup/address/Address;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_2

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logMobileShowForgotSomething(Lcom/squareup/analytics/Analytics;)V

    .line 93
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->showConfirmMobileBusinessDialogScreen()V

    goto :goto_2

    .line 95
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logAddressShowForgotSomething(Lcom/squareup/analytics/Analytics;)V

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->showConfirmBusinessAddressDialogScreen()V

    goto :goto_2

    .line 99
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logCancelAddress(Lcom/squareup/analytics/Analytics;)V

    .line 100
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->goBack()V

    :goto_2
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public final onVerifyBusinessAddressDialogContinue(Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;)V
    .locals 1

    const-string v0, "dialogType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 146
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyBusinessAddressContinue(Lcom/squareup/analytics/Analytics;)V

    goto :goto_0

    .line 145
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyNoAddressContinue(Lcom/squareup/analytics/Analytics;)V

    goto :goto_0

    .line 144
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyCurrentLocationContinue(Lcom/squareup/analytics/Analytics;)V

    :goto_0
    return-void
.end method

.method public final showRequestBusinessAddressDialogScreen()V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->isMobileBusiness()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "state.isMobileBusiness"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyCurrentLocationModal(Lcom/squareup/analytics/Analytics;)V

    .line 119
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

    .line 120
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_dialog_mobile_business:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026s_dialog_mobile_business)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    sget-object v2, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;->MOBILE_BUSINESS:Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;

    .line 119
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;)V

    goto :goto_1

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/squareup/address/Address;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyBusinessAddressModal(Lcom/squareup/analytics/Analytics;)V

    .line 133
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->state:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v1}, Lcom/squareup/merchantprofile/MerchantProfileState;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    const-string v2, "state.address"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/address/AddressFormatter;->toOneLineDisplayString(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object v1

    .line 135
    sget-object v2, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;->HAS_ADDRESS:Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;

    .line 133
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;)V

    goto :goto_1

    .line 125
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyNoAddressModal(Lcom/squareup/analytics/Analytics;)V

    .line 126
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_dialog_no_address:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026ddress_dialog_no_address)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    sget-object v2, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;->NO_ADDRESS:Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;

    .line 126
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Type;)V

    .line 139
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen;-><init>(Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$ScreenData;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
