.class public final Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;
.super Landroid/widget/BaseAdapter;
.source "UnitPickerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/UnitPickerCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnitAdapter"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitPickerCoordinator.kt\ncom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,91:1\n33#2,3:92\n*E\n*S KotlinDebug\n*F\n+ 1 UnitPickerCoordinator.kt\ncom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter\n*L\n70#1,3:92\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000eH\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0010\u001a\u00020\u000eH\u0016J\"\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\u000e2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0016\u001a\u00020\u0017H\u0016R7\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\n\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;",
        "Landroid/widget/BaseAdapter;",
        "()V",
        "<set-?>",
        "",
        "Lcom/squareup/protos/register/api/Unit;",
        "units",
        "getUnits",
        "()Ljava/util/List;",
        "setUnits",
        "(Ljava/util/List;)V",
        "units$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "getCount",
        "",
        "getItem",
        "position",
        "getItemId",
        "",
        "getView",
        "Landroid/view/View;",
        "convertView",
        "parent",
        "Landroid/view/ViewGroup;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final units$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "units"

    const-string v4, "getUnits()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 68
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 70
    sget-object v0, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter$$special$$inlined$observable$1;

    invoke-direct {v1, v0, v0, p0}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;)V

    check-cast v1, Lkotlin/properties/ReadWriteProperty;

    .line 94
    iput-object v1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->units$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->getUnits()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/protos/register/api/Unit;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->getUnits()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/register/api/Unit;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->getItem(I)Lcom/squareup/protos/register/api/Unit;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getUnits()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->units$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    check-cast p2, Lcom/squareup/ui/account/view/LineRow;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    sget-object p3, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    .line 85
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p2

    .line 86
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->getItem(I)Lcom/squareup/protos/register/api/Unit;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/register/api/Unit;->unit_display_name:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    const-string p1, "lineRow"

    .line 87
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    return-object p2
.end method

.method public final setUnits(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->units$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
