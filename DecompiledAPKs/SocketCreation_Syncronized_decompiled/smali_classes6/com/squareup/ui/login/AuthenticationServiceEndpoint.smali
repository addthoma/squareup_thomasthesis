.class public interface abstract Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticationServiceEndpoint$DefaultImpls;,
        Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u001f2\u00020\u0001:\u0001\u001fJ2\u0010\u0002\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH&J\u001e\u0010\u000c\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\r0\u00040\u00032\u0006\u0010\u000e\u001a\u00020\u000fH&J\u001e\u0010\u0010\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00120\u00110\u00032\u0006\u0010\u0013\u001a\u00020\u0014H&J&\u0010\u0015\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00160\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0018H&J>\u0010\u0019\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u001c2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH&J&\u0010\u001d\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u001e0\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
        "",
        "enrollTwoFactor",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "sessionToken",
        "Lcom/squareup/account/SecretString;",
        "twoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "idempotenceToken",
        "",
        "login",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "loginRequest",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "resetPassword",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/ForgotPasswordResponse;",
        "resetPasswordRequest",
        "Lcom/squareup/server/account/ForgotPasswordBody;",
        "selectUnit",
        "Lcom/squareup/protos/register/api/SelectUnitResponse;",
        "selectUnitRequest",
        "Lcom/squareup/protos/register/api/SelectUnitRequest;",
        "sendVerificationCode",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "throttle",
        "",
        "upgradeSession",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;

.field public static final MIN_DELAY_MS:J = 0x2eeL


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;->$$INSTANCE:Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;

    sput-object v0, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->Companion:Lcom/squareup/ui/login/AuthenticationServiceEndpoint$Companion;

    return-void
.end method


# virtual methods
.method public abstract enrollTwoFactor(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract login(Lcom/squareup/protos/register/api/LoginRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract resetPassword(Lcom/squareup/server/account/ForgotPasswordBody;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/ForgotPasswordBody;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/ForgotPasswordResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract selectUnit(Lcom/squareup/account/SecretString;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/register/api/SelectUnitRequest;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SelectUnitResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract sendVerificationCode(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZLjava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upgradeSession(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
            ">;>;"
        }
    .end annotation
.end method
