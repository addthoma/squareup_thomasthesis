.class public final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "EmailPasswordLoginCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J$\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "v",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 125
    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 p1, 0x2

    if-eq p2, p1, :cond_1

    const/4 p1, 0x5

    if-eq p2, p1, :cond_1

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_2

    .line 135
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$isSignInEnabled$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$indicateError(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V

    goto :goto_1

    .line 138
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$getWorkflow$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;

    iget-object p3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {p3}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$getEmail$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Ljava/lang/String;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$attach$3;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$getPassword$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/account/SecretStringKt;->toSecretString(Ljava/lang/CharSequence;)Lcom/squareup/account/SecretString;

    move-result-object v0

    invoke-direct {p2, p3, v0}, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;-><init>(Ljava/lang/String;Lcom/squareup/account/SecretString;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :goto_1
    const/4 p1, 0x1

    :goto_2
    return p1
.end method
