.class public final Lcom/squareup/ui/login/AuthenticatorStateKt;
.super Ljava/lang/Object;
.source "AuthenticatorState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticatorState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticatorState.kt\ncom/squareup/ui/login/AuthenticatorStateKt\n*L\n1#1,88:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "canSkipEnroll",
        "",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "getCanSkipEnroll",
        "(Lcom/squareup/ui/login/AuthenticatorState;)Z",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getCanSkipEnroll(Lcom/squareup/ui/login/AuthenticatorState;)Z
    .locals 3

    const-string v0, "$this$canSkipEnroll"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    .line 81
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;->getCanSkipEnroll()Z

    move-result p0

    goto :goto_0

    .line 82
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;->getCanSkipEnroll()Z

    move-result p0

    :goto_0
    return p0

    .line 83
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected current screen to contain canSkipEnroll flag, but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 83
    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
