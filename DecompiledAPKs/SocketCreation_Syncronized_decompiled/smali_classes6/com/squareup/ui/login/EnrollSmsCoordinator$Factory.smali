.class public final Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;
.super Ljava/lang/Object;
.source "EnrollSmsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/EnrollSmsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u00020\u00082\u0018\u0010\t\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b0\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "scrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;)V",
        "create",
        "Lcom/squareup/ui/login/EnrollSmsCoordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final scrubber:Lcom/squareup/text/InsertingScrubber;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;)V
    .locals 1
    .param p2    # Lcom/squareup/text/InsertingScrubber;
        .annotation runtime Lcom/squareup/text/InsertingScrubber$PhoneNumber;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;->scrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/login/EnrollSmsCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)",
            "Lcom/squareup/ui/login/EnrollSmsCoordinator;"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/ui/login/EnrollSmsCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;->scrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lio/reactivex/Observable;)V

    return-object v0
.end method
