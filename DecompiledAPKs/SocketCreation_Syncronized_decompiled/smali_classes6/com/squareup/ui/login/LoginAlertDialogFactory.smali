.class public final Lcom/squareup/ui/login/LoginAlertDialogFactory;
.super Ljava/lang/Object;
.source "LoginAlertDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoginAlertDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoginAlertDialogFactory.kt\ncom/squareup/ui/login/LoginAlertDialogFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 AuthenticatorEvent.kt\ncom/squareup/ui/login/AuthenticatorEventKt\n*L\n1#1,171:1\n1642#2,2:172\n200#3:174\n*E\n*S KotlinDebug\n*F\n+ 1 LoginAlertDialogFactory.kt\ncom/squareup/ui/login/LoginAlertDialogFactory\n*L\n93#1,2:172\n168#1:174\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u001cB/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0018\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0007\u00a2\u0006\u0002\u0010\u000bJ\u0016\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J,\u0010\u000c\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\t2\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00130\u0007H\u0002J \u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00132\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J(\u0010\u0019\u001a\u00020\u0015*\u00020\u001a2\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00130\u00072\u0006\u0010\u001b\u001a\u00020\u0018H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/login/LoginAlertDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "res",
        "Lcom/squareup/util/Res;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/AlertDialog;",
        "context",
        "Landroid/content/Context;",
        "screen",
        "inputs",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onResult",
        "",
        "workflow",
        "result",
        "Lcom/squareup/protos/multipass/mobile/AlertButton;",
        "addStackedButton",
        "Landroid/view/ViewGroup;",
        "alertButton",
        "Factory",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final res:Lcom/squareup/util/Res;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/util/Res;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "browserLauncher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p2, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$create(Lcom/squareup/ui/login/LoginAlertDialogFactory;Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;Lio/reactivex/Observable;)Landroid/app/AlertDialog;
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->create(Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;Lio/reactivex/Observable;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onResult(Lcom/squareup/ui/login/LoginAlertDialogFactory;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/multipass/mobile/AlertButton;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->onResult(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    return-void
.end method

.method private final addStackedButton(Landroid/view/ViewGroup;Lio/reactivex/Observable;Lcom/squareup/protos/multipass/mobile/AlertButton;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ")V"
        }
    .end annotation

    .line 117
    sget v0, Lcom/squareup/common/authenticatorviews/R$layout;->login_alert_dialog_button:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 118
    iget-object v1, p3, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;

    invoke-direct {v2, p0, p2, v0, p3}, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;-><init>(Lcom/squareup/ui/login/LoginAlertDialogFactory;Lio/reactivex/Observable;Landroid/widget/TextView;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 124
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final create(Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;Lio/reactivex/Observable;)Landroid/app/AlertDialog;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    .line 78
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;->getAlert()Lcom/squareup/protos/multipass/mobile/Alert;

    move-result-object p2

    .line 79
    sget v0, Lcom/squareup/common/authenticatorviews/R$layout;->login_alert_dialog:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string v1, "customView"

    .line 81
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 82
    iget-object v2, p2, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    sget v1, Lcom/squareup/common/authenticatorviews/R$id;->message:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 85
    iget-object v2, p2, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    sget v1, Lcom/squareup/common/authenticatorviews/R$id;->button_container:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 87
    check-cast v1, Landroid/view/ViewGroup;

    .line 90
    iget-object v2, p2, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    if-eqz v2, :cond_0

    .line 91
    iget-object v2, p2, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    const-string v3, "alert.primary_button"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, p3, v2}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->addStackedButton(Landroid/view/ViewGroup;Lio/reactivex/Observable;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    .line 93
    :cond_0
    iget-object p2, p2, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    const-string v2, "alert.other_buttons"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Iterable;

    .line 172
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/multipass/mobile/AlertButton;

    const-string v3, "button"

    .line 94
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, p3, v2}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->addStackedButton(Landroid/view/ViewGroup;Lio/reactivex/Observable;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    goto :goto_0

    .line 101
    :cond_1
    new-instance p2, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;-><init>()V

    .line 102
    iget-object v2, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->dismiss:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    move-result-object p2

    .line 103
    new-instance v2, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    invoke-direct {v2}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;-><init>()V

    invoke-virtual {p2, v2}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss(Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    move-result-object p2

    .line 104
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButton;

    move-result-object p2

    const-string v2, "AlertButton.Builder()\n  \u2026s())\n            .build()"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-direct {p0, v1, p3, p2}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->addStackedButton(Landroid/view/ViewGroup;Lio/reactivex/Observable;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    .line 107
    new-instance p2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {p2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 108
    invoke-virtual {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 109
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026e(true)\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onResult(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/multipass/mobile/AlertButton;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 132
    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_2

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iget-object v1, v1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->url:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 136
    iget-object p2, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iget-object p2, p2, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->dismiss_after_open:Ljava/lang/Boolean;

    if-eqz p2, :cond_1

    goto :goto_1

    .line 137
    :cond_1
    sget-object p2, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->DEFAULT_DISMISS_AFTER_OPEN:Ljava/lang/Boolean;

    :goto_1
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_8

    return-void

    :cond_2
    if-eqz p2, :cond_3

    .line 142
    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    goto :goto_2

    :cond_3
    move-object v1, v0

    :goto_2
    if-eqz v1, :cond_6

    .line 145
    iget-object p2, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    iget-object p2, p2, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    if-eqz p2, :cond_5

    .line 146
    sget-object v1, Lcom/squareup/ui/login/LoginAlertDialogFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_4

    const/4 v0, 0x2

    goto :goto_3

    .line 148
    :cond_4
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;

    invoke-direct {p2, v0, v1, v0}, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void

    .line 145
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Expected flow not to be null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_6
    if-eqz p2, :cond_7

    .line 160
    iget-object v0, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    :cond_7
    if-nez v0, :cond_8

    .line 162
    new-instance p2, Ljava/lang/RuntimeException;

    const-string v0, "unknown login alert button action, dismissing by default"

    invoke-direct {p2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    .line 161
    invoke-static {p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 174
    :cond_8
    :goto_3
    new-instance p2, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    const-class v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    invoke-direct {p2, v0}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;-><init>(Ljava/lang/Class;)V

    .line 168
    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/AlertDialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->screenData:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$inputs$1;->INSTANCE:Lcom/squareup/ui/login/LoginAlertDialogFactory$create$inputs$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory;->screenData:Lio/reactivex/Observable;

    new-instance v2, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;-><init>(Lcom/squareup/ui/login/LoginAlertDialogFactory;Landroid/content/Context;Lio/reactivex/Observable;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screenData.map { (screen\u2026}\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
