.class public final Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "VerificationCodeGoogleAuthCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerificationCodeGoogleAuthCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerificationCodeGoogleAuthCoordinator.kt\ncom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,151:1\n45#2:152\n17#2,22:153\n*E\n*S KotlinDebug\n*F\n+ 1 VerificationCodeGoogleAuthCoordinator.kt\ncom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator\n*L\n44#1:152\n44#1,22:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J\u0008\u0010$\u001a\u00020\u001cH\u0002J\u0006\u0010%\u001a\u00020\u001cR\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u000eR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u00168BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "contactSupport",
        "Lcom/squareup/widgets/MessageView;",
        "isRememberThisDeviceChecked",
        "",
        "()Z",
        "isVerificationCodeValid",
        "rememberThisDeviceToggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "screen",
        "subtitle",
        "title",
        "verificationCode",
        "",
        "getVerificationCode",
        "()Ljava/lang/String;",
        "verificationCodeField",
        "Lcom/squareup/ui/XableEditText;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "buildContactSupportLinkText",
        "",
        "context",
        "Landroid/content/Context;",
        "indicateVerificationCodeError",
        "updateEnabledState",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private contactSupport:Lcom/squareup/widgets/MessageView;

.field private rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private screen:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;

.field private verificationCodeField:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$buildContactSupportLinkText(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->buildContactSupportLinkText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getContactSupport$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->contactSupport:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "contactSupport"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->screen:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getVerificationCode$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Ljava/lang/String;
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->getVerificationCode()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getVerificationCodeField$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "verificationCodeField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$indicateVerificationCodeError(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->indicateVerificationCodeError()V

    return-void
.end method

.method public static final synthetic access$isRememberThisDeviceChecked$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Z
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->isRememberThisDeviceChecked()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isVerificationCodeValid$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Z
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->isVerificationCodeValid()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setContactSupport$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->contactSupport:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->screen:Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    return-void
.end method

.method public static final synthetic access$setVerificationCodeField$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 143
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 144
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 145
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 146
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->verification_code_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    .line 147
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->remember_this_device:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 148
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->contact_support:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->contactSupport:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final buildContactSupportLinkText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2

    .line 128
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    sget p1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_google_auth_support:I

    const-string v1, "contact_support"

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 130
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_contact_support_url:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 131
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_google_auth_contact_support:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 132
    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "LinkSpan.Builder(context\u2026        .asCharSequence()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getVerificationCode()Ljava/lang/String;
    .locals 8

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "verificationCodeField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 152
    check-cast v0, Ljava/lang/CharSequence;

    .line 154
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    move v4, v1

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    if-gt v1, v4, :cond_6

    if-nez v5, :cond_1

    move v6, v1

    goto :goto_1

    :cond_1
    move v6, v4

    .line 159
    :goto_1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-gt v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-nez v5, :cond_4

    if-nez v6, :cond_3

    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    if-nez v6, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_6
    :goto_3
    add-int/2addr v4, v2

    .line 174
    invoke-interface {v0, v1, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_8

    goto :goto_5

    :cond_8
    const-string v0, ""

    :goto_5
    return-object v0
.end method

.method private final indicateVerificationCodeError()V
    .locals 4

    .line 137
    new-instance v0, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {v0}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    .line 138
    new-instance v1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    iget-object v2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    const-string v3, "verificationCodeField"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private final isRememberThisDeviceChecked()Z
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_0

    const-string v1, "rememberThisDeviceToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method private final isVerificationCodeValid()Z
    .locals 1

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->getVerificationCode()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->bindViews(Landroid/view/View;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_google_auth_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_google_auth_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    const-string v1, "verificationCodeField"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->verificationCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->rememberThisDeviceToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_4

    const-string v1, "rememberThisDeviceToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 62
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_verification_remember_this_device:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 61
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;-><init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final updateEnabledState()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->isVerificationCodeValid()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
