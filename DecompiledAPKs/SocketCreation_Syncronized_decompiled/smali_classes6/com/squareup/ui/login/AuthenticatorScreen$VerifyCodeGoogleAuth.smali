.class public final Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;
.super Lcom/squareup/ui/login/AuthenticatorScreen;
.source "AuthenticatorScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VerifyCodeGoogleAuth"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000f\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J-\u0010\u0012\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "allTwoFactorDetails",
        "",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "googleAuthTwoFactorDetails",
        "login",
        "Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
        "(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V",
        "getAllTwoFactorDetails",
        "()Ljava/util/List;",
        "getGoogleAuthTwoFactorDetails",
        "()Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "getLogin",
        "()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allTwoFactorDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

.field private final login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            "Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
            ")V"
        }
    .end annotation

    const-string v0, "allTwoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "googleAuthTwoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "login"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 287
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreen;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->copy(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            "Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
            ")",
            "Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;"
        }
    .end annotation

    const-string v0, "allTwoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "googleAuthTwoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "login"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;-><init>(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllTwoFactorDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    return-object v0
.end method

.method public final getGoogleAuthTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object v0
.end method

.method public final getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VerifyCodeGoogleAuth(allTwoFactorDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->allTwoFactorDetails:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", googleAuthTwoFactorDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->googleAuthTwoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", login="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->login:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
