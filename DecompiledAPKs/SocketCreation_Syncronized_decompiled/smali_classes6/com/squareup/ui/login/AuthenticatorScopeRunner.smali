.class public final Lcom/squareup/ui/login/AuthenticatorScopeRunner;
.super Ljava/lang/Object;
.source "AuthenticatorScope.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/login/AuthenticatorScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticatorScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticatorScope.kt\ncom/squareup/ui/login/AuthenticatorScopeRunner\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,74:1\n19#2:75\n*E\n*S KotlinDebug\n*F\n+ 1 AuthenticatorScope.kt\ncom/squareup/ui/login/AuthenticatorScopeRunner\n*L\n62#1:75\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScopeRunner;",
        "Lmortar/Scoped;",
        "loggedOutRunner",
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        "flow",
        "Lflow/Flow;",
        "(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lflow/Flow;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final loggedOutRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loggedOutRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScopeRunner;->loggedOutRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorScopeRunner;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->Companion:Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/login/AuthenticatorScopeRunner$onEnterScope$1$1;

    iget-object v3, p0, Lcom/squareup/ui/login/AuthenticatorScopeRunner;->flow:Lflow/Flow;

    invoke-direct {v2, v3}, Lcom/squareup/ui/login/AuthenticatorScopeRunner$onEnterScope$1$1;-><init>(Lflow/Flow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 64
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 75
    const-class v1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorScopeRunner$onEnterScope$1$2;

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorScopeRunner;->loggedOutRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/AuthenticatorScopeRunner$onEnterScope$1$2;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
