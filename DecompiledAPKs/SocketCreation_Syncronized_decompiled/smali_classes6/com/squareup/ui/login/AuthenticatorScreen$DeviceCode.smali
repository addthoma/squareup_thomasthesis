.class public final Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;
.super Lcom/squareup/ui/login/AuthenticatorScreen;
.source "AuthenticatorScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceCode"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0006\u0010\u0010\u001a\u00020\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "indicateError",
        "",
        "(I)V",
        "getIndicateError",
        "()I",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "withErrorIndicated",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final indicateError:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreen;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 87
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;-><init>(I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;IILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->copy(I)Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    iget p1, p1, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIndicateError()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceCode(indicateError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final withErrorIndicated()Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;
    .locals 1

    .line 89
    iget v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->indicateError:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->copy(I)Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    move-result-object v0

    return-object v0
.end method
