.class public Lcom/squareup/ui/login/CreateAccountScreen;
.super Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;
.source "CreateAccountScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/login/CreateAccountScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/CreateAccountScreen$Module;,
        Lcom/squareup/ui/login/CreateAccountScreen$Component;,
        Lcom/squareup/ui/login/CreateAccountScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/login/CreateAccountScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final cancelOnBack:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/ui/login/-$$Lambda$CreateAccountScreen$2Mj6e87gRA0SAdXHZYr6XlCyXWM;->INSTANCE:Lcom/squareup/ui/login/-$$Lambda$CreateAccountScreen$2Mj6e87gRA0SAdXHZYr6XlCyXWM;

    .line 64
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/login/CreateAccountScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;-><init>()V

    .line 56
    iput-boolean p1, p0, Lcom/squareup/ui/login/CreateAccountScreen;->cancelOnBack:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/login/CreateAccountScreen;
    .locals 1

    .line 65
    new-instance v0, Lcom/squareup/ui/login/CreateAccountScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result p0

    invoke-static {p0}, Lcom/squareup/util/Booleans;->toBoolean(B)Z

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/CreateAccountScreen;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 60
    iget-boolean p2, p0, Lcom/squareup/ui/login/CreateAccountScreen;->cancelOnBack:Z

    invoke-static {p2}, Lcom/squareup/util/Booleans;->toByte(Z)B

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_CREATE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 71
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 79
    sget v0, Lcom/squareup/loggedout/R$layout;->create_account_view:I

    return v0
.end method
