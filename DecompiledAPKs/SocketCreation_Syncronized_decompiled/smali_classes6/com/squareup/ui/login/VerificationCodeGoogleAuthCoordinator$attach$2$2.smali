.class final Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;
.super Lkotlin/jvm/internal/Lambda;
.source "VerificationCodeGoogleAuthCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "verifyCode",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;

    iput-object p2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;

    iget-object v2, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;

    iget-object v2, v2, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$getVerificationCode$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;

    iget-object v3, v3, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator$attach$2;->this$0:Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;

    invoke-static {v3}, Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;->access$isRememberThisDeviceChecked$p(Lcom/squareup/ui/login/VerificationCodeGoogleAuthCoordinator;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
