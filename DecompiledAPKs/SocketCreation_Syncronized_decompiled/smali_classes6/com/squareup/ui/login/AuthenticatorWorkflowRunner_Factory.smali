.class public final Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "AuthenticatorWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final historySourceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->historySourceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lcom/squareup/ui/login/AuthenticatorViewFactory;Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;)Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lcom/squareup/ui/login/AuthenticatorViewFactory;Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->historySourceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorViewFactory;

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lcom/squareup/ui/login/AuthenticatorViewFactory;Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;)Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner_Factory;->get()Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
