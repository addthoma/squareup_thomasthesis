.class final Lcom/squareup/ui/login/RealAuthenticator$showForgotPasswordToastThenGoBack$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->showForgotPasswordToastThenGoBack(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "state",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $email:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$showForgotPasswordToastThenGoBack$1;->$email:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 10

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 905
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    .line 906
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    .line 907
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 908
    iget-object v5, p0, Lcom/squareup/ui/login/RealAuthenticator$showForgotPasswordToastThenGoBack$1;->$email:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1b

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 907
    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 909
    sget-object v0, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v7

    const/4 v8, 0x7

    move-object v2, p1

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1

    .line 906
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.ui.login.AuthenticatorScreen.EmailPassword"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$showForgotPasswordToastThenGoBack$1;->invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method
