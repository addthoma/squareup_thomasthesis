.class public final Lcom/squareup/ui/login/AuthenticationServiceEndpoint$DefaultImpls;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic enrollTwoFactor$default(Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 111
    check-cast p3, Ljava/lang/String;

    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->enrollTwoFactor(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: enrollTwoFactor"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic sendVerificationCode$default(Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZLjava/lang/String;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p6, :cond_2

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    .line 127
    check-cast p4, Ljava/lang/String;

    :cond_1
    invoke-interface {p0, p1, p2, p3, p4}, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;->sendVerificationCode(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: sendVerificationCode"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
