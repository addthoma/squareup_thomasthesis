.class public final Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"

# interfaces
.implements Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
.implements Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$Error;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u0002:\u00013B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004J0\u0010\u0015\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u00170\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u001e\u0010\u001f\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020 0\u00170\u00162\u0006\u0010!\u001a\u00020\"H\u0016J\u001e\u0010#\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020%0$0\u00162\u0006\u0010&\u001a\u00020\'H\u0016J&\u0010(\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020)0\u00170\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010*\u001a\u00020+H\u0016J:\u0010,\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010-0\u00170\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010.\u001a\u00020/2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J&\u00100\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u0002010\u00170\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u000c\u00102\u001a\u00020\u001e*\u00020\u001aH\u0002R\u0012\u0010\u0005\u001a\u00020\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0012\u0010\t\u001a\u00020\nX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0012\u0010\r\u001a\u00020\u000eX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u0012X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;",
        "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
        "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;",
        "dependencies",
        "(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;)V",
        "authenticationService",
        "Lcom/squareup/server/account/AuthenticationService;",
        "getAuthenticationService",
        "()Lcom/squareup/server/account/AuthenticationService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "getMainScheduler",
        "()Lio/reactivex/Scheduler;",
        "passwordService",
        "Lcom/squareup/server/account/PasswordService;",
        "getPasswordService",
        "()Lcom/squareup/server/account/PasswordService;",
        "res",
        "Lcom/squareup/util/Res;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "enrollTwoFactor",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "sessionToken",
        "Lcom/squareup/account/SecretString;",
        "twoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "idempotenceToken",
        "",
        "login",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "loginRequest",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "resetPassword",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/ForgotPasswordResponse;",
        "resetPasswordRequest",
        "Lcom/squareup/server/account/ForgotPasswordBody;",
        "selectUnit",
        "Lcom/squareup/protos/register/api/SelectUnitResponse;",
        "selectUnitRequest",
        "Lcom/squareup/protos/register/api/SelectUnitRequest;",
        "sendVerificationCode",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "throttle",
        "",
        "upgradeSession",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "asSessionHeader",
        "Error",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;)V
    .locals 1

    const-string v0, "dependencies"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->$$delegate_0:Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;

    return-void
.end method

.method private final asSessionHeader(Lcom/squareup/account/SecretString;)Ljava/lang/String;
    .locals 0

    .line 339
    invoke-virtual {p1}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/server/account/AuthorizationHeaders;->authorizationHeaderValueFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public enrollTwoFactor(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "twoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    new-instance v0, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;-><init>()V

    .line 235
    invoke-virtual {v0, p2}, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;

    move-result-object p2

    if-eqz p3, :cond_0

    goto :goto_0

    .line 236
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p3

    :goto_0
    invoke-virtual {p2, p3}, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;

    move-result-object p2

    .line 237
    invoke-virtual {p2}, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->build()Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;

    move-result-object p2

    .line 239
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;

    move-result-object p3

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->asSessionHeader(Lcom/squareup/account/SecretString;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->enrollTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;

    move-result-object p1

    .line 240
    invoke-virtual {p1}, Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object p1

    .line 241
    sget-object p2, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$enrollTwoFactor$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 246
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationService.en\u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->$$delegate_0:Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;

    invoke-interface {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;->getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;

    move-result-object v0

    return-object v0
.end method

.method public getMainScheduler()Lio/reactivex/Scheduler;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->$$delegate_0:Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;

    invoke-interface {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public getPasswordService()Lcom/squareup/server/account/PasswordService;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->$$delegate_0:Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;

    invoke-interface {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;->getPasswordService()Lcom/squareup/server/account/PasswordService;

    move-result-object v0

    return-object v0
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->$$delegate_0:Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;

    invoke-interface {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public login(Lcom/squareup/protos/register/api/LoginRequest;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "loginRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object v0, p1, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginRequest;->newBuilder()Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p1

    .line 182
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object p1

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;

    move-result-object v0

    const-string v1, "idempotentLoginRequest"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/account/AuthenticationService;->login(Lcom/squareup/protos/register/api/LoginRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object p1

    .line 189
    sget-object v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 191
    sget-object v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$2;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "authenticationService.lo\u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public resetPassword(Lcom/squareup/server/account/ForgotPasswordBody;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/ForgotPasswordBody;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/ForgotPasswordResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "resetPasswordRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getPasswordService()Lcom/squareup/server/account/PasswordService;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/server/account/PasswordService;->forgotPassword(Lcom/squareup/server/account/ForgotPasswordBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public selectUnit(Lcom/squareup/account/SecretString;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/register/api/SelectUnitRequest;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SelectUnitResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectUnitRequest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->asSessionHeader(Lcom/squareup/account/SecretString;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->selectUnit(Ljava/lang/String;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;

    move-result-object p1

    .line 330
    invoke-virtual {p1}, Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 331
    sget-object p2, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$selectUnit$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$selectUnit$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 336
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationService.se\u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public sendVerificationCode(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZLjava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "twoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    new-instance v0, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;-><init>()V

    .line 256
    invoke-virtual {v0, p2}, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;->two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;

    move-result-object p2

    if-eqz p4, :cond_0

    goto :goto_0

    .line 257
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p4

    invoke-virtual {p4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p4

    :goto_0
    invoke-virtual {p2, p4}, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;

    move-result-object p2

    .line 258
    invoke-virtual {p2}, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest$Builder;->build()Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;

    move-result-object p2

    .line 261
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;

    move-result-object p4

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->asSessionHeader(Lcom/squareup/account/SecretString;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "request"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p4, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->sendVerificationCodeTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;

    move-result-object p1

    .line 262
    invoke-virtual {p1}, Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object p1

    .line 263
    sget-object p2, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 278
    sget-object p2, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$sendVerificationCode$response$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 284
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationService.se\u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_1

    .line 288
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "response.toObservable()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 p2, 0x2ee

    .line 289
    sget-object p4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 290
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "response.toObservable()\n\u2026          .firstOrError()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    return-object p1
.end method

.method public upgradeSession(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/SecretString;",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "twoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    new-instance v0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;-><init>()V

    .line 307
    invoke-virtual {v0, p2}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;->two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;

    move-result-object p2

    .line 308
    invoke-virtual {p2}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest$Builder;->build()Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;

    move-result-object p2

    .line 310
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->asSessionHeader(Lcom/squareup/account/SecretString;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "request"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1, p2}, Lcom/squareup/server/account/AuthenticationService;->upgradeSessionTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;

    move-result-object p1

    .line 311
    invoke-virtual {p1}, Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object p1

    .line 312
    sget-object p2, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$upgradeSession$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$upgradeSession$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 317
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;->getMainScheduler()Lio/reactivex/Scheduler;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "authenticationService.up\u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
