.class public final Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;
.super Ljava/lang/Object;
.source "PostInstallEncryptedEmail.kt"

# interfaces
.implements Lcom/squareup/ui/login/PostInstallEncryptedEmail;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/PostInstallEncryptedEmail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoPostInstallLoginEncryptedEmail"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016J\n\u0010\n\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmail;",
        "()V",
        "clearEncryptedEmail",
        "",
        "decryptEmail",
        "Lio/reactivex/Maybe;",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "encryptedEmail",
        "",
        "getEncryptedEmail",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;

    invoke-direct {v0}, Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;->INSTANCE:Lcom/squareup/ui/login/PostInstallEncryptedEmail$NoPostInstallLoginEncryptedEmail;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearEncryptedEmail()V
    .locals 0

    return-void
.end method

.method public decryptEmail(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "encryptedEmail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-static {}, Lio/reactivex/Maybe;->never()Lio/reactivex/Maybe;

    move-result-object p1

    const-string v0, "never()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getEncryptedEmail()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
