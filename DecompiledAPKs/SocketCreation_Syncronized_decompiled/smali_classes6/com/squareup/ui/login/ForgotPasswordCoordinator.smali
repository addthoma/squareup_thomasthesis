.class public final Lcom/squareup/ui/login/ForgotPasswordCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ForgotPasswordCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nForgotPasswordCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ForgotPasswordCoordinator.kt\ncom/squareup/ui/login/ForgotPasswordCoordinator\n*L\n1#1,130:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u0001\u001f\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0016J\u0010\u0010%\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0002J\r\u0010&\u001a\u00020\"H\u0000\u00a2\u0006\u0002\u0008\'J\u0006\u0010(\u001a\u00020\"R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b8@@@X\u0080\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u00188@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010 \u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/ui/login/ForgotPasswordCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "value",
        "",
        "email",
        "getEmail$authenticator_views_release",
        "()Ljava/lang/String;",
        "setEmail$authenticator_views_release",
        "(Ljava/lang/String;)V",
        "emailField",
        "Lcom/squareup/ui/XableEditText;",
        "emailSuggestion",
        "Landroid/widget/TextView;",
        "emailSuggestionHandler",
        "Lcom/squareup/widgets/EmailSuggestionHandler;",
        "isEmailValid",
        "",
        "isEmailValid$authenticator_views_release",
        "()Z",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "watcher",
        "com/squareup/ui/login/ForgotPasswordCoordinator$watcher$1",
        "Lcom/squareup/ui/login/ForgotPasswordCoordinator$watcher$1;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "indicateEmailError",
        "indicateEmailError$authenticator_views_release",
        "updateEnabledState",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private emailField:Lcom/squareup/ui/XableEditText;

.field private emailSuggestion:Landroid/widget/TextView;

.field private emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;

.field private final watcher:Lcom/squareup/ui/login/ForgotPasswordCoordinator$watcher$1;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->screenData:Lio/reactivex/Observable;

    .line 41
    new-instance p1, Lcom/squareup/ui/login/ForgotPasswordCoordinator$watcher$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$watcher$1;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)V

    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->watcher:Lcom/squareup/ui/login/ForgotPasswordCoordinator$watcher$1;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEmailField$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "emailField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEmailSuggestionHandler$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)Lcom/squareup/widgets/EmailSuggestionHandler;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    if-nez p0, :cond_0

    const-string v0, "emailSuggestionHandler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setEmailField$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method public static final synthetic access$setEmailSuggestionHandler$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;Lcom/squareup/widgets/EmailSuggestionHandler;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 123
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 124
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 125
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 126
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->email_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    .line 127
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->email_suggestion:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailSuggestion:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->bindViews(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->forgot_password_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->forgot_password_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    const-string v1, "emailField"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;

    iget-object v2, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailSuggestion:Landroid/widget/TextView;

    if-nez v2, :cond_3

    const-string v3, "emailSuggestion"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {v0, v2, p1}, Lcom/squareup/widgets/EmailSuggestionHandler;->wireEmailSuggestions(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)Lcom/squareup/widgets/EmailSuggestionHandler;

    move-result-object v0

    const-string v2, "wireEmailSuggestions(ema\u2026d, emailSuggestion, view)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->watcher:Lcom/squareup/ui/login/ForgotPasswordCoordinator$watcher$1;

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->screenData:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$screens$1;->INSTANCE:Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$screens$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v1

    const-string v2, "screens.firstElement()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v2, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$2;->INSTANCE:Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/util/rx2/Rx2Kt;->mapNotNull(Lio/reactivex/Maybe;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Maybe;

    move-result-object v1

    .line 103
    new-instance v2, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$3;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Maybe;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 105
    sget-object v1, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$4;->INSTANCE:Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$4;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens.map { it.isWorld\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance v1, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$5;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->updateEnabledState()V

    return-void
.end method

.method public final getEmail$authenticator_views_release()Ljava/lang/String;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "emailField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final indicateEmailError$authenticator_views_release()V
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {v0}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    .line 118
    new-instance v1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    iget-object v2, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    const-string v3, "emailField"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final isEmailValid$authenticator_views_release()Z
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->getEmail$authenticator_views_release()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final setEmail$authenticator_views_release(Ljava/lang/String;)V
    .locals 2

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->emailField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "emailField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final updateEnabledState()V
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->isEmailValid$authenticator_views_release()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
