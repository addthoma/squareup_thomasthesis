.class final Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LoginAlertDialogFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/LoginAlertDialogFactory;->addStackedButton(Landroid/view/ViewGroup;Lio/reactivex/Observable;Lcom/squareup/protos/multipass/mobile/AlertButton;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $alertButton:Lcom/squareup/protos/multipass/mobile/AlertButton;

.field final synthetic $button:Landroid/widget/TextView;

.field final synthetic $inputs:Lio/reactivex/Observable;

.field final synthetic this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/LoginAlertDialogFactory;Lio/reactivex/Observable;Landroid/widget/TextView;Lcom/squareup/protos/multipass/mobile/AlertButton;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory;

    iput-object p2, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->$inputs:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->$button:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->$alertButton:Lcom/squareup/protos/multipass/mobile/AlertButton;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->$inputs:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;-><init>(Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "inputs.subscribe { input\u2026t, alertButton) }\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
