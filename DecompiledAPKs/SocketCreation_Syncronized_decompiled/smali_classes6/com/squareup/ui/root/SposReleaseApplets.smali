.class public Lcom/squareup/ui/root/SposReleaseApplets;
.super Ljava/lang/Object;
.source "SposReleaseApplets.java"

# interfaces
.implements Lcom/squareup/applet/Applets;


# instance fields
.field private final applets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryApplet;Lcom/squareup/ui/items/ItemsApplet;Lcom/squareup/reports/applet/ReportsApplet;Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/ui/help/HelpApplet;Lcom/squareup/ui/settings/SettingsApplet;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/applet/HistoryFactoryApplet;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p5, v0, p1

    const/4 p1, 0x2

    aput-object p10, v0, p1

    const/4 p1, 0x3

    aput-object p6, v0, p1

    const/4 p1, 0x4

    aput-object p3, v0, p1

    const/4 p1, 0x5

    aput-object p4, v0, p1

    const/4 p1, 0x6

    aput-object p9, v0, p1

    const/4 p1, 0x7

    aput-object p2, v0, p1

    const/16 p1, 0x8

    aput-object p8, v0, p1

    const/16 p1, 0x9

    aput-object p7, v0, p1

    const/16 p1, 0xa

    aput-object p11, v0, p1

    .line 40
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/root/SposReleaseApplets;->applets:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getApplets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/root/SposReleaseApplets;->applets:Ljava/util/List;

    return-object v0
.end method
