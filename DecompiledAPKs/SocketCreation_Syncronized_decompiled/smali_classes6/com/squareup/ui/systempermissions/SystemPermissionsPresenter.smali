.class public Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
.super Lmortar/Presenter;
.source "SystemPermissionsPresenter.java"

# interfaces
.implements Landroidx/core/app/ActivityCompat$OnRequestPermissionsResultCallback;
.implements Lcom/squareup/pauses/PausesAndResumes;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;,
        Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Landroid/app/Activity;",
        ">;",
        "Landroidx/core/app/ActivityCompat$OnRequestPermissionsResultCallback;",
        "Lcom/squareup/pauses/PausesAndResumes;"
    }
.end annotation


# static fields
.field public static final DETECTED_ON_RESUME:I = 0x2

.field private static final GRANTED_PERMISSIONS:Ljava/lang/String; = "granted-permissions"

.field public static final NO_CODE:I = 0x1


# instance fields
.field private final askedPermissions:Lcom/squareup/settings/EnumSetLocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;"
        }
    .end annotation
.end field

.field private final checker:Lcom/squareup/systempermissions/SystemPermissionsChecker;

.field lastGrantedNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumePresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

.field private final permissionsChanged:Lcom/jakewharton/rxrelay2/Relay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/Relay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/EnumSetLocalSetting;Lcom/squareup/pauses/PauseAndResumePresenter;Lcom/squareup/systempermissions/SystemPermissionsChecker;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/EnumSetLocalSetting<",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            "Lcom/squareup/systempermissions/SystemPermissionsChecker;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->listeners:Ljava/util/List;

    .line 48
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->permissionsChanged:Lcom/jakewharton/rxrelay2/Relay;

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->askedPermissions:Lcom/squareup/settings/EnumSetLocalSetting;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->pauseAndResumePresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    .line 61
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->checker:Lcom/squareup/systempermissions/SystemPermissionsChecker;

    return-void
.end method

.method private fireDenied(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 247
    invoke-virtual {p2}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Permission %s denied."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->permissionsChanged:Lcom/jakewharton/rxrelay2/Relay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/Relay;->accept(Ljava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;

    .line 250
    invoke-interface {v1, p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;->onDenied(ILcom/squareup/systempermissions/SystemPermission;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireGranted(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 239
    invoke-virtual {p2}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Permission %s granted."

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->permissionsChanged:Lcom/jakewharton/rxrelay2/Relay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/Relay;->accept(Ljava/lang/Object;)V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;

    .line 242
    invoke-interface {v1, p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;->onGranted(ILcom/squareup/systempermissions/SystemPermission;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addPermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public varargs allGranted([Lcom/squareup/systempermissions/SystemPermission;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->permissionsChanged:Lcom/jakewharton/rxrelay2/Relay;

    new-instance v1, Lcom/squareup/ui/systempermissions/-$$Lambda$SystemPermissionsPresenter$F0J49IC1ZrBrgsF99eKbRYfdmjc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/systempermissions/-$$Lambda$SystemPermissionsPresenter$F0J49IC1ZrBrgsF99eKbRYfdmjc;-><init>(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;[Lcom/squareup/systempermissions/SystemPermission;)V

    .line 234
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/Relay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 235
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public canAskDirectly(Lcom/squareup/systempermissions/SystemPermission;)Z
    .locals 0

    .line 180
    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->isAskable()Z

    move-result p1

    return p1
.end method

.method protected extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 65
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 31
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public varargs getFirstDenied([Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/systempermissions/SystemPermission;
    .locals 4

    .line 219
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 220
    invoke-virtual {p0, v2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->isDenied()Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;
    .locals 2

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 135
    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->checker:Lcom/squareup/systempermissions/SystemPermissionsChecker;

    invoke-interface {v1, p1, v0}, Lcom/squareup/systempermissions/SystemPermissionsChecker;->hasPermission(Lcom/squareup/systempermissions/SystemPermission;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    sget-object p1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->GRANTED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    return-object p1

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->askedPermissions:Lcom/squareup/settings/EnumSetLocalSetting;

    invoke-virtual {v1}, Lcom/squareup/settings/EnumSetLocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 138
    sget-object p1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->NEVER_ASKED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    return-object p1

    .line 139
    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/systempermissions/SystemPermission;->hasSelectivelyDenied(Landroid/app/Activity;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 140
    sget-object p1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->DENIED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    return-object p1

    .line 143
    :cond_2
    sget-object p1, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->ALWAYS_DENIED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    return-object p1

    .line 131
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No activity bound to presenter!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public has(Lcom/squareup/systempermissions/SystemPermission;)Z
    .locals 1

    .line 192
    sget-object v0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->GRANTED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public varargs hasAll([Lcom/squareup/systempermissions/SystemPermission;)Z
    .locals 4

    .line 196
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 197
    invoke-virtual {p0, v3}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->has(Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result v3

    if-nez v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public varargs hasNeverAsked([Lcom/squareup/systempermissions/SystemPermission;)Z
    .locals 5

    .line 206
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    .line 207
    invoke-virtual {p0, v3}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->NEVER_ASKED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public synthetic lambda$allGranted$0$SystemPermissionsPresenter([Lcom/squareup/systempermissions/SystemPermission;Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 234
    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->hasAll([Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->pauseAndResumePresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/pauses/PauseAndResumePresenter;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "granted-permissions"

    .line 116
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 70
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_2

    .line 71
    aget-object v2, p2, v1

    invoke-static {v2}, Lcom/squareup/systempermissions/SystemPermission;->fromPermissionName(Ljava/lang/String;)Lcom/squareup/systempermissions/SystemPermission;

    move-result-object v2

    .line 72
    aget v3, p3, v1

    if-nez v3, :cond_1

    .line 73
    iget-object v3, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 75
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    .line 76
    iget-object v3, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    invoke-virtual {v2}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->fireGranted(ILcom/squareup/systempermissions/SystemPermission;)V

    goto :goto_1

    :cond_1
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 80
    aget-object v4, p2, v1

    aput-object v4, v3, v0

    const-string v4, "Permission %s denied."

    invoke-static {v4, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-direct {p0, p1, v2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->fireDenied(ILcom/squareup/systempermissions/SystemPermission;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 8

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 90
    invoke-static {}, Lcom/squareup/systempermissions/SystemPermission;->values()[Lcom/squareup/systempermissions/SystemPermission;

    move-result-object v0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 92
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 93
    iget-object v5, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->checker:Lcom/squareup/systempermissions/SystemPermissionsChecker;

    invoke-interface {v5, v4, v1}, Lcom/squareup/systempermissions/SystemPermissionsChecker;->hasPermission(Lcom/squareup/systempermissions/SystemPermission;Landroid/content/Context;)Z

    move-result v5

    .line 94
    iget-object v6, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    invoke-virtual {v4}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x2

    if-nez v6, :cond_0

    if-eqz v5, :cond_0

    .line 98
    invoke-direct {p0, v7, v4}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->fireGranted(ILcom/squareup/systempermissions/SystemPermission;)V

    goto :goto_1

    :cond_0
    if-eqz v6, :cond_1

    if-nez v5, :cond_1

    .line 100
    invoke-direct {p0, v7, v4}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->fireDenied(ILcom/squareup/systempermissions/SystemPermission;)V

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 103
    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    :cond_3
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v1}, Lcom/squareup/systempermissions/SystemPermission;->allGrantedNames(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 124
    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->lastGrantedNames:Ljava/util/List;

    const-string v1, "granted-permissions"

    .line 125
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public removePermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public requestPermission(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 4

    .line 152
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 157
    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->checker:Lcom/squareup/systempermissions/SystemPermissionsChecker;

    invoke-interface {v1, p2, v0}, Lcom/squareup/systempermissions/SystemPermissionsChecker;->hasPermission(Lcom/squareup/systempermissions/SystemPermission;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->fireGranted(ILcom/squareup/systempermissions/SystemPermission;)V

    return-void

    .line 165
    :cond_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v1

    .line 168
    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->askedPermissions:Lcom/squareup/settings/EnumSetLocalSetting;

    invoke-virtual {v3}, Lcom/squareup/settings/EnumSetLocalSetting;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 169
    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v3, p0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->askedPermissions:Lcom/squareup/settings/EnumSetLocalSetting;

    invoke-virtual {v3, v2}, Lcom/squareup/settings/EnumSetLocalSetting;->set(Ljava/util/Set;)V

    .line 172
    sget-object v2, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->ALWAYS_DENIED:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    if-ne v1, v2, :cond_1

    .line 173
    invoke-static {v0}, Lcom/squareup/util/Intents;->getAppSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 175
    :cond_1
    invoke-virtual {p2, v0, p1}, Lcom/squareup/systempermissions/SystemPermission;->requestPermission(Landroid/app/Activity;I)V

    :goto_0
    return-void

    .line 153
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No activity bound to presenter!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public requestPermission(Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 1

    const/4 v0, 0x1

    .line 148
    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(ILcom/squareup/systempermissions/SystemPermission;)V

    return-void
.end method
