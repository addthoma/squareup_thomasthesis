.class public final Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;
.super Ljava/lang/Object;
.source "ChooseCardOnFileRowView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/tender/ChooseCardOnFileRowView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/tender/ChooseCardOnFileRowView;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/tender/ChooseCardOnFileRowView;Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/tender/ChooseCardOnFileRowView;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView_MembersInjector;->injectCurrencyCode(Lcom/squareup/ui/tender/AbstractCardOnFileRowView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView_MembersInjector;->injectDevice(Lcom/squareup/ui/tender/AbstractCardOnFileRowView;Lcom/squareup/util/Device;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView_MembersInjector;->injectFeatures(Lcom/squareup/ui/tender/AbstractCardOnFileRowView;Lcom/squareup/settings/server/Features;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->injectPresenter(Lcom/squareup/ui/tender/ChooseCardOnFileRowView;Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/ChooseCardOnFileRowView_MembersInjector;->injectMembers(Lcom/squareup/ui/tender/ChooseCardOnFileRowView;)V

    return-void
.end method
