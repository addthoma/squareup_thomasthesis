.class public Lcom/squareup/ui/tender/TenderOrderTicketNameView;
.super Landroid/widget/LinearLayout;
.source "TenderOrderTicketNameView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private nextButtonWrapper:Landroid/view/View;

.field private nextView:Landroid/widget/TextView;

.field private orderTicketName:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 52
    :cond_0
    const-class p2, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen$Component;->inject(Lcom/squareup/ui/tender/TenderOrderTicketNameView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 161
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 162
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->order_ticket_next:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->nextView:Landroid/widget/TextView;

    .line 163
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->order_ticket_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    .line 164
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->order_name_cardholder_name_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    .line 165
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->order_name_fetch_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->progressView:Landroid/view/View;

    .line 166
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->next_button_wrapper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->nextButtonWrapper:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getOrderTicketName()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$requestInitialFocus$0$TenderOrderTicketNameView()V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->bindViews()V

    .line 58
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->order_name_cardholder_name_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/TenderOrderTicketNameView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView$1;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNameView;)V

    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    .line 65
    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/tender/TenderOrderTicketNameView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView$2;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNameView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 78
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->nextView:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/tender/TenderOrderTicketNameView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView$3;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNameView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->nextButtonWrapper:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->nextView:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->expandTouchArea(Landroid/view/View;Landroid/view/View;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->presenter:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->presenter:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->presenter:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->dropView(Ljava/lang/Object;)V

    .line 108
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 98
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->nextButtonWrapper:Landroid/view/View;

    .line 102
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v2

    .line 101
    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/widget/EditText;->setPadding(IIII)V

    :cond_0
    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$TenderOrderTicketNameView$dSA4f8Aq_EPM7SzOU1vJqS22xms;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$TenderOrderTicketNameView$dSA4f8Aq_EPM7SzOU1vJqS22xms;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNameView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method selectAllText()V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    return-void
.end method

.method setCardholderClickable(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    .line 143
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 142
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    goto :goto_0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 146
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 149
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setClickable(Z)V

    return-void
.end method

.method setCardholderNameStatus(Ljava/lang/String;)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setOrderTicketName(Ljava/lang/String;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method setProgressVisible(Z)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->progressView:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
