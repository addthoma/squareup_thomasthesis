.class public Lcom/squareup/ui/tender/RealTenderStarter;
.super Ljava/lang/Object;
.source "RealTenderStarter.java"

# interfaces
.implements Lcom/squareup/ui/tender/TenderStarter;
.implements Lmortar/Scoped;


# instance fields
.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

.field private final checkoutflowConfigFactory:Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;

.field private enteringTenderPaymentFlow:Z

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final nfcState:Lcom/squareup/ui/AndroidNfcState;

.field private final paymentFlowHistoryFactoryStarter:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final readerIssueScreenRequestSink:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

.field private final swipeHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleter:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/ui/AndroidNfcState;Ldagger/Lazy;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/print/TicketAutoIdentifiers;Ldagger/Lazy;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ldagger/Lazy;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Ldagger/Lazy;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/AndroidNfcState;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->nfcState:Lcom/squareup/ui/AndroidNfcState;

    move-object v1, p10

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->paymentFlowHistoryFactoryStarter:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    move-object v1, p2

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->flow:Ldagger/Lazy;

    move-object v1, p3

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object v1, p4

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    move-object v1, p5

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p6

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

    move-object v1, p8

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-object v1, p9

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    move-object v1, p7

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->tenderCompleter:Ldagger/Lazy;

    move-object v1, p11

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object v1, p12

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->swipeHandler:Ldagger/Lazy;

    move-object v1, p13

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    move-object/from16 v1, p14

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->readerIssueScreenRequestSink:Ldagger/Lazy;

    move-object/from16 v1, p15

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    move-object/from16 v1, p16

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter;->checkoutflowConfigFactory:Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;

    return-void
.end method

.method private cartCanBeNamed()Z
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 294
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPaymentWithAtLeastOneTender()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 295
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->canStillNameCart()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private defaultPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 5

    .line 278
    new-instance v0, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 279
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/common/Money;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v2, v3, v4}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    iget-object v3, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    .line 280
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method private hasAnyItemToPrint()Z
    .locals 6

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object v0

    .line 300
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v1}, Lcom/squareup/print/PrinterStations;->getAllEnabledStations()Ljava/util/List;

    move-result-object v1

    .line 302
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStation;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/squareup/print/PrinterStation$Role;

    .line 303
    sget-object v5, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v5, v4, v3

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-virtual {v2, v4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 304
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 305
    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->categoryId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/print/PrinterStation;->isEnabledForCategoryId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v5

    :cond_2
    return v3
.end method

.method public static synthetic lambda$M58iDivgHqsIi6Jk5tV_QqM8k8E(Lcom/squareup/ui/tender/RealTenderStarter;Lcom/squareup/checkoutflow/PaymentProcessingResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/RealTenderStarter;->paymentProcessingComplete(Lcom/squareup/checkoutflow/PaymentProcessingResult;)V

    return-void
.end method

.method public static synthetic lambda$hwSUQvKsg0gLCGrfZRSa19B7vyM(Lcom/squareup/ui/tender/RealTenderStarter;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/RealTenderStarter;->showWorkflowScreenStack(Ljava/util/List;)V

    return-void
.end method

.method private paymentProcessingComplete(Lcom/squareup/checkoutflow/PaymentProcessingResult;)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;->enqueueBillInFlightAwaitingReceiptInfo()V

    .line 137
    sget-object v0, Lcom/squareup/checkoutflow/PaymentProcessingResult$CollectNextTender;->INSTANCE:Lcom/squareup/checkoutflow/PaymentProcessingResult$CollectNextTender;

    if-eq p1, v0, :cond_0

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/payment/Transaction;->setPaymentConfig(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)V

    :cond_0
    return-void
.end method

.method private showOrderTicketName()Z
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->cartCanBeNamed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->hasAnyItemToPrint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->isTicketAutoNumberingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

    invoke-interface {v0}, Lcom/squareup/print/TicketAutoIdentifiers;->requestTicketIdentifierIfNecessary()V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private showOverPaymentFlowBackground(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->paymentFlowHistoryFactoryStarter:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->showOverPaymentFlowBackground(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private showOverPaymentFlowBackground(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->paymentFlowHistoryFactoryStarter:Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->showOverPaymentFlowBackground(Ljava/util/List;)V

    return-void
.end method

.method private showWorkflowScreenStack(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->flow:Ldagger/Lazy;

    .line 323
    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;

    .line 325
    iget-boolean v1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->enteringTenderPaymentFlow:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 326
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/RealTenderStarter;->showOverPaymentFlowBackground(Ljava/util/List;)V

    goto :goto_0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-static {v0, p1}, Lcom/squareup/container/Flows;->pushStack(Lflow/Flow;Ljava/util/List;)V

    :goto_0
    const/4 p1, 0x0

    .line 331
    iput-boolean p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->enteringTenderPaymentFlow:Z

    return-void
.end method

.method private startPaymentWorkflow()V
    .locals 1

    .line 267
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;->DEFAULT:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/RealTenderStarter;->startPaymentWorkflow(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V

    return-void
.end method

.method private startPaymentWorkflow(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;)V
    .locals 2

    const/4 v0, 0x1

    .line 285
    iput-boolean v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->enteringTenderPaymentFlow:Z

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->getNetworkRequests()Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setNetworkRequestModifier(Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->getPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setPaymentConfig(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)V

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->start(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;)V

    return-void
.end method

.method private startPaymentWorkflow(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V
    .locals 2

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->checkoutflowConfigFactory:Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;

    .line 272
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->defaultPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;->createCheckoutFlowConfig(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    move-result-object p1

    .line 271
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/RealTenderStarter;->startPaymentWorkflow(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;)V

    return-void
.end method


# virtual methods
.method public advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z
    .locals 5

    .line 219
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    .line 250
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;->INSTANCE:Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return v2

    .line 240
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/ui/tender/legacy/R$string;->offline_mode_transaction_limit_title:I

    sget v4, Lcom/squareup/transaction/R$string;->offline_mode_transaction_limit_message:I

    invoke-direct {v1, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return v2

    .line 236
    :pswitch_2
    invoke-virtual {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->goToSplitTender()V

    return v2

    .line 232
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    return v2

    .line 228
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {p1, v2}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlowRecreatingSellerFlow(Z)V

    return v2

    .line 224
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {p1, v1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlowRecreatingSellerFlow(Z)V

    return v2

    :pswitch_6
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public completeTenderAndAdvance(Z)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->tenderCompleter:Ldagger/Lazy;

    .line 211
    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    .line 212
    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/RealTenderStarter;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    move-result p1

    return p1
.end method

.method public ensurePipTenderFlowIsShowing()V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->pipTenderScope()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/RealTenderStarter;->showOverPaymentFlowBackground(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public goToSplitTender()V
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->collectNextTender()V

    .line 203
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->startPaymentWorkflow()V

    return-void
.end method

.method public inTenderFlow(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 146
    const-class v0, Lcom/squareup/ui/tender/TenderScope;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    .line 126
    invoke-interface {v0}, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderStarter$hwSUQvKsg0gLCGrfZRSa19B7vyM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$RealTenderStarter$hwSUQvKsg0gLCGrfZRSa19B7vyM;-><init>(Lcom/squareup/ui/tender/RealTenderStarter;)V

    .line 127
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 125
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    .line 130
    invoke-interface {v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->onPaymentProcessingComplete()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$RealTenderStarter$M58iDivgHqsIi6Jk5tV_QqM8k8E;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$RealTenderStarter$M58iDivgHqsIi6Jk5tV_QqM8k8E;-><init>(Lcom/squareup/ui/tender/RealTenderStarter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 129
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public showNfcWarningScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startOrResumeTenderFlowAtPaymentPrompt()V
    .locals 1

    .line 169
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;->PAYMENT_PROMPT:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/RealTenderStarter;->startPaymentWorkflow(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V

    return-void
.end method

.method public startTenderFlow()V
    .locals 1

    .line 187
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->showOrderTicketName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameScreen;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/RealTenderStarter;->showOverPaymentFlowBackground(Lcom/squareup/container/ContainerTreeKey;)V

    return-void

    .line 191
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderStarter;->startPaymentWorkflow()V

    return-void
.end method

.method public startTenderFlowForPaymentConfig(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->checkoutflowConfigFactory:Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;

    sget-object v1, Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;->DEFAULT:Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;

    .line 175
    invoke-interface {v0, v1, p1}, Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;->createCheckoutFlowConfig(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    move-result-object p1

    .line 177
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/RealTenderStarter;->startPaymentWorkflow(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;)V

    return-void
.end method

.method public startTenderFlowWithSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->swipeHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/payment/SwipeHandler;->onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public warnIfNfcEnabled()Z
    .locals 3

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderStarter;->nfcState:Lcom/squareup/ui/AndroidNfcState;

    invoke-virtual {v0}, Lcom/squareup/ui/AndroidNfcState;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;

    new-instance v1, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;

    sget-object v2, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->NFC_ENABLED_WARNING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-direct {v1, v2}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)V

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderStarter;->readerIssueScreenRequestSink:Ldagger/Lazy;

    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
