.class public Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PayCardCnpDisabledScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/tender/PayCardCnpDisabledView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-void
.end method

.method private getMessageResId()I
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->hasPaymentStartedOnContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget v0, Lcom/squareup/ui/tender/legacy/R$string;->reader_sdk_swipe_dip_or_tap_card:I

    return v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isPaymentReadySmartReaderConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    sget v0, Lcom/squareup/ui/tender/legacy/R$string;->reader_sdk_swipe_or_dip_card:I

    return v0

    .line 88
    :cond_1
    sget v0, Lcom/squareup/ui/tender/legacy/R$string;->reader_sdk_swipe_card:I

    return v0
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$PayCardCnpDisabledScreen$Presenter(Lcom/squareup/ui/NfcAuthData;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, p1, Lcom/squareup/ui/NfcAuthData;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object p1, p1, Lcom/squareup/ui/NfcAuthData;->authorizationData:[B

    .line 97
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContactlessTenderInBuyerFlowResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->startMonitoringWithAutoFieldRestart()V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$PayCardCnpDisabledScreen$Presenter$A-E4sX4_N6Oxiclrwj2Il2gElMQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$PayCardCnpDisabledScreen$Presenter$A-E4sX4_N6Oxiclrwj2Il2gElMQ;-><init>(Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayCardCnpDisabledView;

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 72
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->getMessageResId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->setMessageText(I)V

    return-void
.end method
