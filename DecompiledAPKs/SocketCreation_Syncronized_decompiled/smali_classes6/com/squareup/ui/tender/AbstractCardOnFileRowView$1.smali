.class Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AbstractCardOnFileRowView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->addCardRow(IZLcom/squareup/Card$Brand;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/AbstractCardOnFileRowView;

.field final synthetic val$cardNameAndNumber:Ljava/lang/String;

.field final synthetic val$instrumentIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/AbstractCardOnFileRowView;ILjava/lang/String;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->this$0:Lcom/squareup/ui/tender/AbstractCardOnFileRowView;

    iput p2, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->val$instrumentIndex:I

    iput-object p3, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->val$cardNameAndNumber:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->this$0:Lcom/squareup/ui/tender/AbstractCardOnFileRowView;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->this$0:Lcom/squareup/ui/tender/AbstractCardOnFileRowView;

    iget v0, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->val$instrumentIndex:I

    iget-object v1, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;->val$cardNameAndNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->onChargeCardOnFileClicked(ILjava/lang/String;)V

    return-void
.end method
