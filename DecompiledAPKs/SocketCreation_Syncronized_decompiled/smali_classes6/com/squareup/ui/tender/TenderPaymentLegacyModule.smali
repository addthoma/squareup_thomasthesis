.class public abstract Lcom/squareup/ui/tender/TenderPaymentLegacyModule;
.super Ljava/lang/Object;
.source "TenderPaymentLegacyModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providePayCashScreenIdentifier()Lcom/squareup/tenderpayment/PayCashScreenIdentifier;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/ui/tender/RealPayCashScreenIdentifier;->INSTANCE:Lcom/squareup/ui/tender/RealPayCashScreenIdentifier;

    return-object v0
.end method


# virtual methods
.method abstract providePaymentWorkflowResultHandler(Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;)Lcom/squareup/tenderpayment/TenderPaymentResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderStarter(Lcom/squareup/ui/tender/RealTenderStarter;)Lcom/squareup/ui/tender/TenderStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
