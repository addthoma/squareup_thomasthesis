.class public interface abstract Lcom/squareup/ui/tender/TenderScope$Component;
.super Ljava/lang/Object;
.source "TenderScope.java"

# interfaces
.implements Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/tender/TenderScope$RunnerModule;,
        Lcom/squareup/ui/tender/TenderScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/TenderScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract changeHudToaster()Lcom/squareup/tenderpayment/ChangeHudToaster;
.end method

.method public abstract chooseCardOnFile()Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;
.end method

.method public abstract chooseCardOnFileConfirmation()Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Component;
.end method

.method public abstract chooseCustomerV2()Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Component;
.end method

.method public abstract giftCardSelect()Lcom/squareup/ui/tender/GiftCardSelectScreen$Component;
.end method

.method public abstract payCardCnpDisabled()Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Component;
.end method

.method public abstract payCardSwipeOnly()Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen$Component;
.end method

.method public abstract payCash()Lcom/squareup/ui/tender/PayCashScreen$Component;
.end method

.method public abstract payCreditCard()Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen$Component;
.end method

.method public abstract payGiftCard()Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen$Component;
.end method

.method public abstract payThirdPartyCard()Lcom/squareup/ui/tender/PayThirdPartyCardScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/tenderpayment/TenderScopeRunner;
.end method

.method public abstract tenderOrderTicketName()Lcom/squareup/ui/tender/TenderOrderTicketNameScreen$Component;
.end method

.method public abstract thirdPartyCardCharged()Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Component;
.end method
