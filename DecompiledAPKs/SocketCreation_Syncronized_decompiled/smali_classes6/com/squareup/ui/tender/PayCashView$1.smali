.class Lcom/squareup/ui/tender/PayCashView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "PayCashView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/PayCashView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/PayCashView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/PayCashView;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView$1;->this$0:Lcom/squareup/ui/tender/PayCashView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCashView$1;->this$0:Lcom/squareup/ui/tender/PayCashView;

    iget-object p1, p1, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/PayCashPresenter;->update()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 60
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCashView$1;->this$0:Lcom/squareup/ui/tender/PayCashView;

    iget-object p1, p1, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/PayCashPresenter;->onEditAmountBegin()V

    :cond_0
    return-void
.end method
