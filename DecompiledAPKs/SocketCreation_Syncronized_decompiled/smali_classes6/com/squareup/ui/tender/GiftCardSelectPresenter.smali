.class public Lcom/squareup/ui/tender/GiftCardSelectPresenter;
.super Lcom/squareup/mortar/ContextPresenter;
.source "GiftCardSelectPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/GiftCardSelectPresenter$GiftCardView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/ContextPresenter<",
        "Lcom/squareup/ui/tender/GiftCardSelectView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final flow:Lflow/Flow;

.field private final isTablet:Z

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/squareup/mortar/ContextPresenter;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->flow:Lflow/Flow;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 43
    invoke-interface {p5}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->isTablet:Z

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    return-void
.end method


# virtual methods
.method public giftCardOnFileClicked()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;->create(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/InTenderScope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 48
    invoke-super {p0, p1}, Lcom/squareup/mortar/ContextPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {p1}, Lcom/squareup/api/ApiTransactionState;->splitTenderSupported()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 51
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->canUseSplitTender()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 52
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-boolean v0, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->isTablet:Z

    .line 55
    invoke-interface {p1, v0}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildActionBarConfigSplittable(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-boolean v0, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->isTablet:Z

    .line 56
    invoke-interface {p1, v0}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 58
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/GiftCardSelectView;

    .line 59
    invoke-virtual {v0}, Lcom/squareup/ui/tender/GiftCardSelectView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 60
    invoke-virtual {v0}, Lcom/squareup/ui/tender/GiftCardSelectView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    .line 62
    new-instance p1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/GiftCardSelectView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->pay_gift_card_purchase_hint:I

    const-string v2, "dashboard"

    .line 63
    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->gift_card_order_url:I

    .line 64
    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/configure/item/R$string;->dashboard:I

    .line 65
    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 62
    invoke-virtual {v0, p1}, Lcom/squareup/ui/tender/GiftCardSelectView;->setHelperText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public scanOrEnterGiftCardClicked()V
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/tender/GiftCardSelectPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;

    sget-object v2, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
