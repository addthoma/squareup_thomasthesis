.class public final Lcom/squareup/ui/tender/legacy/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/legacy/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final card_on_file_card_expired_text:I = 0x7f0a02ba

.field public static final card_on_file_card_name_number:I = 0x7f0a02bb

.field public static final card_on_file_charge_button:I = 0x7f0a02bc

.field public static final choose_card_on_file_cards:I = 0x7f0a032d

.field public static final choose_card_on_file_cards_label:I = 0x7f0a032e

.field public static final choose_card_on_file_help_text:I = 0x7f0a032f

.field public static final choose_cof_customer_contact_list:I = 0x7f0a0330

.field public static final choose_cof_customer_search_box:I = 0x7f0a0331

.field public static final choose_cof_customer_search_message:I = 0x7f0a0332

.field public static final choose_cof_customer_search_progress_bar:I = 0x7f0a0333

.field public static final choose_gift_card_on_file:I = 0x7f0a0336

.field public static final cnp_disabled_view:I = 0x7f0a0360

.field public static final customer_cards_on_file:I = 0x7f0a0534

.field public static final enter_card_number_or_swipe:I = 0x7f0a0717

.field public static final gift_card_purchase_hint:I = 0x7f0a079f

.field public static final next_button_wrapper:I = 0x7f0a0a23

.field public static final order_name_cardholder_name_status:I = 0x7f0a0ad8

.field public static final order_name_fetch_progress:I = 0x7f0a0ada

.field public static final order_ticket_name_field:I = 0x7f0a0aef

.field public static final order_ticket_next:I = 0x7f0a0af0

.field public static final pay_cash_amount:I = 0x7f0a0bd8

.field public static final pay_cash_tender_button:I = 0x7f0a0bd9

.field public static final pay_third_party_amount:I = 0x7f0a0bdf

.field public static final pay_third_party_card_view:I = 0x7f0a0be0

.field public static final pay_third_party_helper_text:I = 0x7f0a0be1

.field public static final pay_third_party_tender_button:I = 0x7f0a0be2

.field public static final swipe_only_warning:I = 0x7f0a0f53


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
