.class public final Lcom/squareup/ui/tender/legacy/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/legacy/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_other:I = 0x7f12008f

.field public static final add_other_type:I = 0x7f120090

.field public static final card_on_file_action_bar_title:I = 0x7f120318

.field public static final card_on_file_charge_confirmation_body_with_type:I = 0x7f12031a

.field public static final crm_cardonfile_not_available_offline:I = 0x7f12062a

.field public static final customer_cards_on_file:I = 0x7f12079c

.field public static final customer_gift_card_on_file:I = 0x7f1207a3

.field public static final customer_no_cards_on_file:I = 0x7f1207a4

.field public static final customer_no_cards_on_file_help_text:I = 0x7f1207a5

.field public static final customer_no_gift_cards_on_file:I = 0x7f1207a7

.field public static final gift_card_enter_or_swipe:I = 0x7f120b15

.field public static final gift_card_on_file_action_bar_title:I = 0x7f120b21

.field public static final gift_card_order_url:I = 0x7f120b22

.field public static final kitchen_printing_cardholder_name_swipe:I = 0x7f120e88

.field public static final kitchen_printing_cardholder_name_swipe_or_insert:I = 0x7f120e89

.field public static final kitchen_printing_price_name_pattern:I = 0x7f120ea9

.field public static final manual_card_entry:I = 0x7f120f8d

.field public static final offline_mode_transaction_limit_title:I = 0x7f1210da

.field public static final pay_card_card_on_file_url:I = 0x7f12139b

.field public static final pay_card_on_file_title:I = 0x7f1213a1

.field public static final pay_card_swipe_title:I = 0x7f1213a2

.field public static final pay_cash_tender_button:I = 0x7f1213a5

.field public static final pay_cash_title:I = 0x7f1213a6

.field public static final pay_gift_card_purchase_hint:I = 0x7f1213aa

.field public static final payment_type_other:I = 0x7f121406

.field public static final payment_type_other_tablet:I = 0x7f121407

.field public static final reader_sdk_swipe_card:I = 0x7f121586

.field public static final reader_sdk_swipe_dip_or_tap_card:I = 0x7f121587

.field public static final reader_sdk_swipe_or_dip_card:I = 0x7f121588

.field public static final record_payment:I = 0x7f1215e3

.field public static final record_payment_action_label:I = 0x7f1215e4

.field public static final split_tender_secondary_button:I = 0x7f12185f

.field public static final swipe_credit_card:I = 0x7f1218ed

.field public static final uppercase_cards_on_file:I = 0x7f121b0d

.field public static final uppercase_gift_card_on_file:I = 0x7f121b2d

.field public static final uppercase_other:I = 0x7f121b58


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
