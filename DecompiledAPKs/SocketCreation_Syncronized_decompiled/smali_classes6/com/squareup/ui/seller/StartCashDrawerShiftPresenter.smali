.class public Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;
.super Lmortar/ViewPresenter;
.source "StartCashDrawerShiftPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static final CURRENT_AMOUNT_KEY:Ljava/lang/String; = "current_amount"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field currentStartingAmount:Ljava/lang/String;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/text/Formatter;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 43
    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShiftId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 44
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->setShowingCashDrawerDialog(Z)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->currentStartingAmount:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "current_amount"

    .line 47
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->currentStartingAmount:Ljava/lang/String;

    goto :goto_0

    .line 49
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 50
    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->getDefaultStartingCash()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->currentStartingAmount:Ljava/lang/String;

    .line 53
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    invoke-virtual {p1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->showDialog()V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->currentStartingAmount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->setDefaultStartingCash(Ljava/lang/String;)V

    goto :goto_1

    .line 56
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->setShowingCashDrawerDialog(Z)V

    :goto_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->isShowingDialog()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->getStartingCashAmount()Ljava/lang/String;

    move-result-object v0

    const-string v1, "current_amount"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public startCashDrawerShift(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->createNewOpenCashDrawerShift(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->setShowingCashDrawerDialog(Z)V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;

    invoke-virtual {p1}, Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;->dismissDialog()V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/seller/StartCashDrawerShiftPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CASH_MANAGEMENT_START_SHIFT_DRAWER_MODAL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method
