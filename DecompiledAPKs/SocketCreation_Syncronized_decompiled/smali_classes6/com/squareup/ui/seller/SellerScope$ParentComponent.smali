.class public interface abstract Lcom/squareup/ui/seller/SellerScope$ParentComponent;
.super Ljava/lang/Object;
.source "SellerScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/seller/SellerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentComponent"
.end annotation


# virtual methods
.method public abstract sellerScopePhone()Lcom/squareup/ui/seller/SellerScope$PhoneComponent;
.end method

.method public abstract sellerScopeTablet()Lcom/squareup/ui/seller/SellerScope$TabletComponent;
.end method

.method public abstract sellerWorkflowRunner()Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;
.end method
