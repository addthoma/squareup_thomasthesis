.class public final Lcom/squareup/utilities/threeten/compat/ZoneIdsKt;
.super Ljava/lang/Object;
.source "ZoneIds.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toTimeZone",
        "Ljava/util/TimeZone;",
        "Lorg/threeten/bp/ZoneId;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toTimeZone(Lorg/threeten/bp/ZoneId;)Ljava/util/TimeZone;
    .locals 1

    const-string v0, "$this$toTimeZone"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-static {p0}, Lorg/threeten/bp/DateTimeUtils;->toTimeZone(Lorg/threeten/bp/ZoneId;)Ljava/util/TimeZone;

    move-result-object p0

    const-string v0, "DateTimeUtils.toTimeZone(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
