.class public abstract Lcom/squareup/x2/NoX2MainActivityModule;
.super Ljava/lang/Object;
.source "NoX2MainActivityModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideHodorScreenRunner()Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 20
    new-instance v0, Lcom/squareup/x2/MaybeX2SellerScreenRunner$NoOpForProd;

    invoke-direct {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner$NoOpForProd;-><init>()V

    return-object v0
.end method

.method static provideX2TicketSession()Lcom/squareup/ui/ticket/X2TicketRunner;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/ui/ticket/NotX2TicketRunner;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/NotX2TicketRunner;-><init>()V

    return-object v0
.end method


# virtual methods
.method abstract providesProfileAttachmentsVisibility(Lcom/squareup/ui/crm/PosProfileAttachmentsVisibility;)Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
