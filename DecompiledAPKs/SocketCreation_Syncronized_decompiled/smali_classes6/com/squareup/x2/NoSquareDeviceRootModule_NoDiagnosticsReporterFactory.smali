.class public final Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory;
.super Ljava/lang/Object;
.source "NoSquareDeviceRootModule_NoDiagnosticsReporterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/DiagnosticsReporter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory$InstanceHolder;->access$000()Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory;

    move-result-object v0

    return-object v0
.end method

.method public static noDiagnosticsReporter()Lcom/squareup/payment/ledger/DiagnosticsReporter;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/x2/NoSquareDeviceRootModule;->noDiagnosticsReporter()Lcom/squareup/payment/ledger/DiagnosticsReporter;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/DiagnosticsReporter;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/DiagnosticsReporter;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory;->noDiagnosticsReporter()Lcom/squareup/payment/ledger/DiagnosticsReporter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/x2/NoSquareDeviceRootModule_NoDiagnosticsReporterFactory;->get()Lcom/squareup/payment/ledger/DiagnosticsReporter;

    move-result-object v0

    return-object v0
.end method
