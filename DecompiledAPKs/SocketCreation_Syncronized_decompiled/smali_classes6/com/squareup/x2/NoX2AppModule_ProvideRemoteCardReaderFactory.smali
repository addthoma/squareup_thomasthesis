.class public final Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;
.super Ljava/lang/Object;
.source "NoX2AppModule_ProvideRemoteCardReaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/BranRemoteCardReader;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;)",
            "Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRemoteCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/BranRemoteCardReader;
    .locals 0

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/x2/NoX2AppModule;->provideRemoteCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/BranRemoteCardReader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/BranRemoteCardReader;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/BranRemoteCardReader;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v1, p0, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, v1}, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->provideRemoteCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/BranRemoteCardReader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/x2/NoX2AppModule_ProvideRemoteCardReaderFactory;->get()Lcom/squareup/cardreader/BranRemoteCardReader;

    move-result-object v0

    return-object v0
.end method
