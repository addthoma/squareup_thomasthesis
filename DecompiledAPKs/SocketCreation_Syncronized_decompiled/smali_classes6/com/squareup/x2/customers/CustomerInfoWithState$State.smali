.class public final enum Lcom/squareup/x2/customers/CustomerInfoWithState$State;
.super Ljava/lang/Enum;
.source "CustomerInfoWithState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/x2/customers/CustomerInfoWithState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/x2/customers/CustomerInfoWithState$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/x2/customers/CustomerInfoWithState$State;

.field public static final enum CREATING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

.field public static final enum EDITING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 21
    new-instance v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    const/4 v1, 0x0

    const-string v2, "CREATING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/x2/customers/CustomerInfoWithState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->CREATING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    new-instance v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    const/4 v2, 0x1

    const-string v3, "EDITING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/x2/customers/CustomerInfoWithState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->EDITING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    .line 20
    sget-object v3, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->CREATING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->EDITING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->$VALUES:[Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/x2/customers/CustomerInfoWithState$State;
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/x2/customers/CustomerInfoWithState$State;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->$VALUES:[Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    invoke-virtual {v0}, [Lcom/squareup/x2/customers/CustomerInfoWithState$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    return-object v0
.end method
