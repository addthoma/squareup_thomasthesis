.class final Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;
.super Ljava/lang/Object;
.source "PrefsBackedContentProvider.kt"

# interfaces
.implements Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DummyPrefsHelper"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrefsBackedContentProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrefsBackedContentProvider.kt\ncom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,221:1\n152#2:222\n*E\n*S KotlinDebug\n*F\n+ 1 PrefsBackedContentProvider.kt\ncom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper\n*L\n216#1:222\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u0096\u0001J\u0019\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\t\u0010\u000b\u001a\u00020\u000cH\u0096\u0001\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
        "()V",
        "doQuery",
        "Landroid/database/Cursor;",
        "doUpdate",
        "",
        "uri",
        "Landroid/net/Uri;",
        "values",
        "Landroid/content/ContentValues;",
        "hasPref",
        "",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 216
    new-instance v0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;

    invoke-direct {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;-><init>()V

    sput-object v0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;->INSTANCE:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const-class v0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    const-string v1, "This is an unused PrefsHelper to simplify mapping a UriMatcher code to a PrefsHelper. All methods are unimplemented."

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    iput-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;->$$delegate_0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    return-void
.end method


# virtual methods
.method public doQuery()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;->$$delegate_0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    invoke-interface {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;->doQuery()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public doUpdate(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 1

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "values"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;->$$delegate_0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    invoke-interface {v0, p1, p2}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;->doUpdate(Landroid/net/Uri;Landroid/content/ContentValues;)V

    return-void
.end method

.method public hasPref()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;->$$delegate_0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    invoke-interface {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;->hasPref()Z

    move-result v0

    return v0
.end method
