.class public Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;
.super Landroid/content/ContentProvider;
.source "PrefsBackedContentProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrefsBackedContentProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrefsBackedContentProvider.kt\ncom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,221:1\n1651#2,3:222\n*E\n*S KotlinDebug\n*F\n+ 1 PrefsBackedContentProvider.kt\ncom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider\n*L\n114#1,3:222\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\'B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0010\u0008\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\tJ/\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0015H\u0016\u00a2\u0006\u0002\u0010\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0011\u001a\u00020\u0012J\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u0019H\u0016J\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\u0011\u001a\u00020\u0012JK\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\u0011\u001a\u00020\u00122\u000e\u0010 \u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00152\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00152\u0008\u0010!\u001a\u0004\u0018\u00010\u0005H\u0016\u00a2\u0006\u0002\u0010\"J\u0016\u0010#\u001a\u00020$2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u001cJ9\u0010#\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00052\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0015H\u0016\u00a2\u0006\u0002\u0010%J\u000c\u0010&\u001a\u00020\u000c*\u00020\u0012H\u0002R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;",
        "Landroid/content/ContentProvider;",
        "contract",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;",
        "fileName",
        "",
        "contextGetter",
        "Lkotlin/Function0;",
        "Landroid/content/Context;",
        "(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V",
        "prefsHelpers",
        "",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
        "uriMatcher",
        "Landroid/content/UriMatcher;",
        "delete",
        "",
        "uri",
        "Landroid/net/Uri;",
        "selection",
        "selectionArgs",
        "",
        "(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I",
        "getType",
        "hasData",
        "",
        "insert",
        "values",
        "Landroid/content/ContentValues;",
        "onCreate",
        "query",
        "Landroid/database/Cursor;",
        "projection",
        "sortOrder",
        "(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;",
        "update",
        "",
        "(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I",
        "getPrefsHelper",
        "DummyPrefsHelper",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contextGetter:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final contract:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

.field private final fileName:Ljava/lang/String;

.field private prefsHelpers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
            ">;"
        }
    .end annotation
.end field

.field private uriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    const-string v0, "contract"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->contract:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

    iput-object p2, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->fileName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->contextGetter:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 73
    check-cast p3, Lkotlin/jvm/functions/Function0;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;-><init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$getContract$p(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->contract:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

    return-object p0
.end method

.method public static final synthetic access$getFileName$p(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)Ljava/lang/String;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->fileName:Ljava/lang/String;

    return-object p0
.end method

.method private final getPrefsHelper(Landroid/net/Uri;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;
    .locals 3

    .line 210
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    if-nez v0, :cond_0

    const-string v1, "uriMatcher"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 211
    iget-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->prefsHelpers:Ljava/util/List;

    if-nez p1, :cond_1

    const-string v1, "prefsHelpers"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    return-object p1

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 211
    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    const-string p2, "uri"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    new-instance p2, Lkotlin/NotImplementedError;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "delete not implemented for "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public final hasData(Landroid/net/Uri;)Z
    .locals 1

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    invoke-direct {p0, p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->getPrefsHelper(Landroid/net/Uri;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;->hasPref()Z

    move-result p1

    return p1
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    const-string p2, "uri"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    new-instance p2, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "use update instead of insert for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public onCreate()Z
    .locals 8

    .line 81
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->contextGetter:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v1, "context!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    :goto_0
    new-instance v1, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;-><init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;Landroid/content/Context;)V

    .line 93
    new-instance v0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;

    invoke-direct {v0, p0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;-><init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)V

    .line 101
    invoke-virtual {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;->invoke()V

    .line 103
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v0, v2}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 113
    sget-object v2, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;->INSTANCE:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$DummyPrefsHelper;

    const/4 v3, 0x0

    invoke-interface {v0, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 114
    iget-object v2, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->contract:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

    invoke-interface {v2}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;->getItems()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 223
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_2
    check-cast v4, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;

    .line 116
    iget-object v3, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    if-nez v3, :cond_3

    const-string v6, "uriMatcher"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v6, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->contract:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

    invoke-interface {v6}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;->getAuthority()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 117
    invoke-virtual {v1, v4}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$1;->invoke(Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    move-result-object v3

    invoke-interface {v0, v5, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v3, v5

    goto :goto_1

    .line 119
    :cond_4
    iput-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->prefsHelpers:Ljava/util/List;

    const/4 v0, 0x1

    return v0
.end method

.method public final query(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 7

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    .line 127
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    if-nez p5, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->getPrefsHelper(Landroid/net/Uri;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    move-result-object p1

    .line 149
    invoke-interface {p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;->doQuery()Landroid/database/Cursor;

    move-result-object p1

    return-object p1

    .line 144
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "projection, selection, selectionArgs, and sortOrder are not supported. If you\'re calling this directly, consider PrefsBackedContentProvider.query(Uri)."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    if-nez p3, :cond_1

    if-nez p4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    .line 189
    invoke-direct {p0, p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->getPrefsHelper(Landroid/net/Uri;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;

    move-result-object p3

    .line 190
    invoke-interface {p3, p1, p2}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;->doUpdate(Landroid/net/Uri;Landroid/content/ContentValues;)V

    return v1

    .line 185
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "selection and selectionArgs are not supported. If you\'re calling this directly, consider PrefsBackedContentProvider.update(Uri, ContentValues)."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 182
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must supply exactly one value in values."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;)V
    .locals 1

    const-string v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "values"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 161
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method
