.class final Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;
.super Lkotlin/jvm/internal/Lambda;
.source "PrefsBackedContentProvider.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->onCreate()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrefsBackedContentProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrefsBackedContentProvider.kt\ncom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,221:1\n1360#2:222\n1429#2,3:223\n*E\n*S KotlinDebug\n*F\n+ 1 PrefsBackedContentProvider.kt\ncom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2\n*L\n94#1:222\n94#1,3:223\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "checkPaths",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;


# direct methods
.method constructor <init>(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;->this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;->this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;

    invoke-static {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->access$getContract$p(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;->getItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 222
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 223
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 224
    check-cast v2, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;

    .line 94
    invoke-interface {v2}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 225
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 95
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider$onCreate$2;->this$0:Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;

    invoke-static {v1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;->access$getContract$p(Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProvider;)Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Paths should be unique. Maybe your ContractItems have the same class names?"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
