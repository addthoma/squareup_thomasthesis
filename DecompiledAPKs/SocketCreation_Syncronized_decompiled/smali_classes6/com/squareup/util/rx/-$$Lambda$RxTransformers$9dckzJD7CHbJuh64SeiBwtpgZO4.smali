.class public final synthetic Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func1;


# instance fields
.field private final synthetic f$0:Lrx/Observable;

.field private final synthetic f$1:J

.field private final synthetic f$2:Ljava/util/concurrent/TimeUnit;

.field private final synthetic f$3:Lrx/Scheduler;

.field private final synthetic f$4:J


# direct methods
.method public synthetic constructor <init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$0:Lrx/Observable;

    iput-wide p2, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$1:J

    iput-object p4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$2:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$3:Lrx/Scheduler;

    iput-wide p6, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$4:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    iget-object v0, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$0:Lrx/Observable;

    iget-wide v1, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$1:J

    iget-object v3, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$2:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$3:Lrx/Scheduler;

    iget-wide v5, p0, Lcom/squareup/util/rx/-$$Lambda$RxTransformers$9dckzJD7CHbJuh64SeiBwtpgZO4;->f$4:J

    move-object v7, p1

    check-cast v7, Ljava/lang/Void;

    invoke-static/range {v0 .. v7}, Lcom/squareup/util/rx/RxTransformers;->lambda$null$10(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;JLjava/lang/Void;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
