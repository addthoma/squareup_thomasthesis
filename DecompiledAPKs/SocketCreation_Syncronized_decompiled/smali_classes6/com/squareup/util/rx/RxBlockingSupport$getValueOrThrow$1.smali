.class final Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RxBlockingSupport.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrThrow(Lrx/Observable;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;

    invoke-direct {v0}, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;-><init>()V

    sput-object v0, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;->INSTANCE:Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/util/rx/RxBlockingSupport$getValueOrThrow$1;->invoke()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/lang/Void;
    .locals 2

    .line 31
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected Observable to have cached value immediately available, but it didn\'t and would block instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
