.class public final Lcom/squareup/util/rx/RxNullValueHook;
.super Ljava/lang/Object;
.source "RxNullValueHook.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/rx/RxNullValueHook$NullNotAllowedException;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxNullValueHook.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxNullValueHook.kt\ncom/squareup/util/rx/RxNullValueHook\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,168:1\n136#1,8:171\n136#1,8:181\n1203#2,2:169\n1203#2,2:179\n10725#2:189\n10726#2:193\n1550#3,3:190\n*E\n*S KotlinDebug\n*F\n+ 1 RxNullValueHook.kt\ncom/squareup/util/rx/RxNullValueHook\n*L\n112#1,8:171\n129#1,8:181\n111#1,2:169\n126#1,2:179\n148#1:189\n148#1:193\n148#1,3:190\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001$B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u000f\u001a\u00020\u0010H\u0007J\u0012\u0010\u0011\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004H\u0007J!\u0010\u0012\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u0017H\u0082\u0010J\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0014\u0010\u0019\u001a\u00020\u00042\n\u0010\u001a\u001a\u00060\u001bj\u0002`\u001cH\u0002J1\u0010\u001d\u001a\u0002H\u001e\"\u0004\u0008\u0000\u0010\u001e*\u00020\u001f2\u0017\u0010 \u001a\u0013\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u0002H\u001e0!\u00a2\u0006\u0002\u0008\"H\u0082\u0008\u00a2\u0006\u0002\u0010#R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R,\u0010\u0008\u001a \u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\n\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000b\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000b0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R,\u0010\u000c\u001a \u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\r\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000e\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000e0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/util/rx/RxNullValueHook;",
        "",
        "()V",
        "crash",
        "",
        "ignoredClasses",
        "",
        "",
        "observableHook",
        "Lrx/functions/Func2;",
        "Lrx/Observable;",
        "Lrx/Observable$OnSubscribe;",
        "singleHook",
        "Lrx/Single;",
        "Lrx/Single$OnSubscribe;",
        "disable",
        "",
        "enable",
        "findOnAssemblySubscriber",
        "Lrx/Subscriber;",
        "subscription",
        "Lrx/Subscription;",
        "depth",
        "",
        "getAssemblyStacktrace",
        "ignoreException",
        "exception",
        "Ljava/lang/Exception;",
        "Lkotlin/Exception;",
        "getFieldValue",
        "T",
        "Ljava/lang/reflect/Field;",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "(Ljava/lang/reflect/Field;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "NullNotAllowedException",
        "pure"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/rx/RxNullValueHook;

.field private static crash:Z

.field private static final ignoredClasses:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final observableHook:Lrx/functions/Func2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func2<",
            "Lrx/Observable<",
            "*>;",
            "Lrx/Observable$OnSubscribe<",
            "*>;",
            "Lrx/Observable$OnSubscribe<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final singleHook:Lrx/functions/Func2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func2<",
            "Lrx/Single<",
            "*>;",
            "Lrx/Single$OnSubscribe<",
            "*>;",
            "Lrx/Single$OnSubscribe<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 25
    new-instance v0, Lcom/squareup/util/rx/RxNullValueHook;

    invoke-direct {v0}, Lcom/squareup/util/rx/RxNullValueHook;-><init>()V

    sput-object v0, Lcom/squareup/util/rx/RxNullValueHook;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook;

    const-string v1, "com.squareup.queue.sqlite.PendingCapturesSqliteStore"

    const-string v2, "com.squareup.queue.sqlite.TasksSqliteStore"

    const-string v3, "com.squareup.queue.sqlite.StoredPaymentsSqliteStore"

    const-string v4, "com.squareup.development.DelayingFileThreadExecutor"

    const-string v5, "com.squareup.payment.pending.PendingPayments"

    const-string v6, "com.squareup.util.StoppableHandlerExecutor$RemovingRunnableWrapper"

    const-string v7, "com.squareup.util.StoppableHandlerExecutor"

    const-string v8, "com.squareup.datafetch.AbstractLoader"

    const-string v9, "com.squareup.invoices.ui.InvoiceHistoryPresenter"

    const-string v10, "com.squareup.ui.crm.v2.AbstractViewCustomersListCoordinator"

    .line 49
    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    .line 29
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/rx/RxNullValueHook;->ignoredClasses:Ljava/util/Set;

    .line 53
    sget-object v0, Lcom/squareup/util/rx/RxNullValueHook$observableHook$1;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook$observableHook$1;

    check-cast v0, Lrx/functions/Func2;

    sput-object v0, Lcom/squareup/util/rx/RxNullValueHook;->observableHook:Lrx/functions/Func2;

    .line 79
    sget-object v0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook$singleHook$1;

    check-cast v0, Lrx/functions/Func2;

    sput-object v0, Lcom/squareup/util/rx/RxNullValueHook;->singleHook:Lrx/functions/Func2;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getAssemblyStacktrace(Lcom/squareup/util/rx/RxNullValueHook;Lrx/Subscription;)Ljava/lang/String;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/util/rx/RxNullValueHook;->getAssemblyStacktrace(Lrx/Subscription;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCrash$p(Lcom/squareup/util/rx/RxNullValueHook;)Z
    .locals 0

    .line 25
    sget-boolean p0, Lcom/squareup/util/rx/RxNullValueHook;->crash:Z

    return p0
.end method

.method public static final synthetic access$ignoreException(Lcom/squareup/util/rx/RxNullValueHook;Ljava/lang/Exception;)Z
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/util/rx/RxNullValueHook;->ignoreException(Ljava/lang/Exception;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setCrash$p(Lcom/squareup/util/rx/RxNullValueHook;Z)V
    .locals 0

    .line 25
    sput-boolean p1, Lcom/squareup/util/rx/RxNullValueHook;->crash:Z

    return-void
.end method

.method public static final disable()V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    .line 160
    invoke-static {v0}, Lrx/plugins/RxJavaHooks;->setOnObservableStart(Lrx/functions/Func2;)V

    .line 161
    invoke-static {v0}, Lrx/plugins/RxJavaHooks;->setOnSingleStart(Lrx/functions/Func2;)V

    return-void
.end method

.method public static final enable()V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/squareup/util/rx/RxNullValueHook;->enable$default(ZILjava/lang/Object;)V

    return-void
.end method

.method public static final enable(Z)V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 154
    sput-boolean p0, Lcom/squareup/util/rx/RxNullValueHook;->crash:Z

    .line 155
    sget-object p0, Lcom/squareup/util/rx/RxNullValueHook;->observableHook:Lrx/functions/Func2;

    invoke-static {p0}, Lrx/plugins/RxJavaHooks;->setOnObservableStart(Lrx/functions/Func2;)V

    .line 156
    sget-object p0, Lcom/squareup/util/rx/RxNullValueHook;->singleHook:Lrx/functions/Func2;

    invoke-static {p0}, Lrx/plugins/RxJavaHooks;->setOnSingleStart(Lrx/functions/Func2;)V

    return-void
.end method

.method public static synthetic enable$default(ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    .line 153
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/rx/RxNullValueHook;->enable(Z)V

    return-void
.end method

.method private final findOnAssemblySubscriber(Lrx/Subscription;I)Lrx/Subscriber;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscription;",
            "I)",
            "Lrx/Subscriber<",
            "*>;"
        }
    .end annotation

    :goto_0
    const/16 v0, 0x64

    const/4 v1, 0x0

    if-le p2, v0, :cond_0

    return-object v1

    .line 121
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "subscription::class.java.name"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v4, "OnAssemblySubscriber"

    invoke-static {v0, v4, v3, v2, v1}, Lkotlin/text/StringsKt;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    .line 122
    check-cast p1, Lrx/Subscriber;

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type rx.Subscriber<*>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 125
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    const-string v2, "subscription::class.java.declaredFields"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    array-length v2, v0

    :goto_1
    if-ge v3, v2, :cond_4

    aget-object v4, v0, v3

    .line 127
    const-class v5, Lrx/Subscription;

    const-string v6, "it"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    move-object v4, v1

    :goto_2
    if-eqz v4, :cond_6

    .line 181
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v2, 0x1

    .line 184
    :try_start_0
    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 129
    :cond_5
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    goto :goto_3

    :catchall_0
    move-exception p1

    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    throw p1

    :cond_6
    move-object p1, v1

    :goto_3
    instance-of v0, p1, Lrx/Subscription;

    if-nez v0, :cond_7

    move-object p1, v1

    :cond_7
    check-cast p1, Lrx/Subscription;

    if-eqz p1, :cond_8

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_8
    return-object v1
.end method

.method static synthetic findOnAssemblySubscriber$default(Lcom/squareup/util/rx/RxNullValueHook;Lrx/Subscription;IILjava/lang/Object;)Lrx/Subscriber;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 117
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/util/rx/RxNullValueHook;->findOnAssemblySubscriber(Lrx/Subscription;I)Lrx/Subscriber;

    move-result-object p0

    return-object p0
.end method

.method private final getAssemblyStacktrace(Lrx/Subscription;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 109
    invoke-static {p0, p1, v0, v2, v1}, Lcom/squareup/util/rx/RxNullValueHook;->findOnAssemblySubscriber$default(Lcom/squareup/util/rx/RxNullValueHook;Lrx/Subscription;IILjava/lang/Object;)Lrx/Subscriber;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 110
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    const-string v3, "assemblySubscriber::class.java.declaredFields"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    const-string v5, "it"

    .line 111
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "stacktrace"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v4, v1

    :goto_1
    if-eqz v4, :cond_4

    .line 171
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v2, 0x1

    .line 174
    :try_start_0
    invoke-virtual {v4, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 112
    :cond_2
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    instance-of v0, p1, Ljava/lang/String;

    if-nez v0, :cond_3

    move-object p1, v1

    :cond_3
    check-cast p1, Ljava/lang/String;

    return-object p1

    :catchall_0
    move-exception p1

    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    throw p1

    :cond_4
    return-object v1
.end method

.method private final getFieldValue(Ljava/lang/reflect/Field;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Field;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/reflect/Field;",
            "+TT;>;)TT;"
        }
    .end annotation

    .line 136
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->isAccessible()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 139
    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 141
    :cond_0
    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 143
    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-object p2

    :catchall_0
    move-exception p2

    .line 144
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 143
    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p2
.end method

.method private final ignoreException(Ljava/lang/Exception;)Z
    .locals 8

    .line 148
    invoke-virtual {p1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p1

    const-string v0, "exception.stackTrace"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    if-ge v2, v0, :cond_4

    aget-object v4, p1, v2

    .line 149
    sget-object v5, Lcom/squareup/util/rx/RxNullValueHook;->ignoredClasses:Ljava/util/Set;

    check-cast v5, Ljava/lang/Iterable;

    .line 190
    instance-of v6, v5, Ljava/util/Collection;

    if-eqz v6, :cond_1

    move-object v6, v5

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 191
    :cond_1
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 149
    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    return v1
.end method
