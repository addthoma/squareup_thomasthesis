.class public interface abstract Lcom/squareup/util/ForegroundActivityProvider;
.super Ljava/lang/Object;
.source "ForegroundActivityProvider.java"


# virtual methods
.method public abstract getForegroundActivity()Landroid/app/Activity;
.end method

.method public abstract latestCreatedActivity()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract resumedActivity()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end method
