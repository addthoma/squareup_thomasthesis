.class final Lcom/squareup/util/Views$scrollToVisible$1;
.super Ljava/lang/Object;
.source "Views.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Views;->scrollToVisible(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_scrollToVisible:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/Views$scrollToVisible$1;->$this_scrollToVisible:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 480
    iget-object v0, p0, Lcom/squareup/util/Views$scrollToVisible$1;->$this_scrollToVisible:Landroid/view/View;

    const/4 v1, 0x0

    .line 481
    move-object v2, v1

    check-cast v2, Landroid/widget/ScrollView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 485
    instance-of v5, v0, Landroid/widget/ScrollView;

    if-eqz v5, :cond_0

    .line 486
    move-object v2, v0

    check-cast v2, Landroid/widget/ScrollView;

    goto :goto_1

    .line 489
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v4, v5

    .line 492
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 493
    instance-of v5, v0, Landroid/view/View;

    if-eqz v5, :cond_1

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 497
    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/widget/ScrollView;->scrollTo(II)V

    :cond_3
    return-void
.end method
