.class final Lcom/squareup/util/picasso/PicassoHelpers$1;
.super Ljava/lang/Object;
.source "PicassoHelpers.java"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/picasso/PicassoHelpers;->lambda$toSingle$0(Lcom/squareup/picasso/RequestCreator;Lrx/SingleSubscriber;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$subscriber:Lrx/SingleSubscriber;


# direct methods
.method constructor <init>(Lrx/SingleSubscriber;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/util/picasso/PicassoHelpers$1;->val$subscriber:Lrx/SingleSubscriber;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 28
    iget-object p1, p0, Lcom/squareup/util/picasso/PicassoHelpers$1;->val$subscriber:Lrx/SingleSubscriber;

    invoke-virtual {p1}, Lrx/SingleSubscriber;->isUnsubscribed()Z

    move-result p1

    if-nez p1, :cond_0

    .line 29
    iget-object p1, p0, Lcom/squareup/util/picasso/PicassoHelpers$1;->val$subscriber:Lrx/SingleSubscriber;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lrx/SingleSubscriber;->onSuccess(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 0

    .line 22
    iget-object p2, p0, Lcom/squareup/util/picasso/PicassoHelpers$1;->val$subscriber:Lrx/SingleSubscriber;

    invoke-virtual {p2}, Lrx/SingleSubscriber;->isUnsubscribed()Z

    move-result p2

    if-nez p2, :cond_0

    .line 23
    iget-object p2, p0, Lcom/squareup/util/picasso/PicassoHelpers$1;->val$subscriber:Lrx/SingleSubscriber;

    invoke-virtual {p2, p1}, Lrx/SingleSubscriber;->onSuccess(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method
