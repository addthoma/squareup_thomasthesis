.class Lcom/squareup/util/MailCheck$EmailSplit;
.super Ljava/lang/Object;
.source "MailCheck.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/MailCheck;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EmailSplit"
.end annotation


# instance fields
.field public final domain:Ljava/lang/String;

.field public final localpart:Ljava/lang/String;

.field public final tld:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 403
    iput-object p1, p0, Lcom/squareup/util/MailCheck$EmailSplit;->localpart:Ljava/lang/String;

    .line 404
    iput-object p2, p0, Lcom/squareup/util/MailCheck$EmailSplit;->domain:Ljava/lang/String;

    .line 405
    iput-object p3, p0, Lcom/squareup/util/MailCheck$EmailSplit;->tld:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/util/MailCheck$1;)V
    .locals 0

    .line 397
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/util/MailCheck$EmailSplit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
