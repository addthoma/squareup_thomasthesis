.class final Lcom/squareup/util/CountryChecker$LineSegment;
.super Ljava/lang/Object;
.source "CountryChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/CountryChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LineSegment"
.end annotation


# instance fields
.field private final lengthSquared:D

.field private final p0:Lcom/squareup/util/CountryChecker$Point;

.field private final p0p1Latitude:D

.field private final p0p1Longitude:D

.field private final p1:Lcom/squareup/util/CountryChecker$Point;


# direct methods
.method constructor <init>(Lcom/squareup/util/CountryChecker$Point;Lcom/squareup/util/CountryChecker$Point;)V
    .locals 6

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    invoke-static {p2}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    move-object v5, p2

    move-object p2, p1

    move-object p1, v5

    .line 280
    :cond_0
    iput-object p1, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    .line 281
    iput-object p2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p1:Lcom/squareup/util/CountryChecker$Point;

    .line 283
    invoke-static {p2}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Latitude:D

    .line 284
    invoke-static {p2}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide p1

    sub-double/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Longitude:D

    .line 285
    iget-wide p1, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Latitude:D

    mul-double p1, p1, p1

    iget-wide v0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Longitude:D

    mul-double v0, v0, v0

    add-double/2addr p1, v0

    iput-wide p1, p0, Lcom/squareup/util/CountryChecker$LineSegment;->lengthSquared:D

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/util/CountryChecker$LineSegment;)Lcom/squareup/util/CountryChecker$Point;
    .locals 0

    .line 264
    iget-object p0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/util/CountryChecker$LineSegment;)Lcom/squareup/util/CountryChecker$Point;
    .locals 0

    .line 264
    iget-object p0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p1:Lcom/squareup/util/CountryChecker$Point;

    return-object p0
.end method

.method private squaredDistanceTo(Lcom/squareup/util/CountryChecker$Point;)D
    .locals 8

    .line 293
    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v2}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Latitude:D

    mul-double v0, v0, v2

    .line 294
    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    iget-object v4, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v4}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-wide v4, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Longitude:D

    mul-double v2, v2, v4

    add-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v4, v0, v2

    if-gtz v4, :cond_0

    .line 297
    iget-object v0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v0}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v2}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/util/CountryChecker$LineSegment;->squaredLength(DD)D

    move-result-wide v0

    return-wide v0

    .line 298
    :cond_0
    iget-wide v2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->lengthSquared:D

    cmpl-double v4, v0, v2

    if-ltz v4, :cond_1

    .line 299
    iget-object v0, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p1:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v0}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p1:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v2}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/util/CountryChecker$LineSegment;->squaredLength(DD)D

    move-result-wide v0

    return-wide v0

    :cond_1
    div-double/2addr v0, v2

    .line 302
    iget-object v2, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    invoke-static {v2}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Latitude:D

    mul-double v4, v4, v0

    add-double/2addr v2, v4

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$200(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-object v4, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0:Lcom/squareup/util/CountryChecker$Point;

    .line 303
    invoke-static {v4}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v4

    iget-wide v6, p0, Lcom/squareup/util/CountryChecker$LineSegment;->p0p1Longitude:D

    mul-double v0, v0, v6

    add-double/2addr v4, v0

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Point;->access$300(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    sub-double/2addr v4, v0

    .line 302
    invoke-direct {p0, v2, v3, v4, v5}, Lcom/squareup/util/CountryChecker$LineSegment;->squaredLength(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private squaredLength(DD)D
    .locals 0

    mul-double p1, p1, p1

    mul-double p3, p3, p3

    add-double/2addr p1, p3

    return-wide p1
.end method


# virtual methods
.method intersects(Lcom/squareup/util/CountryChecker$Circle;)Z
    .locals 4

    .line 289
    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Circle;->access$600(Lcom/squareup/util/CountryChecker$Circle;)Lcom/squareup/util/CountryChecker$Point;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/util/CountryChecker$LineSegment;->squaredDistanceTo(Lcom/squareup/util/CountryChecker$Point;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/squareup/util/CountryChecker$Circle;->access$700(Lcom/squareup/util/CountryChecker$Circle;)D

    move-result-wide v2

    cmpg-double p1, v0, v2

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
