.class final Lcom/squareup/util/AttachedSubscriptions;
.super Ljava/lang/Object;
.source "RxViews.kt"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxViews.kt\ncom/squareup/util/AttachedSubscriptions\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,258:1\n1642#2,2:259\n*E\n*S KotlinDebug\n*F\n+ 1 RxViews.kt\ncom/squareup/util/AttachedSubscriptions\n*L\n245#1,2:259\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0017\u0010\u0015\u001a\u00020\u00102\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0086\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\'\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/util/AttachedSubscriptions;",
        "Landroid/view/View$OnAttachStateChangeListener;",
        "()V",
        "subs",
        "Lrx/subscriptions/CompositeSubscription;",
        "getSubs",
        "()Lrx/subscriptions/CompositeSubscription;",
        "subscriptionFactories",
        "",
        "Lkotlin/Function0;",
        "Lrx/Subscription;",
        "getSubscriptionFactories",
        "()Ljava/util/List;",
        "subscriptionFactories$delegate",
        "Lkotlin/Lazy;",
        "clearAllSubscriptions",
        "",
        "onViewAttachedToWindow",
        "v",
        "Landroid/view/View;",
        "onViewDetachedFromWindow",
        "plusAssign",
        "subscriptionFactory",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final subs:Lrx/subscriptions/CompositeSubscription;

.field private final subscriptionFactories$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/util/AttachedSubscriptions;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 238
    sget-object v0, Lcom/squareup/util/AttachedSubscriptions$subscriptionFactories$2;->INSTANCE:Lcom/squareup/util/AttachedSubscriptions$subscriptionFactories$2;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/util/AttachedSubscriptions;->subscriptionFactories$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private final getSubscriptionFactories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/jvm/functions/Function0<",
            "Lrx/Subscription;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/util/AttachedSubscriptions;->subscriptionFactories$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final clearAllSubscriptions()V
    .locals 1

    .line 254
    invoke-direct {p0}, Lcom/squareup/util/AttachedSubscriptions;->getSubscriptionFactories()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 255
    iget-object v0, p0, Lcom/squareup/util/AttachedSubscriptions;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method

.method public final getSubs()Lrx/subscriptions/CompositeSubscription;
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/util/AttachedSubscriptions;->subs:Lrx/subscriptions/CompositeSubscription;

    return-object v0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    invoke-direct {p0}, Lcom/squareup/util/AttachedSubscriptions;->getSubscriptionFactories()Ljava/util/List;

    move-result-object p1

    .line 246
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 259
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 246
    iget-object v2, p0, Lcom/squareup/util/AttachedSubscriptions;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/Subscription;

    invoke-static {v2, v1}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    goto :goto_0

    .line 247
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object p1, p0, Lcom/squareup/util/AttachedSubscriptions;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1}, Lrx/subscriptions/CompositeSubscription;->clear()V

    return-void
.end method

.method public final plusAssign(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lrx/Subscription;",
            ">;)V"
        }
    .end annotation

    const-string v0, "subscriptionFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/squareup/util/AttachedSubscriptions;->getSubscriptionFactories()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method
