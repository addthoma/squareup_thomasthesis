.class public Lcom/squareup/util/TaxBreakdown;
.super Ljava/lang/Object;
.source "TaxBreakdown.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;,
        Lcom/squareup/util/TaxBreakdown$TaxInformation;,
        Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;,
        Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;
    }
.end annotation


# instance fields
.field public final sortedTaxBreakdownAmountsByTaxId:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;",
            ">;"
        }
    .end annotation
.end field

.field public final taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

.field public final taxLabelsByCartItemId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/SortedMap;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    .line 117
    iput-object p2, p0, Lcom/squareup/util/TaxBreakdown;->sortedTaxBreakdownAmountsByTaxId:Ljava/util/SortedMap;

    .line 118
    iput-object p3, p0, Lcom/squareup/util/TaxBreakdown;->taxLabelsByCartItemId:Ljava/util/Map;

    return-void
.end method

.method private static buildTaxLabelsByItemItemizationIdMap(Lcom/squareup/payment/Order;Ljava/util/Map;Ljava/util/TreeMap;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;",
            "Ljava/util/TreeMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 284
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    .line 285
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 286
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 287
    invoke-virtual {p0, v2}, Lcom/squareup/payment/Order;->getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object v4

    .line 288
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 289
    invoke-interface {p1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 291
    invoke-virtual {p2, v5}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;

    .line 292
    iget-object v5, v5, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->label:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 296
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 297
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private static buildTaxLabelsByItemItemizationIdMapForReturn(Lcom/squareup/checkout/ReturnCart;Ljava/util/Map;Ljava/util/TreeMap;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;",
            "Ljava/util/TreeMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 313
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 315
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object p0

    .line 316
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 317
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 318
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->appliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 319
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 320
    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 322
    invoke-virtual {p2, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;

    .line 323
    iget-object v4, v4, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->label:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 327
    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 328
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private static calculateTaxBreakdownType(ZI)Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;
    .locals 0

    if-nez p1, :cond_0

    .line 257
    sget-object p0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->NONE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    return-object p0

    :cond_0
    if-eqz p0, :cond_1

    .line 260
    sget-object p0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->SIMPLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    :goto_0
    return-object p0
.end method

.method static canShowTableBreakdown(Ljava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;)Z"
        }
    .end annotation

    .line 265
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/TaxBreakdown$TaxInformation;

    .line 266
    iget-object v1, v0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->after_application_total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_to_money:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    :cond_1
    const/4 p0, 0x0

    return p0

    :cond_2
    const/4 p0, 0x1

    return p0
.end method

.method private static create(Lcom/squareup/payment/Order;Ljava/util/Map;)Lcom/squareup/util/TaxBreakdown;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;)",
            "Lcom/squareup/util/TaxBreakdown;"
        }
    .end annotation

    .line 169
    invoke-static {p1}, Lcom/squareup/util/TaxBreakdown;->getSortedTaxBreakdownAmountsByTaxId(Ljava/util/Map;)Ljava/util/TreeMap;

    move-result-object v0

    .line 173
    invoke-static {p1}, Lcom/squareup/util/TaxBreakdown;->canShowTableBreakdown(Ljava/util/Map;)Z

    move-result v1

    .line 174
    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v2

    .line 175
    invoke-static {v1, v2}, Lcom/squareup/util/TaxBreakdown;->calculateTaxBreakdownType(ZI)Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    move-result-object v1

    .line 180
    invoke-static {p0, p1, v0}, Lcom/squareup/util/TaxBreakdown;->buildTaxLabelsByItemItemizationIdMap(Lcom/squareup/payment/Order;Ljava/util/Map;Ljava/util/TreeMap;)Ljava/util/Map;

    move-result-object p0

    .line 183
    new-instance p1, Lcom/squareup/util/TaxBreakdown;

    invoke-direct {p1, v1, v0, p0}, Lcom/squareup/util/TaxBreakdown;-><init>(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/SortedMap;Ljava/util/Map;)V

    return-object p1
.end method

.method static createTaxInformationMap(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;"
        }
    .end annotation

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 100
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 101
    new-instance v2, Lcom/squareup/util/TaxBreakdown$TaxInformation;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/squareup/util/TaxBreakdown$TaxInformation;-><init>(Lcom/squareup/protos/client/bills/FeeLineItem;Lcom/squareup/util/TaxBreakdown$1;)V

    .line 102
    iget-object v1, v2, Lcom/squareup/util/TaxBreakdown$TaxInformation;->taxId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/util/TaxBreakdown;
    .locals 2

    .line 154
    instance-of v0, p0, Lcom/squareup/payment/OrderSnapshot;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->isRehydrated()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "OrderSnapshot is rehydrated"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->buildTopLevelFeeLineItems()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/TaxBreakdown;->createTaxInformationMap(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 159
    invoke-static {p0, v0}, Lcom/squareup/util/TaxBreakdown;->create(Lcom/squareup/payment/Order;Ljava/util/Map;)Lcom/squareup/util/TaxBreakdown;

    move-result-object p0

    return-object p0
.end method

.method public static fromOrderSnapshot(Lcom/squareup/payment/Order;Ljava/util/List;)Lcom/squareup/util/TaxBreakdown;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)",
            "Lcom/squareup/util/TaxBreakdown;"
        }
    .end annotation

    .line 163
    invoke-static {p1}, Lcom/squareup/util/TaxBreakdown;->createTaxInformationMap(Ljava/util/List;)Ljava/util/Map;

    move-result-object p1

    .line 164
    invoke-static {p0, p1}, Lcom/squareup/util/TaxBreakdown;->create(Lcom/squareup/payment/Order;Ljava/util/Map;)Lcom/squareup/util/TaxBreakdown;

    move-result-object p0

    return-object p0
.end method

.method public static fromReturnCart(Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/util/TaxBreakdown;
    .locals 4

    .line 188
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->buildTopLevelFeeLineItems()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/TaxBreakdown;->createTaxInformationMap(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 191
    invoke-static {v0}, Lcom/squareup/util/TaxBreakdown;->getSortedTaxBreakdownAmountsByTaxId(Ljava/util/Map;)Ljava/util/TreeMap;

    move-result-object v1

    .line 195
    invoke-static {v0}, Lcom/squareup/util/TaxBreakdown;->canShowTableBreakdown(Ljava/util/Map;)Z

    move-result v2

    .line 196
    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    move-result v3

    .line 197
    invoke-static {v2, v3}, Lcom/squareup/util/TaxBreakdown;->calculateTaxBreakdownType(ZI)Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    move-result-object v2

    .line 202
    invoke-static {p0, v0, v1}, Lcom/squareup/util/TaxBreakdown;->buildTaxLabelsByItemItemizationIdMapForReturn(Lcom/squareup/checkout/ReturnCart;Ljava/util/Map;Ljava/util/TreeMap;)Ljava/util/Map;

    move-result-object p0

    .line 205
    new-instance v0, Lcom/squareup/util/TaxBreakdown;

    invoke-direct {v0, v2, v1, p0}, Lcom/squareup/util/TaxBreakdown;-><init>(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/SortedMap;Ljava/util/Map;)V

    return-object v0
.end method

.method private static getSortedTaxBreakdownAmountsByTaxId(Ljava/util/Map;)Ljava/util/TreeMap;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$TaxInformation;",
            ">;)",
            "Ljava/util/TreeMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 212
    invoke-interface/range {p0 .. p0}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0xf

    if-gt v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v3, "Must supply between 0 and 15 taxes."

    invoke-static {v1, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 213
    new-instance v1, Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;

    invoke-direct {v1, v0}, Lcom/squareup/util/TaxBreakdown$TaxBreakdownTableDisplayComparator;-><init>(Ljava/util/Map;)V

    .line 217
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 219
    invoke-static {v3, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 222
    new-instance v4, Ljava/util/ArrayList;

    const-string v5, "A"

    const-string v6, "B"

    const-string v7, "C"

    const-string v8, "D"

    const-string v9, "E"

    const-string v10, "F"

    const-string v11, "G"

    const-string v12, "H"

    const-string v13, "I"

    const-string v14, "J"

    const-string v15, "K"

    const-string v16, "L"

    const-string v17, "M"

    const-string v18, "N"

    const-string v19, "O"

    filled-new-array/range {v5 .. v19}, [Ljava/lang/String;

    move-result-object v5

    .line 223
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 228
    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 231
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 232
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/util/TaxBreakdown$TaxInformation;

    .line 235
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    :goto_2
    move-object v8, v7

    .line 237
    iget-object v9, v6, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    .line 239
    new-instance v13, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;

    iget-object v7, v6, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v10, v7, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_to_money:Lcom/squareup/protos/common/Money;

    iget-object v7, v6, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v11, v7, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v6, v6, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v12, v6, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->after_application_total_money:Lcom/squareup/protos/common/Money;

    move-object v6, v13

    move-object v7, v3

    invoke-direct/range {v6 .. v12}, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 243
    invoke-virtual {v5, v3, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-object v5
.end method
