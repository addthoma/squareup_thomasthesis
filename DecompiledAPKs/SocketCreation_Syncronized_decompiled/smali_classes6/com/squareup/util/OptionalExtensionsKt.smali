.class public final Lcom/squareup/util/OptionalExtensionsKt;
.super Ljava/lang/Object;
.source "OptionalExtensions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOptionalExtensions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OptionalExtensions.kt\ncom/squareup/util/OptionalExtensionsKt\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,52:1\n19#2:53\n24#2:54\n*E\n*S KotlinDebug\n*F\n+ 1 OptionalExtensions.kt\ncom/squareup/util/OptionalExtensionsKt\n*L\n12#1:53\n27#1:54\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\u001a\"\u0010\u0005\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u0002\u0012\u0004\u0012\u0002H\u00030\u0006\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\u001a&\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0008\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0008\u001a&\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\t\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\t\u001a&\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u000b\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u0008\u0012\u0004\u0012\u0002H\u00030\u000c\u001a&\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u000c\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u000b\u00a8\u0006\u000e"
    }
    d2 = {
        "mapIfPresentFlowable",
        "Lio/reactivex/FlowableTransformer;",
        "Lcom/squareup/util/Optional;",
        "T",
        "",
        "mapIfPresentObservable",
        "Lio/reactivex/ObservableTransformer;",
        "mapIfPresent",
        "Lio/reactivex/Flowable;",
        "Lio/reactivex/Observable;",
        "mapToOptional",
        "Lio/reactivex/Single;",
        "Lio/reactivex/Maybe;",
        "takeIfPresent",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final mapIfPresent(Lio/reactivex/Flowable;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Flowable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;)",
            "Lio/reactivex/Flowable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$mapIfPresent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    const-class v0, Lcom/squareup/util/Optional$Present;

    invoke-virtual {p0, v0}, Lio/reactivex/Flowable;->ofType(Ljava/lang/Class;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string v0, "ofType(T::class.java)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$mapIfPresent$2;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$mapIfPresent$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Flowable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string v0, "ofType<Present<T>>().map { it.value }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapIfPresent(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$mapIfPresent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    const-class v0, Lcom/squareup/util/Optional$Present;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "ofType(T::class.java)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$mapIfPresent$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$mapIfPresent$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "ofType<Present<T>>().map { it.value }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapIfPresentFlowable()Lio/reactivex/FlowableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/FlowableTransformer<",
            "Lcom/squareup/util/Optional<",
            "TT;>;TT;>;"
        }
    .end annotation

    .line 35
    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$mapIfPresentFlowable$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$mapIfPresentFlowable$1;

    check-cast v0, Lio/reactivex/FlowableTransformer;

    return-object v0
.end method

.method public static final mapIfPresentObservable()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/util/Optional<",
            "TT;>;TT;>;"
        }
    .end annotation

    .line 20
    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$mapIfPresentObservable$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$mapIfPresentObservable$1;

    check-cast v0, Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public static final mapToOptional(Lio/reactivex/Maybe;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Maybe<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "$this$mapToOptional"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$mapToOptional$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$mapToOptional$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p0

    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p0

    const-string v0, "map { Optional.of(it) }.toSingle(Optional.empty())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final takeIfPresent(Lio/reactivex/Single;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;)",
            "Lio/reactivex/Maybe<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$takeIfPresent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$takeIfPresent$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$takeIfPresent$1;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p0, v0}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p0

    sget-object v0, Lcom/squareup/util/OptionalExtensionsKt$takeIfPresent$2;->INSTANCE:Lcom/squareup/util/OptionalExtensionsKt$takeIfPresent$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p0

    const-string v0, "filter { it.isPresent }.map { it.value }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
