.class public final Lcom/squareup/util/Rx2Tuples;
.super Ljava/lang/Object;
.source "Rx2Tuples.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JA\u0010\u0003\u001a\u001a\u0012\u0004\u0012\u0002H\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00070\u00060\u0004\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u00012\u0006\u0010\u0008\u001a\u0002H\u0007H\u0007\u00a2\u0006\u0002\u0010\tJ:\u0010\n\u001a \u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u00060\u000b\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001H\u0007Jf\u0010\u000e\u001a8\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u0011\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00110\u00120\u000f\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001H\u0007Jl\u0010\u0013\u001a>\u0012\u0004\u0012\u0002H\u0011\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00100\u0014\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00100\u00120\u000b\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001H\u0007JP\u0010\u0015\u001a,\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00100\u00140\u0016\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001H\u0007JV\u0010\u0017\u001a2\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u0006\u0012\u0004\u0012\u0002H\u0010\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00100\u00140\u000b\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001H\u0007JV\u0010\u0018\u001a2\u0012\u0004\u0012\u0002H\u0010\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u0006\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u00140\u000b\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001H\u0007J>\u0010\u0019\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u00060\u001a\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001*\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u001bH\u0007JT\u0010\u001c\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u0006\u0012\u0004\u0012\u0002H\u001d0\u0004\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u001d*\u00020\u0001*\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u001d0\u001eH\u0007Jp\u0010\u001f\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00110\u00120\u001a\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001* \u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u00020 0\u000fH\u0007J\u0080\u0001\u0010!\u001a&\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00110\u0012\u0012\u0004\u0012\u0002H\u001d0\u0004\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0011*\u00020\u0001\"\u0008\u0008\u0004\u0010\u001d*\u00020\u0001* \u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u001d0\u000fH\u0007JZ\u0010\"\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00100\u00140\u001a\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001*\u001a\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u00020 0\u0016H\u0007Jj\u0010#\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00100\u0014\u0012\u0004\u0012\u0002H\u001d0\u0004\"\u0008\u0008\u0000\u0010\u000c*\u00020\u0001\"\u0008\u0008\u0001\u0010\r*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0010*\u00020\u0001\"\u0008\u0008\u0003\u0010\u001d*\u00020\u0001*\u001a\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u001d0\u0016H\u0007JZ\u0010$\u001a&\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00070\u0006\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H%0\u00060\u0004\"\u0008\u0008\u0000\u0010\u0005*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010%*\u00020\u0001*\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H%0\u0004H\u0007\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/util/Rx2Tuples;",
        "",
        "()V",
        "pairWithSecond",
        "Lio/reactivex/functions/Function;",
        "F",
        "Lkotlin/Pair;",
        "S",
        "second",
        "(Ljava/lang/Object;)Lio/reactivex/functions/Function;",
        "toPair",
        "Lio/reactivex/functions/BiFunction;",
        "A",
        "B",
        "toQuartet",
        "Lio/reactivex/functions/Function4;",
        "C",
        "D",
        "Lcom/squareup/util/tuple/Quartet;",
        "toQuartetFromSingle",
        "Lcom/squareup/util/tuple/Triplet;",
        "toTriplet",
        "Lio/reactivex/functions/Function3;",
        "toTripletFromPair",
        "toTripletFromSingle",
        "expandPair",
        "Lio/reactivex/functions/Consumer;",
        "Lio/reactivex/functions/BiConsumer;",
        "expandPairForFunc",
        "R",
        "Lkotlin/Function2;",
        "expandQuartet",
        "",
        "expandQuartetForFunc",
        "expandTriplet",
        "expandTripletForFunc",
        "mapSecond",
        "S2",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/Rx2Tuples;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/util/Rx2Tuples;

    invoke-direct {v0}, Lcom/squareup/util/Rx2Tuples;-><init>()V

    sput-object v0, Lcom/squareup/util/Rx2Tuples;->INSTANCE:Lcom/squareup/util/Rx2Tuples;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/BiConsumer<",
            "TA;TB;>;)",
            "Lio/reactivex/functions/Consumer<",
            "Lkotlin/Pair<",
            "TA;TB;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$expandPair"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/util/Rx2Tuples$expandPair$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$expandPair$1;-><init>(Lio/reactivex/functions/BiConsumer;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    return-object v0
.end method

.method public static final expandPairForFunc(Lkotlin/jvm/functions/Function2;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function2<",
            "-TA;-TB;+TR;>;)",
            "Lio/reactivex/functions/Function<",
            "Lkotlin/Pair<",
            "TA;TB;>;TR;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$expandPairForFunc"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/util/Rx2Tuples$expandPairForFunc$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$expandPairForFunc$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/Function;

    return-object v0
.end method

.method public static final expandQuartet(Lio/reactivex/functions/Function4;)Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function4<",
            "TA;TB;TC;TD;",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TA;TB;TC;TD;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$expandQuartet"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/util/Rx2Tuples$expandQuartet$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$expandQuartet$1;-><init>(Lio/reactivex/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    return-object v0
.end method

.method public static final expandQuartetForFunc(Lio/reactivex/functions/Function4;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function4<",
            "TA;TB;TC;TD;TR;>;)",
            "Lio/reactivex/functions/Function<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TA;TB;TC;TD;>;TR;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$expandQuartetForFunc"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/util/Rx2Tuples$expandQuartetForFunc$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$expandQuartetForFunc$1;-><init>(Lio/reactivex/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Function;

    return-object v0
.end method

.method public static final expandTriplet(Lio/reactivex/functions/Function3;)Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function3<",
            "TA;TB;TC;",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$expandTriplet"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/util/Rx2Tuples$expandTriplet$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$expandTriplet$1;-><init>(Lio/reactivex/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    return-object v0
.end method

.method public static final expandTripletForFunc(Lio/reactivex/functions/Function3;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function3<",
            "TA;TB;TC;TR;>;)",
            "Lio/reactivex/functions/Function<",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;TR;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$expandTripletForFunc"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/util/Rx2Tuples$expandTripletForFunc$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$expandTripletForFunc$1;-><init>(Lio/reactivex/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Function;

    return-object v0
.end method

.method public static final mapSecond(Lio/reactivex/functions/Function;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "TS;TS2;>;)",
            "Lio/reactivex/functions/Function<",
            "Lkotlin/Pair<",
            "TF;TS;>;",
            "Lkotlin/Pair<",
            "TF;TS2;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$mapSecond"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/util/Rx2Tuples$mapSecond$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$mapSecond$1;-><init>(Lio/reactivex/functions/Function;)V

    check-cast v0, Lio/reactivex/functions/Function;

    return-object v0
.end method

.method public static final pairWithSecond(Ljava/lang/Object;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(TS;)",
            "Lio/reactivex/functions/Function<",
            "TF;",
            "Lkotlin/Pair<",
            "TF;TS;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "second"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/squareup/util/Rx2Tuples$pairWithSecond$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/Rx2Tuples$pairWithSecond$1;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lio/reactivex/functions/Function;

    return-object v0
.end method

.method public static final toPair()Lio/reactivex/functions/BiFunction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/functions/BiFunction<",
            "TA;TB;",
            "Lkotlin/Pair<",
            "TA;TB;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 64
    sget-object v0, Lcom/squareup/util/Rx2Tuples$toPair$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toPair$1;

    check-cast v0, Lio/reactivex/functions/BiFunction;

    return-object v0
.end method

.method public static final toQuartet()Lio/reactivex/functions/Function4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/functions/Function4<",
            "TA;TB;TC;TD;",
            "Lcom/squareup/util/tuple/Quartet<",
            "TA;TB;TC;TD;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 83
    sget-object v0, Lcom/squareup/util/Rx2Tuples$toQuartet$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toQuartet$1;

    check-cast v0, Lio/reactivex/functions/Function4;

    return-object v0
.end method

.method public static final toQuartetFromSingle()Lio/reactivex/functions/BiFunction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            "D:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/functions/BiFunction<",
            "TD;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;",
            "Lcom/squareup/util/tuple/Quartet<",
            "TD;TA;TB;TC;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 88
    sget-object v0, Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;

    check-cast v0, Lio/reactivex/functions/BiFunction;

    return-object v0
.end method

.method public static final toTriplet()Lio/reactivex/functions/Function3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/functions/Function3<",
            "TA;TB;TC;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 69
    sget-object v0, Lcom/squareup/util/Rx2Tuples$toTriplet$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toTriplet$1;

    check-cast v0, Lio/reactivex/functions/Function3;

    return-object v0
.end method

.method public static final toTripletFromPair()Lio/reactivex/functions/BiFunction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/functions/BiFunction<",
            "Lkotlin/Pair<",
            "TA;TB;>;TC;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toTripletFromPair$1;

    check-cast v0, Lio/reactivex/functions/BiFunction;

    return-object v0
.end method

.method public static final toTripletFromSingle()Lio/reactivex/functions/BiFunction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            "C:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/functions/BiFunction<",
            "TC;",
            "Lkotlin/Pair<",
            "TA;TB;>;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TC;TA;TB;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 73
    sget-object v0, Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toTripletFromSingle$1;

    check-cast v0, Lio/reactivex/functions/BiFunction;

    return-object v0
.end method
