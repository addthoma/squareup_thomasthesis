.class public Lcom/squareup/util/ListPhrase;
.super Ljava/lang/Object;
.source "ListPhrase.java"


# instance fields
.field private final finalElementPattern:Lcom/squareup/phrase/Phrase;

.field private final nonFinalElementPattern:Lcom/squareup/phrase/Phrase;

.field private final twoElementPattern:Lcom/squareup/phrase/Phrase;


# direct methods
.method private constructor <init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)V
    .locals 1

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "two-element"

    .line 175
    invoke-static {v0, p1}, Lcom/squareup/util/ListPhrase;->validateListPattern(Ljava/lang/String;Lcom/squareup/phrase/Phrase;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/util/ListPhrase;->twoElementPattern:Lcom/squareup/phrase/Phrase;

    const-string p1, "non-final"

    .line 176
    invoke-static {p1, p2}, Lcom/squareup/util/ListPhrase;->validateListPattern(Ljava/lang/String;Lcom/squareup/phrase/Phrase;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/util/ListPhrase;->nonFinalElementPattern:Lcom/squareup/phrase/Phrase;

    const-string p1, "final"

    .line 177
    invoke-static {p1, p3}, Lcom/squareup/util/ListPhrase;->validateListPattern(Ljava/lang/String;Lcom/squareup/phrase/Phrase;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/util/ListPhrase;->finalElementPattern:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method private static asList(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 248
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_0

    .line 249
    check-cast p0, Ljava/util/List;

    goto :goto_1

    .line 251
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 252
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 253
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object p0, v0

    :goto_1
    return-object p0
.end method

.method private static formatIfNotNull(Ljava/lang/Object;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/squareup/text/Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    if-nez p1, :cond_1

    if-nez p0, :cond_0

    const-string p0, ""

    goto :goto_0

    .line 262
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    .line 265
    :cond_1
    invoke-interface {p1, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private formatList(Ljava/util/List;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;",
            "Lcom/squareup/text/Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 210
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_3

    const/4 v3, 0x2

    const-string v4, "b"

    const-string v5, "a"

    if-eq v0, v3, :cond_2

    .line 221
    iget-object v0, p0, Lcom/squareup/util/ListPhrase;->nonFinalElementPattern:Lcom/squareup/phrase/Phrase;

    .line 222
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/squareup/util/ListPhrase;->formatIfNotNull(Ljava/lang/Object;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 223
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 226
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 229
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 230
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 232
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 233
    iget-object v0, p0, Lcom/squareup/util/ListPhrase;->finalElementPattern:Lcom/squareup/phrase/Phrase;

    .line 237
    :cond_0
    invoke-virtual {v0, v5, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 238
    invoke-static {v2, p2}, Lcom/squareup/util/ListPhrase;->formatIfNotNull(Ljava/lang/Object;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 239
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_1
    return-object v1

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/squareup/util/ListPhrase;->twoElementPattern:Lcom/squareup/phrase/Phrase;

    .line 217
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/squareup/util/ListPhrase;->formatIfNotNull(Ljava/lang/Object;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 218
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/util/ListPhrase;->formatIfNotNull(Ljava/lang/Object;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, v4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 214
    :cond_3
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/util/ListPhrase;->formatIfNotNull(Ljava/lang/Object;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_4
    const-string p1, ""

    return-object p1
.end method

.method public static from(Landroid/content/Context;I)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 80
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1, p1, p1}, Lcom/squareup/util/ListPhrase;->from(Landroid/content/res/Resources;III)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/content/Context;III)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 93
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/util/ListPhrase;->from(Landroid/content/res/Resources;III)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/content/res/Resources;I)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 104
    invoke-static {p0, p1, p1, p1}, Lcom/squareup/util/ListPhrase;->from(Landroid/content/res/Resources;III)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/content/res/Resources;III)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 117
    invoke-static {p0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 118
    invoke-static {p0, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    invoke-static {p0, p3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 117
    invoke-static {p1, p2, p0}, Lcom/squareup/util/ListPhrase;->from(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/view/View;I)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 56
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1, p1, p1}, Lcom/squareup/util/ListPhrase;->from(Landroid/content/res/Resources;III)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/view/View;III)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/util/ListPhrase;->from(Landroid/content/res/Resources;III)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Lcom/squareup/phrase/Phrase;)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 138
    invoke-static {p0, p0, p0}, Lcom/squareup/util/ListPhrase;->from(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)Lcom/squareup/util/ListPhrase;
    .locals 1

    .line 165
    new-instance v0, Lcom/squareup/util/ListPhrase;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/util/ListPhrase;-><init>(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)V

    return-object v0
.end method

.method public static from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 128
    invoke-static {p0, p0, p0}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;
    .locals 0

    .line 151
    invoke-static {p0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 152
    invoke-static {p2}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 151
    invoke-static {p0, p1, p2}, Lcom/squareup/util/ListPhrase;->from(Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;Lcom/squareup/phrase/Phrase;)Lcom/squareup/util/ListPhrase;

    move-result-object p0

    return-object p0
.end method

.method private static validateListPattern(Ljava/lang/String;Lcom/squareup/phrase/Phrase;)Lcom/squareup/phrase/Phrase;
    .locals 3

    const-string v0, ""

    :try_start_0
    const-string v1, "a"

    .line 200
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "b"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 202
    :catch_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " list pattern should only contain keys {a} and {b}"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 185
    invoke-virtual {p0, p1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public format(Ljava/lang/Iterable;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "TT;>;",
            "Lcom/squareup/text/Formatter<",
            "TT;>;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 189
    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->asList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/util/ListPhrase;->formatList(Ljava/util/List;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public final varargs format([Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 181
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
