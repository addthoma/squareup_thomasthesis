.class final Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;
.super Ljava/lang/Object;
.source "Rx2Views.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRx2Views.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Rx2Views.kt\ncom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1\n*L\n1#1,245:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_debouncedOnClicked:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;->$this_debouncedOnClicked:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;->$this_debouncedOnClicked:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasOnClickListeners()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;->$this_debouncedOnClicked:Landroid/view/View;

    new-instance v1, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1$2;

    invoke-direct {v1, p1}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1$2;-><init>(Lio/reactivex/ObservableEmitter;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1$3;-><init>(Lcom/squareup/util/rx2/Rx2Views$debouncedOnClicked$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnMainThread(Lio/reactivex/ObservableEmitter;Lkotlin/jvm/functions/Function0;)V

    return-void

    .line 145
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "A view can have only one click listener. Use .share() operator to multicast."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
