.class public final Lcom/squareup/util/rx2/Rx2Views$disposeOnMainThread$1;
.super Lio/reactivex/android/MainThreadDisposable;
.source "Rx2Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Views;->disposeOnMainThread(Lio/reactivex/ObservableEmitter;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0014\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/util/rx2/Rx2Views$disposeOnMainThread$1",
        "Lio/reactivex/android/MainThreadDisposable;",
        "onDispose",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dispose:Lkotlin/jvm/functions/Function0;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$disposeOnMainThread$1;->$dispose:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    return-void
.end method


# virtual methods
.method protected onDispose()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2Views$disposeOnMainThread$1;->$dispose:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
