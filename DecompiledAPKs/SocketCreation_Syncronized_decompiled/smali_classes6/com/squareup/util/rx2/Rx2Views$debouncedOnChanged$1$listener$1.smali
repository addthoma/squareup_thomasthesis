.class public final Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "Rx2Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/ObservableEmitter;


# direct methods
.method constructor <init>(Lio/reactivex/ObservableEmitter;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
