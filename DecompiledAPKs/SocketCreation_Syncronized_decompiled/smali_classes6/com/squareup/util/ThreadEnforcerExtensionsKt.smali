.class public final Lcom/squareup/util/ThreadEnforcerExtensionsKt;
.super Ljava/lang/Object;
.source "ThreadEnforcerExtensions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "logIfNotThread",
        "",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final logIfNotThread(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1

    const-string v0, "$this$logIfNotThread"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    :try_start_0
    invoke-interface {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V
    :try_end_0
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 12
    check-cast p0, Ljava/lang/Throwable;

    invoke-static {p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
