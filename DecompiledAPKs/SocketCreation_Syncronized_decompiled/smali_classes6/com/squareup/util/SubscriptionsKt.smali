.class public final Lcom/squareup/util/SubscriptionsKt;
.super Ljava/lang/Object;
.source "Subscriptions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSubscriptions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Subscriptions.kt\ncom/squareup/util/SubscriptionsKt\n*L\n1#1,46:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0012\u0010\u0008\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "addTo",
        "",
        "Lrx/Subscription;",
        "container",
        "Lrx/subscriptions/CompositeSubscription;",
        "unsubscribeOnDetach",
        "view",
        "Landroid/view/View;",
        "unsubscribeOnExit",
        "scope",
        "Lmortar/MortarScope;",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final addTo(Lrx/Subscription;Lrx/subscriptions/CompositeSubscription;)V
    .locals 1

    const-string v0, "$this$addTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1, p0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public static final unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$unsubscribeOnDetach"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p1}, Lcom/squareup/util/Views;->isAttachedToWindowCompat(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Lcom/squareup/util/SubscriptionsKt$unsubscribeOnDetach$2;

    invoke-direct {v0, p0}, Lcom/squareup/util/SubscriptionsKt$unsubscribeOnDetach$2;-><init>(Lrx/Subscription;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void

    .line 39
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Expected view to already be attached to a window."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "$this$unsubscribeOnExit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    invoke-interface {p0}, Lrx/Subscription;->unsubscribe()V

    return-void

    .line 20
    :cond_0
    new-instance v0, Lcom/squareup/util/SubscriptionsKt$unsubscribeOnExit$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/SubscriptionsKt$unsubscribeOnExit$1;-><init>(Lrx/Subscription;)V

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
