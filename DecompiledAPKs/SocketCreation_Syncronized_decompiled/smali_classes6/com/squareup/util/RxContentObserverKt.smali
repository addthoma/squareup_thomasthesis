.class public final Lcom/squareup/util/RxContentObserverKt;
.super Ljava/lang/Object;
.source "RxContentObserver.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a,\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "listenForChanges",
        "Lrx/Observable;",
        "",
        "Landroid/content/ContentResolver;",
        "contentUri",
        "Landroid/net/Uri;",
        "notifyForDescendants",
        "",
        "backpressureMode",
        "Lrx/Emitter$BackpressureMode;",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final listenForChanges(Landroid/content/ContentResolver;Landroid/net/Uri;ZLrx/Emitter$BackpressureMode;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Landroid/net/Uri;",
            "Z",
            "Lrx/Emitter$BackpressureMode;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$listenForChanges"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentUri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backpressureMode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/util/RxContentObserverKt$listenForChanges$1;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Z)V

    check-cast v0, Lrx/functions/Action1;

    invoke-static {v0, p3}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    .line 43
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    const-string p1, "Observable.create<Unit>(\u2026e)\n      .startWith(Unit)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic listenForChanges$default(Landroid/content/ContentResolver;Landroid/net/Uri;ZLrx/Emitter$BackpressureMode;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 18
    sget-object p3, Lrx/Emitter$BackpressureMode;->ERROR:Lrx/Emitter$BackpressureMode;

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/util/RxContentObserverKt;->listenForChanges(Landroid/content/ContentResolver;Landroid/net/Uri;ZLrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method
