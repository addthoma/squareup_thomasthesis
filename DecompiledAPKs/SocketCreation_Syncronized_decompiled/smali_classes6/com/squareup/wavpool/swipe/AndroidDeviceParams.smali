.class public Lcom/squareup/wavpool/swipe/AndroidDeviceParams;
.super Ljava/lang/Object;
.source "AndroidDeviceParams.java"


# instance fields
.field public final inputSampleRate:I

.field public final outputSampleRate:I

.field public final useVoiceRecognition:Z

.field public final volume:F


# direct methods
.method public constructor <init>(IIZF)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->inputSampleRate:I

    .line 13
    iput p2, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->outputSampleRate:I

    .line 14
    iput-boolean p3, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->useVoiceRecognition:Z

    .line 15
    iput p4, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->volume:F

    return-void
.end method
