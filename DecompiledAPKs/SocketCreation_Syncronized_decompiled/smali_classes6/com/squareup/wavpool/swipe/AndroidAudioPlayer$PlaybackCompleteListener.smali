.class Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;
.super Ljava/lang/Object;
.source "AndroidAudioPlayer.java"

# interfaces
.implements Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PlaybackCompleteListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMarkerReached(Landroid/media/AudioTrack;)V
    .locals 0

    .line 231
    invoke-virtual {p1}, Landroid/media/AudioTrack;->release()V

    .line 232
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->notifyTransmissionComplete()V

    .line 233
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;->this$0:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;

    invoke-static {p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;->access$200(Lcom/squareup/wavpool/swipe/AndroidAudioPlayer;)V

    return-void
.end method

.method public onPeriodicNotification(Landroid/media/AudioTrack;)V
    .locals 0

    return-void
.end method
