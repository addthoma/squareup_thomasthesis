.class public final Lcom/squareup/wavpool/swipe/Headset_Factory;
.super Ljava/lang/Object;
.source "Headset_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/Headset;",
        ">;"
    }
.end annotation


# instance fields
.field private final headsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/Headset_Factory;->headsetConnectionListenerProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/Headset_Factory;->headsetListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/Headset_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/wavpool/swipe/Headset_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/wavpool/swipe/Headset_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/Headset;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/wavpool/swipe/Headset;

    invoke-direct {v0, p0, p1}, Lcom/squareup/wavpool/swipe/Headset;-><init>(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/Headset;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/Headset_Factory;->headsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/Headset_Factory;->headsetListenerProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/wavpool/swipe/Headset_Factory;->newInstance(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/Headset_Factory;->get()Lcom/squareup/wavpool/swipe/Headset;

    move-result-object v0

    return-object v0
.end method
