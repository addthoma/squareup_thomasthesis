.class public abstract Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;
.super Ljava/lang/Object;
.source "AndroidDeviceParamsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field public static final LG_OUTPUT_SAMPLE_RATE:I = 0xbb80

.field public static final REDUCED_VOLUME:F = 0.85f

.field public static final STANDARD_OUTPUT_SAMPLE_RATE:I = 0xac44

.field public static final STANDARD_VOLUME:F = 1.0f


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultDeviceParams()Lcom/squareup/wavpool/swipe/AndroidDeviceParams;
    .locals 27

    const-string v0, "streak"

    const-string v1, "Desire HD"

    const-string v2, "HTC Desire S"

    const-string v3, "ADR6300"

    const-string v4, "htc_glacier"

    const-string v5, "PG86100"

    const-string v6, "PC36100"

    const-string v7, "PG06100"

    const-string v8, "T-Mobile G2"

    const-string v9, "Nexus One"

    const-string v10, "ADR6400L"

    const-string v11, "LS670"

    const-string v12, "thunderc"

    const-string v13, "MB860"

    const-string v14, "Droid"

    const-string v15, "DROID2"

    const-string v16, "DROID PRO"

    const-string v17, "DROIDX"

    const-string v18, "DROID X2"

    const-string v19, "MB855"

    const-string v20, "SCH-I510"

    const-string v21, "SPH-D700"

    const-string v22, "SGH-T959V"

    const-string v23, "SGH-T989"

    const-string v24, "SPH-P100"

    const-string v25, "Nexus S"

    const-string v26, "SPH-M920"

    .line 45
    filled-new-array/range {v0 .. v26}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;->oneOf([Ljava/lang/String;)Z

    move-result v0

    const v1, 0xac44

    if-eqz v0, :cond_0

    :goto_0
    const v0, 0xac44

    goto :goto_1

    :cond_0
    const-string v0, "XT910"

    const-string v2, "XT912"

    const-string v3, "DROID RAZR"

    const-string v4, "SGH-T959"

    .line 76
    filled-new-array {v0, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;->oneOf([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x5622

    goto :goto_1

    .line 83
    :cond_1
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;->isEmulator()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    .line 90
    :goto_1
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "LGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    const v1, 0xbb80

    .line 94
    :goto_2
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "motorola"

    .line 96
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v2, 0x1

    .line 100
    :goto_4
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v4, "samsung"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v4, "oneplus"

    .line 101
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_5

    :cond_6
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_6

    :cond_7
    :goto_5
    const v3, 0x3f59999a    # 0.85f

    .line 105
    :goto_6
    new-instance v4, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;-><init>(IIZF)V

    return-object v4
.end method

.method public static isEmulator()Z
    .locals 2

    .line 117
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    const-string v1, "goldfish"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static varargs oneOf([Ljava/lang/String;)Z
    .locals 5

    .line 121
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-object v3, p0, v2

    .line 122
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const/4 p0, 0x1

    return p0

    :cond_2
    return v1
.end method

.method public static provideAndroidDeviceParams()Lcom/squareup/wavpool/swipe/AndroidDeviceParams;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 21
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;->getDefaultDeviceParams()Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    move-result-object v0

    return-object v0
.end method

.method static provideAudioVolume(Lcom/squareup/wavpool/swipe/AndroidDeviceParams;)F
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    iget p0, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->volume:F

    return p0
.end method

.method static provideInputSampleRate(Lcom/squareup/wavpool/swipe/AndroidDeviceParams;)I
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 31
    iget p0, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->inputSampleRate:I

    invoke-static {p0}, Lcom/squareup/wavpool/swipe/InputSampleRateFinder;->find(I)I

    move-result p0

    return p0
.end method

.method static provideOutputSampleRate(Lcom/squareup/wavpool/swipe/AndroidDeviceParams;)I
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    iget p0, p0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->outputSampleRate:I

    return p0
.end method
