.class public interface abstract Lcom/squareup/wavpool/swipe/CardReaderBridge;
.super Ljava/lang/Object;
.source "CardReaderBridge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/CardReaderBridge$CardReaderNativeBridge;
    }
.end annotation


# virtual methods
.method public abstract cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
.end method

.method public abstract cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
.end method

.method public abstract cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
.end method

.method public abstract cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
.end method

.method public abstract cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
.end method

.method public abstract cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
.end method

.method public abstract process_rpc_callback()V
.end method
