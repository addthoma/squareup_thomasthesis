.class public final Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;
.super Ljava/lang/Object;
.source "DecoderModule_ProvideO1SignalDecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/o1/O1SignalDecoder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory$InstanceHolder;->access$000()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideO1SignalDecoder()Lcom/squareup/squarewave/o1/O1SignalDecoder;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule;->provideO1SignalDecoder()Lcom/squareup/squarewave/o1/O1SignalDecoder;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/o1/O1SignalDecoder;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/o1/O1SignalDecoder;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;->provideO1SignalDecoder()Lcom/squareup/squarewave/o1/O1SignalDecoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;->get()Lcom/squareup/squarewave/o1/O1SignalDecoder;

    move-result-object v0

    return-object v0
.end method
