.class public abstract Lcom/squareup/wavpool/swipe/AudioModule;
.super Ljava/lang/Object;
.source "AudioModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/AudioModule$Real;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideEventDataForwarder(Lcom/squareup/logging/SwipeEventLogger;)Lcom/squareup/squarewave/EventDataForwarder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/squarewave/EventDataForwarder;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/EventDataForwarder;-><init>(Lcom/squareup/logging/SwipeEventLogger;)V

    return-object v0
.end method

.method static provideSampleFeeder(Lcom/squareup/squarewave/gum/SampleProcessor;)Lcom/squareup/squarewave/SampleFeeder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/squarewave/SampleFeeder;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/SampleFeeder;-><init>(Lcom/squareup/squarewave/gum/SampleProcessor;)V

    return-object v0
.end method

.method static provideSquarewaveDecoder(Lcom/squareup/crashnado/Crashnado;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/squarewave/SampleFeeder;Lcom/squareup/squarewave/SignalDecoder;Ljava/util/concurrent/ExecutorService;Lcom/squareup/squarewave/gum/SampleProcessor;Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/logging/SwipeEventLogger;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/SquarewaveDecoder;
    .locals 15
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 50
    new-instance v14, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p11

    move-object/from16 v12, p10

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;-><init>(Lcom/squareup/crashnado/Crashnado;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/squarewave/SampleFeeder;Lcom/squareup/squarewave/SignalDecoder;Ljava/util/concurrent/ExecutorService;Lcom/squareup/squarewave/gum/SampleProcessor;Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/logging/SwipeEventLogger;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V

    return-object v14
.end method


# virtual methods
.method abstract provideOnCarrierDetectListener(Lcom/squareup/wavpool/swipe/SquarewaveDecoder;)Lcom/squareup/squarewave/EventDataForwarder$OnCarrierDetectedListener;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
