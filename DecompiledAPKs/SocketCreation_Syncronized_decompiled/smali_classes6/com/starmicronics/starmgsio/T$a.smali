.class Lcom/starmicronics/starmgsio/T$a;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/T;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starmgsio/T;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/T;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v2

    const/16 v3, 0x41

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x64

    invoke-virtual/range {v2 .. v9}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/T;->e(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbInterface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/T;->f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    :cond_0
    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDevice;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1, v2}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbInterface;)Landroid/hardware/usb/UsbInterface;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T$a;->a:Lcom/starmicronics/starmgsio/T;

    invoke-static {v1, v2}, Lcom/starmicronics/starmgsio/T;->a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDeviceConnection;)Landroid/hardware/usb/UsbDeviceConnection;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
