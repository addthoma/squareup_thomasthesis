.class Lcom/starmicronics/starmgsio/D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:[B


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starmgsio/D;->a:[B

    return-void
.end method


# virtual methods
.method a()Lcom/starmicronics/starmgsio/D;
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/starmicronics/starmgsio/D;->a([B)Lcom/starmicronics/starmgsio/D;

    return-object p0

    :array_0
    .array-data 1
        0x4ft
        0x38t
        0xdt
        0xat
    .end array-data
.end method

.method a(Lcom/starmicronics/starmgsio/ScaleSetting;)Lcom/starmicronics/starmgsio/D;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/C;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x4

    new-array p1, p1, [B

    fill-array-data p1, :array_0

    invoke-virtual {p0, p1}, Lcom/starmicronics/starmgsio/D;->a([B)Lcom/starmicronics/starmgsio/D;

    :goto_0
    return-object p0

    :array_0
    .array-data 1
        0x5at
        0x20t
        0xdt
        0xat
    .end array-data
.end method

.method a([B)Lcom/starmicronics/starmgsio/D;
    .locals 4

    iget-object v0, p0, Lcom/starmicronics/starmgsio/D;->a:[B

    array-length v1, v0

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v1, v1, [B

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/starmicronics/starmgsio/D;->a:[B

    array-length v0, v0

    array-length v2, p1

    invoke-static {p1, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/starmicronics/starmgsio/D;->a:[B

    return-object p0
.end method

.method b()[B
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/D;->a:[B

    return-object v0
.end method
