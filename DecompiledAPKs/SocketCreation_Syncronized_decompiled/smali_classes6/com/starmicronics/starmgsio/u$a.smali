.class Lcom/starmicronics/starmgsio/u$a;
.super Lcom/starmicronics/starmgsio/n;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic b:Lcom/starmicronics/starmgsio/u;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/u;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/u$a;->b:Lcom/starmicronics/starmgsio/u;

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/n;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    :goto_0
    iget-boolean v0, p0, Lcom/starmicronics/starmgsio/n;->a:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u$a;->b:Lcom/starmicronics/starmgsio/u;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    new-instance v1, Lcom/starmicronics/starmgsio/D;

    invoke-direct {v1}, Lcom/starmicronics/starmgsio/D;-><init>()V

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/D;->a()Lcom/starmicronics/starmgsio/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/D;->b()[B

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/u$a;->b:Lcom/starmicronics/starmgsio/u;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v2

    array-length v3, v1

    const/16 v4, 0x2710

    const/4 v5, 0x0

    invoke-interface {v2, v1, v5, v3, v4}, Lcom/starmicronics/starmgsio/p;->b([BIII)I

    move-result v1

    if-gez v1, :cond_0

    monitor-exit v0

    goto/16 :goto_2

    :cond_0
    const/16 v1, 0x80

    new-array v1, v1, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-wide v3, v2

    const/4 v2, 0x0

    :cond_1
    iget-boolean v6, p0, Lcom/starmicronics/starmgsio/n;->a:Z

    if-nez v6, :cond_3

    array-length v6, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v6, :cond_3

    const-wide/16 v6, 0x19

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v6, p0, Lcom/starmicronics/starmgsio/u$a;->b:Lcom/starmicronics/starmgsio/u;

    invoke-static {v6}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v6

    array-length v7, v1

    const/16 v8, 0x32

    invoke-interface {v6, v1, v2, v7, v8}, Lcom/starmicronics/starmgsio/p;->a([BIII)I

    move-result v6

    if-lez v6, :cond_2

    add-int/2addr v2, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v6, Lcom/starmicronics/starmgsio/H$a;

    invoke-direct {v6}, Lcom/starmicronics/starmgsio/H$a;-><init>()V

    new-array v7, v2, [B

    array-length v8, v7

    invoke-static {v1, v5, v7, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v6, v7}, Lcom/starmicronics/starmgsio/H$a;->a([B)V

    invoke-virtual {v6}, Lcom/starmicronics/starmgsio/H$a;->a()Lcom/starmicronics/starmgsio/H;

    move-result-object v7

    iget-object v6, v6, Lcom/starmicronics/starmgsio/H$a;->b:[B

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    new-instance v7, Lcom/starmicronics/starmgsio/o$a;

    invoke-direct {v7}, Lcom/starmicronics/starmgsio/o$a;-><init>()V

    invoke-virtual {v7, v6}, Lcom/starmicronics/starmgsio/o$a;->a([B)V

    invoke-virtual {v7}, Lcom/starmicronics/starmgsio/o$a;->a()Lcom/starmicronics/starmgsio/o;

    move-result-object v6

    if-eqz v6, :cond_1

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/starmicronics/starmgsio/t;

    invoke-direct {v2, p0, v6}, Lcom/starmicronics/starmgsio/t;-><init>(Lcom/starmicronics/starmgsio/u$a;Lcom/starmicronics/starmgsio/o;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_2
    const-wide/16 v6, 0x1f4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v3

    cmp-long v10, v6, v8

    if-gez v10, :cond_1

    :catch_0
    :cond_3
    :goto_1
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-wide/16 v0, 0xfa

    :try_start_3
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    :catch_1
    nop

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :cond_4
    :goto_2
    return-void
.end method
