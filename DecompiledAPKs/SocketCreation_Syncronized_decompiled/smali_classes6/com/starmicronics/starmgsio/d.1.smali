.class Lcom/starmicronics/starmgsio/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/starmicronics/starmgsio/y;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/Handler;

.field private c:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

.field private d:Landroid/bluetooth/le/BluetoothLeScanner;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/starmgsio/ConnectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/bluetooth/le/ScanCallback;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/starmicronics/starmgsio/b;

    invoke-direct {v0, p0}, Lcom/starmicronics/starmgsio/b;-><init>(Lcom/starmicronics/starmgsio/d;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/d;->f:Landroid/bluetooth/le/ScanCallback;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/d;->b:Landroid/os/Handler;

    iput-object p1, p0, Lcom/starmicronics/starmgsio/d;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/d;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/d;->e:Ljava/util/ArrayList;

    return-object p0
.end method

.method private a(Lcom/starmicronics/starmgsio/ConnectionInfo;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/d;->c:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/d;->b:Landroid/os/Handler;

    new-instance v1, Lcom/starmicronics/starmgsio/c;

    invoke-direct {v1, p0, p1}, Lcom/starmicronics/starmgsio/c;-><init>(Lcom/starmicronics/starmgsio/d;Lcom/starmicronics/starmgsio/ConnectionInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/d;Lcom/starmicronics/starmgsio/ConnectionInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starmgsio/d;->a(Lcom/starmicronics/starmgsio/ConnectionInfo;)V

    return-void
.end method

.method private a(Landroid/bluetooth/le/ScanResult;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v0
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/d;Landroid/bluetooth/le/ScanResult;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starmgsio/d;->a(Landroid/bluetooth/le/ScanResult;)Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/d;)Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/d;->c:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/starmgsio/d;->d:Landroid/bluetooth/le/BluetoothLeScanner;

    if-nez v1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/starmicronics/starmgsio/d;->c:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/d;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/starmicronics/starmgsio/a;->a(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/d;->d:Landroid/bluetooth/le/BluetoothLeScanner;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/d;->f:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v2}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    :cond_1
    iput-object v1, p0, Lcom/starmicronics/starmgsio/d;->d:Landroid/bluetooth/le/BluetoothLeScanner;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/starmgsio/d;->d:Landroid/bluetooth/le/BluetoothLeScanner;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/starmicronics/starmgsio/d;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/a;->a(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/starmicronics/starmgsio/d;->e:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/starmicronics/starmgsio/d;->e:Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/starmicronics/starmgsio/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :goto_0
    iput-object p1, p0, Lcom/starmicronics/starmgsio/d;->c:Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    new-instance p1, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {p1}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object p1

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/d;->d:Landroid/bluetooth/le/BluetoothLeScanner;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/d;->d:Landroid/bluetooth/le/BluetoothLeScanner;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/starmicronics/starmgsio/d;->f:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1, p1, v2}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    monitor-exit p0

    return-void

    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
