.class Lcom/starmicronics/starmgsio/g;
.super Landroid/bluetooth/BluetoothGattCallback;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starmgsio/m;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/m;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 7

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    monitor-enter p1

    const-string v0, "2456E1B9-26E2-8F83-E744-F34F01E9D703"

    :try_start_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit p1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    monitor-exit p1

    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object p2

    if-nez p2, :cond_2

    monitor-exit p1

    return-void

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v0

    array-length v0, v0

    array-length v1, p2

    add-int v2, v0, v1

    new-array v2, v2, [B

    iget-object v3, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, v2, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p2, v4, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p2, Lcom/starmicronics/starmgsio/o$a;

    invoke-direct {p2}, Lcom/starmicronics/starmgsio/o$a;-><init>()V

    invoke-virtual {p2, v2}, Lcom/starmicronics/starmgsio/o$a;->a([B)V

    invoke-virtual {p2}, Lcom/starmicronics/starmgsio/o$a;->a()Lcom/starmicronics/starmgsio/o;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleData;)V

    :cond_3
    invoke-virtual {p2}, Lcom/starmicronics/starmgsio/o$a;->b()[B

    move-result-object p2

    new-instance v1, Lcom/starmicronics/starmgsio/J;

    invoke-direct {v1}, Lcom/starmicronics/starmgsio/J;-><init>()V

    invoke-virtual {v1, p2}, Lcom/starmicronics/starmgsio/J;->a([B)V

    invoke-static {}, Lcom/starmicronics/starmgsio/ScaleSetting;->values()[Lcom/starmicronics/starmgsio/ScaleSetting;

    move-result-object p2

    array-length v2, p2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_6

    aget-object v5, p2, v3

    invoke-virtual {v1, v5}, Lcom/starmicronics/starmgsio/J;->a(Lcom/starmicronics/starmgsio/ScaleSetting;)Ljava/lang/Boolean;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    const/16 v2, 0x67

    :goto_1
    invoke-static {p2, v5, v2}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleSetting;I)V

    const/4 p2, 0x1

    goto :goto_2

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_6
    const/4 p2, 0x0

    :goto_2
    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/J;->a()[B

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v2, v1}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;[B)[B

    iget-object v1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_7

    iget-object v1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/m;->l(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    new-array v2, v4, [B

    invoke-static {v1, v2}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;[B)[B

    :cond_7
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [B

    invoke-static {v0, v1}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;[B)[B

    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/m;->l(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/m;->k(Lcom/starmicronics/starmgsio/m;)[B

    move-result-object v2

    array-length v2, v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    if-eqz p2, :cond_9

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2, v4}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;I)I

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_9
    monitor-exit p1

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 1

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    monitor-enter p1

    const-string v0, "2456E1B9-26E2-8F83-E744-F34F01E9D703"

    :try_start_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    monitor-exit p1

    return-void

    :cond_0
    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result p2

    const/4 v0, 0x5

    if-eq p2, v0, :cond_1

    monitor-exit p1

    return-void

    :cond_1
    if-eqz p3, :cond_2

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    const/4 p3, 0x2

    invoke-static {p2, p3}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;I)I

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_2
    monitor-exit p1

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 0

    if-nez p2, :cond_0

    const/4 p1, 0x2

    if-ne p3, p1, :cond_0

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2, p1}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->n(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGatt;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    goto :goto_0

    :cond_0
    if-nez p3, :cond_1

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->f(Lcom/starmicronics/starmgsio/m;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    if-ne p3, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 p1, 0x3

    if-ne p3, p1, :cond_3

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    :goto_0
    return-void
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 0

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "00002902-0000-1000-8000-00805F9B34FB"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result p1

    const/4 p2, 0x3

    if-eq p1, p2, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    if-eqz p3, :cond_2

    :goto_0
    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :cond_2
    const/4 p2, 0x4

    invoke-static {p1, p2}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    const/4 p2, 0x5

    invoke-static {p1, p2}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    goto :goto_0
.end method

.method public onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 0

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result p1

    const/4 p2, 0x4

    if-eq p1, p2, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    const/4 p2, 0x5

    invoke-static {p1, p2}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/bluetooth/BluetoothGattService;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "2456E1B9-26E2-8F83-E744-F34F01E9D701"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    :cond_4
    const-string v0, "2456E1B9-26E2-8F83-E744-F34F01E9D703"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    if-nez p2, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_6

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0, p2}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/m;->e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result p2

    and-int/lit8 p2, p2, 0x10

    if-nez p2, :cond_7

    goto :goto_0

    :cond_7
    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/m;->n(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGatt;

    move-result-object p2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result p2

    if-nez p2, :cond_8

    goto :goto_0

    :cond_8
    iget-object p2, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p2}, Lcom/starmicronics/starmgsio/m;->e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    const-string v0, "00002902-0000-1000-8000-00805F9B34FB"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object p2

    if-nez p2, :cond_9

    goto :goto_0

    :cond_9
    sget-object v0, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {p2, v0}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    move-result v0

    if-nez v0, :cond_a

    goto :goto_0

    :cond_a
    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    iget-object v0, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/m;->n(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result p2

    if-nez p2, :cond_b

    goto/16 :goto_0

    :cond_b
    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    const/4 p2, 0x3

    invoke-static {p1, p2}, Lcom/starmicronics/starmgsio/m;->c(Lcom/starmicronics/starmgsio/m;I)I

    return-void

    :cond_c
    iget-object p1, p0, Lcom/starmicronics/starmgsio/g;->a:Lcom/starmicronics/starmgsio/m;

    invoke-static {p1}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    return-void
.end method
