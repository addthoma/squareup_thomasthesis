.class Lcom/starmicronics/starmgsio/z$a;
.super Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/z;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;)Lcom/starmicronics/starmgsio/z$a;
    .locals 0

    invoke-super {p0, p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->setBleInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    iget-object p1, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/z;->a(Ljava/lang/String;)V

    return-object p0
.end method

.method b(Ljava/lang/String;Ljava/lang/String;)Lcom/starmicronics/starmgsio/z$a;
    .locals 0

    invoke-super {p0, p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->setUsbInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    iget-object p1, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/z;->a(Ljava/lang/String;)V

    return-object p0
.end method

.method public bridge synthetic build()Lcom/starmicronics/starmgsio/ConnectionInfo;
    .locals 1

    invoke-virtual {p0}, Lcom/starmicronics/starmgsio/z$a;->build()Lcom/starmicronics/starmgsio/z;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/starmicronics/starmgsio/z;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    return-object v0
.end method
