.class public final enum Lcom/starmicronics/starmgsio/ScaleData$DataType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/ScaleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starmgsio/ScaleData$DataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum GROSS:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum NET:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum NET_NOT_TARED:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum PRESET_TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum TOTAL:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field public static final enum UNIT:Lcom/starmicronics/starmgsio/ScaleData$DataType;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v2, 0x1

    const-string v3, "NET_NOT_TARED"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->NET_NOT_TARED:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v3, 0x2

    const-string v4, "NET"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->NET:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v4, 0x3

    const-string v5, "TARE"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v5, 0x4

    const-string v6, "PRESET_TARE"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->PRESET_TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v6, 0x5

    const-string v7, "TOTAL"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->TOTAL:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v7, 0x6

    const-string v8, "UNIT"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->UNIT:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v8, 0x7

    const-string v9, "GROSS"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starmgsio/ScaleData$DataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->GROSS:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/starmicronics/starmgsio/ScaleData$DataType;

    sget-object v9, Lcom/starmicronics/starmgsio/ScaleData$DataType;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->NET_NOT_TARED:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->NET:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->PRESET_TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->TOTAL:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->UNIT:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->GROSS:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleData$DataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ScaleData$DataType;
    .locals 1

    const-class v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starmgsio/ScaleData$DataType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starmgsio/ScaleData$DataType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleData$DataType;

    invoke-virtual {v0}, [Lcom/starmicronics/starmgsio/ScaleData$DataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starmgsio/ScaleData$DataType;

    return-object v0
.end method
