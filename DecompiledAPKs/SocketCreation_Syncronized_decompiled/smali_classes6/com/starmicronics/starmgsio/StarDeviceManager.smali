.class public Lcom/starmicronics/starmgsio/StarDeviceManager;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;
    }
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/starmgsio/y;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;->All:Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;

    invoke-direct {p0, p1, v0}, Lcom/starmicronics/starmgsio/StarDeviceManager;-><init>(Landroid/content/Context;Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/starmicronics/starmgsio/StarDeviceManager;->b:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/starmicronics/starmgsio/M;->a(Landroid/content/Context;Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;)Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/starmgsio/StarDeviceManager;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public static getVersionName()Ljava/lang/String;
    .locals 1

    const-string v0, "1.1.0_sample20190703"

    return-object v0
.end method


# virtual methods
.method public createScale(Lcom/starmicronics/starmgsio/ConnectionInfo;)Lcom/starmicronics/starmgsio/Scale;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/StarDeviceManager;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/starmicronics/starmgsio/G;->a(Landroid/content/Context;Lcom/starmicronics/starmgsio/ConnectionInfo;)Lcom/starmicronics/starmgsio/Scale;

    move-result-object p1

    return-object p1
.end method

.method public scanForScales(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/StarDeviceManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/starmicronics/starmgsio/y;

    invoke-interface {v1, p1}, Lcom/starmicronics/starmgsio/y;->a(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public stopScan()V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/StarDeviceManager;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/starmicronics/starmgsio/y;

    invoke-interface {v1}, Lcom/starmicronics/starmgsio/y;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method
