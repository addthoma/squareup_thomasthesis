.class public abstract Lcom/starmicronics/starioextension/StarIoExtManagerListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/starioextension/ai;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessoryConnectFailure()V
    .locals 0

    return-void
.end method

.method public onAccessoryConnectSuccess()V
    .locals 0

    return-void
.end method

.method public onAccessoryDisconnect()V
    .locals 0

    return-void
.end method

.method public onBarcodeDataReceive([B)V
    .locals 0

    return-void
.end method

.method public onBarcodeReaderConnect()V
    .locals 0

    return-void
.end method

.method public onBarcodeReaderDisconnect()V
    .locals 0

    return-void
.end method

.method public onBarcodeReaderImpossible()V
    .locals 0

    return-void
.end method

.method public onCashDrawerClose()V
    .locals 0

    return-void
.end method

.method public onCashDrawerOpen()V
    .locals 0

    return-void
.end method

.method public onPrinterCoverClose()V
    .locals 0

    return-void
.end method

.method public onPrinterCoverOpen()V
    .locals 0

    return-void
.end method

.method public onPrinterImpossible()V
    .locals 0

    return-void
.end method

.method public onPrinterOffline()V
    .locals 0

    return-void
.end method

.method public onPrinterOnline()V
    .locals 0

    return-void
.end method

.method public onPrinterPaperEmpty()V
    .locals 0

    return-void
.end method

.method public onPrinterPaperNearEmpty()V
    .locals 0

    return-void
.end method

.method public onPrinterPaperReady()V
    .locals 0

    return-void
.end method

.method public onStatusUpdate(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
