.class Lcom/starmicronics/starioextension/p;
.super Lcom/starmicronics/starioextension/ah;


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ah;-><init>(I)V

    return-void
.end method

.method private static a([B)Ljava/lang/String;
    .locals 6

    new-instance v0, Lcom/starmicronics/starioextension/p$3;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/p$3;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NwNnWnWnNn"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v3, p0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    aget-byte v5, p0, v4

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lcom/starmicronics/starioextension/p;->a([B)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v1, Lcom/starmicronics/starioextension/p$1;

    invoke-direct {v1, p0}, Lcom/starmicronics/starioextension/p$1;-><init>(Lcom/starmicronics/starioextension/p;)V

    new-instance v2, Lcom/starmicronics/starioextension/p$2;

    invoke-direct {v2, p0}, Lcom/starmicronics/starioextension/p$2;-><init>(Lcom/starmicronics/starioextension/p;)V

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, v0, v1, p2}, Lcom/starmicronics/starioextension/p;->a(Ljava/lang/String;II)V

    array-length p2, p1

    new-array p2, p2, [I

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    aget-byte v1, p1, v0

    aput v1, p2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/p;->a([I)V

    return-void
.end method
