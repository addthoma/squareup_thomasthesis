.class Lcom/starmicronics/starioextension/an$1;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/an;->a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/an;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/an;)V
    .locals 2

    iput-object p1, p0, Lcom/starmicronics/starioextension/an$1;->a:Lcom/starmicronics/starioextension/an;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode1:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode2:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode3:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode4:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode5:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode6:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode7:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode8:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode9:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/an$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
