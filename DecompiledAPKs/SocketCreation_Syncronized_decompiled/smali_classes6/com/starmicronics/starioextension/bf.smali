.class Lcom/starmicronics/starioextension/bf;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;",
            "I)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/bf$1;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/bf$1;-><init>()V

    new-instance v3, Lcom/starmicronics/starioextension/bf$2;

    invoke-direct {v3}, Lcom/starmicronics/starioextension/bf$2;-><init>()V

    const/4 v4, 0x2

    move/from16 v5, p4

    if-ge v5, v4, :cond_1

    const/4 v5, 0x2

    :cond_1
    const/16 v6, 0x8

    if-le v5, v6, :cond_2

    const/16 v5, 0x8

    :cond_2
    const/16 v7, 0x12

    new-array v7, v7, [B

    const/4 v8, 0x0

    const/16 v9, 0x1b

    aput-byte v9, v7, v8

    const/4 v10, 0x1

    const/16 v11, 0x1d

    aput-byte v11, v7, v10

    const/16 v12, 0x79

    aput-byte v12, v7, v4

    const/4 v13, 0x3

    const/16 v14, 0x53

    aput-byte v14, v7, v13

    const/16 v15, 0x30

    const/4 v13, 0x4

    aput-byte v15, v7, v13

    move-object/from16 v15, p2

    invoke-interface {v2, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    const/4 v15, 0x5

    aput-byte v2, v7, v15

    const/4 v2, 0x6

    aput-byte v9, v7, v2

    const/16 v16, 0x7

    aput-byte v11, v7, v16

    aput-byte v12, v7, v6

    const/16 v17, 0x9

    aput-byte v14, v7, v17

    const/16 v17, 0xa

    const/16 v18, 0x31

    aput-byte v18, v7, v17

    const/16 v17, 0xb

    move-object/from16 v2, p3

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v7, v17

    const/16 v2, 0xc

    aput-byte v9, v7, v2

    const/16 v2, 0xd

    aput-byte v11, v7, v2

    const/16 v2, 0xe

    aput-byte v12, v7, v2

    const/16 v2, 0xf

    aput-byte v14, v7, v2

    const/16 v2, 0x10

    const/16 v3, 0x32

    aput-byte v3, v7, v2

    const/16 v2, 0x11

    int-to-byte v3, v5

    aput-byte v3, v7, v2

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v2, v1

    rem-int/lit16 v2, v2, 0x100

    array-length v3, v1

    div-int/lit16 v3, v3, 0x100

    new-array v5, v6, [B

    aput-byte v9, v5, v8

    aput-byte v11, v5, v10

    aput-byte v12, v5, v4

    const/16 v4, 0x44

    const/4 v6, 0x3

    aput-byte v4, v5, v6

    aput-byte v18, v5, v13

    aput-byte v8, v5, v15

    int-to-byte v2, v2

    const/4 v4, 0x6

    aput-byte v2, v5, v4

    int-to-byte v2, v3

    aput-byte v2, v5, v16

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v13, [B

    fill-array-data v1, :array_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x1dt
        0x79t
        0x50t
    .end array-data
.end method

.method static a(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;",
            "II)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance p2, Lcom/starmicronics/starioextension/bf$3;

    invoke-direct {p2}, Lcom/starmicronics/starioextension/bf$3;-><init>()V

    const/4 v0, 0x2

    if-ge p4, v0, :cond_1

    const/4 p4, 0x2

    :cond_1
    const/16 v0, 0x8

    if-le p4, v0, :cond_2

    const/16 p4, 0x8

    :cond_2
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;

    invoke-static {v0, p1}, Lcom/google/zxing/qrcode/encoder/Encoder;->encode(Ljava/lang/String;Lcom/google/zxing/qrcode/decoder/ErrorCorrectionLevel;)Lcom/google/zxing/qrcode/encoder/QRCode;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/QRCode;->getMatrix()Lcom/google/zxing/qrcode/encoder/ByteMatrix;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result p3

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p2

    const/4 p3, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result v1

    const/4 v2, 0x1

    if-ge v0, v1, :cond_5

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v3

    if-ge v1, v3, :cond_4

    invoke-virtual {p1, v1, v0}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->get(II)B

    move-result v3

    if-ne v3, v2, :cond_3

    const/high16 v3, -0x1000000

    goto :goto_2

    :cond_3
    const/4 v3, -0x1

    :goto_2
    invoke-virtual {p2, v1, v0, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getWidth()I

    move-result v0

    mul-int v0, v0, p4

    invoke-virtual {p1}, Lcom/google/zxing/qrcode/encoder/ByteMatrix;->getHeight()I

    move-result p1

    mul-int p1, p1, p4

    invoke-static {p2, v0, p1, p3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance p1, Lcom/starmicronics/starioextension/j;

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x1

    sget-object v8, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/starmicronics/starioextension/j;-><init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    invoke-static {p0, p1, p5, v2}, Lcom/starmicronics/starioextension/i;->e(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V
    :try_end_0
    .catch Lcom/google/zxing/WriterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static b(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;",
            "I)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/starmicronics/starioextension/bf$4;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/bf$4;-><init>()V

    new-instance v3, Lcom/starmicronics/starioextension/bf$5;

    invoke-direct {v3}, Lcom/starmicronics/starioextension/bf$5;-><init>()V

    const/4 v4, 0x2

    move/from16 v5, p4

    if-ge v5, v4, :cond_1

    const/4 v5, 0x2

    :cond_1
    const/16 v6, 0x8

    if-le v5, v6, :cond_2

    const/16 v5, 0x8

    :cond_2
    const/16 v7, 0x19

    new-array v7, v7, [B

    const/16 v8, 0x1d

    const/4 v9, 0x0

    aput-byte v8, v7, v9

    const/4 v10, 0x1

    const/16 v11, 0x28

    aput-byte v11, v7, v10

    const/16 v12, 0x6b

    aput-byte v12, v7, v4

    const/4 v13, 0x4

    const/4 v14, 0x3

    aput-byte v13, v7, v14

    aput-byte v9, v7, v13

    const/4 v15, 0x5

    const/16 v16, 0x31

    aput-byte v16, v7, v15

    const/16 v17, 0x41

    const/16 v18, 0x6

    aput-byte v17, v7, v18

    move-object/from16 v15, p2

    invoke-interface {v2, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    const/4 v15, 0x7

    aput-byte v2, v7, v15

    aput-byte v9, v7, v6

    const/16 v2, 0x9

    aput-byte v8, v7, v2

    const/16 v2, 0xa

    aput-byte v11, v7, v2

    const/16 v2, 0xb

    aput-byte v12, v7, v2

    const/16 v2, 0xc

    aput-byte v14, v7, v2

    const/16 v2, 0xd

    aput-byte v9, v7, v2

    const/16 v2, 0xe

    aput-byte v16, v7, v2

    const/16 v2, 0xf

    const/16 v17, 0x45

    aput-byte v17, v7, v2

    const/16 v2, 0x10

    move-object/from16 v15, p3

    invoke-interface {v3, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v7, v2

    const/16 v2, 0x11

    aput-byte v8, v7, v2

    const/16 v2, 0x12

    aput-byte v11, v7, v2

    const/16 v2, 0x13

    aput-byte v12, v7, v2

    const/16 v2, 0x14

    aput-byte v14, v7, v2

    const/16 v2, 0x15

    aput-byte v9, v7, v2

    const/16 v2, 0x16

    aput-byte v16, v7, v2

    const/16 v2, 0x17

    const/16 v3, 0x43

    aput-byte v3, v7, v2

    const/16 v2, 0x18

    int-to-byte v3, v5

    aput-byte v3, v7, v2

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v2, v1

    add-int/2addr v2, v14

    rem-int/lit16 v2, v2, 0x100

    array-length v3, v1

    add-int/2addr v3, v14

    div-int/lit16 v3, v3, 0x100

    new-array v5, v6, [B

    aput-byte v8, v5, v9

    aput-byte v11, v5, v10

    aput-byte v12, v5, v4

    int-to-byte v2, v2

    aput-byte v2, v5, v14

    int-to-byte v2, v3

    aput-byte v2, v5, v13

    const/4 v2, 0x5

    aput-byte v16, v5, v2

    const/16 v2, 0x50

    aput-byte v2, v5, v18

    const/16 v2, 0x30

    const/4 v3, 0x7

    aput-byte v2, v5, v3

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p0 .. p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v6, [B

    fill-array-data v1, :array_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 1
        0x1dt
        0x28t
        0x6bt
        0x3t
        0x0t
        0x31t
        0x51t
        0x30t
    .end array-data
.end method

.method static c(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;",
            "I)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance p2, Lcom/starmicronics/starioextension/bf$6;

    invoke-direct {p2}, Lcom/starmicronics/starioextension/bf$6;-><init>()V

    const/4 v0, 0x2

    if-ge p4, v0, :cond_1

    const/4 p4, 0x2

    :cond_1
    const/16 v1, 0x8

    if-le p4, v1, :cond_2

    const/16 p4, 0x8

    :cond_2
    const/4 v1, 0x3

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v2, p1

    rem-int/lit16 v2, v2, 0x100

    array-length v3, p1

    div-int/lit16 v3, v3, 0x100

    const/4 v4, 0x7

    new-array v4, v4, [B

    const/16 v5, 0x1b

    const/4 v6, 0x0

    aput-byte v5, v4, v6

    const/16 v5, 0x5a

    const/4 v7, 0x1

    aput-byte v5, v4, v7

    aput-byte v6, v4, v0

    invoke-interface {p2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    aput-byte p2, v4, v1

    const/4 p2, 0x4

    int-to-byte p3, p4

    aput-byte p3, v4, p2

    const/4 p2, 0x5

    int-to-byte p3, v2

    aput-byte p3, v4, p2

    const/4 p2, 0x6

    int-to-byte p3, v3

    aput-byte p3, v4, p2

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array p1, v7, [B

    const/16 p2, 0xa

    aput-byte p2, p1, v6

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :array_0
    .array-data 1
        0x1dt
        0x5at
        0x2t
    .end array-data
.end method

.method static d(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;",
            "I)V"
        }
    .end annotation

    return-void
.end method
