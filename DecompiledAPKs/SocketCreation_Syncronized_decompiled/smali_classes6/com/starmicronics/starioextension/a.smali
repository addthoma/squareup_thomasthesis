.class Lcom/starmicronics/starioextension/a;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0x400

    if-le p1, v1, :cond_1

    const/16 p1, 0x400

    :cond_1
    rem-int/lit16 v1, p1, 0x100

    div-int/lit16 p1, p1, 0x100

    const/4 v2, 0x5

    new-array v2, v2, [B

    const/16 v3, 0x1b

    aput-byte v3, v2, v0

    const/4 v0, 0x1

    const/16 v3, 0x1d

    aput-byte v3, v2, v0

    const/4 v0, 0x2

    const/16 v3, 0x41

    aput-byte v3, v2, v0

    const/4 v0, 0x3

    int-to-byte v1, v1

    aput-byte v1, v2, v0

    const/4 v0, 0x4

    int-to-byte p1, p1

    aput-byte p1, v2, v0

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/16 v1, 0x400

    if-le p1, v1, :cond_1

    const/16 p1, 0x400

    :cond_1
    rem-int/lit16 v1, p1, 0x100

    div-int/lit16 p1, p1, 0x100

    const/4 v2, 0x4

    new-array v2, v2, [B

    const/16 v3, 0x1b

    aput-byte v3, v2, v0

    const/4 v0, 0x1

    const/16 v3, 0x24

    aput-byte v3, v2, v0

    const/4 v0, 0x2

    int-to-byte v1, v1

    aput-byte v1, v2, v0

    const/4 v0, 0x3

    int-to-byte p1, p1

    aput-byte p1, v2, v0

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
