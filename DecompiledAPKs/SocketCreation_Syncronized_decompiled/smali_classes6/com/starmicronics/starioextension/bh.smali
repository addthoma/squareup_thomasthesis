.class Lcom/starmicronics/starioextension/bh;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;",
            "III)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/bh$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/bh$1;-><init>()V

    const/4 v1, 0x1

    if-ge p2, v1, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v2, 0x14

    if-le p2, v2, :cond_1

    const/16 p2, 0x14

    :cond_1
    div-int/2addr p3, v2

    const/4 v3, 0x0

    if-gez p3, :cond_2

    const/4 p3, 0x0

    :cond_2
    const/16 v4, 0xff

    if-le p3, v4, :cond_3

    const/16 p3, 0xff

    :cond_3
    div-int/2addr p4, v2

    if-gez p4, :cond_4

    const/4 p4, 0x0

    :cond_4
    if-le p4, v4, :cond_5

    const/16 p4, 0xff

    :cond_5
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/16 v0, 0xe

    new-array v0, v0, [B

    const/16 v2, 0x1b

    aput-byte v2, v0, v3

    const/16 v4, 0x1d

    aput-byte v4, v0, v1

    const/4 v1, 0x2

    const/16 v5, 0x19

    aput-byte v5, v0, v1

    const/4 v1, 0x3

    const/16 v6, 0x11

    aput-byte v6, v0, v1

    const/4 v1, 0x4

    aput-byte p1, v0, v1

    const/4 v1, 0x5

    int-to-byte p3, p3

    aput-byte p3, v0, v1

    const/4 p3, 0x6

    int-to-byte p4, p4

    aput-byte p4, v0, p3

    const/4 p3, 0x7

    aput-byte v2, v0, p3

    const/16 p3, 0x8

    aput-byte v4, v0, p3

    const/16 p3, 0x9

    aput-byte v5, v0, p3

    const/16 p3, 0xa

    const/16 p4, 0x12

    aput-byte p4, v0, p3

    const/16 p3, 0xb

    aput-byte p1, v0, p3

    const/16 p1, 0xc

    int-to-byte p2, p2

    aput-byte p2, v0, p1

    const/16 p1, 0xd

    aput-byte v3, v0, p1

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;",
            "III)V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;",
            "III)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/bh$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/bh$2;-><init>()V

    const/4 v1, 0x1

    if-ge p2, v1, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v2, 0x14

    if-le p2, v2, :cond_1

    const/16 p2, 0x14

    :cond_1
    div-int/2addr p3, v2

    const/4 v3, 0x0

    if-gez p3, :cond_2

    const/4 p3, 0x0

    :cond_2
    const/16 v4, 0xff

    if-le p3, v4, :cond_3

    const/16 p3, 0xff

    :cond_3
    div-int/2addr p4, v2

    if-gez p4, :cond_4

    const/4 p4, 0x0

    :cond_4
    if-le p4, v4, :cond_5

    const/16 p4, 0xff

    :cond_5
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/16 v0, 0xe

    new-array v0, v0, [B

    const/16 v2, 0x1b

    aput-byte v2, v0, v3

    const/16 v4, 0x1d

    aput-byte v4, v0, v1

    const/4 v1, 0x2

    const/16 v5, 0x19

    aput-byte v5, v0, v1

    const/4 v1, 0x3

    const/16 v6, 0x11

    aput-byte v6, v0, v1

    const/4 v1, 0x4

    aput-byte p1, v0, v1

    const/4 v1, 0x5

    int-to-byte p3, p3

    aput-byte p3, v0, v1

    const/4 p3, 0x6

    int-to-byte p4, p4

    aput-byte p4, v0, p3

    const/4 p3, 0x7

    aput-byte v2, v0, p3

    const/16 p3, 0x8

    aput-byte v4, v0, p3

    const/16 p3, 0x9

    aput-byte v5, v0, p3

    const/16 p3, 0xa

    const/16 p4, 0x12

    aput-byte p4, v0, p3

    const/16 p3, 0xb

    aput-byte p1, v0, p3

    const/16 p1, 0xc

    int-to-byte p2, p2

    aput-byte p2, v0, p1

    const/16 p1, 0xd

    aput-byte v3, v0, p1

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;",
            "III)V"
        }
    .end annotation

    return-void
.end method
