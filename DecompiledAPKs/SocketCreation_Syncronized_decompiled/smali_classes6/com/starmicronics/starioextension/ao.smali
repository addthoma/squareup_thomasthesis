.class Lcom/starmicronics/starioextension/ao;
.super Lcom/starmicronics/starioextension/ah;


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ah;-><init>(I)V

    return-void
.end method

.method private static a([B)Ljava/lang/String;
    .locals 9

    new-instance v0, Lcom/starmicronics/starioextension/ao$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/ao$2;-><init>()V

    new-instance v1, Lcom/starmicronics/starioextension/ao$3;

    invoke-direct {v1}, Lcom/starmicronics/starioextension/ao$3;-><init>()V

    new-instance v2, Lcom/starmicronics/starioextension/ao$4;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/ao$4;-><init>()V

    new-instance v3, Lcom/starmicronics/starioextension/ao$5;

    invoke-direct {v3}, Lcom/starmicronics/starioextension/ao$5;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "101"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x0

    aget-byte v6, p0, v6

    invoke-virtual {v0, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    const/4 v6, 0x1

    :goto_0
    const/4 v7, 0x0

    const/4 v8, 0x7

    if-ge v6, v8, :cond_2

    and-int/lit8 v8, v0, 0x20

    if-nez v8, :cond_0

    aget-byte v8, p0, v6

    invoke-virtual {v1, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto :goto_1

    :cond_0
    aget-byte v8, p0, v6

    invoke-virtual {v2, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    :goto_1
    check-cast v8, Ljava/lang/String;

    if-nez v8, :cond_1

    return-object v7

    :cond_1
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "01010"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const/16 v0, 0xd

    if-ge v8, v0, :cond_4

    aget-byte v0, p0, v8

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_3

    return-object v7

    :cond_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    array-length v0, p1

    const/16 v1, 0xd

    const/16 v2, 0xc

    if-eq v0, v2, :cond_1

    array-length v0, p1

    if-eq v0, v1, :cond_1

    return-void

    :cond_1
    new-array v0, v1, [B

    const/4 v3, 0x0

    invoke-static {p1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p1, v2}, Lcom/starmicronics/starioextension/n;->a([BI)B

    move-result p1

    aput-byte p1, v0, v2

    invoke-static {v0}, Lcom/starmicronics/starioextension/ao;->a([B)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    new-instance v2, Lcom/starmicronics/starioextension/ao$1;

    invoke-direct {v2, p0}, Lcom/starmicronics/starioextension/ao$1;-><init>(Lcom/starmicronics/starioextension/ao;)V

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/starmicronics/starioextension/ao;->a(Ljava/lang/String;I)V

    new-array p1, v1, [I

    :goto_0
    array-length p2, p1

    if-ge v3, p2, :cond_3

    aget-byte p2, v0, v3

    aput p2, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/ao;->a([I)V

    return-void
.end method
