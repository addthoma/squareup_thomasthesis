.class public final enum Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SoundStorageArea"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

.field public static final enum Area1:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

.field public static final enum Area2:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    const/4 v1, 0x0

    const-string v2, "Area1"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area1:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    new-instance v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    const/4 v2, 0x1

    const-string v3, "Area2"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area2:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    sget-object v3, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area1:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    aput-object v3, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area2:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->$VALUES:[Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->$VALUES:[Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    return-object v0
.end method
