.class final Lcom/starmicronics/starioextension/bc$3;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/bc;->b(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC0:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x30

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC1:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x31

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC2:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC3:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x33

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC4:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x34

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC5:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x35

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC6:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x36

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC7:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x37

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;->ECC8:Lcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;

    const/16 v1, 0x38

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/bc$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
