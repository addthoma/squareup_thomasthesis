.class Lcom/starmicronics/starioextension/StarIoExtManager$e;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/StarIoExtManager;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/16 p1, 0xc8

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->b:I

    const/16 p1, 0x3e8

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->c:I

    const/high16 p1, 0x10000

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->d:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/StarIoExtManager$e;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    return-void
.end method


# virtual methods
.method declared-synchronized a(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->n(Lcom/starmicronics/starioextension/StarIoExtManager;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 22

    move-object/from16 v1, p0

    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    const v0, 0x10008

    new-array v0, v0, [B

    :goto_1
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    iget-object v2, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    monitor-enter v2
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v3

    if-nez v3, :cond_1

    monitor-exit v2

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v5

    invoke-virtual {v5}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "usb:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x3

    const/16 v7, 0x1b

    const/4 v8, 0x4

    const/4 v9, 0x2

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v5, :cond_5

    new-array v5, v8, [B

    aput-byte v7, v5, v11

    const/16 v12, 0x1e

    aput-byte v12, v5, v10

    const/16 v12, 0x61

    aput-byte v12, v5, v9

    aput-byte v11, v5, v6

    iget-object v12, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v12}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v12

    array-length v13, v5

    invoke-virtual {v12, v5, v11, v13}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    :goto_2
    iget-object v5, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v5

    array-length v14, v0

    invoke-virtual {v5, v0, v11, v14}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_2

    const-wide/16 v12, 0xa

    :try_start_2
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    :cond_2
    const-wide/16 v14, 0x64

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v12

    cmp-long v5, v14, v16

    if-gez v5, :cond_3

    goto :goto_3

    :cond_3
    const-wide/16 v14, 0x1388

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v3

    cmp-long v5, v14, v16

    if-ltz v5, :cond_4

    goto :goto_2

    :cond_4
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v3, "Error"

    invoke-direct {v0, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_3
    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v3

    array-length v4, v0

    invoke-virtual {v3, v0, v11, v4}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    new-array v3, v8, [B

    aput-byte v7, v3, v11

    const/16 v4, 0x1d

    aput-byte v4, v3, v10

    const/16 v5, 0x42

    aput-byte v5, v3, v9

    const/16 v12, 0x31

    aput-byte v12, v3, v6

    iget-object v13, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v13}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v13

    array-length v14, v3

    invoke-virtual {v13, v3, v11, v14}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    const/4 v3, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a()Z

    move-result v17

    const-wide/16 v18, 0x3e8

    if-eqz v17, :cond_a

    iget-object v6, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v6

    array-length v9, v0

    sub-int/2addr v9, v3

    invoke-virtual {v6, v0, v3, v9}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v6

    add-int/2addr v3, v6

    const/4 v6, 0x5

    if-gt v6, v3, :cond_7

    const/4 v6, 0x0

    :goto_5
    add-int/lit8 v9, v3, -0x5

    if-gt v6, v9, :cond_7

    aget-byte v9, v0, v6

    if-ne v9, v7, :cond_6

    add-int/lit8 v9, v6, 0x1

    aget-byte v9, v0, v9

    if-ne v9, v4, :cond_6

    add-int/lit8 v9, v6, 0x2

    aget-byte v9, v0, v9

    if-ne v9, v5, :cond_6

    add-int/lit8 v9, v6, 0x3

    aget-byte v9, v0, v9

    if-ne v9, v12, :cond_6

    add-int/lit8 v6, v6, 0x4

    aget-byte v6, v0, v6

    move v15, v6

    const/16 v16, 0x1

    goto :goto_6

    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_7
    :goto_6
    if-eqz v16, :cond_8

    goto :goto_7

    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v13

    cmp-long v6, v20, v18

    if-lez v6, :cond_9

    goto :goto_7

    :cond_9
    const/4 v6, 0x3

    const/4 v9, 0x2

    goto :goto_4

    :cond_a
    :goto_7
    if-eqz v16, :cond_14

    and-int/lit8 v3, v15, 0x2

    if-eqz v3, :cond_b

    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->k(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    move-result-object v3

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Connect:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    if-eq v3, v6, :cond_c

    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Connect:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    invoke-static {v3, v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    new-instance v3, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v6, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v9, Lcom/starmicronics/starioextension/StarIoExtManager$a;->o:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v3, v6, v9}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v6, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v6

    :goto_8
    invoke-virtual {v6, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_9

    :cond_b
    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->k(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    move-result-object v3

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Disconnect:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    if-eq v3, v6, :cond_c

    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Disconnect:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    invoke-static {v3, v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    new-instance v3, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v6, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v9, Lcom/starmicronics/starioextension/StarIoExtManager$a;->p:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v3, v6, v9}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v6, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v6

    goto :goto_8

    :cond_c
    :goto_9
    and-int/lit8 v3, v15, 0x1

    if-eqz v3, :cond_15

    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v3

    array-length v6, v0

    invoke-virtual {v3, v0, v11, v6}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    new-array v3, v8, [B

    aput-byte v7, v3, v11

    aput-byte v4, v3, v10

    const/4 v6, 0x2

    aput-byte v5, v3, v6

    const/16 v6, 0x32

    const/4 v8, 0x3

    aput-byte v6, v3, v8

    iget-object v8, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v8}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v8

    array-length v9, v3

    invoke-virtual {v8, v3, v11, v9}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a()Z

    move-result v12

    if-eqz v12, :cond_10

    iget-object v12, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v12}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v12

    array-length v13, v0

    sub-int/2addr v13, v3

    invoke-virtual {v12, v0, v3, v13}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v12

    add-int/2addr v3, v12

    const/4 v12, 0x6

    if-gt v12, v3, :cond_d

    const/4 v12, 0x0

    :goto_a
    add-int/lit8 v13, v3, -0x6

    if-gt v12, v13, :cond_f

    aget-byte v13, v0, v12

    if-ne v13, v7, :cond_e

    add-int/lit8 v13, v12, 0x1

    aget-byte v13, v0, v13

    if-ne v13, v4, :cond_e

    add-int/lit8 v13, v12, 0x2

    aget-byte v13, v0, v13

    if-ne v13, v5, :cond_e

    add-int/lit8 v13, v12, 0x3

    aget-byte v13, v0, v13

    if-ne v13, v6, :cond_e

    add-int/lit8 v8, v12, 0x4

    aget-byte v8, v0, v8

    invoke-static {v8}, Lcom/starmicronics/starioextension/ak;->a(B)I

    move-result v8

    add-int/lit8 v9, v12, 0x5

    aget-byte v9, v0, v9

    invoke-static {v9}, Lcom/starmicronics/starioextension/ak;->a(B)I

    move-result v9

    mul-int/lit16 v9, v9, 0x100

    add-int/2addr v8, v9

    add-int/lit8 v12, v12, 0x6

    sub-int/2addr v3, v12

    invoke-static {v0, v12, v0, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v9, v8

    const/4 v8, 0x1

    goto :goto_b

    :cond_e
    add-int/lit8 v12, v12, 0x1

    goto :goto_a

    :cond_f
    :goto_b
    if-eqz v8, :cond_d

    :cond_10
    if-eqz v8, :cond_15

    const/high16 v4, 0x10000

    if-gt v9, v4, :cond_15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->isInterrupted()Z

    move-result v6

    if-nez v6, :cond_13

    if-gt v9, v3, :cond_12

    goto :goto_c

    :cond_12
    iget-object v6, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v6

    array-length v7, v0

    sub-int/2addr v7, v3

    invoke-virtual {v6, v0, v3, v7}, Lcom/starmicronics/stario/StarIOPort;->readPort([BII)I

    move-result v6

    add-int/2addr v3, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    cmp-long v8, v6, v18

    if-lez v8, :cond_11

    :cond_13
    :goto_c
    if-gt v3, v9, :cond_15

    new-array v4, v3, [B

    invoke-static {v0, v11, v4, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v3, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v5, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$a;->q:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v3, v5, v6}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    invoke-virtual {v3, v4}, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a([B)V

    iget-object v4, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v4}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_d

    :cond_14
    iget-object v3, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->b(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$e;)V

    :cond_15
    :goto_d
    monitor-exit v2

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    nop

    goto/16 :goto_0

    :catch_2
    iget-object v0, v1, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0, v1}, Lcom/starmicronics/starioextension/StarIoExtManager;->b(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$e;)V

    goto/16 :goto_0

    :cond_16
    return-void
.end method
