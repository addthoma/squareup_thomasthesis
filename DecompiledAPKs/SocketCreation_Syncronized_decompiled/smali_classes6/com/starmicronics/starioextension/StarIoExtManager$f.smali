.class Lcom/starmicronics/starioextension/StarIoExtManager$f;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/StarIoExtManager;

.field private final b:I

.field private final c:I

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/16 p1, 0x3e8

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->b:I

    const p1, 0x493e0

    iput p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->c:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/StarIoExtManager$f;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    return-void
.end method


# virtual methods
.method declared-synchronized a(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->n(Lcom/starmicronics/starioextension/StarIoExtManager;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a()Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    monitor-enter v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    :try_start_0
    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v5

    if-eqz v5, :cond_d

    new-instance v5, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v5}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iput-boolean v4, v5, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v5

    invoke-virtual {v5}, Lcom/starmicronics/stario/StarIOPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v5

    iget v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-eqz v6, :cond_9

    iget-boolean v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->i(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Online:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    if-eq v6, v7, :cond_1

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Online:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->e:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    goto :goto_1

    :cond_0
    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->i(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Offline:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    if-eq v6, v7, :cond_1

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Offline:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->f:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    :goto_1
    invoke-virtual {v7, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v3, 0x1

    :cond_1
    iget v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-eqz v6, :cond_d

    iget-boolean v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->q(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Empty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    if-eq v6, v7, :cond_5

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Empty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->i:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    :goto_2
    invoke-virtual {v7, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v3, 0x1

    goto :goto_4

    :cond_2
    iget-boolean v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyInner:Z

    if-nez v6, :cond_4

    iget-boolean v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperNearEmptyOuter:Z

    if-eqz v6, :cond_3

    goto :goto_3

    :cond_3
    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->q(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Ready:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    if-eq v6, v7, :cond_5

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Ready:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->g:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    goto :goto_2

    :cond_4
    :goto_3
    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->q(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->NearEmpty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    if-eq v6, v7, :cond_5

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->NearEmpty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->h:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    goto :goto_2

    :cond_5
    :goto_4
    iget-boolean v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->r(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    if-eq v6, v7, :cond_7

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->j:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    goto :goto_5

    :cond_6
    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->r(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    move-result-object v6

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Close:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    if-eq v6, v7, :cond_7

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Close:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    invoke-static {v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    new-instance v6, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v8, Lcom/starmicronics/starioextension/StarIoExtManager$a;->k:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v6, v7, v8}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v7, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v7}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v7

    :goto_5
    invoke-virtual {v7, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v3, 0x1

    :cond_7
    iget-boolean v5, v5, Lcom/starmicronics/stario/StarPrinterStatus;->compulsionSwitch:Z

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->s(Lcom/starmicronics/starioextension/StarIoExtManager;)Z

    move-result v6

    if-ne v5, v6, :cond_8

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->t(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    move-result-object v5

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    if-eq v5, v6, :cond_d

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-static {v5, v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v5, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$a;->l:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v5, v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v6

    :goto_6
    invoke-virtual {v6, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_7

    :cond_8
    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->t(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    move-result-object v5

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Close:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    if-eq v5, v6, :cond_d

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Close:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-static {v5, v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v5, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v7, Lcom/starmicronics/starioextension/StarIoExtManager$a;->m:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v5, v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v6}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v6

    goto :goto_6

    :cond_9
    new-instance v5, Lcom/starmicronics/stario/StarIOPortException;

    const-string v6, "Status Length is 0."

    invoke-direct {v5, v6}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    goto/16 :goto_8

    :catch_0
    :try_start_1
    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a()Z

    move-result v5

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->i(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    move-result-object v5

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    if-eq v5, v6, :cond_d

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v5, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    invoke-static {v3, v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v5, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v3, v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v5, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    invoke-static {v3, v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v5, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-static {v3, v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v3, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$a;->a:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v3, v5, v6}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v3

    invoke-virtual {v3}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "tcp:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v3

    invoke-virtual {v3}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "bt:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_a
    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->u(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$b;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->u(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExtManager$b;->isAlive()Z

    move-result v3

    if-nez v3, :cond_c

    :cond_b
    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    new-instance v5, Lcom/starmicronics/starioextension/StarIoExtManager$b;

    iget-object v6, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/starmicronics/starioextension/StarIoExtManager$b;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V

    invoke-static {v3, v5}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$b;)Lcom/starmicronics/starioextension/StarIoExtManager$b;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v3}, Lcom/starmicronics/starioextension/StarIoExtManager;->u(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/starmicronics/starioextension/StarIoExtManager$b;->start()V

    :cond_c
    :goto_7
    const/4 v3, 0x1

    :cond_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v0

    const-wide/32 v7, 0x493e0

    cmp-long v9, v5, v7

    if-ltz v9, :cond_e

    const/4 v3, 0x1

    :cond_e
    if-eqz v3, :cond_f

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->j(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :cond_f
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v2, 0x3e8

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :catch_1
    nop

    goto/16 :goto_0

    :goto_8
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_10
    return-void
.end method
