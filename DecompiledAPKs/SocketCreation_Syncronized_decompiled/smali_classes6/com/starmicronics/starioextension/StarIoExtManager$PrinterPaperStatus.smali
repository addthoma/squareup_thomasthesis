.class public final enum Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrinterPaperStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

.field public static final enum Empty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

.field public static final enum Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

.field public static final enum Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

.field public static final enum NearEmpty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

.field public static final enum Ready:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    const/4 v1, 0x0

    const-string v2, "Invalid"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    const/4 v2, 0x1

    const-string v3, "Impossible"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    const/4 v3, 0x2

    const-string v4, "Ready"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Ready:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    const/4 v4, 0x3

    const-string v5, "NearEmpty"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->NearEmpty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    const/4 v5, 0x4

    const-string v6, "Empty"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Empty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    sget-object v6, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Ready:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->NearEmpty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Empty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    return-object v0
.end method
