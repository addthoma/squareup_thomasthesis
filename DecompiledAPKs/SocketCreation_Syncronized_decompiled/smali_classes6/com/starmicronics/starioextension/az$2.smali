.class Lcom/starmicronics/starioextension/az$2;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/az;->a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/az;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/az;)V
    .locals 3

    iput-object p1, p0, Lcom/starmicronics/starioextension/az$2;->a:Lcom/starmicronics/starioextension/az;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode1:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode2:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode3:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode4:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode5:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v1}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode6:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v2}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode7:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode8:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v1}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode9:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, p1, v2}, Lcom/starmicronics/starioextension/az$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
