.class Lcom/starmicronics/starioextension/o;
.super Lcom/starmicronics/starioextension/ah;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/o$a;
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ah;-><init>(I)V

    return-void
.end method

.method private static a([B)[B
    .locals 19

    move-object/from16 v0, p0

    array-length v1, v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-gt v1, v2, :cond_0

    return-object v3

    :cond_0
    const/4 v1, 0x0

    aget-byte v4, v0, v1

    const/16 v5, 0x7b

    if-eq v4, v5, :cond_1

    return-object v3

    :cond_1
    sget-object v4, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    aget-byte v6, v0, v2

    packed-switch v6, :pswitch_data_0

    return-object v3

    :pswitch_0
    sget-object v4, Lcom/starmicronics/starioextension/o$a;->c:Lcom/starmicronics/starioextension/o$a;

    goto :goto_0

    :pswitch_1
    sget-object v4, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    :goto_0
    :pswitch_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Lcom/starmicronics/starioextension/o$2;

    invoke-direct {v7}, Lcom/starmicronics/starioextension/o$2;-><init>()V

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x2

    move-object v9, v4

    const/4 v4, 0x2

    const/4 v8, 0x2

    :goto_1
    array-length v10, v0

    if-ge v4, v10, :cond_18

    aget-byte v10, v0, v8

    const/4 v11, 0x4

    const/4 v12, 0x3

    if-ne v10, v5, :cond_f

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v4, v4, 0x1

    array-length v10, v0

    if-gt v10, v4, :cond_2

    return-object v3

    :cond_2
    aget-byte v10, v0, v8

    sget-object v14, Lcom/starmicronics/starioextension/o$4;->a:[I

    invoke-virtual {v9}, Lcom/starmicronics/starioextension/o$a;->ordinal()I

    move-result v15

    aget v14, v14, v15

    const/16 v15, 0x43

    const/16 v1, 0x42

    const/16 v16, 0x64

    const/16 v17, 0x65

    const/16 v18, 0x66

    const/16 v13, 0x53

    if-eq v14, v2, :cond_a

    if-eq v14, v7, :cond_4

    if-eq v14, v12, :cond_5

    if-eq v14, v11, :cond_3

    goto :goto_2

    :cond_3
    const/16 v11, 0x31

    if-eq v10, v11, :cond_6

    const/16 v9, 0x41

    if-eq v10, v9, :cond_9

    if-eq v10, v1, :cond_e

    return-object v3

    :cond_4
    sget-object v9, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    if-ne v10, v13, :cond_5

    return-object v3

    :cond_5
    const/16 v1, 0x41

    if-eq v10, v1, :cond_9

    if-eq v10, v15, :cond_d

    if-eq v10, v13, :cond_8

    if-eq v10, v5, :cond_7

    packed-switch v10, :pswitch_data_1

    return-object v3

    :pswitch_3
    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto/16 :goto_6

    :pswitch_4
    const/16 v1, 0x60

    goto/16 :goto_5

    :pswitch_5
    const/16 v1, 0x61

    goto/16 :goto_5

    :cond_6
    :pswitch_6
    invoke-static/range {v18 .. v18}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto/16 :goto_6

    :cond_7
    const/16 v1, 0x5b

    goto/16 :goto_5

    :cond_8
    const/16 v1, 0x62

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v9, Lcom/starmicronics/starioextension/o$a;->d:Lcom/starmicronics/starioextension/o$a;

    goto/16 :goto_8

    :cond_9
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v9, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    goto/16 :goto_8

    :cond_a
    sget-object v9, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    if-ne v10, v13, :cond_b

    return-object v3

    :cond_b
    :goto_2
    if-eq v10, v1, :cond_e

    if-eq v10, v15, :cond_d

    if-eq v10, v13, :cond_c

    packed-switch v10, :pswitch_data_2

    return-object v3

    :pswitch_7
    invoke-static/range {v17 .. v17}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_6

    :cond_c
    const/16 v1, 0x62

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v9, Lcom/starmicronics/starioextension/o$a;->e:Lcom/starmicronics/starioextension/o$a;

    goto :goto_8

    :cond_d
    const/16 v1, 0x63

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v9, Lcom/starmicronics/starioextension/o$a;->c:Lcom/starmicronics/starioextension/o$a;

    goto :goto_8

    :cond_e
    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v9, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    goto :goto_8

    :cond_f
    aget-byte v1, v0, v8

    sget-object v10, Lcom/starmicronics/starioextension/o$4;->a:[I

    invoke-virtual {v9}, Lcom/starmicronics/starioextension/o$a;->ordinal()I

    move-result v13

    aget v10, v10, v13

    const/16 v13, 0x20

    if-eq v10, v2, :cond_15

    if-eq v10, v7, :cond_12

    if-eq v10, v12, :cond_13

    if-eq v10, v11, :cond_10

    goto :goto_7

    :cond_10
    if-ltz v1, :cond_11

    const/16 v10, 0x63

    if-gt v1, v10, :cond_11

    goto :goto_5

    :cond_11
    return-object v3

    :cond_12
    sget-object v9, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    :cond_13
    if-lt v1, v13, :cond_14

    const/16 v10, 0x7f

    if-gt v1, v10, :cond_14

    :goto_3
    add-int/lit8 v1, v1, -0x20

    :goto_4
    int-to-byte v1, v1

    :goto_5
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    :goto_6
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_14
    return-object v3

    :cond_15
    sget-object v9, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    :goto_7
    if-ltz v1, :cond_16

    const/16 v10, 0x1f

    if-gt v1, v10, :cond_16

    add-int/lit8 v1, v1, 0x40

    goto :goto_4

    :cond_16
    if-lt v1, v13, :cond_17

    const/16 v10, 0x5f

    if-gt v1, v10, :cond_17

    goto :goto_3

    :goto_8
    add-int/2addr v8, v2

    add-int/2addr v4, v2

    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_17
    return-object v3

    :cond_18
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    add-int/2addr v1, v0

    :goto_9
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_19

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    mul-int v3, v3, v2

    add-int/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_19
    rem-int/lit8 v1, v1, 0x67

    int-to-byte v1, v1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x6a

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [B

    :goto_a
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1a

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_1a
    return-object v1

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x31
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x31
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method

.method private static b([B)[I
    .locals 17

    move-object/from16 v0, p0

    array-length v1, v0

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    sget-object v1, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    aget-byte v3, v0, v2

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v1, Lcom/starmicronics/starioextension/o$a;->c:Lcom/starmicronics/starioextension/o$a;

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    :goto_0
    :pswitch_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x2

    move-object v6, v1

    const/4 v1, 0x2

    const/4 v5, 0x2

    :goto_1
    array-length v7, v0

    if-ge v1, v7, :cond_13

    aget-byte v7, v0, v5

    const/4 v8, 0x4

    const/4 v9, 0x3

    const/16 v10, 0x7b

    if-ne v7, v10, :cond_e

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v1, v1, 0x1

    aget-byte v7, v0, v5

    sget-object v11, Lcom/starmicronics/starioextension/o$4;->a:[I

    invoke-virtual {v6}, Lcom/starmicronics/starioextension/o$a;->ordinal()I

    move-result v12

    aget v11, v11, v12

    const/16 v12, -0x7c

    const/16 v15, 0x53

    const/16 v14, 0x43

    const/16 v13, 0x42

    const/16 v16, -0x7f

    if-eq v11, v2, :cond_b

    const/16 v2, 0x41

    if-eq v11, v4, :cond_5

    if-eq v11, v9, :cond_6

    if-eq v11, v8, :cond_1

    goto :goto_4

    :cond_1
    const/16 v8, 0x31

    if-eq v7, v8, :cond_4

    if-eq v7, v2, :cond_3

    if-eq v7, v13, :cond_2

    goto/16 :goto_7

    :cond_2
    sget-object v6, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    goto/16 :goto_7

    :cond_3
    sget-object v6, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    goto/16 :goto_7

    :cond_4
    :pswitch_3
    invoke-static/range {v16 .. v16}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    goto/16 :goto_6

    :cond_5
    sget-object v6, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    :cond_6
    if-eq v7, v2, :cond_a

    if-eq v7, v14, :cond_9

    if-eq v7, v15, :cond_8

    if-eq v7, v10, :cond_7

    packed-switch v7, :pswitch_data_1

    goto/16 :goto_7

    :pswitch_4
    invoke-static {v12}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    goto :goto_6

    :pswitch_5
    const/16 v2, -0x7d

    goto :goto_2

    :pswitch_6
    const/16 v2, -0x7e

    :goto_2
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    goto :goto_6

    :cond_7
    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    goto :goto_6

    :cond_8
    sget-object v2, Lcom/starmicronics/starioextension/o$a;->d:Lcom/starmicronics/starioextension/o$a;

    goto :goto_3

    :cond_9
    sget-object v2, Lcom/starmicronics/starioextension/o$a;->c:Lcom/starmicronics/starioextension/o$a;

    goto :goto_3

    :cond_a
    sget-object v2, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    :goto_3
    move-object v6, v2

    goto :goto_7

    :cond_b
    sget-object v6, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    :goto_4
    if-eq v7, v13, :cond_d

    if-eq v7, v14, :cond_9

    if-eq v7, v15, :cond_c

    packed-switch v7, :pswitch_data_2

    goto :goto_7

    :cond_c
    sget-object v2, Lcom/starmicronics/starioextension/o$a;->e:Lcom/starmicronics/starioextension/o$a;

    goto :goto_3

    :cond_d
    sget-object v2, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    goto :goto_3

    :cond_e
    aget-byte v2, v0, v5

    sget-object v7, Lcom/starmicronics/starioextension/o$4;->a:[I

    invoke-virtual {v6}, Lcom/starmicronics/starioextension/o$a;->ordinal()I

    move-result v10

    aget v7, v7, v10

    const/4 v10, 0x1

    if-eq v7, v10, :cond_11

    if-eq v7, v4, :cond_10

    if-eq v7, v9, :cond_12

    if-eq v7, v8, :cond_f

    goto :goto_5

    :cond_f
    div-int/lit8 v7, v2, 0xa

    add-int/lit8 v7, v7, 0x30

    int-to-byte v7, v7

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    rem-int/lit8 v2, v2, 0xa

    add-int/lit8 v2, v2, 0x30

    int-to-byte v2, v2

    goto :goto_5

    :cond_10
    sget-object v6, Lcom/starmicronics/starioextension/o$a;->a:Lcom/starmicronics/starioextension/o$a;

    goto :goto_5

    :cond_11
    sget-object v6, Lcom/starmicronics/starioextension/o$a;->b:Lcom/starmicronics/starioextension/o$a;

    :cond_12
    :goto_5
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    :goto_6
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    const/4 v2, 0x1

    add-int/2addr v5, v2

    add-int/2addr v1, v2

    goto/16 :goto_1

    :cond_13
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    const/4 v1, 0x0

    :goto_8
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_14

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_14
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x31
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x31
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private static c([B)Ljava/lang/String;
    .locals 5

    new-instance v0, Lcom/starmicronics/starioextension/o$3;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/o$3;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-byte v4, p0, v3

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-nez v4, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-string p0, "11"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lcom/starmicronics/starioextension/o;->a([B)[B

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-static {v0}, Lcom/starmicronics/starioextension/o;->c([B)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    new-instance v1, Lcom/starmicronics/starioextension/o$1;

    invoke-direct {v1, p0}, Lcom/starmicronics/starioextension/o$1;-><init>(Lcom/starmicronics/starioextension/o;)V

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, v0, p2}, Lcom/starmicronics/starioextension/o;->a(Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/starmicronics/starioextension/o;->b([B)[I

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/o;->a([I)V

    return-void
.end method
