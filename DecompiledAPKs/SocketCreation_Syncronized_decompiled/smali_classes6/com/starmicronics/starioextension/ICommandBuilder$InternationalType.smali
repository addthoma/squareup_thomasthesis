.class public final enum Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ICommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InternationalType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Denmark:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Denmark2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum France:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Germany:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Ireland:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Italy:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Japan:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Korea:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum LatinAmerica:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Legal:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Norway:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Spain:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Spain2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum Sweden:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum UK:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

.field public static final enum USA:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v1, 0x0

    const-string v2, "USA"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->USA:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v2, 0x1

    const-string v3, "France"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->France:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v3, 0x2

    const-string v4, "Germany"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Germany:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v4, 0x3

    const-string v5, "UK"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->UK:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v5, 0x4

    const-string v6, "Denmark"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Denmark:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v6, 0x5

    const-string v7, "Sweden"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Sweden:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v7, 0x6

    const-string v8, "Italy"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Italy:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/4 v8, 0x7

    const-string v9, "Spain"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Spain:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v9, 0x8

    const-string v10, "Japan"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Japan:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v10, 0x9

    const-string v11, "Norway"

    invoke-direct {v0, v11, v10}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Norway:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v11, 0xa

    const-string v12, "Denmark2"

    invoke-direct {v0, v12, v11}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Denmark2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v12, 0xb

    const-string v13, "Spain2"

    invoke-direct {v0, v13, v12}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Spain2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v13, 0xc

    const-string v14, "LatinAmerica"

    invoke-direct {v0, v14, v13}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->LatinAmerica:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v14, 0xd

    const-string v15, "Korea"

    invoke-direct {v0, v15, v14}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Korea:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v15, 0xe

    const-string v14, "Ireland"

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Ireland:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const-string v14, "Legal"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Legal:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    sget-object v14, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->USA:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->France:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Germany:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->UK:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Denmark:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Sweden:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Italy:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Spain:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Japan:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Norway:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Denmark2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Spain2:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->LatinAmerica:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Korea:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Ireland:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->Legal:Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;

    return-object v0
.end method
