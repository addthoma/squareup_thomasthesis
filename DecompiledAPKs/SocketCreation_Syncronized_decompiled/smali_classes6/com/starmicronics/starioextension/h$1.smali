.class final Lcom/starmicronics/starioextension/h$1;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/h;->a(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/16 v1, 0x30

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/h$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Left90:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/16 v1, 0x31

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/h$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Rotate180:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/h$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Right90:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    const/16 v1, 0x33

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/h$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
