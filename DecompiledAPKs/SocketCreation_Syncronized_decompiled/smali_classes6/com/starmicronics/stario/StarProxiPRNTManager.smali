.class public final Lcom/starmicronics/stario/StarProxiPRNTManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/StarProxiPRNTManager$b;,
        Lcom/starmicronics/stario/StarProxiPRNTManager$a;
    }
.end annotation


# static fields
.field public static final KEY_STAR_DEVICETYPE:Ljava/lang/String; = "Device Type"

.field public static final KEY_STAR_MACADDRESS:Ljava/lang/String; = "MAC Address"

.field public static final KEY_STAR_NICKNAME:Ljava/lang/String; = "Nick Name"

.field public static final KEY_STAR_PORTSETTIGS:Ljava/lang/String; = "Port Settings"

.field public static final KEY_STAR_THRESHOLDRSSI:Ljava/lang/String; = "Threshold RSSI"

.field public static final KEY_STAR_WITHDRAWER:Ljava/lang/String; = "With Drawer"

.field private static m:I = 0x7

.field private static final n:Ljava/lang/Object;

.field private static final o:Ljava/lang/Object;

.field private static final p:Ljava/lang/Object;

.field private static q:Z


# instance fields
.field private a:Landroid/bluetooth/BluetoothAdapter;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private f:Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:I

.field private l:Ljava/lang/String;

.field private r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->n:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->o:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->p:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->q:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->f:Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    const-string v0, ""

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->h:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->j:Z

    iput v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->l:Ljava/lang/String;

    new-instance v0, Lcom/starmicronics/stario/StarProxiPRNTManager$1;

    invoke-direct {v0, p0}, Lcom/starmicronics/stario/StarProxiPRNTManager$1;-><init>(Lcom/starmicronics/stario/StarProxiPRNTManager;)V

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->c:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->d:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->e:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/starmicronics/stario/StarProxiPRNTManager$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/stario/StarProxiPRNTManager;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/stario/StarProxiPRNTManager;I)I
    .locals 0

    iput p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I

    return p1
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->n:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/starmicronics/stario/StarProxiPRNTManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarProxiPRNTManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "MAC Address"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method static synthetic a(Lcom/starmicronics/stario/StarProxiPRNTManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    return p0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->q:Z

    return p0
.end method

.method static synthetic b()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->o:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/starmicronics/stario/StarProxiPRNTManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->d:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic c()I
    .locals 1

    sget v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->m:I

    return v0
.end method

.method static synthetic c(Lcom/starmicronics/stario/StarProxiPRNTManager;)Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->f:Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/starmicronics/stario/StarProxiPRNTManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->c:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic d()Z
    .locals 1

    sget-boolean v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->e:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic f(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic g(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->g:Ljava/lang/String;

    return-object p0
.end method

.method public static getSharedManager()Lcom/starmicronics/stario/StarProxiPRNTManager;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager$a;->a:Lcom/starmicronics/stario/StarProxiPRNTManager;

    return-object v0

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic h(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->h:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic i(Lcom/starmicronics/stario/StarProxiPRNTManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->l:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public addSettingForDKAirCashPortName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->p:Ljava/lang/Object;

    monitor-enter v0

    if-eqz p1, :cond_4

    if-eqz p3, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "MAC Address"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "The specified macAddress is already exists."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "Device Type"

    const-string v3, "DK-AirCash"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "With Drawer"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "MAC Address"

    invoke-virtual {v1, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p3, "Nick Name"

    invoke-virtual {v1, p3, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_2

    const-string p3, "Port Settings"

    invoke-virtual {v1, p3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const-string p2, "Threshold RSSI"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {p2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v0

    return-void

    :cond_3
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "The specified portName is already exists."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "The specified portName or macAddress is null."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public addSettingForPrinterPortName(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->p:Ljava/lang/Object;

    monitor-enter v0

    if-eqz p1, :cond_4

    if-eqz p4, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "MAC Address"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eq p4, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "The specified macAddress is already exists."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "Device Type"

    const-string v3, "Printer"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "With Drawer"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {v1, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p3, "MAC Address"

    invoke-virtual {v1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p3, "Nick Name"

    invoke-virtual {v1, p3, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_2

    const-string p3, "Port Settings"

    invoke-virtual {v1, p3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const-string p2, "Threshold RSSI"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v1, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {p2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v0

    return-void

    :cond_3
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "portName is already exists."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "portName or macAddress is null."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public calibrateActionArea(Ljava/lang/String;)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_7

    const-string v0, "^[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    const v2, -0x7fffffff

    const/high16 v3, -0x80000000

    if-nez v1, :cond_3

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->l:Ljava/lang/String;

    iget-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {p1, v1}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    const/4 p1, 0x0

    const/high16 v1, -0x80000000

    const/high16 v4, -0x80000000

    :goto_0
    const/16 v5, 0xf

    if-ge p1, v5, :cond_2

    iput v3, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v5, 0x12c

    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    nop

    :goto_1
    const/4 v5, 0x4

    if-le p1, v5, :cond_1

    :try_start_2
    iget v5, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I

    if-le v5, v4, :cond_0

    iget v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I

    move v7, v4

    move v4, v1

    move v1, v7

    goto :goto_2

    :cond_0
    iget v5, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I

    if-le v5, v1, :cond_1

    iget v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->k:I

    :cond_1
    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v4, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {p1, v4}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    goto :goto_3

    :cond_3
    const v1, -0x7fffffff

    :goto_3
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eq v1, v3, :cond_5

    if-eq v1, v2, :cond_4

    return v1

    :cond_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "A duplicate execution reuest with \'startScan\' was found."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Couldn\'t find device."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    :cond_6
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Address doesn\'t exist or is not accessible. For example, address is \'00:11:22:AA:BB:CC\'."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public deserializeSetting([B)Z
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "MAC Address"

    const-string v2, "Port Settings"

    const-string v3, "Threshold RSSI"

    const-string v4, "Nick Name"

    const-string v5, "With Drawer"

    const-string v6, "Device Type"

    if-eqz v0, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3

    :try_start_1
    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v10, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v8, v6, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8, v5, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8, v4, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8, v3, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8, v2, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v8, v1, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v10, p0

    :try_start_2
    iget-object v11, v10, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    move-object/from16 v10, p0

    :goto_1
    :try_start_3
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    const/4 v1, 0x0

    return v1

    :catch_2
    move-exception v0

    goto :goto_2

    :cond_0
    move-object/from16 v10, p0

    const/4 v0, 0x1

    return v0

    :catch_3
    move-exception v0

    move-object/from16 v10, p0

    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    const/4 v1, 0x0

    return v1

    :cond_1
    move-object/from16 v10, p0

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "The specified settingsByteArray is null."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAllSettings()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->p:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getCallback()Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->f:Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    return-object v0
.end method

.method public getClosestCashDrawer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getClosestPrinter()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getRSSI(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->o:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/HashMap;

    const-string v1, "RSSI"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getSettings(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->p:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/HashMap;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "The inputed portName is null."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public removeSettingsWithPortName(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->p:Ljava/lang/Object;

    monitor-enter v0

    if-eqz p1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v2, "MAC Address"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->c:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->c:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v0

    return-void

    :cond_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "The settings of specified portName is not exists."

    invoke-direct {p1, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "The specified portName is null."

    invoke-direct {p1, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public serializedSettings()[B
    .locals 16

    move-object/from16 v1, p0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    iget-object v0, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    iget-object v5, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    const-string v6, "Device Type"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v7, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    const-string v8, "With Drawer"

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-object v9, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    const-string v10, "Nick Name"

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iget-object v11, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    const-string v12, "Threshold RSSI"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iget-object v13, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v13, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/HashMap;

    const-string v14, "Port Settings"

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    iget-object v15, v1, Lcom/starmicronics/stario/StarProxiPRNTManager;->b:Ljava/util/HashMap;

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/HashMap;

    const-string v1, "MAC Address"

    invoke-virtual {v15, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    :try_start_0
    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v4, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    invoke-virtual {v4, v10, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v4, v12, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v4, v14, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v4, v1, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    :goto_1
    move-object/from16 v1, p0

    goto/16 :goto_0

    :cond_0
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public setCallback(Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->f:Lcom/starmicronics/stario/StarProxiPRNTManagerCallback;

    return-void
.end method

.method public startScan()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_4

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v4, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v1, v4}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    iget-boolean v4, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->j:Z

    if-ne v4, v3, :cond_2

    iput-boolean v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->j:Z

    new-instance v4, Lcom/starmicronics/stario/StarProxiPRNTManager$b;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;-><init>(Lcom/starmicronics/stario/StarProxiPRNTManager;Lcom/starmicronics/stario/StarProxiPRNTManager$1;)V

    new-array v5, v2, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x1

    :cond_2
    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "A duplicate execution reuest with \'startScan\' or \'calibrateActionArea\' was found."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_4
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startScan(I)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_5

    if-lez p1, :cond_4

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    sput p1, Lcom/starmicronics/stario/StarProxiPRNTManager;->m:I

    iget-object p1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {p1, v1}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result p1

    if-ne p1, v3, :cond_2

    iput-boolean v3, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    iget-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->j:Z

    if-ne v1, v3, :cond_2

    iput-boolean v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->j:Z

    new-instance v1, Lcom/starmicronics/stario/StarProxiPRNTManager$b;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;-><init>(Lcom/starmicronics/stario/StarProxiPRNTManager;Lcom/starmicronics/stario/StarProxiPRNTManager$1;)V

    new-array v4, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v4}, Lcom/starmicronics/stario/StarProxiPRNTManager$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    const/4 v2, 0x1

    :cond_2
    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v3, :cond_3

    return p1

    :cond_3
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "A duplicate execution reuest with \'startScan\' or \'calibrateActionArea\' was found."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_4
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "Argument is invalid range value."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v0, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {p1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public stopScan()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/starmicronics/stario/StarProxiPRNTManager;->n:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->j:Z

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->a:Landroid/bluetooth/BluetoothAdapter;

    iget-object v2, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->r:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    const-wide/16 v1, 0x64

    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/stario/StarProxiPRNTManager;->i:Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This device can not use this method, need to be supported Bluetooth Low Energy."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
