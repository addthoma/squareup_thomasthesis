.class public abstract Lcom/starmicronics/stario/StarIOPort;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/StarIOPort$a;,
        Lcom/starmicronics/stario/StarIOPort$b;
    }
.end annotation


# static fields
.field private static final d:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/starmicronics/stario/StarIOPort;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/starmicronics/stario/StarIOPort;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/StarIOPort;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/starmicronics/stario/StarIOPort;->c:I

    return-void
.end method

.method public static InterruptOpen()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-static {}, Lcom/starmicronics/stario/g;->b()V

    return-void
.end method

.method public static compressRasterData(II[BLjava/lang/String;)[B
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MINI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/starmicronics/stario/StarIOPort$a;->c:Lcom/starmicronics/stario/StarIOPort$a;

    invoke-virtual {v3}, Lcom/starmicronics/stario/StarIOPort$a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p3

    sget-object v0, Lcom/starmicronics/stario/StarIOPort$b;->b:Lcom/starmicronics/stario/StarIOPort$b;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPort$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-nez p3, :cond_1

    const/4 p0, 0x0

    return-object p0

    :cond_1
    if-lez p0, :cond_12

    if-lez p1, :cond_11

    if-eqz p2, :cond_10

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :cond_2
    if-lez p1, :cond_e

    const/16 v3, 0xff

    if-le p1, v3, :cond_3

    goto :goto_1

    :cond_3
    move v3, p1

    :goto_1
    sub-int/2addr p1, v3

    const/4 v4, 0x5

    new-array v4, v4, [B

    fill-array-data v4, :array_0

    int-to-byte v5, p0

    const/4 v6, 0x3

    aput-byte v5, v4, v6

    int-to-byte v5, v3

    const/4 v7, 0x4

    aput-byte v5, v4, v7

    const/4 v8, 0x0

    :goto_2
    array-length v9, v4

    if-ge v8, v9, :cond_4

    aget-byte v9, v4, v8

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    invoke-virtual {p3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_4
    mul-int v3, v3, p0

    :goto_3
    if-lez v3, :cond_d

    const/4 v4, 0x2

    if-ge v3, v4, :cond_5

    const/16 v4, -0x7f

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    aget-byte v4, p2, v0

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    :cond_5
    aget-byte v8, p2, v0

    add-int/lit8 v9, v0, 0x1

    aget-byte v9, p2, v9

    const/16 v10, 0x3a

    if-ne v8, v9, :cond_8

    aget-byte v8, p2, v0

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v3, v3, -0x2

    :goto_4
    if-lez v3, :cond_7

    if-ge v4, v10, :cond_7

    aget-byte v9, p2, v0

    if-eq v8, v9, :cond_6

    goto :goto_5

    :cond_6
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_4

    :cond_7
    :goto_5
    or-int/lit16 v4, v4, 0xc0

    int-to-byte v4, v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    aget-byte v8, p2, v0

    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v8, v0

    const/4 v0, 0x0

    :cond_9
    :goto_6
    if-lez v3, :cond_b

    if-ge v0, v10, :cond_b

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v3, v3, -0x1

    if-le v3, v2, :cond_a

    aget-byte v9, p2, v8

    add-int/lit8 v11, v8, 0x1

    aget-byte v11, p2, v11

    if-ne v9, v11, :cond_a

    goto :goto_7

    :cond_a
    array-length v9, p2

    if-ge v8, v9, :cond_9

    if-ge v0, v10, :cond_9

    aget-byte v9, p2, v8

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_b
    :goto_7
    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    :goto_8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v0, v9, :cond_c

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {p3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    move v0, v8

    goto/16 :goto_3

    :cond_d
    new-array v3, v7, [B

    fill-array-data v3, :array_1

    aput-byte v5, v3, v6

    const/4 v4, 0x0

    :goto_9
    array-length v5, v3

    if-ge v4, v5, :cond_2

    aget-byte v5, v3, v4

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_e
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array p0, p0, [B

    :goto_a
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge v1, p1, :cond_f

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    aput-byte p1, p0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_f
    return-object p0

    :cond_10
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "The specified imageData is invalid. - The current imageData is null."

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_11
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "The specified height is invalid. - The current width is 0 or less than 0"

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_12
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "The specified width is invalid. - The current width is 0 or less than 0"

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x58t
        0x33t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x58t
        0x32t
        0x0t
    .end array-data
.end method

.method public static generateBitImageCommand(II[BLjava/lang/String;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p0, p1, p2, p3}, Lcom/starmicronics/stario/StarIOPort;->compressRasterData(II[BLjava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized getPort(Ljava/lang/String;Ljava/lang/String;I)Lcom/starmicronics/stario/StarIOPort;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-class v0, Lcom/starmicronics/stario/StarIOPort;

    monitor-enter v0

    if-eqz p0, :cond_b

    if-nez p1, :cond_0

    :try_start_0
    const-string p1, ""

    :cond_0
    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v1, v2, :cond_3

    sget-object v2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/starmicronics/stario/StarIOPort;

    invoke-virtual {v2}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object p0, v2, Lcom/starmicronics/stario/StarIOPort;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget p0, v2, Lcom/starmicronics/stario/StarIOPort;->c:I

    add-int/2addr p0, v3

    iput p0, v2, Lcom/starmicronics/stario/StarIOPort;->c:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw p0

    :cond_1
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "This port is already opened and is configured with different settings."

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lcom/starmicronics/stario/TCPPort;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MINI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PORTABLE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ESCPOS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "WL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lcom/starmicronics/stario/h;

    invoke-direct {v1, p0, p1, p2}, Lcom/starmicronics/stario/h;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_5
    new-instance v1, Lcom/starmicronics/stario/TCPPort;

    invoke-direct {v1, p0, p1, p2}, Lcom/starmicronics/stario/TCPPort;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_6
    :goto_1
    new-instance v1, Lcom/starmicronics/stario/WTCPPort;

    invoke-direct {v1, p0, p1, p2}, Lcom/starmicronics/stario/WTCPPort;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_7
    invoke-static {p0}, Lcom/starmicronics/stario/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MINI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PORTABLE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ESCPOS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    goto :goto_2

    :cond_8
    new-instance v1, Lcom/starmicronics/stario/a;

    invoke-direct {v1, p0, p1, p2}, Lcom/starmicronics/stario/a;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_9
    :goto_2
    new-instance v1, Lcom/starmicronics/stario/g;

    invoke-direct {v1, p0, p1, p2}, Lcom/starmicronics/stario/g;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_3
    iput v3, v1, Lcom/starmicronics/stario/StarIOPort;->c:I

    sget-object p2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {p2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iput-object p0, v1, Lcom/starmicronics/stario/StarIOPort;->a:Ljava/lang/String;

    iput-object p1, v1, Lcom/starmicronics/stario/StarIOPort;->b:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v0

    return-object v1

    :cond_a
    :try_start_4
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "Failed to open port"

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_b
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "Invalid port name."

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized getPort(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/starmicronics/stario/StarIOPort;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-class v0, Lcom/starmicronics/stario/StarIOPort;

    monitor-enter v0

    if-eqz p0, :cond_c

    if-nez p1, :cond_0

    :try_start_0
    const-string p1, ""

    :cond_0
    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v1, v2, :cond_3

    sget-object v2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/starmicronics/stario/StarIOPort;

    invoke-virtual {v2}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object p0, v2, Lcom/starmicronics/stario/StarIOPort;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget p0, v2, Lcom/starmicronics/stario/StarIOPort;->c:I

    add-int/2addr p0, v3

    iput p0, v2, Lcom/starmicronics/stario/StarIOPort;->c:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw p0

    :cond_1
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "This port is already opened and is configured with different settings."

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lcom/starmicronics/stario/TCPPort;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "MINI"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_6

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "PORTABLE"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_4

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "ESCPOS"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_4

    goto :goto_1

    :cond_4
    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "WL"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_5

    new-instance p3, Lcom/starmicronics/stario/h;

    invoke-direct {p3, p0, p1, p2}, Lcom/starmicronics/stario/h;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_5
    new-instance p3, Lcom/starmicronics/stario/TCPPort;

    invoke-direct {p3, p0, p1, p2}, Lcom/starmicronics/stario/TCPPort;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_6
    :goto_1
    new-instance p3, Lcom/starmicronics/stario/WTCPPort;

    invoke-direct {p3, p0, p1, p2}, Lcom/starmicronics/stario/WTCPPort;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_7
    invoke-static {p0}, Lcom/starmicronics/stario/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "MINI"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_9

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "PORTABLE"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_8

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "ESCPOS"

    invoke-virtual {p3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_8

    goto :goto_2

    :cond_8
    new-instance p3, Lcom/starmicronics/stario/a;

    invoke-direct {p3, p0, p1, p2}, Lcom/starmicronics/stario/a;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_9
    :goto_2
    new-instance p3, Lcom/starmicronics/stario/g;

    invoke-direct {p3, p0, p1, p2}, Lcom/starmicronics/stario/g;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    :cond_a
    invoke-static {p0}, Lcom/starmicronics/stario/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Lcom/starmicronics/stario/e;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/starmicronics/stario/e;-><init>(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V

    move-object p3, v1

    :goto_3
    iput v3, p3, Lcom/starmicronics/stario/StarIOPort;->c:I

    sget-object p2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {p2, p3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iput-object p0, p3, Lcom/starmicronics/stario/StarIOPort;->a:Ljava/lang/String;

    iput-object p1, p3, Lcom/starmicronics/stario/StarIOPort;->b:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v0

    return-object p3

    :cond_b
    :try_start_4
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "Failed to open port"

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_c
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "Invalid port name."

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized getStarIOVersion()Ljava/lang/String;
    .locals 5

    const-class v0, Lcom/starmicronics/stario/StarIOPort;

    monitor-enter v0

    :try_start_0
    const-string v1, "2"

    const-string v2, "4"

    const-string v3, "0"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized releasePort(Lcom/starmicronics/stario/StarIOPort;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-class v0, Lcom/starmicronics/stario/StarIOPort;

    monitor-enter v0

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    sget-object v2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    sget-object v2, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/starmicronics/stario/StarIOPort;

    if-ne v2, p0, :cond_1

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v3, v2, Lcom/starmicronics/stario/StarIOPort;->c:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/starmicronics/stario/StarIOPort;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v3, :cond_0

    :try_start_2
    invoke-virtual {v2}, Lcom/starmicronics/stario/StarIOPort;->a()V
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    :try_start_3
    sget-object v3, Lcom/starmicronics/stario/StarIOPort;->d:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception p0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    monitor-exit v0

    return-void

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized searchPrinter(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/stario/PortInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-class v0, Lcom/starmicronics/stario/StarIOPort;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "TCP:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/starmicronics/stario/TCPPort;->c()Ljava/util/ArrayList;

    move-result-object p0

    :goto_0
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_0
    const-string v2, "BT:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/starmicronics/stario/a;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :goto_1
    monitor-exit v0

    return-object v1

    :cond_1
    :try_start_1
    const-string v1, "StarIOVer:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-static {}, Lcom/starmicronics/stario/StarIOPort;->getStarIOVersion()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Invalid argument."

    invoke-direct {p0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized searchPrinter(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/stario/PortInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-class v0, Lcom/starmicronics/stario/StarIOPort;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "TCP:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/starmicronics/stario/TCPPort;->c()Ljava/util/ArrayList;

    move-result-object p0

    :goto_0
    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_0
    const-string v2, "BT:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/starmicronics/stario/a;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    goto :goto_0

    :cond_1
    const-string v2, "USB:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    invoke-static {p1}, Lcom/starmicronics/stario/e;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :goto_1
    monitor-exit v0

    return-object v1

    :cond_2
    :try_start_1
    new-instance p0, Lcom/starmicronics/stario/StarIOPortException;

    const-string p1, "Invalid argument."

    invoke-direct {p0, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method protected abstract a()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract getDipSwitchInformation()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract getFirmwareInformation()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public declared-synchronized getPortName()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/StarIOPort;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPortSettings()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/stario/StarIOPort;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract readPort([BII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract setEndCheckedBlockTimeoutMillis(I)V
.end method

.method public abstract writePort([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method
