.class public final Lcom/squareup/edititem/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/edititem/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_variation:I = 0x7f12009e

.field public static final assign_stock_error_message:I = 0x7f1200ea

.field public static final assign_stock_error_title:I = 0x7f1200eb

.field public static final category_create:I = 0x7f1203d8

.field public static final category_label_with_name:I = 0x7f1203db

.field public static final checkout_link_settings:I = 0x7f120410

.field public static final checkout_link_settings_help_text:I = 0x7f120411

.field public static final checkout_link_settings_to_checkout_links_url:I = 0x7f120412

.field public static final checkout_link_share_help_text:I = 0x7f120413

.field public static final checkout_link_share_subject:I = 0x7f120414

.field public static final checkout_link_staging_with_item_id:I = 0x7f120415

.field public static final checkout_link_toggle_label:I = 0x7f120416

.field public static final checkout_link_with_item_id:I = 0x7f120417

.field public static final discount_modify_tax_basis_title:I = 0x7f12087f

.field public static final discount_name_required_warning_message:I = 0x7f120881

.field public static final discount_name_required_warning_title:I = 0x7f120882

.field public static final discount_options:I = 0x7f120883

.field public static final discount_rule_type_date_range:I = 0x7f120885

.field public static final discount_rule_type_date_range_ends:I = 0x7f120886

.field public static final discount_rule_type_date_range_starts:I = 0x7f120887

.field public static final discount_rule_type_items:I = 0x7f120888

.field public static final discount_rule_type_schedule:I = 0x7f120889

.field public static final discount_rules_help_text:I = 0x7f12088a

.field public static final discount_variable_auto_apply_warning_text:I = 0x7f12088b

.field public static final discount_variable_auto_apply_warning_title:I = 0x7f12088c

.field public static final duplicate_sku_warning_item_description_multiple_variations:I = 0x7f1208ea

.field public static final duplicate_sku_warning_item_description_one_of_variations:I = 0x7f1208eb

.field public static final duplicate_sku_warning_item_description_only_one_variation:I = 0x7f1208ec

.field public static final duplicate_sku_warning_more_then_three_skus:I = 0x7f1208ed

.field public static final duplicate_sku_warning_one_sku_more_than_three_item:I = 0x7f1208ee

.field public static final duplicate_sku_warning_one_sku_one_item:I = 0x7f1208ef

.field public static final duplicate_sku_warning_one_sku_three_item:I = 0x7f1208f0

.field public static final duplicate_sku_warning_one_sku_two_item:I = 0x7f1208f1

.field public static final duplicate_sku_warning_three_skus:I = 0x7f1208f2

.field public static final duplicate_sku_warning_two_skus:I = 0x7f1208f3

.field public static final duplicate_sku_warning_unnamed:I = 0x7f1208f4

.field public static final edit_discount:I = 0x7f1208fe

.field public static final edit_discount_all_items:I = 0x7f1208ff

.field public static final edit_discount_evenly_across_items:I = 0x7f120900

.field public static final edit_discount_item_plural:I = 0x7f120901

.field public static final edit_discount_item_single:I = 0x7f120902

.field public static final edit_discount_item_zero:I = 0x7f120903

.field public static final edit_item_add_item_options_button_label:I = 0x7f120909

.field public static final edit_item_assign_unit_inventory_not_updated_alert_message:I = 0x7f12090c

.field public static final edit_item_assign_unit_inventory_not_updated_alert_title:I = 0x7f12090d

.field public static final edit_item_default_category_button_text:I = 0x7f120910

.field public static final edit_item_description:I = 0x7f120911

.field public static final edit_item_details_dashboard:I = 0x7f120912

.field public static final edit_item_edit_item_options_button_label:I = 0x7f120913

.field public static final edit_item_image_load_failed:I = 0x7f120914

.field public static final edit_item_item_option_help_text:I = 0x7f120915

.field public static final edit_item_item_options_section_header:I = 0x7f120916

.field public static final edit_item_item_options_unsupported_add_options_with_flat_variations_message:I = 0x7f120917

.field public static final edit_item_item_options_unsupported_add_options_with_flat_variations_title:I = 0x7f120918

.field public static final edit_item_item_options_unsupported_add_variation_message:I = 0x7f120919

.field public static final edit_item_item_options_unsupported_add_variation_title:I = 0x7f12091a

.field public static final edit_item_item_options_unsupported_add_variation_with_remote_variations_message:I = 0x7f12091b

.field public static final edit_item_item_options_unsupported_delete_variation_message:I = 0x7f12091c

.field public static final edit_item_item_options_unsupported_delete_variation_title:I = 0x7f12091d

.field public static final edit_item_item_options_unsupported_edit_options_with_remote_variations_message:I = 0x7f12091e

.field public static final edit_item_item_options_unsupported_edit_options_with_remote_variations_title:I = 0x7f12091f

.field public static final edit_item_label:I = 0x7f120920

.field public static final edit_item_modifier_set_dashboard_hint:I = 0x7f120921

.field public static final edit_item_modifier_sets_dashboard_hint:I = 0x7f120922

.field public static final edit_item_name_none:I = 0x7f120924

.field public static final edit_item_name_required_dialog_message:I = 0x7f120925

.field public static final edit_item_name_required_dialog_title:I = 0x7f120926

.field public static final edit_item_price_add:I = 0x7f120928

.field public static final edit_item_price_hint:I = 0x7f120929

.field public static final edit_item_price_message:I = 0x7f12092a

.field public static final edit_item_price_variable:I = 0x7f12092b

.field public static final edit_item_remove_variation:I = 0x7f12092c

.field public static final edit_item_select_category_none:I = 0x7f12092d

.field public static final edit_item_select_category_title:I = 0x7f12092e

.field public static final edit_item_set_label:I = 0x7f12092f

.field public static final edit_item_sku_none:I = 0x7f120930

.field public static final edit_item_unit_type:I = 0x7f120931

.field public static final edit_item_unit_type_default:I = 0x7f120932

.field public static final edit_item_unit_type_default_standard_unit_value:I = 0x7f120933

.field public static final edit_item_unit_type_selector_help_text:I = 0x7f120934

.field public static final edit_item_unit_type_value:I = 0x7f120935

.field public static final edit_items_hint:I = 0x7f120936

.field public static final edit_price_point:I = 0x7f12094c

.field public static final edit_price_point_help:I = 0x7f12094d

.field public static final edit_service_add_processing_time:I = 0x7f120951

.field public static final edit_service_after_appointment:I = 0x7f120952

.field public static final edit_service_assigned_employees:I = 0x7f120953

.field public static final edit_service_bookable_by_customers_online:I = 0x7f120954

.field public static final edit_service_call_for_price:I = 0x7f120955

.field public static final edit_service_cancellation_fee:I = 0x7f120956

.field public static final edit_service_display_price:I = 0x7f120957

.field public static final edit_service_display_price_help_label:I = 0x7f120958

.field public static final edit_service_display_price_hint:I = 0x7f120959

.field public static final edit_service_duration:I = 0x7f12095a

.field public static final edit_service_employees_all:I = 0x7f12095b

.field public static final edit_service_employees_none:I = 0x7f12095c

.field public static final edit_service_employees_plural:I = 0x7f12095d

.field public static final edit_service_employees_singular:I = 0x7f12095e

.field public static final edit_service_extra_time:I = 0x7f12095f

.field public static final edit_service_final_duration:I = 0x7f120960

.field public static final edit_service_fixed_price_type:I = 0x7f120961

.field public static final edit_service_initial_duration:I = 0x7f120962

.field public static final edit_service_no_price:I = 0x7f120963

.field public static final edit_service_price_type:I = 0x7f120964

.field public static final edit_service_price_type_helper_text:I = 0x7f120965

.field public static final edit_service_processing_duration:I = 0x7f120966

.field public static final edit_service_select_employees:I = 0x7f120967

.field public static final edit_service_select_price_type:I = 0x7f120968

.field public static final edit_service_starting_at:I = 0x7f120969

.field public static final edit_service_unit_type_default:I = 0x7f12096a

.field public static final edit_service_variable_price:I = 0x7f12096b

.field public static final edit_service_variable_price_type:I = 0x7f12096c

.field public static final edit_variation:I = 0x7f12097c

.field public static final empty_discount_standalone_percent_character:I = 0x7f120a3c

.field public static final extra_time_helper_text:I = 0x7f120aa3

.field public static final inventory_receive_stock:I = 0x7f120c6c

.field public static final inventory_stock_count:I = 0x7f120c6d

.field public static final inventory_stock_count_reload:I = 0x7f120c6e

.field public static final inventory_view_stock:I = 0x7f120c71

.field public static final item_editing_confirm_price_update_dialog_title:I = 0x7f120e00

.field public static final item_editing_delete_from_location_discount:I = 0x7f120e01

.field public static final item_editing_read_only_category:I = 0x7f120e0b

.field public static final item_editing_read_only_description:I = 0x7f120e0c

.field public static final item_editing_read_only_name:I = 0x7f120e10

.field public static final item_editing_save_failed_message:I = 0x7f120e11

.field public static final item_editing_save_failed_title:I = 0x7f120e12

.field public static final item_editing_save_will_affect_other_locations_item:I = 0x7f120e13

.field public static final item_editing_save_will_affect_other_locations_one_other_item:I = 0x7f120e14

.field public static final item_editing_select_location_update_price_dialog_title:I = 0x7f120e16

.field public static final item_editing_select_location_update_price_item:I = 0x7f120e17

.field public static final item_editing_select_location_update_price_one_other_item:I = 0x7f120e18

.field public static final item_editing_too_many_variations_dialog_message:I = 0x7f120e19

.field public static final item_editing_too_many_variations_dialog_title:I = 0x7f120e1a

.field public static final item_editing_update_all_locations:I = 0x7f120e1b

.field public static final item_editing_update_this_location_only:I = 0x7f120e1c

.field public static final merchant_profile_saving_image_failed:I = 0x7f120fc0

.field public static final new_discount:I = 0x7f121066

.field public static final new_service:I = 0x7f12106e

.field public static final share:I = 0x7f1217dd

.field public static final sku_error_message:I = 0x7f12181e

.field public static final sku_error_message_batched:I = 0x7f12181f

.field public static final sku_error_message_local:I = 0x7f121820

.field public static final stock:I = 0x7f1218cb

.field public static final tap_to_edit:I = 0x7f121919

.field public static final tap_to_set_color:I = 0x7f12191c

.field public static final tax_basis_help_text:I = 0x7f12192d

.field public static final tax_included_format:I = 0x7f12193f

.field public static final tax_included_in_help_text:I = 0x7f121940

.field public static final tax_included_in_multiple_prices:I = 0x7f121941

.field public static final tax_included_in_one_variable_price:I = 0x7f121942

.field public static final unit_selection_create_unit_button_text:I = 0x7f121aec

.field public static final unit_selection_help_text:I = 0x7f121aed

.field public static final unit_type_hint_url:I = 0x7f121aee

.field public static final uppercase_checkout_link:I = 0x7f121b19

.field public static final uppercase_price:I = 0x7f121b66

.field public static final uppercase_price_and_duration:I = 0x7f121b67

.field public static final uppercase_price_and_inventory:I = 0x7f121b68

.field public static final uppercase_variations:I = 0x7f121b8e


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
