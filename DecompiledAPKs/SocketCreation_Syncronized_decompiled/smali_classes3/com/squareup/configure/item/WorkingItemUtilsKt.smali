.class public final Lcom/squareup/configure/item/WorkingItemUtilsKt;
.super Ljava/lang/Object;
.source "WorkingItemUtils.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "unitAbbreviation",
        "",
        "Lcom/squareup/configure/item/WorkingItem;",
        "getUnitAbbreviation",
        "(Lcom/squareup/configure/item/WorkingItem;)Ljava/lang/String;",
        "configure-item_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getUnitAbbreviation(Lcom/squareup/configure/item/WorkingItem;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$unitAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    iget-object p0, p0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    return-object p0
.end method
