.class public final Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ConfigureItemCompScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final compDiscountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final navigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->compDiscountsCacheProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/util/Res;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;
    .locals 8

    .line 61
    new-instance v7, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/util/Res;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/permissions/EmployeeManagement;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemNavigator;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->compDiscountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/EmployeeManagement;

    invoke-static/range {v1 .. v6}, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/util/Res;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemCompScreen_Presenter_Factory;->get()Lcom/squareup/configure/item/ConfigureItemCompScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
