.class public final Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;
.super Ljava/lang/Object;
.source "ConfigureItemViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConfigureItemViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConfigureItemViewFactory.kt\ncom/squareup/configure/item/ConfigureItemViewFactoryKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,677:1\n1651#2,3:678\n950#2:683\n1651#2,3:684\n1651#2,3:687\n1651#2,3:690\n1642#2,2:693\n1642#2,2:695\n151#3,2:681\n*E\n*S KotlinDebug\n*F\n+ 1 ConfigureItemViewFactory.kt\ncom/squareup/configure/item/ConfigureItemViewFactoryKt\n*L\n89#1,3:678\n238#1:683\n251#1,3:684\n320#1,3:687\n360#1,3:690\n450#1,2:693\n548#1,2:695\n192#1,2:681\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f2\u0001\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\u001a\u001e\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u001ap\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u00102\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00140\u001bH\u0002\u001a\u001e\u0010\u001d\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u001e\u001a\u00020\u0010\u001a4\u0010\u001f\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00102\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u0017\u001a\u00020\u0013\u001aH\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020%0$2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0!2\u0006\u0010(\u001a\u00020)2\u0006\u0010\u0017\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u000e\u001a\u0016\u0010*\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008\u001a\u001e\u0010+\u001a\u00020,2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010-\u001a\u00020.\u001a\u0016\u0010/\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008\u001a\u001e\u00100\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u00101\u001a\u00020\u0016\u001av\u00102\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00132\u0012\u00106\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u0002070\u001b2\u001e\u00108\u001a\u001a\u0012\u0004\u0012\u00020\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00160$0\u001b2\u0012\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020:0$\u001af\u0010;\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010<\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u00102\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00140\u001b\u001a4\u0010=\u001a\u00020>2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00102\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u0006\u0010\u0017\u001a\u00020\u0013\u001a\u001e\u0010?\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010@\u001a\u00020A\u001a\u0016\u0010B\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008\u001a^\u0010C\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010<\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00132\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u00140\u001b\u001a^\u0010D\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020%0$2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0!2\u0006\u0010E\u001a\u00020)2\u0006\u0010\u0017\u001a\u00020\u00132\u0006\u0010F\u001a\u00020G2\u0008\u0010H\u001a\u0004\u0018\u00010\u00102\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002\u001aJ\u0010I\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020%0$2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0!2\u0006\u0010E\u001a\u00020)2\u0006\u0010\u0017\u001a\u00020\u00132\u0008\u0010H\u001a\u0004\u0018\u00010\u0010\u001a&\u0010J\u001a\u00020K2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010@\u001a\u00020L2\u0006\u0010<\u001a\u00020\u0010\u001a&\u0010M\u001a\u00020,2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010N\u001a\u00020\u00102\u0006\u0010-\u001a\u00020.\u001a0\u0010O\u001a\u00020K2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010P\u001a\u0004\u0018\u00010\u00102\u0006\u0010@\u001a\u00020L2\u0006\u0010Q\u001a\u00020R\u001a<\u0010S\u001a\u00020\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u000c\u0010T\u001a\u0008\u0012\u0004\u0012\u00020U0!2\u0006\u0010\u0017\u001a\u00020\u0013\u001a(\u0010V\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010W\u001a\u00020\u0013H\u0002\u001a$\u0010X\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010Y\u001a\u0008\u0012\u0004\u0012\u00020\'0!2\u0006\u0010Z\u001a\u00020\u0013\u001a\u0014\u0010[\u001a\u00020\u0001*\u00020\\2\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\u001a\u001c\u0010]\u001a\u00020\u0001*\u00020\u001c2\u0006\u0010^\u001a\u00020\"2\u0006\u0010_\u001a\u00020\u0013H\u0002\u001a\u001c\u0010]\u001a\u00020\u0001*\u00020`2\u0006\u0010^\u001a\u00020\"2\u0006\u0010_\u001a\u00020\u0013H\u0002\u001a\u001c\u0010a\u001a\u00020\u0001*\u00020\u001c2\u0006\u0010b\u001a\u00020c2\u0006\u0010d\u001a\u00020\u0013H\u0002\u001a4\u0010e\u001a\u00020\u0001*\u00020\u001c2\u0006\u0010f\u001a\u00020\u00142\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010d\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\nH\u0002\u001a$\u0010g\u001a\u00020\u0001*\u00020\u001c2\u0006\u0010h\u001a\u00020U2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010d\u001a\u00020\u0013H\u0002\u001a&\u0010i\u001a\n j*\u0004\u0018\u00010\u00100\u0010*\u00020\'2\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002\u001a \u0010k\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00130l*\u00020\\2\u0006\u0010m\u001a\u00020\nH\u0002\u00a8\u0006n"
    }
    d2 = {
        "alterTopPadding",
        "",
        "header",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "createButtonsContainer",
        "parent",
        "Landroid/view/ViewGroup;",
        "smallTopMargin",
        "",
        "createCheckableGroup",
        "Lcom/squareup/widgets/CheckableGroup;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "groupTitle",
        "",
        "modifiers",
        "Ljava/util/SortedMap;",
        "",
        "Lcom/squareup/checkout/OrderModifier;",
        "unitAbbreviation",
        "",
        "groupId",
        "singleChoice",
        "selectMode",
        "modifierViews",
        "",
        "Lcom/squareup/configure/item/ConfigureItemCheckableRow;",
        "createConfigurationRow",
        "configurationText",
        "createDiningOptionsGroup",
        "diningOptions",
        "",
        "Lcom/squareup/checkout/DiningOption;",
        "createDiscountSwitchGroup",
        "",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "adjustmentPairs",
        "Lcom/squareup/configure/item/AdjustmentPair;",
        "discountRowListener",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
        "createDurationRow",
        "createFixedPriceOverrideSection",
        "Landroid/widget/Button;",
        "clickListener",
        "Landroid/view/View$OnClickListener;",
        "createGapTimeRows",
        "createItemDescriptionSection",
        "itemDescription",
        "createItemOptionsGroup",
        "itemOption",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "itemOptionTitle",
        "valueIdToSelectables",
        "Lcom/squareup/configure/item/ItemOptionsValueRowIndex;",
        "groupTagToIndexToValueId",
        "initialState",
        "Lcom/squareup/shared/catalog/engines/ItemOptionValueState;",
        "createMultiChoiceGroup",
        "title",
        "createNohoDiningOptionsGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "createNotesRow",
        "textWatcher",
        "Landroid/text/TextWatcher;",
        "createQuantitySection",
        "createSingleChoiceGroup",
        "createSwitchGroup",
        "taxRowListener",
        "type",
        "Lcom/squareup/configure/item/SwitchGroupType;",
        "helpText",
        "createTaxSwitchGroup",
        "createTitle",
        "Lcom/squareup/widgets/OnScreenRectangleEditText;",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "createVariablePriceOnlySection",
        "buttonText",
        "createVariablePriceOnlySectionCustomItemVariation",
        "text",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "createVariationsGroup",
        "variations",
        "Lcom/squareup/checkout/OrderVariation;",
        "maybeAlterTopPadding",
        "childrenJustAdded",
        "updateTaxesSwitchGroup",
        "feePairs",
        "taxGroupId",
        "addDividers",
        "Lcom/squareup/widgets/ColumnLayout;",
        "bindDiningOption",
        "diningOption",
        "ordinal",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "bindItemOptionValue",
        "itemOptionValue",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
        "rowId",
        "bindModifier",
        "modifier",
        "bindVariation",
        "variation",
        "formatValue",
        "kotlin.jvm.PlatformType",
        "leftRightPaddingForSwitchRow",
        "Lkotlin/Pair;",
        "leftColumn",
        "configure-item_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final addDividers(Lcom/squareup/widgets/ColumnLayout;Landroid/content/Context;)V
    .locals 2

    .line 502
    invoke-virtual {p0}, Lcom/squareup/widgets/ColumnLayout;->isTwoColumn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$drawable;->noho_divider_column_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 505
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/noho/R$drawable;->noho_divider_column_right:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 503
    invoke-virtual {p0, v0, p1}, Lcom/squareup/widgets/ColumnLayout;->setColumnDividers(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 508
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$drawable;->noho_divider_gutter_half:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ColumnLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method private static final alterTopPadding(Landroid/view/View;Landroid/content/Context;)V
    .locals 3

    .line 666
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 667
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_section_header_medium:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 668
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    .line 669
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    .line 665
    invoke-virtual {p0, v0, p1, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private static final bindDiningOption(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/checkout/DiningOption;I)V
    .locals 0

    .line 338
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setId(I)V

    .line 339
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setRowId(I)V

    .line 340
    invoke-virtual {p1}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "diningOption.name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setLabel(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 341
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setSingleSelect(Z)V

    return-void
.end method

.method private static final bindDiningOption(Lcom/squareup/noho/NohoCheckableRow;Lcom/squareup/checkout/DiningOption;I)V
    .locals 0

    .line 375
    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setId(I)V

    .line 376
    invoke-virtual {p1}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private static final bindItemOptionValue(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;I)V
    .locals 0

    .line 298
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setId(I)V

    .line 299
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setRowId(I)V

    .line 300
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "itemOptionValue.name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setLabel(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 301
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setSingleSelect(Z)V

    return-void
.end method

.method private static final bindModifier(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/checkout/OrderModifier;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;IZ)V
    .locals 1

    .line 215
    invoke-virtual {p0, p4}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setRowId(I)V

    .line 216
    invoke-virtual {p0, p4}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setId(I)V

    .line 217
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifier;->isFreeModifier()Z

    move-result p4

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 220
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object p4

    invoke-virtual {p2, p4, p3}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 221
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 223
    :goto_0
    new-instance p3, Lcom/squareup/util/Res$RealRes;

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const-string v0, "resources"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p4}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    check-cast p3, Lcom/squareup/util/Res;

    invoke-virtual {p1, p3}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setLabel(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setValue(Ljava/lang/String;)V

    .line 225
    invoke-virtual {p0, p5}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setSingleSelect(Z)V

    return-void
.end method

.method private static final bindVariation(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/checkout/OrderVariation;Lcom/squareup/quantity/PerUnitFormatter;I)V
    .locals 2

    .line 112
    invoke-virtual {p0, p3}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setId(I)V

    .line 113
    invoke-virtual {p0, p3}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setRowId(I)V

    .line 114
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p3

    const-string/jumbo v0, "variation.unitAbbreviation"

    if-eqz p3, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/configure/item/R$string;->item_library_variable_price:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "resources.getString(R.st\u2026m_library_variable_price)"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p3}, Lcom/squareup/quantity/ItemQuantities;->formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 118
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p3, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 119
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 121
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo p3, "variation.displayName"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setLabel(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setValue(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setSingleSelect(Z)V

    return-void
.end method

.method public static final createButtonsContainer(Landroid/content/Context;Landroid/view/ViewGroup;Z)V
    .locals 3

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 639
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$layout;->configure_item_buttons:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 640
    sget v0, Lcom/squareup/configure/item/R$id;->buttons_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p2, :cond_1

    const-string/jumbo p2, "view"

    .line 643
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 644
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 645
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    iput p0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_0

    .line 643
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    :goto_0
    return-void
.end method

.method private static final createCheckableGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;IZLjava/lang/CharSequence;Ljava/util/Map;)Lcom/squareup/widgets/CheckableGroup;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Map<",
            "Lcom/squareup/configure/item/ConfigureItemCheckableRow;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;)",
            "Lcom/squareup/widgets/CheckableGroup;"
        }
    .end annotation

    move-object v0, p1

    .line 182
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/squareup/configure/item/R$layout;->configure_item_checkablegroup_section:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 183
    sget v2, Lcom/squareup/configure/item/R$id;->section_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 184
    check-cast v2, Lcom/squareup/widgets/PreservedLabelView;

    .line 185
    invoke-static {}, Lcom/squareup/util/Views;->generateViewId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/PreservedLabelView;->setId(I)V

    move-object/from16 v3, p3

    .line 186
    invoke-virtual {v2, v3}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    move-object/from16 v3, p8

    .line 187
    invoke-virtual {v2, v3}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    const-string/jumbo v3, "this"

    .line 188
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/view/View;

    const/4 v4, 0x2

    move-object v5, p0

    invoke-static {v2, p1, p0, v4}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->maybeAlterTopPadding(Landroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;I)V

    .line 191
    sget v0, Lcom/squareup/configure/item/R$id;->section_checkablegroup:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 192
    move-object v1, v0

    check-cast v1, Lcom/squareup/widgets/CheckableGroup;

    move/from16 v2, p6

    .line 193
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/CheckableGroup;->setId(I)V

    move/from16 v2, p7

    .line 194
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/CheckableGroup;->setSingleChoiceMode(Z)V

    .line 195
    move-object/from16 v4, p4

    check-cast v4, Ljava/util/Map;

    .line 681
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lcom/squareup/checkout/OrderModifier;

    .line 196
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 197
    sget v6, Lcom/squareup/configure/item/R$layout;->configure_item_checkable_row:I

    move-object v7, v1

    check-cast v7, Landroid/view/ViewGroup;

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 198
    sget v6, Lcom/squareup/configure/item/R$id;->configure_item_checkable_row:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 199
    move-object v12, v4

    check-cast v12, Lcom/squareup/configure/item/ConfigureItemCheckableRow;

    const-string v4, "modifier"

    .line 200
    invoke-static {v11, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "ordinal"

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object v4, v12

    move-object v5, v11

    move-object v6, p2

    move-object/from16 v7, p5

    move/from16 v9, p7

    invoke-static/range {v4 .. v9}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->bindModifier(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/checkout/OrderModifier;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;IZ)V

    .line 202
    move-object v4, v12

    check-cast v4, Landroid/view/View;

    new-instance v5, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(II)V

    check-cast v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v4, v5}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const-string v4, "labelView"

    .line 203
    invoke-static {v12, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v4, p9

    invoke-interface {v4, v12, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v2, "section.findViewById<Che\u2026odifier\n        }\n      }"

    .line 192
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method public static final createConfigurationRow(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configurationText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 59
    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 61
    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_multiline_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 63
    new-instance v2, Lcom/squareup/widgets/MessageView;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/squareup/widgets/MessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    invoke-virtual {v2, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    const/16 p0, 0x11

    .line 65
    invoke-virtual {v2, p0}, Lcom/squareup/widgets/MessageView;->setGravity(I)V

    .line 66
    invoke-virtual {v2, v1, v1, v1, v0}, Lcom/squareup/widgets/MessageView;->setPadding(IIII)V

    .line 68
    check-cast v2, Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public static final createDiningOptionsGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Ljava/util/List;I)Lcom/squareup/widgets/CheckableGroup;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/DiningOption;",
            ">;I)",
            "Lcom/squareup/widgets/CheckableGroup;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupTitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diningOptions"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$layout;->configure_item_checkablegroup_section:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 312
    sget v0, Lcom/squareup/configure/item/R$id;->section_header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 313
    check-cast v0, Lcom/squareup/widgets/PreservedLabelView;

    .line 314
    invoke-static {}, Lcom/squareup/util/Views;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setId(I)V

    .line 315
    invoke-virtual {v0, p2}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    const-string/jumbo p2, "this"

    .line 316
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->alterTopPadding(Landroid/view/View;Landroid/content/Context;)V

    .line 319
    sget p0, Lcom/squareup/configure/item/R$id;->section_checkablegroup:I

    invoke-virtual {p1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 320
    move-object p1, p0

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    .line 321
    invoke-virtual {p1, p4}, Lcom/squareup/widgets/CheckableGroup;->setId(I)V

    .line 322
    check-cast p3, Ljava/lang/Iterable;

    .line 688
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    const/4 p4, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    if-gez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v1, Lcom/squareup/checkout/DiningOption;

    .line 323
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/widgets/CheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 324
    sget v4, Lcom/squareup/configure/item/R$layout;->configure_item_checkable_row:I

    move-object v5, p1

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v3, v4, v5, p4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 325
    sget v4, Lcom/squareup/configure/item/R$id;->configure_item_checkable_row:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 326
    check-cast v3, Lcom/squareup/configure/item/ConfigureItemCheckableRow;

    .line 327
    invoke-static {v3, v1, v0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->bindDiningOption(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/checkout/DiningOption;I)V

    .line 329
    check-cast v3, Landroid/view/View;

    new-instance v0, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v1, v4}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(II)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v3, v0}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v2

    goto :goto_0

    :cond_1
    const-string p2, "section.findViewById<Che\u2026NTENT))\n        }\n      }"

    .line 320
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final createDiscountSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILcom/squareup/quantity/PerUnitFormatter;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;",
            "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
            "I",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/widgets/list/ToggleButtonRow;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adjustmentPairs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountRowListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 424
    sget-object v6, Lcom/squareup/configure/item/SwitchGroupType;->DISCOUNT:Lcom/squareup/configure/item/SwitchGroupType;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v8, p5

    .line 423
    invoke-static/range {v1 .. v8}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILcom/squareup/configure/item/SwitchGroupType;Ljava/lang/CharSequence;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final createDurationRow(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_duration_section:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    const-string p1, "from(context).inflate(R.\u2026on_section, parent, true)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final createFixedPriceOverrideSection(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)Landroid/widget/Button;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickListener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 615
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_fixed_price_override_section:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 616
    sget p1, Lcom/squareup/configure/item/R$id;->fixed_price_override_button:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 617
    move-object p1, p0

    check-cast p1, Landroid/widget/Button;

    .line 618
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string p2, "section.findViewById<But\u2026er(clickListener)\n      }"

    .line 617
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final createGapTimeRows(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 404
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_gap_time_section:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    const-string p1, "from(context).inflate(R.\u2026me_section, parent, true)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final createItemDescriptionSection(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemDescription"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 627
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_description:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    const-string p1, "section"

    .line 628
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p1, Lcom/squareup/configure/item/R$id;->configure_item_description:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    .line 629
    check-cast p0, Landroid/widget/TextView;

    .line 630
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final createItemOptionsGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;Ljava/lang/String;ILjava/util/Map;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/widgets/CheckableGroup;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/configure/item/ItemOptionsValueRowIndex;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/shared/catalog/engines/ItemOptionValueState;",
            ">;)",
            "Lcom/squareup/widgets/CheckableGroup;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    const-string v6, "context"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "parent"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "itemOption"

    move-object/from16 v7, p2

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "itemOptionTitle"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v6, "valueIdToSelectables"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "groupTagToIndexToValueId"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "initialState"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v6

    const-string v8, "itemOption.allValues"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Ljava/lang/Iterable;

    .line 683
    new-instance v8, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createItemOptionsGroup$$inlined$sortedBy$1;

    invoke-direct {v8}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createItemOptionsGroup$$inlined$sortedBy$1;-><init>()V

    check-cast v8, Ljava/util/Comparator;

    invoke-static {v6, v8}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v6

    .line 239
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    sget v9, Lcom/squareup/configure/item/R$layout;->configure_item_checkablegroup_section:I

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 240
    sget v8, Lcom/squareup/configure/item/R$id;->section_header:I

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 241
    check-cast v8, Lcom/squareup/widgets/PreservedLabelView;

    .line 242
    invoke-static {}, Lcom/squareup/util/Views;->generateViewId()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/squareup/widgets/PreservedLabelView;->setId(I)V

    .line 243
    sget v9, Lcom/squareup/configure/item/R$string;->configure_item_option_section_header:I

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    invoke-static {v9}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v9

    .line 244
    check-cast v2, Ljava/lang/CharSequence;

    const-string v11, "item_option_set_name"

    invoke-virtual {v9, v11, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 245
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 246
    invoke-virtual {v8}, Lcom/squareup/widgets/PreservedLabelView;->hidePreservedLabel()V

    const-string/jumbo v2, "this"

    .line 247
    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Landroid/view/View;

    invoke-static {v8, v0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->alterTopPadding(Landroid/view/View;Landroid/content/Context;)V

    .line 250
    sget v0, Lcom/squareup/configure/item/R$id;->section_checkablegroup:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 251
    move-object v1, v0

    check-cast v1, Lcom/squareup/widgets/CheckableGroup;

    move/from16 v8, p4

    .line 252
    invoke-virtual {v1, v8}, Lcom/squareup/widgets/CheckableGroup;->setId(I)V

    const/4 v8, 0x0

    .line 254
    invoke-virtual {v1, v8}, Lcom/squareup/widgets/CheckableGroup;->setSaveFromParentEnabled(Z)V

    .line 256
    invoke-virtual {v1, v8}, Lcom/squareup/widgets/CheckableGroup;->setSingleChoiceMode(Z)V

    .line 258
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v9, Ljava/util/Map;

    .line 259
    check-cast v6, Ljava/lang/Iterable;

    .line 685
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v11, 0x0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    const-string v13, "itemOption.id"

    if-eqz v12, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    add-int/lit8 v14, v11, 0x1

    if-gez v11, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v12, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const-string v15, "itemOptionValue"

    .line 260
    invoke-static {v12, v15}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v5, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    .line 261
    sget-object v10, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->HIDDEN:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    if-ne v15, v10, :cond_1

    move-object/from16 p1, v2

    move-object/from16 p3, v6

    const/4 v5, 0x1

    const/4 v6, 0x0

    goto :goto_3

    .line 264
    :cond_1
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    move-object/from16 p1, v2

    .line 265
    sget v2, Lcom/squareup/configure/item/R$layout;->configure_item_checkable_row:I

    move-object v5, v1

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v10, v2, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 266
    sget v5, Lcom/squareup/configure/item/R$id;->configure_item_checkable_row:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 267
    check-cast v2, Lcom/squareup/configure/item/ConfigureItemCheckableRow;

    .line 269
    invoke-static {v2, v12, v11}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->bindItemOptionValue(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;I)V

    .line 271
    invoke-virtual {v12}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v5

    const-string v10, "itemOptionValue.uid"

    invoke-static {v5, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Lcom/squareup/configure/item/ItemOptionsValueRowIndex;

    move-object/from16 p3, v6

    .line 273
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "labelView"

    .line 274
    invoke-static {v2, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    invoke-direct {v8, v11, v6, v2}, Lcom/squareup/configure/item/ItemOptionsValueRowIndex;-><init>(ILjava/lang/String;Lcom/squareup/configure/item/ConfigureItemCheckableRow;)V

    invoke-interface {v3, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v12}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v9, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v5, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    if-ne v15, v5, :cond_2

    const/4 v5, 0x1

    .line 278
    invoke-virtual {v2, v5}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setChecked(Z)V

    goto :goto_1

    :cond_2
    const/4 v5, 0x1

    .line 279
    sget-object v6, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->UNSELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    if-ne v15, v6, :cond_3

    const/4 v6, 0x0

    .line 280
    invoke-virtual {v2, v6}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setEnabled(Z)V

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v6, 0x0

    .line 282
    :goto_2
    check-cast v2, Landroid/view/View;

    new-instance v8, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v8, v10, v11}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(II)V

    check-cast v8, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2, v8}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_3
    move-object/from16 v2, p1

    move-object/from16 v6, p3

    move-object/from16 v5, p7

    move v11, v14

    const/4 v8, 0x0

    const/4 v10, 0x1

    goto/16 :goto_0

    .line 284
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "section.findViewById<Che\u2026] = valueIndexMap\n      }"

    .line 251
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method public static final createMultiChoiceGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/util/Map;)Lcom/squareup/widgets/CheckableGroup;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/CharSequence;",
            "Ljava/util/Map<",
            "Lcom/squareup/configure/item/ConfigureItemCheckableRow;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;)",
            "Lcom/squareup/widgets/CheckableGroup;"
        }
    .end annotation

    const-string v0, "context"

    move-object v1, p0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifiers"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitAbbreviation"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMode"

    move-object/from16 v9, p7

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifierViews"

    move-object/from16 v10, p8

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    move/from16 v7, p6

    .line 157
    invoke-static/range {v1 .. v10}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createCheckableGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;IZLjava/lang/CharSequence;Ljava/util/Map;)Lcom/squareup/widgets/CheckableGroup;

    move-result-object v0

    return-object v0
.end method

.method public static final createNohoDiningOptionsGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Ljava/util/List;I)Lcom/squareup/noho/NohoCheckableGroup;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/DiningOption;",
            ">;I)",
            "Lcom/squareup/noho/NohoCheckableGroup;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupTitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "diningOptions"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$layout;->configure_item_checkablegroup_noho:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 352
    sget v0, Lcom/squareup/configure/item/R$id;->section_header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 353
    check-cast v0, Lcom/squareup/widgets/PreservedLabelView;

    .line 354
    invoke-static {}, Lcom/squareup/util/Views;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setId(I)V

    .line 355
    invoke-virtual {v0, p2}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    const-string/jumbo p2, "this"

    .line 356
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->alterTopPadding(Landroid/view/View;Landroid/content/Context;)V

    .line 359
    sget p2, Lcom/squareup/configure/item/R$id;->section_checkablegroup:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 360
    move-object p2, p1

    check-cast p2, Lcom/squareup/noho/NohoCheckableGroup;

    .line 361
    invoke-virtual {p2, p4}, Lcom/squareup/noho/NohoCheckableGroup;->setId(I)V

    .line 362
    check-cast p3, Ljava/lang/Iterable;

    .line 691
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    const/4 p4, 0x0

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v1, p4, 0x1

    if-gez p4, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v0, Lcom/squareup/checkout/DiningOption;

    .line 363
    new-instance v8, Lcom/squareup/noho/NohoCheckableRow;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v2, v8

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 364
    invoke-static {v8, v0, p4}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->bindDiningOption(Lcom/squareup/noho/NohoCheckableRow;Lcom/squareup/checkout/DiningOption;I)V

    .line 366
    check-cast v8, Landroid/view/View;

    new-instance p4, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    const/4 v0, -0x1

    const/4 v2, -0x2

    invoke-direct {p4, v0, v2}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(II)V

    check-cast p4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, v8, p4}, Lcom/squareup/noho/NohoCheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move p4, v1

    goto :goto_0

    :cond_1
    const-string p0, "section.findViewById<Noh\u2026NTENT))\n        }\n      }"

    .line 360
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p2
.end method

.method public static final createNotesRow(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/text/TextWatcher;)Landroid/view/View;
    .locals 3

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "textWatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 390
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$layout;->configure_item_notes_section:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 391
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_note:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketEditText;

    .line 392
    invoke-virtual {v1, p2}, Lcom/squareup/marketfont/MarketEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 393
    sget p2, Lcom/squareup/configure/item/R$id;->notes_section_header:I

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "findViewById(R.id.notes_section_header)"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-static {p2, p1, p0, v1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->maybeAlterTopPadding(Landroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;I)V

    const-string p0, "from(context).inflate(R.\u2026r), parent, context, 2)\n}"

    .line 390
    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final createQuantitySection(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 382
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$layout;->configure_item_quantity_section:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 383
    sget v1, Lcom/squareup/configure/item/R$id;->quantity_section_header:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.quantity_section_header)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {v1, p1, p0, v2}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->maybeAlterTopPadding(Landroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;I)V

    const-string p0, "from(context).inflate(R.\u2026r), parent, context, 2)\n}"

    .line 382
    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final createSingleChoiceGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;ILjava/util/Map;)Lcom/squareup/widgets/CheckableGroup;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map<",
            "Lcom/squareup/configure/item/ConfigureItemCheckableRow;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;)",
            "Lcom/squareup/widgets/CheckableGroup;"
        }
    .end annotation

    move-object v0, p0

    const-string v1, "context"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "parent"

    move-object v2, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "perUnitFormatter"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "title"

    move-object v4, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "modifiers"

    move-object v5, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "unitAbbreviation"

    move-object v6, p5

    invoke-static {p5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "modifierViews"

    move-object/from16 v9, p7

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    sget v1, Lcom/squareup/configure/item/R$string;->uppercase_choose_one:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v7, "context.getString(R.string.uppercase_choose_one)"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v1

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v7, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    .line 135
    invoke-static/range {v0 .. v9}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createCheckableGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;IZLjava/lang/CharSequence;Ljava/util/Map;)Lcom/squareup/widgets/CheckableGroup;

    move-result-object v0

    return-object v0
.end method

.method private static final createSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILcom/squareup/configure/item/SwitchGroupType;Ljava/lang/CharSequence;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;",
            "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
            "I",
            "Lcom/squareup/configure/item/SwitchGroupType;",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/widgets/list/ToggleButtonRow;",
            ">;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 442
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual/range {p5 .. p5}, Lcom/squareup/configure/item/SwitchGroupType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 444
    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_discounts:I

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 443
    :cond_1
    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_taxes:I

    .line 446
    :goto_0
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    move-object/from16 v3, p1

    invoke-virtual {v2, v0, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 448
    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V

    .line 449
    sget v0, Lcom/squareup/configure/item/R$id;->section_container:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 450
    move-object v11, v0

    check-cast v11, Lcom/squareup/widgets/ColumnLayout;

    move/from16 v0, p4

    .line 451
    invoke-virtual {v11, v0}, Lcom/squareup/widgets/ColumnLayout;->setId(I)V

    .line 453
    invoke-static {v11, v6}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->addDividers(Lcom/squareup/widgets/ColumnLayout;Landroid/content/Context;)V

    .line 456
    move-object/from16 v0, p2

    check-cast v0, Ljava/lang/Iterable;

    .line 693
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v13, 0x0

    if-eqz v0, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/configure/item/AdjustmentPair;

    .line 457
    invoke-virtual {v14}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    if-nez v0, :cond_3

    if-eqz v8, :cond_2

    goto :goto_2

    .line 458
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "perUnitFormatter must not be null for amount discounts"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 463
    :cond_3
    :goto_2
    invoke-virtual {v14}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/checkout/Adjustment;->enabled:Z

    if-nez v0, :cond_4

    goto :goto_1

    .line 465
    :cond_4
    invoke-static {v11, v1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->leftRightPaddingForSwitchRow(Lcom/squareup/widgets/ColumnLayout;Z)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 466
    invoke-virtual {v11}, Lcom/squareup/widgets/ColumnLayout;->isTwoColumn()Z

    move-result v0

    if-eqz v0, :cond_5

    xor-int/lit8 v0, v1, 0x1

    move v15, v0

    goto :goto_3

    :cond_5
    move v15, v1

    .line 468
    :goto_3
    invoke-static {v14, v6, v8}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->formatValue(Lcom/squareup/configure/item/AdjustmentPair;Landroid/content/Context;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 472
    invoke-virtual {v14}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/Adjustment;->name:Ljava/lang/String;

    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    .line 476
    sget v5, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    move-object/from16 v0, p0

    .line 470
    invoke-static/range {v0 .. v5}, Lcom/squareup/widgets/list/ToggleButtonRow;->switchRowPreservedLabel(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v0

    .line 481
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 482
    invoke-virtual {v14}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setButtonTag(Ljava/lang/Object;)V

    move-object/from16 v1, p3

    .line 483
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 484
    invoke-virtual {v14}, Lcom/squareup/configure/item/AdjustmentPair;->getApplied()Z

    move-result v2

    invoke-virtual {v0, v2, v13}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    .line 487
    move-object v2, v0

    check-cast v2, Landroid/view/View;

    invoke-virtual {v11, v2}, Lcom/squareup/widgets/ColumnLayout;->addView(Landroid/view/View;)V

    .line 488
    move-object v2, v10

    check-cast v2, Ljava/util/Map;

    invoke-virtual {v14}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    const-string v4, "adjustmentPair.adjustment.id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "view"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v15

    goto/16 :goto_1

    :cond_6
    if-eqz v7, :cond_7

    .line 494
    sget v0, Lcom/squareup/configure/item/R$id;->conditional_taxes_help_text:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    const-string v1, "helpTextRow"

    .line 495
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v13}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 496
    invoke-virtual {v0, v7}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    :cond_7
    check-cast v10, Ljava/util/Map;

    return-object v10
.end method

.method public static final createTaxSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILjava/lang/CharSequence;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;",
            "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
            "I",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/widgets/list/ToggleButtonRow;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adjustmentPairs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxRowListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 414
    sget-object v6, Lcom/squareup/configure/item/SwitchGroupType;->TAX:Lcom/squareup/configure/item/SwitchGroupType;

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v7, p5

    invoke-static/range {v1 .. v8}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILcom/squareup/configure/item/SwitchGroupType;Ljava/lang/CharSequence;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final createTitle(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/text/EmptyTextWatcher;Ljava/lang/CharSequence;)Lcom/squareup/widgets/OnScreenRectangleEditText;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "textWatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 562
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_title_section:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 563
    sget p1, Lcom/squareup/configure/item/R$id;->title_text:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 564
    move-object p1, p0

    check-cast p1, Lcom/squareup/widgets/OnScreenRectangleEditText;

    .line 565
    invoke-virtual {p1, p3}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    .line 566
    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/OnScreenRectangleEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const-string p2, "section.findViewById<OnS\u2026ener(textWatcher)\n      }"

    .line 564
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final createVariablePriceOnlySection(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/widget/Button;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 577
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_variable_price_section:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 578
    sget p1, Lcom/squareup/configure/item/R$id;->price_button:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 579
    move-object p1, p0

    check-cast p1, Landroid/widget/Button;

    .line 580
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 581
    invoke-virtual {p1, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string p2, "section.findViewById<But\u2026er(clickListener)\n      }"

    .line 579
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final createVariablePriceOnlySectionCustomItemVariation(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Lcom/squareup/text/EmptyTextWatcher;Lcom/squareup/money/PriceLocaleHelper;)Lcom/squareup/widgets/OnScreenRectangleEditText;
    .locals 2

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "textWatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 592
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    .line 593
    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_variable_price_item_variation_section:I

    const/4 v1, 0x1

    .line 592
    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 598
    sget p1, Lcom/squareup/configure/item/R$id;->price_edit_text:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 599
    move-object p1, p0

    check-cast p1, Lcom/squareup/widgets/OnScreenRectangleEditText;

    .line 600
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    .line 601
    move-object v0, p3

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 602
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;-><init>(Lcom/squareup/widgets/OnScreenRectangleEditText;Ljava/lang/CharSequence;Lcom/squareup/text/EmptyTextWatcher;Lcom/squareup/money/PriceLocaleHelper;)V

    check-cast v0, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 605
    move-object p2, p1

    check-cast p2, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {p4, p2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    const-string p2, "section.findViewById<OnS\u2026r.configure(this)\n      }"

    .line 599
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static final createVariationsGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/List;I)Lcom/squareup/widgets/CheckableGroup;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;I)",
            "Lcom/squareup/widgets/CheckableGroup;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupTitle"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "variations"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$layout;->configure_item_checkablegroup_section:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 80
    sget v0, Lcom/squareup/configure/item/R$id;->section_header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 81
    check-cast v0, Lcom/squareup/widgets/PreservedLabelView;

    .line 82
    invoke-static {}, Lcom/squareup/util/Views;->generateViewId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setId(I)V

    .line 83
    invoke-virtual {v0, p3}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 84
    sget p3, Lcom/squareup/configure/item/R$string;->uppercase_choose_one:I

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v0, p3}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    const-string/jumbo p3, "this"

    .line 85
    invoke-static {v0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->alterTopPadding(Landroid/view/View;Landroid/content/Context;)V

    .line 88
    sget p0, Lcom/squareup/configure/item/R$id;->section_checkablegroup:I

    invoke-virtual {p1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 89
    move-object p1, p0

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    .line 90
    invoke-virtual {p1, p5}, Lcom/squareup/widgets/CheckableGroup;->setId(I)V

    const/4 p5, 0x0

    .line 92
    invoke-virtual {p1, p5}, Lcom/squareup/widgets/CheckableGroup;->setSaveFromParentEnabled(Z)V

    .line 94
    check-cast p4, Ljava/lang/Iterable;

    .line 679
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p4

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    if-gez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v1, Lcom/squareup/checkout/OrderVariation;

    .line 95
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/widgets/CheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 96
    sget v4, Lcom/squareup/configure/item/R$layout;->configure_item_checkable_row:I

    move-object v5, p1

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v3, v4, v5, p5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 97
    sget v4, Lcom/squareup/configure/item/R$id;->configure_item_checkable_row:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 98
    check-cast v3, Lcom/squareup/configure/item/ConfigureItemCheckableRow;

    .line 100
    invoke-static {v3, v1, p2, v0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->bindVariation(Lcom/squareup/configure/item/ConfigureItemCheckableRow;Lcom/squareup/checkout/OrderVariation;Lcom/squareup/quantity/PerUnitFormatter;I)V

    .line 102
    check-cast v3, Landroid/view/View;

    new-instance v0, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v1, v4}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(II)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v3, v0}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v0, v2

    goto :goto_0

    :cond_1
    const-string p2, "section.findViewById<Che\u2026NTENT))\n        }\n      }"

    .line 89
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private static final formatValue(Lcom/squareup/configure/item/AdjustmentPair;Landroid/content/Context;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/CharSequence;
    .locals 1

    .line 534
    invoke-virtual {p0}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    if-eqz v0, :cond_0

    .line 535
    sget p2, Lcom/squareup/configure/item/R$string;->percent:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 536
    invoke-virtual {p0}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/checkout/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    invoke-static {p0}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p2, "content"

    invoke-virtual {p1, p2, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 537
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 539
    invoke-virtual {p0}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/checkout/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    const/4 p1, 0x2

    const/4 v0, 0x0

    invoke-static {p2, p0, v0, p1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Required value was null."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final leftRightPaddingForSwitchRow(Lcom/squareup/widgets/ColumnLayout;Z)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/ColumnLayout;",
            "Z)",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 513
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 514
    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half_lollipop:I

    .line 516
    invoke-virtual {p0}, Lcom/squareup/widgets/ColumnLayout;->isTwoColumn()Z

    move-result p0

    if-eqz p0, :cond_2

    if-eqz p1, :cond_0

    .line 518
    sget p0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    goto :goto_0

    .line 520
    :cond_0
    sget p0, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    :goto_0
    move v0, p0

    if-eqz p1, :cond_1

    .line 523
    sget p0, Lcom/squareup/marin/R$dimen;->marin_gap_medium_lollipop:I

    goto :goto_1

    .line 525
    :cond_1
    sget p0, Lcom/squareup/marin/R$dimen;->marin_gutter_half_lollipop:I

    :goto_1
    move v1, p0

    .line 528
    :cond_2
    new-instance p0, Lkotlin/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0
.end method

.method private static final maybeAlterTopPadding(Landroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;I)V
    .locals 0

    .line 656
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-ne p1, p3, :cond_0

    .line 657
    invoke-static {p0, p2}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->alterTopPadding(Landroid/view/View;Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public static final updateTaxesSwitchGroup(Landroid/view/ViewGroup;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feePairs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 547
    invoke-virtual {p0, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 548
    check-cast p1, Ljava/lang/Iterable;

    .line 695
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/configure/item/AdjustmentPair;

    .line 550
    invoke-virtual {p2}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/checkout/Adjustment;->enabled:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 551
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/configure/item/AdjustmentPair;->getAdjustment()Lcom/squareup/checkout/Adjustment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    const-string v1, "taxRow"

    .line 552
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/configure/item/AdjustmentPair;->getApplied()Z

    move-result p2

    invoke-virtual {v0, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method
