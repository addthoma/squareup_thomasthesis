.class public abstract Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen$AtLeastZeroAmountValidationModule;
.super Ljava/lang/Object;
.source "InvoiceConfigureItemDetailScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AtLeastZeroAmountValidationModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideItemAmountValidator(Lcom/squareup/configure/item/AtLeastZeroAmountValidator;)Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
