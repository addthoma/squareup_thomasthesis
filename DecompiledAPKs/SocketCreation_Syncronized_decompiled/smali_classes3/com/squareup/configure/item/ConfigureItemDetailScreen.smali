.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen;
.super Lcom/squareup/configure/item/InConfigureItemScope;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;,
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$GreaterThanZeroAmountValidationModule;,
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;,
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$Component;,
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u0000 \u001b2\u00020\u00012\u00020\u00022\u00020\u0003:\u0005\u001b\u001c\u001d\u001e\u001fB\u0017\u0008\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B\u0017\u0008\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0007H\u0014J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0008\u0010\u001a\u001a\u00020\u0007H\u0016\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/configure/item/ConfigureItemDetailScreen;",
        "Lcom/squareup/configure/item/InConfigureItemScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/spot/HasSpot;",
        "parentPath",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "indexToEdit",
        "",
        "(Lcom/squareup/ui/main/RegisterTreeKey;I)V",
        "workingItem",
        "Lcom/squareup/configure/item/WorkingItem;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V",
        "parent",
        "Lcom/squareup/configure/item/ConfigureItemScope;",
        "(Lcom/squareup/configure/item/ConfigureItemScope;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "context",
        "Landroid/content/Context;",
        "screenLayout",
        "Companion",
        "Component",
        "GreaterThanZeroAmountValidationModule",
        "ItemAmountValidator",
        "Presenter",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen;->Companion:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion;

    .line 1861
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.fromParcel {\u2026ailScreen(parent)\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;)V
    .locals 0

    .line 135
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 134
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V
    .locals 1

    const-string v0, "parentPath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V
    .locals 1

    const-string v0, "parentPath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-super {p0, p1, p2}, Lcom/squareup/configure/item/InConfigureItemScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 160
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen;->configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 163
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    const-string v0, "Spots.BELOW"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 151
    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_detail_view:I

    return v0
.end method
