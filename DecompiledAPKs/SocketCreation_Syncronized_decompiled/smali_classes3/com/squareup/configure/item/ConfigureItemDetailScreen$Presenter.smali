.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;


# annotations
.annotation runtime Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$SharedScope;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$SharedScope;,
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/configure/item/ConfigureItemDetailView;",
        ">;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConfigureItemDetailScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConfigureItemDetailScreen.kt\ncom/squareup/configure/item/ConfigureItemDetailScreen$Presenter\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n+ 3 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1868:1\n67#2:1869\n92#2,3:1870\n18#3:1873\n1265#4,12:1874\n1550#4,3:1886\n*E\n*S KotlinDebug\n*F\n+ 1 ConfigureItemDetailScreen.kt\ncom/squareup/configure/item/ConfigureItemDetailScreen$Presenter\n*L\n569#1:1869\n569#1,3:1870\n611#1:1873\n1262#1,12:1874\n1263#1,3:1886\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e0\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0018\u0008\u0001\u0018\u0000 \u00eb\u00012\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0004\u00eb\u0001\u00ec\u0001B\u00c5\u0001\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u0012\u0006\u0010!\u001a\u00020\"\u0012\u0006\u0010#\u001a\u00020$\u0012\u0006\u0010%\u001a\u00020&\u0012\u0006\u0010\'\u001a\u00020(\u0012\u0006\u0010)\u001a\u00020*\u0012\u0006\u0010+\u001a\u00020,\u0012\u0006\u0010-\u001a\u00020.\u0012\u0006\u0010/\u001a\u000200\u0012\u0006\u00101\u001a\u000202\u00a2\u0006\u0002\u00103J\u0018\u0010y\u001a\u00020z2\u0006\u0010{\u001a\u00020:2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0010\u0010}\u001a\u00020z2\u0006\u0010{\u001a\u00020:H\u0016J\u0010\u0010~\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0010\u0010\u007f\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u001d\u0010\u0080\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u00022\n\u0010\u0081\u0001\u001a\u0005\u0018\u00010\u0082\u0001H\u0002J\u0011\u0010\u0083\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0011\u0010\u0084\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0011\u0010\u0085\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0011\u0010\u0086\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0011\u0010\u0087\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\t\u0010\u0088\u0001\u001a\u00020zH\u0002J\u0011\u0010\u0089\u0001\u001a\u00020Z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0011\u0010\u008a\u0001\u001a\u00020Z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0011\u0010\u008b\u0001\u001a\u00020Z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\u0013\u0010\u008c\u0001\u001a\u00020z2\u0008\u0010\u008d\u0001\u001a\u00030\u008e\u0001H\u0002J\u001a\u0010\u008f\u0001\u001a\u00020Z2\u0006\u0010|\u001a\u00020\u00022\u0007\u0010\u0090\u0001\u001a\u00020ZH\u0002J\u0012\u0010\u0091\u0001\u001a\u00020z2\u0007\u0010\u0092\u0001\u001a\u00020qH\u0002J\t\u0010\u0093\u0001\u001a\u00020zH\u0002J\t\u0010\u0094\u0001\u001a\u00020zH\u0002J\u000b\u0010\u0095\u0001\u001a\u0004\u0018\u00010:H\u0002J\u001e\u0010\u0096\u0001\u001a\u00030\u0097\u00012\u0008\u0010\u0098\u0001\u001a\u00030\u0099\u00012\u0008\u0010\u009a\u0001\u001a\u00030\u0099\u0001H\u0002J\u0014\u0010\u009b\u0001\u001a\u00030\u008e\u00012\u0008\u0010\u0098\u0001\u001a\u00030\u0099\u0001H\u0002J\"\u0010\u009c\u0001\u001a\u0011\u0012\u0005\u0012\u00030\u0099\u0001\u0012\u0005\u0012\u00030\u0097\u00010\u009d\u00012\u0008\u0010\u0098\u0001\u001a\u00030\u0099\u0001H\u0007J\n\u0010\u009e\u0001\u001a\u00030\u009f\u0001H\u0002J\u0012\u0010\u00a0\u0001\u001a\u00030\u0082\u00012\u0008\u0010\u00a1\u0001\u001a\u00030\u0099\u0001J\u0012\u0010\u00a2\u0001\u001a\u00020Z2\u0007\u0010\u00a3\u0001\u001a\u00020:H\u0002J\u0012\u0010\u00a4\u0001\u001a\u00020Z2\u0007\u0010\u00a5\u0001\u001a\u00020:H\u0002J\t\u0010\u00a6\u0001\u001a\u00020zH\u0002J\t\u0010\u00a7\u0001\u001a\u00020ZH\u0002J\u0012\u0010\u00a8\u0001\u001a\u00020z2\u0007\u0010\u0092\u0001\u001a\u00020qH\u0002J\t\u0010\u00a9\u0001\u001a\u00020zH\u0002J\t\u0010\u00aa\u0001\u001a\u00020zH\u0002J\u0013\u0010\u00ab\u0001\u001a\u00020z2\u0008\u0010\u00ac\u0001\u001a\u00030\u00ad\u0001H\u0002J\u0007\u0010\u00ae\u0001\u001a\u00020zJ\u0007\u0010\u00af\u0001\u001a\u00020zJ\u0007\u0010\u00b0\u0001\u001a\u00020zJ\u0007\u0010\u00b1\u0001\u001a\u00020zJ\u0011\u0010\u00b2\u0001\u001a\u00020z2\u0008\u0010\u00b3\u0001\u001a\u00030\u0099\u0001J\u0019\u0010\u00b4\u0001\u001a\u00020z2\u0007\u0010\u00a3\u0001\u001a\u00020:2\u0007\u0010\u00b5\u0001\u001a\u00020ZJ\u0010\u0010\u00b6\u0001\u001a\u00020z2\u0007\u0010\u00b7\u0001\u001a\u00020:J\u0011\u0010\u00b8\u0001\u001a\u00020z2\u0008\u0010\u00b9\u0001\u001a\u00030\u00ba\u0001J\u0013\u0010\u00bb\u0001\u001a\u00020z2\u0008\u0010\u00bc\u0001\u001a\u00030\u00bd\u0001H\u0014J\t\u0010\u00be\u0001\u001a\u00020zH\u0014J\u0007\u0010\u00bf\u0001\u001a\u00020zJ\u0007\u0010\u00c0\u0001\u001a\u00020zJ\u0007\u0010\u00c1\u0001\u001a\u00020zJ\u0010\u0010\u00c2\u0001\u001a\u00020z2\u0007\u0010\u00c3\u0001\u001a\u00020ZJ\u0007\u0010\u00c4\u0001\u001a\u00020zJ\u0015\u0010\u00c5\u0001\u001a\u00020z2\n\u0010\u00c6\u0001\u001a\u0005\u0018\u00010\u00c7\u0001H\u0014J\u001b\u0010\u00c8\u0001\u001a\u00020z2\u0008\u0010\u0098\u0001\u001a\u00030\u0099\u00012\u0008\u0010\u009a\u0001\u001a\u00030\u0099\u0001J\u0010\u0010\u00c9\u0001\u001a\u00020z2\u0007\u0010\u00ca\u0001\u001a\u00020:J\u0011\u0010\u00cb\u0001\u001a\u00020z2\u0008\u0010\u00cc\u0001\u001a\u00030\u0099\u0001J\u0012\u0010\u00cd\u0001\u001a\u00020z2\u0007\u0010\u0092\u0001\u001a\u00020qH\u0007J\u0011\u0010\u00ce\u0001\u001a\u00020Z2\u0008\u0010\u00cf\u0001\u001a\u00030\u0099\u0001J\u0007\u0010\u00d0\u0001\u001a\u00020zJ\"\u0010\u00d1\u0001\u001a\u00020z2\u0007\u0010|\u001a\u00030\u00d2\u00012\u0007\u0010\u00a5\u0001\u001a\u00020:2\u0007\u0010\u00c3\u0001\u001a\u00020ZJ\u0007\u0010\u00d3\u0001\u001a\u00020zJ\u0011\u0010\u00d4\u0001\u001a\u00020z2\u0008\u0010\u00cc\u0001\u001a\u00030\u00d5\u0001J\u0007\u0010\u00d6\u0001\u001a\u00020zJ\u001b\u0010\u00d7\u0001\u001a\u00020z2\u0008\u0010\u00d8\u0001\u001a\u00030\u0099\u00012\u0008\u0010\u00d9\u0001\u001a\u00030\u0099\u0001J\u0007\u0010\u00da\u0001\u001a\u00020zJ\t\u0010\u00db\u0001\u001a\u00020zH\u0002J\t\u0010\u00dc\u0001\u001a\u00020ZH\u0002J\t\u0010\u00dd\u0001\u001a\u00020ZH\u0002J\t\u0010\u00de\u0001\u001a\u00020zH\u0002J\t\u0010\u00df\u0001\u001a\u00020ZH\u0002J\t\u0010\u00e0\u0001\u001a\u00020zH\u0002J\t\u0010\u00e1\u0001\u001a\u00020zH\u0002J\t\u0010\u00e2\u0001\u001a\u00020zH\u0002J\t\u0010\u00e3\u0001\u001a\u00020zH\u0002J\t\u0010\u00e4\u0001\u001a\u00020zH\u0002J\u0011\u0010\u00e5\u0001\u001a\u00020z2\u0006\u0010|\u001a\u00020\u0002H\u0002J\t\u0010\u00e6\u0001\u001a\u00020zH\u0002J\t\u0010\u00e7\u0001\u001a\u00020zH\u0002J\t\u0010\u00e8\u0001\u001a\u00020zH\u0002J\t\u0010\u00e9\u0001\u001a\u00020zH\u0002J\r\u0010\u00ea\u0001\u001a\u00020Z*\u00020\u0007H\u0002R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00104\u001a\u0002058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00086\u00107R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u00108\u001a\u0010\u0012\u0004\u0012\u00020:\u0012\u0004\u0012\u00020;\u0018\u0001098\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008<\u0010=\u001a\u0004\u0008>\u0010?\"\u0004\u0008@\u0010AR\u0014\u0010B\u001a\u0008\u0012\u0004\u0012\u00020D0CX\u0082\u0004\u00a2\u0006\u0002\n\u0000R0\u0010E\u001a\u000e\u0012\u0004\u0012\u00020:\u0012\u0004\u0012\u00020F098\u0006@\u0006X\u0087.\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008G\u0010=\u001a\u0004\u0008H\u0010?\"\u0004\u0008I\u0010AR\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010J\u001a\u0004\u0018\u00010DX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010K\u001a\u0004\u0018\u0001058F\u00a2\u0006\u0006\u001a\u0004\u0008L\u00107R&\u0010M\u001a\u0004\u0018\u00010N8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008O\u0010=\u001a\u0004\u0008P\u0010Q\"\u0004\u0008R\u0010SR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010T\u001a\u0004\u0018\u00010UX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010VR\u000e\u00101\u001a\u000202X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010W\u001a\u0004\u0018\u00010UX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010VR\u0012\u0010X\u001a\u0004\u0018\u00010UX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010VR$\u0010Y\u001a\u00020Z8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008[\u0010=\u001a\u0004\u0008\\\u0010]\"\u0004\u0008^\u0010_R$\u0010`\u001a\u00020Z8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008a\u0010=\u001a\u0004\u0008b\u0010]\"\u0004\u0008c\u0010_R$\u0010d\u001a\u00020Z8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008e\u0010=\u001a\u0004\u0008f\u0010]\"\u0004\u0008g\u0010_R$\u0010h\u001a\u00020Z8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008i\u0010=\u001a\u0004\u0008j\u0010]\"\u0004\u0008k\u0010_R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010l\u001a\u00020ZX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010m\u001a\u0004\u0018\u00010UX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010VR\u000e\u0010/\u001a\u000200X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010n\u001a\u00020Z8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008n\u0010]R\u0011\u0010o\u001a\u00020Z8F\u00a2\u0006\u0006\u001a\u0004\u0008o\u0010]R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010p\u001a\u0004\u0018\u00010qX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010r\u001a\u00020sX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020.X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010t\u001a\u00020Z8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008u\u0010]R\u000e\u0010\'\u001a\u00020(X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010v\u001a\u0008\u0012\u0004\u0012\u00020x0w8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00ed\u0001"
    }
    d2 = {
        "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/configure/item/ConfigureItemDetailView;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;",
        "navigator",
        "Lcom/squareup/configure/item/ConfigureItemNavigator;",
        "scopeRunner",
        "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "currency",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "host",
        "Lcom/squareup/configure/item/ConfigureItemHost;",
        "invoiceTutorialRunner",
        "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
        "barcodeScannerTracker",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "durationPickerRunner",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        "itemAmountValidator",
        "Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "catalogIntegrationController",
        "Lcom/squareup/catalogapi/CatalogIntegrationController;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "scaleTracker",
        "Lcom/squareup/scales/ScaleTracker;",
        "intermissionHelper",
        "Lcom/squareup/intermission/IntermissionHelper;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "(Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/text/DurationFormatter;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/cogs/Cogs;Lcom/squareup/scales/ScaleTracker;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/permissions/EmployeeManagement;)V",
        "actionbarText",
        "",
        "getActionbarText",
        "()Ljava/lang/CharSequence;",
        "availableCartTaxesInOrder",
        "",
        "",
        "Lcom/squareup/checkout/Tax;",
        "availableCartTaxesInOrder$annotations",
        "()V",
        "getAvailableCartTaxesInOrder",
        "()Ljava/util/Map;",
        "setAvailableCartTaxesInOrder",
        "(Ljava/util/Map;)V",
        "availableDiningOptions",
        "",
        "Lcom/squareup/checkout/DiningOption;",
        "availablePerItemDiscountsInOrder",
        "Lcom/squareup/checkout/Discount;",
        "availablePerItemDiscountsInOrder$annotations",
        "getAvailablePerItemDiscountsInOrder",
        "setAvailablePerItemDiscountsInOrder",
        "cartLevelDefaultDiningOption",
        "conditionalTaxesHelpText",
        "getConditionalTaxesHelpText",
        "connectedScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "connectedScale$annotations",
        "getConnectedScale",
        "()Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "setConnectedScale",
        "(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "durationToken",
        "",
        "Ljava/lang/Long;",
        "finalDurationToken",
        "gapDurationToken",
        "hasCompatibleScaleReading",
        "",
        "hasCompatibleScaleReading$annotations",
        "getHasCompatibleScaleReading",
        "()Z",
        "setHasCompatibleScaleReading",
        "(Z)V",
        "hasConnectedScale",
        "hasConnectedScale$annotations",
        "getHasConnectedScale",
        "setHasConnectedScale",
        "hasDisplayedScaleReading",
        "hasDisplayedScaleReading$annotations",
        "getHasDisplayedScaleReading",
        "setHasDisplayedScaleReading",
        "hasScaleError",
        "hasScaleError$annotations",
        "getHasScaleError",
        "setHasScaleError",
        "ignoreListenerChanges",
        "initialDurationToken",
        "isWorkingItem",
        "isZeroQuantityAllowed",
        "lastReceivedScaleDisplayEvent",
        "Lcom/squareup/configure/item/ScaleDisplayEvent;",
        "random",
        "Ljava/util/Random;",
        "shouldLogScaleEvent",
        "getShouldLogScaleEvent",
        "warningPopupPresenter",
        "Lcom/squareup/flowlegacy/NoResultPopupPresenter;",
        "Lcom/squareup/widgets/warning/Warning;",
        "addBarcodeToNote",
        "",
        "barcode",
        "view",
        "barcodeScanned",
        "buildAndPopulateConfigurableSections",
        "buildAndPopulateDurationRow",
        "buildAndPopulateFixedPriceOverrideField",
        "selectedVariation",
        "Lcom/squareup/checkout/OrderVariation;",
        "buildAndPopulateGapTimeRows",
        "buildAndPopulateModifierRows",
        "buildAndPopulateQuantityRow",
        "buildAndPopulateReadOnlyConfigurationSection",
        "buildAndPopulateVariationsRow",
        "buildAndPopulateViews",
        "buildDescriptionSection",
        "buildDiningOptionSection",
        "buildDiscountsSection",
        "buildMinMaxModifierList",
        "modifierList",
        "Lcom/squareup/checkout/OrderModifierList;",
        "buildOrUpdateTaxesSection",
        "updateTaxes",
        "cacheConnectedScale",
        "event",
        "clearScaleQuantity",
        "doDelete",
        "generateConfigurationSummary",
        "getModifierForOrdinal",
        "Lcom/squareup/checkout/OrderModifier;",
        "modifierListClientOrdinal",
        "",
        "modifierOrdinal",
        "getModifierListForOrdinal",
        "getOrCreateSelectedModifierList",
        "Ljava/util/SortedMap;",
        "getQuantityEntryType",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        "getVariationByIndex",
        "idx",
        "hasDiscountApplied",
        "discountId",
        "hasTaxApplied",
        "taxId",
        "hideFixedPriceOverrideField",
        "isSelectedVariationWeightBased",
        "logIfDeviceErrorDebounced",
        "logWeighedItemizationAdded",
        "logWeighedItemizationCanceled",
        "maybeDisplayScaleQuantity",
        "reading",
        "Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;",
        "onCancelSelected",
        "onCommitSelected",
        "onCompButtonClicked",
        "onDeleteButtonClicked",
        "onDiningOptionSelected",
        "diningOptionIndex",
        "onDiscountRowChanged",
        "applied",
        "onEditingItemTitle",
        "string",
        "onEditingPriceEditText",
        "newAmount",
        "Lcom/squareup/protos/common/Money;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onFinalDurationRowClicked",
        "onFixedPriceOverrideButtonClicked",
        "onGapDurationRowClicked",
        "onGapRowCheckChanged",
        "checked",
        "onInitialDurationRowClicked",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onModifierSelected",
        "onNoteChanged",
        "text",
        "onQuantityChanged",
        "quantity",
        "onScaleDisplayEventReceived",
        "onSelectedVariationClicked",
        "checkedIndexId",
        "onStartVisualTransition",
        "onTaxRowChanged",
        "Landroid/widget/Checkable;",
        "onUncompButtonClicked",
        "onUnitQuantityChanged",
        "Ljava/math/BigDecimal;",
        "onVariablePriceButtonClicked",
        "onVariationCheckChanged",
        "checkedOrdinal",
        "previousOrdinal",
        "onVoidButtonClicked",
        "setDefaultQuantity",
        "shouldFocusOnUnitQuantityRow",
        "shouldHideQuantityFooter",
        "showFixedPriceOverrideField",
        "showServiceInfo",
        "showUnitQuantityRow",
        "updateActionBarPrimaryButtonState",
        "updateActionBarText",
        "updateDurationIfService",
        "updateFirstInvoiceTutorial",
        "updateFixedPriceOverrideField",
        "updateIntermissionIfService",
        "updateQuantityEntryType",
        "updateQuantityFooter",
        "updateQuantitySectionHeader",
        "isNonUnitPricedService",
        "Companion",
        "SharedScope",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$Companion;

.field private static final editableDiningOptionTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private availableCartTaxesInOrder:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private final availableDiningOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation
.end field

.field public availablePerItemDiscountsInOrder:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final cartLevelDefaultDiningOption:Lcom/squareup/checkout/DiningOption;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

.field private durationToken:Ljava/lang/Long;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private finalDurationToken:Ljava/lang/Long;

.field private gapDurationToken:Ljava/lang/Long;

.field private hasCompatibleScaleReading:Z

.field private hasConnectedScale:Z

.field private hasDisplayedScaleReading:Z

.field private hasScaleError:Z

.field private final host:Lcom/squareup/configure/item/ConfigureItemHost;

.field private ignoreListenerChanges:Z

.field private initialDurationToken:Ljava/lang/Long;

.field private final intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final itemAmountValidator:Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;

.field private lastReceivedScaleDisplayEvent:Lcom/squareup/configure/item/ScaleDisplayEvent;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final random:Ljava/util/Random;

.field private final res:Lcom/squareup/util/Res;

.field private final scaleTracker:Lcom/squareup/scales/ScaleTracker;

.field private final scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

.field public final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->Companion:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$Companion;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Item$Type;

    .line 1846
    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->editableDiningOptionTypes:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/text/DurationFormatter;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/cogs/Cogs;Lcom/squareup/scales/ScaleTracker;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/scales/ScaleTracker;",
            "Lcom/squareup/intermission/IntermissionHelper;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "navigator"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopeRunner"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBar"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "host"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTutorialRunner"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "barcodeScannerTracker"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationPickerRunner"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemAmountValidator"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogIntegrationController"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scaleTracker"

    move-object/from16 v15, p21

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intermissionHelper"

    move-object/from16 v15, p22

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    move-object/from16 v15, p23

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-direct/range {p0 .. p0}, Lmortar/ViewPresenter;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    iput-object v2, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    iput-object v3, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object v4, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iput-object v5, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object v6, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->localeProvider:Ljavax/inject/Provider;

    iput-object v7, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    iput-object v8, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object v9, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object v10, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    iput-object v11, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v12, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iput-object v13, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iput-object v14, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object v15, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->itemAmountValidator:Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;

    iput-object v2, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    iput-object v2, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    move-object/from16 v1, p21

    move-object/from16 v2, p22

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scaleTracker:Lcom/squareup/scales/ScaleTracker;

    iput-object v2, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    move-object/from16 v1, p23

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 207
    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v1}, Lcom/squareup/configure/item/ConfigureItemHost;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->cartLevelDefaultDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 208
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    .line 211
    new-instance v1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 240
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->random:Ljava/util/Random;

    return-void
.end method

.method public static final synthetic access$doDelete(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->doDelete()V

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getDurationPickerRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/register/widgets/NohoDurationPickerRunner;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    return-object p0
.end method

.method public static final synthetic access$getDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/lang/Long;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationToken:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getEmployeeManagement$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/permissions/EmployeeManagement;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-object p0
.end method

.method public static final synthetic access$getFinalDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/lang/Long;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->finalDurationToken:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getGapDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/lang/Long;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->gapDurationToken:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getHost$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemHost;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    return-object p0
.end method

.method public static final synthetic access$getIgnoreListenerChanges$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Z
    .locals 0

    .line 181
    iget-boolean p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    return p0
.end method

.method public static final synthetic access$getInitialDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/lang/Long;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->initialDurationToken:Ljava/lang/Long;

    return-object p0
.end method

.method public static final synthetic access$getNavigator$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemNavigator;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    return-object p0
.end method

.method public static final synthetic access$getRandom$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/util/Random;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->random:Ljava/util/Random;

    return-object p0
.end method

.method public static final synthetic access$getScaleTracker$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/scales/ScaleTracker;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scaleTracker:Lcom/squareup/scales/ScaleTracker;

    return-object p0
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemDetailView;
    .locals 0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    return-object p0
.end method

.method public static final synthetic access$hasView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Z
    .locals 0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasView()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/Long;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationToken:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$setFinalDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/Long;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->finalDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$setGapDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/Long;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->gapDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$setIgnoreListenerChanges$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Z)V
    .locals 0

    .line 181
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    return-void
.end method

.method public static final synthetic access$setInitialDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/Long;)V
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->initialDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$updateDurationIfService(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateDurationIfService()V

    return-void
.end method

.method public static final synthetic access$updateIntermissionIfService(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateIntermissionIfService()V

    return-void
.end method

.method private final addBarcodeToNote(Ljava/lang/String;Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 6

    .line 387
    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getSelectableNoteText()Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    move-result-object v0

    .line 393
    iget v1, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget v2, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    const/4 v3, 0x0

    if-ne v1, v2, :cond_3

    .line 394
    iget-object v1, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    if-eqz v3, :cond_2

    move-object v1, p1

    goto :goto_0

    .line 397
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    invoke-virtual {v1, v2, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 398
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "StringBuilder(existingTe\u2026              .toString()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 401
    :cond_3
    iget-object v1, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    const-string v2, "existingText.text"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v4, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    const-string v5, "null cannot be cast to non-null type java.lang.String"

    if-eqz v1, :cond_5

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 402
    iget-object v3, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v2, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-eqz v3, :cond_4

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).substring(startIndex)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 403
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 406
    :goto_0
    new-instance v2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    .line 408
    iget v3, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    .line 409
    iget v0, v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr v0, p1

    .line 406
    invoke-direct {v2, v1, v3, v0}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    .line 412
    invoke-virtual {p2, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setNoteText(Lcom/squareup/text/SelectableTextScrubber$SelectableText;)V

    return-void

    .line 402
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 401
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic availableCartTaxesInOrder$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic availablePerItemDiscountsInOrder$annotations()V
    .locals 0

    return-void
.end method

.method private final buildAndPopulateConfigurableSections(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 2

    .line 693
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateVariationsRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 694
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateModifierRows(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 696
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 697
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateDurationRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 698
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateGapTimeRows(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isGiftCard()Z

    move-result v0

    if-nez v0, :cond_1

    .line 703
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateQuantityRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-nez v0, :cond_2

    .line 708
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildNotesRow()V

    .line 709
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setNoteText(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private final buildAndPopulateDurationRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 1

    .line 826
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildDurationRow()V

    .line 828
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDurationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 837
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateDurationIfService()V

    return-void
.end method

.method private final buildAndPopulateFixedPriceOverrideField(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/checkout/OrderVariation;)V
    .locals 2

    .line 785
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildFixedPriceOverridePriceButton()V

    .line 791
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIXED_PRICE_SERVICE_PRICE_EDITING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p2

    if-nez p2, :cond_0

    .line 793
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showFixedPriceOverrideField()V

    .line 794
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateFixedPriceOverrideField(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    goto :goto_0

    .line 796
    :cond_0
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hideFixedPriceOverrideField()V

    :goto_0
    return-void
.end method

.method private final buildAndPopulateGapTimeRows(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 0

    .line 841
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildGapTimeRows()V

    .line 843
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateIntermissionIfService()V

    return-void
.end method

.method private final buildAndPopulateModifierRows(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 7

    .line 801
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/OrderModifierList;

    const-string v3, "modifierList"

    .line 802
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildMinMaxModifierList(Lcom/squareup/checkout/OrderModifierList;)V

    .line 805
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedModifiers()Ljava/util/SortedMap;

    move-result-object v3

    iget v4, v2, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/SortedMap;

    if-eqz v3, :cond_0

    .line 810
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderModifierList;->useMinMaxSelection()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2}, Lcom/squareup/checkout/OrderModifierList;->isMultipleSelection()Z

    move-result v4

    if-nez v4, :cond_2

    .line 812
    iget-object v4, v2, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v4}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 813
    invoke-interface {v3}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 815
    iget-object v4, v2, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v4}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 816
    move-object v5, v3

    check-cast v5, Ljava/util/Map;

    iget-object v6, v2, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-interface {v6, v4}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    :cond_2
    invoke-interface {v3}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 819
    iget v5, v2, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    if-nez v4, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setModifierChecked(II)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private final buildAndPopulateQuantityRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 4

    .line 874
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildQuantityAndUnitQuantityRows()V

    .line 877
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isNonUnitPricedService(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)Z

    move-result v0

    const-string v1, "scopeRunner.state"

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideAllQuantityRows()V

    goto :goto_0

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 879
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    .line 880
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantityPrecision()I

    move-result v2

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->shouldFocusOnUnitQuantityRow()Z

    move-result v3

    .line 878
    invoke-virtual {p1, v0, v2, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showUnitQuantityRow(Ljava/lang/String;IZ)V

    goto :goto_0

    .line 882
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showQuantityRow()V

    .line 885
    :goto_0
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateQuantitySectionHeader()V

    .line 887
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    if-nez v0, :cond_2

    .line 888
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->setDefaultQuantity()V

    goto :goto_1

    .line 889
    :cond_2
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    const-string v2, "scopeRunner.state.quantity"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 890
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setQuantity(Ljava/math/BigDecimal;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private final buildAndPopulateReadOnlyConfigurationSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 6

    .line 597
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getNote()Ljava/lang/String;

    move-result-object v0

    .line 598
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 599
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->generateConfigurationSummary()Ljava/lang/String;

    move-result-object v2

    .line 602
    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 603
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 605
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "\n"

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 609
    :cond_0
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "note"

    .line 611
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "view.context"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string v4, "MarketFont.Weight.DEFAULT"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    .line 1873
    new-instance v5, Lcom/squareup/fonts/FontSpan;

    invoke-static {v3, v4}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v3

    invoke-direct {v5, v0, v3}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v5, Landroid/text/style/CharacterStyle;

    .line 611
    invoke-static {v2, v5}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 610
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 616
    :cond_1
    move-object v0, v1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 617
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildConfigurationDisplayRow(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private final buildAndPopulateVariationsRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 9

    .line 714
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    .line 716
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 718
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getBackingType()Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    move-result-object v2

    .line 719
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-ne v2, v3, :cond_3

    .line 720
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildTitleForCustomAmount(Ljava/lang/CharSequence;)V

    .line 722
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 725
    iget-object v2, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v8, v2, v6

    if-nez v8, :cond_2

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v2, v1, v5, v4, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 727
    :goto_1
    invoke-virtual {p1, v5}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildVariablePriceEditText(Ljava/lang/CharSequence;)V

    .line 728
    invoke-virtual {p1, v5}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setVariableAmountEditText(Ljava/lang/CharSequence;)V

    .line 729
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateFirstInvoiceTutorial()V

    goto/16 :goto_2

    .line 733
    :cond_3
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-static {v2, v3, v5, v4, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 732
    invoke-virtual {p1, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildVariablePriceButton(Ljava/lang/CharSequence;)V

    .line 737
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v2, v1, v5, v4, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 736
    invoke-virtual {p1, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setVariableAmountButton(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 740
    :cond_4
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_7

    .line 743
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "scopeRunner.state.name"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    const-string v4, "localeProvider.get()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/util/Locale;

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 744
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v3

    .line 745
    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 742
    invoke-virtual {p1, v2, v3, v4}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildVariationsRow(Ljava/lang/String;Ljava/util/List;Lcom/squareup/quantity/PerUnitFormatter;)V

    if-eqz v0, :cond_7

    .line 751
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 754
    invoke-virtual {p1, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setVariationSelected(I)V

    .line 756
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 758
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v3

    const-string v4, "selectedVariation.unitAbbreviation"

    if-nez v3, :cond_5

    .line 761
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/configure/item/R$string;->item_library_variable_price:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 762
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 761
    invoke-static {v1, v3}, Lcom/squareup/quantity/ItemQuantities;->formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 759
    invoke-virtual {p1, v2, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setVariation(ILjava/lang/CharSequence;)V

    goto :goto_2

    .line 768
    :cond_5
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 769
    iget-object v5, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v5}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v5

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 770
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 768
    invoke-virtual {v3, v1, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 766
    invoke-virtual {p1, v2, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setVariation(ILjava/lang/CharSequence;)V

    goto :goto_2

    .line 743
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 778
    :cond_7
    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateFixedPriceOverrideField(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/checkout/OrderVariation;)V

    return-void
.end method

.method private final buildAndPopulateViews()V
    .locals 8

    .line 417
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->clearContents()V

    const/4 v0, 0x1

    .line 420
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    .line 423
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    const-string v2, "scopeRunner.state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->isConfigurationLockedFromTicket()Z

    move-result v1

    const-string/jumbo v3, "view"

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->isConfigurationLockedFromScale()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 427
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateConfigurableSections(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    goto :goto_1

    .line 425
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateReadOnlyConfigurationSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 431
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildDiningOptionSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)Z

    move-result v1

    .line 434
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildOrUpdateTaxesSection(Lcom/squareup/configure/item/ConfigureItemDetailView;Z)Z

    move-result v4

    .line 437
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v6}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildDiscountsSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)Z

    move-result v6

    .line 440
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildDescriptionSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)Z

    move-result v3

    .line 442
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/configure/item/ConfigureItemDetailView;

    if-nez v1, :cond_3

    if-nez v4, :cond_3

    if-nez v6, :cond_3

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v7, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildButtonsContainer(Z)V

    .line 446
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v1}, Lcom/squareup/configure/item/ConfigureItemHost;->isVoidCompAllowed()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 447
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->isCompable()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 448
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->configureCompButton()V

    goto :goto_4

    .line 449
    :cond_4
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->isUncompable()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 450
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->configureUncompButton()V

    .line 455
    :cond_5
    :goto_4
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->isDeletable()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 456
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->configureDeleteButton()V

    goto :goto_5

    .line 457
    :cond_6
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->isVoidable()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v1}, Lcom/squareup/configure/item/ConfigureItemHost;->isVoidCompAllowed()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 458
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->configureVoidButton()V

    .line 462
    :cond_7
    :goto_5
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarText()V

    .line 466
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarPrimaryButtonState()V

    .line 468
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 469
    new-instance v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;

    invoke-direct {v2, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 475
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 476
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v4

    if-eqz v4, :cond_8

    sget v4, Lcom/squareup/configure/item/R$string;->add:I

    goto :goto_6

    :cond_8
    sget v4, Lcom/squareup/common/strings/R$string;->save:I

    .line 475
    :goto_6
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 474
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 479
    new-instance v4, Lcom/squareup/configure/item/ConfigureItemDetailScreen$sam$java_lang_Runnable$0;

    invoke-direct {v4, v2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 480
    new-instance v3, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$1;

    invoke-direct {v3, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 481
    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextCentered(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 483
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 486
    iput-boolean v5, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    return-void
.end method

.method private final buildDescriptionSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)Z
    .locals 2

    .line 584
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getItemDescription()Ljava/lang/String;

    move-result-object v0

    .line 585
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 586
    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildItemDescriptionSection(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private final buildDiningOptionSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)Z
    .locals 4

    .line 492
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    .line 493
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->cartLevelDefaultDiningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz v2, :cond_3

    .line 495
    sget-object v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->editableDiningOptionTypes:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    if-nez v0, :cond_1

    .line 502
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->cartLevelDefaultDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 504
    :cond_1
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 505
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v2}, Lcom/squareup/configure/item/ConfigureItemHost;->getDiningOptions()Ljava/util/List;

    move-result-object v2

    const-string v3, "host.diningOptions"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 508
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 509
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 513
    :cond_2
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/configure/item/R$string;->uppercase_cart_dining_option_header:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 514
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    .line 512
    invoke-virtual {p1, v1, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildDiningOptionsList(Ljava/lang/String;Ljava/util/List;)V

    .line 517
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDiningOptionSelected(I)V

    const/4 p1, 0x1

    return p1

    :cond_3
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method private final buildDiscountsSection(Lcom/squareup/configure/item/ConfigureItemDetailView;)Z
    .locals 7

    .line 565
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getToggleableDiscountsForItem()Ljava/util/Map;

    move-result-object v0

    const-string v1, "scopeRunner.toggleableDiscountsForItem"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availablePerItemDiscountsInOrder:Ljava/util/Map;

    .line 567
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isUncompable()Z

    move-result v0

    const/4 v2, 0x1

    const-string v3, "availablePerItemDiscountsInOrder"

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availablePerItemDiscountsInOrder:Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_4

    .line 569
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availablePerItemDiscountsInOrder:Ljava/util/Map;

    if-nez v0, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1869
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 1870
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1871
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 570
    new-instance v6, Lcom/squareup/configure/item/AdjustmentPair;

    check-cast v4, Lcom/squareup/checkout/Adjustment;

    invoke-direct {p0, v5}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDiscountApplied(Ljava/lang/String;)Z

    move-result v5

    invoke-direct {v6, v4, v5}, Lcom/squareup/configure/item/AdjustmentPair;-><init>(Lcom/squareup/checkout/Adjustment;Z)V

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1872
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 576
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    .line 577
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 574
    invoke-virtual {p1, v3, v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildDiscountRows(Ljava/util/List;ILcom/squareup/quantity/PerUnitFormatter;)V

    :cond_4
    return v2
.end method

.method private final buildMinMaxModifierList(Lcom/squareup/checkout/OrderModifierList;)V
    .locals 10

    .line 928
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v6

    .line 931
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifierList;->useMinMaxSelection()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 932
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifierList;->getMinSelectedModifiers()I

    move-result v0

    .line 933
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifierList;->getMaxSelectedModifiers()I

    move-result v2

    goto :goto_0

    .line 935
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifierList;->isMultipleSelection()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 937
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifierList;->size()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "null cannot be cast to non-null type java.lang.String"

    const-string v4, "(this as java.lang.String).toUpperCase(locale)"

    const-string v5, "localeProvider.get()"

    const-string v7, "modifierList.name"

    if-ne v0, v1, :cond_3

    if-ne v2, v1, :cond_3

    .line 948
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    .line 949
    iget v0, p1, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    iget-object v1, p1, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    .line 950
    iget-object v8, p1, Lcom/squareup/checkout/OrderModifierList;->name:Ljava/lang/String;

    invoke-static {v8, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Ljava/util/Locale;

    if-eqz v8, :cond_2

    invoke-virtual {v8, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v3

    check-cast v5, Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 951
    iget v8, p1, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    move v3, v0

    move-object v4, v1

    .line 948
    invoke-virtual/range {v2 .. v8}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildSingleChoiceModifierList(ILjava/util/SortedMap;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/quantity/PerUnitFormatter;I)V

    return-void

    .line 950
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    const-string v8, "count"

    if-nez v0, :cond_5

    if-ne v2, v1, :cond_4

    .line 960
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->uppercase_choose_up_to_one:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_1

    .line 962
    :cond_4
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->uppercase_choose_up_to_count:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 963
    invoke-virtual {v0, v8, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 964
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.phrase(R.string.uppe\u2026)\n              .format()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v1, "res.phrase(R.string.uppe\u2026ed)\n            .format()"

    if-ne v0, v2, :cond_6

    .line 967
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/configure/item/R$string;->uppercase_choose_count:I

    invoke-interface {v0, v9}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 968
    invoke-virtual {v0, v8, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 969
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 971
    :cond_6
    iget-object v8, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/configure/item/R$string;->uppercase_choose_between_counts:I

    invoke-interface {v8, v9}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v8

    const-string/jumbo v9, "x"

    .line 972
    invoke-virtual {v8, v9, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v8, "y"

    .line 973
    invoke-virtual {v0, v8, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 974
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    move-object v9, v0

    .line 976
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    .line 977
    iget v0, p1, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    iget-object v1, p1, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    .line 978
    iget-object v8, p1, Lcom/squareup/checkout/OrderModifierList;->name:Ljava/lang/String;

    invoke-static {v8, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Ljava/util/Locale;

    if-eqz v8, :cond_7

    invoke-virtual {v8, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v3

    check-cast v5, Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 979
    iget v8, p1, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    move v3, v0

    move-object v4, v1

    .line 976
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildMultiChoiceModifierList(ILjava/util/SortedMap;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/quantity/PerUnitFormatter;ILjava/lang/CharSequence;)V

    return-void

    .line 978
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final buildOrUpdateTaxesSection(Lcom/squareup/configure/item/ConfigureItemDetailView;Z)Z
    .locals 10

    .line 526
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getAvailableCartTaxes()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-eqz v0, :cond_7

    .line 531
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    if-nez v3, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 533
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    if-nez v3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/Tax;

    .line 534
    iget-boolean v6, v5, Lcom/squareup/checkout/Tax;->enabled:Z

    or-int/2addr v4, v6

    .line 535
    move-object v6, v0

    check-cast v6, Ljava/util/Collection;

    new-instance v7, Lcom/squareup/configure/item/AdjustmentPair;

    move-object v8, v5

    check-cast v8, Lcom/squareup/checkout/Adjustment;

    iget-object v5, v5, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    const-string v9, "tax.id"

    invoke-static {v5, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasTaxApplied(Ljava/lang/String;)Z

    move-result v5

    invoke-direct {v7, v8, v5}, Lcom/squareup/configure/item/AdjustmentPair;-><init>(Lcom/squareup/checkout/Adjustment;Z)V

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const-string v3, "scopeRunner.state"

    if-eqz v4, :cond_5

    .line 540
    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemState;->isGiftCard()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v2, 0x1

    :cond_5
    if-eqz v2, :cond_7

    if-eqz p2, :cond_6

    .line 545
    check-cast v0, Ljava/util/List;

    .line 546
    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p2

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/SortedMap;->size()I

    move-result p2

    add-int/lit8 p2, p2, 0x2

    .line 544
    invoke-virtual {p1, v0, p2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->updateTaxRows(Ljava/util/List;I)V

    goto :goto_1

    .line 551
    :cond_6
    check-cast v0, Ljava/util/List;

    .line 552
    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p2

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/SortedMap;->size()I

    move-result p2

    add-int/lit8 p2, p2, 0x2

    .line 550
    invoke-virtual {p1, v0, p2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildTaxRows(Ljava/util/List;I)V

    :cond_7
    :goto_1
    return v2
.end method

.method private final cacheConnectedScale(Lcom/squareup/configure/item/ScaleDisplayEvent;)V
    .locals 1

    .line 1664
    instance-of v0, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p1

    goto :goto_0

    .line 1665
    :cond_0
    instance-of v0, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;->getHardwareScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1663
    :goto_0
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-void
.end method

.method private final clearScaleQuantity()V
    .locals 1

    .line 1716
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    if-eqz v0, :cond_0

    .line 1717
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->setDefaultQuantity()V

    .line 1718
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->clearQuantityPrecisionOverride()V

    .line 1719
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showUnitQuantityRow()V

    .line 1721
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->enableUnitQuantityRowAndHideLockIcon()V

    const/4 v0, 0x0

    .line 1722
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    .line 1723
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasCompatibleScaleReading:Z

    return-void
.end method

.method public static synthetic connectedScale$annotations()V
    .locals 0

    return-void
.end method

.method private final doDelete()V
    .locals 2

    .line 1461
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_ITEMIZATION_VOID:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1462
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/configure/item/ConfigureItemHost;->onDeleteSelected(Lcom/squareup/configure/item/ConfigureItemState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1463
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    :cond_0
    return-void
.end method

.method private final generateConfigurationSummary()Ljava/lang/String;
    .locals 8

    .line 625
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    .line 627
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 629
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->orderDestination()Lcom/squareup/checkout/OrderDestination;

    move-result-object v2

    .line 630
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 631
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/OrderDestination;->description(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 632
    move-object v3, v1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v2, "state"

    .line 635
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isGiftCard()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 636
    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/common/card/R$string;->gift_card:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 638
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    move-object v2, v3

    .line 639
    :goto_0
    move-object v4, v2

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v2, :cond_3

    .line 640
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    :cond_4
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedModifiers()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/SortedMap;

    const-string v5, "modifierList"

    .line 645
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/OrderModifier;

    .line 646
    iget-object v6, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v5, v6}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 652
    :cond_6
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v2

    const-string v4, "state.quantity"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    const-string v5, "ONE"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v4}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v2, :cond_8

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v2

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_9

    :cond_8
    const/4 v4, 0x1

    .line 654
    :cond_9
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 655
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantityPrecision()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v2

    .line 656
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    const-string v6, "state.unitAbbreviation"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    .line 657
    invoke-virtual {v0}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 659
    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v5

    const-string v5, "quantity"

    if-eqz v2, :cond_c

    .line 660
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 661
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 662
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 663
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/configure/item/R$string;->configure_item_summary:I

    invoke-interface {v3, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 664
    check-cast v2, Ljava/lang/CharSequence;

    const-string v6, "attributes"

    invoke-virtual {v3, v6, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 665
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    const-string v6, "new_attribute"

    invoke-virtual {v2, v6, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 666
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 667
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_a
    if-eqz v4, :cond_b

    .line 671
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/configure/item/R$string;->configure_item_summary_quantity:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 672
    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "summary"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 673
    invoke-virtual {v1, v5, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 674
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 675
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_b
    return-object v2

    :cond_c
    if-eqz v4, :cond_d

    .line 679
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->configure_item_summary_quantity_only:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 680
    invoke-virtual {v1, v5, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 681
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 682
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_d
    return-object v3
.end method

.method private final getActionbarText()Ljava/lang/CharSequence;
    .locals 4

    const-string v0, ""

    .line 260
    check-cast v0, Ljava/lang/CharSequence;

    .line 262
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    const-string v2, "scopeRunner.state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 263
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    const-string v3, "scopeRunner.state.quantity"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "it"

    .line 264
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 267
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 270
    :goto_0
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 271
    invoke-virtual {v1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    .line 272
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    const-string v3, "scopeRunner.state.unitAbbreviation"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 276
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 281
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/configure/item/R$string;->cart_item_price:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 283
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 284
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 286
    :cond_3
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getName()Ljava/lang/String;

    move-result-object v2

    .line 283
    :goto_2
    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "item_name"

    .line 282
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "item_price"

    .line 288
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "Phrase.from(res.getStrin\u2026ce)\n            .format()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getModifierForOrdinal(II)Lcom/squareup/checkout/OrderModifier;
    .locals 4

    .line 1490
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getModifierListForOrdinal(I)Lcom/squareup/checkout/OrderModifierList;

    move-result-object v0

    .line 1492
    iget-object v1, v0, Lcom/squareup/checkout/OrderModifierList;->modifiers:Ljava/util/SortedMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/OrderModifier;

    if-eqz v1, :cond_0

    return-object v1

    .line 1493
    :cond_0
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    .line 1494
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Locale.US"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1495
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Item modifier list "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/squareup/checkout/OrderModifierList;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ") "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "has no modifier with ordinal "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1496
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    .line 1493
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "java.lang.String.format(locale, format, *args)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1492
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final getModifierListForOrdinal(I)Lcom/squareup/checkout/OrderModifierList;
    .locals 3

    .line 1480
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/OrderModifierList;

    if-eqz v0, :cond_0

    return-object v0

    .line 1481
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1482
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Item has no modifier list with ordinal "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1481
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 3

    .line 1782
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isSelectedVariationWeightBased()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    goto :goto_0

    .line 1785
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    goto :goto_0

    .line 1789
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    const-string v1, "scopeRunner.state.quantity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v2, "ZERO"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1790
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUAL_WITH_CONNECTED_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    goto :goto_0

    .line 1792
    :cond_2
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    :goto_0
    return-object v0
.end method

.method private final getShouldLogScaleEvent()Z
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isSelectedVariationWeightBased()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic hasCompatibleScaleReading$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic hasConnectedScale$annotations()V
    .locals 0

    return-void
.end method

.method private final hasDiscountApplied(Ljava/lang/String;)Z
    .locals 2

    .line 1391
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public static synthetic hasDisplayedScaleReading$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic hasScaleError$annotations()V
    .locals 0

    return-void
.end method

.method private final hasTaxApplied(Ljava/lang/String;)Z
    .locals 2

    .line 1387
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final hideFixedPriceOverrideField()V
    .locals 2

    .line 1213
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideOrShowFixedPriceOverridePrice(Z)V

    return-void
.end method

.method private final isNonUnitPricedService(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)Z
    .locals 1

    .line 1843
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->isUnitPriced()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isSelectedVariationWeightBased()Z
    .locals 2

    .line 1733
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/quantity/ItemQuantities;->isWeight(Lcom/squareup/orders/model/Order$QuantityUnit;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final isWorkingItem()Z
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/configure/item/ConfigureWorkingItemState;

    return v0
.end method

.method private final logIfDeviceErrorDebounced(Lcom/squareup/configure/item/ScaleDisplayEvent;)V
    .locals 2

    .line 1813
    instance-of v0, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->lastReceivedScaleDisplayEvent:Lcom/squareup/configure/item/ScaleDisplayEvent;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 1814
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getShouldLogScaleEvent()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_1

    .line 1818
    :cond_1
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/configure/item/ScaleDisplayErrorEvent;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    invoke-direct {v0, v1}, Lcom/squareup/configure/item/ScaleDisplayErrorEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private final logWeighedItemizationAdded()V
    .locals 6

    .line 1822
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 1823
    :goto_0
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 1824
    new-instance v4, Lcom/squareup/configure/item/ScaleItemizationAddEvent;

    .line 1825
    iget-object v5, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v5}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v5

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v1

    const-string v5, "scopeRunner.state.quantity"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    .line 1826
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v2}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    .line 1827
    :cond_1
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    move-result-object v0

    .line 1828
    iget-object v5, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 1824
    invoke-direct {v4, v1, v2, v0, v5}, Lcom/squareup/configure/item/ScaleItemizationAddEvent;-><init>(Ljava/math/BigDecimal;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v4, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 1823
    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private final logWeighedItemizationCanceled()V
    .locals 4

    .line 1834
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 1835
    new-instance v1, Lcom/squareup/configure/item/ScaleItemizationCancelEvent;

    .line 1836
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    move-result-object v2

    .line 1837
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    .line 1835
    invoke-direct {v1, v2, v3}, Lcom/squareup/configure/item/ScaleItemizationCancelEvent;-><init>(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 1834
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private final maybeDisplayScaleQuantity(Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;)V
    .locals 4

    .line 1681
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isSelectedVariationWeightBased()Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "scopeRunner.state"

    if-eqz v0, :cond_1

    .line 1682
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/configure/item/ScaleDataHelper;->isCompatibleWith(Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;Lcom/squareup/orders/model/Order$QuantityUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasCompatibleScaleReading:Z

    .line 1684
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasCompatibleScaleReading:Z

    if-nez v0, :cond_2

    .line 1686
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->clearScaleQuantity()V

    return-void

    .line 1690
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getWeight()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_3

    return-void

    .line 1695
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getPrecision()I

    move-result v0

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v3

    if-eq v0, v3, :cond_5

    .line 1697
    :cond_4
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getPrecision()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantityPrecisionOverride(I)V

    .line 1698
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showUnitQuantityRow()V

    .line 1701
    :cond_5
    iput-boolean v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    .line 1702
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getWeight()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantity(Ljava/math/BigDecimal;)V

    .line 1704
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->disableUnitQuantityRowAndShowLockIcon()V

    .line 1705
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;->getWeight()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setQuantity(Ljava/math/BigDecimal;)V

    .line 1706
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarPrimaryButtonState()V

    .line 1707
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarText()V

    return-void
.end method

.method private final setDefaultQuantity()V
    .locals 3

    .line 897
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 898
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v2, "ZERO"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 900
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->clearUnitQuantityRowContent()V

    goto :goto_0

    .line 902
    :cond_0
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    const-string v2, "ONE"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 903
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v2, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setQuantity(Ljava/math/BigDecimal;)V

    .line 906
    :goto_0
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantity(Ljava/math/BigDecimal;)V

    return-void
.end method

.method private final shouldFocusOnUnitQuantityRow()Z
    .locals 5

    .line 910
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 911
    :goto_0
    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v4

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 913
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method private final shouldHideQuantityFooter()Z
    .locals 1

    .line 1767
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isSelectedVariationWeightBased()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private final showFixedPriceOverrideField()V
    .locals 2

    .line 1205
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIXED_PRICE_SERVICE_PRICE_EDITING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1207
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isNonUnitPricedService(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1209
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideOrShowFixedPriceOverridePrice(Z)V

    return-void
.end method

.method private final showServiceInfo()Z
    .locals 2

    .line 1631
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isNonUnitPricedService(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final showUnitQuantityRow()V
    .locals 4

    .line 1797
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    .line 1798
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    const-string v2, "scopeRunner.state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    .line 1799
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantityPrecision()I

    move-result v2

    const/4 v3, 0x0

    .line 1797
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showUnitQuantityRow(Ljava/lang/String;IZ)V

    return-void
.end method

.method private final updateActionBarPrimaryButtonState()V
    .locals 8

    .line 1540
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getBackingType()Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    move-result-object v0

    .line 1543
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v2

    const-string v4, "scopeRunner.state.quantity"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1545
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void

    .line 1549
    :cond_1
    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const/4 v4, 0x1

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_5

    .line 1553
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getModifierLists()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/OrderModifierList;

    .line 1557
    iget v6, v5, Lcom/squareup/checkout/OrderModifierList;->clientOrdinal:I

    invoke-virtual {p0, v6}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getOrCreateSelectedModifierList(I)Ljava/util/SortedMap;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/SortedMap;->size()I

    move-result v6

    .line 1558
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifierList;->useMinMaxSelection()Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "modifierList"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifierList;->getMinSelectedModifiers()I

    move-result v7

    if-lt v6, v7, :cond_4

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifierList;->getMaxSelectedModifiers()I

    move-result v5

    if-le v6, v5, :cond_3

    .line 1564
    :cond_4
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void

    :cond_5
    if-eqz v0, :cond_7

    .line 1572
    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-eq v0, v2, :cond_6

    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v0, v2, :cond_7

    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_a

    .line 1576
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1577
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-eqz v0, :cond_8

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->itemAmountValidator:Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;

    invoke-interface {v2, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;->validate(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const/4 v3, 0x1

    :cond_9
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    goto :goto_2

    .line 1579
    :cond_a
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    :goto_2
    return-void
.end method

.method private final updateActionBarText()V
    .locals 3

    .line 1472
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getActionbarText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateDurationIfService()V
    .locals 6

    .line 1584
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1588
    :cond_0
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showServiceInfo()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 1589
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDurationRowVisibility(Z)V

    .line 1590
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    const-string v4, "scopeRunner.state"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v3

    const-string v4, "scopeRunner.state.duration"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, v3, v1, v4, v5}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDuration(Ljava/lang/String;)V

    goto :goto_0

    .line 1592
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDurationRowVisibility(Z)V

    :goto_0
    return-void
.end method

.method private final updateFirstInvoiceTutorial()V
    .locals 5

    .line 1017
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    .line 1018
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->itemAmountValidator:Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;

    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v4

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v4, "scopeRunner.state.currentVariablePrice"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;->validate(Lcom/squareup/protos/common/Money;)Z

    move-result v1

    .line 1019
    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-interface {v3, v2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->invoiceCustomAmountUpdated(Z)V

    return-void
.end method

.method private final updateFixedPriceOverrideField(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 4

    .line 1069
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showFixedPriceOverrideField()V

    .line 1070
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getOverridePrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 1071
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    invoke-virtual {v0, v3}, Lcom/squareup/configure/item/ConfigureItemState;->setOverridePrice(Lcom/squareup/protos/common/Money;)V

    .line 1073
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getOverridePrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1076
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    const/4 v3, 0x2

    invoke-static {v1, v0, v2, v3, v2}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1078
    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->updateFixedPriceOverridePriceButton(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateIntermissionIfService()V
    .locals 5

    .line 1597
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1601
    :cond_0
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showServiceInfo()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 1602
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1603
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 1606
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setGapRowContainerVisibility(Z)V

    return-void

    .line 1610
    :cond_2
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    .line 1611
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    const-string v3, "scopeRunner.state"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getIntermissions()Ljava/util/List;

    move-result-object v2

    const-string v4, "scopeRunner.state.intermissions"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/configure/item/ConfigureItemState;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v3

    const-string v4, "scopeRunner.state.duration"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1610
    invoke-interface {v0, v2, v3}, Lcom/squareup/intermission/IntermissionHelper;->getGapTimeInfo(Ljava/util/List;Lorg/threeten/bp/Duration;)Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1615
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    new-instance v3, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$updateIntermissionIfService$1;

    invoke-direct {v3, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$updateIntermissionIfService$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDurationOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1624
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    sget-object v3, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$updateIntermissionIfService$2;->INSTANCE:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$updateIntermissionIfService$2;

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDurationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1626
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v2, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setGapRowInfo(Lcom/squareup/intermission/GapTimeInfo;)V

    .line 1627
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setGapRowContainerVisibility(Z)V

    return-void
.end method

.method private final updateQuantityEntryType()V
    .locals 2

    .line 1773
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)V

    return-void
.end method

.method private final updateQuantityFooter()V
    .locals 3

    .line 1742
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->shouldHideQuantityFooter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1743
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideQuantityFooter()V

    return-void

    .line 1749
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasScaleError:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/configure/item/R$string;->quantity_footer_error:I

    goto :goto_0

    .line 1752
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasCompatibleScaleReading:Z

    if-nez v0, :cond_2

    sget v0, Lcom/squareup/configure/item/R$string;->quantity_footer_incompatible_unit:I

    goto :goto_0

    .line 1755
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/squareup/configure/item/R$string;->quantity_footer_adjust_weight_on_scale:I

    goto :goto_0

    .line 1758
    :cond_3
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    const-string v1, "scopeRunner.state.quantity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v2, "ZERO"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/squareup/configure/item/R$string;->quantity_footer_manual_weight_entry:I

    goto :goto_0

    .line 1761
    :cond_4
    sget v0, Lcom/squareup/configure/item/R$string;->quantity_footer_no_weight_reading:I

    .line 1764
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showQuantityFooterText(I)V

    return-void
.end method

.method private final updateQuantitySectionHeader()V
    .locals 3

    .line 1187
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isNonUnitPricedService(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isGiftCard()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 1192
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1195
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    .line 1196
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    sget v1, Lcom/squareup/configure/item/R$string;->uppercase_cart_quantity_header:I

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setQuantitySectionHeader(I)V

    return-void

    .line 1200
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    const-string v2, "selectedVariation.quantityUnit!!"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemScreensKt;->headerResourceId(Lcom/squareup/orders/model/Order$QuantityUnit;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setQuantitySectionHeader(I)V

    return-void

    .line 1188
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideQuantitySectionHeader()V

    return-void
.end method


# virtual methods
.method public barcodeScanned(Ljava/lang/String;)V
    .locals 2

    const-string v0, "barcode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    if-eqz v0, :cond_0

    .line 306
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v1

    if-nez v1, :cond_0

    .line 307
    invoke-direct {p0, p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->addBarcodeToNote(Ljava/lang/String;Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    :cond_0
    return-void
.end method

.method public final getAvailableCartTaxesInOrder()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    return-object v0
.end method

.method public final getAvailablePerItemDiscountsInOrder()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availablePerItemDiscountsInOrder:Ljava/util/Map;

    if-nez v0, :cond_0

    const-string v1, "availablePerItemDiscountsInOrder"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getConditionalTaxesHelpText()Ljava/lang/CharSequence;
    .locals 4

    .line 244
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseConditionalTaxes()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    const-string/jumbo v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 250
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 251
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v2, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 252
    sget v0, Lcom/squareup/configure/item/R$string;->conditional_taxes_help_text_for_cart:I

    const-string v3, "dashboard"

    invoke-virtual {v2, v0, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 253
    sget v2, Lcom/squareup/configure/item/R$string;->dashboard_taxes_url:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 254
    sget v1, Lcom/squareup/configure/item/R$string;->dashboard:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getConnectedScale()Lcom/squareup/scales/ScaleTracker$HardwareScale;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-object v0
.end method

.method public final getHasCompatibleScaleReading()Z
    .locals 1

    .line 225
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasCompatibleScaleReading:Z

    return v0
.end method

.method public final getHasConnectedScale()Z
    .locals 1

    .line 217
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    return v0
.end method

.method public final getHasDisplayedScaleReading()Z
    .locals 1

    .line 231
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    return v0
.end method

.method public final getHasScaleError()Z
    .locals 1

    .line 220
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasScaleError:Z

    return v0
.end method

.method public final getOrCreateSelectedModifierList(I)Ljava/util/SortedMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation

    .line 1504
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedModifiers()Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    if-nez v0, :cond_0

    .line 1509
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    check-cast v0, Ljava/util/SortedMap;

    .line 1511
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedModifiers()Ljava/util/SortedMap;

    move-result-object v1

    const-string v2, "scopeRunner.state\n            .selectedModifiers"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Map;

    .line 1512
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 1511
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public final getVariationByIndex(I)Lcom/squareup/checkout/OrderVariation;
    .locals 2

    .line 1468
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getVariations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "scopeRunner.state.variations[idx]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/checkout/OrderVariation;

    return-object p1
.end method

.method public final isZeroQuantityAllowed()Z
    .locals 1

    .line 296
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final onCancelSelected()V
    .locals 2

    .line 1435
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/configure/item/ConfigureItemHost;->onCancelSelected(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1436
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 1437
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getShouldLogScaleEvent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    if-eqz v0, :cond_0

    .line 1438
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->logWeighedItemizationCanceled()V

    .line 1440
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    :cond_1
    return-void
.end method

.method public final onCommitSelected()V
    .locals 4

    .line 1396
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    const-string v1, "scopeRunner.state.quantity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1397
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onDeleteButtonClicked()V

    return-void

    .line 1404
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1405
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateQuantityEntryType()V

    .line 1408
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->commit()Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v1, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_6

    .line 1422
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 1423
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getShouldLogScaleEvent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    if-eqz v0, :cond_2

    .line 1424
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->logWeighedItemizationAdded()V

    .line 1426
    :cond_2
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->onCommitSuccess(Lcom/squareup/configure/item/ConfigureItemState;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1427
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    goto :goto_0

    .line 1415
    :cond_3
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 1416
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    .line 1417
    sget v2, Lcom/squareup/configure/item/R$string;->gift_card_activation_failed:I

    .line 1418
    sget v3, Lcom/squareup/configure/item/R$string;->gift_card_purchase_limit_exceeded_message:I

    .line 1416
    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v1, Landroid/os/Parcelable;

    .line 1415
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 1409
    :cond_4
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 1410
    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    .line 1411
    sget v2, Lcom/squareup/configure/item/R$string;->configure_item_price_required_dialog_title:I

    .line 1412
    sget v3, Lcom/squareup/configure/item/R$string;->configure_item_price_required_dialog_message:I

    .line 1410
    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v1, Landroid/os/Parcelable;

    .line 1409
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    :cond_5
    :goto_0
    return-void

    .line 1430
    :cond_6
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unrecognized commitResult: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public final onCompButtonClicked()V
    .locals 3

    .line 1023
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    .line 1024
    new-instance v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onCompButtonClicked$1;

    invoke-direct {v2, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onCompButtonClicked$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 1023
    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public final onDeleteButtonClicked()V
    .locals 3

    .line 1445
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1449
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isConfigurationLockedFromTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1450
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDeleteButtonClicked$1;

    invoke-direct {v2, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDeleteButtonClicked$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 1456
    :cond_0
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->doDelete()V

    :goto_0
    return-void

    .line 1446
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot delete a configuring item from the library!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final onDiningOptionSelected(I)V
    .locals 3

    .line 1272
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-eqz v0, :cond_0

    return-void

    .line 1274
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableDiningOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/DiningOption;

    .line 1275
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    if-eqz v0, :cond_1

    .line 1276
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)V

    .line 1277
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->DINING_OPTION_ITEM_LEVEL_CHANGED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 1279
    iput-boolean v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    .line 1280
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildOrUpdateTaxesSection(Lcom/squareup/configure/item/ConfigureItemDetailView;Z)Z

    const/4 p1, 0x0

    .line 1281
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    :cond_1
    return-void
.end method

.method public final onDiscountRowChanged(Ljava/lang/String;Z)V
    .locals 4

    const-string v0, "discountId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1343
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-eqz v0, :cond_0

    return-void

    .line 1346
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availablePerItemDiscountsInOrder:Ljava/util/Map;

    if-nez v0, :cond_1

    const-string v1, "availablePerItemDiscountsInOrder"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1347
    :cond_1
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v1

    const-string v2, "scopeRunner.state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemState;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v1

    const-string v2, "scopeRunner.state.appliedDiscounts"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1345
    invoke-static {v0, v1, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDiscountUtils;->getItemScopedDiscount(Ljava/util/Map;Ljava/util/Map;Ljava/lang/String;Z)Lcom/squareup/checkout/Discount;

    move-result-object v0

    if-eqz p2, :cond_2

    .line 1353
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->passcodeRequired()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1354
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v2, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    .line 1355
    new-instance v3, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    invoke-direct {v3, p0, v0, p2, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Lcom/squareup/checkout/Discount;ZLjava/lang/String;)V

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 1354
    invoke-virtual {v1, v2, v3}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 1379
    :cond_2
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    .line 1381
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1, v2}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProtoNonNull(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v1

    .line 1379
    invoke-virtual {p1, v0, p2, v1}, Lcom/squareup/configure/item/ConfigureItemState;->setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V

    :goto_0
    return-void
.end method

.method public final onEditingItemTitle(Ljava/lang/String;)V
    .locals 2

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1005
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setName(Ljava/lang/String;)V

    .line 1006
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateFirstInvoiceTutorial()V

    return-void
.end method

.method public final onEditingPriceEditText(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string v0, "newAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1010
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->onVariablePriceButtonClicked()V

    .line 1011
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V

    .line 1012
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->itemAmountValidator:Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;

    invoke-interface {v1, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;->validate(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 1013
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateFirstInvoiceTutorial()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 316
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object v1, p0

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 319
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->datePicked()Lrx/Observable;

    move-result-object v0

    .line 320
    new-instance v1, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onEnterScope$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    move-result-object v0

    .line 318
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 380
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object v1, p0

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    return-void
.end method

.method public final onFinalDurationRowClicked()V
    .locals 5

    .line 869
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v3, "scopeRunner.state.intermissions[0]"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/configure/item/ConfigureItemState;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v1

    const-string v3, "scopeRunner.state.duration"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/intermission/IntermissionHelperKt;->getFinalDuration(Lcom/squareup/api/items/Intermission;Lorg/threeten/bp/Duration;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 870
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;->startDurationPicker$default(Lcom/squareup/intermission/IntermissionHelper;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->finalDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public final onFixedPriceOverrideButtonClicked()V
    .locals 1

    .line 999
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->onVariablePriceButtonClicked()V

    .line 1000
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 1001
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goToPriceScreen()V

    return-void
.end method

.method public final onGapDurationRowClicked()V
    .locals 5

    .line 863
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "scopeRunner.state.intermissions[0]"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getGapDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 864
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v0, v1, v3, v4}, Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;->startDurationPicker$default(Lcom/squareup/intermission/IntermissionHelper;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->gapDurationToken:Ljava/lang/Long;

    return-void
.end method

.method public final onGapRowCheckChanged(Z)V
    .locals 3

    .line 847
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v0

    const-string v2, "scopeRunner.state.duration"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->gapTimeToggled(Lorg/threeten/bp/Duration;Z)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/Duration;

    .line 848
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/squareup/configure/item/ConfigureItemState;->setIntermissions(Ljava/util/List;)V

    .line 849
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setDuration(Lorg/threeten/bp/Duration;)V

    .line 851
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateDurationIfService()V

    .line 852
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateIntermissionIfService()V

    .line 854
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_CART:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public final onInitialDurationRowClicked()V
    .locals 5

    .line 858
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->getIntermissions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "scopeRunner.state.intermissions[0]"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/api/items/Intermission;

    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getInitialDuration(Lcom/squareup/api/items/Intermission;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 859
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v0, v1, v3, v4}, Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;->startDurationPicker$default(Lcom/squareup/intermission/IntermissionHelper;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->initialDurationToken:Ljava/lang/Long;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 360
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 361
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateViews()V

    if-nez p1, :cond_0

    .line 364
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    const-string v0, "scopeRunner.state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->isConfigurationLockedFromTicket()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 365
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_VIEWED_LOCKED_ITEMIZATION:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 371
    :cond_0
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isWorkingItem()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 372
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onLoad$1;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onLoad$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_1
    return-void
.end method

.method public final onModifierSelected(II)V
    .locals 7

    .line 1221
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-eqz v0, :cond_0

    return-void

    .line 1223
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getModifierListForOrdinal(I)Lcom/squareup/checkout/OrderModifierList;

    move-result-object v0

    .line 1224
    invoke-direct {p0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getModifierForOrdinal(II)Lcom/squareup/checkout/OrderModifier;

    move-result-object v1

    .line 1225
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getOrCreateSelectedModifierList(I)Ljava/util/SortedMap;

    move-result-object v2

    .line 1229
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v3, :cond_3

    .line 1230
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderModifierList;->useMinMaxSelection()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderModifierList;->getMinSelectedModifiers()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderModifierList;->getMaxSelectedModifiers()I

    move-result v3

    if-ne v3, v5, :cond_1

    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 1239
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    .line 1241
    invoke-interface {v2}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v3

    const-string v6, "selectedModifierList.firstKey()"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    .line 1239
    invoke-virtual {v0, p1, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setModifierUnchecked(II)V

    .line 1243
    check-cast v2, Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1244
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderModifierList;->useMinMaxSelection()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderModifierList;->getMaxSelectedModifiers()I

    move-result v0

    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v3

    if-gt v0, v3, :cond_2

    .line 1249
    iput-boolean v5, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    .line 1250
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setModifierUnchecked(II)V

    .line 1251
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showShakeAnimationOnModifier(II)V

    .line 1252
    iput-boolean v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    goto :goto_0

    .line 1254
    :cond_2
    check-cast v2, Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1260
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    const-string p2, "scopeRunner.state"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedModifiers()Ljava/util/SortedMap;

    move-result-object p1

    .line 1261
    invoke-interface {p1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object p1

    const-string p2, "selectedModifiers.values"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 1874
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 1881
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1882
    check-cast v0, Ljava/util/SortedMap;

    .line 1262
    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1883
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 1885
    :cond_4
    check-cast p2, Ljava/util/List;

    check-cast p2, Ljava/lang/Iterable;

    .line 1886
    instance-of p1, p2, Ljava/util/Collection;

    if-eqz p1, :cond_5

    move-object p1, p2

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_2

    .line 1887
    :cond_5
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/OrderModifier;

    .line 1263
    invoke-virtual {p2}, Lcom/squareup/checkout/OrderModifier;->getHideFromCustomer()Z

    move-result p2

    if-eqz p2, :cond_6

    const/4 v4, 0x1

    .line 1265
    :cond_7
    :goto_2
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1, v4}, Lcom/squareup/configure/item/ConfigureItemState;->cacheHasHiddenModifier(Z)V

    .line 1267
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarPrimaryButtonState()V

    .line 1268
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarText()V

    return-void
.end method

.method public final onNoteChanged(Ljava/lang/String;)V
    .locals 2

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1286
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-eqz v0, :cond_0

    return-void

    .line 1288
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setNote(Ljava/lang/String;)V

    return-void
.end method

.method public final onQuantityChanged(I)V
    .locals 2

    .line 1292
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-boolean v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->isRestoringState:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1294
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantity(Ljava/math/BigDecimal;)V

    .line 1295
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarText()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final onScaleDisplayEventReceived(Lcom/squareup/configure/item/ScaleDisplayEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1644
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->cacheConnectedScale(Lcom/squareup/configure/item/ScaleDisplayEvent;)V

    .line 1645
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->logIfDeviceErrorDebounced(Lcom/squareup/configure/item/ScaleDisplayEvent;)V

    .line 1647
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->lastReceivedScaleDisplayEvent:Lcom/squareup/configure/item/ScaleDisplayEvent;

    .line 1648
    instance-of v0, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$NoConnectedScale;

    xor-int/lit8 v1, v0, 0x1

    iput-boolean v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    .line 1649
    instance-of v1, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$DeviceError;

    iput-boolean v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasScaleError:Z

    .line 1652
    instance-of v2, p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    if-eqz v2, :cond_0

    check-cast p1, Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;

    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->maybeDisplayScaleQuantity(Lcom/squareup/configure/item/ScaleDisplayEvent$StableReading;)V

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    .line 1653
    :goto_0
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->clearScaleQuantity()V

    .line 1656
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateQuantityFooter()V

    return-void
.end method

.method public final onSelectedVariationClicked(I)Z
    .locals 2

    .line 1057
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->onSelectedVariationClicked()V

    .line 1058
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getVariationByIndex(I)Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 1060
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1061
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->cacheCurrentVariationAndVariablePrice()V

    .line 1062
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 1063
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {p1}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goToPriceScreen()V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public final onStartVisualTransition()V
    .locals 3

    .line 1476
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->onStartVisualTransition(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    return-void
.end method

.method public final onTaxRowChanged(Landroid/widget/Checkable;Ljava/lang/String;Z)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1315
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-eqz v0, :cond_0

    return-void

    .line 1317
    :cond_0
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/String;ZLandroid/widget/Checkable;)V

    .line 1332
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1333
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object p2, Lcom/squareup/permissions/Permission;->EDIT_TAXES_IN_SALE:Lcom/squareup/permissions/Permission;

    check-cast v0, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 1335
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->success()V

    :goto_0
    return-void
.end method

.method public final onUncompButtonClicked()V
    .locals 2

    .line 1035
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->uncompItem()V

    .line 1036
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_ITEMIZATION_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1037
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->exit()V

    return-void
.end method

.method public final onUnitQuantityChanged(Ljava/math/BigDecimal;)V
    .locals 2

    const-string v0, "quantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1299
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-boolean v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->isRestoringState:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1301
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setQuantity(Ljava/math/BigDecimal;)V

    .line 1303
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1304
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateQuantityFooter()V

    .line 1306
    :cond_1
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarPrimaryButtonState()V

    .line 1307
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarText()V

    :cond_2
    :goto_0
    return-void
.end method

.method public final onVariablePriceButtonClicked()V
    .locals 1

    .line 988
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->onVariablePriceButtonClicked()V

    .line 989
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->cacheCurrentVariationAndVariablePrice()V

    .line 990
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 991
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goToPriceScreen()V

    return-void
.end method

.method public final onVariationCheckChanged(II)V
    .locals 6

    .line 1091
    iget-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    if-eqz v0, :cond_0

    return-void

    .line 1092
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemHost;->onVariationCheckChanged()V

    .line 1094
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getVariationByIndex(I)Lcom/squareup/checkout/OrderVariation;

    move-result-object p1

    .line 1097
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->cacheCurrentVariationAndVariablePrice()V

    .line 1099
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    const-string v1, "scopeRunner.state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setSelectedVariation(Lcom/squareup/checkout/OrderVariation;)V

    .line 1101
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 1103
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 1104
    new-instance v3, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$1;

    invoke-direct {v3, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$1;-><init>(Lcom/squareup/checkout/OrderVariation;)V

    check-cast v3, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v3}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    .line 1107
    new-instance v3, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$2;

    invoke-direct {v3, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$2;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v3, Lrx/functions/Action1;

    invoke-virtual {v0, v3}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 1117
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1118
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->showFixedPriceOverrideField()V

    .line 1119
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    const-string/jumbo v3, "view"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateFixedPriceOverrideField(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    goto :goto_0

    .line 1121
    :cond_1
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hideFixedPriceOverrideField()V

    .line 1124
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v3

    xor-int/2addr v3, v2

    invoke-virtual {v0, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setDurationRowVisibility(Z)V

    .line 1127
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1129
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/configure/item/ConfigureItemState;->setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V

    .line 1130
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 1131
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->navigator:Lcom/squareup/configure/item/ConfigureItemNavigator;

    invoke-interface {p1}, Lcom/squareup/configure/item/ConfigureItemNavigator;->goToPriceScreen()V

    return-void

    :cond_3
    const/4 v0, -0x1

    if-ne p2, v0, :cond_4

    .line 1140
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p2

    goto :goto_1

    .line 1142
    :cond_4
    invoke-virtual {p0, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getVariationByIndex(I)Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    .line 1143
    invoke-static {v0, p1}, Lcom/squareup/configure/item/ConfigureItemScreensKt;->shouldClearQuantity(Lcom/squareup/checkout/OrderVariation;Lcom/squareup/checkout/OrderVariation;)Z

    move-result v1

    .line 1147
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1148
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/configure/item/ConfigureItemDetailView;

    .line 1150
    iget-object v4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/configure/item/R$string;->item_library_variable_price:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1151
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    const-string v5, "previouslySelectedVariation.unitAbbreviation"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1150
    invoke-static {v4, v0}, Lcom/squareup/quantity/ItemQuantities;->formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 1148
    invoke-virtual {v3, p2, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->setVariation(ILjava/lang/CharSequence;)V

    :cond_5
    move p2, v1

    .line 1157
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/squareup/configure/item/ConfigureItemDetailView;->updateModifiers(Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)V

    if-eqz p2, :cond_9

    .line 1160
    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-direct {p0, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isNonUnitPricedService(Lcom/squareup/configure/item/ConfigureItemScopeRunner;)Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideAllQuantityRows()V

    goto :goto_2

    .line 1161
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/configure/item/ConfigureItemDetailView;

    .line 1162
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    .line 1163
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result p1

    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->shouldFocusOnUnitQuantityRow()Z

    move-result v1

    .line 1161
    invoke-virtual {p2, v0, p1, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showUnitQuantityRow(Ljava/lang/String;IZ)V

    goto :goto_2

    .line 1165
    :cond_7
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->showQuantityRow()V

    .line 1170
    :goto_2
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->lastReceivedScaleDisplayEvent:Lcom/squareup/configure/item/ScaleDisplayEvent;

    if-eqz p1, :cond_8

    if-eqz p1, :cond_9

    .line 1171
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onScaleDisplayEventReceived(Lcom/squareup/configure/item/ScaleDisplayEvent;)V

    goto :goto_3

    .line 1173
    :cond_8
    iput-boolean v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    .line 1174
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->setDefaultQuantity()V

    const/4 p1, 0x0

    .line 1175
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->ignoreListenerChanges:Z

    .line 1179
    :cond_9
    :goto_3
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarText()V

    .line 1180
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateActionBarPrimaryButtonState()V

    .line 1181
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateDurationIfService()V

    .line 1182
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->updateQuantitySectionHeader()V

    return-void
.end method

.method public final onVoidButtonClicked()V
    .locals 3

    .line 1041
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;

    invoke-direct {v2, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVoidButtonClicked$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public final setAvailableCartTaxesInOrder(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;)V"
        }
    .end annotation

    .line 213
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availableCartTaxesInOrder:Ljava/util/Map;

    return-void
.end method

.method public final setAvailablePerItemDiscountsInOrder(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->availablePerItemDiscountsInOrder:Ljava/util/Map;

    return-void
.end method

.method public final setConnectedScale(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->connectedScale:Lcom/squareup/scales/ScaleTracker$HardwareScale;

    return-void
.end method

.method public final setHasCompatibleScaleReading(Z)V
    .locals 0

    .line 225
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasCompatibleScaleReading:Z

    return-void
.end method

.method public final setHasConnectedScale(Z)V
    .locals 0

    .line 217
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasConnectedScale:Z

    return-void
.end method

.method public final setHasDisplayedScaleReading(Z)V
    .locals 0

    .line 231
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasDisplayedScaleReading:Z

    return-void
.end method

.method public final setHasScaleError(Z)V
    .locals 0

    .line 220
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->hasScaleError:Z

    return-void
.end method
