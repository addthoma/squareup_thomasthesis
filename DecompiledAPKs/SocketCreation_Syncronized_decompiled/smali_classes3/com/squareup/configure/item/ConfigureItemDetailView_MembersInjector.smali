.class public final Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;
.super Ljava/lang/Object;
.source "ConfigureItemDetailView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/configure/item/ConfigureItemDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/configure/item/ConfigureItemDetailView;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectCurrencyCode(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectLocaleProvider(Lcom/squareup/configure/item/ConfigureItemDetailView;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/ConfigureItemDetailView;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 85
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public static injectScopeRunner(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/configure/item/ConfigureItemScopeRunner;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->injectScopeRunner(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/configure/item/ConfigureItemScopeRunner;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->injectPresenter(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->injectCurrencyCode(Lcom/squareup/configure/item/ConfigureItemDetailView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->injectLocaleProvider(Lcom/squareup/configure/item/ConfigureItemDetailView;Ljavax/inject/Provider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailView_MembersInjector;->injectMembers(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    return-void
.end method
