.class public interface abstract Lcom/squareup/configure/item/ConfigureItemHost;
.super Ljava/lang/Object;
.source "ConfigureItemHost.java"


# virtual methods
.method public abstract commitCartItem(Lcom/squareup/checkout/CartItem;)V
.end method

.method public abstract compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V
.end method

.method public abstract getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAvailableTaxRules()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAvailableTaxes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;
.end method

.method public abstract getDiningOptions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getGiftCardTotal()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getOrderItem(I)Lcom/squareup/checkout/CartItem;
.end method

.method public abstract getOrderItemCopy(I)Lcom/squareup/checkout/CartItem;
.end method

.method public abstract hasTicket()Z
.end method

.method public abstract isVoidCompAllowed()Z
.end method

.method public abstract onCancelSelected(Z)Z
.end method

.method public abstract onCommit(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
.end method

.method public abstract onCommitSuccess(Lcom/squareup/configure/item/ConfigureItemState;Z)Z
.end method

.method public abstract onDeleteSelected(Lcom/squareup/configure/item/ConfigureItemState;)Z
.end method

.method public abstract onSelectedVariationClicked()V
.end method

.method public abstract onStartVisualTransition(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
.end method

.method public abstract onVariablePriceButtonClicked()V
.end method

.method public abstract onVariationCheckChanged()V
.end method

.method public abstract removeItem(I)V
.end method

.method public abstract replaceItem(ILcom/squareup/checkout/CartItem;)V
.end method

.method public abstract uncompItem(I)V
.end method

.method public abstract voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V
.end method
