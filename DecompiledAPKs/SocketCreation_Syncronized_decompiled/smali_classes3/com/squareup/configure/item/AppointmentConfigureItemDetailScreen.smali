.class public Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;
.super Lcom/squareup/configure/item/InConfigureItemScope;
.source "AppointmentConfigureItemDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/configure/item/-$$Lambda$AppointmentConfigureItemDetailScreen$4p7kDCIPjkb7zM2M_nMrFe4fQ1Y;->INSTANCE:Lcom/squareup/configure/item/-$$Lambda$AppointmentConfigureItemDetailScreen$4p7kDCIPjkb7zM2M_nMrFe4fQ1Y;

    .line 68
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;
    .locals 1

    .line 69
    const-class v0, Lcom/squareup/configure/item/ConfigureItemScope;

    .line 70
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/configure/item/ConfigureItemScope;

    .line 71
    new-instance v0, Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 48
    invoke-super {p0, p1, p2}, Lcom/squareup/configure/item/InConfigureItemScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/configure/item/AppointmentConfigureItemDetailScreen;->configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 44
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 40
    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_detail_view:I

    return v0
.end method
