.class final Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;
.super Lkotlin/jvm/internal/Lambda;
.source "JediWorkflowScreenCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jedi/JediWorkflowScreenCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;->this$0:Lcom/squareup/jedi/JediWorkflowScreenCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;->this$0:Lcom/squareup/jedi/JediWorkflowScreenCoordinator;

    invoke-static {v0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->access$getMessagingController$p(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)Lcom/squareup/ui/help/MessagingController;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/help/MessagingController;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6$1;

    invoke-direct {v1, p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6$1;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "messagingController.badg\u2026nCount(count)\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$attach$6;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
