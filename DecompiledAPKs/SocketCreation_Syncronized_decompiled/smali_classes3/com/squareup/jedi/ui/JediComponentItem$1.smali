.class final Lcom/squareup/jedi/ui/JediComponentItem$1;
.super Ljava/lang/Object;
.source "JediComponentItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/ui/JediComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/jedi/ui/JediComponentItem;
    .locals 1

    .line 87
    new-instance v0, Lcom/squareup/jedi/ui/JediComponentItem;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/jedi/service/Component;

    invoke-direct {v0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 84
    invoke-virtual {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/jedi/ui/JediComponentItem;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/jedi/ui/JediComponentItem;
    .locals 0

    .line 92
    new-array p1, p1, [Lcom/squareup/jedi/ui/JediComponentItem;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 84
    invoke-virtual {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem$1;->newArray(I)[Lcom/squareup/jedi/ui/JediComponentItem;

    move-result-object p1

    return-object p1
.end method
