.class public Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediSectionHeaderComponentItem.java"


# instance fields
.field private padding:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;Ljava/lang/Boolean;)V
    .locals 1

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 14
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->SECTION_HEADER:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 15
    iput-object p2, p0, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;->padding:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public applyPadding()Ljava/lang/Boolean;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;->padding:Ljava/lang/Boolean;

    return-object v0
.end method

.method public label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 19
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
