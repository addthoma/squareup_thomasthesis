.class public Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediParagraphComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 38
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method static synthetic lambda$emphasis$0(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;
    .locals 0

    .line 54
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;->access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public emphasis()Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;
    .locals 3

    .line 54
    sget-object v0, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;->MEDIUM:Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediParagraphComponentItem$0ZQwwTVktbjttfIFFKD4-YpPOAc;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$JediParagraphComponentItem$0ZQwwTVktbjttfIFFKD4-YpPOAc;

    const-string v2, "emphasis"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;

    return-object v0
.end method

.method public htmlText()Ljava/lang/CharSequence;
    .locals 4

    .line 46
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->text()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/text/html/TagParser;

    new-instance v2, Lcom/squareup/text/html/ListTagParser;

    invoke-direct {v2}, Lcom/squareup/text/html/ListTagParser;-><init>()V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/text/html/HtmlText;->fromHtml(Ljava/lang/String;[Lcom/squareup/text/html/TagParser;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public htmlTextWithoutLinks()Ljava/lang/CharSequence;
    .locals 4

    .line 50
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->text()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/text/html/TagParser;

    new-instance v2, Lcom/squareup/text/html/ListTagParser;

    invoke-direct {v2}, Lcom/squareup/text/html/ListTagParser;-><init>()V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;

    invoke-direct {v2}, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;-><init>()V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/text/html/HtmlText;->fromHtml(Ljava/lang/String;[Lcom/squareup/text/html/TagParser;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public isHtml()Z
    .locals 2

    const/4 v0, 0x0

    .line 58
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "is_html"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public text()Ljava/lang/String;
    .locals 1

    const-string v0, "text"

    .line 42
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
