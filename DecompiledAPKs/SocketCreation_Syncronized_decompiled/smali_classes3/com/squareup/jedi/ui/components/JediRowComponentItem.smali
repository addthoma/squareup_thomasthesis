.class public Lcom/squareup/jedi/ui/components/JediRowComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediRowComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;,
        Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    }
.end annotation


# instance fields
.field private final canCall:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;Z)V
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 63
    iput-boolean p2, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canCall:Z

    .line 64
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object p2, Lcom/squareup/protos/jedi/service/ComponentKind;->ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method static synthetic lambda$nextPanelViewType$1(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
    .locals 0

    .line 133
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$valueType$0(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    .locals 0

    .line 88
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->access$100(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    move-result-object p0

    return-object p0
.end method

.method private nextPanelViewType()Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
    .locals 3

    .line 132
    sget-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItem$IDrP62xYC9r4BIV4hUli3GN-PIY;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItem$IDrP62xYC9r4BIV4hUli3GN-PIY;

    const-string v2, "next_panel_view"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    return-object v0
.end method


# virtual methods
.method public actionable()Z
    .locals 2

    .line 108
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v1, "actionable"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getBooleanParameterOrDefault(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public canCallPhone()Z
    .locals 2

    .line 104
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->valueType()Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    move-result-object v0

    sget-object v1, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->PHONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canCall:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canMessageUs()Z
    .locals 2

    .line 128
    invoke-direct {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->nextPanelViewType()Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    move-result-object v0

    sget-object v1, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->MESSAGING:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public chevronVisibility()Lcom/squareup/marin/widgets/ChevronVisibility;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    return-object v0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    return-object v0

    .line 100
    :cond_1
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    return-object v0
.end method

.method public hasSublabel()Z
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->sublabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->url()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasValuelabel()Z
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->valuelabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isSearchResult()Z
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->linkPanelToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 68
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public linkPanelToken()Ljava/lang/String;
    .locals 1

    const-string v0, "link_panel_token"

    .line 124
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public sublabel()Ljava/lang/String;
    .locals 1

    const-string v0, "sub_label"

    .line 76
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public url()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "url"

    .line 116
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public valueType()Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    .locals 3

    .line 88
    sget-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItem$eCqF16SZ1Vtal1zuFRGNqGlARQ8;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItem$eCqF16SZ1Vtal1zuFRGNqGlARQ8;

    const-string/jumbo v2, "value_type"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    return-object v0
.end method

.method public valuelabel()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "value_label"

    .line 84
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
