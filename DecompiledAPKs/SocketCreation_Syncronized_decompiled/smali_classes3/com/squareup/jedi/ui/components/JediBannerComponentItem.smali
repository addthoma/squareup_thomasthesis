.class public Lcom/squareup/jedi/ui/components/JediBannerComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediBannerComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 36
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method

.method static synthetic lambda$state$0(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;
    .locals 0

    .line 44
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;->access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 40
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public state()Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;
    .locals 3

    .line 44
    sget-object v0, Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;->INFO:Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;

    sget-object v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediBannerComponentItem$29UYEquJNlYr-vjkSDkKPxU_IXA;->INSTANCE:Lcom/squareup/jedi/ui/components/-$$Lambda$JediBannerComponentItem$29UYEquJNlYr-vjkSDkKPxU_IXA;

    const-string v2, "state"

    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;->getParameterOrDefault(Ljava/lang/String;Ljava/lang/Object;Lrx/functions/Func1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;

    return-object v0
.end method
