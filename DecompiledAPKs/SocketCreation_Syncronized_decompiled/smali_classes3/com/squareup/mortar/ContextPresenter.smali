.class public Lcom/squareup/mortar/ContextPresenter;
.super Lmortar/Presenter;
.source "ContextPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/squareup/mortar/HasContext;",
        ">",
        "Lmortar/Presenter<",
        "TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/mortar/HasContext;)Lmortar/bundler/BundleService;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    .line 8
    invoke-interface {p1}, Lcom/squareup/mortar/HasContext;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/mortar/HasContext;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/ContextPresenter;->extractBundleService(Lcom/squareup/mortar/HasContext;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method
