.class public final Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "RealNotificationCenterWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        ">;",
        "Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00030\u0002B\'\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0010H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;",
        "Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "viewFactory",
        "Lcom/squareup/notificationcenter/NotificationCenterViewFactory;",
        "workflow",
        "Lcom/squareup/notificationcenter/NotificationCenterWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "notificationCenterResultRunner",
        "Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;",
        "(Lcom/squareup/notificationcenter/NotificationCenterViewFactory;Lcom/squareup/notificationcenter/NotificationCenterWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;)V",
        "getWorkflow",
        "()Lcom/squareup/notificationcenter/NotificationCenterWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final notificationCenterResultRunner:Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;

.field private final workflow:Lcom/squareup/notificationcenter/NotificationCenterWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenter/NotificationCenterViewFactory;Lcom/squareup/notificationcenter/NotificationCenterWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationCenterResultRunner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object v0, Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;->Companion:Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->workflow:Lcom/squareup/notificationcenter/NotificationCenterWorkflow;

    iput-object p3, p0, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p4, p0, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->notificationCenterResultRunner:Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;

    return-void
.end method

.method public static final synthetic access$getNotificationCenterResultRunner$p(Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;)Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->notificationCenterResultRunner:Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/notificationcenter/NotificationCenterWorkflow;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->workflow:Lcom/squareup/notificationcenter/NotificationCenterWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->getWorkflow()Lcom/squareup/notificationcenter/NotificationCenterWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 29
    invoke-virtual {p0}, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n        .subs\u2026sultRunner.onResult(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public startWorkflow()V
    .locals 0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/notificationcenterlauncher/RealNotificationCenterWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
