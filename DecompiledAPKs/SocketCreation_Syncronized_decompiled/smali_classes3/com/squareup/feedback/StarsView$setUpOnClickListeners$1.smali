.class final Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;
.super Ljava/lang/Object;
.source "StarsView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/feedback/StarsView;->setUpOnClickListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $childCount:I

.field final synthetic $i:I

.field final synthetic this$0:Lcom/squareup/feedback/StarsView;


# direct methods
.method constructor <init>(Lcom/squareup/feedback/StarsView;II)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->this$0:Lcom/squareup/feedback/StarsView;

    iput p2, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->$childCount:I

    iput p3, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->$i:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .line 81
    iget p1, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->$childCount:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    .line 82
    iget-object v2, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->this$0:Lcom/squareup/feedback/StarsView;

    invoke-virtual {v2, v1}, Lcom/squareup/feedback/StarsView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "getChildAt(j)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v3, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->$i:I

    if-gt v1, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    :cond_1
    iget-object p1, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->this$0:Lcom/squareup/feedback/StarsView;

    invoke-static {p1}, Lcom/squareup/feedback/StarsView;->access$getStarsSelectedRelay$p(Lcom/squareup/feedback/StarsView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/feedback/StarsView$setUpOnClickListeners$1;->this$0:Lcom/squareup/feedback/StarsView;

    invoke-static {v0}, Lcom/squareup/feedback/StarsView;->access$getNumStarsSelected(Lcom/squareup/feedback/StarsView;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
