.class public final Lcom/squareup/feedback/PlayStoreLauncher_Factory;
.super Ljava/lang/Object;
.source "PlayStoreLauncher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/feedback/PlayStoreLauncher;",
        ">;"
    }
.end annotation


# instance fields
.field private final intentAvailabilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final playStoreIntentCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/PlayStoreIntentCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/PlayStoreIntentCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/feedback/PlayStoreLauncher_Factory;->playStoreIntentCreatorProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/feedback/PlayStoreLauncher_Factory;->intentAvailabilityManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/feedback/PlayStoreLauncher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/PlayStoreIntentCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;)",
            "Lcom/squareup/feedback/PlayStoreLauncher_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/feedback/PlayStoreLauncher_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/feedback/PlayStoreLauncher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/feedback/PlayStoreIntentCreator;Lcom/squareup/util/IntentAvailabilityManager;)Lcom/squareup/feedback/PlayStoreLauncher;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/feedback/PlayStoreLauncher;

    invoke-direct {v0, p0, p1}, Lcom/squareup/feedback/PlayStoreLauncher;-><init>(Lcom/squareup/feedback/PlayStoreIntentCreator;Lcom/squareup/util/IntentAvailabilityManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/feedback/PlayStoreLauncher;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/feedback/PlayStoreLauncher_Factory;->playStoreIntentCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/feedback/PlayStoreIntentCreator;

    iget-object v1, p0, Lcom/squareup/feedback/PlayStoreLauncher_Factory;->intentAvailabilityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/IntentAvailabilityManager;

    invoke-static {v0, v1}, Lcom/squareup/feedback/PlayStoreLauncher_Factory;->newInstance(Lcom/squareup/feedback/PlayStoreIntentCreator;Lcom/squareup/util/IntentAvailabilityManager;)Lcom/squareup/feedback/PlayStoreLauncher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/feedback/PlayStoreLauncher_Factory;->get()Lcom/squareup/feedback/PlayStoreLauncher;

    move-result-object v0

    return-object v0
.end method
