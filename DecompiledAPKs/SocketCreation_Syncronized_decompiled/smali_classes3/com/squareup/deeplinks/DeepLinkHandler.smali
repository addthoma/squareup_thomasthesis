.class public interface abstract Lcom/squareup/deeplinks/DeepLinkHandler;
.super Ljava/lang/Object;
.source "DeepLinkHandler.java"


# static fields
.field public static final ACTIVATE_DEEPLINK_PATH:Ljava/lang/String; = "/activate"

.field public static final APPOINTMENTS_DEEPLINK_HOST:Ljava/lang/String; = "appointments"

.field public static final DEEPLINK_URL_FORMAT:Ljava/lang/String; = "%s://%s%s"

.field public static final SETTINGS_DEEPLINK_HOST:Ljava/lang/String; = "settings"

.field public static final SQUARE_DEEPLINK_SCHEME:Ljava/lang/String; = "square-register"


# virtual methods
.method public abstract handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
.end method
