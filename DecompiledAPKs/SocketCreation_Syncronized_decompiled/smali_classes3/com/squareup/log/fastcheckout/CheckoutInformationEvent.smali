.class public interface abstract Lcom/squareup/log/fastcheckout/CheckoutInformationEvent;
.super Ljava/lang/Object;
.source "CheckoutInformationEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/log/fastcheckout/CheckoutInformationEvent;",
        "",
        "TenderType",
        "feature-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
