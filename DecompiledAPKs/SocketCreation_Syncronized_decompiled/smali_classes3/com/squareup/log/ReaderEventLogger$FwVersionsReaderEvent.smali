.class public Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FwVersionsReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final firmwareComponentVersions:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;)V
    .locals 0

    .line 1167
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1168
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->access$400(Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;->firmwareComponentVersions:Ljava/lang/String;

    return-void
.end method
