.class public final Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;
.super Ljava/lang/Object;
.source "AdvancedModifierLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;)Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    iget-object v2, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;)Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger_Factory;->get()Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    move-result-object v0

    return-object v0
.end method
