.class public Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public firmwareUpdateSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1253
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1253
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;
    .locals 1

    .line 1262
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;)V

    return-object v0
.end method

.method public firmwareUpdateSessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;
    .locals 0

    .line 1257
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareUpdateReaderEvent$Builder;->firmwareUpdateSessionId:Ljava/lang/String;

    return-object p0
.end method
