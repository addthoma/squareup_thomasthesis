.class public Lcom/squareup/log/cart/ItemEvent;
.super Lcom/squareup/log/cart/CartInteractionEvent;
.source "ItemEvent.java"


# instance fields
.field private final modifiers_count:I

.field private final variations_count:I


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;II)V
    .locals 7

    .line 14
    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ITEM:Lcom/squareup/analytics/RegisterTimingName;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/log/cart/CartInteractionEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;)V

    .line 15
    iput p6, p0, Lcom/squareup/log/cart/ItemEvent;->variations_count:I

    .line 16
    iput p7, p0, Lcom/squareup/log/cart/ItemEvent;->modifiers_count:I

    return-void
.end method
