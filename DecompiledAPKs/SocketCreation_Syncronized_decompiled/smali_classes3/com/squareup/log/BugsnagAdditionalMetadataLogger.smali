.class public Lcom/squareup/log/BugsnagAdditionalMetadataLogger;
.super Ljava/lang/Object;
.source "BugsnagAdditionalMetadataLogger.java"

# interfaces
.implements Lcom/squareup/log/CrashAdditionalLogger;


# instance fields
.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method constructor <init>(Lcom/squareup/log/OhSnapLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/log/BugsnagAdditionalMetadataLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-void
.end method


# virtual methods
.method public addLogs(Ljava/lang/Throwable;)V
    .locals 0

    .line 14
    iget-object p1, p0, Lcom/squareup/log/BugsnagAdditionalMetadataLogger;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    invoke-interface {p1}, Lcom/squareup/log/OhSnapLogger;->logExtraDataForCrash()V

    return-void
.end method
