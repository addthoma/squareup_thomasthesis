.class Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DipperEventAndReaderState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/dipper/events/DipperEvent;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final dipperEvent:Lcom/squareup/dipper/events/DipperEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;


# direct methods
.method private constructor <init>(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->dipperEvent:Lcom/squareup/dipper/events/DipperEvent;

    .line 195
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->extractReaderState(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;Lcom/squareup/log/ReaderEventLogger$1;)V
    .locals 0

    .line 178
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;-><init>(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/dipper/events/DipperEvent;
    .locals 0

    .line 178
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->dipperEvent:Lcom/squareup/dipper/events/DipperEvent;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 0

    .line 178
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$DipperEventAndReaderState;->readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-object p0
.end method

.method private extractReaderState(Lcom/squareup/dipper/events/DipperEvent;Ljava/util/Collection;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/Collection<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;"
        }
    .end annotation

    .line 184
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 185
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/dipper/events/BleDevice;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method
