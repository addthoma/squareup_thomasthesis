.class public Lcom/squareup/log/deposits/BankLinkingAttemptEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "BankLinkingAttemptEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "deposits_bank_linking_attempts"


# instance fields
.field private final deposits_bank_linking_attempts_description:Ljava/lang/String;

.field private final deposits_bank_linking_attempts_existing_account_state:Ljava/lang/String;

.field private final deposits_bank_linking_attempts_is_successful:Z

.field private final deposits_bank_linking_attempts_routing_number_changed:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    const-string v0, "deposits_bank_linking_attempts"

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 28
    iput-object p1, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_description:Ljava/lang/String;

    .line 29
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_is_successful:Z

    const/4 p1, 0x0

    .line 30
    iput-object p1, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_routing_number_changed:Ljava/lang/Boolean;

    .line 31
    iput-object p1, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_existing_account_state:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1

    const-string v0, "deposits_bank_linking_attempts"

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_description:Ljava/lang/String;

    .line 21
    iput-boolean p2, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_is_successful:Z

    .line 22
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_routing_number_changed:Ljava/lang/Boolean;

    .line 23
    iput-object p4, p0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;->deposits_bank_linking_attempts_existing_account_state:Ljava/lang/String;

    return-void
.end method
