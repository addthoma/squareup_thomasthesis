.class public Lcom/squareup/log/balance/BizbankImpressionsEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "BizbankImpressionsEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "bizbank_impressions"


# instance fields
.field private final bizbank_impressions_imp_description:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "bizbank_impressions"

    .line 15
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/squareup/log/balance/BizbankImpressionsEvent;->bizbank_impressions_imp_description:Ljava/lang/String;

    return-void
.end method
