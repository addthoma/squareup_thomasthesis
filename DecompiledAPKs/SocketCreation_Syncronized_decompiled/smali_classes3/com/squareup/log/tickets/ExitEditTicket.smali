.class public Lcom/squareup/log/tickets/ExitEditTicket;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ExitEditTicket.java"


# instance fields
.field private final result:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_EXIT_EDIT_TICKET:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 13
    iput-object p1, p0, Lcom/squareup/log/tickets/ExitEditTicket;->result:Ljava/lang/String;

    return-void
.end method
