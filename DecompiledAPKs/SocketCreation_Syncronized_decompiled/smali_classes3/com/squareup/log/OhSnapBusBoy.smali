.class public final Lcom/squareup/log/OhSnapBusBoy;
.super Ljava/lang/Object;
.source "OhSnapBusBoy.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;


# instance fields
.field private final logger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/log/OhSnapLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/log/OhSnapBusBoy;->logger:Lcom/squareup/log/OhSnapLogger;

    return-void
.end method

.method public static synthetic lambda$YyzVZn4EOF7w_dmhaYRkKUqmsWY(Lcom/squareup/log/OhSnapBusBoy;Lcom/squareup/log/OhSnapLoggable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/log/OhSnapBusBoy;->onLoggableEvent(Lcom/squareup/log/OhSnapLoggable;)V

    return-void
.end method

.method private onLoggableEvent(Lcom/squareup/log/OhSnapLoggable;)V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/log/OhSnapBusBoy;->logger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->OTTO_EVENT:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {p1}, Lcom/squareup/log/OhSnapLoggable;->getOhSnapMessage()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 22
    const-class v0, Lcom/squareup/log/OhSnapLoggable;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/log/-$$Lambda$OhSnapBusBoy$YyzVZn4EOF7w_dmhaYRkKUqmsWY;

    invoke-direct {v0, p0}, Lcom/squareup/log/-$$Lambda$OhSnapBusBoy$YyzVZn4EOF7w_dmhaYRkKUqmsWY;-><init>(Lcom/squareup/log/OhSnapBusBoy;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
