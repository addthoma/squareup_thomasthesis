.class public final enum Lcom/squareup/log/OhSnapEvent$Orientation;
.super Ljava/lang/Enum;
.source "OhSnapEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/OhSnapEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/log/OhSnapEvent$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/log/OhSnapEvent$Orientation;

.field public static final enum LANDSCAPE:Lcom/squareup/log/OhSnapEvent$Orientation;

.field public static final enum PORTRAIT:Lcom/squareup/log/OhSnapEvent$Orientation;

.field public static final enum UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 28
    new-instance v0, Lcom/squareup/log/OhSnapEvent$Orientation;

    const/4 v1, 0x0

    const-string v2, "PORTRAIT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/log/OhSnapEvent$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->PORTRAIT:Lcom/squareup/log/OhSnapEvent$Orientation;

    new-instance v0, Lcom/squareup/log/OhSnapEvent$Orientation;

    const/4 v2, 0x1

    const-string v3, "LANDSCAPE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/log/OhSnapEvent$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->LANDSCAPE:Lcom/squareup/log/OhSnapEvent$Orientation;

    new-instance v0, Lcom/squareup/log/OhSnapEvent$Orientation;

    const/4 v3, 0x2

    const-string v4, "UNKNOWN"

    invoke-direct {v0, v4, v3}, Lcom/squareup/log/OhSnapEvent$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/log/OhSnapEvent$Orientation;

    .line 27
    sget-object v4, Lcom/squareup/log/OhSnapEvent$Orientation;->PORTRAIT:Lcom/squareup/log/OhSnapEvent$Orientation;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/log/OhSnapEvent$Orientation;->LANDSCAPE:Lcom/squareup/log/OhSnapEvent$Orientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->$VALUES:[Lcom/squareup/log/OhSnapEvent$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/log/OhSnapEvent$Orientation;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/log/OhSnapEvent$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/log/OhSnapEvent$Orientation;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->$VALUES:[Lcom/squareup/log/OhSnapEvent$Orientation;

    invoke-virtual {v0}, [Lcom/squareup/log/OhSnapEvent$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0
.end method
