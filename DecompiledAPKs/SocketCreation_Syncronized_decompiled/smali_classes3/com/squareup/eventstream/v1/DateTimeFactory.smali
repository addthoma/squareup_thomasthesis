.class Lcom/squareup/eventstream/v1/DateTimeFactory;
.super Ljava/lang/Object;
.source "DateTimeFactory.java"


# instance fields
.field private final DATE_TIME_BUILDER:Lcom/squareup/protos/common/time/DateTime$Builder;

.field private previousOrdinal:J

.field private previousTimeMicros:J


# direct methods
.method constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->DATE_TIME_BUILDER:Lcom/squareup/protos/common/time/DateTime$Builder;

    return-void
.end method

.method private static timezoneOffsetMin(Ljava/util/TimeZone;J)I
    .locals 1

    .line 18
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, p1, p2}, Ljava/util/TimeZone;->getOffset(J)I

    move-result p0

    int-to-long p0, p0

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p0

    long-to-int p1, p0

    return p1
.end method

.method private static tzName(Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 1

    .line 22
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/util/TimeZone;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method currentTimeMillis()J
    .locals 2

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method defaultTimeZone()Ljava/util/TimeZone;
    .locals 1

    .line 54
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method public forMillis(J)Lcom/squareup/protos/common/time/DateTime;
    .locals 7

    .line 32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    .line 34
    monitor-enter p0

    .line 35
    :try_start_0
    iget-wide v2, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->previousTimeMicros:J

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v2

    if-nez v6, :cond_0

    .line 36
    iget-wide v2, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->previousOrdinal:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->previousOrdinal:J

    goto :goto_0

    .line 38
    :cond_0
    iput-wide v0, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->previousTimeMicros:J

    .line 39
    iput-wide v4, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->previousOrdinal:J

    .line 41
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/eventstream/v1/DateTimeFactory;->defaultTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    .line 45
    iget-object v3, p0, Lcom/squareup/eventstream/v1/DateTimeFactory;->DATE_TIME_BUILDER:Lcom/squareup/protos/common/time/DateTime$Builder;

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object v0

    .line 47
    invoke-static {v2, p1, p2}, Lcom/squareup/eventstream/v1/DateTimeFactory;->timezoneOffsetMin(Ljava/util/TimeZone;J)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->timezone_offset_min(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    .line 48
    invoke-static {v2}, Lcom/squareup/eventstream/v1/DateTimeFactory;->tzName(Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DateTime$Builder;->tz_name(Ljava/util/List;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    .line 49
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DateTime$Builder;->ordinal(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 41
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public now()Lcom/squareup/protos/common/time/DateTime;
    .locals 2

    .line 26
    invoke-virtual {p0}, Lcom/squareup/eventstream/v1/DateTimeFactory;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/eventstream/v1/DateTimeFactory;->forMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    return-object v0
.end method
