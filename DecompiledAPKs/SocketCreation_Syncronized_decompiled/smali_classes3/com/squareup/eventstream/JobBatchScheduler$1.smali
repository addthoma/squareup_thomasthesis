.class final Lcom/squareup/eventstream/JobBatchScheduler$1;
.super Ljava/lang/Object;
.source "JobBatchScheduler.kt"

# interfaces
.implements Lcom/evernote/android/job/JobCreator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/JobBatchScheduler;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/UploadBatcher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002*\u0001\u0001\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "com/squareup/eventstream/JobBatchScheduler$1$1",
        "tag",
        "",
        "create",
        "(Ljava/lang/String;)Lcom/squareup/eventstream/JobBatchScheduler$1$1;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $uploadBatcher:Lcom/squareup/eventstream/UploadBatcher;

.field final synthetic this$0:Lcom/squareup/eventstream/JobBatchScheduler;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/JobBatchScheduler;Lcom/squareup/eventstream/UploadBatcher;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler$1;->this$0:Lcom/squareup/eventstream/JobBatchScheduler;

    iput-object p2, p0, Lcom/squareup/eventstream/JobBatchScheduler$1;->$uploadBatcher:Lcom/squareup/eventstream/UploadBatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic create(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/JobBatchScheduler$1;->create(Ljava/lang/String;)Lcom/squareup/eventstream/JobBatchScheduler$1$1;

    move-result-object p1

    check-cast p1, Lcom/evernote/android/job/Job;

    return-object p1
.end method

.method public final create(Ljava/lang/String;)Lcom/squareup/eventstream/JobBatchScheduler$1$1;
    .locals 1

    const-string v0, "tag"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/eventstream/JobBatchScheduler$1;->this$0:Lcom/squareup/eventstream/JobBatchScheduler;

    invoke-static {v0}, Lcom/squareup/eventstream/JobBatchScheduler;->access$getJobCreatorTag$p(Lcom/squareup/eventstream/JobBatchScheduler;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 44
    new-instance p1, Lcom/squareup/eventstream/JobBatchScheduler$1$1;

    invoke-direct {p1, p0}, Lcom/squareup/eventstream/JobBatchScheduler$1$1;-><init>(Lcom/squareup/eventstream/JobBatchScheduler$1;)V

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
