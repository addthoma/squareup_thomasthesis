.class public Lcom/squareup/eventstream/v2/catalog/OsCatalog;
.super Ljava/lang/Object;
.source "OsCatalog.java"


# instance fields
.field private os_build_id:Ljava/lang/String;

.field private os_name:Ljava/lang/String;

.field private os_version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setBuildId(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/OsCatalog;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/OsCatalog;->os_build_id:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/OsCatalog;
    .locals 0

    .line 15
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/OsCatalog;->os_name:Ljava/lang/String;

    return-object p0
.end method

.method public setVersion(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/OsCatalog;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/OsCatalog;->os_version:Ljava/lang/String;

    return-object p0
.end method
