.class Lcom/squareup/eventstream/v2/Es2EventFactory;
.super Ljava/lang/Object;
.source "Es2EventFactory.java"

# interfaces
.implements Lcom/squareup/eventstream/EventFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/eventstream/EventFactory<",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
        ">;"
    }
.end annotation


# instance fields
.field private final appName:Ljava/lang/String;

.field private final appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

.field private final deviceState:Lcom/squareup/eventstream/v2/AndroidEvent;

.field private final display:Landroid/view/Display;

.field private final displayMetrics:Landroid/util/DisplayMetrics;

.field private final jsonSerializer:Lcom/squareup/eventstream/v2/Es2JsonSerializer;


# direct methods
.method constructor <init>(Landroid/view/Display;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Ljava/lang/String;Lcom/squareup/eventstream/v2/AndroidEvent;)V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    .line 35
    new-instance v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    .line 40
    iput-object p1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->display:Landroid/view/Display;

    .line 41
    iput-object p2, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->jsonSerializer:Lcom/squareup/eventstream/v2/Es2JsonSerializer;

    .line 42
    iput-object p3, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appName:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->deviceState:Lcom/squareup/eventstream/v2/AndroidEvent;

    return-void
.end method

.method static create(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v2/Es2EventFactory;
    .locals 4

    .line 133
    new-instance v0, Lcom/squareup/eventstream/v2/AndroidEvent;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/AndroidEvent;-><init>()V

    const-string/jumbo v1, "window"

    .line 136
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 137
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 141
    iget-object v2, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->sessionCatalog:Lcom/squareup/eventstream/v2/catalog/SessionCatalog;

    invoke-virtual {v2, p9}, Lcom/squareup/eventstream/v2/catalog/SessionCatalog;->setToken(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SessionCatalog;

    move-result-object p9

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p9, v2, v3}, Lcom/squareup/eventstream/v2/catalog/SessionCatalog;->setStartTimeMillis(J)Lcom/squareup/eventstream/v2/catalog/SessionCatalog;

    .line 146
    iget-object p9, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->mobileAppCatalog:Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    invoke-virtual {p9, p4}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->setVersionName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    move-result-object p4

    .line 147
    invoke-virtual {p4, p5}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->setVersionCode(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    move-result-object p4

    .line 148
    invoke-virtual {p4, p6}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->setBuildSha(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    move-result-object p4

    iget-object p2, p2, Lcom/squareup/eventstream/BaseEventstream$BuildType;->value:Ljava/lang/String;

    .line 149
    invoke-virtual {p4, p2}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->setBuildType(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    move-result-object p2

    .line 150
    invoke-virtual {p2, p7}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->setInstallationID(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    move-result-object p2

    .line 151
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->setPackageName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    .line 154
    iget-object p2, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->osCatalog:Lcom/squareup/eventstream/v2/catalog/OsCatalog;

    const-string p4, "ANDROID"

    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/OsCatalog;->setName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/OsCatalog;

    move-result-object p2

    sget-object p4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 155
    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/OsCatalog;->setVersion(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/OsCatalog;

    move-result-object p2

    sget-object p4, Landroid/os/Build;->ID:Ljava/lang/String;

    .line 156
    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/OsCatalog;->setBuildId(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/OsCatalog;

    .line 159
    iget-object p2, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->androidDeviceCatalog:Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    .line 160
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p4

    const-string p5, "android_id"

    invoke-static {p4, p5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;->setId(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    move-result-object p2

    sget-object p4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 161
    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;->setVersionIncremental(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    .line 162
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x17

    if-lt p2, p4, :cond_0

    .line 163
    iget-object p2, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->androidDeviceCatalog:Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    sget-object p4, Landroid/os/Build$VERSION;->SECURITY_PATCH:Ljava/lang/String;

    invoke-virtual {p2, p4}, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;->setSecurityPatch(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    .line 167
    :cond_0
    invoke-static {v0, v1}, Lcom/squareup/eventstream/v2/Es2EventFactory;->populateDevice(Lcom/squareup/eventstream/v2/AndroidEvent;Landroid/view/Display;)V

    const-string p2, "phone"

    .line 171
    invoke-virtual {p0, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/telephony/TelephonyManager;

    .line 172
    iget-object p4, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->connectionCatalog:Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;

    invoke-static {p2}, Lcom/squareup/eventstream/v2/Es2EventFactory;->networkOperator(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->setNetworkOperator(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;

    move-result-object p4

    .line 173
    invoke-virtual {p4, p8}, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;->setUserAgent(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;

    .line 176
    iget-object p4, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->uCatalog:Lcom/squareup/eventstream/v2/catalog/UCatalog;

    const-string p5, "android/eventstream2"

    invoke-virtual {p4, p5}, Lcom/squareup/eventstream/v2/catalog/UCatalog;->setLibraryName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/UCatalog;

    move-result-object p4

    const-string/jumbo p5, "unspecified"

    .line 177
    invoke-virtual {p4, p5}, Lcom/squareup/eventstream/v2/catalog/UCatalog;->setLibraryVersion(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/UCatalog;

    .line 180
    invoke-static {p0, v0, p2}, Lcom/squareup/eventstream/v2/Es2EventFactory;->populateSim(Landroid/content/Context;Lcom/squareup/eventstream/v2/AndroidEvent;Landroid/telephony/TelephonyManager;)V

    .line 182
    new-instance p0, Lcom/squareup/eventstream/v2/Es2EventFactory;

    invoke-direct {p0, v1, p3, p1, v0}, Lcom/squareup/eventstream/v2/Es2EventFactory;-><init>(Landroid/view/Display;Lcom/squareup/eventstream/v2/Es2JsonSerializer;Ljava/lang/String;Lcom/squareup/eventstream/v2/AndroidEvent;)V

    return-object p0
.end method

.method private static diagonalBucket(Landroid/util/DisplayMetrics;)I
    .locals 4

    .line 201
    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int v0, v0, v1

    iget v1, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int v1, v1, v2

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iget p0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v2, p0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int p0, v0

    return p0
.end method

.method static networkOperator(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
    .locals 2

    .line 236
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 237
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 238
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static orientation(Landroid/util/DisplayMetrics;)Ljava/lang/String;
    .locals 2

    .line 107
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v0, v1, :cond_0

    const-string p0, "portrait"

    return-object p0

    .line 109
    :cond_0
    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget p0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge v0, p0, :cond_1

    const-string p0, "landscape"

    return-object p0

    :cond_1
    const-string p0, "square"

    return-object p0
.end method

.method private static populateDevice(Lcom/squareup/eventstream/v2/AndroidEvent;Landroid/view/Display;)V
    .locals 2

    .line 186
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 187
    invoke-virtual {p1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 189
    iget-object p0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->deviceCatalog:Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    sget-object p1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setBrand(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    sget-object p1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 190
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setManufacturer(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    sget-object p1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 191
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setModel(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    invoke-static {v0}, Lcom/squareup/eventstream/v2/Es2EventFactory;->diagonalBucket(Landroid/util/DisplayMetrics;)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " inches"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setFormFactor(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    iget p1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 193
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setDensityDpi(I)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    sget-object p1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 194
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setBuildDevice(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    sget-object p1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 195
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setBuildProduct(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    sget-object p1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    .line 196
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setCpuAbi(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object p0

    sget-object p1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    .line 197
    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setCpuAbi2(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    return-void
.end method

.method private static populateSim(Landroid/content/Context;Lcom/squareup/eventstream/v2/AndroidEvent;Landroid/telephony/TelephonyManager;)V
    .locals 1

    const-string v0, "android.permission.READ_PHONE_STATE"

    .line 207
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 208
    :goto_0
    invoke-static {p1, p2, p0}, Lcom/squareup/eventstream/v2/Es2EventFactory;->populateSim(Lcom/squareup/eventstream/v2/AndroidEvent;Landroid/telephony/TelephonyManager;Z)V

    return-void
.end method

.method static populateSim(Lcom/squareup/eventstream/v2/AndroidEvent;Landroid/telephony/TelephonyManager;Z)V
    .locals 4

    .line 214
    iget-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->simCatalog:Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->setCountryIso(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    move-result-object v0

    .line 215
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->setOperatorName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    .line 217
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->simCatalog:Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->setMcc(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    move-result-object v1

    .line 220
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->setMnc(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    :cond_0
    if-eqz p2, :cond_1

    .line 226
    :try_start_0
    iget-object p2, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->simCatalog:Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->setSerialNumber(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 230
    iget-object p0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->simCatalog:Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "error getting sim serial: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->setSerialNumber(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    :cond_1
    :goto_0
    return-void
.end method

.method private serializeJsonData(Lcom/squareup/eventstream/v2/AppEvent;)Ljava/lang/String;
    .locals 3

    .line 65
    invoke-direct {p0}, Lcom/squareup/eventstream/v2/Es2EventFactory;->updateBaseData()V

    .line 66
    iget-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->jsonSerializer:Lcom/squareup/eventstream/v2/Es2JsonSerializer;

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    invoke-virtual {v1}, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->getCommonProperties()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->deviceState:Lcom/squareup/eventstream/v2/AndroidEvent;

    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/eventstream/v2/Es2JsonSerializer;->serializeJsonData(Lcom/squareup/eventstream/v2/AppEvent;Ljava/util/Map;Lcom/squareup/eventstream/v2/AndroidEvent;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private updateBaseData()V
    .locals 4

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->display:Landroid/view/Display;

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    :catch_1
    move-exception v0

    .line 76
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "int[] cannot be cast to android.app.ActivityThread$ActivityClientRecord"

    .line 75
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->deviceState:Lcom/squareup/eventstream/v2/AndroidEvent;

    iget-object v0, v0, Lcom/squareup/eventstream/v2/AndroidEvent;->deviceCatalog:Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    invoke-static {v1}, Lcom/squareup/eventstream/v2/Es2EventFactory;->orientation(Landroid/util/DisplayMetrics;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setOrientation(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-long v1, v1

    .line 85
    invoke-virtual {v0, v1, v2}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setScreenWidth(J)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->displayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-long v1, v1

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setScreenHeight(J)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    iget-object v1, v1, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->advertisingId:Ljava/lang/String;

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->setAdvertisingId(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    .line 89
    iget-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    iget-object v0, v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->deviceState:Lcom/squareup/eventstream/v2/AndroidEvent;

    iget-object v1, v1, Lcom/squareup/eventstream/v2/AndroidEvent;->localeCatalog:Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;->setLanguage(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;

    move-result-object v1

    .line 92
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;->setCountryCode(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    iget-object v0, v0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->location:Landroid/location/Location;

    if-eqz v0, :cond_1

    .line 97
    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->deviceState:Lcom/squareup/eventstream/v2/AndroidEvent;

    iget-object v1, v1, Lcom/squareup/eventstream/v2/AndroidEvent;->coordinateCatalog:Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->setAltitude(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    move-result-object v1

    .line 98
    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->setGeographicAccuracy(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    move-result-object v1

    .line 99
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->setLatitude(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    move-result-object v1

    .line 100
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->setLongitude(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    move-result-object v1

    .line 101
    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->setHeading(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    move-result-object v1

    .line 102
    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->setSpeed(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    :cond_1
    return-void

    .line 77
    :cond_2
    throw v0
.end method


# virtual methods
.method public create(Lcom/squareup/eventstream/v2/AppEvent;J)Lcom/squareup/protos/sawmill/EventstreamV2Event;
    .locals 5

    .line 47
    iget-wide v0, p1, Lcom/squareup/eventstream/v2/AppEvent;->recordedAt:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    iget-object p2, p1, Lcom/squareup/eventstream/v2/AppEvent;->recordedAtTimeUnit:Ljava/util/concurrent/TimeUnit;

    iget-wide v0, p1, Lcom/squareup/eventstream/v2/AppEvent;->recordedAt:J

    .line 48
    invoke-virtual {p2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide p2

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 49
    invoke-virtual {v0, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide p2

    .line 50
    :goto_0
    new-instance v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appName:Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v1}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->app_name(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/eventstream/v2/AppEvent;->catalogName:Ljava/lang/String;

    .line 52
    invoke-virtual {v0, v1}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->catalog_name(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    move-result-object v0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/eventstream/v2/Es2EventFactory;->serializeJsonData(Lcom/squareup/eventstream/v2/AppEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->json_data(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    move-result-object v0

    .line 54
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->recorded_at_usec(Ljava/lang/Long;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/eventstream/v2/AppEvent;->uuid:Ljava/util/UUID;

    .line 55
    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->uuid(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->build()Lcom/squareup/protos/sawmill/EventstreamV2Event;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic create(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/eventstream/v2/Es2EventFactory;->create(Lcom/squareup/eventstream/v2/AppEvent;J)Lcom/squareup/protos/sawmill/EventstreamV2Event;

    move-result-object p1

    return-object p1
.end method

.method public state()Lcom/squareup/eventstream/v2/EventstreamV2$AppState;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/eventstream/v2/Es2EventFactory;->appState:Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    return-object v0
.end method

.method public bridge synthetic state()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/eventstream/v2/Es2EventFactory;->state()Lcom/squareup/eventstream/v2/EventstreamV2$AppState;

    move-result-object v0

    return-object v0
.end method
