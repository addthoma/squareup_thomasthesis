.class public abstract Lcom/squareup/eventstream/v2/AppEvent;
.super Ljava/lang/Object;
.source "AppEvent.java"


# instance fields
.field final transient catalogName:Ljava/lang/String;

.field transient recordedAt:J

.field transient recordedAtTimeUnit:Ljava/util/concurrent/TimeUnit;

.field final transient uuid:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    .line 24
    iput-wide v0, p0, Lcom/squareup/eventstream/v2/AppEvent;->recordedAt:J

    .line 28
    iput-object p1, p0, Lcom/squareup/eventstream/v2/AppEvent;->catalogName:Ljava/lang/String;

    .line 29
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/AppEvent;->uuid:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/UUID;)V
    .locals 2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    .line 24
    iput-wide v0, p0, Lcom/squareup/eventstream/v2/AppEvent;->recordedAt:J

    .line 33
    iput-object p1, p0, Lcom/squareup/eventstream/v2/AppEvent;->catalogName:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/squareup/eventstream/v2/AppEvent;->uuid:Ljava/util/UUID;

    return-void
.end method


# virtual methods
.method public recordedAt(JLjava/util/concurrent/TimeUnit;)V
    .locals 0

    .line 38
    iput-wide p1, p0, Lcom/squareup/eventstream/v2/AppEvent;->recordedAt:J

    .line 39
    iput-object p3, p0, Lcom/squareup/eventstream/v2/AppEvent;->recordedAtTimeUnit:Ljava/util/concurrent/TimeUnit;

    return-void
.end method
