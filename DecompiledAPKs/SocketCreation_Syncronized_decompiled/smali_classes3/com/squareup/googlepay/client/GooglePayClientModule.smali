.class public final Lcom/squareup/googlepay/client/GooglePayClientModule;
.super Ljava/lang/Object;
.source "GooglePayClientModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/googlepay/client/GooglePayClientModule;",
        "",
        "()V",
        "providesGooglePayClient",
        "Lcom/google/android/gms/common/api/GoogleApiClient;",
        "applicationContext",
        "Landroid/app/Application;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/googlepay/client/GooglePayClientModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/googlepay/client/GooglePayClientModule;

    invoke-direct {v0}, Lcom/squareup/googlepay/client/GooglePayClientModule;-><init>()V

    sput-object v0, Lcom/squareup/googlepay/client/GooglePayClientModule;->INSTANCE:Lcom/squareup/googlepay/client/GooglePayClientModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final providesGooglePayClient(Landroid/app/Application;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 1
    .annotation runtime Lcom/squareup/googlepay/client/GooglePayClient;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "applicationContext"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/squareup/googlepay/client/GooglePayClientFactory;->INSTANCE:Lcom/squareup/googlepay/client/GooglePayClientFactory;

    invoke-virtual {v0, p0}, Lcom/squareup/googlepay/client/GooglePayClientFactory;->create(Landroid/app/Application;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object p0

    return-object p0
.end method
