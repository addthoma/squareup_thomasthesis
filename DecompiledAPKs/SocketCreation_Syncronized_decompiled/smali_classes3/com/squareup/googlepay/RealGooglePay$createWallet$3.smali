.class final Lcom/squareup/googlepay/RealGooglePay$createWallet$3;
.super Ljava/lang/Object;
.source "RealGooglePay.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/googlepay/RealGooglePay;->createWallet(Landroid/app/Activity;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activity:Landroid/app/Activity;

.field final synthetic this$0:Lcom/squareup/googlepay/RealGooglePay;


# direct methods
.method constructor <init>(Lcom/squareup/googlepay/RealGooglePay;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;->this$0:Lcom/squareup/googlepay/RealGooglePay;

    iput-object p2, p0, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;->$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lio/reactivex/disposables/Disposable;)V
    .locals 3

    .line 175
    sget-object p1, Lcom/google/android/gms/tapandpay/TapAndPay;->TapAndPay:Lcom/google/android/gms/tapandpay/TapAndPay;

    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;->this$0:Lcom/squareup/googlepay/RealGooglePay;

    invoke-static {v0}, Lcom/squareup/googlepay/RealGooglePay;->access$getApiClient$p(Lcom/squareup/googlepay/RealGooglePay;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;->$activity:Landroid/app/Activity;

    invoke-static {}, Lcom/squareup/googlepay/RealGooglePay;->access$Companion()Lcom/squareup/googlepay/RealGooglePay$Companion;

    const/16 v2, 0x12d

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/tapandpay/TapAndPay;->createWallet(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/app/Activity;I)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, p1}, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;->accept(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
