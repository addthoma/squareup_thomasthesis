.class public final Lcom/squareup/container/CardScreenContainers;
.super Ljava/lang/Object;
.source "CardScreenContainers.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "non-instantiable class"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic access$000(Landroid/view/View;Z)V
    .locals 0

    .line 18
    invoke-static {p0, p1}, Lcom/squareup/container/CardScreenContainers;->setVisible(Landroid/view/View;Z)V

    return-void
.end method

.method public static setCardVisible(Landroid/view/ViewGroup;Landroid/view/View;ZZLflow/TraversalCallback;)V
    .locals 6

    .line 27
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne p2, v0, :cond_1

    .line 29
    invoke-interface {p4}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    :cond_1
    if-nez p3, :cond_2

    .line 34
    invoke-static {p0, p2}, Lcom/squareup/container/CardScreenContainers;->setVisible(Landroid/view/View;Z)V

    xor-int/lit8 p0, p2, 0x1

    .line 35
    invoke-static {p1, p0}, Lcom/squareup/container/CardScreenContainers;->setVisible(Landroid/view/View;Z)V

    .line 36
    invoke-interface {p4}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 40
    :cond_2
    invoke-static {p0, v1}, Lcom/squareup/container/CardScreenContainers;->setVisible(Landroid/view/View;Z)V

    .line 41
    invoke-static {p1, v1}, Lcom/squareup/container/CardScreenContainers;->setVisible(Landroid/view/View;Z)V

    .line 43
    new-instance p3, Landroid/animation/AnimatorSet;

    invoke-direct {p3}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz p2, :cond_3

    .line 45
    sget-object v0, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    sget-object v5, Lcom/squareup/container/spot/Spot$Direction;->FORWARD:Lcom/squareup/container/spot/Spot$Direction;

    move-object v1, p3

    move-object v2, p0

    move-object v3, p0

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/spot/Spot;->addEnterAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V

    .line 47
    sget-object v0, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    sget-object v5, Lcom/squareup/container/spot/Spot$Direction;->FORWARD:Lcom/squareup/container/spot/Spot$Direction;

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/spot/Spot;->addExitAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V

    goto :goto_1

    .line 50
    :cond_3
    sget-object v0, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    sget-object v5, Lcom/squareup/container/spot/Spot$Direction;->BACKWARD:Lcom/squareup/container/spot/Spot$Direction;

    move-object v1, p3

    move-object v2, p0

    move-object v3, p1

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/spot/Spot;->addEnterAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V

    .line 52
    sget-object v0, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    sget-object v5, Lcom/squareup/container/spot/Spot$Direction;->BACKWARD:Lcom/squareup/container/spot/Spot$Direction;

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/spot/Spot;->addExitAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V

    .line 55
    :goto_1
    new-instance v0, Lcom/squareup/container/CardScreenContainers$1;

    invoke-direct {v0, p0, p2, p1, p4}, Lcom/squareup/container/CardScreenContainers$1;-><init>(Landroid/view/ViewGroup;ZLandroid/view/View;Lflow/TraversalCallback;)V

    invoke-virtual {p3, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 66
    invoke-static {p3, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 67
    invoke-virtual {p3}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method private static setVisible(Landroid/view/View;Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    .line 71
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 74
    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 75
    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    :cond_1
    return-void
.end method
