.class public abstract Lcom/squareup/container/ComponentWorkflowV2Runner;
.super Lcom/squareup/container/BaseComponentWorkflowV2Runner;
.source "ComponentWorkflowV2Runner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/container/BaseComponentWorkflowV2Runner<",
        "TC;",
        "Lkotlin/Unit;",
        "TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000 \u001d*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00030\u0004:\u0001\u001dB\u00a7\u0001\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c\u0012\u001c\u0010\r\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f0\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0010\u0012J\u0010\u0011\u001aF\u0012\u0004\u0012\u00028\u0000\u0012<\u0012:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u0001\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0015j\u0002`\u00160\u0013j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0014`\u00170\u00120\u000e\u0012\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0019\u0012\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u001b\u00a2\u0006\u0002\u0010\u001c\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/container/ComponentWorkflowV2Runner;",
        "C",
        "",
        "O",
        "Lcom/squareup/container/BaseComponentWorkflowV2Runner;",
        "",
        "serviceName",
        "",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "componentClass",
        "Ljava/lang/Class;",
        "viewFactoryFactory",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "Lcom/squareup/container/WorkflowComponentViewFactory;",
        "workflowFactory",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "cancelAfterOneResult",
        "",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/ComponentWorkflowV2Runner;->Companion:Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;",
            "Ljava/lang/Class<",
            "TC;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TC;+",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TC;+",
            "Lcom/squareup/workflow/Workflow<",
            "-",
            "Lkotlin/Unit;",
            "+TO;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;Z",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")V"
        }
    .end annotation

    const-string v0, "serviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentClass"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactoryFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflowFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-direct/range {p0 .. p7}, Lcom/squareup/container/BaseComponentWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V

    .line 139
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/container/ComponentWorkflowV2Runner;->setProps(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v7, 0x0

    goto :goto_0

    :cond_0
    move v7, p6

    :goto_0
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_1

    .line 133
    invoke-static {}, Lcom/squareup/container/CoroutineDispatcherKt;->getMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    move-object v8, v0

    goto :goto_1

    :cond_1
    move-object/from16 v8, p7

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/ComponentWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Ljava/lang/Class;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZLkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method

.method public static final get(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/ComponentWorkflowV2Runner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            "I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lmortar/MortarScope;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/container/ComponentWorkflowV2Runner<",
            "TC;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/ComponentWorkflowV2Runner;->Companion:Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/container/ComponentWorkflowV2Runner$Companion;->get(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/ComponentWorkflowV2Runner;

    move-result-object p0

    return-object p0
.end method
