.class public final Lcom/squareup/container/WorkflowBodyKey;
.super Lcom/squareup/container/WorkflowLayoutKey;
.source "WorkflowBodyKey.kt"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/layer/HidesMaster;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/WorkflowBodyKey$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowBodyKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowBodyKey.kt\ncom/squareup/container/WorkflowBodyKey\n+ 2 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,38:1\n150#2:39\n24#3,4:40\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowBodyKey.kt\ncom/squareup/container/WorkflowBodyKey\n*L\n35#1:39\n35#1,4:40\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001d2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001\u001dBK\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u0007H\u0016R\u0014\u0010\u0014\u001a\u00020\u0012X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowBodyKey;",
        "Lcom/squareup/container/WorkflowLayoutKey;",
        "Lcom/squareup/container/spot/HasSpot;",
        "Lcom/squareup/container/layer/HidesMaster;",
        "runnerServiceName",
        "",
        "parent",
        "Lcom/squareup/container/ContainerTreeKey;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "",
        "(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V",
        "hideMaster",
        "getHideMaster",
        "()Z",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "context",
        "Landroid/content/Context;",
        "reparent",
        "newParent",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/WorkflowBodyKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/container/WorkflowBodyKey$Companion;


# instance fields
.field private final hideMaster:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/WorkflowBodyKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/WorkflowBodyKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/WorkflowBodyKey;->Companion:Lcom/squareup/container/WorkflowBodyKey$Companion;

    .line 35
    sget-object v0, Lcom/squareup/container/WorkflowTreeKey;->Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;

    .line 40
    new-instance v0, Lcom/squareup/container/WorkflowBodyKey$$special$$inlined$creator$1;

    invoke-direct {v0}, Lcom/squareup/container/WorkflowBodyKey$$special$$inlined$creator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 43
    sput-object v0, Lcom/squareup/container/WorkflowBodyKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)V"
        }
    .end annotation

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapshot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct/range {p0 .. p7}, Lcom/squareup/container/WorkflowLayoutKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    .line 23
    invoke-virtual {p6}, Lcom/squareup/workflow/ScreenHint;->getHideMaster()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/container/WorkflowBodyKey;->hideMaster:Z

    return-void
.end method


# virtual methods
.method public getHideMaster()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/container/WorkflowBodyKey;->hideMaster:Z

    return v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowBodyKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/ScreenHint;->getSpot()Lcom/squareup/container/spot/Spot;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const-string v0, "Spots.HERE"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowBodyKey;
    .locals 9

    const-string v0, "newParent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/container/WorkflowBodyKey;

    .line 30
    iget-object v2, p0, Lcom/squareup/container/WorkflowBodyKey;->runnerServiceName:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/container/WorkflowBodyKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowBodyKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v5

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowBodyKey;->getProps()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowBodyKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v7

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowBodyKey;->isPersistent()Z

    move-result v8

    move-object v1, v0

    move-object v3, p1

    .line 29
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowBodyKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-object v0
.end method

.method public bridge synthetic reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/container/WorkflowBodyKey;->reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowBodyKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    return-object p1
.end method
