.class public final Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0003H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/container/AbstractPosWorkflowRunner$registerServices$1",
        "Lmortar/Scoped;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 315
    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-virtual {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$registerServices$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    const-string v1, "registered scope ended"

    invoke-static {v0, v1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$dropSpentWorkflow(Lcom/squareup/container/AbstractPosWorkflowRunner;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
