.class final Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/BiPredicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1;->invoke(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiPredicate<",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
        "+TP;+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
        "+TP;+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0004\u0008\u0002\u0010\u000528\u0010\u0006\u001a4\u0012\u0004\u0012\u0002H\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\n\u0012\u0006\u0008\u0001\u0012\u00020\t`\u000c0\u000728\u0010\r\u001a4\u0012\u0004\u0012\u0002H\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\n\u0012\u0006\u0008\u0001\u0012\u00020\t`\u000c0\u0007H\n\u00a2\u0006\u0002\u0008\u000e"
    }
    d2 = {
        "<anonymous>",
        "",
        "P",
        "",
        "O",
        "R",
        "old",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "new",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;

    invoke-direct {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;-><init>()V

    sput-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)Z"
        }
    .end annotation

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p2}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/squareup/workflow/LayeredScreenKt;->matches(Ljava/util/Map;Ljava/util/Map;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    check-cast p2, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/container/AbstractPosWorkflowRunner$showFlowScreens$1$3;->test(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Z

    move-result p1

    return p1
.end method
