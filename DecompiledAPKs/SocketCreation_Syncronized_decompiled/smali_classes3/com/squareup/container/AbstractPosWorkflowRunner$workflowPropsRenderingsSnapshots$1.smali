.class final Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowPropsRenderingsSnapshots()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
        "+TO;+",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
        "TP;TR;>;>;",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
        "+TP;+TR;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001aV\u0012$\u0012\"\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004 \u0005*\u0010\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0018\u00010\u00020\u0002 \u0005**\u0012$\u0012\"\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004 \u0005*\u0010\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0006\"\u0004\u0008\u0002\u0010\u00042 \u0010\u0008\u001a\u001c\u0012\u0004\u0012\u0002H\u0007\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\n0\tH\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "P",
        "R",
        "kotlin.jvm.PlatformType",
        "",
        "O",
        "workflowHost",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;

    invoke-direct {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;-><init>()V

    sput-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "+TO;+",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TP;TR;>;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TP;TR;>;>;"
        }
    .end annotation

    const-string/jumbo v0, "workflowHost"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    invoke-interface {p1}, Lcom/squareup/workflow/rx2/RxWorkflowHost;->getRenderingsAndSnapshots()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;->invoke(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
