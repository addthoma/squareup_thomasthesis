.class final Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflowAdapter$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AbstractV1PosWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractV1PosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;TE;TO;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001ax\u0012\u0004\u0012\u00020\u0002\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0003\u0012\u0004\u0012\u0002H\t\u0012\u0004\u0012\u0002H\n\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u00030\u0001\"\u0008\u0008\u0000\u0010\t*\u00020\u000b\"\u0008\u0008\u0001\u0010\n*\u00020\u000bH\n\u00a2\u0006\u0002\u0008\u000c"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "E",
        "O",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractV1PosWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractV1PosWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflowAdapter$2;->this$0:Lcom/squareup/container/AbstractV1PosWorkflowRunner;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;TE;TO;",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflowAdapter$2;->this$0:Lcom/squareup/container/AbstractV1PosWorkflowRunner;

    invoke-virtual {v0}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->getStarter()Lcom/squareup/container/PosWorkflowStarter;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt;->asV2Workflow(Lcom/squareup/container/PosWorkflowStarter;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflowAdapter$2;->invoke()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object v0

    return-object v0
.end method
