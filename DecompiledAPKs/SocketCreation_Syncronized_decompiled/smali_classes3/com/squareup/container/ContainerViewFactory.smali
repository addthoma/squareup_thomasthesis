.class public interface abstract Lcom/squareup/container/ContainerViewFactory;
.super Ljava/lang/Object;
.source "ContainerViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0001H&J \u0010\u0005\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00070\u00062\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0008\u001a\u00020\tH&J \u0010\n\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u000c\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/container/ContainerViewFactory;",
        "",
        "canRender",
        "",
        "key",
        "dialogForKey",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "viewForKey",
        "Landroid/view/View;",
        "contextForNewView",
        "containerView",
        "Landroid/view/ViewGroup;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract canRender(Ljava/lang/Object;)Z
.end method

.method public abstract dialogForKey(Ljava/lang/Object;Landroid/content/Context;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation
.end method

.method public abstract viewForKey(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method
