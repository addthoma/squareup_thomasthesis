.class public Lcom/squareup/container/layer/DialogLayer;
.super Ljava/lang/Object;
.source "DialogLayer.java"

# interfaces
.implements Lcom/squareup/container/layer/ContainerLayer;


# instance fields
.field private final contextFactory:Lflow/path/PathContextFactory;

.field private dialogReference:Lcom/squareup/container/layer/DialogReference;

.field private dialogSub:Lio/reactivex/disposables/Disposable;

.field private final dialogType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/path/PathContextFactory;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/path/PathContextFactory;",
            "Ljava/lang/Class<",
            "+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogSub:Lio/reactivex/disposables/Disposable;

    .line 38
    sget-object v0, Lcom/squareup/container/layer/DialogReference;->NONE:Lcom/squareup/container/layer/DialogReference;

    iput-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    .line 41
    iput-object p1, p0, Lcom/squareup/container/layer/DialogLayer;->contextFactory:Lflow/path/PathContextFactory;

    .line 42
    iput-object p2, p0, Lcom/squareup/container/layer/DialogLayer;->dialogType:Ljava/lang/Class;

    return-void
.end method

.method private varargs showOrReplaceDialog(Lflow/TraversalCallback;Lflow/path/Path;[Lflow/path/PathContext;)V
    .locals 8

    .line 123
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    invoke-virtual {v0, p2}, Lcom/squareup/container/layer/DialogReference;->matches(Lflow/path/Path;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    aput-object p2, p3, v1

    const-string p2, "Already showing dialog %s"

    .line 124
    invoke-static {p2, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-interface {p1}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 130
    :cond_0
    instance-of v0, p2, Lcom/squareup/container/ContainerTreeKey;

    if-eqz v0, :cond_1

    .line 131
    move-object v0, p2

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ParcelableTester;->assertScreenCanBeParceled(Lcom/squareup/container/ContainerTreeKey;)V

    .line 136
    :cond_1
    invoke-direct {p0}, Lcom/squareup/container/layer/DialogLayer;->tearDownDialog()Lflow/path/PathContext;

    move-result-object v4

    .line 137
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->contextFactory:Lflow/path/PathContextFactory;

    aget-object v1, p3, v1

    invoke-static {p2, v0, v1, p3}, Lflow/path/PathContext;->create(Lflow/path/Path;Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)Lflow/path/PathContext;

    move-result-object v5

    .line 140
    invoke-static {v5}, Lcom/squareup/container/ContainerKt;->getViewFactory(Landroid/content/Context;)Lcom/squareup/container/ContainerViewFactory;

    move-result-object v0

    invoke-interface {v0, p2, v5}, Lcom/squareup/container/ContainerViewFactory;->dialogForKey(Ljava/lang/Object;Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object v0

    .line 141
    invoke-static {p2}, Lcom/squareup/container/layer/DialogReference;->expect(Lflow/path/Path;)Lcom/squareup/container/layer/DialogReference;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    .line 142
    new-instance p2, Lcom/squareup/container/layer/-$$Lambda$DialogLayer$FavogQNO8sOmxOBhs58-pCh65Kw;

    move-object v2, p2

    move-object v3, p0

    move-object v6, p3

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/container/layer/-$$Lambda$DialogLayer$FavogQNO8sOmxOBhs58-pCh65Kw;-><init>(Lcom/squareup/container/layer/DialogLayer;Lflow/path/PathContext;Lflow/path/PathContext;[Lflow/path/PathContext;Lflow/TraversalCallback;)V

    invoke-virtual {v0, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/layer/DialogLayer;->dialogSub:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private tearDownDialog()Lflow/path/PathContext;
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogSub:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 114
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogSub:Lio/reactivex/disposables/Disposable;

    .line 116
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    invoke-virtual {v0}, Lcom/squareup/container/layer/DialogReference;->tearDown()Landroid/content/Context;

    move-result-object v0

    .line 117
    sget-object v1, Lcom/squareup/container/layer/DialogReference;->NONE:Lcom/squareup/container/layer/DialogReference;

    iput-object v1, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_0
    invoke-static {v0}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object v0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public as(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public cleanUp()V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/container/layer/DialogLayer;->tearDownDialog()Lflow/path/PathContext;

    return-void
.end method

.method public varargs dispatchLayer(Lflow/Traversal;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 3

    .line 74
    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p0, p1}, Lcom/squareup/container/layer/DialogLayer;->findTopmostScreenToShow(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 77
    invoke-direct {p0}, Lcom/squareup/container/layer/DialogLayer;->tearDownDialog()Lflow/path/PathContext;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/squareup/container/layer/DialogLayer;->contextFactory:Lflow/path/PathContextFactory;

    aget-object v0, p3, v0

    invoke-virtual {p1, v1, v0, p3}, Lflow/path/PathContext;->destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V

    .line 81
    :cond_0
    invoke-interface {p2}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 85
    :cond_1
    const-class v1, Lcom/squareup/container/LayoutScreen;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 91
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/container/layer/DialogLayer;->showOrReplaceDialog(Lflow/TraversalCallback;Lflow/path/Path;[Lflow/path/PathContext;)V

    return-void

    .line 86
    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    const/4 p3, 0x3

    new-array p3, p3, [Ljava/lang/Object;

    aput-object p1, p3, v0

    const/4 p1, 0x1

    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogType:Ljava/lang/Class;

    .line 88
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p3, p1

    const/4 p1, 0x2

    const-class v0, Lcom/squareup/container/LayoutScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p3, p1

    const-string p1, "%s must not be annotated with @%s and implement @%s"

    .line 87
    invoke-static {p1, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public findTopmostScreenToShow(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 51
    invoke-virtual {p1}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 52
    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p0, v0}, Lcom/squareup/container/layer/DialogLayer;->owns(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getVisibleContext()Landroid/content/Context;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    invoke-virtual {v0}, Lcom/squareup/container/layer/DialogReference;->requireContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public is(Ljava/lang/Class;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    const/4 p1, 0x0

    return p1
.end method

.method public isDefault()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    invoke-virtual {v0}, Lcom/squareup/container/layer/DialogReference;->isShowing()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$showOrReplaceDialog$0$DialogLayer(Lflow/path/PathContext;Lflow/path/PathContext;[Lflow/path/PathContext;Lflow/TraversalCallback;Landroid/app/Dialog;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    invoke-virtual {v0, p5}, Lcom/squareup/container/layer/DialogReference;->show(Landroid/app/Dialog;)Lcom/squareup/container/layer/DialogReference;

    move-result-object p5

    iput-object p5, p0, Lcom/squareup/container/layer/DialogLayer;->dialogReference:Lcom/squareup/container/layer/DialogReference;

    if-eqz p1, :cond_0

    .line 147
    iget-object p5, p0, Lcom/squareup/container/layer/DialogLayer;->contextFactory:Lflow/path/PathContextFactory;

    invoke-virtual {p1, p5, p2, p3}, Lflow/path/PathContext;->destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V

    .line 150
    :cond_0
    invoke-interface {p4}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void
.end method

.method public owns(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogType:Ljava/lang/Class;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/container/layer/DialogLayer;->dialogType:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
