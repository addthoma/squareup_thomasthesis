.class final Lcom/squareup/container/Flows$goFromTo$1;
.super Ljava/lang/Object;
.source "Flows.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $expectedScreen:Ljava/lang/Class;

.field final synthetic $screen:Lcom/squareup/container/ContainerTreeKey;


# direct methods
.method constructor <init>(Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/Flows$goFromTo$1;->$expectedScreen:Ljava/lang/Class;

    iput-object p2, p0, Lcom/squareup/container/Flows$goFromTo$1;->$screen:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 40
    iget-object v1, p0, Lcom/squareup/container/Flows$goFromTo$1;->$expectedScreen:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->assertInScopeOf(Ljava/lang/Class;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/container/Flows$goFromTo$1;->$screen:Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->set(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
