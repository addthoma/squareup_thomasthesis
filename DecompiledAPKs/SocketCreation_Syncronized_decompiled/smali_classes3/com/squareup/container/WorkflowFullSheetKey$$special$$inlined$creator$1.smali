.class public final Lcom/squareup/container/WorkflowFullSheetKey$$special$$inlined$creator$1;
.super Lcom/squareup/container/ContainerTreeKey$PathCreator;
.source "Container.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/WorkflowFullSheetKey;-><clinit>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
        "Lcom/squareup/container/WorkflowFullSheetKey;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Container.kt\ncom/squareup/container/ContainerKt$pathCreator$1\n+ 2 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n+ 3 WorkflowFullSheetKey.kt\ncom/squareup/container/WorkflowFullSheetKey\n*L\n1#1,56:1\n151#2,12:57\n32#3:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000G\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J\u0015\u0010\u0002\u001a\u00028\u00002\u0006\u0010\u0003\u001a\u00020\u0004H\u0014\u00a2\u0006\u0002\u0010\u0005J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b\u00b8\u0006\u000c"
    }
    d2 = {
        "com/squareup/container/ContainerKt$pathCreator$1",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator;",
        "doCreateFromParcel",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/container/ContainerTreeKey;",
        "public_release",
        "com/squareup/container/WorkflowFullSheetKey$creator$$inlined$pathCreator$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;-><init>()V

    return-void
.end method


# virtual methods
.method protected doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Lcom/squareup/container/WorkflowFullSheetKey;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v0, "parcel.readString()!!"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 59
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    if-eqz v3, :cond_6

    check-cast v3, Lcom/squareup/container/ContainerTreeKey;

    .line 61
    new-instance v4, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v5, v6}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    sget-object v5, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    if-nez v6, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    const-string v7, "parcel.createByteArray()!!"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v7, v6

    invoke-static {v6, v7}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object v5

    .line 63
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    move-object v6, v0

    .line 64
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    const-string v0, "parcel.readParcelable<ScreenHint>(classLoader)!!"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, p1

    check-cast v7, Lcom/squareup/workflow/ScreenHint;

    const/4 v8, 0x1

    .line 68
    new-instance p1, Lcom/squareup/container/WorkflowFullSheetKey;

    move-object v1, p1

    .line 69
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowFullSheetKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    return-object p1

    .line 59
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.container.ContainerTreeKey"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public newArray(I)[Lcom/squareup/container/ContainerTreeKey;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lcom/squareup/container/WorkflowFullSheetKey;"
        }
    .end annotation

    .line 25
    new-array p1, p1, [Lcom/squareup/container/WorkflowFullSheetKey;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/container/WorkflowFullSheetKey$$special$$inlined$creator$1;->newArray(I)[Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method
