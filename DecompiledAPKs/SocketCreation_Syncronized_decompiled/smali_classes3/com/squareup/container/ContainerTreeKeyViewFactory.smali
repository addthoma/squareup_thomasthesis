.class public Lcom/squareup/container/ContainerTreeKeyViewFactory;
.super Ljava/lang/Object;
.source "ContainerTreeKeyViewFactory.java"

# interfaces
.implements Lcom/squareup/container/ContainerViewFactory;


# static fields
.field private static final NO_DIALOG_FACTORY:Lcom/squareup/workflow/DialogFactory;

.field private static final dialogFactoryByPathClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/squareup/workflow/DialogFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/container/-$$Lambda$ContainerTreeKeyViewFactory$hNwFncOtxIwvNg6hENG89Pp7WsM;->INSTANCE:Lcom/squareup/container/-$$Lambda$ContainerTreeKeyViewFactory$hNwFncOtxIwvNg6hENG89Pp7WsM;

    sput-object v0, Lcom/squareup/container/ContainerTreeKeyViewFactory;->NO_DIALOG_FACTORY:Lcom/squareup/workflow/DialogFactory;

    .line 39
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/squareup/container/ContainerTreeKeyViewFactory;->dialogFactoryByPathClass:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDialogAnnotation(Ljava/lang/Object;)Ljava/lang/annotation/Annotation;
    .locals 6

    .line 106
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/squareup/container/layer/DialogScreen;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/layer/DialogScreen;

    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/squareup/container/layer/DialogCardScreen;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/layer/DialogCardScreen;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    xor-int/2addr v4, v5

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v3

    aput-object v0, v5, v2

    const/4 p0, 0x2

    aput-object v1, v5, p0

    const-string p0, "Expected exactly one dialog annotation on %s, found %s, %s"

    .line 109
    invoke-static {v4, p0, v5}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    return-object v1
.end method

.method static getDialogFactory(Ljava/lang/Object;)Lcom/squareup/workflow/DialogFactory;
    .locals 2

    const-string v0, "path"

    .line 86
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 87
    sget-object v1, Lcom/squareup/container/ContainerTreeKeyViewFactory;->dialogFactoryByPathClass:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/DialogFactory;

    if-nez v1, :cond_1

    .line 89
    invoke-static {p0}, Lcom/squareup/container/ContainerTreeKeyViewFactory;->getDialogAnnotation(Ljava/lang/Object;)Ljava/lang/annotation/Annotation;

    move-result-object p0

    if-nez p0, :cond_0

    .line 92
    sget-object p0, Lcom/squareup/container/ContainerTreeKeyViewFactory;->NO_DIALOG_FACTORY:Lcom/squareup/workflow/DialogFactory;

    goto :goto_0

    .line 94
    :cond_0
    invoke-static {p0}, Lcom/squareup/container/ContainerTreeKeyViewFactory;->getDialogFactoryFromAnnotation(Ljava/lang/annotation/Annotation;)Lcom/squareup/workflow/DialogFactory;

    move-result-object p0

    :goto_0
    move-object v1, p0

    .line 96
    sget-object p0, Lcom/squareup/container/ContainerTreeKeyViewFactory;->dialogFactoryByPathClass:Ljava/util/Map;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    :cond_1
    sget-object p0, Lcom/squareup/container/ContainerTreeKeyViewFactory;->NO_DIALOG_FACTORY:Lcom/squareup/workflow/DialogFactory;

    if-ne v1, p0, :cond_2

    const/4 p0, 0x0

    return-object p0

    :cond_2
    return-object v1
.end method

.method private static getDialogFactoryFromAnnotation(Ljava/lang/annotation/Annotation;)Lcom/squareup/workflow/DialogFactory;
    .locals 4

    .line 124
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "value"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string/jumbo v1, "value() method"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    new-array v1, v2, [Ljava/lang/Object;

    .line 127
    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Class;

    .line 128
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/DialogFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 130
    invoke-static {p0}, Lio/reactivex/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method private static getLayoutId(Lcom/squareup/util/Device;Ljava/lang/Object;)I
    .locals 2

    .line 69
    invoke-interface {p0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result p0

    if-eqz p0, :cond_0

    instance-of p0, p1, Lcom/squareup/container/HasWideTabletLayout;

    if-eqz p0, :cond_0

    .line 70
    check-cast p1, Lcom/squareup/container/HasWideTabletLayout;

    invoke-interface {p1}, Lcom/squareup/container/HasWideTabletLayout;->wideTabletLayout()I

    move-result p0

    return p0

    .line 73
    :cond_0
    instance-of p0, p1, Lcom/squareup/container/LayoutScreen;

    if-eqz p0, :cond_1

    .line 79
    check-cast p1, Lcom/squareup/container/LayoutScreen;

    invoke-interface {p1}, Lcom/squareup/container/LayoutScreen;->screenLayout()I

    move-result p0

    return p0

    .line 74
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 75
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const/4 p1, 0x1

    const-class v1, Lcom/squareup/container/LayoutScreen;

    .line 76
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    const/4 p1, 0x2

    const-class v1, Lcom/squareup/container/HasWideTabletLayout;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    const-string p1, "@%s must implement %s or %s."

    .line 75
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static synthetic lambda$static$0(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1

    .line 36
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Should never be called"

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public canRender(Ljava/lang/Object;)Z
    .locals 1

    .line 43
    instance-of v0, p1, Lcom/squareup/container/LayoutScreen;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/squareup/container/HasWideTabletLayout;

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->isDialogScreen(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public dialogForKey(Ljava/lang/Object;Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 64
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKeyViewFactory;->getDialogFactory(Ljava/lang/Object;)Lcom/squareup/workflow/DialogFactory;

    move-result-object p1

    const-string v0, "getDialogFactory"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/DialogFactory;

    .line 65
    invoke-interface {p1, p2}, Lcom/squareup/workflow/DialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public viewForKey(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 49
    invoke-static {p2}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/container/ContainerTreeKeyViewFactory;->getLayoutId(Lcom/squareup/util/Device;Ljava/lang/Object;)I

    move-result p1

    .line 50
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 51
    invoke-virtual {v0, p2}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    .line 52
    invoke-virtual {v0, p1, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 54
    invoke-static {p2}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p2

    .line 55
    sget-object p3, Lcom/squareup/container/ContainerTreeKey;->COORDINATOR_SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p2, p3}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 56
    sget-object p3, Lcom/squareup/container/ContainerTreeKey;->COORDINATOR_SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p2, p3}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {p1, p2}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    :cond_0
    return-object p1
.end method
