.class Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;
.super Lcom/squareup/container/ContainerTreeKey$PathCreator;
.source "ContainerTreeKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/ContainerTreeKey$PathCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ParceledPathCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/container/ContainerTreeKey;",
        ">",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final parcel:Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final parceledType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc<",
            "TT;>;)V"
        }
    .end annotation

    .line 389
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;-><init>()V

    .line 390
    iput-object p1, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;->parcel:Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    .line 392
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    const-string v0, "invoke"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/os/Parcel;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    .line 394
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;->parceledType:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 396
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;Lcom/squareup/container/ContainerTreeKey$1;)V
    .locals 0

    .line 385
    invoke-direct {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;-><init>(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 385
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method protected doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .line 401
    iget-object v0, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;->parcel:Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-interface {v0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;->invoke(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TT;"
        }
    .end annotation

    .line 406
    iget-object v0, p0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;->parceledType:Ljava/lang/Class;

    invoke-static {v0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/container/ContainerTreeKey;

    check-cast p1, [Lcom/squareup/container/ContainerTreeKey;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 385
    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParceledPathCreator;->newArray(I)[Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method
