.class public final Lcom/squareup/container/spot/SpotHelper;
.super Ljava/lang/Object;
.source "SpotHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/spot/SpotHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSpotHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SpotHelper.kt\ncom/squareup/container/spot/SpotHelper\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,185:1\n111#2:186\n111#2:187\n*E\n*S KotlinDebug\n*F\n+ 1 SpotHelper.kt\ncom/squareup/container/spot/SpotHelper\n*L\n104#1:186\n105#1:187\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u001b\u0012\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\u0004H\u0002J \u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J \u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J \u0010\u0010\u001a\u00020\u000b2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000fJ\u001a\u0010\u0013\u001a\u00020\u000b2\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0015\u001a\u00020\u0004H\u0002J \u0010\u0016\u001a\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fR\u001a\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/container/spot/SpotHelper;",
        "",
        "keyForView",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getCustomSpot",
        "Lcom/squareup/container/spot/Spot;",
        "view",
        "isCrossingCardBoundary",
        "",
        "oldChild",
        "newChild",
        "direction",
        "Lflow/Direction;",
        "isEnteringFullScreen",
        "fromScreen",
        "toScreen",
        "isExitingFullScreen",
        "fromView",
        "toView",
        "whereAreWeGoing",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

.field private static final DEFAULT_SPOT:Lcom/squareup/container/spot/Spot;


# instance fields
.field private final keyForView:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/spot/SpotHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/spot/SpotHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/spot/SpotHelper;->Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

    .line 122
    sget-object v0, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    sput-object v0, Lcom/squareup/container/spot/SpotHelper;->DEFAULT_SPOT:Lcom/squareup/container/spot/Spot;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/container/spot/SpotHelper;-><init>(Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    const-string v0, "keyForView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 25
    sget-object p1, Lcom/squareup/container/spot/SpotHelper$1;->INSTANCE:Lcom/squareup/container/spot/SpotHelper$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/container/spot/SpotHelper;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final findTopChild(Lflow/Direction;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot;)Landroid/view/View;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/spot/SpotHelper;->Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/container/spot/SpotHelper$Companion;->findTopChild(Lflow/Direction;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/spot/SpotHelper;->Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/container/spot/SpotHelper$Companion;->flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;

    move-result-object p0

    return-object p0
.end method

.method private final getCustomSpot(Landroid/view/View;)Lcom/squareup/container/spot/Spot;
    .locals 3

    .line 116
    instance-of v0, p1, Lcom/squareup/container/spot/HasSpot;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/container/spot/HasSpot;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/container/spot/HasSpot;->getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;

    move-result-object v1

    goto :goto_0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/squareup/container/spot/HasSpot;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    check-cast v0, Lcom/squareup/container/spot/HasSpot;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/container/spot/HasSpot;->getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;

    move-result-object v1

    :cond_2
    :goto_0
    return-object v1
.end method

.method private final isCrossingCardBoundary(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Z
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    .line 103
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    .line 186
    const-class v0, Lcom/squareup/container/layer/CardScreen;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p1

    .line 187
    const-class v0, Lcom/squareup/container/layer/CardScreen;

    invoke-static {p2, v0}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p1, p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    .line 107
    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    if-ne p3, v2, :cond_1

    if-nez p2, :cond_3

    :cond_1
    sget-object p2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    if-ne p3, p2, :cond_2

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_1
    return v0
.end method

.method private final isEnteringFullScreen(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Z
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    .line 93
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/container/spot/SpotHelper;->isEnteringFullScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)Z

    move-result p1

    return p1
.end method

.method private final isExitingFullScreen(Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    if-eqz p1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 81
    :goto_0
    iget-object v0, p0, Lcom/squareup/container/spot/SpotHelper;->keyForView:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/container/ContainerTreeKey;

    .line 82
    const-class v0, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {p2, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 83
    const-class v2, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {p1, v2}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    return v0
.end method

.method public static final runAnimation(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/spot/Spot;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Lflow/Direction;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/spot/SpotHelper;->Companion:Lcom/squareup/container/spot/SpotHelper$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/container/spot/SpotHelper$Companion;->runAnimation(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public final isEnteringFullScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)Z
    .locals 3

    const-string/jumbo v0, "toScreen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    const-class v0, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {p2, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 72
    const-class v2, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {p1, v2}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p2, :cond_1

    if-nez p1, :cond_1

    .line 73
    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    if-ne p3, p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final whereAreWeGoing(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Lcom/squareup/container/spot/Spot;
    .locals 2

    const-string v0, "newChild"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    sget-boolean v0, Lcom/squareup/container/spot/Spot;->SUPPRESS_ANIMATIONS_FOR_TESTS:Z

    if-nez v0, :cond_5

    if-eqz p1, :cond_5

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    if-eq p3, v0, :cond_5

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/container/spot/SpotHelper;->isEnteringFullScreen(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 46
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/container/spot/SpotHelper;->isCrossingCardBoundary(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Z

    move-result v0

    const-string v1, "Spots.BELOW"

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 48
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/container/spot/SpotHelper;->isExitingFullScreen(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 57
    :cond_2
    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    if-ne p3, v0, :cond_3

    move-object p1, p2

    .line 58
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/container/spot/SpotHelper;->getCustomSpot(Landroid/view/View;)Lcom/squareup/container/spot/Spot;

    move-result-object p1

    if-eqz p1, :cond_4

    goto :goto_1

    :cond_4
    sget-object p1, Lcom/squareup/container/spot/SpotHelper;->DEFAULT_SPOT:Lcom/squareup/container/spot/Spot;

    const-string p2, "DEFAULT_SPOT"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 42
    :cond_5
    :goto_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const-string p2, "Spots.HERE"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method
