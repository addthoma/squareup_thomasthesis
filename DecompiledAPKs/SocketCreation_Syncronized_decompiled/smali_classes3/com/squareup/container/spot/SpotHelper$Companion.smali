.class public final Lcom/squareup/container/spot/SpotHelper$Companion;
.super Ljava/lang/Object;
.source "SpotHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/spot/SpotHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u000c\u001a\u00020\u0004H\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\tH\u0007J>\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000c\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0017H\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/container/spot/SpotHelper$Companion;",
        "",
        "()V",
        "DEFAULT_SPOT",
        "Lcom/squareup/container/spot/Spot;",
        "kotlin.jvm.PlatformType",
        "findTopChild",
        "Landroid/view/View;",
        "direction",
        "Lflow/Direction;",
        "newView",
        "oldView",
        "spot",
        "flowToSpotDirection",
        "Lcom/squareup/container/spot/Spot$Direction;",
        "flowDirection",
        "runAnimation",
        "",
        "container",
        "Landroid/view/ViewGroup;",
        "from",
        "to",
        "onAnimationEnd",
        "Lkotlin/Function0;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/container/spot/SpotHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final findTopChild(Lflow/Direction;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot;)Landroid/view/View;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "direction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "oldView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "spot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    move-object v0, p0

    check-cast v0, Lcom/squareup/container/spot/SpotHelper$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/container/spot/SpotHelper$Companion;->flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;

    move-result-object v0

    invoke-virtual {p4, v0, p2, p3}, Lcom/squareup/container/spot/Spot;->forceOutgoingViewOnTop(Lcom/squareup/container/spot/Spot$Direction;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 144
    :cond_0
    iget-boolean p4, p4, Lcom/squareup/container/spot/Spot;->isOverlay:Z

    if-eqz p4, :cond_2

    sget-object p4, Lflow/Direction;->FORWARD:Lflow/Direction;

    if-ne p1, p4, :cond_1

    goto :goto_1

    :cond_1
    :goto_0
    move-object p2, p3

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    return-object p2
.end method

.method public final flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "flowDirection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spot$Direction;->FORWARD:Lcom/squareup/container/spot/Spot$Direction;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spot$Direction;->BACKWARD:Lcom/squareup/container/spot/Spot$Direction;

    :goto_0
    return-object p1
.end method

.method public final runAnimation(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/spot/Spot;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Lflow/Direction;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "spot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "to"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAnimationEnd"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    new-instance v0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;-><init>(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V

    .line 181
    check-cast v0, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {p4, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method
