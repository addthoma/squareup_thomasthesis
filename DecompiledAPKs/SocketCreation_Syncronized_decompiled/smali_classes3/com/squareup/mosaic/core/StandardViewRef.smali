.class public abstract Lcom/squareup/mosaic/core/StandardViewRef;
.super Lcom/squareup/mosaic/core/ViewRef;
.source "StandardViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mosaic/core/StandardViewRef$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "-TV;*>;V:",
        "Landroid/view/View;",
        ">",
        "Lcom/squareup/mosaic/core/ViewRef<",
        "TM;TV;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardViewRef.kt\ncom/squareup/mosaic/core/StandardViewRef\n*L\n1#1,83:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000*\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0006\u0008\u0000\u0012\u0002H\u0003\u0012\u0002\u0008\u00030\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005:\u0001 B\r\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u001d\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u00002\u0006\u0010\u0013\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\u0014J\u001d\u0010\u0015\u001a\u00028\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0017J\u001f\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0013\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u001eH\u0016R\u0012\u0010\t\u001a\u0004\u0018\u00018\u0001X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0011\u0010\u000b\u001a\u00028\u00018F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "M",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "_android",
        "Landroid/view/View;",
        "androidView",
        "getAndroidView",
        "()Landroid/view/View;",
        "getContext",
        "()Landroid/content/Context;",
        "canUpdateTo",
        "",
        "currentModel",
        "newModel",
        "(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)Z",
        "createView",
        "model",
        "(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;",
        "doBind",
        "",
        "oldModel",
        "(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "State",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _android:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/mosaic/core/ViewRef;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/core/StandardViewRef;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected canUpdateTo(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TM;)Z"
        }
    .end annotation

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/ViewRef;->canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/mosaic/core/StandardUiModel;->getStyleRes()I

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/mosaic/core/StandardUiModel;->getStyleRes()I

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    check-cast p2, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/core/StandardViewRef;->canUpdateTo(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)Z

    move-result p1

    return p1
.end method

.method public abstract createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TM;)TV;"
        }
    .end annotation
.end method

.method public doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TM;)V"
        }
    .end annotation

    const-string p1, "newModel"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/mosaic/core/StandardViewRef;->_android:Landroid/view/View;

    if-nez p1, :cond_0

    .line 35
    iget-object p1, p0, Lcom/squareup/mosaic/core/StandardViewRef;->context:Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/core/StandardViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/mosaic/core/StandardViewRef;->_android:Landroid/view/View;

    .line 36
    invoke-virtual {p2}, Lcom/squareup/mosaic/core/StandardUiModel;->getCustomOnceBlock$mosaic_core_release()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    .line 38
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/core/StandardUiModel;->getCustomBlock$mosaic_core_release()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    check-cast p2, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    return-void
.end method

.method public final getAndroidView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/mosaic/core/StandardViewRef;->_android:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/mosaic/core/StandardViewRef;->context:Landroid/content/Context;

    return-object v0
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    instance-of v0, p1, Lcom/squareup/mosaic/core/StandardViewRef$State;

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 74
    check-cast p1, Lcom/squareup/mosaic/core/StandardViewRef$State;

    invoke-virtual {p1}, Lcom/squareup/mosaic/core/StandardViewRef$State;->getItems()Landroid/util/SparseArray;

    move-result-object p1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/squareup/mosaic/core/R$id;->models_adhoc_id:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    return-void

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected parcelable to be "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v1, Lcom/squareup/mosaic/core/StandardViewRef$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ". Instead it is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public saveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 59
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 60
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/squareup/mosaic/core/R$id;->models_adhoc_id:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 63
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/StandardViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setId(I)V

    .line 65
    new-instance v0, Lcom/squareup/mosaic/core/StandardViewRef$State;

    invoke-direct {v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef$State;-><init>(Landroid/util/SparseArray;)V

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method
