.class public final Lcom/squareup/mosaic/core/UiModelKt;
.super Ljava/lang/Object;
.source "UiModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UiModel.kt\ncom/squareup/mosaic/core/UiModelKt\n*L\n1#1,125:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008*\u0006\u0012\u0002\u0008\u00030\u00042\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u001a\u0018\u0010\u000c\u001a\u00020\r*\u0006\u0012\u0002\u0008\u00030\u00042\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u001a\u001e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0008*\u0006\u0012\u0002\u0008\u00030\u00042\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u001a\u001c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0006\u0012\u0002\u0008\u00030\u00042\u0006\u0010\u000f\u001a\u00020\u0002\u001a+\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u00110\u0001\"\u0008\u0008\u0000\u0010\u0011*\u00020\t*\u0006\u0012\u0002\u0008\u00030\u00042\u0006\u0010\u000f\u001a\u0002H\u0011\u00a2\u0006\u0002\u0010\u0012\u001a/\u0010\u0013\u001a\u000c\u0012\u0004\u0012\u0002H\u0015\u0012\u0002\u0008\u00030\u0014\"\u000c\u0008\u0000\u0010\u0015*\u0006\u0012\u0002\u0008\u00030\u0004*\u0002H\u00152\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018\"\u0014\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u001f\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0006\u0012\u0002\u0008\u00030\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0019"
    }
    d2 = {
        "emptyStringValue",
        "Lcom/squareup/resources/FixedText;",
        "",
        "emptyString",
        "Lcom/squareup/mosaic/core/UiModel;",
        "getEmptyString",
        "(Lcom/squareup/mosaic/core/UiModel;)Lcom/squareup/resources/FixedText;",
        "charSequence",
        "Lcom/squareup/resources/TextModel;",
        "",
        "id",
        "",
        "dimen",
        "Lcom/squareup/resources/DimenModel;",
        "string",
        "value",
        "text",
        "T",
        "(Lcom/squareup/mosaic/core/UiModel;Ljava/lang/CharSequence;)Lcom/squareup/resources/FixedText;",
        "toView",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "M",
        "context",
        "Landroid/content/Context;",
        "(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;",
        "mosaic-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final emptyStringValue:Lcom/squareup/resources/FixedText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/FixedText<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 124
    new-instance v0, Lcom/squareup/resources/FixedText;

    const-string v1, ""

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lcom/squareup/mosaic/core/UiModelKt;->emptyStringValue:Lcom/squareup/resources/FixedText;

    return-void
.end method

.method public static final charSequence(Lcom/squareup/mosaic/core/UiModel;I)Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;I)",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$charSequence"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    new-instance p0, Lcom/squareup/resources/ResourceCharSequence;

    invoke-direct {p0, p1}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast p0, Lcom/squareup/resources/TextModel;

    return-object p0
.end method

.method public static final dimen(Lcom/squareup/mosaic/core/UiModel;I)Lcom/squareup/resources/DimenModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;I)",
            "Lcom/squareup/resources/DimenModel;"
        }
    .end annotation

    const-string v0, "$this$dimen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance p0, Lcom/squareup/resources/ResourceDimen;

    invoke-direct {p0, p1}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    check-cast p0, Lcom/squareup/resources/DimenModel;

    return-object p0
.end method

.method public static final getEmptyString(Lcom/squareup/mosaic/core/UiModel;)Lcom/squareup/resources/FixedText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;)",
            "Lcom/squareup/resources/FixedText<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$emptyString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object p0, Lcom/squareup/mosaic/core/UiModelKt;->emptyStringValue:Lcom/squareup/resources/FixedText;

    return-object p0
.end method

.method public static final string(Lcom/squareup/mosaic/core/UiModel;Ljava/lang/String;)Lcom/squareup/resources/FixedText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/resources/FixedText<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$string"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p0, "value"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance p0, Lcom/squareup/resources/FixedText;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public static final string(Lcom/squareup/mosaic/core/UiModel;I)Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;I)",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$string"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    new-instance p0, Lcom/squareup/resources/ResourceString;

    invoke-direct {p0, p1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast p0, Lcom/squareup/resources/TextModel;

    return-object p0
.end method

.method public static final text(Lcom/squareup/mosaic/core/UiModel;Ljava/lang/CharSequence;)Lcom/squareup/resources/FixedText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/CharSequence;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;TT;)",
            "Lcom/squareup/resources/FixedText<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$text"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p0, "value"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance p0, Lcom/squareup/resources/FixedText;

    invoke-direct {p0, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public static final toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/mosaic/core/UiModel<",
            "*>;>(TM;",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TM;*>;"
        }
    .end annotation

    const-string v0, "$this$toView"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/core/UiModel;->createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/squareup/mosaic/core/ViewRef;->bind(Lcom/squareup/mosaic/core/UiModel;)V

    return-object p1

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.mosaic.core.ViewRef<M, *>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
