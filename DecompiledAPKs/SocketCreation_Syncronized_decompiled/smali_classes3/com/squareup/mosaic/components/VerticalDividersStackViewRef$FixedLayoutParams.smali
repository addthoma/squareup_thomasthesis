.class final Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;
.super Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;
.source "VerticalDividersStackViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mosaic/components/VerticalDividersStackViewRef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FixedLayoutParams"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0002\u0018\u00002\u00020\u0001B\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;",
        "Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;",
        "edges",
        "Lcom/squareup/mosaic/components/DividerEdge;",
        "(ILkotlin/jvm/internal/DefaultConstructorMarker;)V",
        "getEdges",
        "()I",
        "I",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I


# direct methods
.method private constructor <init>(I)V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, -0x2

    .line 37
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(III)V

    iput p1, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;->edges:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getEdges()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/mosaic/components/VerticalDividersStackViewRef$FixedLayoutParams;->edges:I

    return v0
.end method
