.class public final Lcom/squareup/mosaic/components/VerticalStackUiModel$remaining$1;
.super Ljava/lang/Object;
.source "VerticalStackUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/UiModelContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/components/VerticalStackUiModel;->remaining(Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalStackUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalStackUiModel.kt\ncom/squareup/mosaic/components/VerticalStackUiModel$remaining$1\n*L\n1#1,129:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0016\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/mosaic/components/VerticalStackUiModel$remaining$1",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
        "add",
        "",
        "model",
        "Lcom/squareup/mosaic/core/UiModel;",
        "createParams",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mosaic/components/VerticalStackUiModel;


# direct methods
.method public constructor <init>(Lcom/squareup/mosaic/components/VerticalStackUiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 90
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$remaining$1;->this$0:Lcom/squareup/mosaic/components/VerticalStackUiModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
            ">;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$remaining$1;->this$0:Lcom/squareup/mosaic/components/VerticalStackUiModel;

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getSubModels()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public createParams()Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;
    .locals 7

    .line 91
    new-instance v6, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;

    .line 92
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$remaining$1;->this$0:Lcom/squareup/mosaic/components/VerticalStackUiModel;

    invoke-virtual {v0}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getCurrentSpacing()Lcom/squareup/resources/DimenModel;

    move-result-object v2

    .line 93
    sget-object v3, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->EXTEND:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, v6

    .line 91
    invoke-direct/range {v0 .. v5}, Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;-><init>(ILcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v6
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackUiModel$remaining$1;->createParams()Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;

    move-result-object v0

    return-object v0
.end method
