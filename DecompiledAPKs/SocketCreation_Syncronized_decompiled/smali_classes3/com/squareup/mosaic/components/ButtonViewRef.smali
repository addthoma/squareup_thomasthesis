.class public final Lcom/squareup/mosaic/components/ButtonViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "ButtonViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/ButtonUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoButton;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nButtonViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ButtonViewRef.kt\ncom/squareup/mosaic/components/ButtonViewRef\n+ 2 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n*L\n1#1,31:1\n15#2,2:32\n15#2,2:34\n15#2,2:36\n*E\n*S KotlinDebug\n*F\n+ 1 ButtonViewRef.kt\ncom/squareup/mosaic/components/ButtonViewRef\n*L\n18#1,2:32\n18#1,2:34\n18#1,2:36\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000c\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/ButtonViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/ButtonUiModel;",
        "Lcom/squareup/noho/NohoButton;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 10
    check-cast p2, Lcom/squareup/mosaic/components/ButtonUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/ButtonViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/ButtonUiModel;)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/ButtonUiModel;)Lcom/squareup/noho/NohoButton;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/ButtonUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoButton;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance p2, Lcom/squareup/noho/NohoButton;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/ButtonUiModel;Lcom/squareup/mosaic/components/ButtonUiModel;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/ButtonUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/ButtonUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 18
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ButtonViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 20
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/ButtonUiModel;->getType()Lcom/squareup/noho/NohoButtonType;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ButtonUiModel;->getType()Lcom/squareup/noho/NohoButtonType;

    move-result-object v3

    .line 32
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 20
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    :cond_1
    if-eqz p1, :cond_2

    .line 22
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/ButtonUiModel;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v2

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ButtonUiModel;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 34
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const-string v4, "context"

    if-eqz v2, :cond_3

    .line 22
    invoke-virtual {v0}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 24
    :cond_3
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ButtonUiModel;->isTwoLines()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setTwoLines(Z)V

    if-eqz p1, :cond_4

    .line 25
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/ButtonUiModel;->getSubText()Lcom/squareup/resources/TextModel;

    move-result-object v2

    goto :goto_2

    :cond_4
    move-object v2, v1

    :goto_2
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ButtonUiModel;->getSubText()Lcom/squareup/resources/TextModel;

    move-result-object v3

    .line 36
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_6

    if-eqz v3, :cond_5

    .line 25
    invoke-virtual {v0}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_3

    :cond_5
    move-object v2, v1

    :goto_3
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setSubText(Ljava/lang/CharSequence;)V

    .line 26
    :cond_6
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ButtonUiModel;->isEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 27
    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/ButtonUiModel;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    :cond_7
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ButtonUiModel;->getOnClick()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/mosaic/components/ViewRefUtilsKt;->setOnClickDebouncedIfChanged(Landroid/view/View;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/mosaic/components/ButtonUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/ButtonUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/ButtonViewRef;->doBind(Lcom/squareup/mosaic/components/ButtonUiModel;Lcom/squareup/mosaic/components/ButtonUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/mosaic/components/ButtonUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/ButtonUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/ButtonViewRef;->doBind(Lcom/squareup/mosaic/components/ButtonUiModel;Lcom/squareup/mosaic/components/ButtonUiModel;)V

    return-void
.end method
