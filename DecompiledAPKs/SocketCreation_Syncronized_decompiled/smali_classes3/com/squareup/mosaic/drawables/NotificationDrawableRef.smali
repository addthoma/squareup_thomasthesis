.class public final Lcom/squareup/mosaic/drawables/NotificationDrawableRef;
.super Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;
.source "NotificationDrawableRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef<",
        "Lcom/squareup/mosaic/drawables/NotificationDrawableModel;",
        "Lcom/squareup/noho/NotificationDrawable;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDrawableRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDrawableRef.kt\ncom/squareup/mosaic/drawables/NotificationDrawableRef\n*L\n1#1,37:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0014J\u0018\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\u0002H\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\u0002H\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/NotificationDrawableRef;",
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;",
        "Lcom/squareup/mosaic/drawables/NotificationDrawableModel;",
        "Lcom/squareup/noho/NotificationDrawable;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "canUpdateTo",
        "",
        "currentModel",
        "newModel",
        "createDrawable",
        "model",
        "updateDrawable",
        "",
        "drawable",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/DrawableModel;Lcom/squareup/mosaic/core/DrawableModel;)Z
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    check-cast p2, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableRef;->canUpdateTo(Lcom/squareup/mosaic/drawables/NotificationDrawableModel;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)Z

    move-result p1

    return p1
.end method

.method protected canUpdateTo(Lcom/squareup/mosaic/drawables/NotificationDrawableModel;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)Z
    .locals 2

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getBaseDrawableId()I

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getBaseDrawableId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getDefStyleAttr()I

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getDefStyleAttr()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getDefStyleRes()I

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getDefStyleRes()I

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 6
    check-cast p2, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableRef;->createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)Lcom/squareup/noho/NotificationDrawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method public createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)Lcom/squareup/noho/NotificationDrawable;
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/noho/NotificationDrawable;

    .line 15
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getBaseDrawableId()I

    move-result v1

    .line 16
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getDefStyleAttr()I

    move-result v2

    .line 17
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getDefStyleRes()I

    move-result v3

    .line 13
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/noho/NotificationDrawable;-><init>(Landroid/content/Context;III)V

    .line 18
    invoke-virtual {p0, v0, p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableRef;->updateDrawable(Lcom/squareup/noho/NotificationDrawable;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)V

    return-object v0
.end method

.method public bridge synthetic updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/noho/NotificationDrawable;

    check-cast p2, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableRef;->updateDrawable(Lcom/squareup/noho/NotificationDrawable;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)V

    return-void
.end method

.method public updateDrawable(Lcom/squareup/noho/NotificationDrawable;Lcom/squareup/mosaic/drawables/NotificationDrawableModel;)V
    .locals 1

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NotificationDrawable;->setText(Ljava/lang/String;)V

    return-void
.end method
