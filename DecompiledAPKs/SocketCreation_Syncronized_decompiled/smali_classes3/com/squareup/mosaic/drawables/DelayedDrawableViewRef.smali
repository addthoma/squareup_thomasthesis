.class public abstract Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;
.super Lcom/squareup/mosaic/core/DrawableRef;
.source "DelayedDrawableViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "D:",
        "Landroid/graphics/drawable/Drawable;",
        ">",
        "Lcom/squareup/mosaic/core/DrawableRef<",
        "TM;TD;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005B\r\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u001d\u0010\u0010\u001a\u00028\u00012\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0012J\u0015\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0015J\u001d\u0010\u0016\u001a\u00020\u00142\u0006\u0010\r\u001a\u00028\u00012\u0006\u0010\u0011\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0017R\u0012\u0010\t\u001a\u0004\u0018\u00018\u0001X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00028\u00018VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;",
        "M",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "D",
        "Landroid/graphics/drawable/Drawable;",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "_drawable",
        "Landroid/graphics/drawable/Drawable;",
        "getContext",
        "()Landroid/content/Context;",
        "drawable",
        "getDrawable",
        "()Landroid/graphics/drawable/Drawable;",
        "createDrawable",
        "model",
        "(Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;)Landroid/graphics/drawable/Drawable;",
        "doBind",
        "",
        "(Lcom/squareup/mosaic/core/DrawableModel;)V",
        "updateDrawable",
        "(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/core/DrawableModel;)V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _drawable:Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/mosaic/core/DrawableRef;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public abstract createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;)Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TM;)TD;"
        }
    .end annotation
.end method

.method public doBind(Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->_drawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->context:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->_drawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/core/DrawableModel;)V

    :goto_0
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;->_drawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public abstract updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/core/DrawableModel;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TM;)V"
        }
    .end annotation
.end method
