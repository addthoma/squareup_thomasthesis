.class public final Lcom/squareup/mosaic/basic/ResponsiveModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "Responsive.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Landroid/widget/FrameLayout;",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B2\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012#\u0010\u0006\u001a\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\u0012\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0010J+\u0010\u0013\u001a\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\u000bH\u00c0\u0003\u00a2\u0006\u0002\u0008\u0014JE\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002%\u0008\u0002\u0010\u0006\u001a\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\u000bH\u00c6\u0001\u00a2\u0006\u0002\u0010\u0016J\u0018\u0010\u0017\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R1\u0010\u0006\u001a\u001f\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\u000bX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/mosaic/basic/ResponsiveModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Landroid/widget/FrameLayout;",
        "params",
        "modelProvider",
        "Lkotlin/Function2;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "",
        "Lcom/squareup/mosaic/basic/ResponsiveState;",
        "Lkotlin/ExtensionFunctionType;",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V",
        "getModelProvider$mosaic_core_release",
        "()Lkotlin/jvm/functions/Function2;",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "component1",
        "component2",
        "component2$mosaic_core_release",
        "copy",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/basic/ResponsiveModel;",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final modelProvider:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/mosaic/basic/ResponsiveState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;-",
            "Lcom/squareup/mosaic/basic/ResponsiveState;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modelProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/basic/ResponsiveModel;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lcom/squareup/mosaic/basic/ResponsiveModel;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/basic/ResponsiveModel;->copy(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/basic/ResponsiveModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2$mosaic_core_release()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/mosaic/basic/ResponsiveState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lcom/squareup/mosaic/basic/ResponsiveModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;-",
            "Lcom/squareup/mosaic/basic/ResponsiveState;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/mosaic/basic/ResponsiveModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modelProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/basic/ResponsiveModel;

    invoke-direct {v0, p1, p2}, Lcom/squareup/mosaic/basic/ResponsiveModel;-><init>(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/basic/ResponsiveModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/basic/ResponsiveModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    iget-object p1, p1, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getModelProvider$mosaic_core_release()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/mosaic/basic/ResponsiveState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ResponsiveModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", modelProvider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/basic/ResponsiveModel;->modelProvider:Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
