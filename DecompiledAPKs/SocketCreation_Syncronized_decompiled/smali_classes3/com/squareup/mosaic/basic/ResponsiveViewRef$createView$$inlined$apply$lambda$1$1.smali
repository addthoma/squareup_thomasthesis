.class final Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;
.super Ljava/lang/Object;
.source "Responsive.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->onLayoutChange(Landroid/view/View;IIIIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/mosaic/basic/ResponsiveViewRef$createView$1$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $bottom:I

.field final synthetic $left:I

.field final synthetic $right:I

.field final synthetic $top:I

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;


# direct methods
.method constructor <init>(Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;Landroid/view/View;IIII)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;

    iput-object p2, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$view:Landroid/view/View;

    iput p3, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$right:I

    iput p4, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$left:I

    iput p5, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$bottom:I

    iput p6, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$top:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .line 58
    iget-object v0, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;

    iget-object v0, v0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef;

    .line 59
    iget-object v1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;

    iget-object v1, v1, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;->this$0:Lcom/squareup/mosaic/basic/ResponsiveViewRef;

    invoke-virtual {v1}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/mosaic/basic/ResponsiveModel;

    .line 60
    new-instance v2, Lcom/squareup/mosaic/basic/ResponsiveState;

    iget-object v3, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$view:Landroid/view/View;

    const-string/jumbo v4, "view"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "view.context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v4, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$right:I

    iget v5, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$left:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$bottom:I

    iget v6, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1$1;->$top:I

    sub-int/2addr v5, v6

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/mosaic/basic/ResponsiveState;-><init>(Landroid/content/Context;II)V

    .line 58
    invoke-static {v0, v1, v2}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->access$maybeUpdateContent(Lcom/squareup/mosaic/basic/ResponsiveViewRef;Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveState;)V

    return-void
.end method
