.class public abstract Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "DebouncedChangeOnceTextWatcher.java"


# instance fields
.field private changed:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;->changed:Z

    return-void
.end method


# virtual methods
.method public final doAfterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/debounce/Debouncers;->canPerform()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;->changed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 22
    iput-boolean v0, p0, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;->changed:Z

    .line 23
    invoke-virtual {p0, p1}, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;->doOnFirstTextChange(Landroid/text/Editable;)V

    :cond_0
    return-void
.end method

.method public abstract doOnFirstTextChange(Landroid/text/Editable;)V
.end method
