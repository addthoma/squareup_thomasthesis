.class public abstract Lcom/squareup/debounce/DebouncedOnClickListener;
.super Ljava/lang/Object;
.source "DebouncedOnClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract doClick(Landroid/view/View;)V
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 22
    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->attemptPerform(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0, p1}, Lcom/squareup/debounce/DebouncedOnClickListener;->doClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method
