.class public Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;
.super Ljava/lang/Object;
.source "LoyaltyPointsRowSubtitleRenderer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "loyaltyCalculator",
        "Lcom/squareup/loyalty/LoyaltyCalculator;",
        "loyaltyServiceHelper",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/loyalty/LoyaltyServiceHelper;)V",
        "getSubtitle",
        "Lrx/Observable;",
        "",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "loyalty-calculator_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

.field private final loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/loyalty/LoyaltyServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyCalculator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyServiceHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p2, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

    iput-object p3, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    return-void
.end method

.method public static final synthetic access$getLoyaltyCalculator$p(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)Lcom/squareup/loyalty/LoyaltyCalculator;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltyServiceHelper$p(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)Lcom/squareup/loyalty/LoyaltyServiceHelper;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltySettings$p(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)Lcom/squareup/loyalty/LoyaltySettings;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-object p0
.end method


# virtual methods
.method public getSubtitle(Lcom/squareup/payment/Transaction;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Transaction;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    .line 33
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$1;

    invoke-direct {v1, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$1;-><init>(Lcom/squareup/payment/Transaction;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$2;

    invoke-direct {v1, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$2;-><init>(Lcom/squareup/payment/Transaction;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;

    invoke-direct {v1, p0}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;-><init>(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4;

    invoke-direct {v1, p0}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$4;-><init>(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 72
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->onCustomerChanged()Lrx/Observable;

    move-result-object v1

    .line 73
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v1, v2}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    .line 75
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$1;

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v2, v3}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->mergeWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    .line 76
    new-instance v2, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$2;

    invoke-direct {v2, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$2;-><init>(Lcom/squareup/payment/Transaction;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 77
    new-instance v2, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;

    invoke-direct {v2, p0, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$3;-><init>(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;Lcom/squareup/payment/Transaction;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 78
    sget-object v1, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$4;->INSTANCE:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withoutCustomer$4;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {p1, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 88
    invoke-static {p1, v0}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable.merge(withoutCustomer, withCustomer)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
