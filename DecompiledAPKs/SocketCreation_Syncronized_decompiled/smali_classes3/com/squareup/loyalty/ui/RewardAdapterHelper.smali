.class public Lcom/squareup/loyalty/ui/RewardAdapterHelper;
.super Ljava/lang/Object;
.source "RewardAdapterHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRewardAdapterHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RewardAdapterHelper.kt\ncom/squareup/loyalty/ui/RewardAdapterHelper\n*L\n1#1,127:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use RewardRecyclerViewHelper when feature flag [DISCOUNT_APPLY_MULTIPLE_COUPONS] is on"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0017\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J0\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0002JM\u0010\u0016\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00180\u00172\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00172\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001b2!\u0010\u0013\u001a\u001d\u0012\u0013\u0012\u00110\u000e\u00a2\u0006\u000c\u0008\u001d\u0012\u0008\u0008\u001e\u0012\u0004\u0008\u0008(\r\u0012\u0004\u0012\u00020\u00150\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;)V",
        "getLoyaltySettings",
        "()Lcom/squareup/loyalty/LoyaltySettings;",
        "getPointsTermsFormatter",
        "()Lcom/squareup/loyalty/PointsTermsFormatter;",
        "createRewardWrapper",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "rewardTier",
        "Lcom/squareup/protos/client/loyalty/RewardTier;",
        "isAppliedToCart",
        "",
        "contactPoints",
        "",
        "applyEvent",
        "Lkotlin/Function0;",
        "",
        "createRewardWrapperList",
        "Lrx/Observable;",
        "",
        "contactPointTotal",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p2, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-void
.end method

.method public static final synthetic access$createRewardWrapper(Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/protos/client/loyalty/RewardTier;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/loyalty/ui/RewardWrapper;
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->createRewardWrapper(Lcom/squareup/protos/client/loyalty/RewardTier;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/loyalty/ui/RewardWrapper;

    move-result-object p0

    return-object p0
.end method

.method private final createRewardWrapper(Lcom/squareup/protos/client/loyalty/RewardTier;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/loyalty/ui/RewardWrapper;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            "ZI",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/loyalty/ui/RewardWrapper;"
        }
    .end annotation

    const-string v0, "rewardTier.name"

    const-string v1, "rewardTier.points"

    if-eqz p2, :cond_0

    .line 40
    new-instance p2, Lcom/squareup/loyalty/ui/RewardWrapper;

    .line 41
    iget-object p3, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->name:Ljava/lang/String;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object p4, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 43
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->points:Ljava/lang/Long;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 44
    sget p1, Lcom/squareup/loyalty/R$string;->points_redeem_button_applied:I

    .line 42
    invoke-virtual {p4, v0, v1, p1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    .line 46
    sget-object p4, Lcom/squareup/loyalty/ui/RewardWrapper$Type$AppliedToCart;->INSTANCE:Lcom/squareup/loyalty/ui/RewardWrapper$Type$AppliedToCart;

    check-cast p4, Lcom/squareup/loyalty/ui/RewardWrapper$Type;

    .line 40
    invoke-direct {p2, p3, p1, p4}, Lcom/squareup/loyalty/ui/RewardWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/ui/RewardWrapper$Type;)V

    goto :goto_0

    :cond_0
    int-to-long p2, p3

    .line 49
    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->points:Ljava/lang/Long;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, p2, v2

    if-ltz v4, :cond_1

    new-instance p2, Lcom/squareup/loyalty/ui/RewardWrapper;

    .line 50
    iget-object p3, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->name:Ljava/lang/String;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 52
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->points:Ljava/lang/Long;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 53
    sget p1, Lcom/squareup/loyalty/R$string;->points_redeem_button:I

    .line 51
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    .line 55
    new-instance v0, Lcom/squareup/loyalty/ui/RewardWrapper$Type$Clickable;

    invoke-direct {v0, p4}, Lcom/squareup/loyalty/ui/RewardWrapper$Type$Clickable;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/loyalty/ui/RewardWrapper$Type;

    .line 49
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/loyalty/ui/RewardWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/ui/RewardWrapper$Type;)V

    goto :goto_0

    .line 58
    :cond_1
    new-instance p2, Lcom/squareup/loyalty/ui/RewardWrapper;

    .line 59
    iget-object p3, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->name:Ljava/lang/String;

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object p4, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 61
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->points:Ljava/lang/Long;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 62
    sget p1, Lcom/squareup/loyalty/R$string;->points_redeem_button_disabled:I

    .line 60
    invoke-virtual {p4, v0, v1, p1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    .line 64
    sget-object p4, Lcom/squareup/loyalty/ui/RewardWrapper$Type$Disabled;->INSTANCE:Lcom/squareup/loyalty/ui/RewardWrapper$Type$Disabled;

    check-cast p4, Lcom/squareup/loyalty/ui/RewardWrapper$Type;

    .line 58
    invoke-direct {p2, p3, p1, p4}, Lcom/squareup/loyalty/ui/RewardWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/ui/RewardWrapper$Type;)V

    :goto_0
    return-object p2
.end method

.method static synthetic createRewardWrapper$default(Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/protos/client/loyalty/RewardTier;ZILkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/loyalty/ui/RewardWrapper;
    .locals 0

    if-nez p6, :cond_1

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x0

    .line 37
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->createRewardWrapper(Lcom/squareup/protos/client/loyalty/RewardTier;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/loyalty/ui/RewardWrapper;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: createRewardWrapper"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final createRewardWrapperList(Lrx/Observable;Lcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/checkout/HoldsCoupons;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;>;"
        }
    .end annotation

    const-string v0, "contactPointTotal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applyEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 92
    iget-object v1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/RewardTier;

    const/4 v3, 0x0

    if-eqz p2, :cond_1

    move-object v4, p1

    goto :goto_1

    .line 96
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v4

    :goto_1
    if-eqz p2, :cond_2

    .line 99
    iget-object v5, v2, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    invoke-interface {p2, v5}, Lcom/squareup/checkout/HoldsCoupons;->getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    :goto_2
    check-cast v5, Ljava/util/Collection;

    const/4 v6, 0x1

    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    const/4 v3, 0x1

    :cond_4
    xor-int/2addr v3, v6

    .line 102
    new-instance v5, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;

    invoke-direct {v5, p0, v2, v3, p3}, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;-><init>(Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/server/account/protos/RewardTier;ZLkotlin/jvm/functions/Function1;)V

    check-cast v5, Lrx/functions/Func1;

    invoke-virtual {v4, v5}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v2

    .line 101
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 117
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.just(ArrayList())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 120
    :cond_6
    check-cast v0, Ljava/util/List;

    sget-object p1, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$2;->INSTANCE:Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$2;

    check-cast p1, Lrx/functions/FuncN;

    .line 119
    invoke-static {v0, p1}, Lrx/Observable;->combineLatest(Ljava/util/List;Lrx/functions/FuncN;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026       .toList()\n      })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    return-object p1
.end method

.method public final getLoyaltySettings()Lcom/squareup/loyalty/LoyaltySettings;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-object v0
.end method

.method public final getPointsTermsFormatter()Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-object v0
.end method
