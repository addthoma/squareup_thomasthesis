.class final Lcom/squareup/loyalty/ui/CounterView$1;
.super Ljava/lang/Object;
.source "CounterView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/ui/CounterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyalty/ui/CounterView;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/ui/CounterView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/ui/CounterView$1;->this$0:Lcom/squareup/loyalty/ui/CounterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .line 56
    iget-object v0, p0, Lcom/squareup/loyalty/ui/CounterView$1;->this$0:Lcom/squareup/loyalty/ui/CounterView;

    invoke-static {v0}, Lcom/squareup/loyalty/ui/CounterView;->access$getCurrentCount$p(Lcom/squareup/loyalty/ui/CounterView;)I

    move-result p1

    add-int/lit8 v1, p1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/squareup/loyalty/ui/CounterView;->update$default(Lcom/squareup/loyalty/ui/CounterView;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 58
    iget-object p1, p0, Lcom/squareup/loyalty/ui/CounterView$1;->this$0:Lcom/squareup/loyalty/ui/CounterView;

    invoke-virtual {p1}, Lcom/squareup/loyalty/ui/CounterView;->getCountChangeListener()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/ui/CounterView$1;->this$0:Lcom/squareup/loyalty/ui/CounterView;

    invoke-static {v0}, Lcom/squareup/loyalty/ui/CounterView;->access$getCurrentCount$p(Lcom/squareup/loyalty/ui/CounterView;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
