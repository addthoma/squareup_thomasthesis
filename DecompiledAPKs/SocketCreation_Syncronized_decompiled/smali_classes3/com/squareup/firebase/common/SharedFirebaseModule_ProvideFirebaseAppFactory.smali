.class public final Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;
.super Ljava/lang/Object;
.source "SharedFirebaseModule_ProvideFirebaseAppFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/google/firebase/FirebaseApp;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFirebaseApp(Landroid/app/Application;)Lcom/google/firebase/FirebaseApp;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/firebase/common/SharedFirebaseModule;->provideFirebaseApp(Landroid/app/Application;)Lcom/google/firebase/FirebaseApp;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/firebase/FirebaseApp;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/google/firebase/FirebaseApp;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;->provideFirebaseApp(Landroid/app/Application;)Lcom/google/firebase/FirebaseApp;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/firebase/common/SharedFirebaseModule_ProvideFirebaseAppFactory;->get()Lcom/google/firebase/FirebaseApp;

    move-result-object v0

    return-object v0
.end method
