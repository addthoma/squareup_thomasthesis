.class final Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;
.super Ljava/lang/Object;
.source "FcmPushService.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/firebase/fcm/FcmPushService;->instanceIdPushToken()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012 \u0010\u0002\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/google/android/gms/tasks/Task;",
        "Lcom/google/firebase/iid/InstanceIdResult;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/firebase/fcm/FcmPushService;


# direct methods
.method constructor <init>(Lcom/squareup/firebase/fcm/FcmPushService;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/google/android/gms/tasks/Task<",
            "Lcom/google/firebase/iid/InstanceIdResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/FcmPushService;->access$getPushServiceAvailability$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/squareup/push/PushServiceAvailability;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/push/PushServiceAvailability;->isAvailable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "[PUSH][FCM] Google Play services is not available."

    .line 88
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "The push service is not available"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->onError(Ljava/lang/Throwable;)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/FcmPushService;->access$getFirebaseMessaging$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/messaging/FirebaseMessaging;->isAutoInitEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "[PUSH][FCM] Turning auto init on."

    .line 93
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/FcmPushService;->access$getFirebaseMessaging$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/firebase/messaging/FirebaseMessaging;->setAutoInitEnabled(Z)V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/FcmPushService;->access$getFirebaseInstanceId$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/iid/FirebaseInstanceId;->getInstanceId()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method
