.class public final Lcom/squareup/firebase/fcm/FcmPushService;
.super Ljava/lang/Object;
.source "FcmPushService.kt"

# interfaces
.implements Lcom/squareup/push/PushService;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0001\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001cH\u0016J\u000e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u001fH\u0002J\u0008\u0010 \u001a\u00020!H\u0007J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u001cH\u0016J\u0015\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000bH\u0000\u00a2\u0006\u0002\u0008&R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0012@@X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017R\u001c\u0010\u0018\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00120\u00120\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/firebase/fcm/FcmPushService;",
        "Lcom/squareup/push/PushService;",
        "pushServiceAvailability",
        "Lcom/squareup/push/PushServiceAvailability;",
        "firebaseMessaging",
        "Lcom/google/firebase/messaging/FirebaseMessaging;",
        "firebaseInstanceId",
        "Lcom/google/firebase/iid/FirebaseInstanceId;",
        "(Lcom/squareup/push/PushServiceAvailability;Lcom/google/firebase/messaging/FirebaseMessaging;Lcom/google/firebase/iid/FirebaseInstanceId;)V",
        "messageRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/push/PushServiceMessage;",
        "kotlin.jvm.PlatformType",
        "pushGateway",
        "Lcom/squareup/push/PushGateway;",
        "getPushGateway",
        "()Lcom/squareup/push/PushGateway;",
        "value",
        "",
        "pushToken",
        "getPushToken$fcm_release",
        "()Ljava/lang/String;",
        "setPushToken$fcm_release",
        "(Ljava/lang/String;)V",
        "pushTokenRelay",
        "disable",
        "Lio/reactivex/Completable;",
        "enable",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/push/PushServiceRegistration;",
        "instanceIdPushToken",
        "Lio/reactivex/Single;",
        "isEnabled",
        "",
        "observeMessages",
        "sendMessage",
        "",
        "message",
        "sendMessage$fcm_release",
        "fcm_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final firebaseInstanceId:Lcom/google/firebase/iid/FirebaseInstanceId;

.field private final firebaseMessaging:Lcom/google/firebase/messaging/FirebaseMessaging;

.field private final messageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/push/PushServiceMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final pushGateway:Lcom/squareup/push/PushGateway;

.field private final pushServiceAvailability:Lcom/squareup/push/PushServiceAvailability;

.field private pushToken:Ljava/lang/String;

.field private final pushTokenRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/push/PushServiceAvailability;Lcom/google/firebase/messaging/FirebaseMessaging;Lcom/google/firebase/iid/FirebaseInstanceId;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushServiceAvailability"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseMessaging"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firebaseInstanceId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushServiceAvailability:Lcom/squareup/push/PushServiceAvailability;

    iput-object p2, p0, Lcom/squareup/firebase/fcm/FcmPushService;->firebaseMessaging:Lcom/google/firebase/messaging/FirebaseMessaging;

    iput-object p3, p0, Lcom/squareup/firebase/fcm/FcmPushService;->firebaseInstanceId:Lcom/google/firebase/iid/FirebaseInstanceId;

    .line 32
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<PushServiceMessage>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService;->messageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 33
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<String>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushTokenRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 71
    sget-object p1, Lcom/squareup/push/PushGateway;->GCM:Lcom/squareup/push/PushGateway;

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushGateway:Lcom/squareup/push/PushGateway;

    const-string p1, ""

    .line 81
    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushToken:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getFirebaseInstanceId$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/iid/FirebaseInstanceId;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->firebaseInstanceId:Lcom/google/firebase/iid/FirebaseInstanceId;

    return-object p0
.end method

.method public static final synthetic access$getFirebaseMessaging$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/messaging/FirebaseMessaging;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->firebaseMessaging:Lcom/google/firebase/messaging/FirebaseMessaging;

    return-object p0
.end method

.method public static final synthetic access$getPushServiceAvailability$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/squareup/push/PushServiceAvailability;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushServiceAvailability:Lcom/squareup/push/PushServiceAvailability;

    return-object p0
.end method

.method private final instanceIdPushToken()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 86
    new-instance v0, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$1;-><init>(Lcom/squareup/firebase/fcm/FcmPushService;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    .line 99
    sget-object v1, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$2;->INSTANCE:Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 100
    sget-object v1, Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$3;->INSTANCE:Lcom/squareup/firebase/fcm/FcmPushService$instanceIdPushToken$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single\n        .create<T\u2026        .map { it.token }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public disable()Lio/reactivex/Completable;
    .locals 2

    .line 51
    invoke-virtual {p0}, Lcom/squareup/firebase/fcm/FcmPushService;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 56
    :cond_0
    new-instance v0, Lcom/squareup/firebase/fcm/FcmPushService$disable$1;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/fcm/FcmPushService$disable$1;-><init>(Lcom/squareup/firebase/fcm/FcmPushService;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable\n        .fro\u2026d()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public enable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/push/PushServiceRegistration;",
            ">;"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Lcom/squareup/firebase/fcm/FcmPushService;->instanceIdPushToken()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    iget-object v1, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushTokenRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/squareup/firebase/fcm/FcmPushService$enable$1;->INSTANCE:Lcom/squareup/firebase/fcm/FcmPushService$enable$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.merge(instanc\u2026              }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPushGateway()Lcom/squareup/push/PushGateway;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushGateway:Lcom/squareup/push/PushGateway;

    return-object v0
.end method

.method public final getPushToken$fcm_release()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushToken:Ljava/lang/String;

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->firebaseMessaging:Lcom/google/firebase/messaging/FirebaseMessaging;

    invoke-virtual {v0}, Lcom/google/firebase/messaging/FirebaseMessaging;->isAutoInitEnabled()Z

    move-result v0

    return v0
.end method

.method public observeMessages()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/push/PushServiceMessage;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->messageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final sendMessage$fcm_release(Lcom/squareup/push/PushServiceMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->messageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final setPushToken$fcm_release(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService;->pushTokenRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
