.class final Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;
.super Lkotlin/jvm/internal/Lambda;
.source "DateRangeSelectorLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->showRendering(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;

.field final synthetic this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;->this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;->$rendering:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 49
    iget-object v0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;->$rendering:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;

    invoke-virtual {v0}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;->getOnExitWithSelection()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;->this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;

    invoke-static {v1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->access$getCheckableGroup$p(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;)Lcom/squareup/noho/NohoCheckableGroup;

    move-result-object v2

    const-string v3, "checkableGroup"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/noho/NohoCheckableGroup;->getCheckedId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->access$dateRangeForId(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;I)Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
