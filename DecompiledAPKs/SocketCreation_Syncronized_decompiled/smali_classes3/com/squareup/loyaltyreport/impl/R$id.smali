.class public final Lcom/squareup/loyaltyreport/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltyreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final date_range_checkable_group:I = 0x7f0a054e

.field public static final date_range_label:I = 0x7f0a054f

.field public static final date_range_row_last_month:I = 0x7f0a0550

.field public static final date_range_row_last_year:I = 0x7f0a0551

.field public static final date_range_row_this_month:I = 0x7f0a0552

.field public static final date_range_row_this_year:I = 0x7f0a0553

.field public static final guideline0:I = 0x7f0a07c2

.field public static final guideline1:I = 0x7f0a07c3

.field public static final location_label:I = 0x7f0a095b

.field public static final loyalty_report_average_spend_label:I = 0x7f0a0983

.field public static final loyalty_report_average_visits_count_label:I = 0x7f0a0984

.field public static final loyalty_report_error_message_view:I = 0x7f0a0985

.field public static final loyalty_report_initial_circle:I = 0x7f0a0986

.field public static final loyalty_report_loyalty_customers_count_label:I = 0x7f0a0987

.field public static final loyalty_report_progress_bar:I = 0x7f0a0988

.field public static final loyalty_report_recycler_view:I = 0x7f0a0989

.field public static final loyalty_report_row_icon_initials_title_label:I = 0x7f0a098a

.field public static final loyalty_report_row_icon_initials_value_label:I = 0x7f0a098b

.field public static final loyalty_report_row_title_label:I = 0x7f0a098c

.field public static final loyalty_report_row_value_label:I = 0x7f0a098d

.field public static final loyalty_report_summary_label:I = 0x7f0a098e

.field public static final loyalty_report_summary_loyalty_customers_label:I = 0x7f0a098f

.field public static final loyalty_report_view_in_dashboard_button:I = 0x7f0a0990

.field public static final stable_action_bar:I = 0x7f0a0f1c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
