.class public final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealLoyaltyReportWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;"
        }
    .end annotation

    .line 52
    new-instance v7, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;)Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;
    .locals 8

    .line 58
    new-instance v7, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v6}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->newInstance(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;)Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow_Factory;->get()Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    move-result-object v0

    return-object v0
.end method
