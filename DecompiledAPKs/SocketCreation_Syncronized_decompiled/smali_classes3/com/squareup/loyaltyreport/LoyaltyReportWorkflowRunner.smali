.class public interface abstract Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;
.super Ljava/lang/Object;
.source "LoyaltyReportWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008f\u0018\u0000 \u00062\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0006J\u0015\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0002H&\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "",
        "startWorkflow",
        "loyaltyReportProps",
        "(Lkotlin/Unit;)V",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;->Companion:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startWorkflow(Lkotlin/Unit;)V
.end method
