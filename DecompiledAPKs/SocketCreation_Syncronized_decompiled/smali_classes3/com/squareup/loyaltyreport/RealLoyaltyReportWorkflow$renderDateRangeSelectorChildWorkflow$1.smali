.class final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLoyaltyReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->renderDateRangeSelectorChildWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltyreport/LoyaltyReportState;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "",
        "output",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

.field final synthetic this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/LoyaltyReportState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->$state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->$state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

    .line 144
    instance-of v1, v0, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderDateSelector;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderDateSelector;

    invoke-virtual {v0}, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderDateSelector;->getLoyaltyReport()Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 147
    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->$state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

    invoke-virtual {v2}, Lcom/squareup/loyaltyreport/LoyaltyReportState;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_1

    .line 150
    :cond_1
    iget-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->$state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

    invoke-virtual {v1}, Lcom/squareup/loyaltyreport/LoyaltyReportState;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$renderReportAction(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 148
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$loadAction(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;->invoke(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
