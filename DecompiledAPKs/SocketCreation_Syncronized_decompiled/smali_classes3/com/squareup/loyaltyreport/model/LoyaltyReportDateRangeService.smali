.class public final Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;
.super Ljava/lang/Object;
.source "LoyaltyReportDateRangeService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "localDateConverter",
        "Lcom/squareup/util/threeten/LocalDateConverter;",
        "(Lcom/squareup/util/Clock;Lcom/squareup/util/threeten/LocalDateConverter;)V",
        "dateRangeNow",
        "Lcom/squareup/protos/common/time/DateTimeInterval;",
        "loyaltyReportDateRange",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/util/threeten/LocalDateConverter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localDateConverter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->clock:Lcom/squareup/util/Clock;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    return-void
.end method


# virtual methods
.method public final dateRangeNow(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/protos/common/time/DateTimeInterval;
    .locals 5

    const-string v0, "loyaltyReportDateRange"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDate;->ofEpochDay(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    const-string v2, "now"

    const-wide/16 v3, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 52
    invoke-virtual {v0, v3, v4}, Lorg/threeten/bp/LocalDate;->plusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 53
    new-instance v1, Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 54
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    .line 55
    iget-object v2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    const-string v3, "nextMonth"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    .line 53
    invoke-direct {v1, v0, p1}, Lcom/squareup/protos/common/time/DateTimeInterval;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 44
    :cond_1
    invoke-virtual {v0, v3, v4}, Lorg/threeten/bp/LocalDate;->minusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string v1, "lastMonth"

    .line 45
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 46
    new-instance v1, Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 47
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    invoke-virtual {v3, p1}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    .line 48
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    .line 46
    invoke-direct {v1, p1, v0}, Lcom/squareup/protos/common/time/DateTimeInterval;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_0

    .line 37
    :cond_2
    invoke-virtual {v0, v3, v4}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 38
    new-instance v1, Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 39
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    .line 40
    iget-object v2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    const-string v3, "nextYear"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    .line 38
    invoke-direct {v1, v0, p1}, Lcom/squareup/protos/common/time/DateTimeInterval;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V

    goto :goto_0

    .line 29
    :cond_3
    invoke-virtual {v0, v3, v4}, Lorg/threeten/bp/LocalDate;->minusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string v1, "lastYearLocalDate"

    .line 30
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 31
    new-instance v1, Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 32
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    invoke-virtual {v3, p1}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    .line 33
    iget-object v3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->localDateConverter:Lcom/squareup/util/threeten/LocalDateConverter;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/util/threeten/LocalDateConverter;->asDateTime12am(Lorg/threeten/bp/LocalDate;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    .line 31
    invoke-direct {v1, p1, v0}, Lcom/squareup/protos/common/time/DateTimeInterval;-><init>(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V

    :goto_0
    return-object v1
.end method
