.class final Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InMemoryLocalOrderStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->saveSearchOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/SearchQuery;Z)Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isPaginationResult:Z

.field final synthetic $orders:Ljava/util/List;

.field final synthetic $query:Lcom/squareup/ordermanagerdata/SearchQuery;

.field final synthetic this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/ordermanagerdata/SearchQuery;ZLjava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    iput-boolean p3, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$isPaginationResult:Z

    iput-object p4, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$orders:Ljava/util/List;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 284
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getMostRecentSearchQuery$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$isPaginationResult:Z

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getSearchResultOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    const-string v1, "(searchResultOrdersRelay.value ?: emptyList())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$orders:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$orders:Ljava/util/List;

    .line 289
    :goto_1
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    invoke-static {v1, v2}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$setMostRecentSearchQuery$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;Lcom/squareup/ordermanagerdata/SearchQuery;)V

    .line 290
    check-cast v0, Ljava/lang/Iterable;

    .line 291
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->$query:Lcom/squareup/ordermanagerdata/SearchQuery;

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/SearchQuery;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getCOMPLETED_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v1

    goto :goto_2

    .line 292
    :cond_2
    invoke-static {}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStoreKt;->access$getACTIVE_ORDER_COMPARATOR$p()Ljava/util/Comparator;

    move-result-object v1

    .line 290
    :goto_2
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore$saveSearchOrders$1;->this$0:Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;->access$getSearchResultOrdersRelay$p(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
