.class public final Lcom/squareup/ordermanagerdata/SearchQuery;
.super Ljava/lang/Object;
.source "SearchQuery.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J?\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "",
        "searchTerm",
        "",
        "searchRangeMillis",
        "",
        "orderGroup",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        "fulfillment",
        "Lcom/squareup/orders/model/Order$FulfillmentType;",
        "source",
        "Lcom/squareup/protos/client/orders/Channel;",
        "(Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;)V",
        "getFulfillment",
        "()Lcom/squareup/orders/model/Order$FulfillmentType;",
        "getOrderGroup",
        "()Lcom/squareup/protos/client/orders/OrderGroup;",
        "getSearchRangeMillis",
        "()J",
        "getSearchTerm",
        "()Ljava/lang/String;",
        "getSource",
        "()Lcom/squareup/protos/client/orders/Channel;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

.field private final orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

.field private final searchRangeMillis:J

.field private final searchTerm:Ljava/lang/String;

.field private final source:Lcom/squareup/protos/client/orders/Channel;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;)V
    .locals 1

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderGroup"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    iput-wide p2, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    iput-object p4, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    iput-object p5, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    iput-object p6, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;ILjava/lang/Object;)Lcom/squareup/ordermanagerdata/SearchQuery;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-wide p2, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p4, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    :cond_2
    move-object p8, p4

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    :cond_3
    move-object v2, p5

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p6, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    :cond_4
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    move-object p6, p8

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ordermanagerdata/SearchQuery;->copy(Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    return-wide v0
.end method

.method public final component3()Lcom/squareup/protos/client/orders/OrderGroup;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object v0
.end method

.method public final component4()Lcom/squareup/orders/model/Order$FulfillmentType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/client/orders/Channel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ordermanagerdata/SearchQuery;
    .locals 8

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderGroup"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ordermanagerdata/SearchQuery;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ordermanagerdata/SearchQuery;-><init>(Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ordermanagerdata/SearchQuery;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ordermanagerdata/SearchQuery;

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    iget-wide v2, p1, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    iget-object p1, p1, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFulfillment()Lcom/squareup/orders/model/Order$FulfillmentType;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object v0
.end method

.method public final getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    return-object v0
.end method

.method public final getSearchRangeMillis()J
    .locals 2

    .line 12
    iget-wide v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    return-wide v0
.end method

.method public final getSearchTerm()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    return-object v0
.end method

.method public final getSource()Lcom/squareup/protos/client/orders/Channel;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    invoke-static {v2, v3}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchQuery(searchTerm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchTerm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", searchRangeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->searchRangeMillis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", orderGroup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->orderGroup:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fulfillment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->fulfillment:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SearchQuery;->source:Lcom/squareup/protos/client/orders/Channel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
