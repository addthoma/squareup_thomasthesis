.class public Lcom/squareup/marketfont/MarketCheckedTextView;
.super Landroidx/appcompat/widget/AppCompatCheckedTextView;
.source "MarketCheckedTextView.java"


# static fields
.field private static final DEF_STYLE:I


# instance fields
.field private weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    const v0, 0x1010084

    goto :goto_0

    :cond_0
    const v0, 0x10103c8

    :goto_0
    sput v0, Lcom/squareup/marketfont/MarketCheckedTextView;->DEF_STYLE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p1, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 23
    sget v0, Lcom/squareup/marketfont/MarketCheckedTextView;->DEF_STYLE:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    sget-object v0, Lcom/squareup/marketfont/R$styleable;->MarketCheckedTextView:[I

    sget v1, Lcom/squareup/marketfont/R$styleable;->MarketCheckedTextView_weight:I

    invoke-static {p1, p2, v0, v1, p3}, Lcom/squareup/marketfont/MarketUtils;->getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p1

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method


# virtual methods
.method public getWeight()Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/marketfont/MarketCheckedTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 0

    .line 51
    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketUtils;->ensureSpannableTextType(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)Landroid/widget/TextView$BufferType;

    move-result-object p2

    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatCheckedTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 0

    .line 42
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatCheckedTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 43
    iget-object p1, p0, Lcom/squareup/marketfont/MarketCheckedTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method public setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/marketfont/MarketCheckedTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    if-eq p1, v0, :cond_0

    .line 36
    iput-object p1, p0, Lcom/squareup/marketfont/MarketCheckedTextView;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 37
    invoke-static {p0, p1}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    :cond_0
    return-void
.end method
