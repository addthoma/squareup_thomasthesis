.class public final Lcom/squareup/coordinators/Coordinators;
.super Ljava/lang/Object;
.source "Coordinators.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V
    .locals 1

    .line 35
    invoke-interface {p1, p0}, Lcom/squareup/coordinators/CoordinatorProvider;->provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 40
    :cond_0
    new-instance v0, Lcom/squareup/coordinators/Binding;

    invoke-direct {v0, p1, p0}, Lcom/squareup/coordinators/Binding;-><init>(Lcom/squareup/coordinators/Coordinator;Landroid/view/View;)V

    .line 41
    invoke-virtual {p0, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 45
    invoke-static {p0}, Lcom/squareup/coordinators/Coordinators;->isAttachedToWindow(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 46
    invoke-interface {v0, p0}, Landroid/view/View$OnAttachStateChangeListener;->onViewAttachedToWindow(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public static getCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 64
    sget v0, Lcom/squareup/coordinators/R$id;->coordinator:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/coordinators/Coordinator;

    return-object p0
.end method

.method public static installBinder(Landroid/view/ViewGroup;Lcom/squareup/coordinators/CoordinatorProvider;)V
    .locals 3

    .line 56
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 58
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    :cond_0
    new-instance v0, Lcom/squareup/coordinators/Binder;

    invoke-direct {v0, p1}, Lcom/squareup/coordinators/Binder;-><init>(Lcom/squareup/coordinators/CoordinatorProvider;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    return-void
.end method

.method private static isAttachedToWindow(Landroid/view/View;)Z
    .locals 2

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0

    .line 71
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result p0

    return p0
.end method
