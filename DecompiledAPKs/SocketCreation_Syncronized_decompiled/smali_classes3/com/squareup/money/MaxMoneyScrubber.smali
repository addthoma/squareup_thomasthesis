.class public final Lcom/squareup/money/MaxMoneyScrubber;
.super Ljava/lang/Object;
.source "MaxMoneyScrubber.kt"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/money/MaxMoneyScrubber$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMaxMoneyScrubber.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MaxMoneyScrubber.kt\ncom/squareup/money/MaxMoneyScrubber\n*L\n1#1,40:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u000eH\u0016R\u001a\u0010\u0007\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "Lcom/squareup/text/Scrubber;",
        "moneyExtractor",
        "Lcom/squareup/money/MoneyExtractor;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "max",
        "(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V",
        "getMax",
        "()Lcom/squareup/protos/common/Money;",
        "setMax",
        "(Lcom/squareup/protos/common/Money;)V",
        "scrub",
        "",
        "proposed",
        "Companion",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/money/MaxMoneyScrubber$Companion;

.field public static final MAX_MONEY:J

.field private static final MAX_PRICE_DIGITS:I = 0x8


# instance fields
.field private max:Lcom/squareup/protos/common/Money;

.field private final moneyExtractor:Lcom/squareup/money/MoneyExtractor;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/money/MaxMoneyScrubber$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/money/MaxMoneyScrubber$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/money/MaxMoneyScrubber;->Companion:Lcom/squareup/money/MaxMoneyScrubber$Companion;

    const/16 v0, 0x8

    int-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    .line 37
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const/4 v2, 0x1

    int-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-long v0, v0

    sput-wide v0, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/MoneyExtractor;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    const-string v0, "moneyExtractor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "max"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/MaxMoneyScrubber;->moneyExtractor:Lcom/squareup/money/MoneyExtractor;

    iput-object p2, p0, Lcom/squareup/money/MaxMoneyScrubber;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/money/MaxMoneyScrubber;->max:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public final getMax()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/money/MaxMoneyScrubber;->max:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    if-eqz p1, :cond_0

    .line 17
    iget-object v0, p0, Lcom/squareup/money/MaxMoneyScrubber;->moneyExtractor:Lcom/squareup/money/MoneyExtractor;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Lcom/squareup/money/MoneyExtractor;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/money/MaxMoneyScrubber;->max:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v3, "max.amount"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 19
    iget-object p1, p0, Lcom/squareup/money/MaxMoneyScrubber;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/money/MaxMoneyScrubber;->max:Lcom/squareup/protos/common/Money;

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 20
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public final setMax(Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iput-object p1, p0, Lcom/squareup/money/MaxMoneyScrubber;->max:Lcom/squareup/protos/common/Money;

    return-void
.end method
