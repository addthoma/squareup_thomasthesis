.class public final Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;
.super Ljava/lang/Object;
.source "MoneyModule_ProvideTaxPercentageFormatterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/util/Percentage;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTaxPercentageFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyModule;->provideTaxPercentageFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/text/Formatter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/text/Formatter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->provideTaxPercentageFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/money/MoneyModule_ProvideTaxPercentageFormatterFactory;->get()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method
