.class final Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;
.super Lcom/squareup/money/WholeUnitAmountScrubber;
.source "WholeUnitMoneyHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/money/WholeUnitMoneyHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MoneyScrubber"
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;J",
            "Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;",
            ")V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p3, p4, p5}, Lcom/squareup/money/WholeUnitAmountScrubber;-><init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    .line 38
    iput-object p1, p0, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 39
    iput-object p2, p0, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method protected formatAmount(J)Ljava/lang/String;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    int-to-long v0, v0

    mul-long p1, p1, v0

    .line 44
    iget-object v0, p0, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 45
    iget-object p2, p0, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
