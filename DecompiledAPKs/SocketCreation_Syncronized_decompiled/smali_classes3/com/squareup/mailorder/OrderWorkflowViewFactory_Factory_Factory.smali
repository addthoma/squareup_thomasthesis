.class public final Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;
.super Ljava/lang/Object;
.source "OrderWorkflowViewFactory_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final correctedAddressCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final orderConfirmationCoordinatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final orderCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final unverifiedAddressCoordinatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->orderCoordinatorFactoryProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->correctedAddressCoordinatorFactoryProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->unverifiedAddressCoordinatorProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->orderConfirmationCoordinatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;-><init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->orderCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mailorder/OrderCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->correctedAddressCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->unverifiedAddressCoordinatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    iget-object v3, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->orderConfirmationCoordinatorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->newInstance(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderWorkflowViewFactory_Factory_Factory;->get()Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;

    move-result-object v0

    return-object v0
.end method
