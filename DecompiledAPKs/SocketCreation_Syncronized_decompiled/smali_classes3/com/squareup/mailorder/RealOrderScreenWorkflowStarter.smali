.class public final Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "RealOrderScreenWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/mailorder/OrderScreenWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008JZ\u0010\t\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\u000c0\u000b\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\nj\u0002`\u00122\u001c\u0010\u0013\u001a\u0018\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00110\nj\u0002`\u0016H\u0002JD\u0010\t\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\u000c0\u000b\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\nj\u0002`\u00122\u0006\u0010\u0017\u001a\u00020\u0018H\u0016JD\u0010\t\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\u000c0\u000b\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\nj\u0002`\u00122\u0006\u0010\u0019\u001a\u00020\u001aH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;",
        "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "reactor",
        "Lcom/squareup/mailorder/OrderReactor;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/mailorder/OrderReactor;)V",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "Lcom/squareup/mailorder/OrderScreenWorkflow;",
        "workflow",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "Lcom/squareup/mailorder/OrderEvent;",
        "Lcom/squareup/mailorder/OrderWorkflow;",
        "startArg",
        "Lcom/squareup/mailorder/OrderWorkflowStartArgument;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "ScreenResolver",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ScreenResolver:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final reactor:Lcom/squareup/mailorder/OrderReactor;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->ScreenResolver:Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/mailorder/OrderReactor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reactor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->reactor:Lcom/squareup/mailorder/OrderReactor;

    return-void
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method private final start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "-",
            "Lcom/squareup/mailorder/OrderEvent;",
            "+",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$start$1;-><init>(Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public start(Lcom/squareup/mailorder/OrderWorkflowStartArgument;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderWorkflowStartArgument;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->reactor:Lcom/squareup/mailorder/OrderReactor;

    invoke-virtual {v0, p1}, Lcom/squareup/mailorder/OrderReactor;->startWorkflow(Lcom/squareup/mailorder/OrderWorkflowStartArgument;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->reactor:Lcom/squareup/mailorder/OrderReactor;

    invoke-virtual {v0, p1}, Lcom/squareup/mailorder/OrderReactor;->startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;->start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
