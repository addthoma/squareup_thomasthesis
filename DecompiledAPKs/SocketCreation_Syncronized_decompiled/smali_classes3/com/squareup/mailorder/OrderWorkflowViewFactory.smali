.class public final Lcom/squareup/mailorder/OrderWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "OrderWorkflowViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;,
        Lcom/squareup/mailorder/OrderWorkflowViewFactory$ParentComponent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0002\r\u000eB5\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "orderCoordinatorFactory",
        "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
        "correctedAddressCoordinatorFactory",
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
        "unverifiedAddressCoordinator",
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
        "orderConfirmationCoordinator",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
        "section",
        "Ljava/lang/Class;",
        "(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;Ljava/lang/Class;)V",
        "Factory",
        "ParentComponent",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;Ljava/lang/Class;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
            "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
            "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 17
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 18
    sget-object v2, Lcom/squareup/mailorder/Order;->INSTANCE:Lcom/squareup/mailorder/Order;

    invoke-virtual {v2}, Lcom/squareup/mailorder/Order;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 19
    sget v3, Lcom/squareup/mailorder/R$layout;->order_view:I

    .line 20
    new-instance v16, Lcom/squareup/workflow/ScreenHint;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1f7

    const/4 v15, 0x0

    move-object/from16 v4, v16

    move-object/from16 v8, p5

    invoke-direct/range {v4 .. v15}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    new-instance v4, Lcom/squareup/mailorder/OrderWorkflowViewFactory$1;

    move-object/from16 v5, p1

    invoke-direct {v4, v5}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$1;-><init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object/from16 v4, v16

    .line 17
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 23
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 24
    sget-object v1, Lcom/squareup/mailorder/SelectCorrectedAddress;->INSTANCE:Lcom/squareup/mailorder/SelectCorrectedAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/SelectCorrectedAddress;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 25
    sget v5, Lcom/squareup/mailorder/R$layout;->order_corrected_address_view:I

    .line 26
    new-instance v1, Lcom/squareup/workflow/ScreenHint;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x1f7

    const/16 v17, 0x0

    move-object v6, v1

    move-object/from16 v10, p5

    invoke-direct/range {v6 .. v17}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 27
    new-instance v2, Lcom/squareup/mailorder/OrderWorkflowViewFactory$2;

    move-object/from16 v6, p2

    invoke-direct {v2, v6}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$2;-><init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;)V

    move-object v8, v2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v6, v1

    .line 23
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 29
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 30
    sget-object v1, Lcom/squareup/mailorder/OrderConfirmation;->INSTANCE:Lcom/squareup/mailorder/OrderConfirmation;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderConfirmation;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 31
    sget v5, Lcom/squareup/mailorder/R$layout;->order_confirmation_view:I

    .line 32
    new-instance v1, Lcom/squareup/workflow/ScreenHint;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v6, v1

    move-object/from16 v10, p5

    invoke-direct/range {v6 .. v17}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 33
    new-instance v2, Lcom/squareup/mailorder/OrderWorkflowViewFactory$3;

    move-object/from16 v6, p4

    invoke-direct {v2, v6}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$3;-><init>(Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)V

    move-object v8, v2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v6, v1

    .line 29
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 35
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 36
    sget-object v1, Lcom/squareup/mailorder/UnverifiedAddress;->INSTANCE:Lcom/squareup/mailorder/UnverifiedAddress;

    invoke-virtual {v1}, Lcom/squareup/mailorder/UnverifiedAddress;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 37
    sget v5, Lcom/squareup/mailorder/R$layout;->order_unverified_address_view:I

    .line 38
    new-instance v1, Lcom/squareup/workflow/ScreenHint;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v6, v1

    move-object/from16 v10, p5

    invoke-direct/range {v6 .. v17}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 39
    new-instance v2, Lcom/squareup/mailorder/OrderWorkflowViewFactory$4;

    move-object/from16 v6, p3

    invoke-direct {v2, v6}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$4;-><init>(Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;)V

    move-object v8, v2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v6, v1

    .line 35
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 41
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 42
    sget-object v2, Lcom/squareup/mailorder/OrderWarningDialog;->INSTANCE:Lcom/squareup/mailorder/OrderWarningDialog;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderWarningDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget-object v3, Lcom/squareup/mailorder/OrderWorkflowViewFactory$5;->INSTANCE:Lcom/squareup/mailorder/OrderWorkflowViewFactory$5;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 41
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    move-object/from16 v1, p0

    .line 16
    invoke-direct {v1, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;Ljava/lang/Class;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct/range {p0 .. p5}, Lcom/squareup/mailorder/OrderWorkflowViewFactory;-><init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;Ljava/lang/Class;)V

    return-void
.end method
