.class public final Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;
.super Ljava/lang/Object;
.source "OrderHubAlertsFrequency.kt"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference$Converter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Converter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter<",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter;",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "()V",
        "deserialize",
        "serialized",
        "",
        "serialize",
        "value",
        "settings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;

    invoke-direct {v0}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;-><init>()V

    sput-object v0, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;->INSTANCE:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Ljava/lang/String;)Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;
    .locals 1

    const-string v0, "serialized"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    :try_start_0
    invoke-static {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->valueOf(Ljava/lang/String;)Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 26
    :catch_0
    sget-object p1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->Companion:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;

    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Companion;->getDEFAULT()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic deserialize(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    .line 21
    invoke-virtual {p0, p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;->deserialize(Ljava/lang/String;)Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->name()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-virtual {p0, p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency$Converter;->serialize(Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
