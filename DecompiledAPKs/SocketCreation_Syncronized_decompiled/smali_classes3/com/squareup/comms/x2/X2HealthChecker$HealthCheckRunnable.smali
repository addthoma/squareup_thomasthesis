.class Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;
.super Ljava/lang/Object;
.source "X2HealthChecker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/x2/X2HealthChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HealthCheckRunnable"
.end annotation


# instance fields
.field private final poster:Lcom/squareup/comms/MessagePoster;

.field private final state:Lcom/squareup/comms/x2/X2HealthState;

.field final synthetic this$0:Lcom/squareup/comms/x2/X2HealthChecker;


# direct methods
.method constructor <init>(Lcom/squareup/comms/x2/X2HealthChecker;Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/x2/X2HealthState;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->this$0:Lcom/squareup/comms/x2/X2HealthChecker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->poster:Lcom/squareup/comms/MessagePoster;

    .line 81
    iput-object p3, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->state:Lcom/squareup/comms/x2/X2HealthState;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 85
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->state:Lcom/squareup/comms/x2/X2HealthState;

    invoke-virtual {v0}, Lcom/squareup/comms/x2/X2HealthState;->isStopped()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 90
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->poster:Lcom/squareup/comms/MessagePoster;

    new-instance v1, Lcom/squareup/comms/protos/common/HealthCheckRequest;

    invoke-direct {v1}, Lcom/squareup/comms/protos/common/HealthCheckRequest;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/comms/MessagePoster;->post(Lcom/squareup/wire/Message;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->state:Lcom/squareup/comms/x2/X2HealthState;

    invoke-virtual {v0}, Lcom/squareup/comms/x2/X2HealthState;->onRequestSend()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Health check error, ignoring"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 93
    invoke-static {v0, v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    :goto_0
    iget-object v0, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->this$0:Lcom/squareup/comms/x2/X2HealthChecker;

    invoke-static {v0}, Lcom/squareup/comms/x2/X2HealthChecker;->access$100(Lcom/squareup/comms/x2/X2HealthChecker;)Lcom/squareup/comms/common/IoThread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->this$0:Lcom/squareup/comms/x2/X2HealthChecker;

    invoke-static {v1}, Lcom/squareup/comms/x2/X2HealthChecker;->access$000(Lcom/squareup/comms/x2/X2HealthChecker;)I

    move-result v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, v1, v2}, Lcom/squareup/comms/common/IoThread;->schedule(Ljava/lang/Runnable;ILjava/util/concurrent/TimeUnit;)V

    return-void

    :goto_1
    iget-object v1, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->this$0:Lcom/squareup/comms/x2/X2HealthChecker;

    invoke-static {v1}, Lcom/squareup/comms/x2/X2HealthChecker;->access$100(Lcom/squareup/comms/x2/X2HealthChecker;)Lcom/squareup/comms/common/IoThread;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/comms/x2/X2HealthChecker$HealthCheckRunnable;->this$0:Lcom/squareup/comms/x2/X2HealthChecker;

    invoke-static {v2}, Lcom/squareup/comms/x2/X2HealthChecker;->access$000(Lcom/squareup/comms/x2/X2HealthChecker;)I

    move-result v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p0, v2, v3}, Lcom/squareup/comms/common/IoThread;->schedule(Ljava/lang/Runnable;ILjava/util/concurrent/TimeUnit;)V

    .line 96
    throw v0
.end method
