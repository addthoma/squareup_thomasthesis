.class public final Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion$ADAPTER$1;
.super Lcom/squareup/wire/EnumAdapter;
.source "PaymentPromptVariation.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/PaymentPromptVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/comms/protos/seller/PaymentPromptVariation;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0014\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/comms/protos/seller/PaymentPromptVariation$Companion$ADAPTER$1",
        "Lcom/squareup/wire/EnumAdapter;",
        "Lcom/squareup/comms/protos/seller/PaymentPromptVariation;",
        "fromValue",
        "value",
        "",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lkotlin/reflect/KClass;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/wire/EnumAdapter;-><init>(Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/comms/protos/seller/PaymentPromptVariation;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/comms/protos/seller/PaymentPromptVariation;->Companion:Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion;->fromValue(I)Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/PaymentPromptVariation$Companion$ADAPTER$1;->fromValue(I)Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/WireEnum;

    return-object p1
.end method
