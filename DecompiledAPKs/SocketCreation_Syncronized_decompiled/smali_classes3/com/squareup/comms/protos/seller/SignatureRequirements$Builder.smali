.class public final Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SignatureRequirements.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/SignatureRequirements;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/SignatureRequirements;",
        "Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0012\u001a\u00020\u0002H\u0016J\u0015\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u000b\u001a\u00020\u00002\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u000c\u001a\u00020\u00002\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0015\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0014R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u000c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0011\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/SignatureRequirements;",
        "()V",
        "collect_signature_for_cnp_payments",
        "",
        "Ljava/lang/Boolean;",
        "collect_signature_for_cof_payments",
        "issuer_signature_request",
        "merchant_is_allowed_to_skip_signatures_for_small_payments",
        "merchant_opted_in_to_skip_all_signatures",
        "merchant_opted_in_to_skip_signatures_for_small_payments",
        "sign_on_printed_receipt",
        "signature_agreement",
        "",
        "signature_required_threshold",
        "",
        "Ljava/lang/Long;",
        "build",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;",
        "(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public collect_signature_for_cnp_payments:Ljava/lang/Boolean;

.field public collect_signature_for_cof_payments:Ljava/lang/Boolean;

.field public issuer_signature_request:Ljava/lang/Boolean;

.field public merchant_is_allowed_to_skip_signatures_for_small_payments:Ljava/lang/Boolean;

.field public merchant_opted_in_to_skip_all_signatures:Ljava/lang/Boolean;

.field public merchant_opted_in_to_skip_signatures_for_small_payments:Ljava/lang/Boolean;

.field public sign_on_printed_receipt:Ljava/lang/Boolean;

.field public signature_agreement:Ljava/lang/String;

.field public signature_required_threshold:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 207
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/SignatureRequirements;
    .locals 12

    .line 316
    new-instance v11, Lcom/squareup/comms/protos/seller/SignatureRequirements;

    .line 317
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->collect_signature_for_cnp_payments:Ljava/lang/Boolean;

    .line 318
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->collect_signature_for_cof_payments:Ljava/lang/Boolean;

    .line 319
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->issuer_signature_request:Ljava/lang/Boolean;

    .line 321
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->merchant_is_allowed_to_skip_signatures_for_small_payments:Ljava/lang/Boolean;

    .line 323
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->merchant_opted_in_to_skip_signatures_for_small_payments:Ljava/lang/Boolean;

    .line 324
    iget-object v6, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->signature_required_threshold:Ljava/lang/Long;

    .line 325
    iget-object v7, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->signature_agreement:Ljava/lang/String;

    .line 326
    iget-object v8, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->sign_on_printed_receipt:Ljava/lang/Boolean;

    .line 327
    iget-object v9, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->merchant_opted_in_to_skip_all_signatures:Ljava/lang/Boolean;

    .line 328
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    .line 316
    invoke-direct/range {v0 .. v10}, Lcom/squareup/comms/protos/seller/SignatureRequirements;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->build()Lcom/squareup/comms/protos/seller/SignatureRequirements;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final collect_signature_for_cnp_payments(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->collect_signature_for_cnp_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final collect_signature_for_cof_payments(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->collect_signature_for_cof_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final issuer_signature_request(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->issuer_signature_request:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final merchant_is_allowed_to_skip_signatures_for_small_payments(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->merchant_is_allowed_to_skip_signatures_for_small_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final merchant_opted_in_to_skip_all_signatures(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 312
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->merchant_opted_in_to_skip_all_signatures:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final merchant_opted_in_to_skip_signatures_for_small_payments(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->merchant_opted_in_to_skip_signatures_for_small_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final sign_on_printed_receipt(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 303
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->sign_on_printed_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final signature_agreement(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->signature_agreement:Ljava/lang/String;

    return-object p0
.end method

.method public final signature_required_threshold(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SignatureRequirements$Builder;->signature_required_threshold:Ljava/lang/Long;

    return-object p0
.end method
