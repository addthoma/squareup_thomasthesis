.class public final Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderSwipedCard.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/TenderSwipedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/TenderSwipedCard;",
        "Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/TenderSwipedCard;",
        "()V",
        "card",
        "Lcom/squareup/comms/protos/common/Card;",
        "isGiftCard",
        "",
        "Ljava/lang/Boolean;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/comms/protos/common/Card;

.field public isGiftCard:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/TenderSwipedCard;
    .locals 6

    .line 100
    new-instance v0, Lcom/squareup/comms/protos/seller/TenderSwipedCard;

    .line 101
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;->card:Lcom/squareup/comms/protos/common/Card;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    .line 102
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;->isGiftCard:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 103
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 100
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/seller/TenderSwipedCard;-><init>(Lcom/squareup/comms/protos/common/Card;ZLokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v5, v0, v3

    const-string v1, "isGiftCard"

    aput-object v1, v0, v2

    .line 102
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "card"

    aput-object v1, v0, v2

    .line 101
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;->build()Lcom/squareup/comms/protos/seller/TenderSwipedCard;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final card(Lcom/squareup/comms/protos/common/Card;)Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;->card:Lcom/squareup/comms/protos/common/Card;

    return-object p0
.end method

.method public final isGiftCard(Z)Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;
    .locals 0

    .line 96
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TenderSwipedCard$Builder;->isGiftCard:Ljava/lang/Boolean;

    return-object p0
.end method
