.class public final Lcom/squareup/items/editoption/EditOptionState$InvalidOption;
.super Lcom/squareup/items/editoption/EditOptionState;
.source "EditOptionState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/EditOptionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InvalidOption"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/EditOptionState$InvalidOption$Creator;,
        Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u00002\u00020\u0001:\u0001#B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\tH\u00c6\u0003J1\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\u0013\u0010\u0019\u001a\u00020\u00072\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0018H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\rR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionState$InvalidOption;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "addOptionRowId",
        "",
        "isDisplayNameTrackingName",
        "",
        "reason",
        "Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)V",
        "getAddOptionRowId",
        "()Ljava/lang/String;",
        "()Z",
        "getOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getReason",
        "()Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "InvalidOptionReason",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final addOptionRowId:Ljava/lang/String;

.field private final isDisplayNameTrackingName:Z

.field private final option:Lcom/squareup/cogs/itemoptions/ItemOption;

.field private final reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$Creator;

    invoke-direct {v0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)V
    .locals 1

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addOptionRowId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/EditOptionState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput-object p2, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->addOptionRowId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName:Z

    iput-object p4, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/EditOptionState$InvalidOption;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;ILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getAddOptionRowId()Ljava/lang/String;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName()Z

    move-result p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName()Z

    move-result v0

    return v0
.end method

.method public final component4()Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;
    .locals 1

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addOptionRowId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    iget-object p1, p1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAddOptionRowId()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->addOptionRowId:Ljava/lang/String;

    return-object v0
.end method

.method public getOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final getReason()Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public isDisplayNameTrackingName()Z
    .locals 1

    .line 65
    iget-boolean v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InvalidOption(option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", addOptionRowId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isDisplayNameTrackingName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->addOptionRowId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->isDisplayNameTrackingName:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->reason:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
