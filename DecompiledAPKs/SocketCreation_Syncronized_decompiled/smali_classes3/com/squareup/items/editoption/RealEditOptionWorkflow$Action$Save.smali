.class public final Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;
.super Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.source "RealEditOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Save"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditOptionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,355:1\n1360#2:356\n1429#2,3:357\n1360#2:360\n1429#2,3:361\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save\n*L\n227#1:356\n227#1,3:357\n234#1:360\n234#1,3:361\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\nH\u00c6\u0003J9\u0010\u0018\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u00082\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0004H\u00d6\u0001J\u0018\u0010\u001f\u001a\u00020 *\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020#0!H\u0016R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;",
        "existingOptionSetNames",
        "",
        "",
        "originalOptionSet",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "hasDuplicates",
        "",
        "locale",
        "Ljava/util/Locale;",
        "(Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;)V",
        "getExistingOptionSetNames",
        "()Ljava/util/Set;",
        "getHasDuplicates",
        "()Z",
        "getLocale",
        "()Ljava/util/Locale;",
        "getOriginalOptionSet",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final existingOptionSetNames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final hasDuplicates:Z

.field private final locale:Ljava/util/Locale;

.field private final originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Z",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    const-string v0, "existingOptionSetNames"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 223
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    iput-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput-boolean p3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    iput-object p4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;ILjava/lang/Object;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->copy(Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v0, 0x1

    :goto_2
    const/16 v3, 0xa

    if-nez v0, :cond_4

    .line 226
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    if-eqz v4, :cond_3

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v4, v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_6

    .line 227
    :cond_4
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 357
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 358
    check-cast v2, Ljava/lang/String;

    .line 227
    sget-object v4, Lcom/squareup/cogs/itemoptions/ItemOption;->Companion:Lcom/squareup/cogs/itemoptions/ItemOption$Companion;

    iget-object v5, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v4, v2, v5}, Lcom/squareup/cogs/itemoptions/ItemOption$Companion;->normalizeName(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 359
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 229
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;->DUPLICATE_OPTION_SET_NAME:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionState;->invalidFor(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 234
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 360
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 361
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 362
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 234
    iget-object v3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->normalizedName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 363
    :cond_7
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 234
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 235
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;->EMPTY_OPTION_VALUE_NAME:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionState;->invalidFor(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 239
    :cond_8
    iget-boolean v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    if-eqz v0, :cond_9

    .line 240
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;->DUPLICATE_OPTION_VALUE_NAME:Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionState;->invalidFor(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 243
    :cond_9
    new-instance v0, Lcom/squareup/items/editoption/EditOptionOutput$Save;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/items/editoption/EditOptionOutput$Save;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    return v0
.end method

.method public final component4()Ljava/util/Locale;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public final copy(Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Z",
            "Ljava/util/Locale;",
            ")",
            "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;"
        }
    .end annotation

    const-string v0, "existingOptionSetNames"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;-><init>(Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    iget-object v1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    iget-boolean v1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    iget-object p1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getExistingOptionSetNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 219
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    return-object v0
.end method

.method public final getHasDuplicates()Z
    .locals 1

    .line 221
    iget-boolean v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    return v0
.end method

.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public final getOriginalOptionSet()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Save(existingOptionSetNames="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->existingOptionSetNames:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", originalOptionSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->originalOptionSet:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasDuplicates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->hasDuplicates:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
