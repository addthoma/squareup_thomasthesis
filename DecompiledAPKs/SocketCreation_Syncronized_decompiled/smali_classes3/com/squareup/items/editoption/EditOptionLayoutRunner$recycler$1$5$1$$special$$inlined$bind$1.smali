.class public final Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 EditOptionLayoutRunner.kt\ncom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1\n+ 3 EditOptionValueRow.kt\ncom/squareup/items/editoption/ui/EditOptionValueRow\n*L\n1#1,87:1\n128#2,4:88\n132#2,10:93\n142#2,5:104\n79#3:92\n80#3:103\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context$inlined:Landroid/content/Context;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$context$inlined:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/editoption/ui/EditOptionValueRow;

    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1$lambda$1;

    invoke-direct {v0, p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1$lambda$1;-><init>(Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->setOnDelete(Lkotlin/jvm/functions/Function0;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/editoption/ui/EditOptionValueRow;

    .line 92
    invoke-virtual {p1}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getEditRow()Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    .line 93
    move-object v0, p1

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->getValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 95
    invoke-virtual {p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$context$inlined:Landroid/content/Context;

    sget v1, Lcom/squareup/items/editoption/impl/R$string;->edit_option_new_value_hint:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$context$inlined:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 97
    invoke-virtual {p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isDuplicate()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    sget v1, Lcom/squareup/noho/R$color;->noho_input_error:I

    goto :goto_1

    .line 100
    :cond_1
    sget v1, Lcom/squareup/noho/R$color;->noho_edit_text_color:I

    .line 96
    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setTextColor(I)V

    .line 104
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/editoption/ui/EditOptionValueRow;

    invoke-virtual {p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->setPromoted(Z)V

    .line 105
    invoke-virtual {p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;->isAddValueRow()Z

    move-result p1

    if-nez p1, :cond_2

    .line 106
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/items/editoption/ui/EditOptionValueRow;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->dragHandle:I

    invoke-virtual {p2, v0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.dragHandle)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/squareup/cycler/RecyclerMutationsKt;->dragHandle(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/view/View;)V

    :cond_2
    return-void
.end method
