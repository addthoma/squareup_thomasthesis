.class public final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;
.super Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.source "EditOptionValueWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteOptionValue"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u0004*\u0012\u0012\u0008\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00080\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueState;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;

    invoke-direct {v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;-><init>()V

    sput-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;->INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            "-",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput$OptionValueDeleted;->INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput$OptionValueDeleted;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
