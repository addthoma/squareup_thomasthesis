.class public final Lcom/squareup/items/tutorial/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/tutorial/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final create_item_tutorial_adjust_inventory:I = 0x7f1205c1

.field public static final create_item_tutorial_complete_dialog_button_primary:I = 0x7f1205c2

.field public static final create_item_tutorial_complete_dialog_button_secondary:I = 0x7f1205c3

.field public static final create_item_tutorial_complete_dialog_content:I = 0x7f1205c4

.field public static final create_item_tutorial_complete_dialog_title:I = 0x7f1205c5

.field public static final create_item_tutorial_create_category:I = 0x7f1205c6

.field public static final create_item_tutorial_enter_item_name:I = 0x7f1205c7

.field public static final create_item_tutorial_enter_item_price:I = 0x7f1205c8

.field public static final create_item_tutorial_phone_select_all_items:I = 0x7f1205c9

.field public static final create_item_tutorial_phone_select_create_item:I = 0x7f1205ca

.field public static final create_item_tutorial_phone_select_items:I = 0x7f1205cb

.field public static final create_item_tutorial_save_item:I = 0x7f1205cc

.field public static final create_item_tutorial_skip_tutorial_dialog_button_primary:I = 0x7f1205cd

.field public static final create_item_tutorial_skip_tutorial_dialog_button_secondary:I = 0x7f1205ce

.field public static final create_item_tutorial_skip_tutorial_dialog_content:I = 0x7f1205cf

.field public static final create_item_tutorial_skip_tutorial_dialog_title:I = 0x7f1205d0

.field public static final create_item_tutorial_start_tutorial_dialog_button_primary:I = 0x7f1205d1

.field public static final create_item_tutorial_start_tutorial_dialog_button_secondary:I = 0x7f1205d2

.field public static final create_item_tutorial_start_tutorial_dialog_content:I = 0x7f1205d3

.field public static final create_item_tutorial_start_tutorial_dialog_title:I = 0x7f1205d4

.field public static final create_item_tutorial_tablet_drag_and_drop_item:I = 0x7f1205d6

.field public static final create_item_tutorial_tablet_enter_edit_mode_tooltip:I = 0x7f1205d7

.field public static final create_item_tutorial_tablet_select_create_item:I = 0x7f1205d8

.field public static final create_item_tutorial_tablet_tap_done_editing:I = 0x7f1205d9

.field public static final create_item_tutorial_tablet_tap_empty_tile_tooltip:I = 0x7f1205da


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
