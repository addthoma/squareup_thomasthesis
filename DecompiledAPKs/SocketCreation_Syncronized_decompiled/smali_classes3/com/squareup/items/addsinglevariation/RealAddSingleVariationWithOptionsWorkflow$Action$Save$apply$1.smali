.class final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save$apply$1;
.super Ljava/lang/Object;
.source "RealAddSingleVariationWithOptionsWorkflow.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "o1",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "kotlin.jvm.PlatformType",
        "o2",
        "compare"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $optionsByIds:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save$apply$1;->$optionsByIds:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)I
    .locals 10

    const-string v0, "o1"

    .line 154
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_4

    .line 155
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    const-string v4, "o2"

    .line 156
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 157
    iget-object v5, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    iget-object v6, v4, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    xor-int/2addr v5, v6

    if-eqz v5, :cond_3

    .line 158
    iget-object v5, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save$apply$1;->$optionsByIds:Ljava/util/Map;

    iget-object v7, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 159
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const-string v8, "optionValue"

    .line 160
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 p1, -0x1

    return p1

    .line 162
    :cond_2
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v4, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    return v6

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 139
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save$apply$1;->compare(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)I

    move-result p1

    return p1
.end method
