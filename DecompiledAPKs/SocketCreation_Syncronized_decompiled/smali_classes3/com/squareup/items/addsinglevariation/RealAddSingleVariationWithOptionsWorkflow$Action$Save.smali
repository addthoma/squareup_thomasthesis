.class public final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;
.super Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;
.source "RealAddSingleVariationWithOptionsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Save"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddSingleVariationWithOptionsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddSingleVariationWithOptionsWorkflow.kt\ncom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,206:1\n1360#2:207\n1429#2,3:208\n1360#2:211\n1429#2,3:212\n1360#2:215\n1429#2,3:216\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddSingleVariationWithOptionsWorkflow.kt\ncom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save\n*L\n147#1:207\n147#1,3:208\n171#1:211\n171#1,3:212\n177#1:215\n177#1,3:216\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000f\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005H\u00c6\u0003JC\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00052\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\u0018\u0010\u001e\u001a\u00020\u001f*\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\"0 H\u0016R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;",
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;",
        "newVariation",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "newOptionValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "existingVariation",
        "assignedOptions",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
        "getAssignedOptions",
        "()Ljava/util/List;",
        "getExistingVariation",
        "getNewOptionValues",
        "getNewVariation",
        "()Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignedOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private final existingVariation:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final newOptionValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field

.field private final newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newOptionValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingVariation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignedOptions"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 144
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->copy(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 207
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 208
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 209
    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 147
    new-instance v4, Lkotlin/Pair;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 210
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 148
    invoke-static {v1}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 152
    iget-object v3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v3, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save$apply$1;

    invoke-direct {v3, v0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save$apply$1;-><init>(Ljava/util/Map;)V

    check-cast v3, Ljava/util/Comparator;

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->sortWith(Ljava/util/List;Ljava/util/Comparator;)V

    .line 170
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->withIndex(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v1

    .line 211
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 212
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 213
    check-cast v4, Lkotlin/collections/IndexedValue;

    .line 172
    new-instance v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v4}, Lkotlin/collections/IndexedValue;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-direct {v5, v6}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    .line 173
    invoke-virtual {v4}, Lkotlin/collections/IndexedValue;->getIndex()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 214
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 177
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 215
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 216
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 217
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 178
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->toCatalogItemOptionValueBuilder()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v5

    .line 179
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getOptionId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setOrdinal(Ljava/lang/Integer;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v2

    .line 180
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 218
    :cond_3
    check-cast v4, Ljava/util/List;

    .line 182
    new-instance v0, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;

    invoke-direct {v0, v3, v4}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$VariationCreated;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;"
        }
    .end annotation

    const-string v0, "newVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newOptionValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingVariation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignedOptions"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignedOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    return-object v0
.end method

.method public final getExistingVariation()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    return-object v0
.end method

.method public final getNewOptionValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    return-object v0
.end method

.method public final getNewVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Save(newVariation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newOptionValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->newOptionValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", existingVariation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->existingVariation:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", assignedOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$Save;->assignedOptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
