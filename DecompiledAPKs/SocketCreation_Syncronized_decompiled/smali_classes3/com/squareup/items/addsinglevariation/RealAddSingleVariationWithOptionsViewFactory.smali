.class public final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RealAddSingleVariationWithOptionsViewFactory.kt"

# interfaces
.implements Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsViewFactory;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "selectOptionValuesForVariationViewFactory",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationViewFactory;",
        "selectSingleOptionValueForVariationViewFactory",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationViewFactory;",
        "(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationViewFactory;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationViewFactory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationViewFactory;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationViewFactory;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectOptionValuesForVariationViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectSingleOptionValueForVariationViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 13
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 14
    sget-object v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 15
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 16
    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x3

    aput-object p2, v0, p1

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
