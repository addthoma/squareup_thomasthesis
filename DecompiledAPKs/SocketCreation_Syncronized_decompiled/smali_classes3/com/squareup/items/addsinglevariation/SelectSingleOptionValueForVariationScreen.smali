.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;
.super Ljava/lang/Object;
.source "SelectSingleOptionValueForVariationScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectSingleOptionValueForVariationScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectSingleOptionValueForVariationScreen.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen\n*L\n1#1,26:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008%\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0083\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000c\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012\u00a2\u0006\u0002\u0010\u0015J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012H\u00c6\u0003J\u000f\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012H\u00c6\u0003J\u000f\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010.\u001a\u00020\u0008H\u00c6\u0003J\t\u0010/\u001a\u00020\u0008H\u00c6\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u00101\u001a\u00020\u000cH\u00c6\u0003J\t\u00102\u001a\u00020\u000cH\u00c6\u0003J\u0015\u00103\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000fH\u00c6\u0003J\u000f\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012H\u00c6\u0003J\u009d\u0001\u00105\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c2\u0014\u0008\u0002\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000f2\u000e\u0008\u0002\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u00122\u000e\u0008\u0002\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u00122\u000e\u0008\u0002\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012H\u00c6\u0001J\u0013\u00106\u001a\u00020\u000c2\u0008\u00107\u001a\u0004\u0018\u000108H\u00d6\u0003J\t\u00109\u001a\u00020:H\u00d6\u0001J\u0006\u0010;\u001a\u00020\u000cJ\t\u0010<\u001a\u00020=H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0017\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0017\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001eR\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001eR\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001cR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\'R\u0011\u0010\r\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u001aR\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010\u001a\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "availableValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "searchText",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "newValueInEdit",
        "selectedValue",
        "shouldHighlightDuplicateNewValueName",
        "",
        "shouldFocusOnNewValueRowAndShowKeyboard",
        "onTapValue",
        "Lkotlin/Function1;",
        "",
        "onNewOptionValuePromoted",
        "Lkotlin/Function0;",
        "onNewValueFromCreateButton",
        "onBack",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "getAvailableValues",
        "()Ljava/util/List;",
        "hasReachedOptionValueNumberLimit",
        "getHasReachedOptionValueNumberLimit",
        "()Z",
        "getNewValueInEdit",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnNewOptionValuePromoted",
        "getOnNewValueFromCreateButton",
        "getOnTapValue",
        "()Lkotlin/jvm/functions/Function1;",
        "getOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getSearchText",
        "getSelectedValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "getShouldFocusOnNewValueRowAndShowKeyboard",
        "getShouldHighlightDuplicateNewValueName",
        "component1",
        "component10",
        "component11",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "shouldShowSearchResults",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final availableValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field

.field private final hasReachedOptionValueNumberLimit:Z

.field private final newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onTapValue:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final option:Lcom/squareup/cogs/itemoptions/ItemOption;

.field private final searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

.field private final shouldFocusOnNewValueRowAndShowKeyboard:Z

.field private final shouldHighlightDuplicateNewValueName:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEdit"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTapValue"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewOptionValuePromoted"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewValueFromCreateButton"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p4, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p5, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p6, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    iput-boolean p7, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    iput-object p8, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    iput-object p9, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    iput-object p10, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    iput-object p11, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    .line 24
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/16 p2, 0xfa

    if-lt p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->hasReachedOptionValueNumberLimit:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    goto :goto_a

    :cond_a
    move-object/from16 v1, p11

    :goto_a
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component10()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component11()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component4()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component5()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    return v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component9()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;"
        }
    .end annotation

    const-string v0, "option"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableValues"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchText"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEdit"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTapValue"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewOptionValuePromoted"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewValueFromCreateButton"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    move-object v1, v0

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v12}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    iget-boolean v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    iget-boolean v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAvailableValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    return-object v0
.end method

.method public final getHasReachedOptionValueNumberLimit()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->hasReachedOptionValueNumberLimit:Z

    return v0
.end method

.method public final getNewValueInEdit()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnNewOptionValuePromoted()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnNewValueFromCreateButton()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnTapValue()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final getShouldFocusOnNewValueRowAndShowKeyboard()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    return v0
.end method

.method public final getShouldHighlightDuplicateNewValueName()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_9
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_a
    add-int/2addr v0, v1

    return v0
.end method

.method public final shouldShowSearchResults()Z
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectSingleOptionValueForVariationScreen(option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->option:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", availableValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->availableValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->searchText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newValueInEdit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->newValueInEdit:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldHighlightDuplicateNewValueName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldHighlightDuplicateNewValueName:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldFocusOnNewValueRowAndShowKeyboard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onTapValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onTapValue:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onNewOptionValuePromoted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewOptionValuePromoted:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onNewValueFromCreateButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onNewValueFromCreateButton:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
