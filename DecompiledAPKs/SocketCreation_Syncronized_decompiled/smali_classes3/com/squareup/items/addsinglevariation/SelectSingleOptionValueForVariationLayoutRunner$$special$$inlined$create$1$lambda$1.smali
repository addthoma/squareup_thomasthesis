.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SelectSingleOptionValueForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner\n*L\n1#1,87:1\n94#2,20:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$bind$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $editView$inlined:Lcom/squareup/noho/NohoEditText;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/noho/NohoEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$editView$inlined:Lcom/squareup/noho/NohoEditText;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$editView$inlined:Lcom/squareup/noho/NohoEditText;

    const-string v0, "editView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;->getValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$editView$inlined:Lcom/squareup/noho/NohoEditText;

    .line 90
    invoke-virtual {p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;->getShouldHighlightDuplicateName()Z

    move-result v0

    const-string/jumbo v1, "view.context"

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$color;->noho_input_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$color;->noho_edit_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 89
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setTextColor(I)V

    .line 95
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$editView$inlined:Lcom/squareup/noho/NohoEditText;

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1$1;

    invoke-direct {v0, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1$1;-><init>(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;)V

    check-cast v0, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 104
    invoke-virtual {p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$SelectSingleOptionValueRow$AddNewOptionValueRow;->getShouldFocusAndShowKeyboard()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 105
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;->$editView$inlined:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->focusAndShowKeyboard()V

    :cond_1
    return-void
.end method
