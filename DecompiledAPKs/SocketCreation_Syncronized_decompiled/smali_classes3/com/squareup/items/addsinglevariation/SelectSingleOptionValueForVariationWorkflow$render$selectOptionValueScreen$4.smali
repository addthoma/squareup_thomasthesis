.class final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectSingleOptionValueForVariationWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;->render(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;->$props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;->invoke(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;

    .line 73
    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;->$props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    invoke-virtual {v2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;->getValuesUsedByExistingVariations()Ljava/util/List;

    move-result-object v2

    .line 71
    invoke-direct {v1, p1, v2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$TapOptionValue;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V

    .line 70
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
