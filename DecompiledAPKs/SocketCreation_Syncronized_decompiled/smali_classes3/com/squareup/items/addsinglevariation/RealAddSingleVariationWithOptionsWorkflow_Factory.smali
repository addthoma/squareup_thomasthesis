.class public final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealAddSingleVariationWithOptionsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;-><init>(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;

    invoke-static {v0, v1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;->newInstance(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow_Factory;->get()Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;

    move-result-object v0

    return-object v0
.end method
