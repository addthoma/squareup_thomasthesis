.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;
.super Ljava/lang/Object;
.source "WarnReachingVariationNumberLimitDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0005H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;Landroid/content/Context;Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;)Landroid/app/Dialog;
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;)Landroid/app/Dialog;
    .locals 3

    .line 29
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_reaching_variation_number_limit_message:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "max_number"

    const/16 v2, 0xfa

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    sget p1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variation_number_limit_title:I

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$createDialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$createDialog$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    .line 35
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 37
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 38
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$createDialog$2;

    invoke-direct {v0, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$createDialog$2;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;)V

    check-cast v0, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026iss() }\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 20
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory$create$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
