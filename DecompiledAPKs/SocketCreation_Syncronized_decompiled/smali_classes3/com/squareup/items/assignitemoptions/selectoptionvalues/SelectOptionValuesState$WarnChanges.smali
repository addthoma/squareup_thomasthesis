.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;
.super Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;
.source "SelectOptionValuesState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WarnChanges"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J9\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\u0013\u0010 \u001a\u00020\t2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u00d6\u0003J\t\u0010#\u001a\u00020\u001fH\u00d6\u0001J\t\u0010$\u001a\u00020\rH\u00d6\u0001J\u0019\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "selectValues",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
        "removedValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "valueToExtend",
        "isOptionSetRemoved",
        "",
        "(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V",
        "()Z",
        "newValueInEditing",
        "",
        "getNewValueInEditing",
        "()Ljava/lang/String;",
        "optionValueSelections",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
        "getOptionValueSelections",
        "()Ljava/util/List;",
        "getRemovedValues",
        "getSelectValues",
        "()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
        "getValueToExtend",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final isOptionSetRemoved:Z

.field private final removedValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field

.field private final selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

.field private final valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Z)V"
        }
    .end annotation

    const-string v0, "selectValues"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "removedValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 140
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p4, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    .line 137
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    .line 138
    check-cast p3, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 139
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->copy(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    return v0
.end method

.method public final copy(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            "Z)",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;"
        }
    .end annotation

    const-string v0, "selectValues"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "removedValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    iget-boolean p1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getNewValueInEditing()Ljava/lang/String;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getNewValueInEditing()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOptionValueSelections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getOptionValueSelections()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getRemovedValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    return-object v0
.end method

.method public final getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    return-object v0
.end method

.method public final getValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isOptionSetRemoved()Z
    .locals 1

    .line 139
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WarnChanges(selectValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", removedValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", valueToExtend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isOptionSetRemoved="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->selectValues:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->removedValues:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->valueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
