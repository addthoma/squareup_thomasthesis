.class final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionSink:Lcom/squareup/workflow/Sink;

.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;->$actionSink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 433
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;->$actionSink:Lcom/squareup/workflow/Sink;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$NewValueCreatedFromSearch;

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$NewValueCreatedFromSearch;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
