.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RealAssignOptionToItemViewFactory.kt"

# interfaces
.implements Lcom/squareup/items/assignitemoptions/AssignOptionToItemViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B7\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "editOptionViewFactory",
        "Lcom/squareup/items/editoption/EditOptionViewFactory;",
        "selectOptionValuesViewFactory",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;",
        "selectOptionsViewFactory",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;",
        "selectVariationsToCreateViewFactory",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;",
        "updateExistingVariationsViewFactory",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;",
        "reviewVariationsToDeleteViewFactory",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;",
        "(Lcom/squareup/items/editoption/EditOptionViewFactory;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/items/editoption/EditOptionViewFactory;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editOptionViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectOptionValuesViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectOptionsViewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectVariationsToCreateViewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateExistingVariationsViewFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reviewVariationsToDeleteViewFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 22
    sget-object v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory;

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 23
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 24
    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x2

    aput-object p2, v0, p1

    .line 25
    check-cast p3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x3

    aput-object p3, v0, p1

    .line 26
    check-cast p4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x4

    aput-object p4, v0, p1

    .line 27
    check-cast p5, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x5

    aput-object p5, v0, p1

    .line 28
    check-cast p6, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x6

    aput-object p6, v0, p1

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
