.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;
.super Ljava/lang/Object;
.source "ReviewVariationsToDeleteViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;

    invoke-static {v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;->newInstance(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory_Factory;->get()Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;

    move-result-object v0

    return-object v0
.end method
