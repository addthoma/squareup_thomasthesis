.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealAssignOptionToItemViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/items/editoption/EditOptionViewFactory;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;-><init>(Lcom/squareup/items/editoption/EditOptionViewFactory;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/items/editoption/EditOptionViewFactory;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;

    invoke-static/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->newInstance(Lcom/squareup/items/editoption/EditOptionViewFactory;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateViewFactory;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionViewFactory;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory_Factory;->get()Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemViewFactory;

    move-result-object v0

    return-object v0
.end method
