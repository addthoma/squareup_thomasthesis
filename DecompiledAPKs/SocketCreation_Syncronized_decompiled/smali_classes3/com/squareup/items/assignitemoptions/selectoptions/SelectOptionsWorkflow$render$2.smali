.class final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $optionsInUse:Ljava/util/List;

.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Ljava/util/List;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$optionsInUse:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->invoke(Lcom/squareup/cogs/itemoptions/ItemOption;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cogs/itemoptions/ItemOption;)V
    .locals 5

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;

    .line 112
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$optionsInUse:Ljava/util/List;

    .line 113
    iget-object v3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getMaxOptionsPerItem()I

    move-result v3

    .line 114
    iget-object v4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$2;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {v4}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v4

    .line 110
    invoke-direct {v1, p1, v2, v3, v4}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
