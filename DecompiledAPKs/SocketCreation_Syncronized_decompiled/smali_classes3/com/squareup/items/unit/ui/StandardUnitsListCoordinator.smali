.class public final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "StandardUnitsListCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;,
        Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;,
        Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;,
        Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardUnitsListCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardUnitsListCoordinator.kt\ncom/squareup/items/unit/ui/StandardUnitsListCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 6 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 7 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 8 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 9 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,297:1\n1847#2,3:298\n1288#2:301\n1313#2,3:302\n1316#2,3:312\n1143#2,2:317\n950#2:319\n1146#2:320\n1642#2:321\n1642#2,2:322\n1643#2:324\n347#3,7:305\n428#3:315\n378#3:316\n49#4:325\n50#4,3:331\n53#4:382\n599#5,4:326\n601#5:330\n166#6,5:334\n172#6:350\n48#6:351\n61#6,3:352\n65#6:366\n49#6:367\n328#7:339\n342#7,5:340\n344#7,4:345\n329#7:349\n328#7:355\n342#7,5:356\n344#7,4:361\n329#7:365\n355#7,3:368\n358#7,3:377\n35#8,6:371\n43#9,2:380\n*E\n*S KotlinDebug\n*F\n+ 1 StandardUnitsListCoordinator.kt\ncom/squareup/items/unit/ui/StandardUnitsListCoordinator\n*L\n115#1,3:298\n139#1:301\n139#1,3:302\n139#1,3:312\n141#1,2:317\n141#1:319\n141#1:320\n148#1:321\n148#1,2:322\n148#1:324\n139#1,7:305\n141#1:315\n141#1:316\n166#1:325\n166#1,3:331\n166#1:382\n166#1,4:326\n166#1:330\n166#1,5:334\n166#1:350\n166#1:351\n166#1,3:352\n166#1:366\n166#1:367\n166#1:339\n166#1,5:340\n166#1,4:345\n166#1:349\n166#1:355\n166#1,5:356\n166#1,4:361\n166#1:365\n166#1,3:368\n166#1,3:377\n166#1,6:371\n166#1,2:380\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 )2\u00020\u0001:\u0004)*+,B;\u0008\u0007\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0005H\u0002J\u0010\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0005H\u0002J\u0010\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020\u001aH\u0002J\u001e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00110!2\u0006\u0010\u001c\u001a\u00020\u00052\u0006\u0010\"\u001a\u00020#H\u0002J\u0018\u0010$\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u0005H\u0002J\u0014\u0010%\u001a\u00020\u0016*\u00020&2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u000c\u0010\'\u001a\u00020&*\u00020(H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/StandardUnitsListScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;",
        "searchBar",
        "Lcom/squareup/ui/XableEditText;",
        "searchTextRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "backButtonTapped",
        "screen",
        "configureActionBar",
        "createRecycler",
        "coordinatorView",
        "recyclerItemsList",
        "",
        "isSearching",
        "",
        "update",
        "displayValue",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;",
        "unitFamily",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "Companion",
        "CreateCustomUnitButtonRow",
        "StandardUnitsListItem",
        "UnitFamily",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$Companion;

.field public static final STANDARD_UNITS_SEARCH_DELAY_MS:J = 0x64L


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private searchBar:Lcom/squareup/ui/XableEditText;

.field private final searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->Companion:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$Companion;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p3, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->res:Lcom/squareup/util/Res;

    .line 65
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$backButtonTapped(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Lcom/squareup/items/unit/StandardUnitsListScreen;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->backButtonTapped(Lcom/squareup/items/unit/StandardUnitsListScreen;)V

    return-void
.end method

.method public static final synthetic access$displayValue(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->displayValue(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getSearchTextRelay$p(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Landroid/view/View;Lcom/squareup/items/unit/StandardUnitsListScreen;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->update(Landroid/view/View;Lcom/squareup/items/unit/StandardUnitsListScreen;)V

    return-void
.end method

.method private final backButtonTapped(Lcom/squareup/items/unit/StandardUnitsListScreen;)V
    .locals 1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/items/unit/StandardUnitsListScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/items/unit/StandardUnitsListScreen$Event$Exit;->INSTANCE:Lcom/squareup/items/unit/StandardUnitsListScreen$Event$Exit;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/items/unit/StandardUnitsListScreen;)V
    .locals 4

    .line 127
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 125
    iget-object v2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/editunit/R$string;->standard_units_list_create_unit:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 126
    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$configureActionBar$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$configureActionBar$1;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Lcom/squareup/items/unit/StandardUnitsListScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 5

    .line 164
    sget v0, Lcom/squareup/editunit/R$id;->standard_units_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 166
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 325
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 326
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 327
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 331
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 332
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 167
    sget-object v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 174
    sget-object v0, Lcom/squareup/noho/NohoLabel$Type;->LABEL:Lcom/squareup/noho/NohoLabel$Type;

    new-instance v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v2, p0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 335
    new-instance v3, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoLabel$1;

    invoke-direct {v3, v0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoLabel$1;-><init>(Lcom/squareup/noho/NohoLabel$Type;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 341
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 345
    sget-object v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoLabel$2;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoLabel$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 341
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 339
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 344
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 340
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 351
    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v0, p0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$2;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 353
    sget-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoRow$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 357
    new-instance v3, Lcom/squareup/cycler/BinderRowSpec;

    .line 361
    sget-object v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoRow$3;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$nohoRow$3;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 357
    invoke-direct {v3, v4, v2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 355
    invoke-virtual {v3, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 360
    check-cast v3, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 356
    invoke-virtual {v1, v3}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 369
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$extraItem$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 198
    sget v2, Lcom/squareup/editunit/R$layout;->standard_units_list_create_custom_unit_row:I

    .line 371
    new-instance v3, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$3;

    invoke-direct {v3, v2, p0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$3;-><init>(ILcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 220
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 368
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 380
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0x8

    .line 223
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 380
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 329
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    .line 228
    new-instance v0, Lcom/squareup/items/unit/ui/RecyclerViewTopSpacingDecoration;

    .line 229
    iget-object v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/noho/R$dimen;->noho_gutter_card_vertical:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    .line 228
    invoke-direct {v0, v1}, Lcom/squareup/items/unit/ui/RecyclerViewTopSpacingDecoration;-><init>(I)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    .line 227
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    .line 326
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final displayValue(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 265
    sget-object v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 270
    sget p1, Lcom/squareup/editunit/R$string;->standard_units_list_family_label_time:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 269
    :cond_1
    sget p1, Lcom/squareup/editunit/R$string;->standard_units_list_family_label_volume:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 268
    :cond_2
    sget p1, Lcom/squareup/editunit/R$string;->standard_units_list_family_label_area:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 267
    :cond_3
    sget p1, Lcom/squareup/editunit/R$string;->standard_units_list_family_label_length:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 266
    :cond_4
    sget p1, Lcom/squareup/editunit/R$string;->standard_units_list_family_label_weight:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final recyclerItemsList(Lcom/squareup/items/unit/StandardUnitsListScreen;Z)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/StandardUnitsListScreen;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;",
            ">;"
        }
    .end annotation

    .line 139
    invoke-virtual {p1}, Lcom/squareup/items/unit/StandardUnitsListScreen;->getState()Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;->getStandardUnits()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 301
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 302
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 303
    move-object v3, v2

    check-cast v3, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 139
    invoke-virtual {v3}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->unitFamily(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    move-result-object v3

    .line 305
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 304
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 308
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 312
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 315
    :cond_1
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 316
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 317
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 318
    check-cast v2, Ljava/util/Map$Entry;

    .line 316
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 142
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 319
    new-instance v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$sortedBy$1;

    invoke-direct {v4}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$$special$$inlined$sortedBy$1;-><init>()V

    check-cast v4, Ljava/util/Comparator;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    .line 142
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 145
    :cond_2
    sget-object v1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$recyclerItemsList$sortedAndGroupedUnits$2;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$recyclerItemsList$sortedAndGroupedUnits$2;

    check-cast v1, Ljava/util/Comparator;

    .line 144
    invoke-static {v0, v1}, Lkotlin/collections/MapsKt;->toSortedMap(Ljava/util/Map;Ljava/util/Comparator;)Ljava/util/SortedMap;

    move-result-object v0

    .line 147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 148
    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    const-string v2, "sortedAndGroupedUnits.entries"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 321
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 149
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    .line 150
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez p2, :cond_4

    .line 153
    new-instance v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;

    const-string v5, "family"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, p1, v3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;-><init>(Lcom/squareup/items/unit/StandardUnitsListScreen;Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    const-string/jumbo v3, "units"

    .line 155
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 322
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 156
    new-instance v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;

    invoke-direct {v4, p1, v3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;-><init>(Lcom/squareup/items/unit/StandardUnitsListScreen;Lcom/squareup/items/unit/LocalizedStandardUnit;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    return-object v1
.end method

.method private final unitFamily(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;
    .locals 1

    .line 274
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz v0, :cond_0

    .line 275
    sget-object p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->AREA:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object p1

    .line 277
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v0, :cond_1

    .line 278
    sget-object p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->LENGTH:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object p1

    .line 280
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v0, :cond_2

    .line 281
    sget-object p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->VOLUME:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object p1

    .line 283
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v0, :cond_3

    .line 284
    sget-object p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->WEIGHT:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object p1

    .line 286
    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    if-eqz p1, :cond_4

    .line 287
    sget-object p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->TIME:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    return-object p1

    .line 289
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "No standard unit family recognized"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final update(Landroid/view/View;Lcom/squareup/items/unit/StandardUnitsListScreen;)V
    .locals 7

    .line 102
    invoke-direct {p0, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->configureActionBar(Lcom/squareup/items/unit/StandardUnitsListScreen;)V

    .line 103
    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$update$1;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Lcom/squareup/items/unit/StandardUnitsListScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_0

    const-string v0, "searchBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->valueOrEmpty(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 106
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 107
    :goto_0
    invoke-direct {p0, p2, v0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->recyclerItemsList(Lcom/squareup/items/unit/StandardUnitsListScreen;Z)Ljava/util/List;

    move-result-object v3

    .line 108
    iget-object v4, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    const-string v5, "recycler"

    if-nez v4, :cond_2

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v3}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    if-eqz v0, :cond_3

    .line 110
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    .line 115
    :goto_1
    check-cast v3, Ljava/lang/Iterable;

    .line 298
    instance-of v4, v3, Ljava/util/Collection;

    if-eqz v4, :cond_4

    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_3

    .line 299
    :cond_4
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;

    .line 115
    instance-of v6, v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;

    if-eqz v6, :cond_6

    check-cast v4, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;

    invoke-virtual {v4}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;->getStandardUnit()Lcom/squareup/items/unit/LocalizedStandardUnit;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1, v2}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_5

    const/4 v2, 0x0

    .line 116
    :cond_7
    :goto_3
    invoke-static {p1}, Lcom/squareup/util/Strings;->forceTitleCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->valueOrEmpty(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 117
    iget-object v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v1, :cond_8

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    new-instance v3, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;

    invoke-direct {v3, p2, p1, v0, v2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$CreateCustomUnitButtonRow;-><init>(Lcom/squareup/items/unit/StandardUnitsListScreen;Ljava/lang/String;ZZ)V

    invoke-virtual {v1, v3}, Lcom/squareup/cycler/Recycler;->setExtraItem(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 70
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 71
    sget v0, Lcom/squareup/editunit/R$id;->standard_units_list_search_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->createRecycler(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    const-string v1, "searchBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/editunit/R$string;->standard_units_list_search_bar_hint_text:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchTextRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    const-wide/16 v2, 0x64

    .line 79
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Lcom/jakewharton/rxrelay2/PublishRelay;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    .line 81
    iget-object v2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v3, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$1;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$1;

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    check-cast v2, Lio/reactivex/ObservableSource;

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v3

    .line 80
    invoke-virtual {v0, v2, v3}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    sget-object v2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$2;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$2;

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v2, "searchTextRelay\n        \u2026d(searchQuery))\n        }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->searchBar:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$3;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$attach$4;-><init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { s ->\u2026view, s.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
