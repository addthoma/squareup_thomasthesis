.class public abstract Lcom/squareup/items/unit/EditUnitInput;
.super Ljava/lang/Object;
.source "EditUnitWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/EditUnitInput$CreateUnit;,
        Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;,
        Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0018\u0019\u001aBA\u0008\u0002\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u0082\u0001\u0003\u001b\u001c\u001d\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/items/unit/EditUnitInput;",
        "",
        "defaultStandardUnit",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "unitToEdit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "unitsInUse",
        "",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "prepopulatedName",
        "",
        "unitCreationSource",
        "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
        "(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/util/Set;Ljava/lang/String;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V",
        "getDefaultStandardUnit",
        "()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "getPrepopulatedName",
        "()Ljava/lang/String;",
        "getUnitCreationSource",
        "()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
        "getUnitToEdit",
        "()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "getUnitsInUse",
        "()Ljava/util/Set;",
        "AddDefaultStandardUnit",
        "CreateUnit",
        "ModifyExistingUnit",
        "Lcom/squareup/items/unit/EditUnitInput$CreateUnit;",
        "Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;",
        "Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultStandardUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

.field private final prepopulatedName:Ljava/lang/String;

.field private final unitCreationSource:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

.field private final unitToEdit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

.field private final unitsInUse:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/util/Set;Ljava/lang/String;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
            ")V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/EditUnitInput;->defaultStandardUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object p2, p0, Lcom/squareup/items/unit/EditUnitInput;->unitToEdit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    iput-object p3, p0, Lcom/squareup/items/unit/EditUnitInput;->unitsInUse:Ljava/util/Set;

    iput-object p4, p0, Lcom/squareup/items/unit/EditUnitInput;->prepopulatedName:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/items/unit/EditUnitInput;->unitCreationSource:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/util/Set;Ljava/lang/String;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 70
    move-object p1, v0

    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    :cond_0
    move-object v2, p1

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_1

    const-string p4, ""

    :cond_1
    move-object v5, p4

    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_2

    .line 74
    move-object p5, v0

    check-cast p5, Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    :cond_2
    move-object v6, p5

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/unit/EditUnitInput;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/util/Set;Ljava/lang/String;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V

    return-void
.end method


# virtual methods
.method public getDefaultStandardUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput;->defaultStandardUnit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object v0
.end method

.method public getPrepopulatedName()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput;->prepopulatedName:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput;->unitCreationSource:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    return-object v0
.end method

.method public getUnitToEdit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput;->unitToEdit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-object v0
.end method

.method public getUnitsInUse()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput;->unitsInUse:Ljava/util/Set;

    return-object v0
.end method
