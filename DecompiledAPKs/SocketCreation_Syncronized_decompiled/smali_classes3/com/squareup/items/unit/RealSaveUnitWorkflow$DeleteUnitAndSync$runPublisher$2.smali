.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;
.super Ljava/lang/Object;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->runPublisher()Lorg/reactivestreams/Publisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a,\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u00020\u00012\u0014\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Ljava/lang/Void;",
        "kotlin.jvm.PlatformType",
        "result",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-nez v0, :cond_0

    .line 216
    iget-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;

    iget-object p1, p1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    invoke-static {p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getCogs$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lcom/squareup/cogs/Cogs;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cogs/Cogs;->syncAsSingle()Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;->INSTANCE:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 221
    :cond_0
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 207
    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;->apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
