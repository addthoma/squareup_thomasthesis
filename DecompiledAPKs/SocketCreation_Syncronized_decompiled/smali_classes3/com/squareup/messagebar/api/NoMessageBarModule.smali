.class public abstract Lcom/squareup/messagebar/api/NoMessageBarModule;
.super Ljava/lang/Object;
.source "NoMessageBarModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindMessageBar(Lcom/squareup/messagebar/api/ReaderMessageBar$NoMessageBar;)Lcom/squareup/messagebar/api/ReaderMessageBar;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
