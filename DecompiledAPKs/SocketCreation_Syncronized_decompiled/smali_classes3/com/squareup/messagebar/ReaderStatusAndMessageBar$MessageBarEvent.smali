.class final Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;
.super Ljava/lang/Object;
.source "ReaderStatusAndMessageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderStatusAndMessageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MessageBarEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    }
.end annotation


# instance fields
.field final barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

.field final batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

.field final colorId:I

.field final ignoreColorId:Z

.field final message:Ljava/lang/CharSequence;

.field final messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;


# direct methods
.method private constructor <init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)V
    .locals 1

    .line 713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 714
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$100(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->colorId:I

    .line 715
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$200(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->ignoreColorId:Z

    .line 717
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$400(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->message:Ljava/lang/CharSequence;

    .line 718
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$300(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 719
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$500(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 720
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$600(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    .line 721
    invoke-static {p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->access$600(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object p1

    const-string v0, "Remember to set HIDDEN_BATTERY_LEVEL if you\'re not using it!"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V
    .locals 0

    .line 704
    invoke-direct {p0, p1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;)V

    return-void
.end method

.method public static constructHideEvent()Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;
    .locals 2

    .line 725
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;-><init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$1;)V

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;->CYCLE_OUT_TO_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    .line 726
    invoke-virtual {v0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->barAnimationType(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/messagebar/R$color;->message_bar_background_ready:I

    .line 727
    invoke-virtual {v0, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;->setColorId(I)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .line 804
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->colorId:I

    .line 806
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-boolean v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->ignoreColorId:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->message:Ljava/lang/CharSequence;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->messageAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->barAnimationType:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$BarAnimationType;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;->batteryLevel:Lcom/squareup/cardreader/BatteryLevel;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const-string v2, "MessageBarEvent{colorId=%d, ignoreColorId=%s, message=%s, messageAnimationType=%s, barAnimationType=%s, batteryLevel=%s}"

    .line 804
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
