.class public final Lcom/squareup/marin/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final edit_entry_label_text_height:I = 0x7f070146

.field public static final glyph_logotype_font_size:I = 0x7f07016c

.field public static final glyph_logotype_height:I = 0x7f07016e

.field public static final glyph_logotype_width_en:I = 0x7f070170

.field public static final glyph_logotype_width_es:I = 0x7f070172

.field public static final glyph_logotype_width_fr:I = 0x7f070174

.field public static final glyph_logotype_width_ja:I = 0x7f070176

.field public static final glyph_size_circle:I = 0x7f07017d

.field public static final glyph_vertical_caret:I = 0x7f07017f

.field public static final grid_label_text_size:I = 0x7f070184

.field public static final grid_placeholder_text_max_size:I = 0x7f070185

.field public static final marin_action_bar_button_min_width:I = 0x7f0701fa

.field public static final marin_action_bar_button_padding:I = 0x7f0701fb

.field public static final marin_action_bar_header_gutter:I = 0x7f0701fc

.field public static final marin_action_bar_height:I = 0x7f0701fd

.field public static final marin_action_bar_up_text_left_margin:I = 0x7f0701fe

.field public static final marin_actionbar_badge_horizontal:I = 0x7f0701ff

.field public static final marin_actionbar_badge_vertical:I = 0x7f070200

.field public static final marin_applet_grouping_title_height:I = 0x7f070201

.field public static final marin_badge_corner_radius:I = 0x7f070202

.field public static final marin_badge_padding_bottom:I = 0x7f070203

.field public static final marin_badge_padding_left:I = 0x7f070204

.field public static final marin_badge_padding_right:I = 0x7f070205

.field public static final marin_badge_padding_top:I = 0x7f070206

.field public static final marin_badge_stroke:I = 0x7f070207

.field public static final marin_buyer_flanking_view_min_dimen:I = 0x7f070208

.field public static final marin_buyer_gutter:I = 0x7f070209

.field public static final marin_buyer_gutter_half:I = 0x7f07020a

.field public static final marin_buyer_perimeter_gap:I = 0x7f07020b

.field public static final marin_buyer_receipt_gap:I = 0x7f07020c

.field public static final marin_buyer_receipt_gap_large:I = 0x7f07020d

.field public static final marin_buyer_receipt_gutter_horizontal_half:I = 0x7f07020e

.field public static final marin_buyer_receipt_gutter_vertical:I = 0x7f07020f

.field public static final marin_buyer_under_actionbar_vertical_spacing:I = 0x7f070210

.field public static final marin_calendar_max_tile_size:I = 0x7f070211

.field public static final marin_cart_button_width:I = 0x7f070212

.field public static final marin_cart_item_gutter:I = 0x7f070213

.field public static final marin_cart_item_gutter_half:I = 0x7f070214

.field public static final marin_cart_list_gutter_vertical:I = 0x7f070215

.field public static final marin_charge_height:I = 0x7f070216

.field public static final marin_charge_no_tickets_phone_text_size:I = 0x7f070217

.field public static final marin_charge_width:I = 0x7f070218

.field public static final marin_checkable_text_view_min_height:I = 0x7f070219

.field public static final marin_checkable_text_view_side_padding:I = 0x7f07021a

.field public static final marin_checkbox_size:I = 0x7f07021b

.field public static final marin_clickable_x_large_dimen:I = 0x7f07021c

.field public static final marin_dialog_title_height:I = 0x7f07021d

.field public static final marin_divider_height:I = 0x7f07021e

.field public static final marin_divider_width_1dp:I = 0x7f07021f

.field public static final marin_divider_width_1px:I = 0x7f070220

.field public static final marin_drop_down_row_height:I = 0x7f070221

.field public static final marin_font_body_1:I = 0x7f070222

.field public static final marin_font_body_2:I = 0x7f070223

.field public static final marin_gap_big:I = 0x7f070224

.field public static final marin_gap_calendar_vertical_padding:I = 0x7f070225

.field public static final marin_gap_charge_below:I = 0x7f070226

.field public static final marin_gap_flank_drawables:I = 0x7f070227

.field public static final marin_gap_glyph_title_above:I = 0x7f070228

.field public static final marin_gap_glyph_title_below:I = 0x7f070229

.field public static final marin_gap_grid_inner_padding:I = 0x7f07022a

.field public static final marin_gap_grid_outer_padding:I = 0x7f07022b

.field public static final marin_gap_grid_padding_wide:I = 0x7f07022c

.field public static final marin_gap_large:I = 0x7f07022d

.field public static final marin_gap_medium:I = 0x7f07022e

.field public static final marin_gap_medium_lollipop:I = 0x7f07022f

.field public static final marin_gap_medium_plus_min_height:I = 0x7f070231

.field public static final marin_gap_mini:I = 0x7f070232

.field public static final marin_gap_multiline_padding:I = 0x7f070233

.field public static final marin_gap_null_state_favorites_grid:I = 0x7f070234

.field public static final marin_gap_null_state_with_button:I = 0x7f070235

.field public static final marin_gap_payment_pad_height_portrait:I = 0x7f070236

.field public static final marin_gap_payment_pad_inner_margin_portrait:I = 0x7f070237

.field public static final marin_gap_payment_pad_inner_padding_portrait:I = 0x7f070238

.field public static final marin_gap_payment_pad_outer_padding_landscape:I = 0x7f070239

.field public static final marin_gap_reader_image_padding:I = 0x7f07023a

.field public static final marin_gap_refund_policy_padding:I = 0x7f07023b

.field public static final marin_gap_section_header_below:I = 0x7f07023c

.field public static final marin_gap_section_header_extra_large:I = 0x7f07023d

.field public static final marin_gap_section_header_medium:I = 0x7f07023e

.field public static final marin_gap_signature_screen_button_margins:I = 0x7f07023f

.field public static final marin_gap_signature_screen_extra_padding:I = 0x7f070240

.field public static final marin_gap_signature_screen_padding:I = 0x7f070241

.field public static final marin_gap_signature_screen_tip_bar:I = 0x7f070242

.field public static final marin_gap_small:I = 0x7f070243

.field public static final marin_gap_tablet:I = 0x7f070244

.field public static final marin_gap_thumbnail_content:I = 0x7f070245

.field public static final marin_gap_tile_padding_horizontal:I = 0x7f070246

.field public static final marin_gap_tiny:I = 0x7f070247

.field public static final marin_gap_ultra_large:I = 0x7f070248

.field public static final marin_glyph_left_width:I = 0x7f070249

.field public static final marin_glyph_left_width_plus_padding:I = 0x7f07024a

.field public static final marin_grid_minimum_height:I = 0x7f07024b

.field public static final marin_grid_regular_tile_extra_height:I = 0x7f07024c

.field public static final marin_grid_stacked_tile_extra_height:I = 0x7f07024d

.field public static final marin_grid_tile_card_max_dimen:I = 0x7f07024e

.field public static final marin_grid_tile_max_font_size:I = 0x7f07024f

.field public static final marin_grid_tile_min_font_size:I = 0x7f070250

.field public static final marin_grid_tile_tag_max_dimen:I = 0x7f070251

.field public static final marin_grid_tile_text_size:I = 0x7f070252

.field public static final marin_gutter:I = 0x7f070253

.field public static final marin_gutter_applet_section_button_bottom:I = 0x7f070254

.field public static final marin_gutter_applet_section_button_inner:I = 0x7f070255

.field public static final marin_gutter_half:I = 0x7f070256

.field public static final marin_gutter_half_lollipop:I = 0x7f070257

.field public static final marin_gutter_lollipop:I = 0x7f070258

.field public static final marin_gutter_plus_mini:I = 0x7f070259

.field public static final marin_gutter_quarter:I = 0x7f07025a

.field public static final marin_gutter_row_indent:I = 0x7f07025b

.field public static final marin_gutter_sidebar:I = 0x7f07025c

.field public static final marin_inline_element_height:I = 0x7f07025d

.field public static final marin_inline_glyph_button_total_width:I = 0x7f07025e

.field public static final marin_inline_glyph_margin:I = 0x7f07025f

.field public static final marin_inline_glyph_size:I = 0x7f070260

.field public static final marin_invoice_price:I = 0x7f070261

.field public static final marin_keypad_top_bar_height:I = 0x7f070262

.field public static final marin_letterspacing_logo:I = 0x7f070263

.field public static final marin_library_thumbnail_size:I = 0x7f070264

.field public static final marin_lollipop_ball_radius_inner:I = 0x7f070265

.field public static final marin_lollipop_ball_radius_inner_with_stroke:I = 0x7f070266

.field public static final marin_lollipop_ball_size:I = 0x7f070267

.field public static final marin_lollipop_focus_offset:I = 0x7f070268

.field public static final marin_lollipop_focus_radius:I = 0x7f070269

.field public static final marin_lollipop_switch_stroke:I = 0x7f07026a

.field public static final marin_lollipop_switch_width:I = 0x7f07026b

.field public static final marin_max_message_width:I = 0x7f07026c

.field public static final marin_message_height:I = 0x7f07026d

.field public static final marin_min_height:I = 0x7f07026e

.field public static final marin_min_width:I = 0x7f07026f

.field public static final marin_min_width_plus_marin_gap_medium:I = 0x7f070270

.field public static final marin_min_width_plus_marin_gap_tiny:I = 0x7f070271

.field public static final marin_modal_gap_below_message:I = 0x7f070272

.field public static final marin_modal_gap_below_title:I = 0x7f070273

.field public static final marin_modal_gutter:I = 0x7f070274

.field public static final marin_modal_title_text_size:I = 0x7f070275

.field public static final marin_order_entry_tab_buttons_gone_height:I = 0x7f070276

.field public static final marin_order_entry_tab_buttons_height:I = 0x7f070277

.field public static final marin_order_entry_tab_buttons_slim_height:I = 0x7f070278

.field public static final marin_order_entry_tab_drawable_padding:I = 0x7f070279

.field public static final marin_order_entry_tab_vertical_padding:I = 0x7f07027a

.field public static final marin_overview_numbers:I = 0x7f07027b

.field public static final marin_page_indicator_default_diameter:I = 0x7f07027c

.field public static final marin_payment_row_glyph_left_width:I = 0x7f07027d

.field public static final marin_pinpad_glyph_margin:I = 0x7f07027e

.field public static final marin_pinpad_text_margin:I = 0x7f07027f

.field public static final marin_ribbon_height:I = 0x7f070280

.field public static final marin_ribbon_top_margin:I = 0x7f070281

.field public static final marin_ribbon_width:I = 0x7f070282

.field public static final marin_sales_overview_horizontal_gap:I = 0x7f070283

.field public static final marin_sales_overview_vertical_gap:I = 0x7f070284

.field public static final marin_sales_time:I = 0x7f070285

.field public static final marin_sign_line_small_x_dimen:I = 0x7f070286

.field public static final marin_slim_height:I = 0x7f070287

.field public static final marin_small_modal_line_height:I = 0x7f070288

.field public static final marin_split_tender_banner_height:I = 0x7f070289

.field public static final marin_split_tender_banner_text_size:I = 0x7f07028a

.field public static final marin_split_ticket_gutter:I = 0x7f07028b

.field public static final marin_split_ticket_gutter_half:I = 0x7f07028c

.field public static final marin_split_ticket_ticket_gutter_plus_checkbox:I = 0x7f07028d

.field public static final marin_split_ticket_width:I = 0x7f07028e

.field public static final marin_tall_list_row_height:I = 0x7f07028f

.field public static final marin_text_actionbar:I = 0x7f070290

.field public static final marin_text_badge:I = 0x7f070291

.field public static final marin_text_category:I = 0x7f070292

.field public static final marin_text_charge:I = 0x7f070293

.field public static final marin_text_charge_subtitle:I = 0x7f070294

.field public static final marin_text_default:I = 0x7f070295

.field public static final marin_text_display:I = 0x7f070296

.field public static final marin_text_drawer:I = 0x7f070297

.field public static final marin_text_header_title:I = 0x7f070298

.field public static final marin_text_headline:I = 0x7f070299

.field public static final marin_text_help:I = 0x7f07029a

.field public static final marin_text_home_pager:I = 0x7f07029b

.field public static final marin_text_jumbo_button:I = 0x7f07029c

.field public static final marin_text_keypad:I = 0x7f07029d

.field public static final marin_text_keypad_home:I = 0x7f07029e

.field public static final marin_text_landing_link:I = 0x7f07029f

.field public static final marin_text_landing_message:I = 0x7f0702a0

.field public static final marin_text_landing_message_world:I = 0x7f0702a1

.field public static final marin_text_logo:I = 0x7f0702a2

.field public static final marin_text_logo_min:I = 0x7f0702a3

.field public static final marin_text_message:I = 0x7f0702a4

.field public static final marin_text_ribbon:I = 0x7f0702a5

.field public static final marin_text_section_header:I = 0x7f0702a6

.field public static final marin_text_sign_title:I = 0x7f0702a7

.field public static final marin_text_subtext:I = 0x7f0702a8

.field public static final marin_text_tile_color_block_width:I = 0x7f0702a9

.field public static final marin_text_tile_container_height:I = 0x7f0702aa

.field public static final marin_text_tile_inner_padding:I = 0x7f0702ab

.field public static final marin_text_tile_outer_padding:I = 0x7f0702ac

.field public static final marin_text_tile_padding:I = 0x7f0702ad

.field public static final marin_text_title_1:I = 0x7f0702ae

.field public static final marin_text_title_2:I = 0x7f0702af

.field public static final marin_time_width:I = 0x7f0702b0

.field public static final marin_title_subtitle_row_height:I = 0x7f0702b1

.field public static final marin_undo_bar_corners:I = 0x7f0702b2

.field public static final marin_web_browser_progress_stroke_width:I = 0x7f0702b3

.field public static final marin_x2_max_message_width:I = 0x7f0702b4

.field public static final priority_badge_size:I = 0x7f07043e

.field public static final priority_no_text_badge_size:I = 0x7f07043f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
