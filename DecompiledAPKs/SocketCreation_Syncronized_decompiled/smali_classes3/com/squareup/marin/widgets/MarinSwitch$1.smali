.class Lcom/squareup/marin/widgets/MarinSwitch$1;
.super Ljava/lang/Object;
.source "MarinSwitch.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/marin/widgets/MarinSwitch;->updateCheckedState(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/marin/widgets/MarinSwitch;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinSwitch;)V
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch$1;->this$0:Lcom/squareup/marin/widgets/MarinSwitch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 321
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 322
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch$1;->this$0:Lcom/squareup/marin/widgets/MarinSwitch;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    const/high16 p1, 0x7fc00000    # Float.NaN

    :goto_0
    invoke-static {v1, p1}, Lcom/squareup/marin/widgets/MarinSwitch;->access$002(Lcom/squareup/marin/widgets/MarinSwitch;F)F

    .line 323
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch$1;->this$0:Lcom/squareup/marin/widgets/MarinSwitch;

    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinSwitch;->access$200(Lcom/squareup/marin/widgets/MarinSwitch;)I

    move-result v1

    int-to-float v1, v1

    mul-float v0, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->access$102(Lcom/squareup/marin/widgets/MarinSwitch;F)F

    .line 324
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch$1;->this$0:Lcom/squareup/marin/widgets/MarinSwitch;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSwitch;->invalidate()V

    return-void
.end method
