.class public Lcom/squareup/marin/widgets/MarinSquareCompoundButton;
.super Landroid/widget/CompoundButton;
.source "MarinSquareCompoundButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 0

    .line 19
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSquareCompoundButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result p1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSquareCompoundButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result p2

    .line 21
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 23
    invoke-virtual {p0, p1, p1}, Lcom/squareup/marin/widgets/MarinSquareCompoundButton;->setMeasuredDimension(II)V

    return-void
.end method
