.class public Lcom/squareup/marin/widgets/MarinGlyphMessage;
.super Landroid/widget/LinearLayout;
.source "MarinGlyphMessage.java"


# instance fields
.field private final buttonView:Landroid/widget/Button;

.field private final glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private final messageView:Lcom/squareup/widgets/MessageView;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    sget v0, Lcom/squareup/marin/R$layout;->marin_glyph_message:I

    invoke-static {p1, v0, p0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 v0, 0x1

    .line 41
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setOrientation(I)V

    const/16 v1, 0x11

    .line 42
    invoke-virtual {p0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGravity(I)V

    .line 44
    sget v1, Lcom/squareup/marin/R$id;->glyph_message_glyph:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 45
    sget v1, Lcom/squareup/marin/R$id;->glyph_message_title:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    .line 46
    sget v1, Lcom/squareup/marin/R$id;->glyph_message_message:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    .line 47
    sget v1, Lcom/squareup/marin/R$id;->glyph_message_button:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    .line 49
    sget-object v1, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 50
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_glyph:I

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 52
    sget v1, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_glyphSaveEnabled:I

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 53
    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setSaveEnabled(Z)V

    if-eqz p2, :cond_0

    .line 55
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 58
    :cond_0
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_sq_glyphColor:I

    .line 59
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 61
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 64
    :cond_1
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_marinGlyphMessageTitleColor:I

    .line 65
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 67
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 70
    :cond_2
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_messageColor:I

    .line 71
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 73
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, p2}, Lcom/squareup/widgets/MessageView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 76
    :cond_3
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_glyphVisible:I

    const/4 v1, -0x1

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-eq p2, v1, :cond_5

    .line 78
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    if-ne p2, v0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 81
    :cond_5
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_android_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 82
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 83
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    :cond_6
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_message:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 87
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 88
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 91
    :cond_7
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage_button_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 92
    invoke-static {p2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 93
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_8
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public clearGlyph()V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public hideButton()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public hideMessage()V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public hideTitle()V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setButtonText(I)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 156
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setGlyphWithAnimation(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->flipTo(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method

.method public setMessage(I)V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 142
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->messageView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 123
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->titleView:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public setVector(I)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 105
    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 104
    invoke-static {v0, p1, v1}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p1

    .line 106
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public showButton()V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphMessage;->buttonView:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method
