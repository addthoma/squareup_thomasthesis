.class public abstract Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "MarinLabeledGlyphDrawable.java"


# static fields
.field private static final stateDisabled:[I

.field private static final stateEnabled:[I


# instance fields
.field private final colorStateList:Landroid/content/res/ColorStateList;

.field private drawText:Z

.field private enabled:Z

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

.field private final maxFontSize:F

.field private final minFontSize:F

.field private paintColor:I

.field private final resources:Landroid/content/res/Resources;

.field private final shadowColor:I

.field private final shadowDx:F

.field private final shadowDy:F

.field private final shadowRadius:I

.field protected text:Ljava/lang/String;

.field private final textBoundBottom:I

.field private final textBoundTop:I

.field private final textBoundWidth:I

.field protected final textPaint:Landroid/text/TextPaint;

.field private final textShadowDx:F

.field protected final textXPosition:F

.field protected textYPosition:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    const v3, 0x101009e

    aput v3, v1, v2

    .line 31
    sput-object v1, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->stateEnabled:[I

    new-array v0, v0, [I

    const v1, -0x101009e

    aput v1, v0, v2

    .line 32
    sput-object v0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->stateDisabled:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .line 62
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    .line 64
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$color;->marin_text_shadow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowColor:I

    .line 65
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_shadow_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowRadius:I

    .line 66
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowDx:F

    .line 67
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dy:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowDy:F

    .line 68
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_grid_tile_min_font_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->minFontSize:F

    .line 69
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_grid_tile_max_font_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->maxFontSize:F

    .line 70
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/marin/R$color;->marin_text_selector_white_disabled_white_translucent:I

    .line 71
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    .line 72
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet_10inch:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 73
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 74
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->getGlyph(Z)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 75
    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->initGlyph(Landroid/content/res/Resources;)V

    .line 77
    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    invoke-virtual {p0, v2}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->fetchTextShadowDx(Landroid/content/res/Resources;)F

    move-result v2

    iput v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textShadowDx:F

    .line 78
    invoke-virtual {p0, v0, v1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->calculateTextBoundWidth(ZI)I

    move-result v2

    iput v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundWidth:I

    .line 79
    invoke-virtual {p0, v0, v1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->calculateTextBoundTop(ZI)I

    move-result v2

    iput v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundTop:I

    .line 80
    invoke-virtual {p0, v0, v1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->calculateTextBoundBottom(ZI)I

    move-result v2

    iput v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundBottom:I

    .line 81
    invoke-virtual {p0, v0, v1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->calculateTextPositionX(ZI)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textXPosition:F

    const/4 v0, 0x1

    .line 83
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->enabled:Z

    .line 84
    new-instance v0, Landroid/text/TextPaint;

    const/16 v1, 0x81

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    .line 85
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 87
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->updateDrawableApperance()V

    return-void
.end method

.method private updateDrawableApperance()V
    .locals 6

    .line 177
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->enabled:Z

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    sget-object v1, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->stateEnabled:[I

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->paintColor:I

    goto :goto_0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    sget-object v1, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->stateDisabled:[I

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->paintColor:I

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->paintColor:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 184
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->resources:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 185
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    iget v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->paintColor:I

    .line 186
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 187
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->enabled:Z

    if-eqz v1, :cond_1

    .line 188
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowRadius:I

    int-to-float v2, v2

    iget v3, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textShadowDx:F

    iget v4, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowDy:F

    iget v5, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowColor:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 189
    iget v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowRadius:I

    iget v2, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowDx:F

    iget v3, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowDy:F

    iget v4, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->shadowColor:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadow(IFFI)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    goto :goto_1

    .line 191
    :cond_1
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->clearShadowLayer()V

    .line 193
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    .line 194
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->invalidateSelf()V

    return-void
.end method


# virtual methods
.method protected abstract calculateTextBoundBottom(ZI)I
.end method

.method protected abstract calculateTextBoundTop(ZI)I
.end method

.method protected abstract calculateTextBoundWidth(ZI)I
.end method

.method protected abstract calculateTextPositionX(ZI)I
.end method

.method protected abstract doDrawText(Landroid/graphics/Canvas;)V
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyphDrawable:Lcom/squareup/glyph/SquareGlyphDrawable;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 112
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->drawText:Z

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->doDrawText(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected fetchTextShadowDx(Landroid/content/res/Resources;)F
    .locals 1

    .line 173
    sget v0, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    return p1
.end method

.method protected abstract getGlyph(Z)Lcom/squareup/glyph/GlyphTypeface$Glyph;
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getFixedGlyphHeight()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getFixedGlyphWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isStateful()Z
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    return v0
.end method

.method protected onStateChange([I)Z
    .locals 8

    .line 136
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getColor()I

    move-result v0

    .line 137
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    .line 140
    array-length v2, p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v2, :cond_1

    aget v6, p1, v4

    const v7, 0x101009e

    if-ne v6, v7, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-ne v1, v0, :cond_3

    .line 147
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->enabled:Z

    if-eq v0, p1, :cond_2

    goto :goto_2

    :cond_2
    return v3

    .line 148
    :cond_3
    :goto_2
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->enabled:Z

    .line 149
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->updateDrawableApperance()V

    return v5
.end method

.method public setAlpha(I)V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->updateDrawableApperance()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .line 124
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->updateDrawableApperance()V

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 6

    .line 156
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->drawText:Z

    .line 157
    invoke-static {p1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->text:Ljava/lang/String;

    .line 158
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->drawText:Z

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundWidth:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->maxFontSize:F

    iget v4, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundBottom:I

    iget v5, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundTop:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    .line 160
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 159
    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFF)V

    .line 161
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result p1

    iget v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->minFontSize:F

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    const-string p1, ""

    .line 163
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->text:Ljava/lang/String;

    const/4 p1, 0x0

    .line 164
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->drawText:Z

    goto :goto_0

    .line 166
    :cond_0
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textPaint:Landroid/text/TextPaint;

    iget v0, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundTop:I

    int-to-float v0, v0

    iget v1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textBoundBottom:I

    int-to-float v1, v1

    invoke-static {p1, v0, v1}, Lcom/squareup/text/Fonts;->getYForCenteredText(Landroid/text/TextPaint;FF)F

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->textYPosition:F

    .line 169
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinLabeledGlyphDrawable;->updateDrawableApperance()V

    return-void
.end method
