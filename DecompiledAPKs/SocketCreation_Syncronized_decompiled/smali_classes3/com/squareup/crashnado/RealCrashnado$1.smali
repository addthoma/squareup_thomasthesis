.class Lcom/squareup/crashnado/RealCrashnado$1;
.super Ljava/lang/Object;
.source "RealCrashnado.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crashnado/RealCrashnado;->doInstall(Lcom/squareup/crashnado/Crashnado$CrashnadoListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/crashnado/RealCrashnado;

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>(Lcom/squareup/crashnado/RealCrashnado;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/crashnado/RealCrashnado$1;->this$0:Lcom/squareup/crashnado/RealCrashnado;

    iput-object p2, p0, Lcom/squareup/crashnado/RealCrashnado$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/crashnado/RealCrashnado$1;->this$0:Lcom/squareup/crashnado/RealCrashnado;

    invoke-static {v0}, Lcom/squareup/crashnado/RealCrashnado;->access$000(Lcom/squareup/crashnado/RealCrashnado;)Lcom/squareup/crashnado/CrashnadoReporter;

    move-result-object v0

    const-string v1, "Installing Crashnado on main thread."

    invoke-interface {v0, v1}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/crashnado/RealCrashnado$1;->this$0:Lcom/squareup/crashnado/RealCrashnado;

    invoke-static {v0}, Lcom/squareup/crashnado/RealCrashnado;->access$100(Lcom/squareup/crashnado/RealCrashnado;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/crashnado/RealCrashnado$1;->this$0:Lcom/squareup/crashnado/RealCrashnado;

    invoke-static {v1}, Lcom/squareup/crashnado/RealCrashnado;->access$000(Lcom/squareup/crashnado/RealCrashnado;)Lcom/squareup/crashnado/CrashnadoReporter;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/crashnado/CrashnadoNative;->install(Landroid/content/Context;Lcom/squareup/crashnado/CrashnadoReporter;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/crashnado/RealCrashnado$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method
