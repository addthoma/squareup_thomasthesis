.class Lcom/squareup/glyph/SquareGlyphView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SquareGlyphView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/SquareGlyphView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/glyph/SquareGlyphView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 245
    new-instance v0, Lcom/squareup/glyph/SquareGlyphView$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/glyph/SquareGlyphView$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/glyph/SquareGlyphView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 236
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 237
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphView$SavedState;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/glyph/SquareGlyphView$1;)V
    .locals 0

    .line 227
    invoke-direct {p0, p1}, Lcom/squareup/glyph/SquareGlyphView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0

    .line 231
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 232
    iput-object p2, p0, Lcom/squareup/glyph/SquareGlyphView$SavedState;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/glyph/SquareGlyphView$SavedState;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 227
    iget-object p0, p0, Lcom/squareup/glyph/SquareGlyphView$SavedState;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 241
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 242
    iget-object p2, p0, Lcom/squareup/glyph/SquareGlyphView$SavedState;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
