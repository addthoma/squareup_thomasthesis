.class public Lcom/squareup/notification/NotificationWrapper;
.super Ljava/lang/Object;
.source "NotificationWrapper.java"


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/notification/NotificationWrapper;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private maybeCreateNotificationChannel(Lcom/squareup/notification/Channel;Landroid/app/NotificationManager;)V
    .locals 4

    .line 42
    invoke-interface {p1}, Lcom/squareup/notification/Channel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/NotificationManager;->getNotificationChannel(Ljava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Landroid/app/NotificationChannel;

    .line 44
    invoke-interface {p1}, Lcom/squareup/notification/Channel;->getChannelId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/notification/NotificationWrapper;->res:Lcom/squareup/util/Res;

    invoke-interface {p1}, Lcom/squareup/notification/Channel;->getNameResId()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-interface {p1}, Lcom/squareup/notification/Channel;->getImportance()I

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 46
    invoke-virtual {p2, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public createDefaultNotificationChannels(Landroid/content/Context;[Lcom/squareup/notification/Channel;)V
    .locals 7

    const-string v0, "notification"

    .line 53
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/NotificationManager;

    .line 55
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p2, v1

    .line 56
    invoke-interface {v2}, Lcom/squareup/notification/Channel;->isDefaultChannel()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 57
    new-instance v3, Landroid/app/NotificationChannel;

    .line 58
    invoke-interface {v2}, Lcom/squareup/notification/Channel;->getChannelId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/notification/NotificationWrapper;->res:Lcom/squareup/util/Res;

    invoke-interface {v2}, Lcom/squareup/notification/Channel;->getNameResId()I

    move-result v6

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 59
    invoke-interface {v2}, Lcom/squareup/notification/Channel;->getImportance()I

    move-result v2

    invoke-direct {v3, v4, v5, v2}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 60
    invoke-virtual {p1, v3}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;
    .locals 3

    const-string v0, "notification"

    .line 27
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 29
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_0

    .line 30
    invoke-direct {p0, p2, v0}, Lcom/squareup/notification/NotificationWrapper;->maybeCreateNotificationChannel(Lcom/squareup/notification/Channel;Landroid/app/NotificationManager;)V

    .line 33
    :cond_0
    new-instance v0, Landroidx/core/app/NotificationCompat$Builder;

    invoke-interface {p2}, Lcom/squareup/notification/Channel;->getChannelId()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Landroidx/core/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 34
    invoke-virtual {v0, p1}, Landroidx/core/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 35
    invoke-virtual {p1, p2}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/notification/R$drawable;->notification_square:I

    .line 36
    invoke-virtual {p1, p2}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    return-object p1
.end method

.method public localizeChannelNames(Landroid/content/Context;)V
    .locals 6

    const-string v0, "notification"

    .line 72
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/NotificationManager;

    .line 74
    invoke-virtual {p1}, Landroid/app/NotificationManager;->getNotificationChannels()Ljava/util/List;

    move-result-object v0

    .line 76
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationChannel;

    .line 77
    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/notification/Channels;->getChannelById(Ljava/lang/String;)Lcom/squareup/notification/Channels;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 79
    new-instance v3, Landroid/app/NotificationChannel;

    .line 80
    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/notification/NotificationWrapper;->res:Lcom/squareup/util/Res;

    invoke-virtual {v2}, Lcom/squareup/notification/Channels;->getNameResId()I

    move-result v2

    invoke-interface {v5, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {v1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v1

    invoke-direct {v3, v4, v2, v1}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 82
    invoke-virtual {p1, v3}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    goto :goto_0

    :cond_1
    return-void
.end method
