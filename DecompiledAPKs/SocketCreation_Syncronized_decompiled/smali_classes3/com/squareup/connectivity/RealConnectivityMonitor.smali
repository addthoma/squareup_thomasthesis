.class public final Lcom/squareup/connectivity/RealConnectivityMonitor;
.super Ljava/lang/Object;
.source "RealConnectivityMonitor.java"

# interfaces
.implements Lcom/squareup/connectivity/ConnectivityMonitor;
.implements Lmortar/Scoped;


# instance fields
.field private final connectivityChangeReceiver:Landroid/content/BroadcastReceiver;

.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private final context:Landroid/app/Application;

.field private final internetState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/net/ConnectivityManager;Lcom/squareup/internet/InternetStatusMonitor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/squareup/connectivity/RealConnectivityMonitor$1;

    invoke-direct {v0, p0}, Lcom/squareup/connectivity/RealConnectivityMonitor$1;-><init>(Lcom/squareup/connectivity/RealConnectivityMonitor;)V

    iput-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->connectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 45
    iput-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->context:Landroid/app/Application;

    .line 46
    iput-object p2, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 47
    iput-object p3, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

    .line 49
    invoke-direct {p0}, Lcom/squareup/connectivity/RealConnectivityMonitor;->getInternetState()Lcom/squareup/connectivity/InternetState;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->internetState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/squareup/connectivity/InternetState;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/connectivity/RealConnectivityMonitor;->getInternetState()Lcom/squareup/connectivity/InternetState;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->internetState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/squareup/internet/InternetStatusMonitor;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

    return-object p0
.end method

.method private getInternetState()Lcom/squareup/connectivity/InternetState;
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, "NetworkInfo is %s"

    .line 74
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public internetState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->internetState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isConnected()Z
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->internetState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 61
    iget-object p1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->context:Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->connectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/Application;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->context:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/connectivity/RealConnectivityMonitor;->connectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
