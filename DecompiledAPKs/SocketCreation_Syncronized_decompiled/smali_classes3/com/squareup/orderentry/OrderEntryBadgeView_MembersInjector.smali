.class public final Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;
.super Ljava/lang/Object;
.source "OrderEntryBadgeView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/OrderEntryBadgeView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/OrderEntryBadgeView;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/orderentry/OrderEntryBadgeView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryBadgeView;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/OrderEntryBadgeView;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;->injectDevice(Lcom/squareup/orderentry/OrderEntryBadgeView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/orderentry/OrderEntryBadgeView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryBadgeView_MembersInjector;->injectMembers(Lcom/squareup/orderentry/OrderEntryBadgeView;)V

    return-void
.end method
