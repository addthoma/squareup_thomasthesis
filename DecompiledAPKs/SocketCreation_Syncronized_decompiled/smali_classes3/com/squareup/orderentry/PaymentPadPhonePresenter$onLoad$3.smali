.class public final Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "PaymentPadPhonePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadPhonePresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3",
        "Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;",
        "onPageScrolled",
        "",
        "position",
        "",
        "positionOffset",
        "",
        "positionOffsetPixels",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 81
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrolled(IFI)V
    .locals 0

    if-nez p1, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$hasView(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 88
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadPhonePresenter$onLoad$3;->this$0:Lcom/squareup/orderentry/PaymentPadPhonePresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadPhonePresenter;->access$getView(Lcom/squareup/orderentry/PaymentPadPhonePresenter;)Lcom/squareup/orderentry/PaymentPadPhoneView;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/PaymentPadPhoneView;->setTabsOffset(F)V

    :cond_0
    return-void
.end method
