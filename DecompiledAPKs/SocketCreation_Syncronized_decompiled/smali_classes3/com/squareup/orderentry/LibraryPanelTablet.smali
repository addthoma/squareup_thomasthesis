.class public Lcom/squareup/orderentry/LibraryPanelTablet;
.super Landroid/widget/LinearLayout;
.source "LibraryPanelTablet.java"


# static fields
.field private static final SEARCH_DELAY_MS:J = 0x64L

.field private static final X_ANIMATION_DURATION_MS:J = 0x64L


# instance fields
.field private backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private bringInX:Landroid/animation/Animator;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchEditText:Landroid/widget/EditText;

.field private searchExit:Landroid/view/View;

.field private final searchRunnable:Ljava/lang/Runnable;

.field private sendOutX:Landroid/animation/Animator;

.field private titleBar:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    .line 51
    new-instance p1, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTablet$Lwk6poY70qeKVFrrQKGVfOS1byU;

    invoke-direct {p1, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTablet$Lwk6poY70qeKVFrrQKGVfOS1byU;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    iput-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/LibraryPanelTablet;)Ljava/lang/Runnable;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/LibraryPanelTablet;)Landroid/view/View;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    return-object p0
.end method

.method private initializeAnimations()V
    .locals 6

    .line 117
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    const-string v3, "alpha"

    invoke-static {v0, v3, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->bringInX:Landroid/animation/Animator;

    .line 118
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->bringInX:Landroid/animation/Animator;

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 119
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->bringInX:Landroid/animation/Animator;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->bringInX:Landroid/animation/Animator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->bringInX:Landroid/animation/Animator;

    new-instance v2, Lcom/squareup/orderentry/LibraryPanelTablet$4;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/LibraryPanelTablet$4;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    new-array v1, v1, [F

    fill-array-data v1, :array_1

    invoke-static {v0, v3, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->sendOutX:Landroid/animation/Animator;

    .line 129
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->sendOutX:Landroid/animation/Animator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 130
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->sendOutX:Landroid/animation/Animator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->sendOutX:Landroid/animation/Animator;

    new-instance v1, Lcom/squareup/orderentry/LibraryPanelTablet$5;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryPanelTablet$5;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method protected clearSearchText()V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected hideKeyboard()V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    return-void
.end method

.method protected hideSearchExit(Z)V
    .locals 1

    const/4 v0, 0x4

    if-eqz p1, :cond_0

    .line 172
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eq p1, v0, :cond_1

    .line 173
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->sendOutX:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 176
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$new$0$LibraryPanelTablet()V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    iget-object v1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->onTextSearched(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$LibraryPanelTablet(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 101
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->startSearching()V

    goto :goto_0

    .line 103
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 104
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->stopSearching()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/LibraryPanelTablet;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 112
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->dropView(Ljava/lang/Object;)V

    .line 113
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    sget v0, Lcom/squareup/orderentry/R$id;->library_title_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->titleBar:Landroid/view/View;

    .line 58
    sget v0, Lcom/squareup/orderentry/R$id;->search_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    .line 59
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryPanelTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SEARCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 60
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    .line 61
    invoke-virtual {v1}, Landroid/widget/EditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList(Landroid/content/res/ColorStateList;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 65
    sget v0, Lcom/squareup/orderentry/R$id;->library_back_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/orderentry/LibraryPanelTablet$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryPanelTablet$1;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    sget v0, Lcom/squareup/orderentry/R$id;->library_search_exit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/LibraryPanelTablet$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryPanelTablet$2;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget v0, Lcom/squareup/orderentry/R$drawable;->panel_background_clear_bottom_border:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/LibraryPanelTablet;->setBackgroundResource(I)V

    .line 84
    :cond_0
    invoke-direct {p0}, Lcom/squareup/orderentry/LibraryPanelTablet;->initializeAnimations()V

    .line 86
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->takeView(Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/orderentry/LibraryPanelTablet$3;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryPanelTablet$3;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTablet$HeksKD-TmtbP89DJ9lU8XhW0wec;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryPanelTablet$HeksKD-TmtbP89DJ9lU8XhW0wec;-><init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method protected setBackButtonVisible(Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 144
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 145
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setClickable(Z)V

    goto :goto_0

    .line 147
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 148
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setClickable(Z)V

    :goto_0
    return-void
.end method

.method public setLibraryBarTitle(Ljava/lang/String;)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->backButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLibraryBarVisible(Z)V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->titleBar:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method protected showSearchExit(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 162
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 163
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->bringInX:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 166
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet;->searchExit:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method
