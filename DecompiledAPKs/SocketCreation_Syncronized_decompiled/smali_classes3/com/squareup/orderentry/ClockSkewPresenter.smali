.class public Lcom/squareup/orderentry/ClockSkewPresenter;
.super Lmortar/ViewPresenter;
.source "ClockSkewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/ClockSkewView;",
        ">;"
    }
.end annotation


# instance fields
.field private clockSkew:Lcom/squareup/orderentry/ClockSkew;

.field private final deepLinks:Lcom/squareup/ui/main/DeepLinks;

.field private flow:Lflow/Flow;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/ClockSkew;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/DeepLinks;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->clockSkew:Lcom/squareup/orderentry/ClockSkew;

    .line 26
    iput-object p2, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->flow:Lflow/Flow;

    .line 27
    iput-object p3, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 28
    iput-object p4, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    return-void
.end method

.method private checkForClockSkew()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->clockSkew:Lcom/squareup/orderentry/ClockSkew;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$ClockSkewPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/orderentry/ClockSkewPresenter;->checkForClockSkew()V

    return-void
.end method

.method protected onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 32
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 35
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$ClockSkewPresenter$eNK7D6sKIxSNelpOCsHcpTZ70E4;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$ClockSkewPresenter$eNK7D6sKIxSNelpOCsHcpTZ70E4;-><init>(Lcom/squareup/orderentry/ClockSkewPresenter;)V

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 34
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method showDateAndTimeSettings()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkewPresenter;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    sget-object v1, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;->INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;

    invoke-virtual {v1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;->getDeepLink()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/squareup/orderentry/ClockSkewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/ClockSkewView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ClockSkewView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Intents;->showDateAndTimeSettings(Landroid/content/Context;)V

    :cond_0
    return-void
.end method
