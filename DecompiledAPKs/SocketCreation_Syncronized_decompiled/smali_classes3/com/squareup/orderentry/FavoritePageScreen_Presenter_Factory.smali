.class public final Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "FavoritePageScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/FavoritePageScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardActivationFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardsFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tagDiscountFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/TagDiscountFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/TagDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 80
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 81
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 82
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 83
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 84
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 85
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 86
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->tagDiscountFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 87
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 88
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 89
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 90
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 91
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->itemSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 92
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 93
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->redeemRewardsFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 94
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 95
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 96
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->giftCardActivationFlowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/TagDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ">;)",
            "Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 118
    new-instance v18, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/cart/TagDiscountFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;)Lcom/squareup/orderentry/FavoritePageScreen$Presenter;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            "Lcom/squareup/badbus/BadBus;",
            "Lflow/Flow;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/cart/TagDiscountFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ")",
            "Lcom/squareup/orderentry/FavoritePageScreen$Presenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 130
    new-instance v18, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;-><init>(Ljavax/inject/Provider;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/cart/TagDiscountFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/FavoritePageScreen$Presenter;
    .locals 19

    move-object/from16 v0, p0

    .line 101
    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/CheckoutEntryHandler;

    iget-object v3, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v4, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lflow/Flow;

    iget-object v5, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/orderentry/pages/OrderEntryPages;

    iget-object v6, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v7, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->tagDiscountFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/cart/TagDiscountFormatter;

    iget-object v8, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v9, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v10, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v10}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v11, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v12, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->itemSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v12}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v13, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v13}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/util/Res;

    iget-object v14, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->redeemRewardsFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v14}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    iget-object v15, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v15}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->giftCardActivationFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    move-object/from16 v1, v18

    invoke-static/range {v1 .. v17}, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/cart/TagDiscountFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;)Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen_Presenter_Factory;->get()Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
