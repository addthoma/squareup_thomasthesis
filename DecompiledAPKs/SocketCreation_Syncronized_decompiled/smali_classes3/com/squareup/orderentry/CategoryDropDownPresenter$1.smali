.class Lcom/squareup/orderentry/CategoryDropDownPresenter$1;
.super Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;
.source "CategoryDropDownPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/CategoryDropDownPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/CategoryDropDownPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter$1;->this$0:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropDownHiding(Landroid/view/View;)V
    .locals 1

    .line 97
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter$1;->this$0:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->access$000(Lcom/squareup/orderentry/CategoryDropDownPresenter;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onDropDownOpening(Landroid/view/View;)V
    .locals 1

    .line 93
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter$1;->this$0:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->access$000(Lcom/squareup/orderentry/CategoryDropDownPresenter;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
