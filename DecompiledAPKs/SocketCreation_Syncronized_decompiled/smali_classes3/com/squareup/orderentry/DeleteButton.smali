.class public Lcom/squareup/orderentry/DeleteButton;
.super Landroid/view/View;
.source "DeleteButton.java"


# instance fields
.field private final circlePaint:Landroid/graphics/Paint;

.field private final circlePressedPaint:Landroid/graphics/Paint;

.field private halfXStroke:F

.field private final rectF:Landroid/graphics/RectF;

.field private final strokePaint:Landroid/graphics/Paint;

.field private final xCirclePaint:Landroid/graphics/Paint;

.field private final xPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 28
    new-instance p2, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p2, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->strokePaint:Landroid/graphics/Paint;

    .line 29
    iget-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->strokePaint:Landroid/graphics/Paint;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    iget-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->strokePaint:Landroid/graphics/Paint;

    sget v1, Lcom/squareup/widgets/pos/R$dimen;->grid_delete_button_circle_stroke:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 31
    iget-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->strokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 33
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->circlePaint:Landroid/graphics/Paint;

    .line 34
    iget-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->circlePaint:Landroid/graphics/Paint;

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->circlePressedPaint:Landroid/graphics/Paint;

    .line 37
    iget-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->circlePressedPaint:Landroid/graphics/Paint;

    sget v1, Lcom/squareup/marin/R$color;->marin_ultra_light_gray:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/orderentry/DeleteButton;->xPaint:Landroid/graphics/Paint;

    .line 40
    sget p2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    .line 41
    iget-object v1, p0, Lcom/squareup/orderentry/DeleteButton;->xPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    sget v1, Lcom/squareup/widgets/pos/R$dimen;->grid_delete_button_x_stroke:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, p1, v1

    .line 43
    iput v1, p0, Lcom/squareup/orderentry/DeleteButton;->halfXStroke:F

    .line 44
    iget-object v1, p0, Lcom/squareup/orderentry/DeleteButton;->xPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 46
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/orderentry/DeleteButton;->xCirclePaint:Landroid/graphics/Paint;

    .line 47
    iget-object p1, p0, Lcom/squareup/orderentry/DeleteButton;->xCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/DeleteButton;->rectF:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .line 53
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 15

    move-object v0, p0

    move-object/from16 v7, p1

    .line 58
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 60
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getWidth()I

    move-result v1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getHeight()I

    move-result v2

    .line 63
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getPaddingLeft()I

    move-result v3

    .line 64
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getPaddingTop()I

    move-result v4

    .line 65
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getPaddingRight()I

    move-result v5

    .line 66
    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->getPaddingBottom()I

    move-result v6

    .line 68
    iget-object v8, v0, Lcom/squareup/orderentry/DeleteButton;->rectF:Landroid/graphics/RectF;

    int-to-float v9, v3

    int-to-float v10, v4

    sub-int v11, v1, v5

    int-to-float v12, v11

    sub-int v13, v2, v6

    int-to-float v14, v13

    invoke-virtual {v8, v9, v10, v12, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    iget-object v8, v0, Lcom/squareup/orderentry/DeleteButton;->rectF:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/squareup/orderentry/DeleteButton;->isPressed()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, v0, Lcom/squareup/orderentry/DeleteButton;->circlePressedPaint:Landroid/graphics/Paint;

    goto :goto_0

    :cond_0
    iget-object v9, v0, Lcom/squareup/orderentry/DeleteButton;->circlePaint:Landroid/graphics/Paint;

    :goto_0
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 71
    iget-object v8, v0, Lcom/squareup/orderentry/DeleteButton;->rectF:Landroid/graphics/RectF;

    iget-object v9, v0, Lcom/squareup/orderentry/DeleteButton;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    sub-int/2addr v1, v3

    sub-int/2addr v1, v5

    .line 73
    div-int/lit8 v1, v1, 0x3

    sub-int/2addr v2, v4

    sub-int/2addr v2, v6

    .line 74
    div-int/lit8 v2, v2, 0x3

    add-int/2addr v3, v1

    add-int/2addr v4, v2

    sub-int/2addr v11, v1

    sub-int/2addr v13, v2

    int-to-float v8, v3

    int-to-float v9, v4

    int-to-float v10, v11

    int-to-float v11, v13

    .line 81
    iget-object v6, v0, Lcom/squareup/orderentry/DeleteButton;->xPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v2, v8

    move v3, v9

    move v4, v10

    move v5, v11

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 82
    iget-object v6, v0, Lcom/squareup/orderentry/DeleteButton;->xPaint:Landroid/graphics/Paint;

    move v3, v11

    move v5, v9

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 83
    iget v1, v0, Lcom/squareup/orderentry/DeleteButton;->halfXStroke:F

    iget-object v2, v0, Lcom/squareup/orderentry/DeleteButton;->xCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v8, v9, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 84
    iget v1, v0, Lcom/squareup/orderentry/DeleteButton;->halfXStroke:F

    iget-object v2, v0, Lcom/squareup/orderentry/DeleteButton;->xCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v10, v9, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 85
    iget v1, v0, Lcom/squareup/orderentry/DeleteButton;->halfXStroke:F

    iget-object v2, v0, Lcom/squareup/orderentry/DeleteButton;->xCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v8, v11, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 86
    iget v1, v0, Lcom/squareup/orderentry/DeleteButton;->halfXStroke:F

    iget-object v2, v0, Lcom/squareup/orderentry/DeleteButton;->xCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v10, v11, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method
