.class public Lcom/squareup/orderentry/KeypadEntryCardPresenter;
.super Lmortar/ViewPresenter;
.source "KeypadEntryCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;,
        Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/KeypadEntryCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

.field private final vibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Landroid/os/Vibrator;Lcom/squareup/orderentry/KeypadEntryScreen$Runner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->res:Lcom/squareup/util/Res;

    .line 41
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 42
    iput-object p3, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 43
    iput-object p4, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->vibrator:Landroid/os/Vibrator;

    .line 44
    iput-object p5, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Ljava/lang/Object;
    .locals 0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Landroid/os/Vibrator;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->vibrator:Landroid/os/Vibrator;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Lcom/squareup/orderentry/KeypadEntryScreen$Runner;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->displayInfo()V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)Ljava/lang/Object;
    .locals 0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/orderentry/KeypadEntryCardPresenter;Z)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->displayPercentage(Z)V

    return-void
.end method

.method private displayAmount(Z)V
    .locals 4

    .line 94
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v0

    .line 95
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/KeypadEntryCardView;

    iget-object v2, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 96
    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 97
    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-virtual {v2, v3, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/KeypadEntryCardView;->setAmountText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->updateValueColor(Z)V

    return-void
.end method

.method private displayInfo()V
    .locals 6

    .line 81
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$1;->$SwitchMap$com$squareup$orderentry$KeypadEntryScreen$KeypadInfo$Type:[I

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 86
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->displayPercentage(Z)V

    goto :goto_1

    .line 89
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->displayAmount(Z)V

    :goto_1
    return-void
.end method

.method private displayPercentage(Z)V
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "0"

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/KeypadEntryCardView;

    iget-object v2, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/configure/item/R$string;->percent:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "content"

    .line 123
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 122
    invoke-virtual {v1, v2}, Lcom/squareup/orderentry/KeypadEntryCardView;->setAmountText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/util/Percentage;->isPositive()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    .line 126
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 125
    :goto_0
    invoke-direct {p0, v2}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->updateValueColor(Z)V

    return-void
.end method

.method private updateValueColor(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/KeypadEntryCardView;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/KeypadEntryCardView;->setAmountColor(I)V

    goto :goto_0

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/KeypadEntryCardView;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/KeypadEntryCardView;->setAmountColor(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->closeKeypadEntryCard(Z)V

    return-void
.end method

.method onBackPressed()Z
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->closeKeypadEntryCard(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 48
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 52
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/KeypadEntryCardView;

    .line 56
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$1;->$SwitchMap$com$squareup$orderentry$KeypadEntryScreen$KeypadInfo$Type:[I

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 61
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter$PercentageEntryKeypadListener;-><init>(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V

    goto :goto_0

    .line 64
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected Keypad Type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    .line 65
    invoke-interface {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 58
    :cond_1
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter$MoneyEntryKeypadListener;-><init>(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V

    .line 68
    :goto_0
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->displayInfo()V

    .line 70
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    .line 71
    invoke-interface {v3}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$eOvmzID3wUd-nJBKAdvutPEX9vs;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/-$$Lambda$eOvmzID3wUd-nJBKAdvutPEX9vs;-><init>(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V

    .line 72
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    .line 73
    invoke-interface {v2}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getPrimaryButtonText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$xDP9LraCq1K4L8s2Oe_xzoIVaSE;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/-$$Lambda$xDP9LraCq1K4L8s2Oe_xzoIVaSE;-><init>(Lcom/squareup/orderentry/KeypadEntryCardPresenter;)V

    .line 74
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 76
    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/KeypadEntryCardView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 77
    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/KeypadEntryCardView;->setListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method public setMoney(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->setMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-void
.end method

.method public setPercentage(Ljava/lang/String;)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->runner:Lcom/squareup/orderentry/KeypadEntryScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$Runner;->keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->setPercentage(Ljava/lang/String;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-void
.end method
