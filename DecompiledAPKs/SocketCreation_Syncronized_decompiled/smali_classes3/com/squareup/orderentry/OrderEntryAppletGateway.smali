.class public interface abstract Lcom/squareup/orderentry/OrderEntryAppletGateway;
.super Ljava/lang/Object;
.source "OrderEntryAppletGateway.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0005H&J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&J\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000cH&J\u0008\u0010\u000e\u001a\u00020\u0003H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "",
        "activateApplet",
        "",
        "hasFavoritesEditor",
        "",
        "hasOrderEntryApplet",
        "historyFactoryForMode",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "mode",
        "Lcom/squareup/orderentry/OrderEntryMode;",
        "historyForFavoritesEditor",
        "Lflow/History;",
        "currentHistory",
        "selectApplet",
        "order-entry-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract activateApplet()V
.end method

.method public abstract hasFavoritesEditor()Z
.end method

.method public abstract hasOrderEntryApplet()Z
.end method

.method public abstract historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;
.end method

.method public abstract historyForFavoritesEditor(Lflow/History;)Lflow/History;
.end method

.method public abstract selectApplet()V
.end method
