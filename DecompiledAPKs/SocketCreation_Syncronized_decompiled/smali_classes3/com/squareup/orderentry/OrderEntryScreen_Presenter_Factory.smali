.class public final Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "OrderEntryScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/OrderEntryScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final backHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final clockSkewProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ClockSkew;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final drawerPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedInQueuesEmptyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/user/NotificationPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final o1ReminderLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseNarcRegistryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final sellerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private final switchEmployeesEducationPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tempPhotoDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/user/NotificationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ClockSkew;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 119
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 120
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 121
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->switchEmployeesEducationPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 122
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->pauseNarcRegistryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 123
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->loggedInQueuesEmptyProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 124
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 125
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->tempPhotoDirProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 126
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 127
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 128
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 129
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 130
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->backHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 131
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->notificationPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 132
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 133
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->clockSkewProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 134
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 135
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->drawerPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 136
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 137
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 138
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 139
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 140
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 141
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->o1ReminderLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 142
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 143
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 144
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 145
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->sellerScopeRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 146
    iput-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/user/NotificationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/ClockSkew;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;)",
            "Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    .line 179
    new-instance v29, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;

    move-object/from16 v0, v29

    invoke-direct/range {v0 .. v28}, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v29
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/pauses/PauseAndResumeRegistrar;Ljavax/inject/Provider;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Ljava/io/File;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenBackHandler;Lcom/squareup/user/NotificationPresenter;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/orderentry/ClockSkew;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Ljava/util/concurrent/Executor;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;)Lcom/squareup/orderentry/OrderEntryScreen$Presenter;
    .locals 30
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            "Ljava/io/File;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler;",
            "Lcom/squareup/user/NotificationPresenter;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/orderentry/ClockSkew;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ")",
            "Lcom/squareup/orderentry/OrderEntryScreen$Presenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    .line 197
    new-instance v29, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    move-object/from16 v0, v29

    invoke-direct/range {v0 .. v28}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/pauses/PauseAndResumeRegistrar;Ljavax/inject/Provider;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Ljava/io/File;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenBackHandler;Lcom/squareup/user/NotificationPresenter;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/orderentry/ClockSkew;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Ljava/util/concurrent/Executor;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;)V

    return-object v29
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/OrderEntryScreen$Presenter;
    .locals 30

    move-object/from16 v0, p0

    .line 151
    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->switchEmployeesEducationPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->pauseNarcRegistryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v6, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->loggedInQueuesEmptyProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->tempPhotoDirProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Ljava/io/File;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->orderEntryViewPagerPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->backHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/orderentry/OrderEntryScreenBackHandler;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->notificationPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/user/NotificationPresenter;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->clockSkewProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/orderentry/ClockSkew;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->drawerPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/applet/AppletsDrawerPresenter;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Ljava/util/concurrent/Executor;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->o1ReminderLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->sellerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/ui/seller/SellerScopeRunner;

    iget-object v1, v0, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/applet/ActionBarNavigationHelper;

    invoke-static/range {v2 .. v29}, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;Lcom/squareup/pauses/PauseAndResumeRegistrar;Ljavax/inject/Provider;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Ljava/io/File;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenBackHandler;Lcom/squareup/user/NotificationPresenter;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/orderentry/ClockSkew;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Ljava/util/concurrent/Executor;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/reader_deprecation/O1ReminderLauncher;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;)Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreen_Presenter_Factory;->get()Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
