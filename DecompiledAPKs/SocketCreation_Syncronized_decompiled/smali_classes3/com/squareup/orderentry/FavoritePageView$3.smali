.class Lcom/squareup/orderentry/FavoritePageView$3;
.super Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/FavoritePageView;->createItemView(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/FavoritePageView;

.field final synthetic val$item:Lcom/squareup/shared/catalog/models/CatalogItem;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/shared/catalog/models/CatalogItem;)V
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$3;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView$3;->val$view:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/orderentry/FavoritePageView$3;->val$item:Lcom/squareup/shared/catalog/models/CatalogItem;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;-><init>(Lcom/squareup/orderentry/FavoritePageView$1;)V

    return-void
.end method


# virtual methods
.method public click(Landroid/view/View;)V
    .locals 2

    .line 365
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$3;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    iget-object p1, p1, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView$3;->val$view:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageView$3;->val$item:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->onItemClicked(Landroid/view/View;Lcom/squareup/shared/catalog/models/CatalogItem;)V

    return-void
.end method
