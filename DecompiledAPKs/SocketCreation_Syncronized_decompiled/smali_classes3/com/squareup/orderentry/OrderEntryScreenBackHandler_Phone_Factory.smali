.class public final Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;
.super Ljava/lang/Object;
.source "OrderEntryScreenBackHandler_Phone_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;",
        ">;"
    }
.end annotation


# instance fields
.field private final categoryDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CategoryDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CategoryDropDownPresenter;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->libraryStateProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->categoryDropDownPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CategoryDropDownPresenter;",
            ">;)",
            "Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/CategoryDropDownPresenter;)Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/CategoryDropDownPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->libraryStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/librarylist/LibraryListStateManager;

    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->categoryDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-static {v0, v1, v2}, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/CategoryDropDownPresenter;)Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreenBackHandler_Phone_Factory;->get()Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;

    move-result-object v0

    return-object v0
.end method
