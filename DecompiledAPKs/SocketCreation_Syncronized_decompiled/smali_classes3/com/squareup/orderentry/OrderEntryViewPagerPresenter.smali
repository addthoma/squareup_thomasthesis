.class public Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;
.super Lmortar/ViewPresenter;
.source "OrderEntryViewPagerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/OrderEntryViewPager;",
        ">;"
    }
.end annotation


# instance fields
.field private currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field private final device:Lcom/squareup/util/Device;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

.field private latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private orderEntryScreen:Lcom/squareup/orderentry/OrderEntryScreen;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private pageScrollListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

.field private final subs:Lrx/subscriptions/CompositeSubscription;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/ui/items/ItemsAppletGateway;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 41
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 43
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 59
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->device:Lcom/squareup/util/Device;

    .line 60
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 61
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 62
    iput-object p4, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 63
    iput-object p5, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

    .line 64
    iput-object p6, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->pageScrollListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Lcom/squareup/orderentry/pages/OrderEntryPageList;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Ljava/lang/Object;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Ljava/lang/Object;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Lcom/squareup/orderentry/pages/OrderEntryPages;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method

.method private postTutorialEventKeypadDisplayed()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "KeyPad Displayed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method private shouldEnablePaging(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)Z
    .locals 1

    .line 190
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->device:Lcom/squareup/util/Device;

    .line 192
    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public animate(Z)V
    .locals 1

    .line 200
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getCurrentItem()I

    move-result v0

    if-eqz p1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 206
    :goto_0
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelCount()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(I)V

    return-void
.end method

.method public animateToKeypad()V
    .locals 4

    .line 211
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPanelIndex()I

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 212
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Animating to keypad, index %d"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(I)V

    return-void
.end method

.method public animateToLibrary()V
    .locals 4

    .line 217
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->libraryPanelIndex()I

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 218
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Animating to library, index %d"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(I)V

    return-void
.end method

.method public currentPage()I
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelIndex(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)I

    move-result v0

    return v0
.end method

.method public dropView(Lcom/squareup/orderentry/OrderEntryViewPager;)V
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 180
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 182
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->dropView(Lcom/squareup/orderentry/OrderEntryViewPager;)V

    return-void
.end method

.method public getPageAt(I)Lcom/squareup/orderentry/pages/OrderEntryPage;
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->getPanel(I)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object p1

    return-object p1
.end method

.method public goBackToItemsAppletAfterDoneEditing()V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/items/ItemsAppletGateway;->goBackToItemsAppletWithHistoryPreserved()V

    return-void
.end method

.method public keypadPanelIndex()I
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPanelIndex()I

    move-result v0

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$OrderEntryViewPagerPresenter(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 73
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 75
    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive()Z

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->shouldEnablePaging(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)Z

    move-result p1

    .line 74
    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setPagingEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$OrderEntryViewPagerPresenter(ZLcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 2

    .line 82
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 83
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    if-eqz p2, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelIndex(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)I

    move-result v0

    invoke-virtual {p2, v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(IZ)V

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    sget-object p2, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, p2, :cond_1

    .line 87
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->postTutorialEventKeypadDisplayed()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$onEnterScope$2$OrderEntryViewPagerPresenter(ZLcom/squareup/orderentry/pages/OrderEntryPageList;)V
    .locals 2

    .line 94
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryViewPager;->updateAdapter()V

    .line 96
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelIndex(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)I

    move-result v0

    invoke-virtual {p2, v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setCurrentItem(IZ)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$3$OrderEntryViewPagerPresenter(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 104
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 106
    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v1

    .line 107
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 106
    invoke-direct {p0, v1, p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->shouldEnablePaging(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)Z

    move-result p1

    .line 105
    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setPagingEnabled(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$4$OrderEntryViewPagerPresenter(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq p1, v0, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->goBackToItemsAppletAfterDoneEditing()V

    :cond_0
    return-void
.end method

.method public libraryPanelIndex()I
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->libraryPanelIndex()I

    move-result v0

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 68
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen;

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreen:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 70
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 71
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$S9BUSCRreEvd5Pu2N9bVN4tH6XQ;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$S9BUSCRreEvd5Pu2N9bVN4tH6XQ;-><init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 70
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 79
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 80
    invoke-virtual {v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->observeCurrentPage()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$mJgrEC7DE43aDZpRrwVbKgATrno;

    invoke-direct {v2, p0, v0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$mJgrEC7DE43aDZpRrwVbKgATrno;-><init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Z)V

    .line 81
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 79
    invoke-static {p1, v1}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 92
    invoke-virtual {v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->observe()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$kK8EBhCcvSbmmP3gqJlNg3oG66I;

    invoke-direct {v2, p0, v0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$kK8EBhCcvSbmmP3gqJlNg3oG66I;-><init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;Z)V

    .line 93
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 91
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 102
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->librarySearchIsActive()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$UPizUd2kL2N1FogB-H9773yReO8;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$UPizUd2kL2N1FogB-H9773yReO8;-><init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V

    .line 103
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 101
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 114
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPager;

    .line 117
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->installAdapter()V

    .line 120
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 121
    invoke-virtual {v2}, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive()Z

    move-result v2

    .line 120
    invoke-direct {p0, v1, v2}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->shouldEnablePaging(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setPagingEnabled(Z)V

    if-nez p1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreen:Lcom/squareup/orderentry/OrderEntryScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    sget-object v2, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    if-ne v1, v2, :cond_0

    .line 127
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v2, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v1, v2}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 128
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->startEditing()V

    :cond_0
    if-nez p1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreen:Lcom/squareup/orderentry/OrderEntryScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    sget-object v2, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    if-ne v1, v2, :cond_1

    .line 135
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v2, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v1, v2}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    :cond_1
    if-nez p1, :cond_2

    .line 138
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreen:Lcom/squareup/orderentry/OrderEntryScreen;

    iget-object p1, p1, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

    if-ne p1, v1, :cond_2

    .line 139
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {p1, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 142
    :cond_2
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreen:Lcom/squareup/orderentry/OrderEntryScreen;

    iget-object p1, p1, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    if-ne p1, v1, :cond_3

    .line 143
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 144
    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$ekO1GoqJAHRmaf0TDesmA1AHwDk;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryViewPagerPresenter$ekO1GoqJAHRmaf0TDesmA1AHwDk;-><init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V

    .line 145
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 143
    invoke-virtual {p1, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 155
    :cond_3
    new-instance p1, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;-><init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method public pageIdIndex(Ljava/lang/String;)I
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPageIdIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public panelCount()I
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelCount()I

    move-result v0

    return v0
.end method

.method public setPageScrollListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->pageScrollListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    return-void
.end method
