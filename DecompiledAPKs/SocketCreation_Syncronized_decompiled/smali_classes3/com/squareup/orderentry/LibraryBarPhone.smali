.class public Lcom/squareup/orderentry/LibraryBarPhone;
.super Landroid/widget/FrameLayout;
.source "LibraryBarPhone.java"


# static fields
.field private static final SEARCH_DELAY_MS:J = 0x64L


# instance fields
.field private defaultContainer:Landroid/view/View;

.field private dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

.field private dropDownTitle:Landroid/widget/TextView;

.field private final fadeInDuration:I

.field private final minHeight:I

.field presenter:Lcom/squareup/orderentry/LibraryBarPhonePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchButton:Landroid/view/View;

.field private searchContainer:Landroid/view/View;

.field private searchEditText:Landroid/widget/EditText;

.field private searchExitButton:Landroid/view/View;

.field private final searchRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryBarPhone;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;->inject(Lcom/squareup/orderentry/LibraryBarPhone;)V

    .line 46
    new-instance p1, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhone$aFx2TOIXDPdGa6mRRoIlsolt3P8;

    invoke-direct {p1, p0}, Lcom/squareup/orderentry/-$$Lambda$LibraryBarPhone$aFx2TOIXDPdGa6mRRoIlsolt3P8;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;)V

    iput-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchRunnable:Ljava/lang/Runnable;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/orderentry/LibraryBarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 48
    sget p2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/LibraryBarPhone;->minHeight:I

    const p2, 0x10e0001

    .line 49
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->fadeInDuration:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/LibraryBarPhone;)Landroid/widget/EditText;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/LibraryBarPhone;)Ljava/lang/Runnable;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method private setSearchImmediately(Z)V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x8

    if-eqz p1, :cond_0

    .line 142
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 143
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 146
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .line 100
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->presenter:Lcom/squareup/orderentry/LibraryBarPhonePresenter;

    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->onTextSearched(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$new$0$LibraryBarPhone()V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->presenter:Lcom/squareup/orderentry/LibraryBarPhonePresenter;

    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->onTextSearched(Ljava/lang/String;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/LibraryBarPhone;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 109
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->presenter:Lcom/squareup/orderentry/LibraryBarPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->dropView(Ljava/lang/Object;)V

    .line 110
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 53
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 55
    sget v0, Lcom/squareup/orderentry/R$id;->library_search_activator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchButton:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/LibraryBarPhone$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryBarPhone$1;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    sget v0, Lcom/squareup/orderentry/R$id;->library_search_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    .line 64
    sget v0, Lcom/squareup/orderentry/R$id;->library_default_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    .line 66
    sget v0, Lcom/squareup/orderentry/R$id;->category_drop_down_arrow:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    .line 67
    sget v0, Lcom/squareup/orderentry/R$id;->category_drop_down_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownTitle:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/orderentry/LibraryBarPhone$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryBarPhone$2;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    sget v0, Lcom/squareup/orderentry/R$id;->library_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    .line 77
    sget v0, Lcom/squareup/orderentry/R$id;->library_search_exit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchExitButton:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchExitButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/LibraryBarPhone$3;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryBarPhone$3;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchExitButton:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->presenter:Lcom/squareup/orderentry/LibraryBarPhonePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/LibraryBarPhonePresenter;->takeView(Ljava/lang/Object;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/orderentry/LibraryBarPhone$4;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/LibraryBarPhone$4;-><init>(Lcom/squareup/orderentry/LibraryBarPhone;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setCategoryDropDownEnabled(Z)V
    .locals 4

    .line 182
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/squareup/orderentry/LibraryBarPhone;->minHeight:I

    if-eqz p1, :cond_1

    mul-int/lit8 v3, v3, 0x2

    :cond_1
    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 185
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setLibraryBarTitle(Ljava/lang/String;)V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSearch(ZZ)V
    .locals 4

    if-nez p2, :cond_0

    .line 116
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/LibraryBarPhone;->setSearchImmediately(Z)V

    goto :goto_0

    :cond_0
    const/16 p2, 0x8

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    .line 118
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 120
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->fadeInDuration:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 122
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    .line 125
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    .line 126
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    .line 128
    :cond_1
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 129
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->defaultContainer:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    iget v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->fadeInDuration:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 132
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchContainer:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchEditText:Landroid/widget/EditText;

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public toggleSearchButton(ZZ)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    if-eqz p2, :cond_1

    .line 155
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 156
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->searchButton:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 159
    iget-object v1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setTranslationX(F)V

    :goto_1
    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 164
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animateArrowDown()V

    goto :goto_2

    .line 166
    :cond_2
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setArrowDown()V

    goto :goto_2

    :cond_3
    if-eqz p2, :cond_4

    .line 170
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->animateArrowUp()V

    goto :goto_2

    .line 172
    :cond_4
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryBarPhone;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setArrowUp()V

    :goto_2
    return-void
.end method
