.class Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ChargeAndTicketsButtons.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTicketSavedButtonsOutAnimator()Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 268
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$200(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/ImageView;

    move-result-object p1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$200(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/ImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 260
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$300(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$200(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 262
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setEnabled(Z)V

    return-void
.end method
