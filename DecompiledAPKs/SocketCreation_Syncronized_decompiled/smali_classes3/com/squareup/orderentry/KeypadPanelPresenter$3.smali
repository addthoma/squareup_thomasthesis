.class Lcom/squareup/orderentry/KeypadPanelPresenter$3;
.super Lcom/squareup/mortar/PopupPresenter;
.source "KeypadPanelPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/KeypadPanelPresenter;->buildClearCardOrSalePopupPresenter()Lcom/squareup/mortar/PopupPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/Showing;",
        "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$3;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;)V
    .locals 1

    .line 276
    sget-object v0, Lcom/squareup/orderentry/KeypadPanelPresenter$4;->$SwitchMap$com$squareup$orderentry$ClearCardOrSalePopup$Choices:[I

    invoke-virtual {p1}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 287
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Unexpected result from ClearCardPopup!"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 281
    :cond_1
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$3;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$000(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->reset()V

    goto :goto_0

    .line 278
    :cond_2
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$3;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$000(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 274
    check-cast p1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadPanelPresenter$3;->onPopupResult(Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;)V

    return-void
.end method
