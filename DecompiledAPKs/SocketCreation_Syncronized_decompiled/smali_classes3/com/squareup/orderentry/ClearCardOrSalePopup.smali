.class Lcom/squareup/orderentry/ClearCardOrSalePopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "ClearCardOrSalePopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/Showing;",
        "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    if-eq p2, p1, :cond_0

    goto :goto_0

    .line 39
    :cond_0
    sget-object p1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->SALE:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_1
    sget-object p1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CARD:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 45
    sget-object p1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CANCEL:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    .line 46
    sget-object p1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CANCEL:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Showing;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/CharSequence;

    .line 26
    invoke-virtual {p0}, Lcom/squareup/orderentry/ClearCardOrSalePopup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/orderentry/R$string;->clear_card:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    const/4 v0, 0x0

    aput-object p2, p1, v0

    .line 27
    invoke-virtual {p0}, Lcom/squareup/orderentry/ClearCardOrSalePopup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/orderentry/R$string;->clear_sale:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    const/4 v0, 0x1

    aput-object p2, p1, v0

    .line 29
    new-instance p2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/orderentry/ClearCardOrSalePopup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$ClearCardOrSalePopup$JKMBKfLUu57jUVhVzBgu9dd49SU;

    invoke-direct {v0, p3}, Lcom/squareup/orderentry/-$$Lambda$ClearCardOrSalePopup$JKMBKfLUu57jUVhVzBgu9dd49SU;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 30
    invoke-virtual {p2, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$ClearCardOrSalePopup$UY3oWAdSZnrFbgWf5OyVNO5_XtU;

    invoke-direct {v0, p3}, Lcom/squareup/orderentry/-$$Lambda$ClearCardOrSalePopup$UY3oWAdSZnrFbgWf5OyVNO5_XtU;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 44
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/orderentry/-$$Lambda$ClearCardOrSalePopup$B7UrPZUuyRSJMaBMnA_mZ-bTCP0;

    invoke-direct {p2, p3}, Lcom/squareup/orderentry/-$$Lambda$ClearCardOrSalePopup$B7UrPZUuyRSJMaBMnA_mZ-bTCP0;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 46
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_title:I

    .line 47
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/ui/Showing;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/orderentry/ClearCardOrSalePopup;->createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
