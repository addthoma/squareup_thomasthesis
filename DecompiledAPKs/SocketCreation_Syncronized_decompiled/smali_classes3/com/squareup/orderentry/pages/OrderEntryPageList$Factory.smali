.class Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;
.super Ljava/lang/Object;
.source "OrderEntryPageList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/pages/OrderEntryPageList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Factory"
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final device:Lcom/squareup/util/Device;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->device:Lcom/squareup/util/Device;

    .line 177
    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->res:Lcom/squareup/util/Res;

    .line 178
    iput-object p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method static synthetic lambda$createFavoritesPage$0(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$createFavoritesPage$1(Lcom/squareup/util/Res;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)Ljava/lang/String;
    .locals 1

    .line 208
    sget v0, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_page:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 209
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    const-string v0, "page_number"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 210
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 211
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$createFavoritesPage$2(Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/CharSequence;
    .locals 1

    .line 214
    sget v0, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_hint:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 215
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "tab_description"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 216
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$createOtherPage$3(Lcom/squareup/util/Res;Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/catalogapi/CatalogIntegrationController;)Ljava/lang/String;
    .locals 0

    .line 230
    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->getNameId(Lcom/squareup/catalogapi/CatalogIntegrationController;)I

    move-result p1

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$createOtherPage$4(Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/CharSequence;
    .locals 1

    .line 233
    sget v0, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_hint:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 234
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "tab_description"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 235
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public create(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)Lcom/squareup/orderentry/pages/OrderEntryPageList;
    .locals 7

    .line 182
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    .line 183
    :goto_0
    iget-object v1, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 184
    iget-object v1, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 185
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogPage;->getName()Ljava/lang/String;

    move-result-object v3

    .line 187
    sget-object v4, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    iget-object v5, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogPage;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v5, v1, v3}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->createFavoritesPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 191
    iget-object p1, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->widePages:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 192
    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-virtual {p0, v0, v1, v4}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->createOtherPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 195
    :cond_1
    new-instance p1, Lcom/squareup/orderentry/pages/OrderEntryPageList;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->device:Lcom/squareup/util/Device;

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    iget-object v4, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 196
    invoke-virtual {p0, v0, v4, v5}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->createOtherPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object v4

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    iget-object v5, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 197
    invoke-virtual {p0, v0, v5, v6}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->createOtherPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/pages/OrderEntryPageList;-><init>(Lcom/squareup/util/Device;Ljava/util/List;Ljava/util/List;Lcom/squareup/orderentry/pages/OrderEntryPage;Lcom/squareup/orderentry/pages/OrderEntryPage;Lcom/squareup/orderentry/pages/OrderEntryPageList$1;)V

    return-object p1
.end method

.method createFavoritesPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/orderentry/pages/OrderEntryPage;
    .locals 7

    .line 202
    iget-boolean v0, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    if-eqz v0, :cond_2

    .line 206
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$ZZguXUygVhvK7FcJPOQSDrMyL8Q;

    invoke-direct {v0, p4}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$ZZguXUygVhvK7FcJPOQSDrMyL8Q;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$Cm809GQdZ6odRg-Y70DHSPM5AEE;

    invoke-direct {v0, p2, p1}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$Cm809GQdZ6odRg-Y70DHSPM5AEE;-><init>(Lcom/squareup/util/Res;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    :goto_0
    move-object v4, v0

    .line 213
    new-instance v2, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$Ie8t6PeiZw68ojrUioorYflOauU;

    invoke-direct {v2, p2, v4}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$Ie8t6PeiZw68ojrUioorYflOauU;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    .line 219
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    move-object v3, p2

    .line 220
    new-instance p2, Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-object v1, p2

    move-object v5, p3

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/orderentry/pages/OrderEntryPage;-><init>(Ljavax/inject/Provider;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljavax/inject/Provider;Ljava/lang/String;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    return-object p2

    .line 203
    :cond_2
    new-instance p2, Ljava/lang/UnsupportedOperationException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not a favorites page"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method createOtherPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/pages/OrderEntryPage;
    .locals 7

    .line 225
    iget-boolean v0, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    if-nez v0, :cond_0

    .line 229
    new-instance v4, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;

    invoke-direct {v4, p2, p1, p3}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;-><init>(Lcom/squareup/util/Res;Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    .line 232
    new-instance v2, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$zXAQLhnl7M4YdT6nnxeEOeGxprs;

    invoke-direct {v2, p2, v4}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$zXAQLhnl7M4YdT6nnxeEOeGxprs;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    .line 237
    new-instance p2, Lcom/squareup/orderentry/pages/OrderEntryPage;

    iget-object v3, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x0

    move-object v1, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/orderentry/pages/OrderEntryPage;-><init>(Ljavax/inject/Provider;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljavax/inject/Provider;Ljava/lang/String;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    return-object p2

    .line 226
    :cond_0
    new-instance p2, Ljava/lang/UnsupportedOperationException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is a favorites page"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
