.class public Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;
.super Ljava/lang/Object;
.source "OrderEntryPages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/pages/OrderEntryPages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PageListCache"
.end annotation


# instance fields
.field final favsPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation
.end field

.field final widePages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ">;)V"
        }
    .end annotation

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "favsPages"

    .line 101
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string/jumbo v0, "widePages"

    .line 102
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    .line 104
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->widePages:Ljava/util/List;

    return-void
.end method

.method static empty(Lcom/squareup/util/Device;)Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;
    .locals 2

    .line 94
    invoke-interface {p0}, Lcom/squareup/util/Device;->isLongTablet()Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL_ITEMS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 95
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 96
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    .line 97
    :goto_0
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 114
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 116
    :cond_1
    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    .line 117
    iget-object v2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->widePages:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->widePages:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 121
    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->widePages:Ljava/util/List;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method isEmpty()Z
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method
