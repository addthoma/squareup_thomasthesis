.class public Lcom/squareup/orderentry/pages/OrderEntryPages;
.super Ljava/lang/Object;
.source "OrderEntryPages.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;,
        Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;
    }
.end annotation


# static fields
.field private static final CATALOG_PAGE_COMPARATOR:Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;

.field private static final INTERESTING_TYPES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field private static final INTERESTING_TYPES_LONG:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field private static final LAST_HOME_PAGE:Ljava/lang/String; = "last-home-page"

.field public static final MAX_FAVS:I = 0x7


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final currentPage:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final pageList:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList;",
            ">;"
        }
    .end annotation
.end field

.field private pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

.field private final pageListCacheKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;"
        }
    .end annotation
.end field

.field private final pageListFactory:Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final shouldPreLoadOrderEntryPages:Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 127
    new-instance v0, Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;-><init>(Lcom/squareup/orderentry/pages/OrderEntryPages$1;)V

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPages;->CATALOG_PAGE_COMPARATOR:Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;

    const/4 v0, 0x2

    new-array v1, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 129
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sput-object v1, Lcom/squareup/orderentry/pages/OrderEntryPages;->INTERESTING_TYPES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 134
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v1, v0

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/squareup/orderentry/pages/OrderEntryPages;->INTERESTING_TYPES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 137
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 138
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    sput-object v0, Lcom/squareup/orderentry/pages/OrderEntryPages;->INTERESTING_TYPES_LONG:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;Lcom/squareup/BundleKey;Landroid/content/SharedPreferences;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 171
    new-instance v4, Lcom/squareup/settings/EnumLocalSetting;

    const-class v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const-string v1, "last-home-page"

    move-object/from16 v2, p7

    invoke-direct {v4, v2, v1, v0}, Lcom/squareup/settings/EnumLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v12}, Lcom/squareup/orderentry/pages/OrderEntryPages;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/util/Device;Lcom/squareup/settings/LocalSetting;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/util/Device;Lcom/squareup/settings/LocalSetting;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ">;",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;",
            ")V"
        }
    .end annotation

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    invoke-static {p3}, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->empty(Lcom/squareup/util/Device;)Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    .line 187
    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->cogsProvider:Ljavax/inject/Provider;

    .line 188
    iput-object p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->device:Lcom/squareup/util/Device;

    .line 189
    iput-object p5, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 190
    iput-object p6, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListFactory:Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;

    .line 191
    iget-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-virtual {p6, p2}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->create(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)Lcom/squareup/orderentry/pages/OrderEntryPageList;

    move-result-object p2

    invoke-static {p2}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageList:Lrx/subjects/BehaviorSubject;

    .line 192
    iput-object p7, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCacheKey:Lcom/squareup/BundleKey;

    .line 193
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 194
    iput-object p8, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 195
    iput-object p9, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 196
    iput-object p10, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 197
    iput-object p11, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->analytics:Lcom/squareup/analytics/Analytics;

    .line 198
    iput-object p12, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->shouldPreLoadOrderEntryPages:Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;

    const/4 p2, 0x0

    .line 200
    invoke-interface {p4, p2}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_0

    .line 202
    invoke-interface {p3}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->defaultIndex(Lcom/squareup/settings/server/AccountStatusSettings;Z)Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object p1

    .line 201
    invoke-interface {p4, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 204
    :cond_0
    invoke-interface {p4}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->currentPage:Lrx/subjects/BehaviorSubject;

    .line 205
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->currentPage:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p2, Lcom/squareup/orderentry/pages/-$$Lambda$Z2JG2giWwNIMX7Yo9-heTNfJeCs;

    invoke-direct {p2, p4}, Lcom/squareup/orderentry/pages/-$$Lambda$Z2JG2giWwNIMX7Yo9-heTNfJeCs;-><init>(Lcom/squareup/settings/LocalSetting;)V

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/pages/OrderEntryPages;)Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->doPageCacheUpdate(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->onPagesUpdated(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    return-void
.end method

.method private deleteEmptyPages(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Z)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation

    .line 334
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 336
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 339
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x1

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 340
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPage;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1, v9}, Lcom/squareup/shared/catalog/Catalog$Local;->findPageTiles(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/PageTiles;

    move-result-object v9

    .line 341
    iget-object v10, v9, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_0

    if-nez p3, :cond_0

    .line 342
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 344
    :cond_0
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogPage;->getPageIndex()I

    move-result v10

    if-eq v10, v5, :cond_1

    .line 345
    invoke-virtual {v7, v5}, Lcom/squareup/shared/catalog/models/CatalogPage;->copyWithIndex(I)Lcom/squareup/shared/catalog/models/CatalogPage;

    move-result-object v7

    .line 346
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_1
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_2

    .line 352
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v8

    if-ne v5, v7, :cond_2

    iget-object v7, v9, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    .line 353
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 354
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    const/4 v9, 0x7

    if-gt v7, v9, :cond_2

    const/4 v6, 0x1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 365
    :cond_3
    iget-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result p2

    if-nez p2, :cond_5

    iget-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object p3, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    .line 366
    invoke-interface {p2, p3}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result p2

    if-eqz p2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    :cond_5
    :goto_1
    if-nez v8, :cond_6

    return-object v0

    .line 373
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    if-nez p2, :cond_9

    .line 374
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p2

    if-lez p2, :cond_8

    .line 375
    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 376
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogPage;->getPageIndex()I

    move-result p3

    if-eqz p3, :cond_7

    .line 377
    invoke-virtual {p2, v4}, Lcom/squareup/shared/catalog/models/CatalogPage;->copyWithIndex(I)Lcom/squareup/shared/catalog/models/CatalogPage;

    move-result-object p2

    .line 378
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 380
    :cond_7
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 382
    :cond_8
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object p2

    .line 383
    new-instance p3, Lcom/squareup/api/items/Page$Builder;

    invoke-direct {p3}, Lcom/squareup/api/items/Page$Builder;-><init>()V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/squareup/api/items/Page$Builder;->page_index(Ljava/lang/Integer;)Lcom/squareup/api/items/Page$Builder;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/squareup/api/items/Page$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/api/items/Page$Builder;->build()Lcom/squareup/api/items/Page;

    move-result-object p3

    .line 384
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v3, p2, p3}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 385
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    if-eqz v6, :cond_a

    .line 389
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object p2

    .line 390
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p3

    .line 391
    new-instance v3, Lcom/squareup/api/items/Page$Builder;

    invoke-direct {v3}, Lcom/squareup/api/items/Page$Builder;-><init>()V

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {v3, p3}, Lcom/squareup/api/items/Page$Builder;->page_index(Ljava/lang/Integer;)Lcom/squareup/api/items/Page$Builder;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/squareup/api/items/Page$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/api/items/Page$Builder;->build()Lcom/squareup/api/items/Page;

    move-result-object p3

    .line 392
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v3, p2, p3}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogPage;

    .line 393
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    :cond_a
    :goto_2
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->canWrite()Z

    move-result p2

    if-eqz p2, :cond_b

    .line 398
    invoke-interface {p1, v1, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    :cond_b
    return-object v0
.end method

.method private doPageCacheUpdate(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V
    .locals 1

    .line 410
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 414
    :cond_0
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    .line 415
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListFactory:Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->create(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)Lcom/squareup/orderentry/pages/OrderEntryPageList;

    move-result-object p1

    .line 416
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageList:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private onPagesUpdated(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V
    .locals 4

    .line 420
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 422
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->currentPage:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 425
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne v0, v1, :cond_0

    goto :goto_2

    .line 429
    :cond_0
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 430
    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    iget-object v3, v3, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 431
    iget-object v2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    iget-object v2, v2, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogPage;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPage;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 432
    :goto_0
    iget-object v3, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 433
    iget-object v3, p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->favsPages:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogPage;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogPage;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 434
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 438
    :goto_1
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->doPageCacheUpdate(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_3

    .line 426
    :cond_4
    :goto_2
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->doPageCacheUpdate(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    .line 445
    :goto_3
    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    :cond_5
    return-void
.end method

.method private sortPages(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation

    .line 404
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 405
    sget-object p1, Lcom/squareup/orderentry/pages/OrderEntryPages;->CATALOG_PAGE_COMPARATOR:Lcom/squareup/orderentry/pages/OrderEntryPages$CatalogPageComparator;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method public getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->currentPage:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 209
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method interestingCatalogTypes()[Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 329
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLongTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPages;->INTERESTING_TYPES_LONG:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPages;->INTERESTING_TYPES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    :goto_0
    return-object v0
.end method

.method public synthetic lambda$loadAndPost$0$OrderEntryPages(ZLcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;
    .locals 3

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 287
    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isLongTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL_ITEMS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->CATEGORIES:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->DISCOUNTS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->GIFT_CARDS:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_0
    invoke-interface {p2}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllPages()Ljava/util/List;

    move-result-object v1

    .line 300
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->sortPages(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 302
    invoke-direct {p0, p2, v1, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->deleteEmptyPages(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    .line 306
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 v1, 0x7

    if-le p2, v1, :cond_1

    .line 307
    new-instance p2, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-interface {p1, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p1, p2

    .line 310
    :cond_1
    new-instance p2, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-direct {p2, p1, v0}, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object p2
.end method

.method public loadAndPost(Ljava/lang/Runnable;Z)V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->shouldPreLoadOrderEntryPages:Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;

    invoke-interface {v0}, Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;->value()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPages$nEp7kCqL2VDtPTURsTyN6U9JFaA;

    invoke-direct {v1, p0, p2}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPages$nEp7kCqL2VDtPTURsTyN6U9JFaA;-><init>(Lcom/squareup/orderentry/pages/OrderEntryPages;Z)V

    new-instance p2, Lcom/squareup/orderentry/pages/OrderEntryPages$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages$1;-><init>(Lcom/squareup/orderentry/pages/OrderEntryPages;Ljava/lang/Runnable;)V

    .line 284
    invoke-interface {v0, v1, p2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 279
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_2
    return-void
.end method

.method public observe()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList;",
            ">;"
        }
    .end annotation

    .line 235
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageList:Lrx/subjects/BehaviorSubject;

    return-object v0
.end method

.method public observeCurrentPage()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ">;"
        }
    .end annotation

    .line 242
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->currentPage:Lrx/subjects/BehaviorSubject;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 213
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCacheKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    if-eqz p1, :cond_0

    .line 216
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->onPagesUpdated(Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 222
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCacheKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->pageListCache:Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 1

    .line 251
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 252
    iget-boolean v0, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isInLibrary:Z

    if-eqz v0, :cond_1

    .line 257
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eq p1, v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-static {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->tabToLibraryFilter(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListStateManager;->setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    .line 261
    :cond_0
    sget-object p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->currentPage:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 264
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPages$2;->$SwitchMap$com$squareup$orderentry$pages$OrderEntryPageKey:[I

    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    goto :goto_0

    .line 269
    :cond_2
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CHECKOUT_KEYPAD_VIEW:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_0

    .line 266
    :cond_3
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CHECKOUT_LIBRARY_VIEW:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    :goto_0
    return-void
.end method
