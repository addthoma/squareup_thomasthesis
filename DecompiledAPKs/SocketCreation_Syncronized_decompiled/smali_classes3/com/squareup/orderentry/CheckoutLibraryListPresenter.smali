.class public Lcom/squareup/orderentry/CheckoutLibraryListPresenter;
.super Lcom/squareup/librarylist/SimpleLibraryListPresenter;
.source "CheckoutLibraryListPresenter.java"


# instance fields
.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/badbus/BadBus;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Device;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/orderentry/OrderEntryScreenState;Ljavax/inject/Provider;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/util/Clock;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/orderentry/CheckoutLibraryListAssistant;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
            "Lcom/squareup/shared/i18n/Localizer;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v17, p2

    move-object/from16 v7, p3

    move-object/from16 v13, p4

    move-object/from16 v18, p5

    move-object/from16 v3, p6

    move-object/from16 v2, p8

    move-object/from16 v11, p9

    move-object/from16 v4, p10

    move-object/from16 v5, p11

    move-object/from16 v6, p12

    move-object/from16 v1, p13

    move-object/from16 v12, p14

    move-object/from16 v9, p15

    move-object/from16 v10, p16

    move-object/from16 v14, p17

    move-object/from16 v15, p18

    move-object/from16 v16, p19

    move-object/from16 v19, p20

    move-object/from16 v20, p21

    move-object/from16 v21, p22

    move-object/from16 v22, p23

    move-object/from16 v23, p24

    .line 68
    invoke-direct/range {v0 .. v23}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;-><init>(Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/librarylist/LibraryListStateManager;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/util/Clock;Lcom/squareup/badbus/BadBus;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    move-object/from16 v1, p7

    .line 73
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object/from16 v1, p15

    .line 74
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private onCartChanged()V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->notifyDataSetChanged()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$CheckoutLibraryListPresenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    invoke-direct {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->onCartChanged()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$CheckoutLibraryListPresenter(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 86
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->hasView()Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$2$CheckoutLibraryListPresenter(Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CheckoutLibraryListView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/CheckoutLibraryListView;->setListEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$3$CheckoutLibraryListPresenter(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 92
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->hasView()Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$4$CheckoutLibraryListPresenter(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListView;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListView;->resetListPosition()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$5$CheckoutLibraryListPresenter(Lkotlin/Unit;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->hasCursor()Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$6$CheckoutLibraryListPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->updateList()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 78
    invoke-super {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$QLf0eLdHkYDjieiiDdoAqKJ9SFw;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$QLf0eLdHkYDjieiiDdoAqKJ9SFw;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 83
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionModeIsAnimating()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$YJTskQCelt9rpe_vOKd6_Clhm4g;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$YJTskQCelt9rpe_vOKd6_Clhm4g;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$lGDe56OeS1PooL8CqoxuGbY1GRQ;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$lGDe56OeS1PooL8CqoxuGbY1GRQ;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 82
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 91
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->librarySearchIsActive()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$-Ub7HCsIiAYXdn3k-PZUAK6g-xU;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$-Ub7HCsIiAYXdn3k-PZUAK6g-xU;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    .line 92
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$MUne6pcK4eEk7Nh7GC0_mtwe8dA;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$MUne6pcK4eEk7Nh7GC0_mtwe8dA;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 90
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 96
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$N31UNoLD5F59kP2gfOtVAzcZCMw;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$N31UNoLD5F59kP2gfOtVAzcZCMw;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$Pj87rcZOyZeP3jVf6EHylPZbvs0;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListPresenter$Pj87rcZOyZeP3jVf6EHylPZbvs0;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)V

    .line 98
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 95
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 102
    invoke-super {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/CheckoutLibraryListView;

    .line 105
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 107
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "Set LibraryList enabled on load: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/CheckoutLibraryListView;->setListEnabled(Z)V

    .line 109
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;->adapter:Lcom/squareup/librarylist/CheckoutLibraryListAdapter;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/CheckoutLibraryListView;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
