.class public Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;
.super Ljava/lang/Object;
.source "OrderEntryScreenBackHandler.java"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryScreenBackHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenBackHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Tablet"
.end annotation


# instance fields
.field private final cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

.field private final libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CartDropDownPresenter;Lcom/squareup/librarylist/LibraryListStateManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 64
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    .line 65
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 3

    .line 69
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->isShowing()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/CartDropDownPresenter;->hideCart(Z)V

    return v1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->goBack()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v2, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v2, :cond_3

    .line 79
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating()Z

    move-result v0

    if-nez v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->endEditing()V

    :cond_2
    return v1

    :cond_3
    const/4 v0, 0x0

    return v0
.end method
