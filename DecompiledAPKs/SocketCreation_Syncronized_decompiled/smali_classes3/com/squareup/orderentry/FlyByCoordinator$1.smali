.class Lcom/squareup/orderentry/FlyByCoordinator$1;
.super Ljava/lang/Object;
.source "FlyByCoordinator.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/FlyByCoordinator;->buildFlyByAnimator(Landroid/widget/TextView;ZZILcom/squareup/orderentry/FlyByCoordinator$Offsets;Landroid/widget/ImageView;Landroid/widget/ImageView;Lcom/squareup/orderentry/FlyByListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/FlyByCoordinator;

.field final synthetic val$calculatedDuration:I

.field final synthetic val$destinationView:Landroid/widget/TextView;

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$listener:Lcom/squareup/orderentry/FlyByListener;

.field final synthetic val$newQuantity:I

.field final synthetic val$pulseDestination:Z

.field final synthetic val$shadowView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/FlyByCoordinator;ZLandroid/widget/TextView;IILcom/squareup/orderentry/FlyByListener;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    iput-boolean p2, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$pulseDestination:Z

    iput-object p3, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$destinationView:Landroid/widget/TextView;

    iput p4, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$newQuantity:I

    iput p5, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$calculatedDuration:I

    iput-object p6, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$listener:Lcom/squareup/orderentry/FlyByListener;

    iput-object p7, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$imageView:Landroid/widget/ImageView;

    iput-object p8, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$shadowView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .line 284
    new-instance p1, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;

    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$imageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$shadowView:Landroid/widget/ImageView;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/orderentry/FlyByCoordinator$RemoveFlyByViewAndShadow;-><init>(Lcom/squareup/orderentry/FlyByCoordinator;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 286
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$200(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$100(Lcom/squareup/orderentry/FlyByCoordinator;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4

    .line 272
    iget-boolean p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$pulseDestination:Z

    if-eqz p1, :cond_0

    .line 275
    new-instance p1, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;

    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    iget-object v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$destinationView:Landroid/widget/TextView;

    iget v2, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$newQuantity:I

    const/4 v3, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;-><init>(Lcom/squareup/orderentry/FlyByCoordinator;Landroid/widget/TextView;ILcom/squareup/orderentry/FlyByCoordinator$1;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$400(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$100(Lcom/squareup/orderentry/FlyByCoordinator;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$calculatedDuration:I

    add-int/lit8 v1, v1, -0x55

    int-to-long v1, v1

    invoke-virtual {v0, p1, v1, v2}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 279
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$1;->val$listener:Lcom/squareup/orderentry/FlyByListener;

    invoke-interface {p1}, Lcom/squareup/orderentry/FlyByListener;->onStart()V

    return-void
.end method
