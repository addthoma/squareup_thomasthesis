.class public abstract Lcom/squareup/orderentry/OrderEntryScreen$Module;
.super Ljava/lang/Object;
.source "OrderEntryScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindEntryHandler(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/librarylist/LibraryListEntryHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindLibraryListPresenter(Lcom/squareup/orderentry/CheckoutLibraryListPresenter;)Lcom/squareup/librarylist/LibraryListPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideFlyBySource(Lcom/squareup/orderentry/OrderEntryScreen$Presenter;)Lcom/squareup/orderentry/FlyBySource;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
