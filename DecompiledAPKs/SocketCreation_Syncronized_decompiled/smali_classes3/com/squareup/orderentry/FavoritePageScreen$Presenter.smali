.class Lcom/squareup/orderentry/FavoritePageScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "FavoritePageScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritePageScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/FavoritePageView;",
        ">;"
    }
.end annotation


# static fields
.field private static final FAVORITES_GRID_TILE_TAG:Ljava/lang/String; = "FAVORITE GRID TILE: {x},{y},{page_id}"


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private columnCount:I

.field private final entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

.field private final favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

.field private final flow:Lflow/Flow;

.field private final giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

.field private final itemSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private model:Lcom/squareup/shared/catalog/models/PageTiles;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

.field private final redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

.field private final res:Lcom/squareup/util/Res;

.field private rowCount:I

.field private screen:Lcom/squareup/orderentry/FavoritePageScreen;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final tagDiscountFormatter:Lcom/squareup/ui/cart/TagDiscountFormatter;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/cart/TagDiscountFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            "Lcom/squareup/badbus/BadBus;",
            "Lflow/Flow;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/cart/TagDiscountFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 146
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 147
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 148
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    move-object v1, p3

    .line 149
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p4

    .line 150
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    move-object v1, p5

    .line 151
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-object v1, p6

    .line 152
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object v1, p7

    .line 153
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->tagDiscountFormatter:Lcom/squareup/ui/cart/TagDiscountFormatter;

    move-object v1, p8

    .line 154
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p9

    .line 155
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p10

    .line 156
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    move-object v1, p11

    .line 157
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p12

    .line 158
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->itemSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-object v1, p13

    .line 159
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p14

    .line 160
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    move-object/from16 v1, p15

    .line 161
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    move-object/from16 v1, p16

    .line 162
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v1, p17

    .line 163
    iput-object v1, v0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;)Lcom/squareup/orderentry/OrderEntryScreenState;
    .locals 0

    .line 111
    iget-object p0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    return-object p0
.end method

.method private static buildGridTileTag(IILjava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "FAVORITE GRID TILE: {x},{y},{page_id}"

    .line 537
    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "x"

    .line 538
    invoke-virtual {v0, v1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string/jumbo v0, "y"

    .line 539
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "page_id"

    .line 540
    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 541
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 542
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private loadTiles()Lcom/squareup/shared/catalog/CatalogTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "Lcom/squareup/orderentry/PlaceholderCounts;",
            ">;"
        }
    .end annotation

    .line 259
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$UfHhyr6Gii_ZYDDCzTepCPtY31M;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$UfHhyr6Gii_ZYDDCzTepCPtY31M;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;)V

    return-object v0
.end method

.method private loadTilesInBackground()V
    .locals 3

    .line 174
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->loadTiles()Lcom/squareup/shared/catalog/CatalogTask;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->onTilesLoaded()Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private onCartChanged()V
    .locals 1

    .line 193
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/FavoritePageView;

    invoke-direct {p0, v0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->updateEnabledState(Lcom/squareup/orderentry/FavoritePageView;)V

    :cond_0
    return-void
.end method

.method private onTilesLoaded()Lcom/squareup/shared/catalog/CatalogCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/orderentry/PlaceholderCounts;",
            ">;"
        }
    .end annotation

    .line 266
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$nAQwa0C0X613mz2cdARufATyPmI;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$nAQwa0C0X613mz2cdARufATyPmI;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;)V

    return-object v0
.end method

.method private showModelLoaded(Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 12

    .line 315
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->itemSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/FavoritePageView;->setTextTile(Z)V

    .line 317
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->itemSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    .line 318
    iput v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    const/16 v0, 0x9

    .line 319
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->rowCount:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    .line 321
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    .line 322
    iput v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->rowCount:I

    .line 325
    :goto_0
    iget v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    iget v2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->rowCount:I

    mul-int v0, v0, v2

    new-array v0, v0, [Landroid/view/View;

    .line 327
    iget-object v2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v2, v2, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/Tile;

    .line 328
    iget-object v5, v3, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 329
    iget-object v6, v3, Lcom/squareup/shared/catalog/models/Tile;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 331
    iget v7, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {v5, v7}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getRowIndex(I)I

    move-result v7

    .line 332
    iget v8, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {v5, v8}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getColumnIndex(I)I

    move-result v8

    if-ltz v8, :cond_1

    .line 333
    iget v9, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    if-ge v8, v9, :cond_1

    if-ltz v7, :cond_1

    iget v9, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->rowCount:I

    if-lt v7, v9, :cond_2

    goto :goto_1

    .line 340
    :cond_2
    sget-object v9, Lcom/squareup/orderentry/FavoritePageScreen$1;->$SwitchMap$com$squareup$api$items$Type:[I

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoObjectType()Lcom/squareup/api/items/Type;

    move-result-object v10

    invoke-virtual {v10}, Lcom/squareup/api/items/Type;->ordinal()I

    move-result v10

    aget v9, v9, v10

    const/4 v10, 0x1

    if-eq v9, v10, :cond_d

    const/4 v11, 0x2

    if-eq v9, v11, :cond_a

    if-eq v9, v1, :cond_9

    const/4 v3, 0x4

    if-eq v9, v3, :cond_3

    .line 393
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->createUnknownView()Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    .line 368
    :cond_3
    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;

    .line 369
    sget-object v4, Lcom/squareup/orderentry/FavoritePageScreen$1;->$SwitchMap$com$squareup$api$items$Placeholder$PlaceholderType:[I

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;->getPlaceholderType()Lcom/squareup/api/items/Placeholder$PlaceholderType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    if-eq v4, v10, :cond_8

    if-eq v4, v11, :cond_7

    if-eq v4, v1, :cond_6

    if-eq v4, v3, :cond_4

    .line 389
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->createUnknownView()Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    .line 382
    :cond_4
    iget-object v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->canUseRewards()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 383
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->createRewardsSearchView()Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    .line 385
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->createUnknownView()Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    .line 378
    :cond_6
    iget-object v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    sget-object v4, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 379
    invoke-virtual {v3, v4}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object v3

    .line 378
    invoke-virtual {p1, v3}, Lcom/squareup/orderentry/FavoritePageView;->createAllGiftCardsView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_3

    .line 374
    :cond_7
    iget-object v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    sget-object v4, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 375
    invoke-virtual {v3, v4}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object v3

    .line 374
    invoke-virtual {p1, v3}, Lcom/squareup/orderentry/FavoritePageView;->createAllDiscountsView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    goto :goto_3

    .line 371
    :cond_8
    iget-object v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    sget-object v4, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-virtual {v3, v4}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/squareup/orderentry/FavoritePageView;->createAllItemsView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    goto :goto_3

    .line 364
    :cond_9
    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 365
    invoke-virtual {p1, v6}, Lcom/squareup/orderentry/FavoritePageView;->createMenuCategoryView(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)Landroid/view/View;

    move-result-object v3

    goto :goto_3

    .line 342
    :cond_a
    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 343
    iget-object v9, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v4, v4, [Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v9, v4}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 345
    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 346
    iget-object v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v4, v4, Lcom/squareup/shared/catalog/models/PageTiles;->imagesById:Ljava/util/Map;

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/models/CatalogItem;->getImageId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getUrl()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_b
    const/4 v4, 0x0

    .line 350
    :goto_2
    iget-object v9, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, v3, Lcom/squareup/shared/catalog/models/Tile;->price:Lcom/squareup/protos/common/Money;

    invoke-interface {v9, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 351
    invoke-virtual {p1, v6, v4, v3}, Lcom/squareup/orderentry/FavoritePageView;->createItemView(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    .line 352
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v7, v4}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->buildGridTileTag(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    .line 354
    :cond_c
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->createUnknownView()Landroid/view/View;

    move-result-object v3

    goto :goto_3

    .line 359
    :cond_d
    check-cast v6, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 360
    iget-object v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->tagDiscountFormatter:Lcom/squareup/ui/cart/TagDiscountFormatter;

    invoke-virtual {v3, v6}, Lcom/squareup/ui/cart/TagDiscountFormatter;->format(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1, v6, v3}, Lcom/squareup/orderentry/FavoritePageView;->createDiscountView(Lcom/squareup/shared/catalog/models/CatalogDiscount;Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v3

    .line 395
    :goto_3
    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-static {v8, v7, v4}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->coordinatesToPosition(III)I

    move-result v4

    aput-object v3, v0, v4

    goto/16 :goto_1

    :cond_e
    const/4 v1, 0x0

    .line 397
    :goto_4
    array-length v2, v0

    if-ge v1, v2, :cond_10

    .line 398
    aget-object v2, v0, v1

    if-nez v2, :cond_f

    .line 399
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->createEmptyView()Lcom/squareup/orderentry/EmptyTile;

    move-result-object v2

    aput-object v2, v0, v1

    .line 400
    aget-object v2, v0, v1

    sget v3, Lcom/squareup/orderentryappletapi/R$id;->favorite_grid_empty_tile:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 404
    :cond_10
    iget v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->rowCount:I

    iget v2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {p1, v1, v2, v0}, Lcom/squareup/orderentry/FavoritePageView;->setViews(II[Landroid/view/View;)V

    .line 405
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->updateEnabledState(Lcom/squareup/orderentry/FavoritePageView;)V

    .line 406
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    invoke-direct {p0, p1, v0, v4}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->updateMode(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V

    .line 407
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Favorites Grid Tiles Loaded"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 408
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->requestLayoutsForTutorial()V

    return-void
.end method

.method private updateEnabledState(Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 6

    .line 293
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    if-nez v0, :cond_0

    return-void

    .line 298
    :cond_0
    iget-object v0, v0, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/Tile;

    .line 299
    iget-object v2, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 300
    iget-object v1, v1, Lcom/squareup/shared/catalog/models/Tile;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 302
    iget v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getRowIndex(I)I

    move-result v3

    .line 303
    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {v2, v4}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getColumnIndex(I)I

    move-result v2

    if-ltz v2, :cond_1

    .line 304
    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    if-ge v2, v4, :cond_1

    if-ltz v3, :cond_1

    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->rowCount:I

    if-lt v3, v4, :cond_2

    goto :goto_0

    .line 309
    :cond_2
    iget-object v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v5

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z

    move-result v1

    .line 310
    invoke-virtual {p1, v2, v3, v1}, Lcom/squareup/orderentry/FavoritePageView;->updateChildEnabled(IIZ)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private updateMode(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V
    .locals 1

    .line 527
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p2, v0, :cond_0

    .line 528
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageView;->showEditMode()V

    goto :goto_0

    .line 530
    :cond_0
    invoke-virtual {p1, p3}, Lcom/squareup/orderentry/FavoritePageView;->showSaleMode(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method canDragTiles()Z
    .locals 2

    .line 462
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method canGoToEditMode()Z
    .locals 1

    .line 442
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    return v0
.end method

.method endItemDrag()V
    .locals 2

    .line 454
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Favorites Grid Drag and Drop Item"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method inSaleMode()Z
    .locals 2

    .line 458
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$loadTiles$1$FavoritePageScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/orderentry/PlaceholderCounts;
    .locals 3

    .line 260
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->screen:Lcom/squareup/orderentry/FavoritePageScreen;

    invoke-static {v0}, Lcom/squareup/orderentry/FavoritePageScreen;->access$000(Lcom/squareup/orderentry/FavoritePageScreen;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findPageTiles(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/PageTiles;

    move-result-object v0

    .line 261
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1, p1, v2, v0}, Lcom/squareup/orderentry/PlaceholderCounts;->from(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/models/PageTiles;)Lcom/squareup/orderentry/PlaceholderCounts;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$3$FavoritePageScreen$Presenter(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 286
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 287
    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->screen:Lcom/squareup/orderentry/FavoritePageScreen;

    invoke-static {v1}, Lcom/squareup/orderentry/FavoritePageScreen;->access$200(Lcom/squareup/orderentry/FavoritePageScreen;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 286
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->updateMode(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Z)V

    .line 288
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->updateEnabledState(Lcom/squareup/orderentry/FavoritePageView;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$FavoritePageScreen$Presenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 168
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->onCartChanged()V

    return-void
.end method

.method public synthetic lambda$onLoad$4$FavoritePageScreen$Presenter(Lcom/squareup/orderentry/FavoritePageView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 284
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$8ocGro2-VEfC4wcMxb1lgdEVlds;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$8ocGro2-VEfC4wcMxb1lgdEVlds;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;Lcom/squareup/orderentry/FavoritePageView;)V

    .line 285
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onTilesLoaded$2$FavoritePageScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 267
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PlaceholderCounts;

    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    .line 268
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PlaceholderCounts;->getPageTiles()Lcom/squareup/shared/catalog/models/PageTiles;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    .line 269
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/FavoritePageView;

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->showModelLoaded(Lcom/squareup/orderentry/FavoritePageView;)V

    :cond_0
    return-void
.end method

.method moveTile(IIII)V
    .locals 5

    .line 504
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/Tile;

    .line 505
    iget-object v3, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getColumnIndex(I)I

    move-result v3

    if-ne v3, p1, :cond_0

    iget-object v3, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    .line 506
    invoke-virtual {v3, v4}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getRowIndex(I)I

    move-result v3

    if-ne v3, p2, :cond_0

    .line 507
    iget-object v0, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    goto :goto_0

    :cond_1
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 516
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->object()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/ItemPageMembership;

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemPageMembership;->newBuilder()Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p1

    .line 517
    invoke-virtual {p1, v2}, Lcom/squareup/api/items/ItemPageMembership$Builder;->row(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p1

    .line 518
    invoke-virtual {p1, v2}, Lcom/squareup/api/items/ItemPageMembership$Builder;->column(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p1

    iget p2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    .line 519
    invoke-static {p3, p4, p2}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->coordinatesToPosition(III)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/ItemPageMembership$Builder;->position(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p1

    .line 520
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->build()Lcom/squareup/api/items/ItemPageMembership;

    move-result-object p1

    .line 516
    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->copy(Lcom/squareup/api/items/ItemPageMembership;)Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    move-result-object p1

    .line 522
    iget-object p2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cogs/Cogs;

    .line 523
    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object p3

    invoke-virtual {p3, p1}, Lcom/squareup/cogs/WriteBuilder;->update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object p1

    invoke-static {p2}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object p3

    invoke-interface {p2, p1, p3}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 512
    :cond_2
    new-instance p3, Ljava/lang/IllegalStateException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No tile to move at x:"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", y"

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p3
.end method

.method onAllDiscountsClicked()V
    .locals 2

    .line 424
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/orderentry/category/ItemListScreen;->allDiscounts()Lcom/squareup/orderentry/category/ItemListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onAllGiftCardsClicked()V
    .locals 2

    .line 430
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    invoke-interface {v1}, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;->activationScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onAllItemsClicked()V
    .locals 2

    .line 412
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/orderentry/category/ItemListScreen;->allItems()Lcom/squareup/orderentry/category/ItemListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onCategoryClicked(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V
    .locals 2

    .line 418
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/orderentry/category/ItemListScreen;->category(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/orderentry/category/ItemListScreen;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 6

    .line 199
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    if-nez v0, :cond_0

    return-void

    .line 204
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    :goto_0
    const/4 p1, 0x1

    goto/16 :goto_2

    :cond_1
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 206
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v3, v0, v1

    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v3, v0, v2

    const/4 v3, 0x2

    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v4, v0, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v4, v0, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v4, v0, v3

    const/4 v3, 0x5

    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v4, v0, v3

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 210
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->getUpdated()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 211
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->getDeleted()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 214
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 215
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v3

    .line 216
    sget-object v4, Lcom/squareup/orderentry/FavoritePageScreen$1;->$SwitchMap$com$squareup$api$items$Type:[I

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getProtoObjectType()Lcom/squareup/api/items/Type;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/api/items/Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 241
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/PageTiles;->imagesById:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 227
    :pswitch_1
    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 229
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->hasObjectExtension()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getPageId()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->screen:Lcom/squareup/orderentry/FavoritePageScreen;

    .line 230
    invoke-static {v4}, Lcom/squareup/orderentry/FavoritePageScreen;->access$000(Lcom/squareup/orderentry/FavoritePageScreen;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_0

    .line 235
    :cond_3
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/PageTiles;->tileIds:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_0

    .line 221
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/PageTiles;->objectIds:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_0

    :cond_4
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_5

    new-array p1, v2, [Ljava/lang/Object;

    .line 253
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->screen:Lcom/squareup/orderentry/FavoritePageScreen;

    invoke-static {v0}, Lcom/squareup/orderentry/FavoritePageScreen;->access$000(Lcom/squareup/orderentry/FavoritePageScreen;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v1

    const-string v0, "Refreshing page %s"

    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->loadTilesInBackground()V

    :cond_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method onDeleteTile(II)V
    .locals 4

    .line 486
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/Tile;

    .line 487
    iget-object v2, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    iget v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getColumnIndex(I)I

    move-result v2

    if-ne v2, p1, :cond_0

    iget-object v2, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    iget v3, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    .line 488
    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getRowIndex(I)I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 489
    iget-object p1, v1, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_2

    return-void

    .line 498
    :cond_2
    iget-object p2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cogs/Cogs;

    .line 499
    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cogs/WriteBuilder;->delete(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object p1

    invoke-static {p2}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method onDiscountClicked(Landroid/view/View;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_FAVORITE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountClicked(Landroid/view/View;Ljava/lang/String;Z)V

    return-void
.end method

.method onEmptyViewClicked(II)V
    .locals 5

    .line 466
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    .line 470
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/FavoritePageView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritePageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/FavoritePageScreen;

    .line 471
    new-instance v1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;

    .line 472
    invoke-static {v0}, Lcom/squareup/orderentry/FavoritePageScreen;->access$000(Lcom/squareup/orderentry/FavoritePageScreen;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    iget v4, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->columnCount:I

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;-><init>(Ljava/lang/String;Landroid/graphics/Point;I)V

    .line 476
    iget-object v2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    invoke-virtual {v2, v1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->selectTile(Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;)V

    .line 477
    iget-object v2, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->pageId:Ljava/lang/String;

    .line 479
    invoke-static {p1, p2, v1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->buildGridTileTag(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Favorites Grid Empty Tile Selected"

    .line 477
    invoke-interface {v2, v3, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    .line 481
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    invoke-static {v0}, Lcom/squareup/orderentry/FavoritePageScreen;->access$000(Lcom/squareup/orderentry/FavoritePageScreen;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/squareup/orderentry/category/ItemListScreen;->createTile(Ljava/lang/String;II)Lcom/squareup/orderentry/category/ItemListScreen;

    move-result-object p1

    invoke-virtual {v1, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 467
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Should only be able to click in edit mode: x="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", y="

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 167
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/FavoritePageScreen;

    iput-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->screen:Lcom/squareup/orderentry/FavoritePageScreen;

    .line 168
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$WX49h1H7AJ9-7bQ3aGGiVW94DPs;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$WX49h1H7AJ9-7bQ3aGGiVW94DPs;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$KNdmSFuUSwUjh2WzxPb1mfDoGmE;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$KNdmSFuUSwUjh2WzxPb1mfDoGmE;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 170
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->loadTilesInBackground()V

    return-void
.end method

.method onItemClicked(Landroid/view/View;Lcom/squareup/shared/catalog/models/CatalogItem;)V
    .locals 2

    .line 178
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_FAVORITE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 276
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 277
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/FavoritePageView;

    .line 279
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->model:Lcom/squareup/shared/catalog/models/PageTiles;

    if-eqz v0, :cond_0

    .line 280
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->showModelLoaded(Lcom/squareup/orderentry/FavoritePageView;)V

    .line 283
    :cond_0
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$IysIOOJqb2Oevi6w1Rq-N7cpXfU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageScreen$Presenter$IysIOOJqb2Oevi6w1Rq-N7cpXfU;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;Lcom/squareup/orderentry/FavoritePageView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onRewardsSearchCardClicked()V
    .locals 2

    .line 436
    invoke-virtual {p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->inSaleMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    invoke-interface {v1}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method startEditing()V
    .locals 3

    .line 446
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/orderentry/FavoritePageScreen$Presenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter$1;-><init>(Lcom/squareup/orderentry/FavoritePageScreen$Presenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
