.class public Lcom/squareup/orderentry/OrderEntryBadgeView;
.super Lcom/squareup/widgets/MarinBadgeView;
.source "OrderEntryBadgeView.java"


# instance fields
.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/orderentry/OrderEntryBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/MarinBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/OrderEntryBadgeView;)V

    return-void
.end method

.method private isWideTablet()Z
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryBadgeView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public showBadge(Ljava/lang/CharSequence;)V
    .locals 0

    .line 27
    invoke-super {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->showBadge(Ljava/lang/CharSequence;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryBadgeView;->isWideTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 29
    sget p1, Lcom/squareup/orderentry/R$drawable;->order_entry_wide_badge_background:I

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryBadgeView;->setBackgroundResource(I)V

    goto :goto_0

    .line 31
    :cond_0
    sget p1, Lcom/squareup/orderentry/R$drawable;->order_entry_badge_background:I

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryBadgeView;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method

.method public showFatalPriorityBadge()V
    .locals 1

    .line 47
    invoke-super {p0}, Lcom/squareup/widgets/MarinBadgeView;->showFatalPriorityBadge()V

    .line 48
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryBadgeView;->isWideTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    sget v0, Lcom/squareup/orderentry/R$drawable;->order_entry_wide_badge_background_fatal:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryBadgeView;->setBackgroundResource(I)V

    goto :goto_0

    .line 51
    :cond_0
    sget v0, Lcom/squareup/orderentry/R$drawable;->order_entry_badge_background_fatal:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/OrderEntryBadgeView;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method

.method public showHighPriorityBadge(Ljava/lang/CharSequence;)V
    .locals 0

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->showHighPriorityBadge(Ljava/lang/CharSequence;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/orderentry/OrderEntryBadgeView;->isWideTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 39
    sget p1, Lcom/squareup/orderentry/R$drawable;->order_entry_wide_badge_background_red:I

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryBadgeView;->setBackgroundResource(I)V

    goto :goto_0

    .line 41
    :cond_0
    sget p1, Lcom/squareup/orderentry/R$drawable;->order_entry_badge_background_red:I

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryBadgeView;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method
