.class public final Lcom/squareup/orderentry/OrderEntryTutorialEvents;
.super Ljava/lang/Object;
.source "OrderEntryTutorialEvents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008#\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/orderentry/OrderEntryTutorialEvents;",
        "",
        "()V",
        "ADJUST_INVENTORY_SCREEN_DISMISSED",
        "",
        "ADJUST_INVENTORY_SCREEN_SHOWN",
        "ALL_ITEMS_SCREEN_SHOWN",
        "APPLETS_DRAWER_CLOSED",
        "APPLETS_DRAWER_OPENED",
        "EDIT_CATEGORY_SCREEN_DISMISSED",
        "EDIT_CATEGORY_SCREEN_NEW_CATEGORY_BUTTON_SHOWN",
        "EDIT_DISCOUNT_SCREEN_DISMISSED",
        "EDIT_DISCOUNT_SCREEN_SHOWN",
        "EDIT_ITEM_LABEL_SCREEN_DISMISSED",
        "EDIT_ITEM_LABEL_SCREEN_SHOWN",
        "FAVORITES_GRID_DONE_EDITING_TAPPED",
        "FAVORITES_GRID_DRAG_AND_DROP_ITEM",
        "FAVORITES_GRID_EMPTY_TILE_SELECTED",
        "FAVORITES_GRID_TILES_LOADED",
        "FAVORITES_TAB_SELECTED",
        "ITEMS_APPLET_LOADED",
        "ITEMS_APPLET_SECTION_TAPPED",
        "ITEM_CHANGES_DISCARDED",
        "ITEM_CREATE_CLICKED",
        "ITEM_LIST_SCREEN_DISMISSED",
        "ITEM_LIST_SCREEN_SHOWN",
        "ITEM_VARIATION_SCREEN_DISMISSED",
        "ITEM_VARIATION_SCREEN_SHOWN",
        "KEYPAD_DISPLAYED",
        "ON_EDIT_MODE",
        "ON_HOME_READY_TO_ACCEPT_CARD",
        "OPEN_TICKETS_HOME_SHOWN",
        "PRICE_ENTRY_SCREEN_DISMISSED",
        "PRICE_ENTRY_SCREEN_SHOWN",
        "SECTION_LIST_ADD_BUTTON_SHOWN",
        "SECTION_LIST_ADD_BUTTON_TAPPED",
        "SECTION_LIST_SEARCH_ENDED",
        "SECTION_LIST_SEARCH_STARTED",
        "SHOWN",
        "order-entry-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADJUST_INVENTORY_SCREEN_DISMISSED:Ljava/lang/String; = "Adjust Inventory Screen Dismissed"

.field public static final ADJUST_INVENTORY_SCREEN_SHOWN:Ljava/lang/String; = "Adjust Inventory Screen Shown"

.field public static final ALL_ITEMS_SCREEN_SHOWN:Ljava/lang/String; = "All Items Screen Shown"

.field public static final APPLETS_DRAWER_CLOSED:Ljava/lang/String; = "Applets Drawer Closed"

.field public static final APPLETS_DRAWER_OPENED:Ljava/lang/String; = "Applets Drawer Opened"

.field public static final EDIT_CATEGORY_SCREEN_DISMISSED:Ljava/lang/String; = "Edit Category Screen Dismissed"

.field public static final EDIT_CATEGORY_SCREEN_NEW_CATEGORY_BUTTON_SHOWN:Ljava/lang/String; = "Edit Category Screen Shown"

.field public static final EDIT_DISCOUNT_SCREEN_DISMISSED:Ljava/lang/String; = "Edit Discount Dismissed"

.field public static final EDIT_DISCOUNT_SCREEN_SHOWN:Ljava/lang/String; = "Edit Discount Shown"

.field public static final EDIT_ITEM_LABEL_SCREEN_DISMISSED:Ljava/lang/String; = "Edit Item Label Screen Dismissed"

.field public static final EDIT_ITEM_LABEL_SCREEN_SHOWN:Ljava/lang/String; = "Edit Item Label Screen Shown"

.field public static final FAVORITES_GRID_DONE_EDITING_TAPPED:Ljava/lang/String; = "Favorites Grid Done Editing Tapped"

.field public static final FAVORITES_GRID_DRAG_AND_DROP_ITEM:Ljava/lang/String; = "Favorites Grid Drag and Drop Item"

.field public static final FAVORITES_GRID_EMPTY_TILE_SELECTED:Ljava/lang/String; = "Favorites Grid Empty Tile Selected"

.field public static final FAVORITES_GRID_TILES_LOADED:Ljava/lang/String; = "Favorites Grid Tiles Loaded"

.field public static final FAVORITES_TAB_SELECTED:Ljava/lang/String; = "Favorites Tab Selected"

.field public static final INSTANCE:Lcom/squareup/orderentry/OrderEntryTutorialEvents;

.field public static final ITEMS_APPLET_LOADED:Ljava/lang/String; = "Items Applet Loaded"

.field public static final ITEMS_APPLET_SECTION_TAPPED:Ljava/lang/String; = "Items Applet Section Tapped"

.field public static final ITEM_CHANGES_DISCARDED:Ljava/lang/String; = "Item Changes Discarded"

.field public static final ITEM_CREATE_CLICKED:Ljava/lang/String; = "Item Create Clicked"

.field public static final ITEM_LIST_SCREEN_DISMISSED:Ljava/lang/String; = "Item List Screen Dismissed"

.field public static final ITEM_LIST_SCREEN_SHOWN:Ljava/lang/String; = "Item List Screen Shown"

.field public static final ITEM_VARIATION_SCREEN_DISMISSED:Ljava/lang/String; = "Item Variation Screen Dismissed"

.field public static final ITEM_VARIATION_SCREEN_SHOWN:Ljava/lang/String; = "Item Variation Screen Shown"

.field public static final KEYPAD_DISPLAYED:Ljava/lang/String; = "KeyPad Displayed"

.field public static final ON_EDIT_MODE:Ljava/lang/String; = "On home edit mode"

.field public static final ON_HOME_READY_TO_ACCEPT_CARD:Ljava/lang/String; = "On OrderEntryScreen and ready for card"

.field public static final OPEN_TICKETS_HOME_SHOWN:Ljava/lang/String; = "Shown MasterDetailTicketScreen"

.field public static final PRICE_ENTRY_SCREEN_DISMISSED:Ljava/lang/String; = "Price Entry Screen Dismissed"

.field public static final PRICE_ENTRY_SCREEN_SHOWN:Ljava/lang/String; = "Price Entry Screen Shown"

.field public static final SECTION_LIST_ADD_BUTTON_SHOWN:Ljava/lang/String; = "Items Section List Add Button Shown"

.field public static final SECTION_LIST_ADD_BUTTON_TAPPED:Ljava/lang/String; = "Items Section List Add Button Tapped"

.field public static final SECTION_LIST_SEARCH_ENDED:Ljava/lang/String; = "Items Section List Search Ended"

.field public static final SECTION_LIST_SEARCH_STARTED:Ljava/lang/String; = "Items Section List Search Started"

.field public static final SHOWN:Ljava/lang/String; = "Shown OrderEntryScreen"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4
    new-instance v0, Lcom/squareup/orderentry/OrderEntryTutorialEvents;

    invoke-direct {v0}, Lcom/squareup/orderentry/OrderEntryTutorialEvents;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryTutorialEvents;->INSTANCE:Lcom/squareup/orderentry/OrderEntryTutorialEvents;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
