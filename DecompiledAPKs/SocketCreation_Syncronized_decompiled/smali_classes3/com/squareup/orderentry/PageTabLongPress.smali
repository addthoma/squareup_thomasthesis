.class Lcom/squareup/orderentry/PageTabLongPress;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "PageTabLongPress.java"


# instance fields
.field final from_edit_mode:Z

.field final page_already_has_name:Z


# direct methods
.method constructor <init>(ZZ)V
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->EDIT_FAVORITES_PAGE_NAME:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 12
    iput-boolean p1, p0, Lcom/squareup/orderentry/PageTabLongPress;->page_already_has_name:Z

    .line 13
    iput-boolean p2, p0, Lcom/squareup/orderentry/PageTabLongPress;->from_edit_mode:Z

    return-void
.end method
