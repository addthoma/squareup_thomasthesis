.class Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ChargeAndTicketButtonsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->onChargeClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;->this$0:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;->this$0:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->onCancelConfirmClicked()V

    return-void
.end method

.method public success()V
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;->this$0:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->access$000(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    .line 370
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$1;->this$0:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->access$100(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;)V

    return-void
.end method
