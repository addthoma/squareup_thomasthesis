.class final Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;
.super Ljava/lang/Object;
.source "RealInstantDepositRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/instantdeposit/RealInstantDepositRunner;->onCheckEligibility(Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;


# direct methods
.method constructor <init>(Lcom/squareup/instantdeposit/RealInstantDepositRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;->this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;->this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-static {v0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->access$onCanMakeDeposit(Lcom/squareup/instantdeposit/RealInstantDepositRunner;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 135
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;->this$0:Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;->access$onNotEligible(Lcom/squareup/instantdeposit/RealInstantDepositRunner;Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/instantdeposit/RealInstantDepositRunner$onCheckEligibility$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
