.class public final Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;
.super Ljava/lang/Object;
.source "InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/instantdeposit/InstantDepositsService;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)",
            "Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideInstantDepositsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/instantdeposit/InstantDepositsService;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/instantdeposit/InstantDepositsServiceModule;->provideInstantDepositsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/instantdeposit/InstantDepositsService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/instantdeposit/InstantDepositsService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/instantdeposit/InstantDepositsService;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;->provideInstantDepositsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/instantdeposit/InstantDepositsService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/instantdeposit/InstantDepositsServiceModule_ProvideInstantDepositsServiceFactory;->get()Lcom/squareup/instantdeposit/InstantDepositsService;

    move-result-object v0

    return-object v0
.end method
