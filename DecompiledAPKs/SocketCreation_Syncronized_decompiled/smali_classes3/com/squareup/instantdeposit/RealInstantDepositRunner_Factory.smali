.class public final Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;
.super Ljava/lang/Object;
.source "RealInstantDepositRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/instantdeposit/RealInstantDepositRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositsService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositsService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletGateway;",
            ">;)",
            "Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;"
        }
    .end annotation

    .line 50
    new-instance v7, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Landroid/app/Application;Landroid/content/res/Resources;Lcom/squareup/instantdeposit/InstantDepositsService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletGateway;)Lcom/squareup/instantdeposit/RealInstantDepositRunner;
    .locals 8

    .line 56
    new-instance v7, Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/instantdeposit/RealInstantDepositRunner;-><init>(Landroid/app/Application;Landroid/content/res/Resources;Lcom/squareup/instantdeposit/InstantDepositsService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletGateway;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/instantdeposit/RealInstantDepositRunner;
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/instantdeposit/InstantDepositsService;

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/balance/core/server/transfers/TransfersService;

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-static/range {v1 .. v6}, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->newInstance(Landroid/app/Application;Landroid/content/res/Resources;Lcom/squareup/instantdeposit/InstantDepositsService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletGateway;)Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/instantdeposit/RealInstantDepositRunner_Factory;->get()Lcom/squareup/instantdeposit/RealInstantDepositRunner;

    move-result-object v0

    return-object v0
.end method
