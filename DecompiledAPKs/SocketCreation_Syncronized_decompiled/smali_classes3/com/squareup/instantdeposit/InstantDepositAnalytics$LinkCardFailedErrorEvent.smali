.class public final Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;
.super Lcom/squareup/analytics/event/v1/ErrorEvent;
.source "InstantDepositAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LinkCardFailedErrorEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;",
        "Lcom/squareup/analytics/event/v1/ErrorEvent;",
        "title",
        "",
        "message",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "getMessage",
        "()Ljava/lang/String;",
        "getTitle",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTANT_DEPOSIT_LINK_CARD_FAILED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ErrorEvent;-><init>(Lcom/squareup/analytics/RegisterErrorName;)V

    iput-object p1, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$LinkCardFailedErrorEvent;->title:Ljava/lang/String;

    return-object v0
.end method
