.class public final Lcom/squareup/salesreport/print/SalesReportRenderer;
.super Ljava/lang/Object;
.source "SalesReportRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportRenderer.kt\ncom/squareup/salesreport/print/SalesReportRenderer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,131:1\n1642#2,2:132\n1642#2,2:134\n1642#2,2:136\n1642#2,2:138\n1642#2:140\n1642#2,2:141\n1643#2:143\n*E\n*S KotlinDebug\n*F\n+ 1 SalesReportRenderer.kt\ncom/squareup/salesreport/print/SalesReportRenderer\n*L\n62#1,2:132\n77#1,2:134\n94#1,2:136\n107#1,2:138\n120#1:140\n120#1,2:141\n120#1:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0014\u0010\t\u001a\u00020\n*\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0014\u0010\r\u001a\u00020\n*\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0014\u0010\u0010\u001a\u00020\n*\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0014\u0010\u0013\u001a\u00020\n*\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0014\u0010\u0016\u001a\u00020\n*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0017H\u0002J\u0014\u0010\u0018\u001a\u00020\n*\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0014\u0010\u001b\u001a\u00020\n*\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u001dH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/salesreport/print/SalesReportRenderer;",
        "",
        "builder",
        "Lcom/squareup/print/ThermalBitmapBuilder;",
        "(Lcom/squareup/print/ThermalBitmapBuilder;)V",
        "renderBitmap",
        "Landroid/graphics/Bitmap;",
        "payload",
        "Lcom/squareup/salesreport/print/SalesReportPayload;",
        "renderCategorySales",
        "",
        "categorySection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;",
        "renderDiscounts",
        "discountSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;",
        "renderItems",
        "itemsSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;",
        "renderPaymentRows",
        "paymentsSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;",
        "renderReportHeader",
        "Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;",
        "renderSalesSummaryRows",
        "salesSummarySection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;",
        "textWithDividers",
        "title",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final builder:Lcom/squareup/print/ThermalBitmapBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/print/SalesReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method private final renderCategorySales(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;)V
    .locals 2

    .line 104
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;->getSectionRows()Ljava/util/List;

    move-result-object v0

    .line 105
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 106
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;->getHeaderText()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/print/SalesReportRenderer;->textWithDividers(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 107
    check-cast v0, Ljava/lang/Iterable;

    .line 138
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection$CategorySectionRow;->component2()Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 109
    check-cast v1, Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final renderDiscounts(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;)V
    .locals 2

    .line 91
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;->getSectionRows()Ljava/util/List;

    move-result-object v0

    .line 92
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 93
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;->getHeaderText()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/print/SalesReportRenderer;->textWithDividers(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 94
    check-cast v0, Ljava/lang/Iterable;

    .line 136
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection$DiscountSectionRow;->component2()Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 96
    check-cast v1, Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final renderItems(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V
    .locals 5

    .line 117
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;->getSectionRows()Ljava/util/List;

    move-result-object v0

    .line 118
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 119
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;->getHeaderText()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/print/SalesReportRenderer;->textWithDividers(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 120
    check-cast v0, Ljava/lang/Iterable;

    .line 140
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;->component2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;->component3()Ljava/util/List;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 122
    check-cast v1, Ljava/lang/CharSequence;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    if-eqz v0, :cond_0

    .line 123
    check-cast v0, Ljava/lang/Iterable;

    .line 141
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;

    invoke-virtual {v1}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;->component1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection$ItemSectionRow;->component2()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 125
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final renderPaymentRows(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;)V
    .locals 4

    .line 74
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;->getSectionRows()Ljava/util/List;

    move-result-object v0

    .line 75
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 76
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;->getHeaderText()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/print/SalesReportRenderer;->textWithDividers(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 77
    check-cast v0, Ljava/lang/Iterable;

    .line 134
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;->component2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection$PaymentSectionRow;->component3()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v0

    .line 78
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 79
    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    if-ne v0, v3, :cond_0

    .line 80
    invoke-virtual {p1, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingTextMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 82
    :cond_0
    check-cast v1, Ljava/lang/CharSequence;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final renderReportHeader(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;)V
    .locals 1

    .line 31
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getHeaderText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 32
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 33
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getReportDateRangeText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 35
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getReportedOnDateText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getReportedOnDateText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 39
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getSelectedEmployeeText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getSelectedEmployeeText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 43
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getSelectedDeviceText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 44
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;->getSelectedDeviceText()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 46
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method private final renderSalesSummaryRows(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;)V
    .locals 4

    .line 59
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;->getSectionRows()Ljava/util/List;

    move-result-object v0

    .line 60
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 61
    invoke-virtual {p2}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;->getHeaderText()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/print/SalesReportRenderer;->textWithDividers(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 62
    check-cast v0, Ljava/lang/Iterable;

    .line 132
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;->component1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;->component2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection$SalesSectionRow;->component3()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 64
    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    if-ne v0, v3, :cond_0

    .line 65
    invoke-virtual {p1, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingTextMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 67
    :cond_0
    check-cast v1, Ljava/lang/CharSequence;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final textWithDividers(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V
    .locals 0

    .line 50
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 51
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 52
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 53
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 54
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method


# virtual methods
.method public final renderBitmap(Lcom/squareup/salesreport/print/SalesReportPayload;)Landroid/graphics/Bitmap;
    .locals 2

    const-string v0, "payload"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    .line 20
    invoke-virtual {p1}, Lcom/squareup/salesreport/print/SalesReportPayload;->getHeaderSection()Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderReportHeader(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;)V

    .line 21
    invoke-virtual {p1}, Lcom/squareup/salesreport/print/SalesReportPayload;->getSalesSection()Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderSalesSummaryRows(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;)V

    .line 22
    invoke-virtual {p1}, Lcom/squareup/salesreport/print/SalesReportPayload;->getPaymentsSection()Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderPaymentRows(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;)V

    .line 23
    invoke-virtual {p1}, Lcom/squareup/salesreport/print/SalesReportPayload;->getDiscountSection()Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderDiscounts(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;)V

    .line 24
    invoke-virtual {p1}, Lcom/squareup/salesreport/print/SalesReportPayload;->getCategorySection()Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderCategorySales(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;)V

    .line 25
    invoke-virtual {p1}, Lcom/squareup/salesreport/print/SalesReportPayload;->getItemsSection()Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderItems(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V

    .line 27
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v0, "builder\n        .apply {\u2026      }\n        .render()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
