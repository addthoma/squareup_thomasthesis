.class abstract Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$View;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V",
        "Action",
        "Tap",
        "View",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$View;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 0

    .line 313
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 310
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
