.class public abstract Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;
.super Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Tap"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ChangeComparison;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SelectQuickTimerange;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$PrintReport;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$EmailReport;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToCount;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToGrossSalesChart;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToNetSalesChart;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToSalesCountChart;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopFive;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopTen;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToOverview;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToDetails;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideItemsForCategory;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowItemsForCategory;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideVariationsForItem;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowVariationsForItem;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails;,
        Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0016\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001aB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0016\u001b\u001c\u001d\u001e\u001f !\"#$%&\'()*+,-./0\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;",
        "registerTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "(Lcom/squareup/analytics/RegisterTapName;)V",
        "ChangeComparison",
        "CollapseAllDetails",
        "CustomizeReport",
        "EmailReport",
        "ExpandAllDetails",
        "HideItemsForCategory",
        "HideVariationsForItem",
        "PrintReport",
        "SelectQuickTimerange",
        "ShowAll",
        "ShowItemsForCategory",
        "ShowTopFive",
        "ShowTopTen",
        "ShowVariationsForItem",
        "SwitchToAmount",
        "SwitchToCount",
        "SwitchToDetails",
        "SwitchToGrossSalesChart",
        "SwitchToNetSalesChart",
        "SwitchToOverview",
        "SwitchToSalesCountChart",
        "ViewInDashboard",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ChangeComparison;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SelectQuickTimerange;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$PrintReport;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$EmailReport;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToCount;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToGrossSalesChart;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToNetSalesChart;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToSalesCountChart;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopFive;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopTen;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToOverview;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToDetails;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideItemsForCategory;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowItemsForCategory;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideVariationsForItem;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowVariationsForItem;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/analytics/RegisterTapName;)V
    .locals 2

    .line 316
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object p1, p1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    const-string v1, "registerTapName.value"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 315
    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 315
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;-><init>(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
