.class public final Lcom/squareup/salesreport/chart/SalesByHourAdapter;
.super Lcom/squareup/salesreport/chart/SalesAdapter;
.source "SalesAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BL\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012!\u0010\u0008\u001a\u001d\u0012\u0013\u0012\u00110\n\u00a2\u0006\u000c\u0008\u000b\u0012\u0008\u0008\u000c\u0012\u0004\u0008\u0008(\r\u0012\u0004\u0012\u00020\u000e0\t\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/salesreport/chart/SalesByHourAdapter;",
        "Lcom/squareup/salesreport/chart/SalesAdapter;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "data",
        "",
        "Lcom/squareup/salesreport/chart/SalesDataPoint;",
        "rangeLabelFormatter",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "value",
        "",
        "current24HourClockMode",
        "Lcom/squareup/time/Current24HourClockMode;",
        "(Ljavax/inject/Provider;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lcom/squareup/time/Current24HourClockMode;)V",
        "dateTimeFormatter",
        "Lcom/squareup/salesreport/chart/ChartHourFormatter;",
        "domainLabel",
        "step",
        "",
        "rangeLabel",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateTimeFormatter:Lcom/squareup/salesreport/chart/ChartHourFormatter;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lcom/squareup/time/Current24HourClockMode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/chart/SalesDataPoint;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Double;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/time/Current24HourClockMode;",
            ")V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rangeLabelFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "current24HourClockMode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0, p2, p3}, Lcom/squareup/salesreport/chart/SalesAdapter;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    .line 63
    new-instance p2, Lcom/squareup/salesreport/chart/ChartHourFormatter;

    .line 65
    invoke-interface {p4}, Lcom/squareup/time/Current24HourClockMode;->is24HourClock()Lio/reactivex/Observable;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrThrow(Lio/reactivex/Observable;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    .line 63
    invoke-direct {p2, p1, p3}, Lcom/squareup/salesreport/chart/ChartHourFormatter;-><init>(Ljavax/inject/Provider;Z)V

    iput-object p2, p0, Lcom/squareup/salesreport/chart/SalesByHourAdapter;->dateTimeFormatter:Lcom/squareup/salesreport/chart/ChartHourFormatter;

    return-void
.end method


# virtual methods
.method public domainLabel(I)Ljava/lang/String;
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/salesreport/chart/SalesByHourAdapter;->dateTimeFormatter:Lcom/squareup/salesreport/chart/ChartHourFormatter;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/salesreport/chart/SalesByHourAdapter;->getData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {p1}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getDomain()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/threeten/bp/LocalDateTime;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/salesreport/chart/SalesByHourAdapter;->getData()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {v1}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getDomain()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDateTime;->getHour()I

    move-result v1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/salesreport/chart/SalesByHourAdapter;->getData()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {v2}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getDomain()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDateTime;->getHour()I

    move-result v2

    .line 73
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/salesreport/chart/ChartHourFormatter;->format(Lorg/threeten/bp/LocalDateTime;II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public rangeLabel(D)Ljava/lang/String;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/salesreport/chart/SalesByHourAdapter;->getRangeLabelFormatter()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method
