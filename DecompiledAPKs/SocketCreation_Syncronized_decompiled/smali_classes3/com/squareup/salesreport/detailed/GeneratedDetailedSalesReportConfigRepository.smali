.class public final Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;
.super Ljava/lang/Object;
.source "DetailedSalesReportConfigRepository.kt"

# interfaces
.implements Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;",
        "Lcom/squareup/salesreport/detailed/DetailedSalesReportConfigRepository;",
        "defaultReportConfigProvider",
        "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
        "(Lcom/squareup/salesreport/DefaultReportConfigProvider;)V",
        "get",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultReportConfigProvider:Lcom/squareup/salesreport/DefaultReportConfigProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/DefaultReportConfigProvider;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "defaultReportConfigProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;->defaultReportConfigProvider:Lcom/squareup/salesreport/DefaultReportConfigProvider;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/customreport/data/ReportConfig;
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;->defaultReportConfigProvider:Lcom/squareup/salesreport/DefaultReportConfigProvider;

    sget-object v1, Lcom/squareup/api/salesreport/DetailLevel$Detailed;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    check-cast v1, Lcom/squareup/api/salesreport/DetailLevel;

    invoke-interface {v0, v1}, Lcom/squareup/salesreport/DefaultReportConfigProvider;->get(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    return-object v0
.end method
