.class final Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow;->render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/salesreport/export/ExportReportProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    iput-object p3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 62
    invoke-virtual {p0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 86
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {v1}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logEmailReport(Lcom/squareup/customreport/data/ReportConfig;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrepareEmailReport;

    iget-object v2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {v2}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getAccountStatusSettings$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrepareEmailReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/settings/server/AccountStatusSettings;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
