.class final Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;
.super Lkotlin/jvm/internal/Lambda;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow;->render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "includeItems",
        "",
        "deviceName",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/salesreport/export/ExportReportState;

.field final synthetic this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    iput-object p3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->invoke(ZLjava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ZLjava/lang/String;)V
    .locals 4

    .line 153
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    instance-of v0, v0, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getPrinterDispatcher$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getSalesReportPrintPayloadFactory$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    move-result-object v1

    .line 156
    iget-object v2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v2, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    invoke-virtual {v2}, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v2

    .line 157
    iget-object v3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v3, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    invoke-virtual {v3}, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    .line 155
    invoke-virtual {v1, v2, v3, p1, p2}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;->buildPayload(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;ZLjava/lang/String;)Lcom/squareup/salesreport/print/SalesReportPayload;

    move-result-object p2

    .line 154
    invoke-interface {v0, p2}, Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;->print(Lcom/squareup/salesreport/print/SalesReportPayload;)V

    .line 162
    iget-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {p2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v0, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    invoke-virtual {v0}, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-interface {p2, v0, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logReportPrinted(Lcom/squareup/customreport/data/ReportConfig;Z)V

    .line 163
    iget-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$sink:Lcom/squareup/workflow/Sink;

    new-instance p2, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrintReport;

    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;->$state:Lcom/squareup/salesreport/export/ExportReportState;

    check-cast v0, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    invoke-virtual {v0}, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrintReport;-><init>(Lcom/squareup/customreport/data/ReportConfig;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
