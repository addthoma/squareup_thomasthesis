.class final Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow;->render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/salesreport/export/ExportReportProps;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/export/ExportReportProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$2;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;

    iget-object v1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$2;->$props:Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {v1}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;-><init>(Lcom/squareup/customreport/data/ReportConfig;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$2;->invoke(Ljava/lang/String;)Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;

    move-result-object p1

    return-object p1
.end method
