.class abstract Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;
.super Ljava/lang/Object;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrepareEmailReport;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SendEmailReport;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PreparePrintReport;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrintReport;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/salesreport/export/ExportReportState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0007H\u0004\u0082\u0001\u0007\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/salesreport/export/ExportReportState;",
        "",
        "()V",
        "toEmail",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;",
        "",
        "Exit",
        "PrepareEmailReport",
        "PreparePrintReport",
        "PrintReport",
        "SendEmailReport",
        "SetIncludeItemsConfig",
        "SetReportEmail",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetReportEmail;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrepareEmailReport;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SendEmailReport;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PreparePrintReport;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$PrintReport;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 198
    invoke-direct {p0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 198
    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/salesreport/export/ExportReportState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/salesreport/export/ExportReportState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method

.method protected final toEmail(Ljava/lang/String;)Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;
    .locals 1

    const-string v0, "$this$toEmail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    new-instance v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    invoke-direct {v0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    goto :goto_0

    .line 269
    :cond_0
    new-instance v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$InvalidEmail;

    invoke-direct {v0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$InvalidEmail;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    :goto_0
    return-object v0
.end method
