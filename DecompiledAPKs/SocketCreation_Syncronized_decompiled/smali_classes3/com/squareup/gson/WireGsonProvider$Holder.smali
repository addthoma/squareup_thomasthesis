.class Lcom/squareup/gson/WireGsonProvider$Holder;
.super Ljava/lang/Object;
.source "WireGsonProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/gson/WireGsonProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Holder"
.end annotation


# static fields
.field static final gson:Lcom/google/gson/Gson;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    invoke-static {}, Lcom/squareup/gson/GsonProvider;->gsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    new-instance v1, Lcom/squareup/wire/WireTypeAdapterFactory;

    invoke-direct {v1}, Lcom/squareup/wire/WireTypeAdapterFactory;-><init>()V

    .line 12
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/squareup/gson/WireGsonProvider$Holder;->gson:Lcom/google/gson/Gson;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
