.class public final Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory$InstanceHolder;->access$000()Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideTextAboveImageSplashScreenConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
    .locals 2

    .line 28
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule$NoSplashScreenModule;->provideTextAboveImageSplashScreenConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory;->provideTextAboveImageSplashScreenConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory;->get()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    move-result-object v0

    return-object v0
.end method
