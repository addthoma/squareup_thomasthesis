.class public abstract Lcom/squareup/loggedout/LoggedOutFeatureModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loggedout/LoggedOutFeatureModule$NoSplashScreenModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$DefaultSplashScreenModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$LoggedOutStarterModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$DeviceCodeViewBindingModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$NoLogInCheckModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$AccountStatusFailureDialogFactoryModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$DisableDeviceCodeLoginModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$EnableDeviceCodeLoginModule;
    }
.end annotation


# static fields
.field private static final POST_INSTALL_LOGIN:Ljava/lang/String; = "post_install_login"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAuthenticationServiceEndpoint(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 64
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint;-><init>(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;)V

    return-object v0
.end method

.method static provideDefaultToLogin(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;"
        }
    .end annotation

    .line 57
    const-class v0, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;

    const-string v1, "post_install_login"

    invoke-static {p1, v1, p0, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract provideCountryGuesser(Lcom/squareup/location/CountryCodeGuesser;)Lcom/squareup/location/CountryGuesser;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideLoggedOutFeatureServicesAsSet()Ljava/util/Set;
    .annotation runtime Ldagger/multibindings/Multibinds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end method

.method abstract provideTrustedDeviceDetailsStore(Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;)Lcom/squareup/ui/login/TrustedDeviceDetailsStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
