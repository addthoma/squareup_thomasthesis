.class final Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5$3;
.super Lkotlin/jvm/internal/Lambda;
.source "ActivateEGiftCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
        "it",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5$3;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v7, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5$3;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;

    iget-object v1, v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    invoke-virtual {v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5$3;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;

    iget-object v1, v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    invoke-virtual {v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;->getDesignId()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5$3;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;

    iget-object v1, v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    check-cast v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    invoke-virtual {v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;->getEmail()Ljava/lang/String;

    move-result-object v5

    .line 91
    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;->getAutoClose()Z

    move-result v6

    move-object v1, v7

    .line 90
    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)V

    invoke-direct {v0, v7}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5$3;->invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$EmailSelected;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
