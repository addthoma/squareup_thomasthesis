.class public final Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseDesignCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDesignCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDesignCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseDesignCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,103:1\n1103#2,7:104\n1360#3:111\n1429#3,3:112\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDesignCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseDesignCoordinator\n*L\n85#1,7:104\n90#1:111\n90#1,3:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001cB3\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J&\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00052\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001bH\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ChooseEGiftCardDesignScreen;",
        "device",
        "Lcom/squareup/util/Device;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Device;Lcom/squareup/picasso/Picasso;)V",
        "adapter",
        "Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;",
        "backButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "state",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Factory",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;

.field private backButton:Lcom/squareup/glyph/SquareGlyphView;

.field private final device:Lcom/squareup/util/Device;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Device;Lcom/squareup/picasso/Picasso;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/picasso/Picasso;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picasso"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->device:Lcom/squareup/util/Device;

    .line 35
    new-instance p1, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;

    invoke-direct {p1, p3}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;-><init>(Lcom/squareup/picasso/Picasso;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->adapter:Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 98
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_actionbarbutton:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 99
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez v0, :cond_0

    const-string v1, "backButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 100
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_choosedesign_recyclerview:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    .line 84
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$1;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_0

    const-string v0, "backButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    .line 104
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 88
    sget-object v0, Lcom/squareup/egiftcard/activation/ChooseDesignItem$HeaderItem;->INSTANCE:Lcom/squareup/egiftcard/activation/ChooseDesignItem$HeaderItem;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getEnabledThemes()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 112
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 113
    check-cast v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    .line 91
    new-instance v2, Lcom/squareup/egiftcard/activation/ChooseDesignItem$ImageItem;

    iget-object v3, v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;->image_url:Ljava/lang/String;

    const-string v4, "it.image_url"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;

    invoke-direct {v4, v1, p3}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;-><init>(Lcom/squareup/protos/client/giftcards/EGiftTheme;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-direct {v2, v3, v4}, Lcom/squareup/egiftcard/activation/ChooseDesignItem$ImageItem;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 90
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 94
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->adapter:Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p2, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;->setItems(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->bindViews(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "recyclerView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->adapter:Lcom/squareup/egiftcard/activation/ChooseDesignRecyclerAdapter;

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string/jumbo v4, "view.resources"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    goto :goto_0

    .line 61
    :cond_3
    new-instance v0, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 62
    new-instance v2, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$attach$1;

    invoke-direct {v2}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$attach$1;-><init>()V

    check-cast v2, Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    .line 69
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v2, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 72
    :goto_0
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$attach$2;-><init>(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
