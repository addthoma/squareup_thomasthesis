.class public final Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;
.super Ljava/lang/Object;
.source "ActivatingErrorSellerDialogScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;",
        "",
        "()V",
        "KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;

.field public static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 15
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;

    .line 17
    new-instance v1, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
