.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;
.super Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.source "ActivateEGiftCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RegisteringEGiftCard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J;\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\u0013\u0010\u001d\u001a\u00020\n2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020\u001cH\u00d6\u0001J\t\u0010!\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "config",
        "Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "designId",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "email",
        "autoClose",
        "",
        "(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getAutoClose",
        "()Z",
        "getConfig",
        "()Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "getDesignId",
        "()Ljava/lang/String;",
        "getEmail",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final autoClose:Z

.field private final config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

.field private final designId:Ljava/lang/String;

.field private final email:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard$Creator;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard$Creator;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "designId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 51
    invoke-direct/range {v0 .. v5}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->copy(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    return v0
.end method

.method public final copy(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;
    .locals 7

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "designId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    iget-boolean p1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAutoClose()Z
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    return v0
.end method

.method public getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    return-object v0
.end method

.method public final getDesignId()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    return-object v0
.end method

.method public final getEmail()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RegisteringEGiftCard(config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", designId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", autoClose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->designId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->email:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->autoClose:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
