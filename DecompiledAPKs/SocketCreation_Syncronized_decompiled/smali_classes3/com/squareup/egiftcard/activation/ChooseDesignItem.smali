.class public abstract Lcom/squareup/egiftcard/activation/ChooseDesignItem;
.super Ljava/lang/Object;
.source "ChooseDesignViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ChooseDesignItem$HeaderItem;,
        Lcom/squareup/egiftcard/activation/ChooseDesignItem$ImageItem;,
        Lcom/squareup/egiftcard/activation/ChooseDesignItem$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00072\u00020\u0001:\u0003\u0007\u0008\tB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseDesignItem;",
        "",
        "ordinal",
        "",
        "(I)V",
        "getOrdinal",
        "()I",
        "Companion",
        "HeaderItem",
        "ImageItem",
        "Lcom/squareup/egiftcard/activation/ChooseDesignItem$HeaderItem;",
        "Lcom/squareup/egiftcard/activation/ChooseDesignItem$ImageItem;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/egiftcard/activation/ChooseDesignItem$Companion;

.field public static final HEADER_ROW:I = 0x0

.field public static final IMAGE_ROW:I = 0x1


# instance fields
.field private final ordinal:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseDesignItem$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/egiftcard/activation/ChooseDesignItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/egiftcard/activation/ChooseDesignItem;->Companion:Lcom/squareup/egiftcard/activation/ChooseDesignItem$Companion;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignItem;->ordinal:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseDesignItem;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getOrdinal()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignItem;->ordinal:I

    return v0
.end method
