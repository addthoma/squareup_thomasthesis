.class public final Lcom/squareup/cycler/Recycler;
.super Ljava/lang/Object;
.source "Recycler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cycler/Recycler$CreatorContext;,
        Lcom/squareup/cycler/Recycler$Config;,
        Lcom/squareup/cycler/Recycler$Adapter;,
        Lcom/squareup/cycler/Recycler$RowSpec;,
        Lcom/squareup/cycler/Recycler$ViewHolder;,
        Lcom/squareup/cycler/Recycler$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecycler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,606:1\n240#2,2:607\n1313#2:609\n1382#2,3:610\n1587#2,2:613\n*E\n*S KotlinDebug\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler\n*L\n116#1,2:607\n108#1:609\n108#1,3:610\n231#1,2:613\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u0000 2*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002:\u0006123456B\u001d\u0008\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010!\u001a\u00020\"J\u001c\u0010#\u001a\u0004\u0018\u0001H$\"\n\u0008\u0001\u0010$\u0018\u0001*\u00020\u0002H\u0086\u0008\u00a2\u0006\u0002\u0010\u001aJ\'\u0010#\u001a\u0004\u0018\u0001H$\"\u0008\u0008\u0001\u0010$*\u00020\u00022\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u0002H$0&H\u0001\u00a2\u0006\u0002\u0010\'J\u000e\u0010(\u001a\u00020\"2\u0006\u0010)\u001a\u00020*J\u0016\u0010(\u001a\u00020\"2\u0006\u0010+\u001a\u00020*2\u0006\u0010,\u001a\u00020*J%\u0010-\u001a\u00020\"2\u001d\u0010.\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\r\u0012\u0004\u0012\u00020\"0/\u00a2\u0006\u0002\u00080R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00000\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R0\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000f2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000f8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0018\u001a\u0004\u0018\u00010\u00022\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u00028F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 \u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/cycler/Recycler;",
        "I",
        "",
        "view",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "config",
        "Lcom/squareup/cycler/Recycler$Config;",
        "(Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/cycler/Recycler$Config;)V",
        "adapter",
        "Lcom/squareup/cycler/Recycler$Adapter;",
        "backgroundContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "currentUpdate",
        "Lcom/squareup/cycler/Update;",
        "value",
        "Lcom/squareup/cycler/DataSource;",
        "data",
        "getData",
        "()Lcom/squareup/cycler/DataSource;",
        "setData",
        "(Lcom/squareup/cycler/DataSource;)V",
        "extensions",
        "",
        "Lcom/squareup/cycler/Extension;",
        "extraItem",
        "getExtraItem",
        "()Ljava/lang/Object;",
        "setExtraItem",
        "(Ljava/lang/Object;)V",
        "mainScope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "getView",
        "()Landroidx/recyclerview/widget/RecyclerView;",
        "clear",
        "",
        "extension",
        "T",
        "type",
        "Ljava/lang/Class;",
        "(Ljava/lang/Class;)Ljava/lang/Object;",
        "refresh",
        "position",
        "",
        "from",
        "until",
        "update",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "Adapter",
        "Companion",
        "Config",
        "CreatorContext",
        "RowSpec",
        "ViewHolder",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cycler/Recycler$Companion;


# instance fields
.field private final adapter:Lcom/squareup/cycler/Recycler$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler$Adapter<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final backgroundContext:Lkotlin/coroutines/CoroutineContext;

.field private final config:Lcom/squareup/cycler/Recycler$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;"
        }
    .end annotation
.end field

.field private currentUpdate:Lcom/squareup/cycler/Update;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Update<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final extensions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cycler/Extension<",
            "TI;>;>;"
        }
    .end annotation
.end field

.field private final mainScope:Lkotlinx/coroutines/CoroutineScope;

.field private final view:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cycler/Recycler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cycler/Recycler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/cycler/Recycler$Config;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/Recycler;->view:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/squareup/cycler/Recycler;->config:Lcom/squareup/cycler/Recycler$Config;

    .line 108
    iget-object p1, p0, Lcom/squareup/cycler/Recycler;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getExtensionSpecs$lib_release()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 609
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 610
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 611
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 109
    invoke-interface {v0}, Lcom/squareup/cycler/ExtensionSpec;->create()Lcom/squareup/cycler/Extension;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 612
    :cond_0
    check-cast p2, Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/cycler/Recycler;->extensions:Ljava/util/List;

    .line 110
    new-instance p1, Lcom/squareup/cycler/Recycler$Adapter;

    new-instance p2, Lcom/squareup/cycler/Recycler$CreatorContext;

    invoke-direct {p2, p0}, Lcom/squareup/cycler/Recycler$CreatorContext;-><init>(Lcom/squareup/cycler/Recycler;)V

    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {p1, p2, v0}, Lcom/squareup/cycler/Recycler$Adapter;-><init>(Lcom/squareup/cycler/Recycler$CreatorContext;Lcom/squareup/cycler/Recycler$Config;)V

    iput-object p1, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    .line 163
    iget-object p1, p0, Lcom/squareup/cycler/Recycler;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$Config;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p1

    check-cast p1, Lkotlin/coroutines/CoroutineContext;

    invoke-static {p1}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cycler/Recycler;->mainScope:Lkotlinx/coroutines/CoroutineScope;

    .line 164
    iget-object p1, p0, Lcom/squareup/cycler/Recycler;->mainScope:Lkotlinx/coroutines/CoroutineScope;

    invoke-interface {p1}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cycler/Recycler;->config:Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p2}, Lcom/squareup/cycler/Recycler$Config;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    check-cast p2, Lkotlin/coroutines/CoroutineContext;

    invoke-interface {p1, p2}, Lkotlin/coroutines/CoroutineContext;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cycler/Recycler;->backgroundContext:Lkotlin/coroutines/CoroutineContext;

    .line 231
    iget-object p1, p0, Lcom/squareup/cycler/Recycler;->extensions:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 613
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cycler/Extension;

    .line 231
    invoke-interface {p2, p0}, Lcom/squareup/cycler/Extension;->attach(Lcom/squareup/cycler/Recycler;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Adapter;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    return-object p0
.end method

.method public static final synthetic access$getBackgroundContext$p(Lcom/squareup/cycler/Recycler;)Lkotlin/coroutines/CoroutineContext;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/cycler/Recycler;->backgroundContext:Lkotlin/coroutines/CoroutineContext;

    return-object p0
.end method

.method public static final synthetic access$getConfig$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Config;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/cycler/Recycler;->config:Lcom/squareup/cycler/Recycler$Config;

    return-object p0
.end method

.method public static final synthetic access$getCurrentUpdate$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Update;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/cycler/Recycler;->currentUpdate:Lcom/squareup/cycler/Update;

    return-object p0
.end method

.method public static final synthetic access$getExtensions$p(Lcom/squareup/cycler/Recycler;)Ljava/util/List;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/cycler/Recycler;->extensions:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$setCurrentUpdate$p(Lcom/squareup/cycler/Recycler;Lcom/squareup/cycler/Update;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/cycler/Recycler;->currentUpdate:Lcom/squareup/cycler/Update;

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .line 134
    sget-object v0, Lcom/squareup/cycler/Recycler$clear$1;->INSTANCE:Lcom/squareup/cycler/Recycler$clear$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final synthetic extension()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    const/4 v0, 0x4

    const-string v1, "T"

    .line 112
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler;->extension(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final extension(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->extensions:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 607
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 116
    invoke-virtual {p1, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 608
    :goto_0
    instance-of p1, v1, Ljava/lang/Object;

    if-nez p1, :cond_2

    move-object v1, v2

    :cond_2
    return-object v1
.end method

.method public final getData()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Adapter;->getCurrentRecyclerData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    return-object v0
.end method

.method public final getExtraItem()Ljava/lang/Object;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Adapter;->getCurrentRecyclerData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->view:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public final refresh(I)V
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Adapter;->notifyItemChanged(I)V

    return-void
.end method

.method public final refresh(II)V
    .locals 2

    if-gt p1, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    sub-int/2addr p2, p1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/cycler/Recycler$Adapter;->notifyItemRangeChanged(II)V

    return-void

    .line 226
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "From ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ") cannot be greater than until ("

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final setData(Lcom/squareup/cycler/DataSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/squareup/cycler/Recycler$data$1;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/Recycler$data$1;-><init>(Lcom/squareup/cycler/DataSource;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final setExtraItem(Ljava/lang/Object;)V
    .locals 1

    .line 156
    new-instance v0, Lcom/squareup/cycler/Recycler$extraItem$1;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/Recycler$extraItem$1;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public final update(Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/Update<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->currentUpdate:Lcom/squareup/cycler/Update;

    if-eqz v0, :cond_0

    .line 172
    new-instance v1, Lcom/squareup/cycler/Update;

    invoke-direct {v1, v0}, Lcom/squareup/cycler/Update;-><init>(Lcom/squareup/cycler/Update;)V

    goto :goto_0

    .line 175
    :cond_0
    new-instance v1, Lcom/squareup/cycler/Update;

    iget-object v0, p0, Lcom/squareup/cycler/Recycler;->adapter:Lcom/squareup/cycler/Recycler$Adapter;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Adapter;->getCurrentRecyclerData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/cycler/Update;-><init>(Lcom/squareup/cycler/RecyclerData;)V

    .line 176
    :goto_0
    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    iput-object v1, p0, Lcom/squareup/cycler/Recycler;->currentUpdate:Lcom/squareup/cycler/Update;

    .line 179
    iget-object v2, p0, Lcom/squareup/cycler/Recycler;->mainScope:Lkotlinx/coroutines/CoroutineScope;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance p1, Lcom/squareup/cycler/Recycler$update$1;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v1, v0}, Lcom/squareup/cycler/Recycler$update$1;-><init>(Lcom/squareup/cycler/Recycler;Lcom/squareup/cycler/Update;Lkotlin/coroutines/Continuation;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function2;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    return-void
.end method
