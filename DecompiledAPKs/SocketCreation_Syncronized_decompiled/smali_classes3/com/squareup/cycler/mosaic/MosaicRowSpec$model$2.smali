.class final Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;
.super Lkotlin/jvm/internal/Lambda;
.source "MosaicRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lcom/squareup/mosaic/core/UiModel<",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpec$model$2\n*L\n1#1,198:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u0002H\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u0002H\u0005H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/mosaic/core/UiModel;",
        "",
        "I",
        "",
        "S",
        "index",
        "",
        "dataItem",
        "invoke",
        "(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;

.field final synthetic $modelLambda:Lkotlin/jvm/functions/Function3;


# direct methods
.method constructor <init>(Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;Lkotlin/jvm/functions/Function3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;->$context:Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;

    iput-object p2, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;->$modelLambda:Lkotlin/jvm/functions/Function3;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "dataItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;->$context:Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;

    iget-object v1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;->$modelLambda:Lkotlin/jvm/functions/Function3;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, v0, p1, p2}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$context$1;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 125
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec$model$2;->invoke(ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    return-object p1
.end method
