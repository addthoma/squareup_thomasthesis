.class final Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "RealOrderMagstripeWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "Lio/reactivex/CompletableSource;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/CompletableSource;",
        "kotlin.jvm.PlatformType",
        "result",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/mailorder/OrderWorkflowResult;)Lio/reactivex/CompletableSource;
    .locals 3

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;->INSTANCE:Lcom/squareup/mailorder/OrderWorkflowResult$FinishedWithOrder;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 74
    iget-object p1, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->access$getSetupGuideIntegrationRunner$p(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    move-result-object p1

    .line 75
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ORDER_FREE_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    sget-object v1, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    const-string v2, "MainActivityScope.INSTANCE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 76
    new-instance v2, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2$1;

    invoke-direct {v2, p0}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2$1;-><init>(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 74
    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;->handleCompletion(Lcom/squareup/protos/checklist/common/ActionItemName;Lcom/squareup/ui/main/RegisterTreeKey;Lkotlin/jvm/functions/Function0;)Lio/reactivex/Completable;

    move-result-object p1

    check-cast p1, Lio/reactivex/CompletableSource;

    goto :goto_0

    .line 78
    :cond_0
    iget-object p1, p0, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;->access$getContainer$p(Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    .line 79
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object p1

    check-cast p1, Lio/reactivex/CompletableSource;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/mailorder/OrderWorkflowResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermagstripe/RealOrderMagstripeWorkflowRunner$onEnterScope$2;->apply(Lcom/squareup/mailorder/OrderWorkflowResult;)Lio/reactivex/CompletableSource;

    move-result-object p1

    return-object p1
.end method
