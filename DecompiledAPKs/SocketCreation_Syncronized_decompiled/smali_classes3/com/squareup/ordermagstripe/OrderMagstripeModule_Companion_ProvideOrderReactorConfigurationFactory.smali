.class public final Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;
.super Ljava/lang/Object;
.source "OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderReactor$Configuration;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideOrderReactorConfiguration(Lcom/squareup/settings/server/Features;)Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeModule;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;->provideOrderReactorConfiguration(Lcom/squareup/settings/server/Features;)Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/mailorder/OrderReactor$Configuration;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {v0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;->provideOrderReactorConfiguration(Lcom/squareup/settings/server/Features;)Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule_Companion_ProvideOrderReactorConfigurationFactory;->get()Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    return-object v0
.end method
