.class final Lcom/squareup/fonts/FontSpan$font$2;
.super Lkotlin/jvm/internal/Lambda;
.source "FontSpan.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Landroid/graphics/Typeface;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/graphics/Typeface;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/fonts/FontSpan;


# direct methods
.method constructor <init>(Lcom/squareup/fonts/FontSpan;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/fonts/FontSpan$font$2;->this$0:Lcom/squareup/fonts/FontSpan;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Landroid/graphics/Typeface;
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/fonts/FontSpan$font$2;->this$0:Lcom/squareup/fonts/FontSpan;

    invoke-static {v0}, Lcom/squareup/fonts/FontSpan;->access$getAppContext$p(Lcom/squareup/fonts/FontSpan;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/fonts/FontSpan$font$2;->this$0:Lcom/squareup/fonts/FontSpan;

    invoke-static {v1}, Lcom/squareup/fonts/FontSpan;->access$getFontId$p(Lcom/squareup/fonts/FontSpan;)I

    move-result v1

    invoke-static {v0, v1}, Landroidx/core/content/res/ResourcesCompat;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/fonts/FontSpan$font$2;->invoke()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method
