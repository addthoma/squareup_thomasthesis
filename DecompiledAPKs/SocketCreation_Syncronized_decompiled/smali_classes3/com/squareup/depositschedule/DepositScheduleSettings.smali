.class public interface abstract Lcom/squareup/depositschedule/DepositScheduleSettings;
.super Ljava/lang/Object;
.source "DepositScheduleSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/depositschedule/DepositScheduleSettings$State;,
        Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;,
        Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;,
        Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;,
        Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;,
        Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001:\u0006\u0017\u0018\u0019\u001a\u001b\u001cJ\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u000c\u001a\u00020\u0006H\'J\u001e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\'J\u0010\u0010\u0011\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000fH&J\u0016\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0013\u001a\u00020\u0006H\'J\u0016\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u000c\u001a\u00020\u0006H\'J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0016H\'\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/depositschedule/DepositScheduleSettings;",
        "",
        "getDepositSchedule",
        "Lio/reactivex/Single;",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "isVisible",
        "",
        "setAllToCustom",
        "",
        "setAllToOneToTwoBusinessDays",
        "setAllToSameDay",
        "setAutomaticDepositsEnabled",
        "enabled",
        "setCustom",
        "dayOfWeek",
        "",
        "hourOfDay",
        "setDayOfWeek",
        "setSameDayDeposit",
        "sameDay",
        "setWeekendBalanceEnabled",
        "state",
        "Lio/reactivex/Observable;",
        "AutomaticDeposits",
        "DepositScheduleState",
        "DepositSpeed",
        "SetDepositSpeed",
        "State",
        "WeekendBalance",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDepositSchedule()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isVisible()Z
.end method

.method public abstract setAllToCustom()V
.end method

.method public abstract setAllToOneToTwoBusinessDays()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setAllToSameDay()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setAutomaticDepositsEnabled(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCustom(II)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setDayOfWeek(I)V
.end method

.method public abstract setSameDayDeposit(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setWeekendBalanceEnabled(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method

.method public abstract state()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end method
