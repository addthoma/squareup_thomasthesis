.class public final Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealVerifyCardChangeWorkflow.kt"

# interfaces
.implements Lcom/squareup/debitcard/VerifyCardChangeWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/debitcard/VerifyCardChangeProps;",
        "Lcom/squareup/debitcard/VerifyCardChangeState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/debitcard/VerifyCardChangeWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealVerifyCardChangeWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealVerifyCardChangeWorkflow.kt\ncom/squareup/debitcard/RealVerifyCardChangeWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,97:1\n32#2,12:98\n149#3,5:110\n149#3,5:115\n149#3,5:120\n*E\n*S KotlinDebug\n*F\n+ 1 RealVerifyCardChangeWorkflow.kt\ncom/squareup/debitcard/RealVerifyCardChangeWorkflow\n*L\n42#1,12:98\n57#1,5:110\n60#1,5:115\n68#1,5:120\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\u001bB\u001f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u001a\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016JN\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0004H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;",
        "Lcom/squareup/debitcard/VerifyCardChangeWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/debitcard/VerifyCardChangeProps;",
        "Lcom/squareup/debitcard/VerifyCardChangeState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "verifyingCardChangeWorkflow",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;",
        "verifyCardChangeSuccessWorkflow",
        "Lcom/squareup/debitcard/VerifyCardChangeSuccessWorkflow;",
        "verifyCardChangeFailureWorkflow",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;",
        "(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;Lcom/squareup/debitcard/VerifyCardChangeSuccessWorkflow;Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final verifyCardChangeFailureWorkflow:Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;

.field private final verifyCardChangeSuccessWorkflow:Lcom/squareup/debitcard/VerifyCardChangeSuccessWorkflow;

.field private final verifyingCardChangeWorkflow:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;Lcom/squareup/debitcard/VerifyCardChangeSuccessWorkflow;Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "verifyingCardChangeWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifyCardChangeSuccessWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifyCardChangeFailureWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->verifyingCardChangeWorkflow:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;

    iput-object p2, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->verifyCardChangeSuccessWorkflow:Lcom/squareup/debitcard/VerifyCardChangeSuccessWorkflow;

    iput-object p3, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->verifyCardChangeFailureWorkflow:Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/debitcard/VerifyCardChangeState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 98
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 103
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 105
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 106
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 107
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 109
    :cond_3
    check-cast v1, Lcom/squareup/debitcard/VerifyCardChangeState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 42
    :cond_4
    sget-object p1, Lcom/squareup/debitcard/VerifyCardChangeState$VerifyingCardChange;->INSTANCE:Lcom/squareup/debitcard/VerifyCardChangeState$VerifyingCardChange;

    move-object v1, p1

    check-cast v1, Lcom/squareup/debitcard/VerifyCardChangeState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/debitcard/VerifyCardChangeProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->initialState(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/debitcard/VerifyCardChangeState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/debitcard/VerifyCardChangeProps;

    check-cast p2, Lcom/squareup/debitcard/VerifyCardChangeState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->render(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/debitcard/VerifyCardChangeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/debitcard/VerifyCardChangeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/debitcard/VerifyCardChangeProps;",
            "Lcom/squareup/debitcard/VerifyCardChangeState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/debitcard/VerifyCardChangeState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object v0, Lcom/squareup/debitcard/VerifyCardChangeState$VerifyingCardChange;->INSTANCE:Lcom/squareup/debitcard/VerifyCardChangeState$VerifyingCardChange;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->verifyingCardChangeWorkflow:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;

    move-object v3, p2

    check-cast v3, Lcom/squareup/workflow/Workflow;

    const/4 v5, 0x0

    sget-object p2, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$1;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$1;

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    move-object v4, p1

    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 111
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 112
    const-class p3, Lcom/squareup/debitcard/VerifyingCardChangeScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 113
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 111
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto :goto_0

    .line 59
    :cond_0
    sget-object p1, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingSuccessMessage;->INSTANCE:Lcom/squareup/debitcard/VerifyCardChangeState$ShowingSuccessMessage;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->verifyCardChangeSuccessWorkflow:Lcom/squareup/debitcard/VerifyCardChangeSuccessWorkflow;

    move-object v3, p1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    const/4 v4, 0x0

    sget-object p1, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 116
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 117
    const-class p3, Lcom/squareup/debitcard/VerifyCardChangeSuccessScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 118
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 116
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    goto :goto_0

    .line 62
    :cond_1
    instance-of p1, p2, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingFailureMessage;

    if-eqz p1, :cond_2

    .line 63
    new-instance v4, Lcom/squareup/debitcard/VerifyCardChangeFailureProps;

    .line 64
    check-cast p2, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingFailureMessage;

    invoke-virtual {p2}, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingFailureMessage;->getTitle()Ljava/lang/String;

    move-result-object p1

    .line 65
    invoke-virtual {p2}, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingFailureMessage;->getMessage()Ljava/lang/String;

    move-result-object p2

    .line 63
    invoke-direct {v4, p1, p2}, Lcom/squareup/debitcard/VerifyCardChangeFailureProps;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->verifyCardChangeFailureWorkflow:Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;

    move-object v3, p1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    const/4 v5, 0x0

    sget-object p1, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$3;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$3;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 121
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 122
    const-class p3, Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 123
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 121
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 70
    :goto_0
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 124
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/debitcard/VerifyCardChangeState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/debitcard/VerifyCardChangeState;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->snapshotState(Lcom/squareup/debitcard/VerifyCardChangeState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
