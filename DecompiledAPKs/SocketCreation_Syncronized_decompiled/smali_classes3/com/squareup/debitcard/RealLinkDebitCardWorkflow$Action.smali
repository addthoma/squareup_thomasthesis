.class public abstract Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action;
.super Ljava/lang/Object;
.source "RealLinkDebitCardWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$Finish;,
        Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$StartOver;,
        Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;,
        Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/debitcard/LinkDebitCardState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0006H\u0016\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/debitcard/LinkDebitCardState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Finish",
        "LinkCard",
        "ShowResult",
        "StartOver",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$Finish;",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$StartOver;",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 319
    invoke-direct {p0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 319
    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/debitcard/LinkDebitCardState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    sget-object v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$Finish;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 322
    :cond_0
    sget-object v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$StartOver;->INSTANCE:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$StartOver;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardState$PrepareToLinkCard;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardState$PrepareToLinkCard;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 325
    :cond_1
    instance-of v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;

    if-eqz v0, :cond_2

    .line 326
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkingCard;

    move-object v1, p0

    check-cast v1, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;

    invoke-virtual {v1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$LinkCard;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/LinkDebitCardState$LinkingCard;-><init>(Lcom/squareup/protos/client/bills/CardData;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 328
    :cond_2
    instance-of v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;

    if-eqz v0, :cond_3

    .line 329
    move-object v0, p0

    check-cast v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;

    invoke-virtual {v0}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;->getLinkResult()Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
