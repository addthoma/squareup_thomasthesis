.class final Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LinkDebitCardEntryLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->onScreen(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/Card;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/Card;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/debitcard/LinkDebitCardEntryScreen;

.field final synthetic this$0:Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;->this$0:Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;

    iput-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;->$screen:Lcom/squareup/debitcard/LinkDebitCardEntryScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;->invoke(Lcom/squareup/Card;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/Card;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;->this$0:Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;->$screen:Lcom/squareup/debitcard/LinkDebitCardEntryScreen;

    invoke-static {p1, v0}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->access$onLink(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    return-void
.end method
