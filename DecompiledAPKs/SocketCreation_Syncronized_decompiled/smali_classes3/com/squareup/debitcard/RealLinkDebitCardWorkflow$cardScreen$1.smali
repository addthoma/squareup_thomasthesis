.class final Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLinkDebitCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->cardScreen(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;",
        "it",
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

.field final synthetic this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    iput-object p2, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;->$props:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;->this$0:Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;

    iget-object v1, p0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;->$props:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    invoke-static {v0, v1, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;->access$logLinkDebitCard(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)V

    .line 102
    new-instance v0, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;-><init>(Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$cardScreen$1;->invoke(Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;)Lcom/squareup/debitcard/RealLinkDebitCardWorkflow$Action$ShowResult;

    move-result-object p1

    return-object p1
.end method
