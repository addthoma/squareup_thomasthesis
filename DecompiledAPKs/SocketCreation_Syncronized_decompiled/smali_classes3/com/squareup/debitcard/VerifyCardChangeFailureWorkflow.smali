.class public final Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "VerifyCardChangeFailureWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureProps;",
        "Lkotlin/Unit;",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0005J$\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\u00022\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u000eH\u0016R\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureProps;",
        "",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;",
        "()V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "getFinish",
        "()Lcom/squareup/workflow/WorkflowAction;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final finish:Lcom/squareup/workflow/WorkflowAction;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    .line 11
    sget-object v0, Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow$finish$1;->INSTANCE:Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow$finish$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method


# virtual methods
.method public final getFinish()Lcom/squareup/workflow/WorkflowAction;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public render(Lcom/squareup/debitcard/VerifyCardChangeFailureProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;

    .line 19
    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyCardChangeFailureProps;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 20
    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyCardChangeFailureProps;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 21
    new-instance v2, Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow$render$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow$render$1;-><init>(Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 18
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/debitcard/VerifyCardChangeFailureProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/VerifyCardChangeFailureWorkflow;->render(Lcom/squareup/debitcard/VerifyCardChangeFailureProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;

    move-result-object p1

    return-object p1
.end method
