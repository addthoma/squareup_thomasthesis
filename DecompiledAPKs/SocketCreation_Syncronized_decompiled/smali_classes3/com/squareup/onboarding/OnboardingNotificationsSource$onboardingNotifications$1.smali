.class final Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;
.super Ljava/lang/Object;
.source "OnboardingNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/OnboardingNotificationsSource;->onboardingNotifications()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingNotificationsSource.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingNotificationsSource.kt\ncom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,220:1\n1360#2:221\n1429#2,3:222\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingNotificationsSource.kt\ncom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1\n*L\n117#1:221\n117#1,3:222\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u000124\u0010\u0003\u001a0\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0004H\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Triple;",
        "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
        "kotlin.jvm.PlatformType",
        "",
        "",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/OnboardingNotificationsSource;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/OnboardingNotificationsSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;->this$0:Lcom/squareup/onboarding/OnboardingNotificationsSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lkotlin/Triple;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;->apply(Lkotlin/Triple;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lkotlin/Triple;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Triple<",
            "+",
            "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
            "Lkotlin/Unit;",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
            "+",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    invoke-virtual {p1}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 99
    iget-object v2, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;->this$0:Lcom/squareup/onboarding/OnboardingNotificationsSource;

    invoke-static {v2}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->access$getSettings$p(Lcom/squareup/onboarding/OnboardingNotificationsSource;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v2

    .line 102
    invoke-virtual {v2}, Lcom/squareup/settings/server/OnboardingSettings;->showInAppActivationPostSignup()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v0, "activationFeatures"

    .line 103
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/settings/server/OnboardingSettings;->isEligibleForSquareCardPayments()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupInApp;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupInApp;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v2

    if-eqz v2, :cond_5

    if-nez v0, :cond_2

    goto :goto_0

    .line 110
    :cond_2
    sget-object v2, Lcom/squareup/onboarding/OnboardingNotificationsSource$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    goto :goto_0

    .line 112
    :cond_3
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_4
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_5
    :goto_0
    check-cast v1, Ljava/lang/Iterable;

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 222
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 223
    check-cast v2, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;

    .line 118
    iget-object v3, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;->this$0:Lcom/squareup/onboarding/OnboardingNotificationsSource;

    .line 120
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/notificationcenterdata/Notification$State;

    if-eqz v4, :cond_6

    goto :goto_2

    :cond_6
    sget-object v4, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    .line 118
    :goto_2
    invoke-static {v3, v2, v4}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->access$createOnboardingNotification(Lcom/squareup/onboarding/OnboardingNotificationsSource;Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v2

    .line 121
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 224
    :cond_7
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
