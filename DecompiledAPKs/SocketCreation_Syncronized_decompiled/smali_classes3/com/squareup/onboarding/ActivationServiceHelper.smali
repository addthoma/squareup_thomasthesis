.class public Lcom/squareup/onboarding/ActivationServiceHelper;
.super Ljava/lang/Object;
.source "ActivationServiceHelper.java"


# static fields
.field public static final WEB_ACTIVATION_COMPLETE:Ljava/lang/String; = "complete"


# instance fields
.field private final application:Landroid/app/Application;

.field private final service:Lcom/squareup/server/activation/ActivationService;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/server/activation/ActivationService;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/onboarding/ActivationServiceHelper;->application:Landroid/app/Application;

    .line 35
    iput-object p2, p0, Lcom/squareup/onboarding/ActivationServiceHelper;->service:Lcom/squareup/server/activation/ActivationService;

    return-void
.end method

.method private getCallbackUrl()Ljava/lang/String;
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationServiceHelper;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/squareup/onboarding/ActivationServiceHelper;->application:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    .line 82
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    .line 83
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v1, 0x1

    const-string v2, "complete"

    .line 84
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    invoke-virtual {v0, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toMerchantAnalyticValue(Lcom/squareup/onboarding/intent/WhereOptions;)Ljava/lang/String;
    .locals 2

    .line 69
    sget-object v0, Lcom/squareup/onboarding/ActivationServiceHelper$1;->$SwitchMap$com$squareup$onboarding$intent$WhereOptions:[I

    invoke-virtual {p1}, Lcom/squareup/onboarding/intent/WhereOptions;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/squareup/onboarding/intent/WhereOptions;->name()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "tablet_or_register"

    return-object p1
.end method


# virtual methods
.method public activationUrl()Lcom/squareup/server/SimpleStandardResponse;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/activation/ActivationUrl;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationServiceHelper;->service:Lcom/squareup/server/activation/ActivationService;

    invoke-direct {p0}, Lcom/squareup/onboarding/ActivationServiceHelper;->getCallbackUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/activation/ActivationService;->activationUrl(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    return-object v0
.end method

.method public logMerchantAnalytics(Ljava/util/Set;Lcom/squareup/onboarding/intent/HowOptions;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/onboarding/intent/WhereOptions;",
            ">;",
            "Lcom/squareup/onboarding/intent/HowOptions;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation

    .line 46
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 47
    new-instance p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    new-instance p2, Lcom/squareup/server/SimpleResponse;

    invoke-direct {p2, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-direct {p1, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 50
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-nez p2, :cond_1

    const/4 v1, 0x0

    :cond_1
    add-int/2addr v0, v1

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 52
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/intent/WhereOptions;

    .line 53
    new-instance v2, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;-><init>()V

    sget-object v3, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_LOCATION:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 54
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->name(Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;

    move-result-object v2

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/onboarding/ActivationServiceHelper;->toMerchantAnalyticValue(Lcom/squareup/onboarding/intent/WhereOptions;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->build()Lcom/squareup/protos/client/onboard/MerchantAnalytic;

    move-result-object v0

    .line 53
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    .line 59
    new-instance p1, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;->PAYMENT_TYPE:Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;

    .line 60
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->name(Lcom/squareup/protos/client/onboard/MerchantAnalytic$MerchantAnalyticName;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p2}, Lcom/squareup/onboarding/intent/HowOptions;->name()Ljava/lang/String;

    move-result-object p2

    sget-object v0, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/MerchantAnalytic$Builder;->build()Lcom/squareup/protos/client/onboard/MerchantAnalytic;

    move-result-object p1

    .line 59
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_3
    iget-object p1, p0, Lcom/squareup/onboarding/ActivationServiceHelper;->service:Lcom/squareup/server/activation/ActivationService;

    new-instance p2, Lcom/squareup/protos/client/onboard/MerchantAnalyticRequest;

    invoke-direct {p2, v1}, Lcom/squareup/protos/client/onboard/MerchantAnalyticRequest;-><init>(Ljava/util/List;)V

    invoke-interface {p1, p2}, Lcom/squareup/server/activation/ActivationService;->merchantAnalytic(Lcom/squareup/protos/client/onboard/MerchantAnalyticRequest;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
