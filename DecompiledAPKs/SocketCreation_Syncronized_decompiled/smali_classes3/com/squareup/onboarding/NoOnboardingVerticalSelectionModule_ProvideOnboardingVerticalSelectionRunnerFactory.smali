.class public final Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory;
.super Ljava/lang/Object;
.source "NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory$InstanceHolder;->access$000()Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideOnboardingVerticalSelectionRunner()Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule;->provideOnboardingVerticalSelectionRunner()Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory;->provideOnboardingVerticalSelectionRunner()Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/onboarding/NoOnboardingVerticalSelectionModule_ProvideOnboardingVerticalSelectionRunnerFactory;->get()Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;

    move-result-object v0

    return-object v0
.end method
