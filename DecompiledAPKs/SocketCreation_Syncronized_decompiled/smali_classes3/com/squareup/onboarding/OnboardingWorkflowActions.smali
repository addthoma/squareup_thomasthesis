.class public final Lcom/squareup/onboarding/OnboardingWorkflowActions;
.super Ljava/lang/Object;
.source "OnboardingWorkflowActions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingWorkflowActions;",
        "",
        "()V",
        "BACK",
        "",
        "EXIT",
        "FINISH",
        "SKIP",
        "SUBMIT",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BACK:Ljava/lang/String; = "back"

.field public static final EXIT:Ljava/lang/String; = "exit"

.field public static final FINISH:Ljava/lang/String; = "finish"

.field public static final INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowActions;

.field public static final SKIP:Ljava/lang/String; = "skip"

.field public static final SUBMIT:Ljava/lang/String; = "submit"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/onboarding/OnboardingWorkflowActions;

    invoke-direct {v0}, Lcom/squareup/onboarding/OnboardingWorkflowActions;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/OnboardingWorkflowActions;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowActions;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
