.class public final Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializerKt;
.super Ljava/lang/Object;
.source "OnboardingWorkflowSerializer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toSnapshot(Lcom/squareup/onboarding/flow/OnboardingState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "$this$toSnapshot"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;

    invoke-virtual {v0, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowSerializer;->snapshotState(Lcom/squareup/onboarding/flow/OnboardingState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method
