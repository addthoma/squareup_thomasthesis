.class public final Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$DefaultImpls;
.super Ljava/lang/Object;
.source "PanelMemory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static getBoolean(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;Z)Z
    .locals 1

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-interface {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->get(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/onboard/Output;->boolean_value:Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method public static getInt(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;I)I
    .locals 1

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-interface {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->get(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/onboard/Output;->integer_value:Ljava/lang/Integer;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_0
    return p2
.end method

.method public static getString(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-interface {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->get(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, p2

    :goto_0
    return-object p0
.end method

.method public static synthetic getString$default(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, ""

    .line 83
    :cond_0
    invoke-interface {p0, p1, p2}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: getString"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static getStringList(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-interface {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->get(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    if-eqz p0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    const-string p0, "\t"

    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->split$default(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, p2

    :goto_0
    return-object p0
.end method

.method public static synthetic getStringList$default(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 74
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-interface {p0, p1, p2}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getStringList(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: getStringList"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
