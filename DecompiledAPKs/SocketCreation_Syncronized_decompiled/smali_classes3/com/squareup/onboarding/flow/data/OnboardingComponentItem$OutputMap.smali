.class public final Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;
.super Ljava/lang/Object;
.source "OnboardingComponentItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "OutputMap"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingComponentItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingComponentItem.kt\ncom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,131:1\n1099#2,2:132\n1127#2,4:134\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingComponentItem.kt\ncom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap\n*L\n84#1,2:132\n84#1,4:134\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0008H\u0086\u0002R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00040\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;",
        "",
        "outputs",
        "",
        "Lcom/squareup/protos/client/onboard/Output;",
        "(Ljava/util/List;)V",
        "outputMap",
        "",
        "",
        "get",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;",
        "outputName",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final outputMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;)V"
        }
    .end annotation

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    check-cast p1, Ljava/lang/Iterable;

    const/16 v0, 0xa

    .line 132
    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 133
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v1, Ljava/util/Map;

    .line 134
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 135
    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/client/onboard/Output;

    .line 84
    iget-object v2, v2, Lcom/squareup/protos/client/onboard/Output;->name:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 137
    :cond_0
    iput-object v1, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;->outputMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;
    .locals 2

    const-string v0, "outputName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;->outputMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/Output;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;-><init>(Lcom/squareup/protos/client/onboard/Output;)V

    return-object v0
.end method
