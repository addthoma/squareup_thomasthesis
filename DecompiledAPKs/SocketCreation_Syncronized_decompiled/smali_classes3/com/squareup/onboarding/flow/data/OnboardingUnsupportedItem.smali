.class public final Lcom/squareup/onboarding/flow/data/OnboardingUnsupportedItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingUnsupportedItem.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingUnsupportedItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "(Lcom/squareup/protos/client/onboard/Component;)V",
        "debugInfo",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 7
    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public final debugInfo()Ljava/lang/String;
    .locals 2

    .line 9
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingUnsupportedItem;->getComponent()Lcom/squareup/protos/client/onboard/Component;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/Component;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "component.toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
