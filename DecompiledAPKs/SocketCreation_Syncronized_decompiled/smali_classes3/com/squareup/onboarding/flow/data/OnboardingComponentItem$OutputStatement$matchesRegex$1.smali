.class final Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingComponentItem.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->matchesRegex(Lkotlin/text/Regex;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/onboard/Output;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingComponentItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingComponentItem.kt\ncom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1\n*L\n1#1,131:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/protos/client/onboard/Output;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $regex:Lkotlin/text/Regex;


# direct methods
.method constructor <init>(Lkotlin/text/Regex;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1;->$regex:Lkotlin/text/Regex;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/protos/client/onboard/Output;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1;->invoke(Lcom/squareup/protos/client/onboard/Output;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/onboard/Output;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    const-string v0, "it.string_value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1;->$regex:Lkotlin/text/Regex;

    invoke-virtual {v0, p1}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method
