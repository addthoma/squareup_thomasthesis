.class public final Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;
.super Ljava/lang/Object;
.source "OnboardingComponentsFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J(\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;",
        "",
        "()V",
        "createComponentItem",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "memory",
        "Lcom/squareup/onboarding/flow/PanelMemory;",
        "createComponentViewHolder",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "type",
        "Lcom/squareup/protos/client/onboard/ComponentType;",
        "parent",
        "Landroid/view/ViewGroup;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createComponentItem(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "memory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Component;->name:Ljava/lang/String;

    const-string v1, "component.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/squareup/onboarding/flow/PanelMemory;->get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object p2

    .line 88
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Component;->type:Lcom/squareup/protos/client/onboard/ComponentType;

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 102
    :pswitch_0
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto/16 :goto_1

    .line 101
    :pswitch_1
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto/16 :goto_1

    .line 100
    :pswitch_2
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 99
    :pswitch_3
    new-instance p2, Lcom/squareup/onboarding/flow/data/OnboardingImageItem;

    invoke-direct {p2, p1}, Lcom/squareup/onboarding/flow/data/OnboardingImageItem;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 98
    :pswitch_4
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 97
    :pswitch_5
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 96
    :pswitch_6
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 95
    :pswitch_7
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 94
    :pswitch_8
    new-instance p2, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;

    invoke-direct {p2, p1}, Lcom/squareup/onboarding/flow/data/OnboardingParagraphItem;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 93
    :pswitch_9
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingEditItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 92
    :pswitch_a
    new-instance p2, Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;

    invoke-direct {p2, p1}, Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 91
    :pswitch_b
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingMultiSelectListItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 90
    :pswitch_c
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 89
    :pswitch_d
    new-instance p2, Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;

    invoke-direct {p2, p1}, Lcom/squareup/onboarding/flow/data/OnboardingButtonItem;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    goto :goto_1

    .line 103
    :goto_0
    new-instance p2, Lcom/squareup/onboarding/flow/data/OnboardingUnsupportedItem;

    invoke-direct {p2, p1}, Lcom/squareup/onboarding/flow/data/OnboardingUnsupportedItem;-><init>(Lcom/squareup/protos/client/onboard/Component;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    :goto_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final createComponentViewHolder(Lcom/squareup/protos/client/onboard/ComponentType;Landroid/view/ViewGroup;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/onboard/ComponentType;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
            ")",
            "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
            "+",
            "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    goto/16 :goto_0

    .line 65
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    .line 79
    :pswitch_0
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto/16 :goto_1

    .line 78
    :pswitch_1
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto/16 :goto_1

    .line 77
    :pswitch_2
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 76
    :pswitch_3
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingImageViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 75
    :pswitch_4
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 74
    :pswitch_5
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 73
    :pswitch_6
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingAddressViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 72
    :pswitch_7
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 71
    :pswitch_8
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingParagraphViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 70
    :pswitch_9
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingEditViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 69
    :pswitch_a
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 68
    :pswitch_b
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingMultiSelectListViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 67
    :pswitch_c
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 66
    :pswitch_d
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingButtonViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    goto :goto_1

    .line 80
    :goto_0
    new-instance p1, Lcom/squareup/onboarding/flow/view/OnboardingUnsupportedViewHolder;

    invoke-direct {p1, p3, p2}, Lcom/squareup/onboarding/flow/view/OnboardingUnsupportedViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V

    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    :goto_1
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
