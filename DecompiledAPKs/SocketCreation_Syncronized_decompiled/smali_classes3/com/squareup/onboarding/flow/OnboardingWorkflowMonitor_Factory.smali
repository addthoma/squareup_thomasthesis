.class public final Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;
.super Ljava/lang/Object;
.source "OnboardingWorkflowMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final bankSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->activationServiceHelperProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->bankSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->onboardingTypeProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;)",
            "Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;"
        }
    .end annotation

    .line 83
    new-instance v11, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/onboarding/OnboardingType;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ldagger/Lazy<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;)",
            "Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;"
        }
    .end annotation

    .line 92
    new-instance v11, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;-><init>(Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;
    .locals 11

    .line 71
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->activationServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/onboarding/ActivationServiceHelper;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->bankSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->onboardingTypeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/onboarding/OnboardingType;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v10

    invoke-static/range {v1 .. v10}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->newInstance(Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor_Factory;->get()Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    move-result-object v0

    return-object v0
.end method
