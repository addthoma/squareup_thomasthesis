.class public final Lcom/squareup/onboarding/flow/OnboardingReactor;
.super Ljava/lang/Object;
.source "OnboardingReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingReactor.kt\ncom/squareup/onboarding/flow/OnboardingReactor\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,359:1\n215#2,2:360\n250#2,2:362\n1550#2,3:364\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingReactor.kt\ncom/squareup/onboarding/flow/OnboardingReactor\n*L\n174#1,2:360\n245#1,2:362\n268#1,3:364\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001KBW\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u0018\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J$\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J*\u0010$\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040%2\u0006\u0010&\u001a\u00020\u00022\u0006\u0010\'\u001a\u00020(H\u0016J\u001c\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010 \u001a\u00020*H\u0002J\u001c\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u001c\u0010,\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010 \u001a\u00020!H\u0002J:\u0010-\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f0.2\u0006\u0010 \u001a\u00020\u00022\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u00020\u0003002\u0006\u0010\'\u001a\u00020(H\u0016J\u0010\u00101\u001a\u00020\u00022\u0006\u00102\u001a\u000203H\u0002J\u0018\u00104\u001a\u00020\u00022\u0006\u00105\u001a\u00020!2\u0006\u00106\u001a\u00020#H\u0002J\u0018\u00107\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!2\u0006\u00106\u001a\u000208H\u0002J,\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020=2\u0006\u0010\u001b\u001a\u00020\u001aH\u0002J$\u0010>\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010?\u001a\u00020@2\u0006\u0010\u001b\u001a\u00020\u001aH\u0002J\"\u0010A\u001a\u00020!2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010:\u001a\u00020;2\u0008\u0008\u0002\u0010B\u001a\u00020CH\u0002J`\u0010D\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001f0.\"\u0012\u0008\u0000\u0010E*\u000c\u0012\u0004\u0012\u0002HE\u0012\u0002\u0008\u00030F*\u0008\u0012\u0004\u0012\u0002HE0G2\u0006\u0010 \u001a\u00020\u00022\u0008\u0008\u0002\u0010H\u001a\u00020;2\u0012\u0010I\u001a\u000e\u0012\u0004\u0012\u0002HE\u0012\u0004\u0012\u00020@0JH\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006L"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "onboardingService",
        "Lcom/squareup/server/onboard/OnboardingService;",
        "messages",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "panelVerifier",
        "Lcom/squareup/onboarding/flow/PanelVerifier;",
        "unique",
        "Lcom/squareup/util/Unique;",
        "secureScopeManager",
        "Lcom/squareup/secure/SecureScopeManager;",
        "(Lcom/squareup/server/onboard/OnboardingService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Lcom/squareup/onboarding/flow/PanelVerifier;Lcom/squareup/util/Unique;Lcom/squareup/secure/SecureScopeManager;)V",
        "addScreenToHistory",
        "Lcom/squareup/onboarding/flow/PanelHistory;",
        "history",
        "newTop",
        "Lcom/squareup/onboarding/flow/PanelScreen;",
        "handleEventFromPanel",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
        "event",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "onBackPressedFromError",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;",
        "onBackPressedFromPanel",
        "onExitAsked",
        "onReact",
        "Lio/reactivex/Single;",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "requestFirstStep",
        "flow",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "requestNextStep",
        "fromState",
        "action",
        "requestNextStepWithValidation",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;",
        "screenDataFromError",
        "sessionToken",
        "",
        "message",
        "Lcom/squareup/receiving/FailureMessage;",
        "screenDataFromSuccess",
        "payload",
        "Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;",
        "showPanel",
        "error",
        "Lcom/squareup/onboarding/flow/PanelError;",
        "reactionFromState",
        "T",
        "Lcom/squareup/wire/Message;",
        "Lcom/squareup/server/StatusResponse;",
        "fallbackSessionToken",
        "payloadParser",
        "Lkotlin/Function1;",
        "ResponsePayload",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final messages:Lcom/squareup/receiving/FailureMessageFactory;

.field private final onboardingService:Lcom/squareup/server/onboard/OnboardingService;

.field private final panelVerifier:Lcom/squareup/onboarding/flow/PanelVerifier;

.field private final secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final unique:Lcom/squareup/util/Unique;


# direct methods
.method public constructor <init>(Lcom/squareup/server/onboard/OnboardingService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Lcom/squareup/onboarding/flow/PanelVerifier;Lcom/squareup/util/Unique;Lcom/squareup/secure/SecureScopeManager;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/onboard/OnboardingService;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/onboarding/flow/PanelVerifier;",
            "Lcom/squareup/util/Unique;",
            "Lcom/squareup/secure/SecureScopeManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "panelVerifier"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unique"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secureScopeManager"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->onboardingService:Lcom/squareup/server/onboard/OnboardingService;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->messages:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p6, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->localeProvider:Ljavax/inject/Provider;

    iput-object p7, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->panelVerifier:Lcom/squareup/onboarding/flow/PanelVerifier;

    iput-object p8, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->unique:Lcom/squareup/util/Unique;

    iput-object p9, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    return-void
.end method

.method public static final synthetic access$getMessages$p(Lcom/squareup/onboarding/flow/OnboardingReactor;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->messages:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getSecureScopeManager$p(Lcom/squareup/onboarding/flow/OnboardingReactor;)Lcom/squareup/secure/SecureScopeManager;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    return-object p0
.end method

.method public static final synthetic access$handleEventFromPanel(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->handleEventFromPanel(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onBackPressedFromError(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->onBackPressedFromError(Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$requestFirstStep(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/OnboardingFlow;)Lcom/squareup/onboarding/flow/OnboardingState;
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->requestFirstStep(Lcom/squareup/onboarding/OnboardingFlow;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$screenDataFromError(Lcom/squareup/onboarding/flow/OnboardingReactor;Ljava/lang/String;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingReactor;->screenDataFromError(Ljava/lang/String;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$screenDataFromSuccess(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->screenDataFromSuccess(Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method private final addScreenToHistory(Lcom/squareup/onboarding/flow/PanelHistory;Lcom/squareup/onboarding/flow/PanelScreen;)Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 4

    .line 268
    :goto_0
    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 364
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 365
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/flow/PanelScreen;

    .line 268
    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/onboard/Panel;->name:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/onboard/Panel;->name:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 269
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelHistory;->pop()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object p1

    goto :goto_0

    .line 272
    :cond_3
    invoke-virtual {p1, p2}, Lcom/squareup/onboarding/flow/PanelHistory;->plus(Lcom/squareup/onboarding/flow/PanelScreen;)Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object p1

    return-object p1
.end method

.method private final handleEventFromPanel(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
            "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 328
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->onBackPressedFromPanel(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    goto :goto_0

    .line 329
    :cond_0
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->onExitAsked(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    goto :goto_0

    .line 330
    :cond_1
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Finish;

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object p2, Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;

    invoke-direct {p1, p2}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 331
    :cond_2
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Skip;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->requestNextStep(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 332
    :cond_3
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;

    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->requestNextStepWithValidation(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 333
    :cond_4
    instance-of v0, p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;

    if-eqz v0, :cond_5

    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;->getEvent()Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->handleEventFromPanel(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final onBackPressedFromError(Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 299
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 306
    :cond_0
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;->getSessionToken()Ljava/lang/String;

    move-result-object p1

    sget-object v3, Lcom/squareup/onboarding/flow/PanelError;->Companion:Lcom/squareup/onboarding/flow/PanelError$Companion;

    invoke-virtual {v3}, Lcom/squareup/onboarding/flow/PanelError$Companion;->getNONE()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1
.end method

.method private final onBackPressedFromPanel(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 278
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    return-object p1

    .line 281
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Navigation;->can_go_back:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 283
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    return-object v0

    .line 286
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->pop()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v2

    .line 287
    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/PanelHistory;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 289
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->onExitAsked(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1

    .line 293
    :cond_2
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getSessionToken()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/squareup/onboarding/flow/OnboardingReactor;->showPanel$default(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;ILjava/lang/Object;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    return-object v0
.end method

.method private final onExitAsked(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 313
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Navigation;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    if-eqz v0, :cond_0

    .line 317
    new-instance v1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v2, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-direct {v2, p1, v0}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;-><init>(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)V

    invoke-direct {v1, v2}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 320
    :cond_0
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object v1
.end method

.method private final reactionFromState(Lcom/squareup/server/StatusResponse;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "TT;*>;>(",
            "Lcom/squareup/server/StatusResponse<",
            "TT;>;",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;>;"
        }
    .end annotation

    .line 212
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 213
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 214
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;

    invoke-direct {v0, p0, p4, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lkotlin/jvm/functions/Function1;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "successOrFailure()\n     \u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic reactionFromState$default(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/server/StatusResponse;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    const-string p3, ""

    .line 209
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/onboarding/flow/OnboardingReactor;->reactionFromState(Lcom/squareup/server/StatusResponse;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final requestFirstStep(Lcom/squareup/onboarding/OnboardingFlow;)Lcom/squareup/onboarding/flow/OnboardingState;
    .locals 4

    .line 147
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/onboarding/flow/StartFlowEvent;

    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingFlow;->getFlowName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingFlow;->getSpecVersion()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/onboarding/flow/StartFlowEvent;-><init>(Ljava/lang/String;I)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "localeProvider.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "localeProvider.get()\n        .language"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Locale.US"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    new-instance v2, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;-><init>()V

    .line 154
    iget-object v3, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->unique:Lcom/squareup/util/Unique;

    invoke-interface {v3}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    move-result-object v2

    .line 155
    invoke-static {v0}, Lcom/squareup/protos/common/countries/Country;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/countries/Country;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    move-result-object v0

    .line 156
    invoke-static {v1}, Lcom/squareup/protos/common/languages/Language;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/languages/Language;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    move-result-object v0

    .line 157
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingFlow;->getFlowName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->flow_name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    move-result-object v0

    .line 158
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingFlow;->getSpecVersion()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->spec_version(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    move-result-object p1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->build()Lcom/squareup/protos/client/onboard/StartSessionRequest;

    move-result-object p1

    const-string v0, "StartSessionRequest.Buil\u2026Version)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    sget-object v1, Lcom/squareup/onboarding/flow/PanelHistory;->Companion:Lcom/squareup/onboarding/flow/PanelHistory$Companion;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelHistory$Companion;->getEMPTY()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Lcom/squareup/protos/client/onboard/StartSessionRequest;)V

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    return-object v0

    .line 151
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final requestNextStep(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/onboarding/flow/OnboardingState;
    .locals 3

    .line 187
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    .line 188
    new-instance v1, Lcom/squareup/protos/client/onboard/NextRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;-><init>()V

    .line 189
    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->unique:Lcom/squareup/util/Unique;

    invoke-interface {v2}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;

    move-result-object v1

    .line 190
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getSessionToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;

    move-result-object p1

    .line 191
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/onboard/Panel;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->panel_name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;

    move-result-object p1

    .line 192
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->getActionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->action(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;

    move-result-object p1

    .line 193
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->getOutputs()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->outputs(Ljava/util/List;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->build()Lcom/squareup/protos/client/onboard/NextRequest;

    move-result-object p1

    const-string p2, "NextRequest.Builder()\n  \u2026outputs)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    new-instance p2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    invoke-direct {p2, v0, p1}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Lcom/squareup/protos/client/onboard/NextRequest;)V

    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState;

    return-object p2
.end method

.method private final requestNextStepWithValidation(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;)Lcom/squareup/onboarding/flow/OnboardingState;
    .locals 4

    .line 168
    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->getFromTimeout()Z

    move-result v0

    if-nez v0, :cond_2

    .line 169
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->panelVerifier:Lcom/squareup/onboarding/flow/PanelVerifier;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->getOutputs()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/onboarding/flow/PanelVerifier;->unsatisfiedValidators(Lcom/squareup/protos/client/onboard/Panel;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 171
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    .line 170
    check-cast v1, Lcom/squareup/protos/client/onboard/Validator;

    if-eqz v1, :cond_2

    .line 174
    iget-object p2, v0, Lcom/squareup/protos/client/onboard/Panel;->components:Ljava/util/List;

    const-string v0, "panel.components"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Iterable;

    .line 360
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/client/onboard/Component;

    .line 174
    iget-object v3, v2, Lcom/squareup/protos/client/onboard/Component;->validators:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string p2, "panel.components.first {\u2026idator in it.validators }"

    .line 361
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    new-instance p2, Lcom/squareup/onboarding/flow/PanelError;

    iget-object v0, v2, Lcom/squareup/protos/client/onboard/Component;->name:Ljava/lang/String;

    const-string v2, "failedComponent.name"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v1, Lcom/squareup/protos/client/onboard/Validator;->message:Ljava/lang/String;

    const-string/jumbo v2, "validator.message"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, v0, v1}, Lcom/squareup/onboarding/flow/PanelError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getSessionToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)V

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingState;

    return-object v0

    .line 361
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string p2, "Collection contains no element matching the predicate."

    invoke-direct {p1, p2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 180
    :cond_2
    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->requestNextStep(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/onboarding/flow/OnboardingState;

    move-result-object p1

    return-object p1
.end method

.method private final screenDataFromError(Ljava/lang/String;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/receiving/FailureMessage;",
            "Lcom/squareup/onboarding/flow/PanelHistory;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempting to resume from an error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, p3, p1, v2, p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    return-object v0
.end method

.method private final screenDataFromSuccess(Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;Lcom/squareup/onboarding/flow/PanelHistory;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;",
            "Lcom/squareup/onboarding/flow/PanelHistory;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 236
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->getStep()Lcom/squareup/protos/client/onboard/Step;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    if-eqz v0, :cond_1

    .line 238
    new-instance p2, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult;->Companion:Lcom/squareup/onboarding/OnboardingWorkflowResult$Companion;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->getStep()Lcom/squareup/protos/client/onboard/Step;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Step;->event:Lcom/squareup/protos/client/onboard/TerminalEvent;

    const-string v1, "payload.step.event"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/OnboardingWorkflowResult$Companion;->fromTerminalEvent(Lcom/squareup/protos/client/onboard/TerminalEvent;)Lcom/squareup/onboarding/OnboardingWorkflowResult;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Reaction;

    return-object p2

    .line 241
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->getStep()Lcom/squareup/protos/client/onboard/Step;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Step;->panel:Lcom/squareup/protos/client/onboard/Panel;

    const-string v1, "payload.step.panel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    iget-object v1, v0, Lcom/squareup/protos/client/onboard/Panel;->components:Ljava/util/List;

    const-string v2, "panel.components"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 362
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/onboard/Component;

    .line 245
    iget-object v3, v3, Lcom/squareup/protos/client/onboard/Component;->error_message:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x1

    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v3, 0x1

    :goto_1
    xor-int/2addr v3, v4

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    .line 363
    :goto_2
    check-cast v2, Lcom/squareup/protos/client/onboard/Component;

    if-eqz v2, :cond_6

    .line 246
    new-instance v1, Lcom/squareup/onboarding/flow/PanelError;

    iget-object v3, v2, Lcom/squareup/protos/client/onboard/Component;->name:Ljava/lang/String;

    const-string v4, "it.name"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v2, Lcom/squareup/protos/client/onboard/Component;->error_message:Ljava/lang/String;

    const-string v4, "it.error_message"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v3, v2}, Lcom/squareup/onboarding/flow/PanelError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 247
    :cond_6
    sget-object v1, Lcom/squareup/onboarding/flow/PanelError;->Companion:Lcom/squareup/onboarding/flow/PanelError$Companion;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/PanelError$Companion;->getNONE()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object v1

    .line 249
    :goto_3
    new-instance v2, Lcom/squareup/onboarding/flow/PanelScreen;

    invoke-direct {v2, v0}, Lcom/squareup/onboarding/flow/PanelScreen;-><init>(Lcom/squareup/protos/client/onboard/Panel;)V

    invoke-direct {p0, p2, v2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->addScreenToHistory(Lcom/squareup/onboarding/flow/PanelHistory;Lcom/squareup/onboarding/flow/PanelScreen;)Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object p2

    .line 250
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->getSessionToken()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    invoke-direct {p0, p2, p1, v1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->showPanel(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    return-object v0
.end method

.method private final showPanel(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;
    .locals 2

    .line 348
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;-><init>(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)V

    .line 349
    iget-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/onboarding/flow/PanelViewEvent;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelHistory;->getTop()Lcom/squareup/onboarding/flow/PanelScreen;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelScreen;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel;->name:Ljava/lang/String;

    const-string v1, "history.top.panel.name"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p1}, Lcom/squareup/onboarding/flow/PanelViewEvent;-><init>(Ljava/lang/String;)V

    check-cast p3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-object v0
.end method

.method static synthetic showPanel$default(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;ILjava/lang/Object;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 347
    sget-object p3, Lcom/squareup/onboarding/flow/PanelError;->Companion:Lcom/squareup/onboarding/flow/PanelError$Companion;

    invoke-virtual {p3}, Lcom/squareup/onboarding/flow/PanelError$Companion;->getNONE()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingReactor;->showPanel(Lcom/squareup/onboarding/flow/PanelHistory;Ljava/lang/String;Lcom/squareup/onboarding/flow/PanelError;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public launch(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/flow/OnboardingEvent;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 88
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    sget-object p2, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingReactor$launch$workflow$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 96
    iget-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->unique:Lcom/squareup/util/Unique;

    invoke-interface {p2}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object p2

    const-string/jumbo v0, "unique.generate()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    invoke-virtual {v0, p2}, Lcom/squareup/secure/SecureScopeManager;->enterSecureSession(Ljava/lang/String;)V

    .line 98
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    .line 99
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;

    move-result-object v1

    .line 100
    new-instance v2, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;

    invoke-direct {v2, p0, p2, v0}, Lcom/squareup/onboarding/flow/OnboardingReactor$launch$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Ljava/lang/String;Lio/reactivex/disposables/CompositeDisposable;)V

    check-cast v2, Lio/reactivex/functions/Action;

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    const-string/jumbo v1, "workflow.toCompletable()\u2026   subs.clear()\n        }"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-static {v0, p2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingReactor;->launch(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/onboarding/flow/OnboardingEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    instance-of p3, p1, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    if-eqz p3, :cond_0

    .line 114
    new-instance p1, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$1;

    invoke-direct {p1, p0}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 118
    :cond_0
    instance-of p3, p1, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    if-eqz p3, :cond_1

    .line 119
    iget-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->onboardingService:Lcom/squareup/server/onboard/OnboardingService;

    move-object p3, p1

    check-cast p3, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;

    invoke-virtual {p3}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingFirst;->getRequest()Lcom/squareup/protos/client/onboard/StartSessionRequest;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/server/onboard/OnboardingService;->startSession(Lcom/squareup/protos/client/onboard/StartSessionRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v1

    const/4 v3, 0x0

    .line 120
    sget-object p2, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$2;

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/onboarding/flow/OnboardingReactor;->reactionFromState$default(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/server/StatusResponse;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 122
    :cond_1
    instance-of p3, p1, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    if-eqz p3, :cond_2

    .line 123
    move-object p2, p1

    check-cast p2, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;->getRequest()Lcom/squareup/protos/client/onboard/NextRequest;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/protos/client/onboard/NextRequest;->session_token:Ljava/lang/String;

    const-string v0, "state.request.session_token"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor;->onboardingService:Lcom/squareup/server/onboard/OnboardingService;

    invoke-virtual {p2}, Lcom/squareup/onboarding/flow/OnboardingState$LoadingNext;->getRequest()Lcom/squareup/protos/client/onboard/NextRequest;

    move-result-object p2

    invoke-interface {v0, p2}, Lcom/squareup/server/onboard/OnboardingService;->next(Lcom/squareup/protos/client/onboard/NextRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p2

    .line 125
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$3;

    invoke-direct {v0, p3}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$3;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p2, p1, p3, v0}, Lcom/squareup/onboarding/flow/OnboardingReactor;->reactionFromState(Lcom/squareup/server/StatusResponse;Lcom/squareup/onboarding/flow/OnboardingState;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 127
    :cond_2
    instance-of p3, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    if-eqz p3, :cond_3

    .line 128
    new-instance p3, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;

    invoke-direct {p3, p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 134
    :cond_3
    instance-of p3, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingError;

    if-eqz p3, :cond_4

    .line 135
    new-instance p3, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$5;

    invoke-direct {p3, p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$5;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 139
    :cond_4
    instance-of p3, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    if-eqz p3, :cond_5

    .line 140
    new-instance p3, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$6;

    invoke-direct {p3, p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$6;-><init>(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingReactor;->onReact(Lcom/squareup/onboarding/flow/OnboardingState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
