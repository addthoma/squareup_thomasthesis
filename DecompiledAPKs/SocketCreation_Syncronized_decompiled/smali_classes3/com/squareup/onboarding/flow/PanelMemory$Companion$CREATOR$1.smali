.class public final Lcom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "PanelMemory.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/PanelMemory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/onboarding/flow/PanelMemory;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanelMemory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PanelMemory.kt\ncom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1\n+ 2 Parcels.kt\ncom/squareup/util/Parcels\n*L\n1#1,163:1\n15#2,9:164\n*E\n*S KotlinDebug\n*F\n+ 1 PanelMemory.kt\ncom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1\n*L\n120#1,9:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/onboarding/flow/PanelMemory;",
        "createFromParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/onboarding/flow/PanelMemory;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/onboarding/flow/PanelMemory;
    .locals 4

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 172
    const-class v3, Lcom/squareup/protos/client/onboard/Output;

    invoke-static {p1, v3}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 164
    :cond_0
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 120
    new-instance p1, Lcom/squareup/onboarding/flow/PanelMemory;

    invoke-direct {p1, v1}, Lcom/squareup/onboarding/flow/PanelMemory;-><init>(Ljava/util/Collection;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/onboarding/flow/PanelMemory;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/onboarding/flow/PanelMemory;
    .locals 0

    .line 122
    new-array p1, p1, [Lcom/squareup/onboarding/flow/PanelMemory;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$Companion$CREATOR$1;->newArray(I)[Lcom/squareup/onboarding/flow/PanelMemory;

    move-result-object p1

    return-object p1
.end method
