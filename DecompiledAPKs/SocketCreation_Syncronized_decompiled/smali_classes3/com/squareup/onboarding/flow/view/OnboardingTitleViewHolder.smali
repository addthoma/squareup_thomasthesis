.class public final Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingTitleViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "spacer",
        "Landroid/view/View;",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "onBindComponent",
        "",
        "component",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final spacer:Landroid/view/View;

.field private final subtitle:Lcom/squareup/widgets/MessageView;

.field private final title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_title:I

    .line 16
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 19
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->title:Lcom/squareup/widgets/MessageView;

    .line 20
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->spacer:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->spacer:Landroid/view/View;

    .line 21
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->subtitle:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;)V
    .locals 2

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->title:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;->title()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingTitleItem;->subtitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 26
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->spacer:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->title:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingTitleViewHolder;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
