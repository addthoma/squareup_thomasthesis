.class final Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;
.super Ljava/lang/Object;
.source "OnboardingRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ScreenInput"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;",
        "",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/analytics/Analytics;)V",
        "forDialog",
        "Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;",
        "getForDialog",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "forPanel",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        "getForPanel",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final forDialog:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forPanel:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/onboarding/flow/OnboardingEvent;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1;

    invoke-direct {v0, p2}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1;-><init>(Lcom/squareup/analytics/Analytics;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->forPanel:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 112
    sget-object p2, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->forDialog:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public final getForDialog()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->forDialog:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForPanel()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;->forPanel:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method
