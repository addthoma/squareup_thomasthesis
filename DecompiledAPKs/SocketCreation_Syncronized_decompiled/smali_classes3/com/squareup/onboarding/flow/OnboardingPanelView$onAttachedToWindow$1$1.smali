.class final Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;
.super Ljava/lang/Object;
.source "OnboardingPanelView.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screenData",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 157
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;

    const-string v1, "screenData"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->access$maybeShowPanel(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;

    invoke-static {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->access$maybeShowError(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;

    invoke-static {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->access$maybeUpdateTimeout(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;

    iget-object v0, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingPanelView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1$1;->accept(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    return-void
.end method
