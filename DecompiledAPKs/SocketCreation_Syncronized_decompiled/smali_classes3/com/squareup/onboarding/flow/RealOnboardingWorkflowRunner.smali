.class public final Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;
.super Lcom/squareup/container/SimpleWorkflowRunner;
.source "RealOnboardingWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingWorkflowRunner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/SimpleWorkflowRunner<",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        ">;",
        "Lcom/squareup/onboarding/OnboardingWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;",
        "Lcom/squareup/onboarding/OnboardingWorkflowRunner;",
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "viewFactory",
        "Lcom/squareup/onboarding/flow/OnboardingViewFactory;",
        "workflowStarter",
        "Lcom/squareup/onboarding/flow/Starter;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onboarding/flow/OnboardingViewFactory;Lcom/squareup/onboarding/flow/Starter;)V",
        "startWorkflow",
        "",
        "flow",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/onboarding/flow/OnboardingViewFactory;Lcom/squareup/onboarding/flow/Starter;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflowStarter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->Companion:Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 33
    move-object v4, p2

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    .line 34
    move-object v5, p3

    check-cast v5, Lcom/squareup/container/PosWorkflowStarter;

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    .line 30
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public startWorkflow(Lcom/squareup/onboarding/OnboardingFlow;)V
    .locals 1

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;->ensureWorkflow()V

    .line 45
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingEvent$StartFlow;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingEvent$StartFlow;-><init>(Lcom/squareup/onboarding/OnboardingFlow;)V

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/RealOnboardingWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
