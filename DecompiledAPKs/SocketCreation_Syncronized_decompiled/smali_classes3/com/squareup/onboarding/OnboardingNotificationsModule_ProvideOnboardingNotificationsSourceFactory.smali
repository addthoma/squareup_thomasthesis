.class public final Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;
.super Ljava/lang/Object;
.source "OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingNotificationsSourceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;->onboardingNotificationsSourceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideOnboardingNotificationsSource(Lcom/squareup/onboarding/OnboardingNotificationsSource;Lcom/squareup/settings/server/Features;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;"
        }
    .end annotation

    .line 44
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsModule;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsModule;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/onboarding/OnboardingNotificationsModule;->provideOnboardingNotificationsSource(Lcom/squareup/onboarding/OnboardingNotificationsSource;Lcom/squareup/settings/server/Features;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;->onboardingNotificationsSourceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/OnboardingNotificationsSource;

    iget-object v1, p0, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/onboarding/OnboardingNotificationsModule_ProvideOnboardingNotificationsSourceFactory;->provideOnboardingNotificationsSource(Lcom/squareup/onboarding/OnboardingNotificationsSource;Lcom/squareup/settings/server/Features;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
