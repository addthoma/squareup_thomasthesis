.class final Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/datafetch/AbstractLoader;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\u0003H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "TInput",
        "TItem",
        "",
        "input",
        "apply",
        "(Ljava/lang/Object;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $scope:Lmortar/MortarScope;

.field final synthetic this$0:Lcom/squareup/datafetch/AbstractLoader;


# direct methods
.method constructor <init>(Lcom/squareup/datafetch/AbstractLoader;Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;->$scope:Lmortar/MortarScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;->this$0:Lcom/squareup/datafetch/AbstractLoader;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;->$scope:Lmortar/MortarScope;

    invoke-static {v0, v1, p1}, Lcom/squareup/datafetch/AbstractLoader;->access$doRequest(Lcom/squareup/datafetch/AbstractLoader;Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;->apply(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
