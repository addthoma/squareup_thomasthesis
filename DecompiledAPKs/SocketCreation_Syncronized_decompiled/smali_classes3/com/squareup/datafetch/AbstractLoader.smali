.class public abstract Lcom/squareup/datafetch/AbstractLoader;
.super Ljava/lang/Object;
.source "AbstractLoader.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/datafetch/AbstractLoader$LoaderState;,
        Lcom/squareup/datafetch/AbstractLoader$Response;,
        Lcom/squareup/datafetch/AbstractLoader$PagingParams;,
        Lcom/squareup/datafetch/AbstractLoader$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lmortar/Scoped;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractLoader.kt\ncom/squareup/datafetch/AbstractLoader\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 3 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,321:1\n19#2:322\n306#3,3:323\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractLoader.kt\ncom/squareup/datafetch/AbstractLoader\n*L\n120#1:322\n170#1,3:323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0010\u0008&\u0018\u0000 :*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u00020\u0004:\u0004:;<=B+\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ/\u0010\u001e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010 0\u001f2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010$J=\u0010%\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010 0&2\u0006\u0010#\u001a\u00028\u00002\u0006\u0010\'\u001a\u00020(2\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00020+0*H$\u00a2\u0006\u0002\u0010,J\u0008\u0010-\u001a\u00020.H\u0002J\u000e\u0010#\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u001fH$J\u0015\u0010/\u001a\u00020\u00152\u0008\u00100\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u00101J\u0010\u00102\u001a\u00020\u00152\u0006\u0010!\u001a\u00020\"H\u0017J\u0008\u00103\u001a\u00020\u0015H\u0017J\u001c\u00104\u001a\u00020\u00152\u0012\u00105\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010 H\u0002J\u0008\u00106\u001a\u00020\u0015H\u0016J\u0018\u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001d0\u001fJ\u0008\u00107\u001a\u00020\u0015H\u0002J\u000e\u00108\u001a\u00020\u00152\u0006\u00100\u001a\u00020\u000fJ\u0018\u00109\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00120\u001fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0010\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0017\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000f0\u00180\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0019\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001b0\u00180\u001a0\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u001c\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001d0\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/datafetch/AbstractLoader;",
        "TInput",
        "",
        "TItem",
        "Lmortar/Scoped;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "minRefreshRateSeconds",
        "",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;J)V",
        "defaultPageSize",
        "",
        "loaderState",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState;",
        "onRecover",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "onRefresh",
        "pageSizes",
        "Lcom/squareup/util/Optional;",
        "pagingKeys",
        "Lio/reactivex/subjects/BehaviorSubject;",
        "",
        "results",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;",
        "doRequest",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "scope",
        "Lmortar/MortarScope;",
        "input",
        "(Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;",
        "fetch",
        "Lio/reactivex/Single;",
        "pagingParams",
        "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "onSubscribe",
        "Lio/reactivex/functions/Consumer;",
        "Lio/reactivex/disposables/Disposable;",
        "(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;",
        "hasFailure",
        "",
        "loadMore",
        "pageSize",
        "(Ljava/lang/Integer;)V",
        "onEnterScope",
        "onExitScope",
        "onResponse",
        "response",
        "refresh",
        "retryIfLastError",
        "setDefaultPageSize",
        "state",
        "Companion",
        "LoaderState",
        "PagingParams",
        "Response",
        "data-fetch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/datafetch/AbstractLoader$Companion;

.field public static final DEFAULT_MIN_REFRESH_RATE_SECONDS:J = 0x4L

.field public static final DEFAULT_PAGE_SIZE:I = 0x14


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private defaultPageSize:I

.field private final loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final minRefreshRateSeconds:J

.field private final onRecover:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final pageSizes:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/Integer;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final results:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation
.end field

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/datafetch/AbstractLoader$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/datafetch/AbstractLoader;->Companion:Lcom/squareup/datafetch/AbstractLoader$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;J)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/datafetch/AbstractLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-wide p4, p0, Lcom/squareup/datafetch/AbstractLoader;->minRefreshRateSeconds:J

    .line 77
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 80
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->onRecover:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 84
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 86
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 87
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/16 p1, 0x14

    .line 89
    iput p1, p0, Lcom/squareup/datafetch/AbstractLoader;->defaultPageSize:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    const-wide/16 p4, 0x4

    :cond_0
    move-wide v4, p4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 73
    invoke-direct/range {v0 .. v5}, Lcom/squareup/datafetch/AbstractLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;J)V

    return-void
.end method

.method public static final synthetic access$doRequest(Lcom/squareup/datafetch/AbstractLoader;Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/squareup/datafetch/AbstractLoader;->doRequest(Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDefaultPageSize$p(Lcom/squareup/datafetch/AbstractLoader;)I
    .locals 0

    .line 69
    iget p0, p0, Lcom/squareup/datafetch/AbstractLoader;->defaultPageSize:I

    return p0
.end method

.method public static final synthetic access$getLoaderState$p(Lcom/squareup/datafetch/AbstractLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getPagingKeys$p(Lcom/squareup/datafetch/AbstractLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 69
    iget-object p0, p0, Lcom/squareup/datafetch/AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$onResponse(Lcom/squareup/datafetch/AbstractLoader;Lcom/squareup/datafetch/AbstractLoader$Response;)V
    .locals 0

    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/datafetch/AbstractLoader;->onResponse(Lcom/squareup/datafetch/AbstractLoader$Response;)V

    return-void
.end method

.method public static final synthetic access$retryIfLastError(Lcom/squareup/datafetch/AbstractLoader;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/datafetch/AbstractLoader;->retryIfLastError()V

    return-void
.end method

.method public static final synthetic access$setDefaultPageSize$p(Lcom/squareup/datafetch/AbstractLoader;I)V
    .locals 0

    .line 69
    iput p1, p0, Lcom/squareup/datafetch/AbstractLoader;->defaultPageSize:I

    return-void
.end method

.method private final doRequest(Lmortar/MortarScope;Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "TTInput;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 167
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-static {v0}, Lio/reactivex/Observable;->switchOnNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "switchOnNext(pagingKeys)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v1}, Lio/reactivex/Observable;->switchOnNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "switchOnNext(pageSizes)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 324
    new-instance v2, Lcom/squareup/datafetch/AbstractLoader$doRequest$$inlined$zip$1;

    invoke-direct {v2}, Lcom/squareup/datafetch/AbstractLoader$doRequest$$inlined$zip$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 323
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.zip(source1, \u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 173
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 174
    new-instance v2, Lcom/squareup/datafetch/AbstractLoader$doRequest$2;

    invoke-direct {v2, p1}, Lcom/squareup/datafetch/AbstractLoader$doRequest$2;-><init>(Lmortar/MortarScope;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 175
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->onRecover:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2TransformersKt;->resubscribeWhen(Lio/reactivex/Observable;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 176
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->trampoline()Lio/reactivex/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 177
    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$doRequest$3;

    invoke-direct {v0, p0}, Lcom/squareup/datafetch/AbstractLoader$doRequest$3;-><init>(Lcom/squareup/datafetch/AbstractLoader;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 182
    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/datafetch/AbstractLoader$doRequest$4;-><init>(Lcom/squareup/datafetch/AbstractLoader;Ljava/lang/Object;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo p2, "zip(switchOnNext(pagingK\u2026              }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final hasFailure()Z
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;

    return v0
.end method

.method private final onResponse(Lcom/squareup/datafetch/AbstractLoader$Response;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "TTInput;TTItem;>;)V"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 200
    instance-of v0, p1, Lcom/squareup/datafetch/AbstractLoader$Response$Error;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response;->getInput()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v3

    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$Response$Error;

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response$Error;->getError()Lcom/squareup/datafetch/LoaderError;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;-><init>(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 203
    :cond_0
    instance-of v0, p1, Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    if-eqz v0, :cond_9

    .line 205
    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response;->getPagingParams()Lcom/squareup/datafetch/AbstractLoader$PagingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->getPagingKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 206
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 208
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 210
    :goto_2
    move-object v1, p1

    check-cast v1, Lcom/squareup/datafetch/AbstractLoader$Response$Success;

    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getFetchedItems()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v2, 0x1

    :goto_4
    if-nez v2, :cond_6

    .line 211
    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getFetchedItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getNextPagingKey()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v3, 0x1

    .line 215
    :cond_7
    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v4, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$Response;->getInput()Ljava/lang/Object;

    move-result-object p1

    invoke-direct {v4, p1, v0, v3}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;-><init>(Ljava/lang/Object;Ljava/util/List;Z)V

    invoke-virtual {v2, v4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    if-eqz v3, :cond_8

    .line 218
    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/subjects/BehaviorSubject;

    if-eqz p1, :cond_9

    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$Response$Success;->getNextPagingKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/util/Optional$Companion;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_5

    .line 220
    :cond_8
    iget-object p1, p0, Lcom/squareup/datafetch/AbstractLoader;->pagingKeys:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/subjects/BehaviorSubject;

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lio/reactivex/subjects/BehaviorSubject;->onComplete()V

    :cond_9
    :goto_5
    return-void
.end method

.method private final retryIfLastError()V
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 155
    invoke-direct {p0}, Lcom/squareup/datafetch/AbstractLoader;->hasFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->onRecover:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract fetch(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
            "Lio/reactivex/functions/Consumer<",
            "Lio/reactivex/disposables/Disposable;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation
.end method

.method protected abstract input()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "TTInput;>;"
        }
    .end annotation
.end method

.method public final loadMore(Ljava/lang/Integer;)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 145
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->pageSizes:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/squareup/datafetch/AbstractLoader;->defaultPageSize:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_0
    invoke-virtual {v1, p1}, Lcom/squareup/util/Optional$Companion;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 8

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 108
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 109
    sget-object v1, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$1;->INSTANCE:Lcom/squareup/datafetch/AbstractLoader$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 110
    new-instance v1, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$2;-><init>(Lcom/squareup/datafetch/AbstractLoader;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "connectivityMonitor.inte\u2026be { retryIfLastError() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 113
    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader;->input()Lio/reactivex/Observable;

    move-result-object v2

    .line 114
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    move-object v3, v0

    check-cast v3, Lio/reactivex/Observable;

    iget-wide v4, p0, Lcom/squareup/datafetch/AbstractLoader;->minRefreshRateSeconds:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lcom/squareup/datafetch/AbstractLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-static/range {v2 .. v7}, Lcom/squareup/util/rx2/Rx2TransformersKt;->refreshWhen(Lio/reactivex/Observable;Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    new-instance v1, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$3;-><init>(Lcom/squareup/datafetch/AbstractLoader;Lmortar/MortarScope;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    new-instance v1, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$4;

    move-object v2, p0

    check-cast v2, Lcom/squareup/datafetch/AbstractLoader;

    invoke-direct {v1, v2}, Lcom/squareup/datafetch/AbstractLoader$onEnterScope$4;-><init>(Lcom/squareup/datafetch/AbstractLoader;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/datafetch/AbstractLoader$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/datafetch/AbstractLoader$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "input()\n        .refresh\u2026 .subscribe(::onResponse)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    .line 322
    const-class v1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "loaderState\n        .ofT\u2026      .subscribe(results)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 131
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final results()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final setDefaultPageSize(I)V
    .locals 1

    if-lez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 140
    iput p1, p0, Lcom/squareup/datafetch/AbstractLoader;->defaultPageSize:I

    return-void

    .line 139
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Default page size should be positive integer."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState<",
            "TTInput;TTItem;>;>;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader;->loaderState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
