.class Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;
.super Ljava/lang/Object;
.source "SingleImagePicasso.java"

# interfaces
.implements Lcom/squareup/picasso/Picasso$RequestTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/merchantimages/SingleImagePicasso;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UriOverrider"
.end annotation


# instance fields
.field private final override:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final requestTransformer:Lcom/squareup/picasso/Picasso$RequestTransformer;


# direct methods
.method constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/picasso/Picasso$RequestTransformer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;",
            ">;",
            "Lcom/squareup/picasso/Picasso$RequestTransformer;",
            ")V"
        }
    .end annotation

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->override:Lcom/f2prateek/rx/preferences2/Preference;

    .line 86
    iput-object p2, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->requestTransformer:Lcom/squareup/picasso/Picasso$RequestTransformer;

    return-void
.end method


# virtual methods
.method public setOverride(Ljava/lang/String;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->override:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    invoke-virtual {v1, p1}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->withOverride(Ljava/lang/String;)Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public transformRequest(Lcom/squareup/picasso/Request;)Lcom/squareup/picasso/Request;
    .locals 2

    .line 94
    iget-object v0, p1, Lcom/squareup/picasso/Request;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->transformRequestUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->requestTransformer:Lcom/squareup/picasso/Picasso$RequestTransformer;

    invoke-virtual {p1}, Lcom/squareup/picasso/Request;->buildUpon()Lcom/squareup/picasso/Request$Builder;

    move-result-object p1

    .line 96
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/picasso/Request$Builder;->setUri(Landroid/net/Uri;)Lcom/squareup/picasso/Request$Builder;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/picasso/Request$Builder;->build()Lcom/squareup/picasso/Request;

    move-result-object p1

    .line 95
    invoke-interface {v1, p1}, Lcom/squareup/picasso/Picasso$RequestTransformer;->transformRequest(Lcom/squareup/picasso/Request;)Lcom/squareup/picasso/Request;

    move-result-object p1

    return-object p1
.end method

.method transformRequestUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->override:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    .line 103
    iget-object v1, v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->lastSeenUri:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->override:Lcom/f2prateek/rx/preferences2/Preference;

    new-instance v1, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 106
    :cond_0
    iget-object v1, v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->override:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    iget-object p1, v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->override:Ljava/lang/String;

    :cond_1
    :goto_0
    return-object p1
.end method
