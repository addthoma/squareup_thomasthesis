.class final Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;
.super Ljava/lang/Object;
.source "AndroidGeoAddressProvider.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/location/AndroidGeoAddressProvider;->getAddressImpl(Landroid/location/Location;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAndroidGeoAddressProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AndroidGeoAddressProvider.kt\ncom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1\n*L\n1#1,68:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/core/location/providers/AddressProvider$Result;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $geocoder:Landroid/location/Geocoder;

.field final synthetic $location:Landroid/location/Location;

.field final synthetic $locationName:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/location/Location;Landroid/location/Geocoder;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$location:Landroid/location/Location;

    iput-object p2, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$geocoder:Landroid/location/Geocoder;

    iput-object p3, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$locationName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 52
    :try_start_0
    iget-object v1, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$location:Landroid/location/Location;

    if-eqz v1, :cond_0

    .line 53
    iget-object v2, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$geocoder:Landroid/location/Geocoder;

    iget-object v1, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$location:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    iget-object v1, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$location:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$geocoder:Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$locationName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    .line 57
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->singleOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Address;

    if-eqz v1, :cond_1

    new-instance v2, Lcom/squareup/core/location/providers/AddressProvider$Result$Resolved;

    invoke-direct {v2, v1}, Lcom/squareup/core/location/providers/AddressProvider$Result$Resolved;-><init>(Landroid/location/Address;)V

    check-cast v2, Lcom/squareup/core/location/providers/AddressProvider$Result;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/core/location/providers/AddressProvider$Result$NotResolved;->INSTANCE:Lcom/squareup/core/location/providers/AddressProvider$Result$NotResolved;

    move-object v2, v1

    check-cast v2, Lcom/squareup/core/location/providers/AddressProvider$Result;

    :goto_1
    invoke-interface {p1, v2}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    .line 59
    check-cast v1, Ljava/lang/Throwable;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;->$location:Landroid/location/Location;

    aput-object v3, v0, v2

    const-string v2, "Failed to resolve address from location %s"

    invoke-static {v1, v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    sget-object v0, Lcom/squareup/core/location/providers/AddressProvider$Result$NotResolved;->INSTANCE:Lcom/squareup/core/location/providers/AddressProvider$Result$NotResolved;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    :goto_2
    return-void
.end method
