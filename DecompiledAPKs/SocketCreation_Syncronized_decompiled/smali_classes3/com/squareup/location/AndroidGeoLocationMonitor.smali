.class public Lcom/squareup/location/AndroidGeoLocationMonitor;
.super Lcom/squareup/core/location/monitors/BaseLocationMonitor;
.source "AndroidGeoLocationMonitor.java"

# interfaces
.implements Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;


# static fields
.field private static final GRACE_PERIOD:J = 0x1388L


# instance fields
.field private locationInterval:Lcom/squareup/core/location/monitors/LocationInterval;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final onLocation:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private running:Z

.field private starts:I


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/location/LocationManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/core/location/comparer/LocationComparer;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 7
    .param p7    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/core/location/providers/GpsProvider;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/core/location/providers/GpsProvider;-><init>(Landroid/content/Context;Landroid/location/LocationManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    new-instance v3, Lcom/squareup/core/location/providers/SimpleLocationProvider;

    const-string p5, "network"

    invoke-direct {v3, p1, p2, p5}, Lcom/squareup/core/location/providers/SimpleLocationProvider;-><init>(Landroid/content/Context;Landroid/location/LocationManager;Ljava/lang/String;)V

    new-instance v4, Lcom/squareup/core/location/providers/SimpleLocationProvider;

    const-string p5, "passive"

    invoke-direct {v4, p1, p2, p5}, Lcom/squareup/core/location/providers/SimpleLocationProvider;-><init>(Landroid/content/Context;Landroid/location/LocationManager;Ljava/lang/String;)V

    move-object v0, p0

    move-object v2, v6

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;-><init>(Landroid/content/Context;Lcom/squareup/core/location/providers/LocationProvider;Lcom/squareup/core/location/providers/LocationProvider;Lcom/squareup/core/location/providers/LocationProvider;Landroid/location/LocationManager;Lcom/squareup/core/location/comparer/LocationComparer;)V

    .line 40
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->onLocation:Lcom/jakewharton/rxrelay2/PublishRelay;

    const/4 p1, 0x0

    .line 45
    iput p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->starts:I

    .line 48
    iput-boolean p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    .line 60
    iput-object p3, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 61
    sget-object p1, Lcom/squareup/core/location/monitors/LocationInterval;->STANDARD:Lcom/squareup/core/location/monitors/LocationInterval;

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->locationInterval:Lcom/squareup/core/location/monitors/LocationInterval;

    return-void
.end method

.method private forceStart()V
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/location/AndroidGeoLocationMonitor;->requestLocationUpdates()V

    const/4 v0, 0x1

    .line 86
    iput-boolean v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    return-void
.end method


# virtual methods
.method protected getMinTime()J
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->locationInterval:Lcom/squareup/core/location/monitors/LocationInterval;

    invoke-virtual {v0}, Lcom/squareup/core/location/monitors/LocationInterval;->interval()J

    move-result-wide v0

    return-wide v0
.end method

.method public isRunning()Z
    .locals 1

    .line 90
    iget-boolean v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    return v0
.end method

.method protected isSingleShot()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$pause$0$AndroidGeoLocationMonitor(I)V
    .locals 1

    .line 107
    iget v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->starts:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "activity that requires location is no longer visible, killing updates."

    .line 108
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/location/AndroidGeoLocationMonitor;->removeUpdates()V

    :cond_0
    return-void
.end method

.method protected notifyLocationChanged(Landroid/location/Location;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->onLocation:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLocation()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->onLocation:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public pause()V
    .locals 5

    .line 102
    iget-boolean v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    if-nez v0, :cond_0

    return-void

    .line 104
    :cond_0
    iget v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->starts:I

    .line 105
    iget-object v1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/location/-$$Lambda$AndroidGeoLocationMonitor$7Tg-e4MKISbou1Trx7WvemZsH8Y;

    invoke-direct {v2, p0, v0}, Lcom/squareup/location/-$$Lambda$AndroidGeoLocationMonitor$7Tg-e4MKISbou1Trx7WvemZsH8Y;-><init>(Lcom/squareup/location/AndroidGeoLocationMonitor;I)V

    const-wide/16 v3, 0x1388

    invoke-interface {v1, v2, v3, v4}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected removeUpdates()V
    .locals 1

    .line 133
    invoke-super {p0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->removeUpdates()V

    const/4 v0, 0x0

    .line 134
    iput-boolean v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    return-void
.end method

.method public setInterval(Lcom/squareup/core/location/monitors/LocationInterval;)V
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->locationInterval:Lcom/squareup/core/location/monitors/LocationInterval;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 68
    invoke-virtual {p1}, Lcom/squareup/core/location/monitors/LocationInterval;->interval()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "changing location interval to %d"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->locationInterval:Lcom/squareup/core/location/monitors/LocationInterval;

    .line 72
    iget-boolean p1, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    if-eqz p1, :cond_1

    .line 73
    invoke-direct {p0}, Lcom/squareup/location/AndroidGeoLocationMonitor;->forceStart()V

    :cond_1
    return-void

    .line 66
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "interval"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public start()V
    .locals 1

    .line 78
    iget v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->starts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->starts:I

    .line 79
    iget-boolean v0, p0, Lcom/squareup/location/AndroidGeoLocationMonitor;->running:Z

    if-eqz v0, :cond_0

    return-void

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/squareup/location/AndroidGeoLocationMonitor;->forceStart()V

    return-void
.end method
