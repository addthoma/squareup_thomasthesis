.class final Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;
.super Ljava/lang/Object;
.source "RealSalesReportRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/customreport/data/RealSalesReportRepository;->salesSummary(Lcom/squareup/customreport/data/ReportConfig;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/customreport/data/SalesReportRepository$Result;",
        "Lcom/squareup/customreport/data/SalesSummaryReport;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;

    invoke-direct {v0}, Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;->INSTANCE:Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/customreport/data/SalesReportRepository$Result;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;)",
            "Lcom/squareup/customreport/data/SalesReportRepository$Result<",
            "Lcom/squareup/customreport/data/SalesSummaryReport;",
            ">;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Failure;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    goto :goto_0

    .line 134
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;

    .line 135
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    invoke-static {p1}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->access$getAggregateReport$p(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2, v1, v2}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesSummary$default(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;ILjava/lang/Object;)Lcom/squareup/customreport/data/SalesSummaryReport;

    move-result-object p1

    .line 134
    invoke-direct {v0, p1}, Lcom/squareup/customreport/data/SalesReportRepository$Result$Success;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/customreport/data/SalesReportRepository$Result;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/customreport/data/RealSalesReportRepository$salesSummary$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/customreport/data/SalesReportRepository$Result;

    move-result-object p1

    return-object p1
.end method
