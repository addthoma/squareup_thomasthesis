.class public final Lcom/squareup/customreport/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/customreport/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final cart_variation:I = 0x7f12035d

.field public static final translation_type_auto_gratuity:I = 0x7f121a15

.field public static final translation_type_cash_refund:I = 0x7f121a16

.field public static final translation_type_change:I = 0x7f121a17

.field public static final translation_type_credit:I = 0x7f121a18

.field public static final translation_type_custom_amount_item:I = 0x7f121a19

.field public static final translation_type_custom_amount_item_variation:I = 0x7f121a1a

.field public static final translation_type_default_comp_reason:I = 0x7f121a1b

.field public static final translation_type_default_device:I = 0x7f121a1c

.field public static final translation_type_default_discount:I = 0x7f121a1d

.field public static final translation_type_default_item:I = 0x7f121a1e

.field public static final translation_type_default_item_modifier:I = 0x7f121a1f

.field public static final translation_type_default_item_variation:I = 0x7f121a20

.field public static final translation_type_default_measurement_unit:I = 0x7f121a21

.field public static final translation_type_default_modifier_set:I = 0x7f121a22

.field public static final translation_type_default_order_source_name:I = 0x7f121a23

.field public static final translation_type_default_sales_tax:I = 0x7f121a24

.field public static final translation_type_default_surcharge:I = 0x7f121a25

.field public static final translation_type_default_tip:I = 0x7f121a26

.field public static final translation_type_employee_role_account_owner_role:I = 0x7f121a27

.field public static final translation_type_employee_role_mobile_staff_role:I = 0x7f121a28

.field public static final translation_type_employee_role_owner_role:I = 0x7f121a29

.field public static final translation_type_inclusive_tax:I = 0x7f121a2a

.field public static final translation_type_no_category:I = 0x7f121a2b

.field public static final translation_type_no_comp:I = 0x7f121a2c

.field public static final translation_type_no_comp_reason:I = 0x7f121a2d

.field public static final translation_type_no_contact_token_name:I = 0x7f121a2e

.field public static final translation_type_no_conversational_mode:I = 0x7f121a2f

.field public static final translation_type_no_device_credential:I = 0x7f121a30

.field public static final translation_type_no_dining_option:I = 0x7f121a31

.field public static final translation_type_no_discount:I = 0x7f121a32

.field public static final translation_type_no_employee:I = 0x7f121a33

.field public static final translation_type_no_employee_role:I = 0x7f121a34

.field public static final translation_type_no_gift_card:I = 0x7f121a35

.field public static final translation_type_no_menu:I = 0x7f121a36

.field public static final translation_type_no_mobile_staff:I = 0x7f121a37

.field public static final translation_type_no_modifier:I = 0x7f121a38

.field public static final translation_type_no_modifier_set:I = 0x7f121a39

.field public static final translation_type_no_payment_source_name:I = 0x7f121a3a

.field public static final translation_type_no_reporting_group:I = 0x7f121a3b

.field public static final translation_type_no_subunit:I = 0x7f121a3c

.field public static final translation_type_no_surcharge:I = 0x7f121a3d

.field public static final translation_type_no_tax:I = 0x7f121a3e

.field public static final translation_type_no_ticket_group:I = 0x7f121a3f

.field public static final translation_type_no_ticket_template_custom_check:I = 0x7f121a40

.field public static final translation_type_no_ticket_template_new_sales:I = 0x7f121a41

.field public static final translation_type_no_vendor:I = 0x7f121a42

.field public static final translation_type_no_void_reason:I = 0x7f121a43

.field public static final translation_type_order_source_appointments:I = 0x7f121a44

.field public static final translation_type_order_source_billing:I = 0x7f121a45

.field public static final translation_type_order_source_capital:I = 0x7f121a46

.field public static final translation_type_order_source_cash:I = 0x7f121a47

.field public static final translation_type_order_source_egifting:I = 0x7f121a48

.field public static final translation_type_order_source_external_api:I = 0x7f121a49

.field public static final translation_type_order_source_invoices:I = 0x7f121a4a

.field public static final translation_type_order_source_market:I = 0x7f121a4b

.field public static final translation_type_order_source_online_store:I = 0x7f121a4c

.field public static final translation_type_order_source_order:I = 0x7f121a4d

.field public static final translation_type_order_source_payroll:I = 0x7f121a4e

.field public static final translation_type_order_source_register:I = 0x7f121a4f

.field public static final translation_type_order_source_register_hardware:I = 0x7f121a50

.field public static final translation_type_order_source_starbucks:I = 0x7f121a51

.field public static final translation_type_order_source_store:I = 0x7f121a52

.field public static final translation_type_order_source_terminal:I = 0x7f121a53

.field public static final translation_type_order_source_white_label:I = 0x7f121a54

.field public static final translation_type_refund:I = 0x7f121a55

.field public static final translation_type_surcharge:I = 0x7f121a56

.field public static final translation_type_swedish_rounding:I = 0x7f121a57

.field public static final translation_type_ticket_group_name:I = 0x7f121a58

.field public static final translation_type_ticket_name:I = 0x7f121a59

.field public static final translation_type_unitemized_tax:I = 0x7f121a5a

.field public static final translation_type_unknown:I = 0x7f121a5b

.field public static final translation_type_unknown_payment_source_name:I = 0x7f121a5c

.field public static final translation_type_unknown_vendor:I = 0x7f121a5d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
