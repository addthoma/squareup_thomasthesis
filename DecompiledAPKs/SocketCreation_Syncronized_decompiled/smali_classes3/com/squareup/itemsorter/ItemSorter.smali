.class public final Lcom/squareup/itemsorter/ItemSorter;
.super Ljava/lang/Object;
.source "ItemSorter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;,
        Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemSorter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemSorter.kt\ncom/squareup/itemsorter/ItemSorter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,128:1\n1370#2:129\n1401#2,4:130\n1265#2,12:134\n*E\n*S KotlinDebug\n*F\n+ 1 ItemSorter.kt\ncom/squareup/itemsorter/ItemSorter\n*L\n29#1:129\n29#1,4:130\n44#1,12:134\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\u0017\u0018B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J]\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u00050\u0004\"\u0014\u0008\u0000\u0010\u0006*\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0008\u0008\u0001\u0010\u0007*\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u0006\u0010\u000b\u001a\u0002H\u00072\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010\u000eJ \u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\t2\u0006\u0010\u000c\u001a\u00020\rH\u0002JQ\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u0004\"\u0014\u0008\u0000\u0010\u0006*\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0008\u0008\u0001\u0010\u0007*\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u0006\u0010\u000b\u001a\u0002H\u00072\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a2\u0006\u0002\u0010\u000eJ\\\u0010\u0014\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u00050\u0004\"\u0014\u0008\u0000\u0010\u0006*\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0008\"\u0008\u0008\u0001\u0010\u0007*\u00020\t2\u0018\u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u00160\u00042\u0006\u0010\u000c\u001a\u00020\rH\u0002\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/itemsorter/ItemSorter;",
        "",
        "()V",
        "groupItemsByDiningOption",
        "",
        "Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;",
        "I",
        "D",
        "Lcom/squareup/itemsorter/SortableItem;",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "items",
        "cartDiningOption",
        "locale",
        "Ljava/util/Locale;",
        "(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;",
        "sameDiningOption",
        "",
        "lastDiningOption",
        "diningOption",
        "sortItemsByDiningOption",
        "splitByDiningOption",
        "sortableItemsWithDiningOptions",
        "Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;",
        "DiningOptionGroup",
        "SortableItemWithDiningOption",
        "itemsorter"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/itemsorter/ItemSorter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/itemsorter/ItemSorter;

    invoke-direct {v0}, Lcom/squareup/itemsorter/ItemSorter;-><init>()V

    sput-object v0, Lcom/squareup/itemsorter/ItemSorter;->INSTANCE:Lcom/squareup/itemsorter/ItemSorter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final groupItemsByDiningOption(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I::",
            "Lcom/squareup/itemsorter/SortableItem<",
            "TI;TD;>;D:",
            "Lcom/squareup/itemsorter/SortableDiningOption;",
            ">(",
            "Ljava/util/List<",
            "+TI;>;TD;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup<",
            "TI;TD;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "items"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartDiningOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p0, Ljava/lang/Iterable;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 131
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_0

    .line 132
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v2, Lcom/squareup/itemsorter/SortableItem;

    .line 29
    new-instance v4, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;

    invoke-direct {v4, v2, p1, v1}, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;-><init>(Lcom/squareup/itemsorter/SortableItem;Lcom/squareup/itemsorter/SortableDiningOption;I)V

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_0

    .line 133
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 30
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->sorted(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    .line 31
    sget-object p1, Lcom/squareup/itemsorter/ItemSorter;->INSTANCE:Lcom/squareup/itemsorter/ItemSorter;

    invoke-direct {p1, p0, p2}, Lcom/squareup/itemsorter/ItemSorter;->splitByDiningOption(Ljava/util/List;Ljava/util/Locale;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final sameDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Z
    .locals 2

    .line 82
    invoke-virtual {p1}, Lcom/squareup/itemsorter/SortableDiningOption;->getOrdinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/itemsorter/SortableDiningOption;->getOrdinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/itemsorter/SortableDiningOption;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    if-eqz p1, :cond_1

    invoke-virtual {p1, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/itemsorter/SortableDiningOption;->getName()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2, p3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static final sortItemsByDiningOption(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I::",
            "Lcom/squareup/itemsorter/SortableItem<",
            "TI;TD;>;D:",
            "Lcom/squareup/itemsorter/SortableDiningOption;",
            ">(",
            "Ljava/util/List<",
            "+TI;>;TD;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List<",
            "TI;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "items"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartDiningOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p0, p1, p2}, Lcom/squareup/itemsorter/ItemSorter;->groupItemsByDiningOption(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 134
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 141
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 142
    check-cast p2, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    .line 44
    invoke-virtual {p2}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->getItems()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 143
    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 145
    :cond_0
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final splitByDiningOption(Ljava/util/List;Ljava/util/Locale;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I::",
            "Lcom/squareup/itemsorter/SortableItem<",
            "TI;TD;>;D:",
            "Lcom/squareup/itemsorter/SortableDiningOption;",
            ">(",
            "Ljava/util/List<",
            "Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption<",
            "TI;TD;>;>;",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup<",
            "TI;TD;>;>;"
        }
    .end annotation

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    const/4 v2, 0x0

    .line 54
    check-cast v2, Lcom/squareup/itemsorter/SortableDiningOption;

    .line 55
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;

    if-eqz v2, :cond_0

    .line 58
    invoke-virtual {v3}, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->getDiningOption$itemsorter()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object v4

    invoke-direct {p0, v2, v4, p2}, Lcom/squareup/itemsorter/ItemSorter;->sameDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 59
    new-instance v4, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    invoke-direct {v4, v2, v1}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;-><init>(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 65
    :cond_0
    invoke-virtual {v3}, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->getItem$itemsorter()Lcom/squareup/itemsorter/SortableItem;

    move-result-object v2

    .line 66
    invoke-virtual {v3}, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->getDiningOption$itemsorter()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/squareup/itemsorter/SortableItem;->setSelectedDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;

    move-result-object v2

    .line 67
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-virtual {v3}, Lcom/squareup/itemsorter/ItemSorter$SortableItemWithDiningOption;->getDiningOption$itemsorter()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object v2

    goto :goto_0

    .line 70
    :cond_1
    move-object p1, v1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_2

    .line 71
    new-instance p1, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    invoke-direct {p1, v2, v1}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;-><init>(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method
