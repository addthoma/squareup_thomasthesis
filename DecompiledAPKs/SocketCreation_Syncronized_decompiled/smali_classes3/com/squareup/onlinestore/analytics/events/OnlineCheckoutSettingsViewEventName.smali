.class public final enum Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;
.super Ljava/lang/Enum;
.source "OnlineCheckoutSettingsEvents.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "ACCOUNT_INELIGIBLE_ERROR",
        "ACTIONS",
        "CREATE_PAY",
        "FETCHING_ACCOUNT_STATUS_ERROR",
        "VIEW_DONATION_LINK",
        "VIEW_LINK_LIST",
        "VIEW_SHARE_PAY_LINK",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum ACCOUNT_INELIGIBLE_ERROR:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum ACTIONS:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum CREATE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum FETCHING_ACCOUNT_STATUS_ERROR:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum VIEW_DONATION_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum VIEW_LINK_LIST:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

.field public static final enum VIEW_SHARE_PAY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x0

    const-string v3, "ACCOUNT_INELIGIBLE_ERROR"

    const-string v4, "SPOS Settings: Online Checkout: View Link List Error: Account Ineligible"

    .line 29
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->ACCOUNT_INELIGIBLE_ERROR:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x1

    const-string v3, "ACTIONS"

    const-string v4, "SPOS Settings: Online Checkout: Home: Actions"

    .line 32
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->ACTIONS:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x2

    const-string v3, "CREATE_PAY"

    const-string v4, "SPOS Settings: Online Checkout: Home: Create Pay"

    .line 33
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->CREATE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x3

    const-string v3, "FETCHING_ACCOUNT_STATUS_ERROR"

    const-string v4, "SPOS Settings: Online Checkout: View Link List Error: Fetching Account Status"

    .line 34
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->FETCHING_ACCOUNT_STATUS_ERROR:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x4

    const-string v3, "VIEW_DONATION_LINK"

    const-string v4, "SPOS Settings: Online Checkout: Home: View Donation Link"

    .line 37
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->VIEW_DONATION_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x5

    const-string v3, "VIEW_LINK_LIST"

    const-string v4, "SPOS Settings: Online Checkout: View Link List"

    .line 38
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->VIEW_LINK_LIST:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    const/4 v2, 0x6

    const-string v3, "VIEW_SHARE_PAY_LINK"

    const-string v4, "SPOS Settings: Online Checkout: Home: View Share Pay Link"

    .line 39
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->VIEW_SHARE_PAY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;
    .locals 1

    const-class v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;
    .locals 1

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    invoke-virtual {v0}, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsViewEventName;->value:Ljava/lang/String;

    return-object v0
.end method
