.class public final Lcom/squareup/onlinestore/ui/util/NohoEditRowUtilKt;
.super Ljava/lang/Object;
.source "NohoEditRowUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "moveCursorPosToEnd",
        "",
        "Lcom/squareup/noho/NohoEditRow;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final moveCursorPosToEnd(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "$this$moveCursorPosToEnd"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-static {}, Lcom/squareup/onlinestore/ui/util/UtilKt;->isUsingRtlText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->setSelection(I)V

    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->setSelection(I)V

    :goto_0
    return-void
.end method
