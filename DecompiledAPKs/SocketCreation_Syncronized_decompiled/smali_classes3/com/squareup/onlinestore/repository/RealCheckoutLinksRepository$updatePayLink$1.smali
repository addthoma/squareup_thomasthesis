.class final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->updatePayLink(Ljava/lang/String;Lcom/squareup/onlinestore/repository/PayLink;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckoutLinksRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckoutLinksRepository.kt\ncom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1\n*L\n1#1,350:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $payLink:Lcom/squareup/onlinestore/repository/PayLink;

.field final synthetic this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/onlinestore/repository/PayLink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    iput-object p2, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;->$payLink:Lcom/squareup/onlinestore/repository/PayLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;

    .line 187
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;->$payLink:Lcom/squareup/onlinestore/repository/PayLink;

    .line 188
    instance-of v1, v0, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    if-eqz v1, :cond_3

    .line 189
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getAmountMoney()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getHasBuyerControlledPrice()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    new-instance v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$Success;

    .line 195
    new-instance v1, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getName()Ljava/lang/String;

    move-result-object v3

    .line 197
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getAmountCurrency()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getAmountMoney()Ljava/lang/Long;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getEnabled()Z

    move-result p1

    .line 195
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v1, Lcom/squareup/onlinestore/repository/PayLink;

    .line 194
    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$Success;-><init>(Lcom/squareup/onlinestore/repository/PayLink;)V

    goto :goto_1

    .line 191
    :cond_2
    :goto_0
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    return-object p1

    .line 201
    :cond_3
    instance-of v0, v0, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    if-eqz v0, :cond_5

    .line 202
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getHasBuyerControlledPrice()Z

    move-result v0

    if-nez v0, :cond_4

    .line 204
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    return-object p1

    .line 207
    :cond_4
    new-instance v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$Success;

    new-instance v1, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getEnabled()Z

    move-result p1

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/onlinestore/repository/PayLink$Donation;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v1, Lcom/squareup/onlinestore/repository/PayLink;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$Success;-><init>(Lcom/squareup/onlinestore/repository/PayLink;)V

    .line 186
    :goto_1
    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    goto :goto_2

    .line 207
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 211
    :cond_6
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->access$processUpdatePayLinkError(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    move-result-object p1

    return-object p1
.end method
