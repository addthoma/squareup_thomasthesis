.class final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantExists()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;

    invoke-direct {v0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;->INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_2

    .line 314
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;

    .line 315
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;->getExists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Exists;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Exists;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;

    goto :goto_0

    .line 317
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;->getExists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;->getEligible()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 318
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Eligible;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Eligible;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;

    goto :goto_0

    .line 320
    :cond_1
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$NotEligible;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$NotEligible;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;

    goto :goto_0

    .line 323
    :cond_2
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Error;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Error;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;

    move-result-object p1

    return-object p1
.end method
