.class final Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$1;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutBuyLinkWorkflow.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "kotlin.jvm.PlatformType",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Z
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    iget-object v0, v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;

    iget-object v1, v1, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$enableCheckoutLinksWorker$1;->$merchantCatalogObjectToken:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;->access$enableEcomAvailable(Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
