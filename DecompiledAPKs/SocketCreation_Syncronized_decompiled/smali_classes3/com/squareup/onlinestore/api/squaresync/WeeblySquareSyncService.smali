.class public interface abstract Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;
.super Ljava/lang/Object;
.source "WeeblySquareSyncService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService$MultiPassSessionAuthResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001&J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'J\"\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000cH\'J2\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u0011\u001a\u00020\n2\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0013H\'J\u0018\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00072\u0008\u0008\u0001\u0010\u0016\u001a\u00020\nH\'J6\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00072\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u0019\u001a\u00020\n2\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u001b2\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u001bH\'J\u0018\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u00072\u0008\u0008\u0001\u0010\u001f\u001a\u00020 H\'J2\u0010!\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\"\u001a\u00020\n2\u0008\u0008\u0001\u0010#\u001a\u00020$H\'J,\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u0011\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000cH\'\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
        "",
        "authenticateMultiPassSession",
        "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService$MultiPassSessionAuthResponse;",
        "multiPassAuthRequestBody",
        "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;",
        "createPayLink",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
        "jsonWebToken",
        "",
        "payLinkReqBody",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;",
        "deletePayLink",
        "Lio/reactivex/Single;",
        "Lretrofit2/Response;",
        "Ljava/lang/Void;",
        "payLinkId",
        "deletePayLinkReqBody",
        "Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;",
        "getMerchant",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;",
        "id",
        "getPayLinks",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;",
        "merchantId",
        "page",
        "",
        "perPage",
        "merchantExists",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;",
        "merchantExistsReqBody",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;",
        "syncItemWithWeebly",
        "itemId",
        "enableEcomAvailableRequestBody",
        "Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;",
        "updatePayLink",
        "MultiPassSessionAuthResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract authenticateMultiPassSession(Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;)Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService$MultiPassSessionAuthResponse;
    .param p1    # Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthRequestBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/app/square-sync/api/sqs"
    .end annotation
.end method

.method public abstract createPayLink(Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/app/square-sync/api/sq-dashboard/pay-links"
    .end annotation
.end method

.method public abstract deletePayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "payLinkId"
        .end annotation
    .end param
    .param p3    # Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;",
            ")",
            "Lio/reactivex/Single<",
            "Lretrofit2/Response<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/HTTP;
        hasBody = true
        method = "DELETE"
        path = "/app/square-sync/api/sq-dashboard/pay-links/{payLinkId}"
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation
.end method

.method public abstract getMerchant(Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/app/square-sync/api/merchant/{id}"
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json"
        }
    .end annotation
.end method

.method public abstract getPayLinks(Ljava/lang/String;Ljava/lang/String;II)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "square_merchant_id"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lretrofit2/http/Query;
            value = "page"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lretrofit2/http/Query;
            value = "per_page"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/app/square-sync/api/sq-dashboard/pay-links"
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json"
        }
    .end annotation
.end method

.method public abstract merchantExists(Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsReqBody;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantExistsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json",
            "Cache-Control: no-cache"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/app/square-sync/api/sqs/exists"
    .end annotation
.end method

.method public abstract syncItemWithWeebly(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "itemId"
        .end annotation
    .end param
    .param p3    # Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;",
            ")",
            "Lio/reactivex/Single<",
            "Lretrofit2/Response<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "/app/square-sync/api/sq-dashboard/sync-item/{itemId}"
    .end annotation
.end method

.method public abstract updatePayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "payLinkId"
        .end annotation
    .end param
    .param p3    # Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "/app/square-sync/api/sq-dashboard/pay-links/{payLinkId}"
    .end annotation
.end method
