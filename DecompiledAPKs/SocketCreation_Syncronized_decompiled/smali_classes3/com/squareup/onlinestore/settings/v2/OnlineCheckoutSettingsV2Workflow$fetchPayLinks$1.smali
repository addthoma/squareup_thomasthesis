.class final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsV2Workflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->fetchPayLinks(I)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;",
        "kotlin.jvm.PlatformType",
        "tokenResult",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $page:I

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    iput p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->$page:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "tokenResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 593
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object v0

    .line 596
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;->getJsonWebToken()Ljava/lang/String;

    move-result-object p1

    .line 597
    iget v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->$page:I

    const/16 v2, 0xa

    .line 599
    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    invoke-static {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->access$getOnlineStoreRestrictions$p(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    move-result-object v3

    .line 600
    invoke-interface {v3}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isOnlineCheckoutSettingsDonationsEnabled()Z

    move-result v3

    .line 595
    invoke-interface {v0, p1, v1, v2, v3}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getPayLinks(Ljava/lang/String;IIZ)Lio/reactivex/Single;

    move-result-object p1

    .line 602
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1$1;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$sam$io_reactivex_functions_Function$0;

    invoke-direct {v1, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository\n\u2026(::processPayLinksResult)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 604
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;

    const-string v1, "Single.just(FetchCheckoutLinksResult.Error)"

    if-eqz v0, :cond_1

    .line 612
    sget-object p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Error;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Error;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 614
    :cond_1
    instance-of p1, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;

    if-eqz p1, :cond_2

    .line 615
    sget-object p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Error;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Error;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
