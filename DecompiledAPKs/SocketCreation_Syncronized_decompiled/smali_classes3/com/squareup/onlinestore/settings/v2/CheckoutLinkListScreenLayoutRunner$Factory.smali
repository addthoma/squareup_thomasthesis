.class public final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "CheckoutLinkListScreenLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/text/Formatter;)V",
        "create",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;

    .line 312
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 311
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/text/Formatter;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method
