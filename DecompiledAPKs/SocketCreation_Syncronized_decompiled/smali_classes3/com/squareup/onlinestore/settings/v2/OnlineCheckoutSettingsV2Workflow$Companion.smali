.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002R\u0016\u0010\u0006\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0007\u0010\u0002R\u0016\u0010\u0008\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\t\u0010\u0002R\u0016\u0010\n\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u000b\u0010\u0002R\u0016\u0010\u000c\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\r\u0010\u0002R\u0016\u0010\u000e\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u000f\u0010\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082T\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0013\u0010\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;",
        "",
        "()V",
        "CHECKOUT_LINK_LIST_WORKER_KEY",
        "",
        "CHECKOUT_LINK_LIST_WORKER_KEY$annotations",
        "INITIAL_LOADING_WORKER_KEY",
        "INITIAL_LOADING_WORKER_KEY$annotations",
        "LOG_ACTIONS_WORKER_KEY",
        "LOG_ACTIONS_WORKER_KEY$annotations",
        "LOG_INELIGIBLE_KEY",
        "LOG_INELIGIBLE_KEY$annotations",
        "LOG_LINK_LIST_ERROR_KEY",
        "LOG_LINK_LIST_ERROR_KEY$annotations",
        "LOG_VIEW_LINK_LIST_KEY",
        "LOG_VIEW_LINK_LIST_KEY$annotations",
        "PAY_LINKS_PER_PAGE_COUNT",
        "",
        "UPDATE_LINK_STATUS_WORKER_KEY",
        "UPDATE_LINK_STATUS_WORKER_KEY$annotations",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 703
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;-><init>()V

    return-void
.end method

.method public static synthetic CHECKOUT_LINK_LIST_WORKER_KEY$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic INITIAL_LOADING_WORKER_KEY$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic LOG_ACTIONS_WORKER_KEY$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic LOG_INELIGIBLE_KEY$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic LOG_LINK_LIST_ERROR_KEY$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic LOG_VIEW_LINK_LIST_KEY$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic UPDATE_LINK_STATUS_WORKER_KEY$annotations()V
    .locals 0

    return-void
.end method
