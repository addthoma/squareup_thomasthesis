.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "EditLinkScreenLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B9\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;",
        "",
        "moneyScrubber",
        "Lcom/squareup/text/SelectableTextScrubber;",
        "moneyDigitsKeyListener",
        "Landroid/text/method/DigitsKeyListener;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "onlineStoreRestrictions",
        "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
        "(Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V",
        "create",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyDigitsKeyListener:Landroid/text/method/DigitsKeyListener;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyScrubber:Lcom/squareup/text/SelectableTextScrubber;

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;


# direct methods
.method public constructor <init>(Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V
    .locals 1
    .param p1    # Lcom/squareup/text/SelectableTextScrubber;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .param p2    # Landroid/text/method/DigitsKeyListener;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/SelectableTextScrubber;",
            "Landroid/text/method/DigitsKeyListener;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyScrubber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyDigitsKeyListener"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineStoreRestrictions"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->moneyScrubber:Lcom/squareup/text/SelectableTextScrubber;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->moneyDigitsKeyListener:Landroid/text/method/DigitsKeyListener;

    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;
    .locals 8

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;

    .line 281
    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->moneyScrubber:Lcom/squareup/text/SelectableTextScrubber;

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->moneyDigitsKeyListener:Landroid/text/method/DigitsKeyListener;

    iget-object v5, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v6, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 282
    iget-object v7, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    move-object v1, v0

    move-object v2, p1

    .line 280
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V

    return-object v0
.end method
