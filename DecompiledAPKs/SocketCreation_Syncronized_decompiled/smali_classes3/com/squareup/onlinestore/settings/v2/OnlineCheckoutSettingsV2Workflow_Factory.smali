.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsV2Workflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clipboard;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clipboard;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 62
    iput-object p11, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg10Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clipboard;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;"
        }
    .end annotation

    .line 77
    new-instance v12, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/http/Server;Lcom/squareup/util/Res;Lcom/squareup/util/Clipboard;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;
    .locals 13

    .line 84
    new-instance v12, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;-><init>(Lcom/squareup/http/Server;Lcom/squareup/util/Res;Lcom/squareup/util/Clipboard;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;
    .locals 12

    .line 67
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/http/Server;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Clipboard;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-static/range {v1 .. v11}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->newInstance(Lcom/squareup/http/Server;Lcom/squareup/util/Res;Lcom/squareup/util/Clipboard;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow_Factory;->get()Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    move-result-object v0

    return-object v0
.end method
