.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "EditLinkWorkflowViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "createLinkScreenLayoutRunnerFactory",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;",
        "(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "createLinkScreenLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Binding;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
