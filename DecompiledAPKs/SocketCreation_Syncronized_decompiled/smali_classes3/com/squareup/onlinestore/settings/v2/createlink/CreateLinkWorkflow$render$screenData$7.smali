.class final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "+",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
        "it",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->$state:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$Success;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 117
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$AlreadyExistsError;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$2;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$2;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_1
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$FeatureNotAvailable;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    sget-object v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$3;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$3;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 123
    :cond_2
    instance-of p1, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult$OtherError;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$4;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7$4;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;->invoke(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
