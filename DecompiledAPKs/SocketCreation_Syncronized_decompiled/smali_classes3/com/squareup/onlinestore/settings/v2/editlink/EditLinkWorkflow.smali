.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditLinkWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditLinkWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditLinkWorkflow.kt\ncom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,172:1\n32#2,12:173\n149#3,5:185\n85#4:190\n240#5:191\n276#6:192\n*E\n*S KotlinDebug\n*F\n+ 1 EditLinkWorkflow.kt\ncom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow\n*L\n48#1,12:173\n135#1,5:185\n152#1:190\n152#1:191\n152#1:192\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u001f2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u001fB\u0017\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00022\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016JN\u0010\u0013\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00032\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0003H\u0016J\u0016\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u000c\u0010\u001d\u001a\u00020\u001e*\u00020\u001cH\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "checkoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "updatePayLink",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
        "editLinkInfo",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
        "toPayLink",
        "Lcom/squareup/onlinestore/repository/PayLink;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$Companion;

.field public static final EDIT_PAY_LINK_WORKER_KEY:Ljava/lang/String; = "edit-pay-link-worker-key"


# instance fields
.field private final analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->Companion:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "checkoutLinksRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    return-object p0
.end method

.method public static final synthetic access$toPayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/onlinestore/repository/PayLink;
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->toPayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/onlinestore/repository/PayLink;

    move-result-object p0

    return-object p0
.end method

.method private final toPayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/onlinestore/repository/PayLink;
    .locals 4

    .line 155
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    .line 156
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->getId()Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 159
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;->getEnabled()Z

    move-result p1

    .line 155
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v0, Lcom/squareup/onlinestore/repository/PayLink;

    goto :goto_0

    .line 161
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    .line 162
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;->getId()Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;->getEnabled()Z

    move-result p1

    .line 161
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/onlinestore/repository/PayLink$Donation;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v0, Lcom/squareup/onlinestore/repository/PayLink;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final updatePayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 144
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$1;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository.\u2026rError)\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$updatePayLink$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 191
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 192
    const-class v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method


# virtual methods
.method public initialState(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 173
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 178
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 180
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 181
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 182
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 184
    :cond_3
    check-cast v2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;

    if-eqz v2, :cond_4

    goto :goto_3

    .line 48
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object p1

    .line 50
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz p2, :cond_5

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result p1

    invoke-direct {p2, v0, v1, v2, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    goto :goto_2

    .line 51
    :cond_5
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz p2, :cond_6

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result p1

    invoke-direct {p2, v0, v1, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    .line 53
    :goto_2
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$EditLinkState;

    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$EditLinkState;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V

    .line 48
    move-object v2, p1

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;

    :goto_3
    return-object v2

    .line 51
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->initialState(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->render(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$EditLinkState;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    .line 63
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$EditLinkState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$EditLinkState;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p2

    .line 64
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 67
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$2;

    invoke-direct {v1, p0, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$2;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 62
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;

    goto :goto_0

    .line 71
    :cond_0
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkAlreadyExistsErrorState;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;

    .line 72
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkAlreadyExistsErrorState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkAlreadyExistsErrorState;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p2

    .line 73
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$3;

    invoke-direct {v0, p0, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$3;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 76
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$4;

    invoke-direct {v1, p0, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$4;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 71
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;

    goto :goto_0

    .line 80
    :cond_1
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkCreationFailedErrorState;

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;

    .line 81
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkCreationFailedErrorState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkCreationFailedErrorState;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p2

    .line 82
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$5;

    invoke-direct {v0, p0, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$5;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 85
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$6;

    invoke-direct {v1, p0, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$6;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 80
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$LinkCreationFailedErrorScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;

    goto :goto_0

    .line 89
    :cond_2
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$SaveLinkState;

    if-eqz p1, :cond_3

    .line 90
    move-object p1, p2

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$SaveLinkState;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$SaveLinkState;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->updatePayLink(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;

    invoke-direct {v0, p0, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v1, "edit-pay-link-worker-key"

    invoke-interface {p3, p1, v1, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 125
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;

    .line 126
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$8;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$8;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 125
    invoke-direct {p1, v0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$SaveLinkScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;

    .line 135
    :goto_0
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 186
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 187
    const-class p3, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 188
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 186
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 136
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 125
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->snapshotState(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
