.class public final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;
.super Ljava/lang/Object;
.source "FrontOfTransactionCheckInState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;",
        "",
        "contactAddedToSale",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "cartRewardState",
        "Lcom/squareup/loyalty/CartRewardState;",
        "(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;)V",
        "getCartRewardState",
        "()Lcom/squareup/loyalty/CartRewardState;",
        "getContactAddedToSale",
        "()Lcom/squareup/protos/client/rolodex/Contact;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cartRewardState:Lcom/squareup/loyalty/CartRewardState;

.field private final contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;)V
    .locals 1

    const-string v0, "cartRewardState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;ILjava/lang/Object;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->copy(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public final component2()Lcom/squareup/loyalty/CartRewardState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;
    .locals 1

    const-string v0, "cartRewardState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;

    invoke-direct {v0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;-><init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/CartRewardState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    iget-object p1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCartRewardState()Lcom/squareup/loyalty/CartRewardState;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    return-object v0
.end method

.method public final getContactAddedToSale()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FrontOfTransactionCheckInInput(contactAddedToSale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->contactAddedToSale:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cartRewardState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInInput;->cartRewardState:Lcom/squareup/loyalty/CartRewardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
