.class public final Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;
.super Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;
.source "LoyaltyBuyerCartBannerFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoBanner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;",
        "()V",
        "toProto",
        "Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;

    invoke-direct {v0}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;-><init>()V

    sput-object v0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;->INSTANCE:Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public toProto()Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
