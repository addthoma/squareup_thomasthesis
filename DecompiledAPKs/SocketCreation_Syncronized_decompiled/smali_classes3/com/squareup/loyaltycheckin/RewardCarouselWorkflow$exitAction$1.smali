.class final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$exitAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;-><init>(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Mutator<",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        ">;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$exitAction$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            ">;)",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    iget-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$exitAction$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    invoke-static {p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getAnalytics$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ExitRewardCarousel;->INSTANCE:Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ExitRewardCarousel;

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 248
    sget-object p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$exitAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$Exit;

    move-result-object p1

    return-object p1
.end method
