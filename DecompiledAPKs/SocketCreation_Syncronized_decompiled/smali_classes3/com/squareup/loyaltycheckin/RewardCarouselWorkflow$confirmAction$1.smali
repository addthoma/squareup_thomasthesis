.class final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->confirmAction(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyalty/CartRewardState$RewardTierState;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "-",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $difference:I

.field final synthetic $props:Lcom/squareup/loyaltycheckin/RewardCarouselProps;

.field final synthetic $removableCoupons:Ljava/util/List;

.field final synthetic $rewardTierState:Lcom/squareup/loyalty/CartRewardState$RewardTierState;


# direct methods
.method constructor <init>(ILcom/squareup/loyalty/CartRewardState$RewardTierState;Ljava/util/List;Lcom/squareup/loyaltycheckin/RewardCarouselProps;)V
    .locals 0

    iput p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$difference:I

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$rewardTierState:Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    iput-object p3, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$removableCoupons:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$props:Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    sget-object v0, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    .line 224
    iget v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$difference:I

    neg-int v0, v0

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$rewardTierState:Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    invoke-virtual {v1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v2, v1

    mul-int v0, v0, v2

    .line 226
    new-instance v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$RemoveCoupon;

    .line 227
    iget-object v2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$removableCoupons:Ljava/util/List;

    iget v3, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$difference:I

    neg-int v3, v3

    const/4 v4, 0x0

    invoke-interface {v2, v4, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 228
    iget-object v3, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;->$props:Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    invoke-virtual {v3}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPoints()I

    move-result v3

    add-int/2addr v3, v0

    .line 226
    invoke-direct {v1, v2, v3}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$RemoveCoupon;-><init>(Ljava/util/List;I)V

    .line 225
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
