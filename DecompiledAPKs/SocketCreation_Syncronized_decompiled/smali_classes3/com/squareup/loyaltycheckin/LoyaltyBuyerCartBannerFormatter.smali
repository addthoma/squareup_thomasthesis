.class public interface abstract Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBannerFormatter;
.super Ljava/lang/Object;
.source "LoyaltyBuyerCartBannerFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBannerFormatter;",
        "",
        "banner",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract banner()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;",
            ">;"
        }
    .end annotation
.end method
