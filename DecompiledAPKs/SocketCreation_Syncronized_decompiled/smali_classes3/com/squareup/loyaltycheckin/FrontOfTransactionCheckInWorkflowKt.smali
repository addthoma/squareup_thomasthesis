.class public final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;
.super Ljava/lang/Object;
.source "FrontOfTransactionCheckInWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u0014\u0010\u0005\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0003\"\u0014\u0010\u0007\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0003\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u0014\u0010\n\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0003\"\u0014\u0010\u000c\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u0003\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000*\"\u0010\u000f\"\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u00102\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\u0010\u00a8\u0006\u0013"
    }
    d2 = {
        "CHECKED_IN_INACTIVITY_AUTOCLOSE_MS",
        "",
        "getCHECKED_IN_INACTIVITY_AUTOCLOSE_MS",
        "()J",
        "CHECKED_IN_INACTIVITY_AUTOCLOSE_S",
        "ERROR_AUTOCLOSE_MS",
        "getERROR_AUTOCLOSE_MS",
        "PHONE_INACTIVITY_AUTOCLOSE_MS",
        "getPHONE_INACTIVITY_AUTOCLOSE_MS",
        "PHONE_INACTIVITY_AUTOCLOSE_S",
        "REDEEMED_REWARD_AUTOCLOSE_MS",
        "getREDEEMED_REWARD_AUTOCLOSE_MS",
        "WELCOME_AUTOCLOSE_MS",
        "getWELCOME_AUTOCLOSE_MS",
        "WELCOME_AUTOCLOSE_S",
        "Action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CHECKED_IN_INACTIVITY_AUTOCLOSE_MS:J

.field private static final CHECKED_IN_INACTIVITY_AUTOCLOSE_S:J = 0xfL

.field private static final ERROR_AUTOCLOSE_MS:J

.field private static final PHONE_INACTIVITY_AUTOCLOSE_MS:J

.field private static final PHONE_INACTIVITY_AUTOCLOSE_S:J = 0x3cL

.field private static final REDEEMED_REWARD_AUTOCLOSE_MS:J

.field private static final WELCOME_AUTOCLOSE_MS:J

.field private static final WELCOME_AUTOCLOSE_S:J = 0x3L


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 51
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->WELCOME_AUTOCLOSE_MS:J

    .line 52
    sget-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->WELCOME_AUTOCLOSE_MS:J

    sput-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->REDEEMED_REWARD_AUTOCLOSE_MS:J

    .line 53
    sput-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->ERROR_AUTOCLOSE_MS:J

    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->PHONE_INACTIVITY_AUTOCLOSE_MS:J

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->CHECKED_IN_INACTIVITY_AUTOCLOSE_MS:J

    return-void
.end method

.method public static final getCHECKED_IN_INACTIVITY_AUTOCLOSE_MS()J
    .locals 2

    .line 55
    sget-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->CHECKED_IN_INACTIVITY_AUTOCLOSE_MS:J

    return-wide v0
.end method

.method public static final getERROR_AUTOCLOSE_MS()J
    .locals 2

    .line 53
    sget-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->ERROR_AUTOCLOSE_MS:J

    return-wide v0
.end method

.method public static final getPHONE_INACTIVITY_AUTOCLOSE_MS()J
    .locals 2

    .line 54
    sget-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->PHONE_INACTIVITY_AUTOCLOSE_MS:J

    return-wide v0
.end method

.method public static final getREDEEMED_REWARD_AUTOCLOSE_MS()J
    .locals 2

    .line 52
    sget-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->REDEEMED_REWARD_AUTOCLOSE_MS:J

    return-wide v0
.end method

.method public static final getWELCOME_AUTOCLOSE_MS()J
    .locals 2

    .line 51
    sget-wide v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->WELCOME_AUTOCLOSE_MS:J

    return-wide v0
.end method
