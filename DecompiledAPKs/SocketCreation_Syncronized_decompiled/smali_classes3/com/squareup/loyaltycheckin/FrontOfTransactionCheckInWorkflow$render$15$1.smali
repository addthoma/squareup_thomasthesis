.class final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;
.super Lkotlin/jvm/internal/Lambda;
.source "FrontOfTransactionCheckInWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;->invoke(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "-",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $output:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

.field final synthetic this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->$output:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->$output:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    instance-of v0, v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$RemoveCoupon;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;

    iget-object v0, v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;->$state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->this$0:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;

    iget-object v1, v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15;->$state:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState;

    check-cast v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;->getData()Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->$output:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    check-cast v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$RemoveCoupon;

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$RemoveCoupon;->getNewPointsTotal()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/squareup/loyaltycheckin/CheckedInData;->copy$default(Lcom/squareup/loyaltycheckin/CheckedInData;Ljava/lang/String;ILcom/squareup/protos/client/rolodex/Contact;ILjava/lang/Object;)Lcom/squareup/loyaltycheckin/CheckedInData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;->copy(Lcom/squareup/loyaltycheckin/CheckedInData;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInState$RewardCarousel;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflow$render$15$1;->$output:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
