.class final Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRemoteMessagesStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/communications/Message;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRemoteMessagesStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRemoteMessagesStore.kt\ncom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,65:1\n1360#2:66\n1429#2,3:67\n*E\n*S KotlinDebug\n*F\n+ 1 RealRemoteMessagesStore.kt\ncom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1\n*L\n53#1:66\n53#1,3:67\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/communications/Message;",
        "response",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;

    invoke-direct {v0}, Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;-><init>()V

    sput-object v0, Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;->INSTANCE:Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1$1;->invoke(Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object p1, p1, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;->message_units:Ljava/util/List;

    const-string v0, "response.message_units"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 67
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 68
    check-cast v1, Lcom/squareup/protos/messageservice/service/MessageUnit;

    const-string v2, "messageUnit"

    .line 54
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/communications/utils/MessageUnitsKt;->toMessage(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
