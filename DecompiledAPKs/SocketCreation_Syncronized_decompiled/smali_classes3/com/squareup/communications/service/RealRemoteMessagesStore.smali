.class public final Lcom/squareup/communications/service/RealRemoteMessagesStore;
.super Ljava/lang/Object;
.source "RealRemoteMessagesStore.kt"

# interfaces
.implements Lcom/squareup/communications/service/RemoteMessagesStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/communications/service/RealRemoteMessagesStore$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\"\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c0\u000b0\u00082\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\"\u0010\u0010\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c0\u000b0\u00112\u0006\u0010\u000e\u001a\u00020\u000fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/communications/service/RealRemoteMessagesStore;",
        "Lcom/squareup/communications/service/RemoteMessagesStore;",
        "service",
        "Lcom/squareup/communications/service/CommunicationsService;",
        "notifier",
        "Lcom/squareup/communications/service/sync/MessagesSyncNotifier;",
        "(Lcom/squareup/communications/service/CommunicationsService;Lcom/squareup/communications/service/sync/MessagesSyncNotifier;)V",
        "syncEvents",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/communications/service/sync/SyncEvent;",
        "messages",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "",
        "Lcom/squareup/communications/Message;",
        "placementId",
        "",
        "requestMessages",
        "Lio/reactivex/Single;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/communications/service/RealRemoteMessagesStore$Companion;

.field private static final MESSAGE_LIMIT:I = 0x64


# instance fields
.field private final service:Lcom/squareup/communications/service/CommunicationsService;

.field private final syncEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/communications/service/sync/SyncEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/communications/service/RealRemoteMessagesStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/communications/service/RealRemoteMessagesStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/communications/service/RealRemoteMessagesStore;->Companion:Lcom/squareup/communications/service/RealRemoteMessagesStore$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/communications/service/CommunicationsService;Lcom/squareup/communications/service/sync/MessagesSyncNotifier;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notifier"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore;->service:Lcom/squareup/communications/service/CommunicationsService;

    .line 24
    invoke-virtual {p2}, Lcom/squareup/communications/service/sync/MessagesSyncNotifier;->events()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore;->syncEvents:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$requestMessages(Lcom/squareup/communications/service/RealRemoteMessagesStore;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/communications/service/RealRemoteMessagesStore;->requestMessages(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final requestMessages(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;>;>;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;-><init>()V

    .line 44
    invoke-virtual {v0, p1}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->placement_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;

    move-result-object p1

    const/16 v0, 0x64

    .line 45
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->build()Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;

    move-result-object p1

    const-string v0, "GetMessageUnitsRequest\n \u2026E_LIMIT)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore;->service:Lcom/squareup/communications/service/CommunicationsService;

    .line 49
    invoke-interface {v0, p1}, Lcom/squareup/communications/service/CommunicationsService;->getMessageUnits(Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 51
    sget-object v0, Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1;->INSTANCE:Lcom/squareup/communications/service/RealRemoteMessagesStore$requestMessages$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "service\n        .getMess\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public messages(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "placementId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/communications/service/RealRemoteMessagesStore;->syncEvents:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/communications/service/RealRemoteMessagesStore$messages$1;-><init>(Lcom/squareup/communications/service/RealRemoteMessagesStore;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "syncEvents.switchMapSing\u2026stMessages(placementId) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
