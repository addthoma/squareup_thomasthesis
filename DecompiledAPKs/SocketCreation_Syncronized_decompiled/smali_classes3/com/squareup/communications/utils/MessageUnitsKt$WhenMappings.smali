.class public final synthetic Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/protos/messageservice/service/State;->values()[Lcom/squareup/protos/messageservice/service/State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/State;->UNREAD:Lcom/squareup/protos/messageservice/service/State;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/State;->READ:Lcom/squareup/protos/messageservice/service/State;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/State;->DISMISSED:Lcom/squareup/protos/messageservice/service/State;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/State;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/messageservice/service/MessageClass;->values()[Lcom/squareup/protos/messageservice/service/MessageClass;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_COMPLETE_ORDERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_CUSTOMER_COMMS:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_HIGH_PRIORITY:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_RESOLVE_DISPUTE:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SETUP_POS:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SUPPORT_CENTER:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_CUSTOMERS_YOU_WANT_TO_ACQUIRE:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_YOUR_EXISTING_CUSTOMERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->NON_URGENT_BUSINESS_NOTICES_FOR_SELLERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    return-void
.end method
