.class public interface abstract Lcom/squareup/communications/MessageResolver;
.super Ljava/lang/Object;
.source "MessageResolver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/communications/MessageResolver$Resolution;,
        Lcom/squareup/communications/MessageResolver$Result;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0002\t\nJ\u001e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/communications/MessageResolver;",
        "",
        "resolve",
        "Lio/reactivex/Single;",
        "Lcom/squareup/communications/MessageResolver$Result;",
        "requestToken",
        "",
        "resolution",
        "Lcom/squareup/communications/MessageResolver$Resolution;",
        "Resolution",
        "Result",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract resolve(Ljava/lang/String;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/communications/MessageResolver$Resolution;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/communications/MessageResolver$Result;",
            ">;"
        }
    .end annotation
.end method
