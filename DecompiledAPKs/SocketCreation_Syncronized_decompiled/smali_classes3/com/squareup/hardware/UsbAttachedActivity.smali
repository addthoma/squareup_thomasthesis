.class public Lcom/squareup/hardware/UsbAttachedActivity;
.super Landroid/app/Activity;
.source "UsbAttachedActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/hardware/UsbAttachedActivity$Component;
    }
.end annotation


# instance fields
.field eventSink:Lcom/squareup/badbus/BadEventSink;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object p1

    const-class v0, Lcom/squareup/hardware/UsbAttachedActivity$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/hardware/UsbAttachedActivity$Component;

    invoke-interface {p1, p0}, Lcom/squareup/hardware/UsbAttachedActivity$Component;->inject(Lcom/squareup/hardware/UsbAttachedActivity;)V

    .line 42
    iget-object p1, p0, Lcom/squareup/hardware/UsbAttachedActivity;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    invoke-virtual {p1}, Lcom/squareup/usb/UsbDiscoverer;->scheduleSearchForUsbDevices()V

    .line 43
    iget-object p1, p0, Lcom/squareup/hardware/UsbAttachedActivity;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/hardware/UsbDevicesChangedEvent;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/hardware/UsbDevicesChangedEvent;-><init>(Z)V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/hardware/UsbAttachedActivity;->finish()V

    return-void
.end method
