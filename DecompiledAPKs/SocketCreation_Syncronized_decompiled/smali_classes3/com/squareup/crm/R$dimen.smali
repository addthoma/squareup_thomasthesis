.class public final Lcom/squareup/crm/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final crm_appointment_icon_font_size:I = 0x7f0700d7

.field public static final crm_appointment_icon_size:I = 0x7f0700d8

.field public static final crm_card_divider_height:I = 0x7f0700da

.field public static final crm_contact_list_padding:I = 0x7f0700dd

.field public static final crm_customer_unit_label_row_height:I = 0x7f0700e5

.field public static final crm_customer_unit_row_height:I = 0x7f0700e6

.field public static final crm_customer_unit_row_text_margin:I = 0x7f0700e7

.field public static final crm_initial_circle_nonscaling_text_size:I = 0x7f0700ea

.field public static final crm_initial_circle_size:I = 0x7f0700eb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
