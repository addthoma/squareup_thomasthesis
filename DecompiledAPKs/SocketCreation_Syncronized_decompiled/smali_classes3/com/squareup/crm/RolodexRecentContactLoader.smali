.class public interface abstract Lcom/squareup/crm/RolodexRecentContactLoader;
.super Ljava/lang/Object;
.source "RolodexRecentContactLoader.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/RolodexRecentContactLoader$SharedScope;
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use ChooseCustomerFlow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008g\u0018\u00002\u00020\u0001:\u0001\u0014J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00080\u0007H&J\u0008\u0010\t\u001a\u00020\nH&J\u001a\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c0\u0007H&J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\nH&\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/crm/RolodexRecentContactLoader;",
        "Lmortar/Scoped;",
        "addContact",
        "",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "contacts",
        "Lrx/Observable;",
        "",
        "getFirstContactJustSaved",
        "",
        "progress",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "removeContact",
        "contactToken",
        "",
        "setFirstContactJustSaved",
        "value",
        "SharedScope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addContact(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract contacts()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getFirstContactJustSaved()Z
.end method

.method public abstract progress()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            ">;>;>;"
        }
    .end annotation
.end method

.method public abstract removeContact(Ljava/lang/String;)V
.end method

.method public abstract setFirstContactJustSaved(Z)V
.end method
