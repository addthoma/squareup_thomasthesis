.class public abstract Lcom/squareup/crm/model/ContactAttribute;
.super Ljava/lang/Object;
.source "ContactAttribute.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/model/ContactAttribute$NameAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;,
        Lcom/squareup/crm/model/ContactAttribute$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \t2\u00020\u0001:\n\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\t\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute;",
        "Landroid/os/Parcelable;",
        "()V",
        "key",
        "",
        "getKey",
        "()Ljava/lang/String;",
        "AddressAttribute",
        "BirthdayAttribute",
        "Companion",
        "CompanyAttribute",
        "CustomAttribute",
        "EmailAttribute",
        "GroupsAttribute",
        "NameAttribute",
        "PhoneAttribute",
        "ReferenceAttribute",
        "Lcom/squareup/crm/model/ContactAttribute$NameAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$PhoneAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$EmailAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$AddressAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$BirthdayAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CompanyAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$ReferenceAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/crm/model/ContactAttribute$Companion;

.field public static final DEFAULT_ADDRESS_KEY:Ljava/lang/String; = "default:address"

.field public static final DEFAULT_BIRTHDAY_KEY:Ljava/lang/String; = "default:birthday"

.field public static final DEFAULT_COMPANY_KEY:Ljava/lang/String; = "default:company"

.field public static final DEFAULT_EMAIL_KEY:Ljava/lang/String; = "default:email_address"

.field public static final DEFAULT_GROUPS_KEY:Ljava/lang/String; = "default:groups"

.field public static final DEFAULT_NAME_KEY:Ljava/lang/String; = "default:name"

.field public static final DEFAULT_PHONE_KEY:Ljava/lang/String; = "default:phone_number"

.field public static final DEFAULT_REFERENCE_KEY:Ljava/lang/String; = "default:reference_id"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/crm/model/ContactAttribute$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/crm/model/ContactAttribute;->Companion:Lcom/squareup/crm/model/ContactAttribute$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/crm/model/ContactAttribute;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getKey()Ljava/lang/String;
.end method
