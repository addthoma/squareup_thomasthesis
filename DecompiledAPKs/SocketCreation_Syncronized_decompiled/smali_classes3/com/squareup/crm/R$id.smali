.class public final Lcom/squareup/crm/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final boolean_attribute:I = 0x7f0a023d

.field public static final checkable:I = 0x7f0a0315

.field public static final chevron:I = 0x7f0a0329

.field public static final confirm_button:I = 0x7f0a038b

.field public static final crm_add_coupon_row_title:I = 0x7f0a03e1

.field public static final crm_cardonfile_card_glyph:I = 0x7f0a03fb

.field public static final crm_cardonfile_card_name:I = 0x7f0a03fc

.field public static final crm_cardonfile_card_status:I = 0x7f0a03fd

.field public static final crm_cardonfile_unlink_card_button:I = 0x7f0a0408

.field public static final crm_contact_list_bottom_row_message:I = 0x7f0a0410

.field public static final crm_contact_list_bottom_row_progress:I = 0x7f0a0411

.field public static final crm_create_new_customer:I = 0x7f0a041c

.field public static final crm_customer_check_circle:I = 0x7f0a0422

.field public static final crm_customer_contact_info:I = 0x7f0a0423

.field public static final crm_customer_contact_line_pii_wrapper:I = 0x7f0a0424

.field public static final crm_customer_display_name:I = 0x7f0a0425

.field public static final crm_customer_display_name_pii_wrapper:I = 0x7f0a0426

.field public static final crm_customer_initial_circle:I = 0x7f0a0427

.field public static final crm_customer_initial_circle_container:I = 0x7f0a0428

.field public static final crm_customer_name_pii_wrapper:I = 0x7f0a0434

.field public static final crm_customer_status_line:I = 0x7f0a0435

.field public static final crm_group_name_field:I = 0x7f0a0456

.field public static final crm_initial_circle_frame_layout:I = 0x7f0a0462

.field public static final crm_initial_circle_text:I = 0x7f0a0463

.field public static final crm_line_data_row_info:I = 0x7f0a0468

.field public static final crm_line_data_row_title:I = 0x7f0a0469

.field public static final crm_list_header_row:I = 0x7f0a046a

.field public static final crm_list_header_row_text:I = 0x7f0a046b

.field public static final crm_loyalty_phone_check:I = 0x7f0a046c

.field public static final crm_loyalty_phone_number:I = 0x7f0a046d

.field public static final crm_message_creator_timestamp:I = 0x7f0a0488

.field public static final crm_message_left:I = 0x7f0a048c

.field public static final crm_message_right:I = 0x7f0a048d

.field public static final crm_note_row_contents:I = 0x7f0a04a6

.field public static final crm_note_row_creator_timestamp:I = 0x7f0a04a7

.field public static final crm_note_row_note:I = 0x7f0a04a8

.field public static final crm_note_row_reminder:I = 0x7f0a04a9

.field public static final crm_profile_section_header_action:I = 0x7f0a04c5

.field public static final crm_profile_section_header_divider:I = 0x7f0a04c6

.field public static final crm_profile_section_header_dropdown:I = 0x7f0a04c7

.field public static final crm_profile_section_header_right_container:I = 0x7f0a04c8

.field public static final crm_profile_section_header_title:I = 0x7f0a04c9

.field public static final crm_remove_add_coupon_row:I = 0x7f0a04d9

.field public static final crm_search_box:I = 0x7f0a04e7

.field public static final crm_summary_title:I = 0x7f0a0501

.field public static final crm_summary_value:I = 0x7f0a0502

.field public static final crm_view_attribute_title:I = 0x7f0a050d

.field public static final crm_view_attribute_value:I = 0x7f0a050e

.field public static final customer_unit_row:I = 0x7f0a053c

.field public static final email_attribute:I = 0x7f0a06b3

.field public static final enum_attribute:I = 0x7f0a0719

.field public static final icon:I = 0x7f0a0818

.field public static final icon_image:I = 0x7f0a081b

.field public static final icon_text:I = 0x7f0a081d

.field public static final number_attribute:I = 0x7f0a0a7d

.field public static final phone_attribute:I = 0x7f0a0c24

.field public static final spinner_glyph:I = 0x7f0a0ebd

.field public static final spinner_message:I = 0x7f0a0ebf

.field public static final spinner_title:I = 0x7f0a0ec1

.field public static final subtitle:I = 0x7f0a0f49

.field public static final text_attribute:I = 0x7f0a0f9d

.field public static final title:I = 0x7f0a103f

.field public static final unknown_attribute:I = 0x7f0a10b7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
