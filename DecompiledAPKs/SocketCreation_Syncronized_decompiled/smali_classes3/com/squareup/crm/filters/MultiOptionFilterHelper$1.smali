.class synthetic Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;
.super Ljava/lang/Object;
.source "MultiOptionFilterHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/filters/MultiOptionFilterHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    invoke-static {}, Lcom/squareup/protos/client/rolodex/Filter$Type;->values()[Lcom/squareup/protos/client/rolodex/Filter$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    :try_start_0
    sget-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_ENUM:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_VISITED_LOCATION:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/crm/filters/MultiOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CREATION_SOURCE_FILTER:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
