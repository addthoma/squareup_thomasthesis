.class public final Lcom/squareup/crm/MergeProposalLoader;
.super Lcom/squareup/datafetch/AbstractLoader;
.source "MergeProposalLoader.kt"


# annotations
.annotation runtime Lcom/squareup/crm/MergeProposalLoader$SharedScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/MergeProposalLoader$SharedScope;,
        Lcom/squareup/crm/MergeProposalLoader$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/datafetch/AbstractLoader<",
        "Lkotlin/Unit;",
        "Lcom/squareup/protos/client/rolodex/MergeProposal;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 \u001c2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u001c\u001dB+\u0008\u0001\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ=\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0016\u00a2\u0006\u0002\u0010\u001aJ\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u001bH\u0014J\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u001bR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/crm/MergeProposalLoader;",
        "Lcom/squareup/datafetch/AbstractLoader;",
        "",
        "Lcom/squareup/protos/client/rolodex/MergeProposal;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V",
        "totalDuplicateCount",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "fetch",
        "Lio/reactivex/Single;",
        "Lcom/squareup/datafetch/AbstractLoader$Response;",
        "input",
        "pagingParams",
        "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
        "onSubscribe",
        "Lio/reactivex/functions/Consumer;",
        "Lio/reactivex/disposables/Disposable;",
        "(Lkotlin/Unit;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;",
        "Lio/reactivex/Observable;",
        "Companion",
        "SharedScope",
        "crm-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/crm/MergeProposalLoader$Companion;

.field public static final EMPTY_RESULTS:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "Lkotlin/Unit;",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final totalDuplicateCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/crm/MergeProposalLoader$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/crm/MergeProposalLoader$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/crm/MergeProposalLoader;->Companion:Lcom/squareup/crm/MergeProposalLoader$Companion;

    .line 75
    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;-><init>(Ljava/lang/Object;Ljava/util/List;Z)V

    sput-object v0, Lcom/squareup/crm/MergeProposalLoader;->EMPTY_RESULTS:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 9
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodex"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v5, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 28
    invoke-direct/range {v1 .. v8}, Lcom/squareup/datafetch/AbstractLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p4, p0, Lcom/squareup/crm/MergeProposalLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 33
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Int>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/MergeProposalLoader;->totalDuplicateCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getTotalDuplicateCount$p(Lcom/squareup/crm/MergeProposalLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/crm/MergeProposalLoader;->totalDuplicateCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic fetch(Ljava/lang/Object;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;
    .locals 0

    .line 23
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/crm/MergeProposalLoader;->fetch(Lkotlin/Unit;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public fetch(Lkotlin/Unit;Lcom/squareup/datafetch/AbstractLoader$PagingParams;Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/datafetch/AbstractLoader$PagingParams;",
            "Lio/reactivex/functions/Consumer<",
            "Lio/reactivex/disposables/Disposable;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/datafetch/AbstractLoader$Response<",
            "Lkotlin/Unit;",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pagingParams"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSubscribe"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->getPagingKey()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/crm/MergeProposalLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexServiceHelper;->runMergeProposalJob()Lio/reactivex/Completable;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_0
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "Completable.complete()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    :goto_0
    iget-object v1, p0, Lcom/squareup/crm/MergeProposalLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->getPageSize()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$PagingParams;->getPagingKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/squareup/crm/RolodexServiceHelper;->listMergeProposals(Ljava/lang/Integer;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 57
    invoke-virtual {v1, p3}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p3

    .line 58
    new-instance v1, Lcom/squareup/crm/MergeProposalLoader$fetch$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/MergeProposalLoader$fetch$1;-><init>(Lcom/squareup/crm/MergeProposalLoader;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p3, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p3

    .line 62
    new-instance v1, Lcom/squareup/crm/MergeProposalLoader$fetch$2;

    invoke-direct {v1, p1, p2}, Lcom/squareup/crm/MergeProposalLoader$fetch$2;-><init>(Lkotlin/Unit;Lcom/squareup/datafetch/AbstractLoader$PagingParams;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p3, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    .line 55
    invoke-virtual {v0, p1}, Lio/reactivex/Completable;->andThen(Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "preamble.andThen(\n      \u2026    }\n            }\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected input()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 40
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(Unit)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final totalDuplicateCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/crm/MergeProposalLoader;->totalDuplicateCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "totalDuplicateCount.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
