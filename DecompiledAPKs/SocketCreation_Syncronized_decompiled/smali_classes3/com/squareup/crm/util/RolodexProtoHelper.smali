.class public final Lcom/squareup/crm/util/RolodexProtoHelper;
.super Ljava/lang/Object;
.source "RolodexProtoHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRolodexProtoHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RolodexProtoHelper.kt\ncom/squareup/crm/util/RolodexProtoHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,104:1\n747#2:105\n769#2,2:106\n747#2:108\n769#2,2:109\n747#2:111\n769#2,2:112\n747#2:114\n769#2,2:115\n*E\n*S KotlinDebug\n*F\n+ 1 RolodexProtoHelper.kt\ncom/squareup/crm/util/RolodexProtoHelper\n*L\n53#1:105\n53#1,2:106\n63#1:108\n63#1,2:109\n67#1:111\n67#1,2:112\n71#1:114\n71#1,2:115\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0001*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0008\u001a\u00020\t\u001a\n\u0010\n\u001a\u00020\u0001*\u00020\u000b\u001a\u0014\u0010\u000c\u001a\u00020\u0001*\u00020\r2\u0006\u0010\u0008\u001a\u00020\tH\u0002\u001a\n\u0010\u000e\u001a\u00020\u000b*\u00020\u000f\u001a\n\u0010\u0010\u001a\u00020\u000f*\u00020\u000b\u001a\u0012\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0012*\u0004\u0018\u00010\u000f\u00a8\u0006\u0013"
    }
    d2 = {
        "formatDate",
        "",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "formatExpirationPolicy",
        "Lcom/squareup/server/account/protos/ExpirationPolicy;",
        "pointsTerm",
        "res",
        "Lcom/squareup/util/Res;",
        "formatSingleLine",
        "Lcom/squareup/address/Address;",
        "getExpirationPeriodUnitString",
        "Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;",
        "toAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "toGlobalAddress",
        "toList",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final formatDate(Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$formatDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;->expires_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, v0, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    const-string v1, "expires_at.year"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;->expires_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, v1, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;->expires_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object p0, p0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    const-string v2, "expires_at.day_of_month"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-static {v0, v1, p0}, Lcom/squareup/util/Times;->asDate(III)Ljava/util/Date;

    move-result-object p0

    .line 76
    invoke-virtual {p1, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "dateFormat.format(date)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final formatExpirationPolicy(Lcom/squareup/server/account/protos/ExpirationPolicy;Ljava/lang/String;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$formatExpirationPolicy"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object p0, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 85
    sget v1, Lcom/squareup/crm/R$string;->expiration_policy:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 86
    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "points_terminology"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 87
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "expiration_period"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 88
    iget-object p0, p0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    if-eqz p0, :cond_1

    invoke-static {p0, p2}, Lcom/squareup/crm/util/RolodexProtoHelper;->getExpirationPeriodUnitString(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    check-cast v0, Ljava/lang/CharSequence;

    const-string p0, "expiration_unit"

    invoke-virtual {p1, p0, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 90
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const-string v0, ""

    :goto_1
    return-object v0
.end method

.method public static final formatSingleLine(Lcom/squareup/address/Address;)Ljava/lang/String;
    .locals 10

    const-string v0, "$this$formatSingleLine"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    .line 52
    iget-object v3, p0, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    aput-object v0, v2, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget-object p0, p0, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    aput-object p0, v2, v0

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 106
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 53
    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-nez v3, :cond_1

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 107
    :cond_4
    check-cast v0, Ljava/util/List;

    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    const-string p0, ", "

    .line 54
    move-object v2, p0

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getExpirationPeriodUnitString(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/crm/util/RolodexProtoHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const-string p0, ""

    goto :goto_0

    .line 100
    :cond_0
    sget p0, Lcom/squareup/crm/R$string;->date_unit_weeks:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 99
    :cond_1
    sget p0, Lcom/squareup/crm/R$string;->date_unit_years:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 98
    :cond_2
    sget p0, Lcom/squareup/crm/R$string;->date_unit_days:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 97
    :cond_3
    sget p0, Lcom/squareup/crm/R$string;->date_unit_months:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final toAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;
    .locals 8

    const-string v0, "$this$toAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    sget-object v1, Lcom/squareup/protos/common/countries/Country;->ZZ:Lcom/squareup/protos/common/countries/Country;

    if-eq v0, v1, :cond_0

    .line 35
    iget-object v0, p0, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {v0}, Lcom/squareup/protos/common/countries/Country;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/CountryCode;->parseCountryCode(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v7, v0

    .line 40
    new-instance v0, Lcom/squareup/address/Address;

    .line 41
    iget-object v2, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    .line 42
    iget-object v3, p0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    .line 43
    iget-object v4, p0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    .line 44
    iget-object v5, p0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    .line 45
    iget-object v6, p0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    move-object v1, v0

    .line 40
    invoke-direct/range {v1 .. v7}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v0
.end method

.method public static final toGlobalAddress(Lcom/squareup/address/Address;)Lcom/squareup/protos/common/location/GlobalAddress;
    .locals 3

    const-string v0, "$this$toGlobalAddress"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/protos/common/countries/Country;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/countries/Country;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    new-instance v1, Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;-><init>()V

    .line 22
    iget-object v2, p0, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_1(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object v1

    .line 23
    iget-object v2, p0, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_2(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object v1

    .line 24
    iget-object v2, p0, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->locality(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object v1

    .line 25
    iget-object v2, p0, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_1(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object v1

    .line 26
    iget-object p0, p0, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    invoke-virtual {v1, p0}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object p0

    .line 27
    invoke-virtual {p0, v0}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;

    move-result-object p0

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->build()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p0

    const-string v0, "GlobalAddress.Builder()\n\u2026untryCode)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toList(Lcom/squareup/protos/common/location/GlobalAddress;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    if-nez v0, :cond_0

    .line 59
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    .line 62
    iget-object v3, v0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, v0, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 108
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 109
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Ljava/lang/String;

    .line 63
    check-cast v7, Ljava/lang/CharSequence;

    if-eqz v7, :cond_3

    invoke-static {v7}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v7, 0x1

    :goto_2
    if-nez v7, :cond_1

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_4
    check-cast v3, Ljava/util/List;

    move-object v6, v3

    check-cast v6, Ljava/lang/Iterable;

    const-string v2, ", "

    .line 64
    move-object v7, v2

    check-cast v7, Ljava/lang/CharSequence;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3e

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v6, v1, [Ljava/lang/String;

    .line 66
    iget-object v7, v0, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    aput-object v7, v6, v4

    iget-object v7, v0, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    aput-object v7, v6, v5

    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 111
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/Collection;

    .line 112
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Ljava/lang/String;

    .line 67
    check-cast v9, Ljava/lang/CharSequence;

    if-eqz v9, :cond_7

    invoke-static {v9}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    goto :goto_4

    :cond_6
    const/4 v9, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v9, 0x1

    :goto_5
    if-nez v9, :cond_5

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 113
    :cond_8
    check-cast v7, Ljava/util/List;

    move-object v8, v7

    check-cast v8, Ljava/lang/Iterable;

    .line 68
    move-object v9, v2

    check-cast v9, Ljava/lang/CharSequence;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    aput-object v3, v6, v4

    aput-object v2, v6, v5

    .line 70
    iget-object v0, v0, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    aput-object v0, v6, v1

    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 115
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 71
    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_b

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    goto :goto_8

    :cond_b
    :goto_7
    const/4 v3, 0x1

    :goto_8
    if-nez v3, :cond_9

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 116
    :cond_c
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
