.class public final Lcom/squareup/crm/util/CustomerColorUtilsKt;
.super Ljava/lang/Object;
.source "CustomerColorUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomerColorUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomerColorUtils.kt\ncom/squareup/crm/util/CustomerColorUtilsKt\n*L\n1#1,36:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u00a8\u0006\n"
    }
    d2 = {
        "circleColors",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "getCircleColor",
        "",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "contactToken",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final circleColors(Lcom/squareup/util/Res;)[I
    .locals 6

    .line 28
    sget v0, Lcom/squareup/crm/R$color;->crm_initial_circle_blue:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    .line 29
    sget v1, Lcom/squareup/crm/R$color;->crm_initial_circle_azure:I

    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    .line 30
    sget v2, Lcom/squareup/crm/R$color;->crm_initial_circle_purple:I

    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    .line 31
    sget v3, Lcom/squareup/crm/R$color;->crm_initial_circle_registerblue:I

    invoke-interface {p0, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v3

    .line 32
    sget v4, Lcom/squareup/crm/R$color;->crm_initial_circle_registerbluepressed:I

    invoke-interface {p0, v4}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p0

    const/4 v4, 0x5

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v0, v4, v5

    const/4 v0, 0x1

    aput v1, v4, v0

    const/4 v0, 0x2

    aput v2, v4, v0

    const/4 v0, 0x3

    aput v3, v4, v0

    const/4 v0, 0x4

    aput p0, v4, v0

    return-object v4
.end method

.method public static final getCircleColor(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)I
    .locals 1

    const-string v0, "contact"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/crm/util/CustomerColorUtilsKt;->getCircleColor(Ljava/lang/String;Lcom/squareup/util/Res;)I

    move-result p0

    return p0
.end method

.method public static final getCircleColor(Ljava/lang/String;Lcom/squareup/util/Res;)I
    .locals 2

    const-string v0, "contactToken"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p1}, Lcom/squareup/crm/util/CustomerColorUtilsKt;->circleColors(Lcom/squareup/util/Res;)[I

    move-result-object p1

    .line 22
    invoke-static {p0}, Lcom/squareup/util/Strings;->createMD5Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x10

    .line 23
    invoke-static {p0, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result p0

    .line 24
    array-length v0, p1

    rem-int/2addr p0, v0

    aget p0, p1, p0

    return p0

    .line 22
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
