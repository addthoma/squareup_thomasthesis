.class public Lcom/squareup/foregroundservice/RealForegroundServiceStarter;
.super Ljava/lang/Object;
.source "RealForegroundServiceStarter.java"

# interfaces
.implements Lcom/squareup/foregroundservice/ForegroundServiceStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;
    }
.end annotation


# static fields
.field static final MAX_WAIT_BEFORE_STARTING_FOREGROUND_SERVICE_MS:I = 0x4e20

.field private static final START_IN_FOREGROUND:Ljava/lang/String;


# instance fields
.field private final context:Landroid/app/Application;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final serviceClassName:Ljava/lang/String;

.field startInForegroundEnabled:Z

.field private volatile startServiceRunnable:Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    .line 33
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":start.in.foreground"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->START_IN_FOREGROUND:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/thread/executor/MainThread;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/notification/NotificationWrapper;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Service;",
            ">;)V"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 44
    iput-boolean v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startInForegroundEnabled:Z

    .line 134
    iput-object p1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->context:Landroid/app/Application;

    .line 135
    iput-object p2, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 136
    iput-object p3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->res:Lcom/squareup/util/Res;

    .line 137
    iput-object p4, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 138
    iput-object p5, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 139
    invoke-virtual {p6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->serviceClassName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->serviceClassName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startServiceRunnable:Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startServiceRunnable:Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/log/OhSnapLogger;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->START_IN_FOREGROUND:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Landroid/app/Application;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->context:Landroid/app/Application;

    return-object p0
.end method

.method private appIsInForeground()Z
    .locals 3

    .line 202
    new-instance v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v0}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    const/4 v1, 0x0

    .line 205
    :try_start_0
    invoke-static {v0}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v2, 0x64

    if-gt v0, v2, :cond_0

    const/4 v1, 0x1

    :catch_0
    :cond_0
    return v1
.end method

.method private createNotification(Landroid/content/Context;)Landroid/app/Notification;
    .locals 3

    .line 214
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/foreground_service/R$string;->foreground_service_notification_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    sget-object v2, Lcom/squareup/notification/Channels;->BACKGROUND_PROCESSING:Lcom/squareup/notification/Channels;

    invoke-virtual {v1, p1, v2}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    const-string v1, "service"

    .line 216
    invoke-virtual {p1, v1}, Landroidx/core/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/foreground_service/R$string;->foreground_service_notification_title:I

    .line 217
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 218
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    new-instance v1, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 219
    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 220
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public maybePromoteToForeground(Landroid/app/Service;Landroid/content/Intent;)V
    .locals 2

    .line 151
    sget-object v0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->START_IN_FOREGROUND:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 152
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1a

    if-lt p2, v0, :cond_0

    .line 162
    sget p2, Lcom/squareup/foreground_service/R$id;->notification_foregrounded_service:I

    invoke-direct {p0, p1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->createNotification(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 4

    .line 171
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startServiceRunnable:Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->serviceClassName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " started as normal service on foreground"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->shouldStartAsForegroundService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startServiceRunnable:Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->access$800(Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)V

    goto :goto_0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startServiceRunnable:Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->access$900(Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setStartInForegroundEnabled(Z)V
    .locals 0

    .line 167
    iput-boolean p1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startInForegroundEnabled:Z

    return-void
.end method

.method shouldStartAsForegroundService()Z
    .locals 2

    .line 190
    iget-boolean v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->startInForegroundEnabled:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->appIsInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public startInForeground(Landroid/content/Intent;)V
    .locals 2

    .line 143
    new-instance v0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;-><init>(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Landroid/content/Intent;Lcom/squareup/foregroundservice/RealForegroundServiceStarter$1;)V

    invoke-virtual {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->run()V

    return-void
.end method

.method public startWaitingForForeground(Landroid/content/Intent;)V
    .locals 2

    .line 147
    new-instance v0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;-><init>(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Landroid/content/Intent;Lcom/squareup/foregroundservice/RealForegroundServiceStarter$1;)V

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->access$700(Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)V

    return-void
.end method
