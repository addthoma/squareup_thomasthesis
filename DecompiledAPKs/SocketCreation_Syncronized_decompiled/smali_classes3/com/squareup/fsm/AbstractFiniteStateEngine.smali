.class abstract Lcom/squareup/fsm/AbstractFiniteStateEngine;
.super Ljava/lang/Object;
.source "AbstractFiniteStateEngine.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final log:Ljava/util/logging/Logger;

.field private static final logLevel:Ljava/util/logging/Level;


# instance fields
.field private final rules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/fsm/Rule<",
            "TS;-TE;>;>;"
        }
    .end annotation
.end field

.field private state:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/fsm/AbstractFiniteStateEngine;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->log:Ljava/util/logging/Logger;

    .line 19
    sget-object v0, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    sput-object v0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->logLevel:Ljava/util/logging/Level;

    return-void
.end method

.method varargs constructor <init>([Lcom/squareup/fsm/Rule;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/fsm/Rule<",
            "TS;-TE;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->rules:Ljava/util/List;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->log(Ljava/lang/String;)V

    return-void
.end method

.method private fireEntryActions()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/fsm/Rule;

    .line 75
    iget-object v2, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/squareup/fsm/Rule;->maybeFireEntryAction(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireExitActions()V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/fsm/Rule;

    .line 81
    iget-object v2, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/squareup/fsm/Rule;->maybeFireExitAction(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 2

    .line 111
    sget-object v0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->log:Ljava/util/logging/Logger;

    sget-object v1, Lcom/squareup/fsm/AbstractFiniteStateEngine;->logLevel:Ljava/util/logging/Level;

    invoke-virtual {v0, v1, p0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    return-void
.end method

.method private maybeMakeTransition(Lcom/squareup/fsm/Rule$Phase;Ljava/lang/Object;Lcom/squareup/fsm/Rule;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/Rule$Phase;",
            "TE;",
            "Lcom/squareup/fsm/Rule<",
            "TS;-TE;>;)Z"
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    invoke-virtual {p3, p1, v0, p2}, Lcom/squareup/fsm/Rule;->matchesTransition(Lcom/squareup/fsm/Rule$Phase;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 99
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/fsm/Rule;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->log(Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->fireExitActions()V

    .line 101
    iget-object p1, p3, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    .line 105
    invoke-virtual {p3, p2}, Lcom/squareup/fsm/Rule;->fireAction(Ljava/lang/Object;)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->fireEntryActions()V

    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method doOnEvent(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 55
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->rules:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/fsm/Rule;

    .line 60
    sget-object v2, Lcom/squareup/fsm/Rule$Phase;->GUARDED:Lcom/squareup/fsm/Rule$Phase;

    invoke-direct {p0, v2, p1, v0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->maybeMakeTransition(Lcom/squareup/fsm/Rule$Phase;Ljava/lang/Object;Lcom/squareup/fsm/Rule;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    if-nez v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->rules:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/fsm/Rule;

    .line 67
    sget-object v2, Lcom/squareup/fsm/Rule$Phase;->UNGUARDED:Lcom/squareup/fsm/Rule$Phase;

    invoke-direct {p0, v2, p1, v1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->maybeMakeTransition(Lcom/squareup/fsm/Rule$Phase;Ljava/lang/Object;Lcom/squareup/fsm/Rule;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_3
    return-void

    .line 52
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Engine must be started before receiving events."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public peekStateToSave()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    return-object v0
.end method

.method public startFromState(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 46
    iput-object p1, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    .line 47
    invoke-direct {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->fireEntryActions()V

    return-void

    .line 44
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can only be started once."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 87
    iget-object v2, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->state:Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "(%s)"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v1, p0, Lcom/squareup/fsm/AbstractFiniteStateEngine;->rules:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/fsm/Rule;

    const-string v3, "\n  "

    .line 89
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/squareup/fsm/Rule;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 91
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
