.class public final Lcom/squareup/fsm/Rule;
.super Ljava/lang/Object;
.source "Rule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/fsm/Rule$Phase;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final action:Lcom/squareup/fsm/SideEffectForEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/fsm/SideEffectForEvent<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final guard:Lcom/squareup/fsm/GuardForEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/fsm/GuardForEvent<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final matcher:Lcom/squareup/fsm/EventMatcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/fsm/EventMatcher<",
            "TS;>;"
        }
    .end annotation
.end field

.field private final phase:Lcom/squareup/fsm/Rule$Phase;

.field final toState:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/EventMatcher<",
            "TS;>;TS;",
            "Lcom/squareup/fsm/SideEffectForEvent<",
            "TE;>;",
            "Lcom/squareup/fsm/GuardForEvent<",
            "TE;>;",
            "Lcom/squareup/fsm/Rule$Phase;",
            ")V"
        }
    .end annotation

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    .line 131
    iput-object p2, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    .line 132
    iput-object p3, p0, Lcom/squareup/fsm/Rule;->action:Lcom/squareup/fsm/SideEffectForEvent;

    .line 133
    iput-object p4, p0, Lcom/squareup/fsm/Rule;->guard:Lcom/squareup/fsm/GuardForEvent;

    .line 134
    iput-object p5, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    return-void
.end method

.method private static equalEvent(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/EventMatcher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(TS;TE;)",
            "Lcom/squareup/fsm/EventMatcher<",
            "TS;>;"
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/fsm/Rule$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/fsm/Rule$2;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static eventType(Ljava/lang/Object;Ljava/lang/Class;)Lcom/squareup/fsm/EventMatcher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(TS;",
            "Ljava/lang/Class<",
            "-TE;>;)",
            "Lcom/squareup/fsm/EventMatcher<",
            "TS;>;"
        }
    .end annotation

    .line 147
    new-instance v0, Lcom/squareup/fsm/Rule$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/fsm/Rule$1;-><init>(Ljava/lang/Object;Ljava/lang/Class;)V

    return-object v0
.end method

.method static synthetic lambda$doAction$2(Lcom/squareup/fsm/SideEffect;Ljava/lang/Object;)V
    .locals 0

    .line 79
    invoke-interface {p0}, Lcom/squareup/fsm/SideEffect;->call()V

    return-void
.end method

.method static synthetic lambda$noAction$5(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method static synthetic lambda$onEntry$0(Lcom/squareup/fsm/SideEffect;Ljava/lang/Object;)V
    .locals 0

    .line 56
    invoke-interface {p0}, Lcom/squareup/fsm/SideEffect;->call()V

    return-void
.end method

.method static synthetic lambda$onExit$1(Lcom/squareup/fsm/SideEffect;Ljava/lang/Object;)V
    .locals 0

    .line 64
    invoke-interface {p0}, Lcom/squareup/fsm/SideEffect;->call()V

    return-void
.end method

.method static synthetic lambda$onlyIf$3(Lcom/squareup/fsm/Guard;Ljava/lang/Object;)Z
    .locals 0

    .line 98
    invoke-interface {p0}, Lcom/squareup/fsm/Guard;->call()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$unguarded$4(Ljava/lang/Object;)Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method private static noAction()Lcom/squareup/fsm/SideEffectForEvent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/fsm/SideEffectForEvent<",
            "TE;>;"
        }
    .end annotation

    .line 142
    sget-object v0, Lcom/squareup/fsm/-$$Lambda$Rule$ArSvgs-XrKj7Aoc6s4JnITjb4Ac;->INSTANCE:Lcom/squareup/fsm/-$$Lambda$Rule$ArSvgs-XrKj7Aoc6s4JnITjb4Ac;

    return-object v0
.end method

.method public static onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(TS;",
            "Lcom/squareup/fsm/SideEffect;",
            ")",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 56
    new-instance v6, Lcom/squareup/fsm/Rule;

    new-instance v3, Lcom/squareup/fsm/-$$Lambda$Rule$ysZB0cUjpGmUIkdJwLJnIW5hbtA;

    invoke-direct {v3, p1}, Lcom/squareup/fsm/-$$Lambda$Rule$ysZB0cUjpGmUIkdJwLJnIW5hbtA;-><init>(Lcom/squareup/fsm/SideEffect;)V

    invoke-static {}, Lcom/squareup/fsm/Rule;->unguarded()Lcom/squareup/fsm/GuardForEvent;

    move-result-object v4

    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->ENTRY:Lcom/squareup/fsm/Rule$Phase;

    const/4 v1, 0x0

    move-object v0, v6

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method public static onExit(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(TS;",
            "Lcom/squareup/fsm/SideEffect;",
            ")",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 64
    new-instance v6, Lcom/squareup/fsm/Rule;

    new-instance v3, Lcom/squareup/fsm/-$$Lambda$Rule$38EMCLQkdoC2Yh0t2iK20_C8BOo;

    invoke-direct {v3, p1}, Lcom/squareup/fsm/-$$Lambda$Rule$38EMCLQkdoC2Yh0t2iK20_C8BOo;-><init>(Lcom/squareup/fsm/SideEffect;)V

    invoke-static {}, Lcom/squareup/fsm/Rule;->unguarded()Lcom/squareup/fsm/GuardForEvent;

    move-result-object v4

    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->EXIT:Lcom/squareup/fsm/Rule$Phase;

    const/4 v1, 0x0

    move-object v0, v6

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method public static transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(TS;",
            "Ljava/lang/Class<",
            "-TE;>;TS;)",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/fsm/Rule;

    invoke-static {p0, p1}, Lcom/squareup/fsm/Rule;->eventType(Ljava/lang/Object;Ljava/lang/Class;)Lcom/squareup/fsm/EventMatcher;

    move-result-object v1

    invoke-static {}, Lcom/squareup/fsm/Rule;->noAction()Lcom/squareup/fsm/SideEffectForEvent;

    move-result-object v3

    invoke-static {}, Lcom/squareup/fsm/Rule;->unguarded()Lcom/squareup/fsm/GuardForEvent;

    move-result-object v4

    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->UNGUARDED:Lcom/squareup/fsm/Rule$Phase;

    move-object v0, v6

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method public static transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(TS;TE;TS;)",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 40
    new-instance v6, Lcom/squareup/fsm/Rule;

    invoke-static {p0, p1}, Lcom/squareup/fsm/Rule;->equalEvent(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/EventMatcher;

    move-result-object v1

    invoke-static {}, Lcom/squareup/fsm/Rule;->noAction()Lcom/squareup/fsm/SideEffectForEvent;

    move-result-object v3

    invoke-static {}, Lcom/squareup/fsm/Rule;->unguarded()Lcom/squareup/fsm/GuardForEvent;

    move-result-object v4

    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->UNGUARDED:Lcom/squareup/fsm/Rule$Phase;

    move-object v0, v6

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method private static unguarded()Lcom/squareup/fsm/GuardForEvent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/fsm/GuardForEvent<",
            "TE;>;"
        }
    .end annotation

    .line 138
    sget-object v0, Lcom/squareup/fsm/-$$Lambda$Rule$bx9dbI-LmzyC9RpwzpgHAoL6dns;->INSTANCE:Lcom/squareup/fsm/-$$Lambda$Rule$bx9dbI-LmzyC9RpwzpgHAoL6dns;

    return-object v0
.end method


# virtual methods
.method public doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/SideEffect;",
            ")",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 79
    new-instance v6, Lcom/squareup/fsm/Rule;

    iget-object v1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    iget-object v2, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    new-instance v3, Lcom/squareup/fsm/-$$Lambda$Rule$3OF-4_IlKbKg1eJBD-xsvxgLPA0;

    invoke-direct {v3, p1}, Lcom/squareup/fsm/-$$Lambda$Rule$3OF-4_IlKbKg1eJBD-xsvxgLPA0;-><init>(Lcom/squareup/fsm/SideEffect;)V

    iget-object v4, p0, Lcom/squareup/fsm/Rule;->guard:Lcom/squareup/fsm/GuardForEvent;

    iget-object v5, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method public doAction(Lcom/squareup/fsm/SideEffectForEvent;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/SideEffectForEvent<",
            "TE;>;)",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 72
    new-instance v6, Lcom/squareup/fsm/Rule;

    iget-object v1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    iget-object v2, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/fsm/Rule;->guard:Lcom/squareup/fsm/GuardForEvent;

    iget-object v5, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    move-object v0, v6

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method fireAction(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/squareup/fsm/Rule;->action:Lcom/squareup/fsm/SideEffectForEvent;

    invoke-interface {v0, p1}, Lcom/squareup/fsm/SideEffectForEvent;->call(Ljava/lang/Object;)V

    return-void
.end method

.method matchesTransition(Lcom/squareup/fsm/Rule$Phase;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/Rule$Phase;",
            "TS;TE;)Z"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    invoke-virtual {v0, p1}, Lcom/squareup/fsm/Rule$Phase;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    if-eqz p1, :cond_0

    .line 108
    invoke-interface {p1, p2, p3}, Lcom/squareup/fsm/EventMatcher;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/squareup/fsm/Rule;->guard:Lcom/squareup/fsm/GuardForEvent;

    .line 109
    invoke-interface {p1, p3}, Lcom/squareup/fsm/GuardForEvent;->call(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method maybeFireEntryAction(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    sget-object v1, Lcom/squareup/fsm/Rule$Phase;->ENTRY:Lcom/squareup/fsm/Rule$Phase;

    invoke-virtual {v0, v1}, Lcom/squareup/fsm/Rule$Phase;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/fsm/Rule;->fireAction(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method maybeFireExitAction(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    .line 119
    iget-object v0, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    sget-object v1, Lcom/squareup/fsm/Rule$Phase;->EXIT:Lcom/squareup/fsm/Rule$Phase;

    invoke-virtual {v0, v1}, Lcom/squareup/fsm/Rule$Phase;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/fsm/Rule;->fireAction(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onlyIf(Lcom/squareup/fsm/Guard;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/Guard;",
            ")",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 98
    new-instance v6, Lcom/squareup/fsm/Rule;

    iget-object v1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    iget-object v2, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    iget-object v3, p0, Lcom/squareup/fsm/Rule;->action:Lcom/squareup/fsm/SideEffectForEvent;

    new-instance v4, Lcom/squareup/fsm/-$$Lambda$Rule$iiGj6G1UtCOlq60LhGz94JyD558;

    invoke-direct {v4, p1}, Lcom/squareup/fsm/-$$Lambda$Rule$iiGj6G1UtCOlq60LhGz94JyD558;-><init>(Lcom/squareup/fsm/Guard;)V

    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->GUARDED:Lcom/squareup/fsm/Rule$Phase;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method public onlyIf(Lcom/squareup/fsm/GuardForEvent;)Lcom/squareup/fsm/Rule;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/fsm/GuardForEvent<",
            "TE;>;)",
            "Lcom/squareup/fsm/Rule<",
            "TS;TE;>;"
        }
    .end annotation

    .line 91
    new-instance v6, Lcom/squareup/fsm/Rule;

    iget-object v1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    iget-object v2, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    iget-object v3, p0, Lcom/squareup/fsm/Rule;->action:Lcom/squareup/fsm/SideEffectForEvent;

    sget-object v5, Lcom/squareup/fsm/Rule$Phase;->GUARDED:Lcom/squareup/fsm/Rule$Phase;

    move-object v0, v6

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/fsm/Rule;-><init>(Lcom/squareup/fsm/EventMatcher;Ljava/lang/Object;Lcom/squareup/fsm/SideEffectForEvent;Lcom/squareup/fsm/GuardForEvent;Lcom/squareup/fsm/Rule$Phase;)V

    return-object v6
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Lcom/squareup/fsm/Rule;->matcher:Lcom/squareup/fsm/EventMatcher;

    if-nez v1, :cond_0

    const-string v1, "*"

    :cond_0
    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/fsm/Rule;->toState:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/squareup/fsm/Rule;->phase:Lcom/squareup/fsm/Rule$Phase;

    sget-object v3, Lcom/squareup/fsm/Rule$Phase;->GUARDED:Lcom/squareup/fsm/Rule$Phase;

    if-ne v2, v3, :cond_1

    const-string v2, " (conditional)"

    goto :goto_0

    :cond_1
    const-string v2, ""

    :goto_0
    aput-object v2, v0, v1

    const-string v1, "%s --> %s%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
