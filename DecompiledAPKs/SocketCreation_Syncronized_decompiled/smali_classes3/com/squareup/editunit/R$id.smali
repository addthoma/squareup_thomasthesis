.class public final Lcom/squareup/editunit/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/editunit/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final edit_unit_abbreviation:I = 0x7f0a067f

.field public static final edit_unit_abbreviation_label:I = 0x7f0a0680

.field public static final edit_unit_custom_unit_fields:I = 0x7f0a0681

.field public static final edit_unit_delete:I = 0x7f0a0682

.field public static final edit_unit_details:I = 0x7f0a0683

.field public static final edit_unit_name:I = 0x7f0a0684

.field public static final edit_unit_precision_0:I = 0x7f0a0685

.field public static final edit_unit_precision_1:I = 0x7f0a0686

.field public static final edit_unit_precision_2:I = 0x7f0a0687

.field public static final edit_unit_precision_3:I = 0x7f0a0688

.field public static final edit_unit_precision_4:I = 0x7f0a0689

.field public static final edit_unit_precision_5:I = 0x7f0a068a

.field public static final edit_unit_precision_help_text:I = 0x7f0a068b

.field public static final edit_unit_save_spinner:I = 0x7f0a068d

.field public static final edit_unit_scroll_view:I = 0x7f0a068e

.field public static final edit_unit_unit_label:I = 0x7f0a068f

.field public static final precision_group:I = 0x7f0a0c42

.field public static final precision_label:I = 0x7f0a0c43

.field public static final standard_units_list:I = 0x7f0a0f23

.field public static final standard_units_list_create_custom_unit_button:I = 0x7f0a0f24

.field public static final standard_units_list_no_search_result_help_text:I = 0x7f0a0f25

.field public static final standard_units_list_search_bar:I = 0x7f0a0f26

.field public static final standard_units_list_view:I = 0x7f0a0f27


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
