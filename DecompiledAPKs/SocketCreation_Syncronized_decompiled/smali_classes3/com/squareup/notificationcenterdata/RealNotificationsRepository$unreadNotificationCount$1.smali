.class final Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;
.super Ljava/lang/Object;
.source "RealNotificationsRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/RealNotificationsRepository;->unreadNotificationCount(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationsRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,105:1\n1577#2,4:106\n*E\n*S KotlinDebug\n*F\n+ 1 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1\n*L\n31#1,4:106\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;->INSTANCE:Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)I
    .locals 4

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;->getNotifications()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 106
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_2

    .line 108
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/notificationcenterdata/Notification;

    .line 31
    invoke-virtual {v2}, Lcom/squareup/notificationcenterdata/Notification;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v2

    sget-object v3, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    if-gez v0, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_3
    :goto_2
    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;->apply(Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
