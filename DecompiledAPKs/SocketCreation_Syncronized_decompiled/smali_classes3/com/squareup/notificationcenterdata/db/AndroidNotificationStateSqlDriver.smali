.class public final Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;
.super Ljava/lang/Object;
.source "AndroidNotificationStateSqlDriver.kt"

# interfaces
.implements Lcom/squareup/sqldelight/db/SqlDriver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u0007\u001a\u00020\u0008H\u0096\u0001J\u000b\u0010\t\u001a\u0004\u0018\u00010\nH\u0096\u0001JE\u0010\u000b\u001a\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\r2\u001b\u0008\u0002\u0010\u0011\u001a\u0015\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0012\u00a2\u0006\u0002\u0008\u0014H\u0096\u0001\u00a2\u0006\u0002\u0010\u0015JE\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\r2\u001b\u0008\u0002\u0010\u0011\u001a\u0015\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0012\u00a2\u0006\u0002\u0008\u0014H\u0096\u0001\u00a2\u0006\u0002\u0010\u0018J\t\u0010\u0019\u001a\u00020\nH\u0096\u0001\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "application",
        "Landroid/app/Application;",
        "userDir",
        "Ljava/io/File;",
        "(Landroid/app/Application;Ljava/io/File;)V",
        "close",
        "",
        "currentTransaction",
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "execute",
        "identifier",
        "",
        "sql",
        "",
        "parameters",
        "binders",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "Lkotlin/ExtensionFunctionType;",
        "(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V",
        "executeQuery",
        "Lcom/squareup/sqldelight/db/SqlCursor;",
        "(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)Lcom/squareup/sqldelight/db/SqlCursor;",
        "newTransaction",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATABASE_FILE_NAME:Ljava/lang/String; = "notifications_state.db"


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->Companion:Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Ljava/io/File;)V
    .locals 10
    .param p2    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userDir"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    .line 15
    sget-object v1, Lcom/squareup/notificationcenterdata/impl/Database;->Companion:Lcom/squareup/notificationcenterdata/impl/Database$Companion;

    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/impl/Database$Companion;->getSchema()Lcom/squareup/sqldelight/db/SqlDriver$Schema;

    move-result-object v2

    .line 16
    move-object v3, p1

    check-cast v3, Landroid/content/Context;

    .line 17
    new-instance p1, Ljava/io/File;

    const-string v1, "notifications_state.db"

    invoke-direct {p1, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x38

    const/4 v9, 0x0

    move-object v1, v0

    .line 14
    invoke-direct/range {v1 .. v9}, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;-><init>(Lcom/squareup/sqldelight/db/SqlDriver$Schema;Landroid/content/Context;Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->$$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->$$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    invoke-virtual {v0}, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;->close()V

    return-void
.end method

.method public currentTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->$$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    invoke-virtual {v0}, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;->currentTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v0

    return-object v0
.end method

.method public execute(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "sql"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->$$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;->execute(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public executeQuery(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)Lcom/squareup/sqldelight/db/SqlCursor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/sqldelight/db/SqlCursor;"
        }
    .end annotation

    const-string v0, "sql"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->$$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;->executeQuery(Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/functions/Function1;)Lcom/squareup/sqldelight/db/SqlCursor;

    move-result-object p1

    return-object p1
.end method

.method public newTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/AndroidNotificationStateSqlDriver;->$$delegate_0:Lcom/squareup/sqldelight/android/AndroidSqliteDriver;

    invoke-virtual {v0}, Lcom/squareup/sqldelight/android/AndroidSqliteDriver;->newTransaction()Lcom/squareup/sqldelight/Transacter$Transaction;

    move-result-object v0

    return-object v0
.end method
