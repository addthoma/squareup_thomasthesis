.class public final Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;
.super Ljava/lang/Object;
.source "RemoteNotificationsSource.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRemoteNotificationsSource.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RemoteNotificationsSource.kt\ncom/squareup/notificationcenterdata/RemoteNotificationsSource\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,93:1\n1288#2:94\n1313#2,3:95\n1316#2,3:105\n347#3,7:98\n46#4,12:108\n*E\n*S KotlinDebug\n*F\n+ 1 RemoteNotificationsSource.kt\ncom/squareup/notificationcenterdata/RemoteNotificationsSource\n*L\n72#1:94\n72#1,3:95\n72#1,3:105\n72#1,7:98\n73#1,12:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u000cH\u0016J\u0018\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u000f*\u0008\u0012\u0004\u0012\u00020\u00140\u000fH\u0002R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000RJ\u0010\u000e\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0010 \u0011*\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f0\u000f \u0011*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0010 \u0011*\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000f0\u000f\u0018\u00010\u000c0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0012\u001a\u00020\u0013*\u00020\u00148BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0015\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "clientActionTranslationDispatcher",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "messagesRepository",
        "Lcom/squareup/communications/MessagesRepository;",
        "notificationStateStore",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/analytics/Analytics;Lcom/squareup/communications/MessagesRepository;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V",
        "allRemoteNotificationsResult",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/communications/MessagesRepository$Result;",
        "readRemoteNotificationIds",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "isAggregated",
        "",
        "Lcom/squareup/communications/Message;",
        "(Lcom/squareup/communications/Message;)Z",
        "notifications",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "aggregated",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NOTIFICATION_CENTER_PLACEMENT:Ljava/lang/String; = "2781a955-a35d-4c77-9e26-00daa1524780"


# instance fields
.field private final allRemoteNotificationsResult:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/communications/MessagesRepository$Result;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clientActionTranslationDispatcher:Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

.field private final readRemoteNotificationIds:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->Companion:Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/analytics/Analytics;Lcom/squareup/communications/MessagesRepository;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clientActionTranslationDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesRepository"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStateStore"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->clientActionTranslationDispatcher:Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->analytics:Lcom/squareup/analytics/Analytics;

    const-string p1, "2781a955-a35d-4c77-9e26-00daa1524780"

    .line 31
    invoke-interface {p3, p1}, Lcom/squareup/communications/MessagesRepository;->messages(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->allRemoteNotificationsResult:Lio/reactivex/Observable;

    .line 34
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$Source;->REMOTE:Lcom/squareup/notificationcenterdata/Notification$Source;

    sget-object p2, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-interface {p4, p1, p2}, Lcom/squareup/notificationcenterdata/NotificationStateStore;->getNotificationIds(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lio/reactivex/Observable;

    move-result-object p1

    .line 35
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->readRemoteNotificationIds:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$aggregated(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->aggregated(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getClientActionTranslationDispatcher$p(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->clientActionTranslationDispatcher:Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

    return-object p0
.end method

.method private final aggregated(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/communications/Message;",
            ">;"
        }
    .end annotation

    .line 71
    check-cast p1, Ljava/lang/Iterable;

    .line 94
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 95
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 96
    move-object v2, v1

    check-cast v2, Lcom/squareup/communications/Message;

    .line 72
    invoke-virtual {v2}, Lcom/squareup/communications/Message;->getAggregationId()Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 97
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 101
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 105
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 115
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 116
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 76
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/communications/Message;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-direct {p0, v2}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->isAggregated(Lcom/squareup/communications/Message;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v3, v2

    :cond_2
    if-nez v3, :cond_3

    goto :goto_2

    .line 80
    :cond_3
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 81
    :goto_2
    check-cast v1, Ljava/lang/Iterable;

    .line 117
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 119
    :cond_4
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final isAggregated(Lcom/squareup/communications/Message;)Z
    .locals 1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/communications/Message;->getContent()Lcom/squareup/communications/Message$Content;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/communications/Message$Content$NotificationContent;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/communications/Message$Content$NotificationContent;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/communications/Message$Content$NotificationContent;->isAggregated()Z

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public notifications()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 42
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->allRemoteNotificationsResult:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;->readRemoteNotificationIds:Lio/reactivex/Observable;

    const-string v3, "readRemoteNotificationIds"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;

    invoke-direct {v1, p0}, Lcom/squareup/notificationcenterdata/RemoteNotificationsSource$notifications$1;-><init>(Lcom/squareup/notificationcenterdata/RemoteNotificationsSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(allRemoteN\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
