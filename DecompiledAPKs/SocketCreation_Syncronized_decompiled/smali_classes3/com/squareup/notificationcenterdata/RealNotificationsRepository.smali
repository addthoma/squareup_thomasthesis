.class public final Lcom/squareup/notificationcenterdata/RealNotificationsRepository;
.super Ljava/lang/Object;
.source "RealNotificationsRepository.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsRepository;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationsRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,105:1\n1360#2:106\n1429#2,3:107\n73#3:110\n*E\n*S KotlinDebug\n*F\n+ 1 RealNotificationsRepository.kt\ncom/squareup/notificationcenterdata/RealNotificationsRepository\n*L\n49#1:106\n49#1,3:107\n48#1:110\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B,\u0008\u0007\u0012\u0011\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003\u0012\u0010\u0010\u0006\u001a\u000c\u0012\u0004\u0012\u00020\u00080\u0007j\u0002`\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0016\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/RealNotificationsRepository;",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
        "notificationsSources",
        "",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "notificationComparator",
        "Ljava/util/Comparator;",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "Lcom/squareup/notificationcenterdata/NotificationComparator;",
        "(Ljava/util/Set;Ljava/util/Comparator;)V",
        "allNotifications",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "notifications",
        "priority",
        "Lcom/squareup/notificationcenterdata/Notification$Priority;",
        "unreadNotificationCount",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allNotifications:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Comparator;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource;",
            ">;",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "notificationsSources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationComparator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 45
    new-instance p1, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, p2, v1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;-><init>(Ljava/util/List;Z)V

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.just(Result(n\u2026ist(), hasError = false))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 47
    :cond_0
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 49
    check-cast p1, Ljava/lang/Iterable;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 107
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 108
    check-cast v2, Lcom/squareup/notificationcenterdata/NotificationsSource;

    .line 51
    invoke-interface {v2}, Lcom/squareup/notificationcenterdata/NotificationsSource;->notifications()Lio/reactivex/Observable;

    move-result-object v2

    .line 52
    sget-object v3, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;->INSTANCE:Lcom/squareup/notificationcenterdata/RealNotificationsRepository$allNotifications$1$1;

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 63
    new-instance v3, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;-><init>(Ljava/util/List;Z)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 110
    new-instance p1, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$$special$$inlined$combineLatest$1;

    invoke-direct {p1, p2}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$$special$$inlined$combineLatest$1;-><init>(Ljava/util/Comparator;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-static {v0, p1}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ements.map { it as T }) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 89
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observables\n          .c\u2026(1)\n          .refCount()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    :goto_1
    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository;->allNotifications:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public notifications(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$Priority;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "priority"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository;->allNotifications:Lio/reactivex/Observable;

    .line 98
    new-instance v1, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1;

    invoke-direct {v1, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$notifications$1;-><init>(Lcom/squareup/notificationcenterdata/Notification$Priority;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "allNotifications\n       \u2026ror\n          )\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public unreadNotificationCount(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$Priority;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "priority"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/RealNotificationsRepository;->notifications(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;

    move-result-object p1

    .line 30
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;->INSTANCE:Lcom/squareup/notificationcenterdata/RealNotificationsRepository$unreadNotificationCount$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "notifications(priority)\n\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
