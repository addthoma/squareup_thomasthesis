.class public final Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 NotificationCenterRecyclerFactory.kt\ncom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n69#2,5:88\n74#2:100\n1103#3,7:93\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$$special$$inlined$bind$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $messageView$inlined:Lcom/squareup/noho/NohoMessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoMessageView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->$messageView$inlined:Lcom/squareup/noho/NohoMessageView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->$messageView$inlined:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->$messageView$inlined:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;->getTitleStyle()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitleTextAppearance(I)V

    .line 90
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->$messageView$inlined:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;->getContent()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->$messageView$inlined:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;->getIconDrawable()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 92
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;->getOnClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1;->$messageView$inlined:Lcom/squareup/noho/NohoMessageView;

    check-cast p2, Landroid/view/View;

    .line 93
    new-instance v0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1$1;

    invoke-direct {v0, p1}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createWarningBannerRow$$inlined$create$1$lambda$1$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
