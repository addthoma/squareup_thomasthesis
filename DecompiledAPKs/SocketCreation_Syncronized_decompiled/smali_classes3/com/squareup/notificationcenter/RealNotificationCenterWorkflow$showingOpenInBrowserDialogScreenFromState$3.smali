.class final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealNotificationCenterWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->showingOpenInBrowserDialogScreenFromState(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/workflow/Sink;)Lcom/squareup/notificationcenter/NotificationCenterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

.field final synthetic this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->$state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->invoke(Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/notificationcenter/NotificationCenterTab;)V
    .locals 4

    const-string v0, "selectedTab"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->$state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$SelectTab;

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->$state:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    iget-object v3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$SelectTab;-><init>(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/notificationcenter/NotificationCenterTab;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
