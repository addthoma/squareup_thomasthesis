.class public abstract Lcom/squareup/notificationcenter/NotificationCenterState;
.super Ljava/lang/Object;
.source "NotificationCenterState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/NotificationCenterState$LoadingNotificationsState;,
        Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;,
        Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationCenterState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationCenterState.kt\ncom/squareup/notificationcenter/NotificationCenterState\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,70:1\n1847#2,3:71\n1847#2,3:74\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationCenterState.kt\ncom/squareup/notificationcenter/NotificationCenterState\n*L\n53#1,3:71\n57#1,3:74\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J)\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0000\u00a2\u0006\u0002\u0008\t\u0082\u0001\u0003\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "Landroid/os/Parcelable;",
        "()V",
        "validate",
        "",
        "importantNotifications",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "generalNotifications",
        "validate$impl_release",
        "LoadingNotificationsState",
        "ShowingNotificationsState",
        "ShowingOpenInBrowserDialogState",
        "Lcom/squareup/notificationcenter/NotificationCenterState$LoadingNotificationsState;",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/notificationcenter/NotificationCenterState;-><init>()V

    return-void
.end method


# virtual methods
.method public final validate$impl_release(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;)V"
        }
    .end annotation

    const-string v0, "importantNotifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generalNotifications"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    check-cast p1, Ljava/lang/Iterable;

    .line 71
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    .line 72
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notificationcenterdata/Notification;

    .line 53
    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/Notification;->getPriority()Lcom/squareup/notificationcenterdata/Notification$Priority;

    move-result-object v0

    sget-object v3, Lcom/squareup/notificationcenterdata/Notification$Priority$General;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$General;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_7

    .line 57
    check-cast p2, Ljava/lang/Iterable;

    .line 74
    instance-of p1, p2, Ljava/util/Collection;

    if-eqz p1, :cond_3

    move-object p1, p2

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_1

    .line 75
    :cond_3
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/notificationcenterdata/Notification;

    .line 57
    invoke-virtual {p2}, Lcom/squareup/notificationcenterdata/Notification;->getPriority()Lcom/squareup/notificationcenterdata/Notification$Priority;

    move-result-object p2

    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    const/4 v2, 0x0

    :cond_5
    :goto_1
    if-eqz v2, :cond_6

    return-void

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "General notifications cannot contain important notifications."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 53
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Important notifications cannot contain general notifications."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
