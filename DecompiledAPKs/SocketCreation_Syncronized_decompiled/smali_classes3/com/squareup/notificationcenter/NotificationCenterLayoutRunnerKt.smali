.class public final Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;
.super Ljava/lang/Object;
.source "NotificationCenterLayoutRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0014\u0010\u000c\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0008H\u0002\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\"\u0018\u0010\u0007\u001a\u00020\u0008*\u00020\t8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0010"
    }
    d2 = {
        "contentAnimatorViewId",
        "",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        "getContentAnimatorViewId",
        "(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I",
        "loadingAnimatorViewId",
        "getLoadingAnimatorViewId",
        "showOpenInBrowserIcon",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "getShowOpenInBrowserIcon",
        "(Lcom/squareup/notificationcenterdata/Notification;)Z",
        "setSelectionStyle",
        "",
        "Lcom/squareup/noho/NohoLabel;",
        "selected",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getContentAnimatorViewId$p(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->getContentAnimatorViewId(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getLoadingAnimatorViewId$p(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->getLoadingAnimatorViewId(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getShowOpenInBrowserIcon$p(Lcom/squareup/notificationcenterdata/Notification;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->getShowOpenInBrowserIcon(Lcom/squareup/notificationcenterdata/Notification;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setSelectionStyle(Lcom/squareup/noho/NohoLabel;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->setSelectionStyle(Lcom/squareup/noho/NohoLabel;Z)V

    return-void
.end method

.method private static final getContentAnimatorViewId(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I
    .locals 1

    .line 231
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    if-eqz v0, :cond_0

    sget p0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_recycler_view:I

    goto :goto_0

    .line 232
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    if-eqz v0, :cond_1

    sget p0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_recycler_view:I

    goto :goto_0

    .line 233
    :cond_1
    instance-of v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    if-eqz v0, :cond_2

    sget p0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_no_notifications_message_view:I

    goto :goto_0

    .line 234
    :cond_2
    instance-of p0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    if-eqz p0, :cond_3

    sget p0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_no_notifications_message_view:I

    :goto_0
    return p0

    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getLoadingAnimatorViewId(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I
    .locals 0

    .line 220
    instance-of p0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    if-eqz p0, :cond_0

    sget p0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_progress_bar:I

    goto :goto_0

    .line 221
    :cond_0
    sget p0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_content_container:I

    :goto_0
    return p0
.end method

.method private static final getShowOpenInBrowserIcon(Lcom/squareup/notificationcenterdata/Notification;)Z
    .locals 0

    .line 237
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object p0

    instance-of p0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    return p0
.end method

.method private static final setSelectionStyle(Lcom/squareup/noho/NohoLabel;Z)V
    .locals 2

    .line 240
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 242
    sget v1, Lcom/squareup/notificationcenter/impl/R$dimen;->tab_bar_selected_elevation:I

    goto :goto_0

    .line 244
    :cond_0
    sget v1, Lcom/squareup/notificationcenter/impl/R$dimen;->tab_bar_unselected_elevation:I

    .line 240
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLabel;->setElevation(F)V

    .line 249
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_1

    .line 250
    sget v1, Lcom/squareup/notificationcenter/impl/R$color;->selected_tab_text_color:I

    goto :goto_1

    :cond_1
    sget v1, Lcom/squareup/notificationcenter/impl/R$color;->unselected_tab_text_color:I

    .line 249
    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 248
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    if-eqz p1, :cond_2

    .line 255
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLabel;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/notificationcenter/impl/R$drawable;->selected_tab_background:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 254
    :goto_2
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
