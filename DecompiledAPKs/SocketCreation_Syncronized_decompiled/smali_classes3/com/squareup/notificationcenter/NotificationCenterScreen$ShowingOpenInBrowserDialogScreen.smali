.class public final Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;
.super Lcom/squareup/notificationcenter/NotificationCenterScreen;
.source "NotificationCenterScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/NotificationCenterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingOpenInBrowserDialogScreen"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB#\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0008H\u00c6\u0003J-\u0010\u0013\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00082\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        "notifications",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "showError",
        "",
        "(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)V",
        "getNotifications",
        "()Ljava/util/List;",
        "getSelectedTab",
        "()Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "getShowError",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final notifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

.field private final showError:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen$Companion;

    .line 53
    const-class v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Z)V"
        }
    .end annotation

    const-string v0, "notifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/NotificationCenterScreen;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    iput-boolean p3, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZILjava/lang/Object;)Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->copy(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/notificationcenter/NotificationCenterTab;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    return v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Z)",
            "Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;"
        }
    .end annotation

    const-string v0, "notifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;-><init>(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    iget-boolean p1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNotifications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    return-object v0
.end method

.method public final getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    return-object v0
.end method

.method public final getShowError()Z
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingOpenInBrowserDialogScreen(notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->notifications:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->showError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
