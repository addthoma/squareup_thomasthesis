.class public final Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "OpenInBrowserDialogWorkflow.kt"

# interfaces
.implements Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;",
        ">;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J$\u0010\u0007\u001a\u00020\u00052\u0006\u0010\u0008\u001a\u00020\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00040\nH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    .line 22
    new-instance v1, Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow$render$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow$render$1;-><init>(Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 23
    new-instance v2, Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow$render$2;

    invoke-direct {v2, p0, p2}, Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow$render$2;-><init>(Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 20
    invoke-direct {v0, p1, v2, v1}, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;-><init>(Lcom/squareup/notificationcenterdata/Notification;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/notificationcenterdata/Notification;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenter/RealOpenInBrowserDialogWorkflow;->render(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    move-result-object p1

    return-object p1
.end method
