.class public final Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;
.super Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;
.source "LibraryItemListNohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextIconItem"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J;\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00072\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001R\u0014\u0010\u0005\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0014\u0010\r\u001a\u00020\u0007X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        "itemName",
        "",
        "optionNote",
        "amount",
        "isTextTile",
        "",
        "iconText",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V",
        "getAmount",
        "()Ljava/lang/String;",
        "getIconText",
        "isChevronVisible",
        "()Z",
        "getItemName",
        "getOptionNote",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Ljava/lang/String;

.field private final iconText:Ljava/lang/String;

.field private final isChevronVisible:Z

.field private final isTextTile:Z

.field private final itemName:Ljava/lang/String;

.field private final optionNote:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionNote"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iconText"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 97
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->itemName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->optionNote:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->amount:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile:Z

    iput-object p5, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getItemName()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getOptionNote()Ljava/lang/String;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getAmount()Ljava/lang/String;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile()Z

    move-result p4

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getOptionNote()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getAmount()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile()Z

    move-result v0

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;
    .locals 7

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionNote"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iconText"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getItemName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getOptionNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getOptionNote()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getAmount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getAmount()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAmount()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final getIconText()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public getOptionNote()Ljava/lang/String;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->optionNote:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getOptionNote()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getAmount()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public isChevronVisible()Z
    .locals 1

    .line 98
    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isChevronVisible:Z

    return v0
.end method

.method public isTextTile()Z
    .locals 1

    .line 95
    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextIconItem(itemName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getItemName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", optionNote="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getOptionNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getAmount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isTextTile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->isTextTile()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", iconText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->iconText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
