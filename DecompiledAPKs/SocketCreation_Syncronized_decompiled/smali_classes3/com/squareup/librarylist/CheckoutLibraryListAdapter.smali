.class public final Lcom/squareup/librarylist/CheckoutLibraryListAdapter;
.super Landroid/widget/BaseAdapter;
.source "CheckoutLibraryListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutLibraryListAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutLibraryListAdapter.kt\ncom/squareup/librarylist/CheckoutLibraryListAdapter\n*L\n1#1,310:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001:\u0001GBS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010\'\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0018\u0010(\u001a\u00020#2\u0006\u0010)\u001a\u00020*2\u0006\u0010$\u001a\u00020%H\u0002J\u0018\u0010+\u001a\u00020#2\u0006\u0010)\u001a\u00020*2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010,\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010-\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010.\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0018\u0010/\u001a\u00020#2\u0006\u0010)\u001a\u00020*2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u00100\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002J\u0008\u00101\u001a\u00020*H\u0016J\u0008\u00102\u001a\u00020*H\u0002J\u0012\u00103\u001a\u0004\u0018\u0001042\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u00105\u001a\u0002062\u0006\u0010)\u001a\u00020*H\u0016J\u000e\u00107\u001a\u00020 2\u0006\u0010)\u001a\u00020*J\u0010\u00108\u001a\u00020*2\u0006\u00109\u001a\u00020*H\u0002J\u000e\u0010:\u001a\u00020\u00182\u0006\u0010)\u001a\u00020*J\u0010\u0010;\u001a\u00020*2\u0006\u0010)\u001a\u00020*H\u0002J\"\u0010<\u001a\u00020=2\u0006\u0010)\u001a\u00020*2\u0008\u0010>\u001a\u0004\u0018\u00010=2\u0006\u0010?\u001a\u00020@H\u0016J\u0010\u0010A\u001a\u00020\u000e2\u0006\u0010)\u001a\u00020*H\u0002J\u000e\u0010B\u001a\u00020\u000e2\u0006\u0010)\u001a\u00020*J\u0010\u0010C\u001a\u00020\u000e2\u0006\u0010)\u001a\u00020*H\u0016J\u000e\u0010D\u001a\u00020\u000e2\u0006\u0010)\u001a\u00020*J2\u0010E\u001a\u00020#2\u0006\u0010F\u001a\u00020\u001e2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020 0\u00172\u0006\u0010!\u001a\u00020\u000eR\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0019\u001a\u00020\u001a8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001cR\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020 0\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"
    }
    d2 = {
        "Lcom/squareup/librarylist/CheckoutLibraryListAdapter;",
        "Landroid/widget/BaseAdapter;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "isTextTile",
        "",
        "itemDisabledController",
        "Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;",
        "res",
        "Lcom/squareup/util/Res;",
        "catalogIntegrationController",
        "Lcom/squareup/catalogapi/CatalogIntegrationController;",
        "(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V",
        "categoryPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "currentFilter",
        "",
        "getCurrentFilter",
        "()Ljava/lang/String;",
        "cursor",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "itemSuggestions",
        "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
        "showCreateNew",
        "bindAllGiftCardsRow",
        "",
        "view",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow;",
        "bindAllItemsRow",
        "bindAllServicesRow",
        "bindCatalogItemRow",
        "position",
        "",
        "bindCategoryPlaceholder",
        "bindCreateNewRow",
        "bindCustomAmountRow",
        "bindDiscountsRow",
        "bindItemSuggestionRow",
        "bindRewardsRow",
        "getCount",
        "getCursorCount",
        "getItem",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "getItemId",
        "",
        "getItemSuggestion",
        "getItemSuggestionPosition",
        "rawPosition",
        "getPlaceholder",
        "getRegularItemPosition",
        "getView",
        "Landroid/view/View;",
        "convertView",
        "parent",
        "Landroid/view/ViewGroup;",
        "isCategoryPlaceholder",
        "isCreateNewItemButton",
        "isEnabled",
        "isItemSuggestion",
        "update",
        "newCursor",
        "ItemDisabledController",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private categoryPlaceholders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final isTextTile:Z

.field private final itemDisabledController:Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private itemSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private showCreateNew:Z


# direct methods
.method public constructor <init>(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Z",
            "Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")V"
        }
    .end annotation

    const-string v0, "itemPhotos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemDisabledController"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogIntegrationController"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p3, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p5, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-boolean p6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    iput-object p7, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemDisabledController:Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;

    iput-object p8, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->res:Lcom/squareup/util/Res;

    iput-object p9, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    const-string p2, "emptyList()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    return-void
.end method

.method private final bindAllGiftCardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 8

    .line 280
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forGiftCard(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string v0, "drawable"

    .line 283
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v4, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    sget-object v6, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v7, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    const/4 v5, 0x0

    move-object v2, p1

    .line 282
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindAllItemsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 239
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forAllItems(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 240
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    sget v0, Lcom/squareup/librarylist/R$string;->item_library_all_items_and_services:I

    goto :goto_0

    .line 243
    :cond_0
    sget v0, Lcom/squareup/librarylist/R$string;->item_library_all_items:I

    :goto_0
    move v3, v0

    const-string v0, "drawable"

    .line 247
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    move-object v1, p1

    .line 246
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindAllServicesRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 252
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forAllServices(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 254
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_all_services:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 253
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindCatalogItemRow(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 11

    .line 220
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 222
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    instance-of v1, v0, Lcom/squareup/prices/DiscountRulesLibraryCursor;

    const/4 v10, 0x1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    .line 223
    check-cast v0, Lcom/squareup/prices/DiscountRulesLibraryCursor;

    .line 225
    iget-object v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 226
    iget-object v5, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-boolean v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    invoke-virtual {v0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->discountDescription()Ljava/lang/String;

    move-result-object v7

    const-string v0, "discountRulesCursor.discountDescription()"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->res:Lcom/squareup/util/Res;

    move-object v0, p2

    move-object v1, p1

    .line 224
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindDiscountItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;ZLjava/lang/String;Lcom/squareup/util/Res;)V

    goto :goto_0

    .line 223
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.prices.DiscountRulesLibraryCursor"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    .line 231
    iget-object v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-boolean v8, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    iget-object v9, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->res:Lcom/squareup/util/Res;

    move-object v0, p2

    move-object v1, p1

    .line 229
    invoke-virtual/range {v0 .. v9}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V

    .line 235
    :goto_0
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemDisabledController:Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;->isItemDisabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    xor-int/2addr p1, v10

    invoke-virtual {p2, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setEnabled(Z)V

    return-void

    .line 220
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unable to get LibraryEntry"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final bindCategoryPlaceholder(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown category placeholder: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 99
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindCustomAmountRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_0

    .line 98
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindRewardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_0

    .line 97
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindAllServicesRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_0

    .line 96
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindAllItemsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_0

    .line 95
    :pswitch_4
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindAllGiftCardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_0

    .line 94
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindDiscountsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final bindCreateNewRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 3

    .line 289
    sget v0, Lcom/squareup/librarylist/R$drawable;->library_list_create_new_plus_icon:I

    .line 290
    sget v1, Lcom/squareup/librarylist/R$string;->item_library_create_new:I

    .line 291
    iget-boolean v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    .line 288
    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindActionItem(IIZ)V

    return-void
.end method

.method private final bindCustomAmountRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 8

    .line 273
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forCustomAmount(Lcom/squareup/protos/common/CurrencyCode;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string v0, "drawable"

    .line 275
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v4, Lcom/squareup/librarylist/R$string;->item_library_custom_amount:I

    sget-object v6, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v7, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    const/4 v5, 0x0

    move-object v2, p1

    .line 274
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindDiscountsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 259
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 261
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_all_discounts:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 260
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final bindItemSuggestionRow(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 8

    .line 299
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItemSuggestion(I)Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    move-result-object p1

    .line 301
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getIconText()Ljava/lang/String;

    move-result-object v1

    .line 302
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getName()Ljava/lang/String;

    move-result-object v2

    .line 303
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getAmount()J

    move-result-wide v3

    .line 304
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    .line 305
    iget-boolean v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    .line 306
    iget-object v7, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    move-object v0, p2

    .line 300
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindItemSuggestion(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/quantity/PerUnitFormatter;)V

    return-void
.end method

.method private final bindRewardsRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 7

    .line 266
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forReward(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v0, "drawable"

    .line 268
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_redeem_rewards:I

    sget-object v5, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v6, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isTextTile:Z

    const/4 v4, 0x0

    move-object v1, p1

    .line 267
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    return-void
.end method

.method private final getCursorCount()I
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    .line 167
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v1

    :cond_0
    return v1
.end method

.method private final getItemSuggestionPosition(I)I
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr p1, v0

    return p1
.end method

.method private final getRegularItemPosition(I)I
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr p1, v0

    return p1
.end method

.method private final isCategoryPlaceholder(I)Z
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    invoke-direct {p0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getCursorCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    iget-boolean v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->showCreateNew:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public final getCurrentFilter()Ljava/lang/String;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    .line 58
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getSearchFilter()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cursor!!.searchFilter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 57
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The adapter must have a cursor before calling getCurrentFilter()"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 2

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isCategoryPlaceholder(I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 141
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isCreateNewItemButton(I)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v1

    .line 144
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isItemSuggestion(I)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object v1

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getRegularItemPosition(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 148
    :cond_3
    iget-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    :cond_4
    return-object v1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemSuggestion(I)Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;
    .locals 1

    .line 128
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isItemSuggestion(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItemSuggestionPosition(I)I

    move-result p1

    .line 130
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    return-object p1

    .line 128
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is not a valid item suggestion position."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getPlaceholder(I)Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 73
    sget p2, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    goto :goto_0

    .line 75
    :cond_0
    check-cast p2, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    .line 77
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindCatalogItemRow(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 80
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isCategoryPlaceholder(I)Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindCategoryPlaceholder(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 81
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isItemSuggestion(I)Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindItemSuggestionRow(ILcom/squareup/librarylist/LibraryItemListNohoRow;)V

    goto :goto_1

    .line 82
    :cond_3
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->isCreateNewItemButton(I)Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-direct {p0, p2}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->bindCreateNewRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    .line 86
    :goto_1
    check-cast p2, Landroid/view/View;

    return-object p2

    .line 83
    :cond_4
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected non-null item at position "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final isCreateNewItemButton(I)Z
    .locals 2

    .line 213
    iget-boolean v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->showCreateNew:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemDisabledController:Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter$ItemDisabledController;->isItemDisabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final isItemSuggestion(I)Z
    .locals 4

    .line 206
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 207
    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    .line 208
    iget-object v3, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v2

    if-eqz v3, :cond_1

    if-le v0, p1, :cond_0

    goto :goto_0

    :cond_0
    if-lt v1, p1, :cond_1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return v2
.end method

.method public final update(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "newCursor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categoryPlaceholders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemSuggestions"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iput-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->categoryPlaceholders:Ljava/util/List;

    .line 111
    iput-object p3, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->itemSuggestions:Ljava/util/List;

    .line 112
    iput-boolean p4, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->showCreateNew:Z

    .line 114
    iget-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eq p2, p1, :cond_0

    if-eqz p2, :cond_0

    .line 115
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result p3

    const/4 p4, 0x1

    xor-int/2addr p3, p4

    if-ne p3, p4, :cond_0

    .line 116
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 119
    :cond_0
    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/librarylist/CheckoutLibraryListAdapter;->notifyDataSetChanged()V

    return-void
.end method
