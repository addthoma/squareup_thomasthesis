.class public final Lcom/squareup/librarylist/SimpleLibraryListStateSaver;
.super Ljava/lang/Object;
.source "LibraryListStateSaver.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListStateSaver;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/librarylist/SimpleLibraryListStateSaver;",
        "Lcom/squareup/librarylist/LibraryListStateSaver;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/util/Device;)V",
        "load",
        "Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "save",
        "",
        "holder",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListStateSaver;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public load()Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 2

    .line 20
    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListStateSaver;->device:Lcom/squareup/util/Device;

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;->getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object v0

    return-object v0
.end method

.method public save(Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
