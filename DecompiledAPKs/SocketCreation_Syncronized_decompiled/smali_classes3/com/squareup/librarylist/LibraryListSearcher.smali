.class public interface abstract Lcom/squareup/librarylist/LibraryListSearcher;
.super Ljava/lang/Object;
.source "LibraryListSearcher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J$\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&J$\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u000b\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&J$\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListSearcher;",
        "",
        "readFromFilter",
        "Lrx/Single;",
        "Lcom/squareup/librarylist/CatalogQueryResult;",
        "state",
        "Lcom/squareup/librarylist/LibraryListState;",
        "topLevelPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "searchByName",
        "currentState",
        "topLevelSearch",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract readFromFilter(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract searchByName(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract topLevelSearch(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/librarylist/CatalogQueryResult;",
            ">;"
        }
    .end annotation
.end method
