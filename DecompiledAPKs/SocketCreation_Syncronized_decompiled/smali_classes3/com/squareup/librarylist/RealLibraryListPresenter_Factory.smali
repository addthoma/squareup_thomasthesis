.class public final Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;
.super Ljava/lang/Object;
.source "RealLibraryListPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/RealLibraryListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final configurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListAdapterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListAssistantProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAssistant;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->libraryListManagerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->configurationProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p8, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->libraryListAdapterFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
            ">;)",
            "Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;"
        }
    .end annotation

    .line 64
    new-instance v9, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)Lcom/squareup/librarylist/RealLibraryListPresenter;
    .locals 10

    .line 72
    new-instance v9, Lcom/squareup/librarylist/RealLibraryListPresenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/librarylist/RealLibraryListPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/RealLibraryListPresenter;
    .locals 9

    .line 54
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->libraryListManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/librarylist/LibraryListManager;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/librarylist/LibraryListAssistant;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/librarylist/LibraryListEntryHandler;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->configurationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/librarylist/LibraryListConfiguration;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->libraryListAdapterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/librarylist/LibraryListAdapter$Factory;

    invoke-static/range {v1 .. v8}, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)Lcom/squareup/librarylist/RealLibraryListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListPresenter_Factory;->get()Lcom/squareup/librarylist/RealLibraryListPresenter;

    move-result-object v0

    return-object v0
.end method
