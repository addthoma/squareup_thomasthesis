.class public interface abstract Lcom/squareup/librarylist/LibraryListEntryHandler;
.super Ljava/lang/Object;
.source "LibraryListEntryHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0019J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J \u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&J\u0018\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H&J\"\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0010H&J \u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u0010H&J\u0010\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u0018H&\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListEntryHandler;",
        "",
        "createNewLibraryItemClicked",
        "",
        "customAmountClicked",
        "discountClicked",
        "sourceView",
        "Landroid/view/View;",
        "libraryEntry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "useDiscountDrawable",
        "",
        "isEntryEnabled",
        "catalogType",
        "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
        "objectId",
        "",
        "itemClicked",
        "itemThumbnail",
        "Landroid/widget/ImageView;",
        "searchFilter",
        "itemLongClicked",
        "itemSuggestionClicked",
        "itemSuggestion",
        "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
        "CogsSelectedEntryData",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createNewLibraryItemClicked()V
.end method

.method public abstract customAmountClicked()V
.end method

.method public abstract discountClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V
.end method

.method public abstract isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z
.end method

.method public abstract itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V
.end method

.method public abstract itemLongClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V
.end method

.method public abstract itemSuggestionClicked(Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V
.end method
