.class public final Lcom/squareup/librarylist/SimpleLibraryListAssistant;
.super Ljava/lang/Object;
.source "SimpleLibraryListAssistant.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListAssistant;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\nH\u0016J\u0008\u0010\u000c\u001a\u00020\nH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
        "Lcom/squareup/librarylist/LibraryListAssistant;",
        "()V",
        "hasSearchText",
        "",
        "getHasSearchText",
        "()Z",
        "setHasSearchText",
        "(Z)V",
        "logLibraryListItemClicked",
        "",
        "logLibraryListItemLongClicked",
        "openItemsApplet",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private hasSearchText:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHasSearchText()Z
    .locals 1

    .line 4
    iget-boolean v0, p0, Lcom/squareup/librarylist/SimpleLibraryListAssistant;->hasSearchText:Z

    return v0
.end method

.method public logLibraryListItemClicked()V
    .locals 0

    return-void
.end method

.method public logLibraryListItemLongClicked()V
    .locals 0

    return-void
.end method

.method public openItemsApplet()V
    .locals 2

    .line 7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot open Items Applet from SimpleLibraryListPresenter"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public setHasSearchText(Z)V
    .locals 0

    .line 4
    iput-boolean p1, p0, Lcom/squareup/librarylist/SimpleLibraryListAssistant;->hasSearchText:Z

    return-void
.end method
