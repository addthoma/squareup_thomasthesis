.class public final synthetic Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->values()[Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/librarylist/LibraryListAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    return-void
.end method
