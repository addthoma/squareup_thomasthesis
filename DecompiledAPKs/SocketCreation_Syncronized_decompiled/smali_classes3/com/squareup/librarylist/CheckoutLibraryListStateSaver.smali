.class public final Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;
.super Ljava/lang/Object;
.source "LibraryListStateSaver.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListStateSaver;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryListStateSaver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryListStateSaver.kt\ncom/squareup/librarylist/CheckoutLibraryListStateSaver\n*L\n1#1,51:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0008\u001a\u00020\u0004H\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0004H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;",
        "Lcom/squareup/librarylist/LibraryListStateSaver;",
        "holderSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Device;)V",
        "load",
        "save",
        "",
        "holder",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final holderSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Device;)V
    .locals 1
    .param p1    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/librarylist/LastLibraryFilter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;",
            "Lcom/squareup/util/Device;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "holderSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;->holderSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public load()Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;->holderSetting:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 39
    sget-object v2, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListState$Filter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListState$Filter;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;->device:Lcom/squareup/util/Device;

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;->getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    goto :goto_0

    .line 44
    :cond_1
    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;->device:Lcom/squareup/util/Device;

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;->getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public save(Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;->holderSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method
