.class public final Lcom/squareup/librarylist/LibraryItemListNohoRowKt;
.super Ljava/lang/Object;
.source "LibraryItemListNohoRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "loadIconIntoRow",
        "",
        "Lcom/squareup/noho/NohoRow;",
        "thumbnailData",
        "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
        "transitionTime",
        "",
        "library-list_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final loadIconIntoRow(Lcom/squareup/noho/NohoRow;Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;I)V
    .locals 7

    const-string v0, "$this$loadIconIntoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "thumbnailData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 426
    instance-of v0, p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$TextThumbnailData;

    if-eqz v0, :cond_0

    .line 427
    new-instance p2, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    .line 428
    check-cast p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$TextThumbnailData;

    invoke-virtual {p1}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$TextThumbnailData;->getText()Ljava/lang/String;

    move-result-object v2

    .line 429
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/squareup/librarylist/R$color;->library_item_background_color_action:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p2

    .line 427
    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;-><init>(Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    goto :goto_1

    .line 432
    :cond_0
    instance-of v0, p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$ResourceThumbnailData;

    if-eqz v0, :cond_1

    .line 433
    new-instance p2, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;

    .line 434
    check-cast p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$ResourceThumbnailData;

    invoke-virtual {p1}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$ResourceThumbnailData;->getDrawableRes()I

    move-result p1

    .line 435
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$color;->library_item_background_color_action:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 433
    invoke-direct {p2, p1, v0}, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;-><init>(II)V

    check-cast p2, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 437
    sget p1, Lcom/squareup/librarylist/R$style;->LibraryItemListNohoRow_IconAction:I

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow;->setIconStyleId(I)V

    goto :goto_1

    .line 439
    :cond_1
    instance-of v0, p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;

    if-eqz v0, :cond_3

    .line 440
    move-object v0, p1

    check-cast v0, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;

    invoke-virtual {v0}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;->getShouldShowFadingAnimation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 441
    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;

    new-instance v1, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$1;-><init>(Lcom/squareup/noho/NohoRow;Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;I)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    goto :goto_0

    .line 449
    :cond_2
    new-instance p2, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;

    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$2;

    invoke-direct {v0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$2;-><init>(Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v0}, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    .line 440
    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 451
    sget p1, Lcom/squareup/noho/R$style;->Widget_Noho_Row_Icon:I

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow;->setIconStyleId(I)V

    :cond_3
    :goto_1
    return-void
.end method
