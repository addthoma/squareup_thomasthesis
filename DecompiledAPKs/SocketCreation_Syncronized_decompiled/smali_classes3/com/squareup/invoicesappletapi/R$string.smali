.class public final Lcom/squareup/invoicesappletapi/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoicesappletapi/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final after_one_invoice:I = 0x7f1200bf

.field public static final after_plural_invoices:I = 0x7f1200c0

.field public static final day:I = 0x7f1207d7

.field public static final month:I = 0x7f121020

.field public static final never:I = 0x7f121061

.field public static final on_date:I = 0x7f1210e4

.field public static final one_day:I = 0x7f121124

.field public static final one_month:I = 0x7f121125

.field public static final one_week:I = 0x7f121127

.field public static final one_year:I = 0x7f121128

.field public static final plural_days:I = 0x7f121449

.field public static final plural_months:I = 0x7f12144a

.field public static final plural_weeks:I = 0x7f12144b

.field public static final plural_years:I = 0x7f12144c

.field public static final week:I = 0x7f121bd6

.field public static final year:I = 0x7f121bed


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
