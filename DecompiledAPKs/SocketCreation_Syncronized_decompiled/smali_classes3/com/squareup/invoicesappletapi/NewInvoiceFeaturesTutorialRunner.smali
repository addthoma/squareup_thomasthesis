.class public interface abstract Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;
.super Ljava/lang/Object;
.source "NewInvoiceFeaturesTutorialRunner.java"


# static fields
.field public static final NOT_SUPPORTED:Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;

    .line 7
    invoke-static {v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;

    sput-object v0, Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;->NOT_SUPPORTED:Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;

    return-void
.end method


# virtual methods
.method public abstract onRequestExitTutorial()V
.end method
