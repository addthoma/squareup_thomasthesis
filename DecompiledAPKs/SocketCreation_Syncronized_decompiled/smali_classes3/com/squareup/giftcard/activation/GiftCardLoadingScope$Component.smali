.class public interface abstract Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;
.super Ljava/lang/Object;
.source "GiftCardLoadingScope.kt"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
.end annotation

.annotation runtime Lcom/squareup/ui/ErrorsBarPresenter$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/giftcard/activation/GiftCardLoadingScopeModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\rH&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;",
        "Lcom/squareup/ui/ErrorsBarView$Component;",
        "GiftCardLoadingScopeRunner",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;",
        "giftCardBalanceCoordinator",
        "Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;",
        "giftCardChooseTypeCoordinator",
        "Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;",
        "giftCardClearBalanceCoordinator",
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;",
        "giftCardHistoryCoordinator",
        "Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;",
        "giftCardLookupCoordinator",
        "Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract GiftCardLoadingScopeRunner()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;
.end method

.method public abstract giftCardBalanceCoordinator()Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;
.end method

.method public abstract giftCardChooseTypeCoordinator()Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;
.end method

.method public abstract giftCardClearBalanceCoordinator()Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;
.end method

.method public abstract giftCardHistoryCoordinator()Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;
.end method

.method public abstract giftCardLookupCoordinator()Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;
.end method
