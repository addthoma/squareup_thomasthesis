.class public Lcom/squareup/giftcard/activation/GiftCardItemizer;
.super Ljava/lang/Object;
.source "GiftCardItemizer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J0\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardItemizer;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)V",
        "createGiftCardItemization",
        "Lcom/squareup/checkout/CartItem;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "activityType",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "isEGiftCard",
        "",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method public createGiftCardItemization(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;Lcom/squareup/protos/client/IdPair;Z)Lcom/squareup/checkout/CartItem;
    .locals 3

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCard"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idPair"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;-><init>()V

    .line 49
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->activity_type(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    move-result-object p3

    .line 50
    iget-object v0, p2, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->gift_card_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    move-result-object p3

    .line 51
    iget-object v0, p2, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;

    move-result-object p3

    .line 52
    invoke-virtual {p3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object p3

    .line 54
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;-><init>()V

    .line 56
    new-instance v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;-><init>()V

    .line 58
    new-instance v2, Lcom/squareup/api/items/ItemVariation$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/ItemVariation$Builder;-><init>()V

    .line 59
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/api/items/ItemVariation$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object p1

    .line 60
    sget-object v2, Lcom/squareup/api/items/PricingType;->FIXED_PRICING:Lcom/squareup/api/items/PricingType;

    invoke-virtual {p1, v2}, Lcom/squareup/api/items/ItemVariation$Builder;->pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/ItemVariation$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemVariation$Builder;->build()Lcom/squareup/api/items/ItemVariation;

    move-result-object p1

    .line 57
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->item_variation(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    move-result-object p1

    .line 55
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    move-result-object p1

    .line 65
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->gift_card_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    move-result-object p1

    .line 68
    new-instance p3, Ljava/util/Date;

    invoke-direct {p3}, Ljava/util/Date;-><init>()V

    .line 69
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->item_variation_default_name_regular:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 70
    invoke-static {p1, v1, v2}, Lcom/squareup/checkout/OrderVariation;->of(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;Ljava/lang/String;Lcom/squareup/quantity/UnitDisplayData;)Lcom/squareup/checkout/OrderVariation;

    move-result-object p1

    .line 76
    new-instance v1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v1}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 78
    invoke-virtual {v1, p4}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p4

    .line 79
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->res:Lcom/squareup/util/Res;

    iget-object p2, p2, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    invoke-static {v1, p2, p5}, Lcom/squareup/text/Cards;->formattedGiftCard(Lcom/squareup/util/Res;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, p2}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 80
    sget-object p4, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-virtual {p2, p4}, Lcom/squareup/checkout/CartItem$Builder;->backingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 82
    new-instance p4, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    .line 84
    new-instance v1, Lcom/squareup/api/items/Item$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/Item$Builder;-><init>()V

    .line 86
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->res:Lcom/squareup/util/Res;

    if-eqz p5, :cond_0

    .line 88
    sget p5, Lcom/squareup/common/card/R$string;->electronic_gift_card:I

    goto :goto_0

    .line 89
    :cond_0
    sget p5, Lcom/squareup/giftcard/activation/R$string;->gift_card:I

    .line 86
    :goto_0
    invoke-interface {v2, p5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p5

    .line 85
    invoke-virtual {v1, p5}, Lcom/squareup/api/items/Item$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    move-result-object p5

    .line 92
    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {p5, v1}, Lcom/squareup/api/items/Item$Builder;->type(Lcom/squareup/api/items/Item$Type;)Lcom/squareup/api/items/Item$Builder;

    move-result-object p5

    .line 93
    invoke-virtual {p5}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object p5

    .line 83
    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object p4

    .line 95
    invoke-virtual {p4}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object p4

    .line 81
    invoke-virtual {p2, p4}, Lcom/squareup/checkout/CartItem$Builder;->backingDetails(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 97
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/checkout/CartItem$Builder;->variations(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 98
    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1, p3}, Lcom/squareup/checkout/CartItem$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 100
    iget-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardItemizer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/checkout/CartItem$Builder;->appliedDiscounts(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 101
    invoke-static {v0, p3}, Lcom/squareup/checkout/util/ItemizationEvents;->creationEvent(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 102
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    const-string p2, "CartItem.Builder()\n     \u2026e, now))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
