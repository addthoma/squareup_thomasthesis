.class public final Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "GiftCardClearBalanceCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "errorsBar",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "(Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method public static final synthetic access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 36
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/giftcard/activation/R$string;->gift_card_clear_balance:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 37
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 39
    sget v0, Lcom/squareup/giftcard/activation/R$id;->cash_out_reason:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 40
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;Lcom/squareup/marketfont/MarketCheckedTextView;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 43
    sget v0, Lcom/squareup/giftcard/activation/R$id;->clear_balance_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/giftcard/activation/R$string;->gift_card_clear_balance_amount:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 45
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;

    invoke-interface {v3}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 46
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    .line 47
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    .line 50
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)V

    check-cast v1, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 52
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gift_card_clear_reason_other_text:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 53
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$4;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$4;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 62
    sget v2, Lcom/squareup/giftcard/activation/R$id;->reason_checkable_group:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/widgets/CheckableGroup;

    .line 63
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;

    invoke-direct {v3, p0, v1, v0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;Landroid/widget/EditText;Lcom/squareup/ui/ConfirmButton;)V

    check-cast v3, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 69
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_clear_balance_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 71
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;

    invoke-direct {v1, p0, v0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$6;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;Landroid/widget/ProgressBar;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 76
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;

    invoke-direct {v0, p0}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$7;-><init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
