.class public final Lcom/squareup/giftcard/activation/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final duplicate_gift_card_message:I = 0x7f1208e8

.field public static final gift_card:I = 0x7f120af8

.field public static final gift_card_activation_hint:I = 0x7f120afe

.field public static final gift_card_activity_header_caps:I = 0x7f120aff

.field public static final gift_card_add_value:I = 0x7f120b00

.field public static final gift_card_add_value_uppercase:I = 0x7f120b01

.field public static final gift_card_check_balance:I = 0x7f120b02

.field public static final gift_card_choose_type_hint:I = 0x7f120b07

.field public static final gift_card_clear_balance:I = 0x7f120b08

.field public static final gift_card_clear_balance_amount:I = 0x7f120b09

.field public static final gift_card_clear_balance_failure:I = 0x7f120b0a

.field public static final gift_card_clear_balance_reason_cash_out:I = 0x7f120b0b

.field public static final gift_card_clear_balance_reason_lost_gift_card:I = 0x7f120b0c

.field public static final gift_card_clear_balance_reason_other:I = 0x7f120b0d

.field public static final gift_card_clear_balance_reason_other_hint:I = 0x7f120b0e

.field public static final gift_card_clear_balance_reason_reset_gift_card:I = 0x7f120b0f

.field public static final gift_card_clear_balance_reason_title_caps:I = 0x7f120b10

.field public static final gift_card_clear_balance_warning:I = 0x7f120b11

.field public static final gift_card_current_balance_label:I = 0x7f120b12

.field public static final gift_card_custom_amount:I = 0x7f120b14

.field public static final gift_card_history_load_failure:I = 0x7f120b17

.field public static final gift_card_invalid_subtitle:I = 0x7f120b18

.field public static final gift_card_invalid_title:I = 0x7f120b19

.field public static final gift_card_load:I = 0x7f120b1a

.field public static final gift_card_lookup_hint:I = 0x7f120b1c

.field public static final gift_card_name_balance_format:I = 0x7f120b1d

.field public static final gift_card_number_hint:I = 0x7f120b1e

.field public static final gift_card_number_hint_enter_card:I = 0x7f120b1f

.field public static final gift_card_sell_e_gift_card:I = 0x7f120b24

.field public static final gift_card_sell_physical_gift_card:I = 0x7f120b25

.field public static final gift_card_unsupported_message:I = 0x7f120b26

.field public static final gift_card_unsupported_message_keyed:I = 0x7f120b27


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
