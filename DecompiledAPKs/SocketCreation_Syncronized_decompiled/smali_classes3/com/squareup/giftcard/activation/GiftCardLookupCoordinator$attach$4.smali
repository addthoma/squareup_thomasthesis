.class final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardLookupCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

.field final synthetic $panEditor:Lcom/squareup/register/widgets/card/PanEditor;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/ui/TokenView;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$panEditor:Lcom/squareup/register/widgets/card/PanEditor;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$view:Landroid/view/View;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->card()Lrx/Observable;

    move-result-object v0

    .line 121
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "controller.card()\n      \u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
