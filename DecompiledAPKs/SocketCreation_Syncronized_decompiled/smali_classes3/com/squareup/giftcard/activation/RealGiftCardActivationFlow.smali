.class public final Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;
.super Ljava/lang/Object;
.source "RealGiftCardActivationFlow.kt"

# interfaces
.implements Lcom/squareup/giftcardactivation/GiftCardActivationFlow;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0018\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;",
        "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "activationScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "checkBalanceFor",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "parentKey",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public activationScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 5

    .line 17
    iget-object v0, p0, Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EGIFT_CARD_IN_POS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const-string v1, "MainActivityScope.INSTANCE"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 18
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen;

    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    sget-object v4, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v3, v2, v4}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;-><init>(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v3, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v3}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;

    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    sget-object v4, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v3, v2, v4}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;-><init>(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v3, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v3}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    :goto_0
    return-object v0
.end method

.method public checkBalanceFor(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 2

    const-string v0, "giftCard"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen;

    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    invoke-direct {v1, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;-><init>(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method
