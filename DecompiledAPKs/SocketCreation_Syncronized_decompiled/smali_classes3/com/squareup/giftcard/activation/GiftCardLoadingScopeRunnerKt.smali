.class public final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0016\u0010\u0000\u001a\u00020\u00018\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0002\u0010\u0003\"\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "AUTO_ADVANCE_DELAY_SECONDS",
        "",
        "AUTO_ADVANCE_DELAY_SECONDS$annotations",
        "()V",
        "EMPTY_REGISTER_CARD",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "getEMPTY_REGISTER_CARD",
        "()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "giftcard-activation_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final AUTO_ADVANCE_DELAY_SECONDS:J = 0x1L

.field private static final EMPTY_REGISTER_CARD:Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 91
    new-instance v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;-><init>()V

    .line 92
    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    move-result-object v0

    const-string v1, "RegisterGiftCardResponse.Builder()\n    .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;->EMPTY_REGISTER_CARD:Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    return-void
.end method

.method public static synthetic AUTO_ADVANCE_DELAY_SECONDS$annotations()V
    .locals 0

    return-void
.end method

.method public static final getEMPTY_REGISTER_CARD()Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunnerKt;->EMPTY_REGISTER_CARD:Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    return-object v0
.end method
