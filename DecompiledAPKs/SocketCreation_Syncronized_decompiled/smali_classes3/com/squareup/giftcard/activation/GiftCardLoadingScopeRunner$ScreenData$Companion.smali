.class public final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004J\u0016\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\u0004\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;",
        "",
        "()V",
        "cardBalanceError",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;",
        "loading",
        "success",
        "cardBalance",
        "Lcom/squareup/protos/common/Money;",
        "cardName",
        "",
        "swipeError",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 620
    invoke-direct {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final cardBalanceError()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    .locals 3

    .line 631
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;->CARD_BALANCE_ERROR:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final loading()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    .locals 3

    .line 623
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;->LOADING:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final success(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    .locals 3

    const-string v0, "cardBalance"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 638
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;->SUCCESS:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final swipeError()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    .locals 3

    .line 627
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;->SWIPE_ERROR:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
