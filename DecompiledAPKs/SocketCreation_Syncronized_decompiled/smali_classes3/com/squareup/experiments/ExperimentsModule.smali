.class public Lcom/squareup/experiments/ExperimentsModule;
.super Ljava/lang/Object;
.source "ExperimentsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/experiments/ExperimentsModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAllExperiments(Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;Lcom/squareup/experiments/SquareCardUpsellExperiment;Lcom/squareup/experiments/ExperimentProfile;)Ljava/util/List;
    .locals 2
    .param p3    # Lcom/squareup/experiments/ExperimentProfile;
        .annotation runtime Lcom/squareup/experiments/IposSkipOnboardingExperiment;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/experiments/ExperimentProfile;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    const/4 p0, 0x2

    aput-object p2, v0, p0

    const/4 p0, 0x3

    aput-object p3, v0, p0

    .line 42
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static provideBranPaymentPromptExperiment(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;

    invoke-direct {v0, p0, p1}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;-><init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method static provideExperimentsCache(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;"
        }
    .end annotation

    .line 52
    const-class v0, Lcom/squareup/experiments/ExperimentMap;

    const-string v1, "experiments"

    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideIposSkipOnboardingExperiment(Lcom/squareup/http/Server;Lcom/squareup/experiments/IposSkipOnboardingProductionExperiment;Lcom/squareup/experiments/IposSkipOnboardingStagingExperiment;)Lcom/squareup/experiments/ExperimentProfile;
    .locals 0
    .annotation runtime Lcom/squareup/experiments/IposSkipOnboardingExperiment;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 60
    invoke-static {p0, p1, p2}, Lcom/squareup/experiments/ExperimentsKt;->chooseExperiment(Lcom/squareup/http/Server;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/experiments/ExperimentProfile;

    move-result-object p0

    return-object p0
.end method
