.class Lcom/squareup/experiments/ServerExperiments$1;
.super Ljava/lang/Object;
.source "ServerExperiments.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/experiments/ServerExperiments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/server/ExperimentsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/experiments/ServerExperiments;


# direct methods
.method constructor <init>(Lcom/squareup/experiments/ServerExperiments;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/experiments/ServerExperiments$1;->this$0:Lcom/squareup/experiments/ServerExperiments;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Lcom/squareup/server/ExperimentsResponse;)V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments$1;->this$0:Lcom/squareup/experiments/ServerExperiments;

    invoke-static {v0}, Lcom/squareup/experiments/ServerExperiments;->access$000(Lcom/squareup/experiments/ServerExperiments;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    new-instance v1, Lcom/squareup/experiments/ExperimentMap;

    invoke-direct {v1}, Lcom/squareup/experiments/ExperimentMap;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/experiments/ExperimentMap;

    .line 61
    invoke-virtual {v0, p1}, Lcom/squareup/experiments/ExperimentMap;->updateFrom(Lcom/squareup/server/ExperimentsResponse;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/experiments/ServerExperiments$1;->this$0:Lcom/squareup/experiments/ServerExperiments;

    iget-object p1, p1, Lcom/squareup/experiments/ServerExperiments;->lastResponse:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/experiments/ServerExperiments$1;->this$0:Lcom/squareup/experiments/ServerExperiments;

    invoke-static {p1}, Lcom/squareup/experiments/ServerExperiments;->access$000(Lcom/squareup/experiments/ServerExperiments;)Lcom/squareup/settings/LocalSetting;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 58
    check-cast p1, Lcom/squareup/server/ExperimentsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/experiments/ServerExperiments$1;->accept(Lcom/squareup/server/ExperimentsResponse;)V

    return-void
.end method
