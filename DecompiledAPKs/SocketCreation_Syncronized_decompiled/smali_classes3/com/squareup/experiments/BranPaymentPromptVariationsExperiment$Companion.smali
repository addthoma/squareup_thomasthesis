.class public final Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;
.super Ljava/lang/Object;
.source "BranPaymentPromptVariationsExperiment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\t\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;",
        "",
        "()V",
        "CLASSIC",
        "Lcom/squareup/server/ExperimentsResponse$Bucket;",
        "DEFAULT_CONFIGURATION",
        "Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;",
        "getDEFAULT_CONFIGURATION",
        "()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;",
        "STATIC",
        "VIDEO",
        "experiments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDEFAULT_CONFIGURATION()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 1

    .line 52
    invoke-static {}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->access$getDEFAULT_CONFIGURATION$cp()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object v0

    return-object v0
.end method
