.class public final Lcom/squareup/experiments/ServerExperiments_Factory;
.super Ljava/lang/Object;
.source "ServerExperiments_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/experiments/ServerExperiments;",
        ">;"
    }
.end annotation


# instance fields
.field private final allExperimentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final cachedResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;>;"
        }
    .end annotation
.end field

.field private final computationSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/PublicApiService;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/PublicApiService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->allExperimentsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->cachedResponseProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->serviceProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->userTokenProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->installationIdProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p9, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/experiments/ServerExperiments_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/PublicApiService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/experiments/ServerExperiments_Factory;"
        }
    .end annotation

    .line 69
    new-instance v10, Lcom/squareup/experiments/ServerExperiments_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/experiments/ServerExperiments_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Ljava/util/List;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/PublicApiService;Lio/reactivex/Scheduler;Ljavax/inject/Provider;Ljavax/inject/Provider;Lio/reactivex/Scheduler;)Lcom/squareup/experiments/ServerExperiments;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/account/LegacyAuthenticator;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/experiments/ExperimentMap;",
            ">;",
            "Lcom/squareup/server/PublicApiService;",
            "Lio/reactivex/Scheduler;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/experiments/ServerExperiments;"
        }
    .end annotation

    .line 77
    new-instance v10, Lcom/squareup/experiments/ServerExperiments;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/experiments/ServerExperiments;-><init>(Ljava/util/List;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/PublicApiService;Lio/reactivex/Scheduler;Ljavax/inject/Provider;Ljavax/inject/Provider;Lio/reactivex/Scheduler;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/experiments/ServerExperiments;
    .locals 10

    .line 59
    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->allExperimentsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/List;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->cachedResponseProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/server/PublicApiService;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    iget-object v7, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->userTokenProvider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->installationIdProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/experiments/ServerExperiments_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v9}, Lcom/squareup/experiments/ServerExperiments_Factory;->newInstance(Ljava/util/List;Lcom/squareup/analytics/Analytics;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/PublicApiService;Lio/reactivex/Scheduler;Ljavax/inject/Provider;Ljavax/inject/Provider;Lio/reactivex/Scheduler;)Lcom/squareup/experiments/ServerExperiments;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/experiments/ServerExperiments_Factory;->get()Lcom/squareup/experiments/ServerExperiments;

    move-result-object v0

    return-object v0
.end method
