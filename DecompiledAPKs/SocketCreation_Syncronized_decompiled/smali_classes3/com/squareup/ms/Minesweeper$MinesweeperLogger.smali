.class public interface abstract Lcom/squareup/ms/Minesweeper$MinesweeperLogger;
.super Ljava/lang/Object;
.source "Minesweeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ms/Minesweeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MinesweeperLogger"
.end annotation


# virtual methods
.method public abstract logMinesweeperEvent(Ljava/lang/String;)V
.end method

.method public abstract onMinesweeperFailureToInitialize(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V
.end method
