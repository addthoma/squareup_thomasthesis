.class public final Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;
.super Ljava/lang/Object;
.source "ReleaseMinesweeperModule_GetMsFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ms/MsFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final dataListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$DataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final executorServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final msProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Ms;",
            ">;"
        }
    .end annotation
.end field

.field private final noopMinesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$DataListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Ms;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->executorServiceProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->dataListenerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->libraryLoaderProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->minesweeperLoggerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->msProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p8, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->noopMinesweeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$DataListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper$MinesweeperLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Ms;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)",
            "Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;"
        }
    .end annotation

    .line 64
    new-instance v9, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static getMsFactory(Landroid/app/Application;Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/ms/Minesweeper$DataListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms;Lcom/squareup/ms/Minesweeper;)Lcom/squareup/ms/MsFactory;
    .locals 0

    .line 71
    invoke-static/range {p0 .. p7}, Lcom/squareup/ms/ReleaseMinesweeperModule;->getMsFactory(Landroid/app/Application;Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/ms/Minesweeper$DataListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms;Lcom/squareup/ms/Minesweeper;)Lcom/squareup/ms/MsFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/MsFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ms/MsFactory;
    .locals 9

    .line 54
    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->executorServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ms/MinesweeperExecutorService;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->dataListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ms/Minesweeper$DataListener;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->libraryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/loader/LibraryLoader;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->minesweeperLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->msProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ms/Ms;

    iget-object v0, p0, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->noopMinesweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ms/Minesweeper;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->getMsFactory(Landroid/app/Application;Lcom/squareup/ms/MinesweeperExecutorService;Lcom/squareup/ms/Minesweeper$DataListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms;Lcom/squareup/ms/Minesweeper;)Lcom/squareup/ms/MsFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ms/ReleaseMinesweeperModule_GetMsFactoryFactory;->get()Lcom/squareup/ms/MsFactory;

    move-result-object v0

    return-object v0
.end method
