.class public interface abstract Lcom/squareup/ms/AppFunctionStatusListener;
.super Ljava/lang/Object;
.source "AppFunctionStatusListener.java"


# virtual methods
.method public abstract onStatusUpdate(Lcom/squareup/ms/AppFunction;Lcom/squareup/protos/client/flipper/AugmentedStatus;)V
.end method

.method public abstract onStatusUpdateInvalidAppFunction(I)V
.end method

.method public abstract onStatusUpdateInvalidData(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method
