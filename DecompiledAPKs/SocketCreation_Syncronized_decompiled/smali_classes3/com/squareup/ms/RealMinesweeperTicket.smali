.class public Lcom/squareup/ms/RealMinesweeperTicket;
.super Ljava/lang/Object;
.source "RealMinesweeperTicket.java"

# interfaces
.implements Lcom/squareup/ms/MinesweeperTicket;


# static fields
.field private static final BLOCKING_PERIOD:I = 0xa

.field private static final TICKET_FILE_NAME:Ljava/lang/String; = "register_ms_ticket"


# instance fields
.field private final filesDir:Ljava/io/File;

.field private final minesweeper:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final newTicketReceived:Ljava/util/concurrent/locks/Condition;

.field private final newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

.field private ticket:Lcom/squareup/protos/client/flipper/SealedTicket;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    .line 25
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceived:Ljava/util/concurrent/locks/Condition;

    .line 32
    iput-object p1, p0, Lcom/squareup/ms/RealMinesweeperTicket;->filesDir:Ljava/io/File;

    .line 33
    iput-object p2, p0, Lcom/squareup/ms/RealMinesweeperTicket;->minesweeper:Ljavax/inject/Provider;

    .line 35
    invoke-direct {p0}, Lcom/squareup/ms/RealMinesweeperTicket;->getTicketFile()Ljava/io/File;

    move-result-object p1

    .line 36
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 37
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int p2, v0

    new-array p2, p2, [B

    .line 39
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 40
    invoke-virtual {v0, p2}, Ljava/io/FileInputStream;->read([B)I

    .line 41
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 48
    :try_start_1
    sget-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/flipper/SealedTicket;

    iput-object p2, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 50
    :catch_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catch_1
    move-exception p1

    const-string p2, "Failed to read ms ticket"

    .line 43
    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private getTicketFile()Ljava/io/File;
    .locals 3

    .line 133
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/ms/RealMinesweeperTicket;->filesDir:Ljava/io/File;

    const-string v2, "register_ms_ticket"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getFreshTicket()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/16 v0, 0xa

    .line 90
    invoke-virtual {p0, v0}, Lcom/squareup/ms/RealMinesweeperTicket;->getFreshTicketImpl(I)[B

    move-result-object v0

    return-object v0
.end method

.method protected getFreshTicketImpl(I)[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .line 98
    monitor-enter p0

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 102
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v0, :cond_4

    .line 105
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v2, 0xd

    .line 106
    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->add(II)V

    .line 107
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    .line 109
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 111
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->minesweeper:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    invoke-interface {v0}, Lcom/squareup/ms/Minesweeper;->updateTicketAsync()V

    :goto_2
    if-eqz v1, :cond_3

    .line 115
    :try_start_1
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    iget-object v2, v2, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_3

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceived:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0, p1}, Ljava/util/concurrent/locks/Condition;->awaitUntil(Ljava/util/Date;)Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    .line 119
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 120
    throw p1

    .line 119
    :cond_3
    iget-object p1, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 123
    :cond_4
    monitor-enter p0

    .line 124
    :try_start_2
    iget-object p1, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    if-nez p1, :cond_5

    const/4 p1, 0x0

    .line 125
    monitor-exit p0

    return-object p1

    .line 128
    :cond_5
    sget-object p1, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {p1, v0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    .line 129
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1

    :catchall_2
    move-exception p1

    .line 102
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw p1
.end method

.method public getTicket()Lcom/squareup/protos/client/flipper/SealedTicket;
    .locals 1

    .line 79
    monitor-enter p0

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    .line 81
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTicket(Lcom/squareup/protos/client/flipper/SealedTicket;)V
    .locals 3

    .line 56
    monitor-enter p0

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ms/RealMinesweeperTicket;->ticket:Lcom/squareup/protos/client/flipper/SealedTicket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 62
    :try_start_1
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceived:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    :try_start_2
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 66
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 69
    :try_start_3
    invoke-direct {p0}, Lcom/squareup/ms/RealMinesweeperTicket;->getTicketFile()Ljava/io/File;

    move-result-object v0

    .line 70
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 71
    sget-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 72
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Failed to write ms ticket"

    .line 74
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    .line 64
    :try_start_4
    iget-object v0, p0, Lcom/squareup/ms/RealMinesweeperTicket;->newTicketReceivedLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 65
    throw p1

    :catchall_1
    move-exception p1

    .line 66
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1
.end method
