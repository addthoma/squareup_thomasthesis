.class public final Lcom/squareup/disputes/DisputesTutorial;
.super Ljava/lang/Object;
.source "DisputesTutorial.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000eH\u0016J\u0008\u0010\u0012\u001a\u00020\u000eH\u0016J\u001a\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0018\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u000eH\u0002J\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u001dH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesTutorial;",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "runner",
        "Lcom/squareup/disputes/DisputesTutorialRunner;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/disputes/DisputesTutorialRunner;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/analytics/Analytics;)V",
        "output",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "kotlin.jvm.PlatformType",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitRequested",
        "onExitScope",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "stopImmediately",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

.field private final output:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/disputes/DisputesTutorialRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/DisputesTutorialRunner;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlesDisputes"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorial;->runner:Lcom/squareup/disputes/DisputesTutorialRunner;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesTutorial;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<TutorialState>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method private final stopImmediately()V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorial;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-interface {v0}, Lcom/squareup/disputes/api/HandlesDisputes;->markPopupClosed()V

    .line 58
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 32
    iget-object p1, p0, Lcom/squareup/disputes/DisputesTutorial;->runner:Lcom/squareup/disputes/DisputesTutorialRunner;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesTutorialRunner;->showPrompt()V

    return-void
.end method

.method public onExitRequested()V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const-string p2, "name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const v0, -0x2947e0cb

    if-eq p2, v0, :cond_1

    const v0, -0xc1c675b

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, "DisputesTutorialDialog Cancel"

    .line 48
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 49
    iget-object p1, p0, Lcom/squareup/disputes/DisputesTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->canceledNotificationPopup(Lcom/squareup/analytics/Analytics;)V

    .line 50
    invoke-direct {p0}, Lcom/squareup/disputes/DisputesTutorial;->stopImmediately()V

    goto :goto_0

    :cond_1
    const-string p2, "DisputesTutorialDialog Open"

    .line 43
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 44
    iget-object p1, p0, Lcom/squareup/disputes/DisputesTutorial;->runner:Lcom/squareup/disputes/DisputesTutorialRunner;

    invoke-virtual {p1}, Lcom/squareup/disputes/DisputesTutorialRunner;->jumpToDisputes()V

    .line 45
    iget-object p1, p0, Lcom/squareup/disputes/DisputesTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->popupGoToDisputes(Lcom/squareup/analytics/Analytics;)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/disputes/DisputesTutorial;->stopImmediately()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
