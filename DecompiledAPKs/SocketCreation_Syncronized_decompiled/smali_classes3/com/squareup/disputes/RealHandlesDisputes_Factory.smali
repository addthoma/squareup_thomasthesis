.class public final Lcom/squareup/disputes/RealHandlesDisputes_Factory;
.super Ljava/lang/Object;
.source "RealHandlesDisputes_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/RealHandlesDisputes;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final lastSeenDisputesPopupProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final lastSeenDisputesReportProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->lastSeenDisputesReportProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->lastSeenDisputesPopupProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/RealHandlesDisputes_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/disputes/RealHandlesDisputes_Factory;"
        }
    .end annotation

    .line 62
    new-instance v8, Lcom/squareup/disputes/RealHandlesDisputes_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/disputes/RealHandlesDisputes_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lio/reactivex/Scheduler;)Lcom/squareup/disputes/RealHandlesDisputes;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/util/Clock;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/disputes/RealHandlesDisputes;"
        }
    .end annotation

    .line 69
    new-instance v8, Lcom/squareup/disputes/RealHandlesDisputes;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/disputes/RealHandlesDisputes;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lio/reactivex/Scheduler;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/RealHandlesDisputes;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->lastSeenDisputesReportProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->lastSeenDisputesPopupProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v7}, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Clock;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lio/reactivex/Scheduler;)Lcom/squareup/disputes/RealHandlesDisputes;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/disputes/RealHandlesDisputes_Factory;->get()Lcom/squareup/disputes/RealHandlesDisputes;

    move-result-object v0

    return-object v0
.end method
