.class final Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;
.super Ljava/lang/Object;
.source "AllDisputesAdapter.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;->bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;

.field final synthetic this$0:Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;->this$0:Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;->$data:Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 305
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;->this$0:Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;

    invoke-virtual {v0}, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;->getWorkflow()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    new-instance v1, Lcom/squareup/disputes/AllDisputesEvent$LoadMoreFromAllDisputes;

    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;->$data:Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;

    invoke-virtual {v2}, Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;->getCursor()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/disputes/AllDisputesEvent$LoadMoreFromAllDisputes;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
