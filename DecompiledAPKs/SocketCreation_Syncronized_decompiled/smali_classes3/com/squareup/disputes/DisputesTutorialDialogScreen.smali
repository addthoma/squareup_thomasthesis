.class public final Lcom/squareup/disputes/DisputesTutorialDialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "DisputesTutorialDialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesTutorialDialogScreen$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003R\u000e\u0010\u0004\u001a\u00020\u0005X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesTutorialDialogScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "CANCEL",
        "",
        "OPEN_DISPUTES",
        "Factory",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CANCEL:Ljava/lang/String; = "DisputesTutorialDialog Cancel"

.field public static final INSTANCE:Lcom/squareup/disputes/DisputesTutorialDialogScreen;

.field public static final OPEN_DISPUTES:Ljava/lang/String; = "DisputesTutorialDialog Open"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/disputes/DisputesTutorialDialogScreen;

    invoke-direct {v0}, Lcom/squareup/disputes/DisputesTutorialDialogScreen;-><init>()V

    sput-object v0, Lcom/squareup/disputes/DisputesTutorialDialogScreen;->INSTANCE:Lcom/squareup/disputes/DisputesTutorialDialogScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .line 18
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
