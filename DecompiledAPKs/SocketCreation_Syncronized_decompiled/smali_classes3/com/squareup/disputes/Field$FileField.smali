.class public final Lcom/squareup/disputes/Field$FileField;
.super Lcom/squareup/disputes/Field;
.source "Field.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FileField"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nField.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Field.kt\ncom/squareup/disputes/Field$FileField\n+ 2 Gsons.kt\ncom/squareup/gson/GsonsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,144:1\n16#2:145\n1642#3,2:146\n*E\n*S KotlinDebug\n*F\n+ 1 Field.kt\ncom/squareup/disputes/Field$FileField\n*L\n117#1:145\n118#1,2:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u0014\u001a\u00020\u0015J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0006\u0010\u001c\u001a\u00020\u0015R \u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\r\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/disputes/Field$FileField;",
        "Lcom/squareup/disputes/Field;",
        "section",
        "Lcom/squareup/protos/client/irf/Section;",
        "fieldProto",
        "Lcom/squareup/protos/client/irf/Field;",
        "gson",
        "Lcom/google/gson/Gson;",
        "(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Lcom/google/gson/Gson;)V",
        "files",
        "",
        "Lcom/squareup/disputes/FileUpload;",
        "getFiles",
        "()Ljava/util/List;",
        "setFiles",
        "(Ljava/util/List;)V",
        "options",
        "",
        "Lcom/squareup/protos/client/irf/Option;",
        "getOptions",
        "deleteFile",
        "",
        "hasAnswer",
        "",
        "meetsCriteria",
        "",
        "value",
        "",
        "uploadFile",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private files:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/disputes/FileUpload;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Lcom/google/gson/Gson;)V
    .locals 11

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldProto"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7c

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 108
    invoke-direct/range {v1 .. v10}, Lcom/squareup/disputes/Field;-><init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 112
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/disputes/Field$FileField;->files:Ljava/util/List;

    .line 113
    iget-object p1, p2, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    const-string v0, "fieldProto.option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/disputes/Field$FileField;->options:Ljava/util/List;

    .line 116
    iget-object p1, p2, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    const-string v0, "fieldProto.value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 117
    iget-object p1, p2, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    new-instance p2, Lcom/squareup/disputes/Field$FileField$$special$$inlined$fromJson$1;

    invoke-direct {p2}, Lcom/squareup/disputes/Field$FileField$$special$$inlined$fromJson$1;-><init>()V

    invoke-virtual {p2}, Lcom/squareup/disputes/Field$FileField$$special$$inlined$fromJson$1;->getType()Ljava/lang/reflect/Type;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/disputes/Field$FileField;->files:Ljava/util/List;

    .line 118
    iget-object p1, p0, Lcom/squareup/disputes/Field$FileField;->files:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 146
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/disputes/FileUpload;

    .line 118
    iget-object p3, p0, Lcom/squareup/disputes/Field$FileField;->options:Ljava/util/List;

    invoke-virtual {p2, p3}, Lcom/squareup/disputes/FileUpload;->setUpFormattedCategory(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final deleteFile()V
    .locals 0

    return-void
.end method

.method public final getFiles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/disputes/FileUpload;",
            ">;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/disputes/Field$FileField;->files:Ljava/util/List;

    return-object v0
.end method

.method public final getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/disputes/Field$FileField;->options:Ljava/util/List;

    return-object v0
.end method

.method public hasAnswer()Z
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/disputes/Field$FileField;->files:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/disputes/Field$FileField;->isSkipped()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public meetsCriteria(Ljava/lang/String;)Ljava/lang/Void;
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Criteria targets file field"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic meetsCriteria(Ljava/lang/String;)Z
    .locals 0

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/disputes/Field$FileField;->meetsCriteria(Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public final setFiles(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/disputes/FileUpload;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iput-object p1, p0, Lcom/squareup/disputes/Field$FileField;->files:Ljava/util/List;

    return-void
.end method

.method public final uploadFile()V
    .locals 1

    const/4 v0, 0x0

    .line 124
    invoke-virtual {p0, v0}, Lcom/squareup/disputes/Field$FileField;->setSkipped(Z)V

    return-void
.end method
