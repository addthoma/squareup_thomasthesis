.class public final Lcom/squareup/disputes/DisputesDetailCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DisputesDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesDetailCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesDetailCoordinator.kt\ncom/squareup/disputes/DisputesDetailCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,294:1\n1103#2,7:295\n1103#2,7:302\n1103#2,7:309\n1103#2,7:316\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesDetailCoordinator.kt\ncom/squareup/disputes/DisputesDetailCoordinator\n*L\n146#1,7:295\n147#1,7:302\n150#1,7:309\n166#1,7:316\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001:\u0001BBW\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000c\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u0010+\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0002J(\u0010,\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0016\u0010-\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u001e\u0010.\u001a\u00020(2\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u00020\u0006002\u0006\u00101\u001a\u000202H\u0002J\u0012\u00103\u001a\u00020(*\u0008\u0012\u0004\u0012\u00020\u000600H\u0002J\u000c\u00104\u001a\u000205*\u000202H\u0002J\u000c\u00106\u001a\u000207*\u00020\nH\u0002J\u0016\u00106\u001a\u000207*\u0002082\u0008\u0008\u0001\u00109\u001a\u000205H\u0002J\u0016\u0010:\u001a\u000207*\u00020;2\u0008\u0008\u0001\u00109\u001a\u000205H\u0002J\u000c\u0010<\u001a\u000207*\u00020;H\u0002J\u000c\u0010=\u001a\u000207*\u000208H\u0002J\u000c\u0010>\u001a\u000207*\u00020;H\u0002J\u000c\u0010?\u001a\u000207*\u000202H\u0002J\u000c\u0010@\u001a\u00020A*\u000202H\u0002R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006C"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesDetailCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
        "Lcom/squareup/disputes/DetailEvent;",
        "Lcom/squareup/disputes/DisputesDetailScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "mediumDateFormatter",
        "Ljava/text/DateFormat;",
        "mediumDateNoYearFormatter",
        "dateTimeFormatter",
        "locale",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V",
        "acceptButton",
        "Landroid/widget/Button;",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "cardRow",
        "Lcom/squareup/noho/NohoRow;",
        "challengeButton",
        "context",
        "Landroid/content/Context;",
        "deadlineRow",
        "disputedAmountRow",
        "heldAmountRow",
        "heldAmountText",
        "Lcom/squareup/marketfont/MarketTextView;",
        "overviewRow",
        "reasonRow",
        "receiptRow",
        "transactionAmountRow",
        "transactionDateRow",
        "viewSubmissionRow",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onScreen",
        "screen",
        "setUpActionBar",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "disputedPayment",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "backFromDetail",
        "descriptionForReason",
        "",
        "format",
        "",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "phraseId",
        "formatInString",
        "Lcom/squareup/protos/common/time/DateTime;",
        "formatMedium",
        "formatMediumNoYear",
        "formatMediumWithTime",
        "getOverviewText",
        "hasOverviewText",
        "",
        "Factory",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private acceptButton:Landroid/widget/Button;

.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private cardRow:Lcom/squareup/noho/NohoRow;

.field private challengeButton:Landroid/widget/Button;

.field private context:Landroid/content/Context;

.field private final dateTimeFormatter:Ljava/text/DateFormat;

.field private deadlineRow:Lcom/squareup/noho/NohoRow;

.field private disputedAmountRow:Lcom/squareup/noho/NohoRow;

.field private heldAmountRow:Lcom/squareup/noho/NohoRow;

.field private heldAmountText:Lcom/squareup/marketfont/MarketTextView;

.field private final locale:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mediumDateFormatter:Ljava/text/DateFormat;

.field private final mediumDateNoYearFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private overviewRow:Lcom/squareup/noho/NohoRow;

.field private reasonRow:Lcom/squareup/noho/NohoRow;

.field private receiptRow:Lcom/squareup/noho/NohoRow;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
            "Lcom/squareup/disputes/DetailEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private transactionAmountRow:Lcom/squareup/noho/NohoRow;

.field private transactionDateRow:Lcom/squareup/noho/NohoRow;

.field private viewSubmissionRow:Lcom/squareup/noho/NohoRow;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
            "Lcom/squareup/disputes/DetailEvent;",
            ">;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediumDateFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediumDateNoYearFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateTimeFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->mediumDateFormatter:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->mediumDateNoYearFormatter:Ljava/text/DateFormat;

    iput-object p5, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->dateTimeFormatter:Ljava/text/DateFormat;

    iput-object p6, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->locale:Ljavax/inject/Provider;

    return-void
.end method

.method public static final synthetic access$backFromDetail(Lcom/squareup/disputes/DisputesDetailCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->backFromDetail(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/disputes/DisputesDetailCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/disputes/DisputesDetailCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final backFromDetail(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/DetailEvent;",
            ">;)V"
        }
    .end annotation

    .line 222
    sget-object v0, Lcom/squareup/disputes/DetailEvent$BackFromDisputesDetail;->INSTANCE:Lcom/squareup/disputes/DetailEvent$BackFromDisputesDetail;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    .line 205
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 206
    sget v0, Lcom/squareup/disputes/R$id;->disputed_amount_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->disputedAmountRow:Lcom/squareup/noho/NohoRow;

    .line 207
    sget v0, Lcom/squareup/disputes/R$id;->deadline_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->deadlineRow:Lcom/squareup/noho/NohoRow;

    .line 208
    sget v0, Lcom/squareup/disputes/R$id;->reason_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->reasonRow:Lcom/squareup/noho/NohoRow;

    .line 209
    sget v0, Lcom/squareup/disputes/R$id;->held_amount_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->heldAmountRow:Lcom/squareup/noho/NohoRow;

    .line 210
    sget v0, Lcom/squareup/disputes/R$id;->receipt_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->receiptRow:Lcom/squareup/noho/NohoRow;

    .line 211
    sget v0, Lcom/squareup/disputes/R$id;->transaction_amount_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->transactionAmountRow:Lcom/squareup/noho/NohoRow;

    .line 212
    sget v0, Lcom/squareup/disputes/R$id;->transaction_date_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->transactionDateRow:Lcom/squareup/noho/NohoRow;

    .line 213
    sget v0, Lcom/squareup/disputes/R$id;->card_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->cardRow:Lcom/squareup/noho/NohoRow;

    .line 214
    sget v0, Lcom/squareup/disputes/R$id;->overview_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->overviewRow:Lcom/squareup/noho/NohoRow;

    .line 215
    sget v0, Lcom/squareup/disputes/R$id;->held_amount_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->heldAmountText:Lcom/squareup/marketfont/MarketTextView;

    .line 216
    sget v0, Lcom/squareup/disputes/R$id;->challenge_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->challengeButton:Landroid/widget/Button;

    .line 217
    sget v0, Lcom/squareup/disputes/R$id;->accept_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->acceptButton:Landroid/widget/Button;

    .line 218
    sget v0, Lcom/squareup/disputes/R$id;->view_submission:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->viewSubmissionRow:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method private final descriptionForReason(Lcom/squareup/protos/client/cbms/DisputedPayment;)I
    .locals 1

    .line 247
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 262
    :pswitch_0
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_unknown:I

    goto :goto_0

    .line 261
    :pswitch_1
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_unauthorized:I

    goto :goto_0

    .line 260
    :pswitch_2
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_paid_by_other_means:I

    goto :goto_0

    .line 259
    :pswitch_3
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_not_received:I

    goto :goto_0

    .line 258
    :pswitch_4
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_not_as_described:I

    goto :goto_0

    .line 257
    :pswitch_5
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_no_knowledge:I

    goto :goto_0

    .line 256
    :pswitch_6
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_fraud:I

    goto :goto_0

    .line 255
    :pswitch_7
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_emv_liability_shift:I

    goto :goto_0

    .line 254
    :pswitch_8
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_duplicate:I

    goto :goto_0

    .line 253
    :pswitch_9
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_dissatisfied:I

    goto :goto_0

    .line 252
    :pswitch_a
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_customer_requests_credit:I

    goto :goto_0

    .line 250
    :pswitch_b
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_compliance:I

    goto :goto_0

    .line 249
    :pswitch_c
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_cancelled:I

    goto :goto_0

    .line 248
    :pswitch_d
    sget p1, Lcom/squareup/disputes/R$string;->dispute_reason_amount_differs:I

    :goto_0
    return p1

    .line 262
    :cond_0
    :goto_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(this)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final format(Lcom/squareup/protos/common/time/YearMonthDay;I)Ljava/lang/CharSequence;
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v1, "context"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 243
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->formatMediumNoYear(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "date"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(context, phr\u2026Year())\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final formatInString(Lcom/squareup/protos/common/time/DateTime;I)Ljava/lang/CharSequence;
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v1, "context"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 238
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->formatMedium(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "date"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 239
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(context, phr\u2026dium())\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final formatMedium(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->mediumDateFormatter:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->locale:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "mediumDateFormatter.form\u2026Date(this, locale.get()))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final formatMediumNoYear(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;
    .locals 1

    .line 228
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->mediumDateNoYearFormatter:Ljava/text/DateFormat;

    invoke-static {p1}, Lcom/squareup/disputes/AllDisputesAdapterKt;->asDate(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "mediumDateNoYearFormatter.format(asDate())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final formatMediumWithTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->dateTimeFormatter:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->locale:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "dateTimeFormatter.format\u2026Date(this, locale.get()))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final getOverviewText(Lcom/squareup/protos/client/cbms/DisputedPayment;)Ljava/lang/CharSequence;
    .locals 5

    .line 267
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "context"

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v0

    aget v0, v4, v0

    if-eq v0, v2, :cond_10

    if-eq v0, v1, :cond_f

    const/4 v4, 0x3

    if-eq v0, v4, :cond_d

    const/4 v4, 0x4

    if-eq v0, v4, :cond_b

    .line 273
    :goto_0
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    sget-object v4, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v0

    aget v0, v4, v0

    if-eq v0, v2, :cond_9

    if-eq v0, v1, :cond_7

    .line 280
    :goto_1
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result p1

    aget p1, v0, p1

    if-eq p1, v2, :cond_5

    if-eq p1, v1, :cond_3

    :goto_2
    const-string p1, ""

    goto :goto_3

    .line 282
    :cond_3
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_under_review:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 281
    :cond_5
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_pending:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_3
    const-string/jumbo v0, "when (resolution) {\n    \u2026   else -> \"\"\n          }"

    .line 280
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_4

    .line 277
    :cond_7
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_8

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 278
    :cond_8
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_no_action_refunded:I

    .line 277
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(\n     \u2026tion_refunded\n          )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_4

    .line 274
    :cond_9
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_a

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 275
    :cond_a
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_no_action_low:I

    .line 274
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(\n     \u2026no_action_low\n          )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    :goto_4
    return-object p1

    .line 271
    :cond_b
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_c

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_reopened:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.stri\u2026ail_description_reopened)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_5

    .line 270
    :cond_d
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_won:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.stri\u2026e_detail_description_won)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_5

    .line 269
    :cond_f
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    const-string v0, "resolution_decided_at"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_lost:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/disputes/DisputesDetailCoordinator;->formatInString(Lcom/squareup/protos/common/time/DateTime;I)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_5

    .line 268
    :cond_10
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p1, :cond_11

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    sget v0, Lcom/squareup/disputes/R$string;->dispute_detail_description_accepted:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.stri\u2026ail_description_accepted)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    :goto_5
    return-object p1
.end method

.method private final hasOverviewText(Lcom/squareup/protos/client/cbms/DisputedPayment;)Z
    .locals 2

    .line 292
    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-eq v0, v1, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sget-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
            "Lcom/squareup/disputes/DetailEvent;",
            ">;)V"
        }
    .end annotation

    .line 107
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 108
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/disputes/DisputesDetail$ScreenData;

    .line 109
    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$1;-><init>(Lcom/squareup/disputes/DisputesDetailCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 110
    invoke-virtual {p2}, Lcom/squareup/disputes/DisputesDetail$ScreenData;->getDisputedPayment()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    .line 112
    invoke-virtual {p2}, Lcom/squareup/disputes/DisputesDetail$ScreenData;->getDisputedPayment()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    .line 113
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->disputedAmountRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_0

    const-string v1, "disputedAmountRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    const-string v2, "disputedPayment.current_disputed_amount"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 114
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->reasonRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_1

    const-string v1, "reasonRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    const-string v2, "context"

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->descriptionForReason(Lcom/squareup/protos/client/cbms/DisputedPayment;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 115
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->heldAmountRow:Lcom/squareup/noho/NohoRow;

    const-string v1, "heldAmountRow"

    if-nez p2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    const-string v4, "disputedPayment.current_amount_held"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/squareup/disputes/DisputesDetailCoordinator;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 117
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->hasOverviewText(Lcom/squareup/protos/client/cbms/DisputedPayment;)Z

    move-result p2

    const/4 v3, 0x0

    const-string v4, "overviewRow"

    if-eqz p2, :cond_9

    .line 118
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->overviewRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->getOverviewText(Lcom/squareup/protos/client/cbms/DisputedPayment;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 119
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->overviewRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 120
    :cond_5
    iget-object v4, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sget-object v5, Lcom/squareup/protos/client/cbms/ActionableStatus;->ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-ne v4, v5, :cond_6

    .line 121
    sget v4, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Actionable:I

    goto :goto_0

    .line 123
    :cond_6
    iget-object v4, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    sget-object v5, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-ne v4, v5, :cond_7

    .line 124
    sget v4, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Won:I

    goto :goto_0

    .line 126
    :cond_7
    iget-object v4, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    sget-object v5, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-ne v4, v5, :cond_8

    .line 127
    sget v4, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Lost:I

    goto :goto_0

    .line 130
    :cond_8
    sget v4, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Pending:I

    .line 119
    :goto_0
    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoRow;->setDescriptionAppearanceId(I)V

    goto :goto_1

    .line 134
    :cond_9
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->overviewRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    move-object v4, v3

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 137
    :goto_1
    invoke-static {}, Lcom/squareup/disputes/DisputesDetailKt;->getOPEN_DISPUTE_RESOLUTIONS()Ljava/util/List;

    move-result-object p2

    iget-object v4, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-interface {p2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_14

    .line 138
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v4, 0x0

    if-ne p2, v1, :cond_11

    .line 140
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->deadlineRow:Lcom/squareup/noho/NohoRow;

    const-string v1, "deadlineRow"

    if-nez p2, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 139
    :cond_b
    iget-object v5, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    const-string v6, "disputedPayment.information_request"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/cbms/InformationRequest;

    iget-object v5, v5, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    const-string v6, "disputedPayment.informat\u2026         .customer_due_at"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-direct {p0, v5}, Lcom/squareup/disputes/DisputesDetailCoordinator;->formatMedium(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 141
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->deadlineRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 143
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->challengeButton:Landroid/widget/Button;

    const-string v1, "challengeButton"

    if-nez p2, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 144
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->acceptButton:Landroid/widget/Button;

    const-string v5, "acceptButton"

    if-nez p2, :cond_e

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 146
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->challengeButton:Landroid/widget/Button;

    if-nez p2, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast p2, Landroid/view/View;

    .line 295
    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, v0}, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->acceptButton:Landroid/widget/Button;

    if-nez p2, :cond_10

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast p2, Landroid/view/View;

    .line 302
    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$2;

    invoke-direct {v1, v0}, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 148
    :cond_11
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->VIEWABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-ne p2, v1, :cond_17

    .line 149
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->viewSubmissionRow:Lcom/squareup/noho/NohoRow;

    const-string/jumbo v1, "viewSubmissionRow"

    if-nez p2, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p2, v4}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 150
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->viewSubmissionRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    check-cast p2, Landroid/view/View;

    .line 309
    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$3;

    invoke-direct {v1, v0}, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 153
    :cond_14
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->heldAmountRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_15

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    const/16 v1, 0x8

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 154
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->heldAmountText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p2, :cond_16

    const-string v4, "heldAmountText"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 157
    :cond_17
    :goto_2
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->transactionAmountRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_18

    const-string/jumbo v1, "transactionAmountRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    const-string v4, "disputedPayment.payment_amount"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 158
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->transactionDateRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_19

    const-string/jumbo v1, "transactionDateRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_19
    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->formatMediumWithTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;

    move-result-object v3

    :cond_1a
    invoke-virtual {p2, v3}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 160
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez p2, :cond_1b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1b
    iget-object v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    const-string v3, "disputedPayment.payment_card_brand"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/disputes/DisputesDetailKt;->nameForInstrument(Lcom/squareup/protos/common/instrument/InstrumentType;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v1, "context.getString(nameFo\u2026ment.payment_card_brand))"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->cardRow:Lcom/squareup/noho/NohoRow;

    if-nez v1, :cond_1c

    const-string v3, "cardRow"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v1, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 165
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->receiptRow:Lcom/squareup/noho/NohoRow;

    const-string v1, "receiptRow"

    if-nez p2, :cond_1d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 163
    :cond_1d
    iget-object v3, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->context:Landroid/content/Context;

    if-nez v3, :cond_1e

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1e
    sget v2, Lcom/squareup/disputes/R$string;->dispute_receipt_token:I

    invoke-static {v3, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 164
    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    const-string v4, "disputedPayment.payment_token"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lkotlin/text/StringsKt;->takeLast(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    const-string/jumbo v4, "token"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 165
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 166
    iget-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->receiptRow:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_1f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1f
    check-cast p2, Landroid/view/View;

    .line 316
    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$4;

    invoke-direct {v1, v0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator$onScreen$$inlined$onClickDebounced$4;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/client/cbms/DisputedPayment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/DetailEvent;",
            ">;",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ")V"
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 176
    iget-object v1, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    sget-object v2, Lcom/squareup/disputes/DisputesDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 195
    :pswitch_0
    new-instance p2, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/disputes/R$string;->dispute_detail_title_pending:I

    invoke-direct {p2, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p2, Lcom/squareup/resources/TextModel;

    .line 194
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_1

    .line 192
    :pswitch_1
    new-instance p2, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/disputes/R$string;->dispute_detail_title_under_review:I

    invoke-direct {p2, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p2, Lcom/squareup/resources/TextModel;

    .line 191
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_1

    .line 189
    :pswitch_2
    new-instance p2, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/disputes/R$string;->dispute_detail_title_closed:I

    invoke-direct {p2, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p2, Lcom/squareup/resources/TextModel;

    .line 188
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_1

    .line 179
    :pswitch_3
    invoke-static {}, Lcom/squareup/disputes/DisputesDetailKt;->getNO_ACTION_STATUSES()Ljava/util/List;

    move-result-object v1

    iget-object v2, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    new-instance p2, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/disputes/R$string;->dispute_detail_title_pending:I

    invoke-direct {p2, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p2, Lcom/squareup/util/ViewString;

    goto :goto_0

    .line 182
    :cond_1
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    iget-object p2, p2, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const-string v2, "disputedPayment.latest_reporting_date"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v2, Lcom/squareup/disputes/R$string;->dispute_detail_title_open:I

    invoke-direct {p0, p2, v2}, Lcom/squareup/disputes/DisputesDetailCoordinator;->format(Lcom/squareup/protos/common/time/YearMonthDay;I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p2, v1

    check-cast p2, Lcom/squareup/util/ViewString;

    .line 184
    :goto_0
    check-cast p2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 199
    :goto_1
    sget-object p2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$setUpActionBar$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator$setUpActionBar$1;-><init>(Lcom/squareup/disputes/DisputesDetailCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, p2, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 200
    iget-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_2

    const-string p2, "actionBar"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator;->bindViews(Landroid/view/View;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetailCoordinator;->screens:Lio/reactivex/Observable;

    .line 99
    new-instance v1, Lcom/squareup/disputes/DisputesDetailCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/disputes/DisputesDetailCoordinator$attach$1;-><init>(Lcom/squareup/disputes/DisputesDetailCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .subscribe { onScreen(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
