.class public final Lcom/squareup/disputes/Starter;
.super Ljava/lang/Object;
.source "DisputesWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowStarter<",
        "Lcom/squareup/disputes/InitializeWorkflowEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesWorkflowRunner.kt\ncom/squareup/disputes/Starter\n*L\n1#1,100:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J`\u0010\t\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\u000cj\n\u0012\u0006\u0008\u0001\u0012\u00020\r`\u00100\u000b\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\nj\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003`\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/disputes/Starter;",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "Lcom/squareup/disputes/InitializeWorkflowEvent;",
        "",
        "reactor",
        "Lcom/squareup/disputes/DisputesReactor;",
        "renderer",
        "Lcom/squareup/disputes/DisputesRenderer;",
        "(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesRenderer;)V",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reactor:Lcom/squareup/disputes/DisputesReactor;

.field private final renderer:Lcom/squareup/disputes/DisputesRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/Starter;->reactor:Lcom/squareup/disputes/DisputesReactor;

    iput-object p2, p0, Lcom/squareup/disputes/Starter;->renderer:Lcom/squareup/disputes/DisputesRenderer;

    return-void
.end method

.method public static final synthetic access$getRenderer$p(Lcom/squareup/disputes/Starter;)Lcom/squareup/disputes/DisputesRenderer;
    .locals 0

    .line 82
    iget-object p0, p0, Lcom/squareup/disputes/Starter;->renderer:Lcom/squareup/disputes/DisputesRenderer;

    return-object p0
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/disputes/InitializeWorkflowEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 90
    sget-object v0, Lcom/squareup/disputes/DisputesSerializer;->INSTANCE:Lcom/squareup/disputes/DisputesSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/disputes/DisputesSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/disputes/DisputesState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/disputes/DisputesState$WaitingToStart;->INSTANCE:Lcom/squareup/disputes/DisputesState$WaitingToStart;

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    .line 91
    :goto_0
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 92
    iget-object v1, p0, Lcom/squareup/disputes/Starter;->reactor:Lcom/squareup/disputes/DisputesReactor;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/disputes/DisputesReactor;->launch(Lcom/squareup/disputes/DisputesState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 94
    new-instance v1, Lcom/squareup/disputes/Starter$start$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Lcom/squareup/disputes/Starter$start$1;-><init>(Lcom/squareup/disputes/Starter;Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
