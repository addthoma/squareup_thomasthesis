.class final Lcom/squareup/disputes/DisputesReactor$onReact$4$4;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/disputes/DetailEvent$Accept;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/disputes/DisputesState$ActionDialogState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/disputes/DisputesState$ActionDialogState;",
        "it",
        "Lcom/squareup/disputes/DetailEvent$Accept;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$4$4;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/disputes/DetailEvent$Accept;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DetailEvent$Accept;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/disputes/DisputesState$ActionDialogState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$4$4;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/disputes/DisputesReactor$onReact$4;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesReactor;->access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$4$4;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$4;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v0, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-virtual {v0}, Lcom/squareup/disputes/DisputesState$DetailState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    const-string v1, "state.dispute.payment_token"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/disputes/DisputesAnalyticsKt;->clickedRefund(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 125
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$4$4;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$4;

    iget-object v1, v1, Lcom/squareup/disputes/DisputesReactor$onReact$4;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v1, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-virtual {v1}, Lcom/squareup/disputes/DisputesState$DetailState;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/disputes/DisputesState$ActionDialogState;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/DetailEvent$Accept;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$4$4;->invoke(Lcom/squareup/disputes/DetailEvent$Accept;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
