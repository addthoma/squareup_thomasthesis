.class public final Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;
.super Lcom/squareup/disputes/DisputesState$OverviewState;
.source "DisputesState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesState$OverviewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisputesLoaded"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
        "Lcom/squareup/disputes/DisputesState$OverviewState;",
        "response",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        "loadingMoreError",
        "",
        "(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Z)V",
        "getLoadingMoreError",
        "()Z",
        "getResponse",
        "()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        "component1",
        "component2",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loadingMoreError:Z

.field private final response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Z)V
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/disputes/DisputesState$OverviewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    iput-boolean p2, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 15
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;ZILjava/lang/Object;)Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->copy(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Z)Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    return v0
.end method

.method public final copy(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Z)Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    invoke-direct {v0, p1, p2}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    iget-object v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    iget-object v1, p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    iget-boolean p1, p1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLoadingMoreError()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    return v0
.end method

.method public final getResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisputesLoaded(response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->response:Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loadingMoreError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;->loadingMoreError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
