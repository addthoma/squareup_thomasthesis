.class public final synthetic Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/protos/common/instrument/InstrumentType;->values()[Lcom/squareup/protos/common/instrument/InstrumentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->ALIPAY:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->AMERICAN_EXPRESS:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->BALANCE:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->CASH_APP:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->DISCOVER:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->DISCOVER_DINERS:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->EFTPOS:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->FELICA:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->INTERAC:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->JCB:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->MASTER_CARD:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->UNION_PAY:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->UNKNOWN:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->VISA:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/DisputesDetailKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/common/instrument/InstrumentType;->EBT:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1

    return-void
.end method
