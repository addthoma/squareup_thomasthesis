.class public final Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;
.super Ljava/lang/Object;
.source "AuthenticatedSquareDownloadManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/AuthenticatedSquareDownloadManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/DownloadManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/DownloadManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p3, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/DownloadManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;)",
            "Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/DownloadManager;Ljavax/inject/Provider;Lcom/squareup/http/Server;)Lcom/squareup/http/AuthenticatedSquareDownloadManager;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/DownloadManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/http/Server;",
            ")",
            "Lcom/squareup/http/AuthenticatedSquareDownloadManager;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/http/AuthenticatedSquareDownloadManager;-><init>(Landroid/app/DownloadManager;Ljavax/inject/Provider;Lcom/squareup/http/Server;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/AuthenticatedSquareDownloadManager;
    .locals 3

    .line 28
    iget-object v0, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iget-object v1, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/http/Server;

    invoke-static {v0, v1, v2}, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->newInstance(Landroid/app/DownloadManager;Ljavax/inject/Provider;Lcom/squareup/http/Server;)Lcom/squareup/http/AuthenticatedSquareDownloadManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/http/AuthenticatedSquareDownloadManager_Factory;->get()Lcom/squareup/http/AuthenticatedSquareDownloadManager;

    move-result-object v0

    return-object v0
.end method
