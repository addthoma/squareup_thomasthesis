.class public final Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;
.super Ljava/lang/Object;
.source "AuthHttpInterceptor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/AuthHttpInterceptor;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/AuthHttpInterceptor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/http/interceptor/AuthHttpInterceptor;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/http/interceptor/AuthHttpInterceptor;

    invoke-direct {v0, p0}, Lcom/squareup/http/interceptor/AuthHttpInterceptor;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/AuthHttpInterceptor;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/AuthHttpInterceptor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/AuthHttpInterceptor_Factory;->get()Lcom/squareup/http/interceptor/AuthHttpInterceptor;

    move-result-object v0

    return-object v0
.end method
