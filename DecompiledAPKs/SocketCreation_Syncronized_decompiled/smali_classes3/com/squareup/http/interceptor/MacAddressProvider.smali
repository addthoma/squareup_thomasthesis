.class public final Lcom/squareup/http/interceptor/MacAddressProvider;
.super Ljava/lang/Object;
.source "MacAddressProvider.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMacAddressProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MacAddressProvider.kt\ncom/squareup/http/interceptor/MacAddressProvider\n*L\n1#1,23:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008\u0001\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001d\u0010\u0005\u001a\u0004\u0018\u00010\u00068FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/http/interceptor/MacAddressProvider;",
        "",
        "wifiManager",
        "Landroid/net/wifi/WifiManager;",
        "(Landroid/net/wifi/WifiManager;)V",
        "macAddress",
        "",
        "getMacAddress",
        "()Ljava/lang/String;",
        "macAddress$delegate",
        "Lkotlin/Lazy;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final macAddress$delegate:Lkotlin/Lazy;

.field private final wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/net/wifi/WifiManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "wifiManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/http/interceptor/MacAddressProvider;->wifiManager:Landroid/net/wifi/WifiManager;

    .line 14
    new-instance p1, Lcom/squareup/http/interceptor/MacAddressProvider$macAddress$2;

    invoke-direct {p1, p0}, Lcom/squareup/http/interceptor/MacAddressProvider$macAddress$2;-><init>(Lcom/squareup/http/interceptor/MacAddressProvider;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/http/interceptor/MacAddressProvider;->macAddress$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getWifiManager$p(Lcom/squareup/http/interceptor/MacAddressProvider;)Landroid/net/wifi/WifiManager;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/http/interceptor/MacAddressProvider;->wifiManager:Landroid/net/wifi/WifiManager;

    return-object p0
.end method


# virtual methods
.method public final getMacAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/http/interceptor/MacAddressProvider;->macAddress$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
