.class public final enum Lcom/squareup/http/Endpoints$Server;
.super Ljava/lang/Enum;
.source "Endpoints.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/http/Endpoints;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Server"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/http/Endpoints$Server;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/http/Endpoints$Server;

.field public static final enum CUSTOM:Lcom/squareup/http/Endpoints$Server;

.field public static final enum MOCK_MODE:Lcom/squareup/http/Endpoints$Server;

.field public static final enum PRODUCTION:Lcom/squareup/http/Endpoints$Server;

.field public static final enum STAGING:Lcom/squareup/http/Endpoints$Server;


# instance fields
.field public final environmentName:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 25
    new-instance v0, Lcom/squareup/http/Endpoints$Server;

    const/4 v1, 0x0

    const-string v2, "PRODUCTION"

    const-string v3, "Production"

    const-string v4, "https://api-global.squareup.com/"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/squareup/http/Endpoints$Server;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/http/Endpoints$Server;->PRODUCTION:Lcom/squareup/http/Endpoints$Server;

    .line 26
    new-instance v0, Lcom/squareup/http/Endpoints$Server;

    const/4 v2, 0x1

    const-string v3, "STAGING"

    const-string v4, "Staging"

    const-string v5, "https://api-global.squareupstaging.com/"

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/squareup/http/Endpoints$Server;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/http/Endpoints$Server;->STAGING:Lcom/squareup/http/Endpoints$Server;

    .line 27
    new-instance v0, Lcom/squareup/http/Endpoints$Server;

    const/4 v3, 0x2

    const-string v4, "MOCK_MODE"

    const-string v5, "Mock Mode"

    const-string v6, "https://localhost.mock.test/"

    invoke-direct {v0, v4, v3, v5, v6}, Lcom/squareup/http/Endpoints$Server;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/http/Endpoints$Server;->MOCK_MODE:Lcom/squareup/http/Endpoints$Server;

    .line 28
    new-instance v0, Lcom/squareup/http/Endpoints$Server;

    const/4 v4, 0x3

    const-string v5, "CUSTOM"

    const-string v6, "Custom"

    const-string v7, ""

    invoke-direct {v0, v5, v4, v6, v7}, Lcom/squareup/http/Endpoints$Server;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/http/Endpoints$Server;->CUSTOM:Lcom/squareup/http/Endpoints$Server;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/http/Endpoints$Server;

    .line 23
    sget-object v5, Lcom/squareup/http/Endpoints$Server;->PRODUCTION:Lcom/squareup/http/Endpoints$Server;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/http/Endpoints$Server;->STAGING:Lcom/squareup/http/Endpoints$Server;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/http/Endpoints$Server;->MOCK_MODE:Lcom/squareup/http/Endpoints$Server;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/http/Endpoints$Server;->CUSTOM:Lcom/squareup/http/Endpoints$Server;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/http/Endpoints$Server;->$VALUES:[Lcom/squareup/http/Endpoints$Server;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/squareup/http/Endpoints$Server;->environmentName:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/squareup/http/Endpoints$Server;->url:Ljava/lang/String;

    return-void
.end method

.method public static from(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;
    .locals 5

    .line 39
    invoke-static {}, Lcom/squareup/http/Endpoints$Server;->values()[Lcom/squareup/http/Endpoints$Server;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 40
    iget-object v4, v3, Lcom/squareup/http/Endpoints$Server;->url:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 44
    :cond_1
    sget-object p0, Lcom/squareup/http/Endpoints$Server;->CUSTOM:Lcom/squareup/http/Endpoints$Server;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;
    .locals 1

    .line 23
    const-class v0, Lcom/squareup/http/Endpoints$Server;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/http/Endpoints$Server;

    return-object p0
.end method

.method public static values()[Lcom/squareup/http/Endpoints$Server;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/http/Endpoints$Server;->$VALUES:[Lcom/squareup/http/Endpoints$Server;

    invoke-virtual {v0}, [Lcom/squareup/http/Endpoints$Server;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/http/Endpoints$Server;

    return-object v0
.end method
