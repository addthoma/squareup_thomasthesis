.class public Lcom/squareup/jail/DeterminateProgressView;
.super Landroid/widget/FrameLayout;
.source "DeterminateProgressView.java"


# static fields
.field private static final ANIMATION_DURATION_KEY:Ljava/lang/String; = "animation_duration"

.field static final ANIM_TIME_MS:J = 0x320L

.field private static final DRAWN_PROGRESS_KEY:Ljava/lang/String; = "drawn_progress"

.field private static final SUPER_STATE_KEY:Ljava/lang/String; = "super_state"

.field private static final TARGET_PROGRESS_KEY:Ljava/lang/String; = "target_progress"


# instance fields
.field private animator:Landroid/animation/ValueAnimator;

.field private final circlePaint:Landroid/graphics/Paint;

.field private drawnProgress:F

.field private final progressPaint:Landroid/graphics/Paint;

.field private final progressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final rectangle:Landroid/graphics/RectF;

.field private targetProgress:F

.field private final thickness:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 28
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 29
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->rectangle:Landroid/graphics/RectF;

    .line 41
    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/jail/R$dimen;->determinate_progress_circle_thickness:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    iput p1, p0, Lcom/squareup/jail/DeterminateProgressView;->thickness:F

    const/4 p1, 0x0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/jail/DeterminateProgressView;->setWillNotDraw(Z)V

    .line 46
    new-instance p1, Landroid/graphics/Paint;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->circlePaint:Landroid/graphics/Paint;

    .line 47
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->circlePaint:Landroid/graphics/Paint;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_ultra_light_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 47
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->circlePaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->circlePaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/squareup/jail/DeterminateProgressView;->thickness:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 52
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressPaint:Landroid/graphics/Paint;

    .line 53
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$color;->marin_black:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/jail/DeterminateProgressView;->thickness:F

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$setProgress$0$DeterminateProgressView(Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 132
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    iput p1, p0, Lcom/squareup/jail/DeterminateProgressView;->drawnProgress:F

    .line 133
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget v0, p0, Lcom/squareup/jail/DeterminateProgressView;->drawnProgress:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 134
    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->invalidate()V

    return-void
.end method

.method observeProgressPercentage()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/jail/DeterminateProgressView;->progressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .line 73
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 76
    iget-object v1, p0, Lcom/squareup/jail/DeterminateProgressView;->rectangle:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/squareup/jail/DeterminateProgressView;->circlePaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 79
    iget-object v7, p0, Lcom/squareup/jail/DeterminateProgressView;->rectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/squareup/jail/DeterminateProgressView;->drawnProgress:F

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float v9, v0, v1

    iget-object v11, p0, Lcom/squareup/jail/DeterminateProgressView;->progressPaint:Landroid/graphics/Paint;

    const/high16 v8, 0x43870000    # 270.0f

    const/4 v10, 0x0

    move-object v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 59
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->getMeasuredWidth()I

    move-result p1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->getMeasuredHeight()I

    move-result p2

    .line 64
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 65
    invoke-virtual {p0, p1, p1}, Lcom/squareup/jail/DeterminateProgressView;->setMeasuredDimension(II)V

    .line 68
    iget p2, p0, Lcom/squareup/jail/DeterminateProgressView;->thickness:F

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr p2, v0

    .line 69
    iget-object v0, p0, Lcom/squareup/jail/DeterminateProgressView;->rectangle:Landroid/graphics/RectF;

    int-to-float p1, p1

    sub-float/2addr p1, p2

    invoke-virtual {v0, p2, p2, p1, p1}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 10

    .line 93
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "super_state"

    .line 94
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 99
    iget v1, p0, Lcom/squareup/jail/DeterminateProgressView;->targetProgress:F

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    const-string v1, "target_progress"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    :cond_0
    move v6, v1

    .line 100
    iget-object v1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    const-string v1, "animation_duration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v1

    :goto_0
    move-wide v7, v1

    const-string v1, "drawn_progress"

    .line 101
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    const/high16 v9, 0x42c80000    # 100.0f

    mul-float v0, v0, v9

    float-to-int v1, v0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/jail/DeterminateProgressView;->setProgress(IJJ)V

    mul-float v6, v6, v9

    float-to-int v1, v6

    move-wide v2, v7

    move-wide v4, v7

    .line 102
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/jail/DeterminateProgressView;->setProgress(IJJ)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 83
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 84
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "super_state"

    .line 85
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 86
    iget v0, p0, Lcom/squareup/jail/DeterminateProgressView;->drawnProgress:F

    const-string v2, "drawn_progress"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 87
    iget v0, p0, Lcom/squareup/jail/DeterminateProgressView;->targetProgress:F

    const-string v2, "target_progress"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 88
    iget-object v0, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    :goto_0
    const-string v0, "animation_duration"

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-object v1
.end method

.method resetProgress()V
    .locals 2

    const/4 v0, 0x0

    .line 145
    iput v0, p0, Lcom/squareup/jail/DeterminateProgressView;->drawnProgress:F

    .line 146
    iput v0, p0, Lcom/squareup/jail/DeterminateProgressView;->targetProgress:F

    .line 147
    iget-object v1, p0, Lcom/squareup/jail/DeterminateProgressView;->progressRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 148
    invoke-virtual {p0}, Lcom/squareup/jail/DeterminateProgressView;->invalidate()V

    return-void
.end method

.method setProgress(I)V
    .locals 6

    const-wide/16 v2, 0x320

    const-wide/16 v4, 0x320

    move-object v0, p0

    move v1, p1

    .line 110
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/jail/DeterminateProgressView;->setProgress(IJJ)V

    return-void
.end method

.method setProgress(IJJ)V
    .locals 2

    int-to-float p1, p1

    const/high16 v0, 0x42c80000    # 100.0f

    div-float/2addr p1, v0

    .line 122
    iput p1, p0, Lcom/squareup/jail/DeterminateProgressView;->targetProgress:F

    .line 124
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    if-eqz p1, :cond_0

    .line 125
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    const/4 p1, 0x0

    .line 126
    iput-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [F

    const/4 v0, 0x0

    .line 129
    iget v1, p0, Lcom/squareup/jail/DeterminateProgressView;->drawnProgress:F

    aput v1, p1, v0

    const/4 v0, 0x1

    iget v1, p0, Lcom/squareup/jail/DeterminateProgressView;->targetProgress:F

    aput v1, p1, v0

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    .line 130
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    iget v0, p0, Lcom/squareup/jail/DeterminateProgressView;->targetProgress:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    goto :goto_0

    :cond_1
    move-wide p2, p4

    :goto_0
    invoke-virtual {p1, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 131
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    new-instance p2, Lcom/squareup/jail/-$$Lambda$DeterminateProgressView$OT_xU3oxHY6eNNA3fE6Al_pocig;

    invoke-direct {p2, p0}, Lcom/squareup/jail/-$$Lambda$DeterminateProgressView$OT_xU3oxHY6eNNA3fE6Al_pocig;-><init>(Lcom/squareup/jail/DeterminateProgressView;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/jail/DeterminateProgressView;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
