.class public abstract Lcom/squareup/encryption/AbstractCryptoPrimitive;
.super Ljava/lang/Object;
.source "AbstractCryptoPrimitive.java"

# interfaces
.implements Lcom/squareup/encryption/CryptoPrimitive;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/encryption/CryptoPrimitive<",
        "TK;>;"
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/encryption/CryptoKeyAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "TK;>;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/encryption/AbstractCryptoPrimitive;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/encryption/AbstractCryptoPrimitive<",
            "TK;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iget-object v0, p1, Lcom/squareup/encryption/AbstractCryptoPrimitive;->key:Ljava/lang/Object;

    iput-object v0, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->key:Ljava/lang/Object;

    .line 23
    iget-object p1, p1, Lcom/squareup/encryption/AbstractCryptoPrimitive;->adapter:Lcom/squareup/encryption/CryptoKeyAdapter;

    iput-object p1, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->adapter:Lcom/squareup/encryption/CryptoKeyAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/squareup/encryption/CryptoKeyAdapter<",
            "TK;>;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->key:Ljava/lang/Object;

    .line 18
    iput-object p2, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->adapter:Lcom/squareup/encryption/CryptoKeyAdapter;

    return-void
.end method


# virtual methods
.method public compute([B)Lcom/squareup/encryption/CryptoResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/squareup/encryption/CryptoResult<",
            "TK;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 35
    invoke-virtual {p0}, Lcom/squareup/encryption/AbstractCryptoPrimitive;->isExpired()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/encryption/AbstractCryptoPrimitive;->doCompute([B)Lcom/squareup/encryption/CryptoResult;

    move-result-object p1

    return-object p1

    .line 36
    :cond_0
    new-instance p1, Ljava/security/InvalidKeyException;

    invoke-direct {p1}, Ljava/security/InvalidKeyException;-><init>()V

    throw p1
.end method

.method protected abstract doCompute([B)Lcom/squareup/encryption/CryptoResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/squareup/encryption/CryptoResult<",
            "TK;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation
.end method

.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public isExpired()Z
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->adapter:Lcom/squareup/encryption/CryptoKeyAdapter;

    iget-object v1, p0, Lcom/squareup/encryption/AbstractCryptoPrimitive;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/squareup/encryption/CryptoKeyAdapter;->isExpired(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
