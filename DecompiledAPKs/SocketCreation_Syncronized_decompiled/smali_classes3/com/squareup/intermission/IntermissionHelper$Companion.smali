.class public final Lcom/squareup/intermission/IntermissionHelper$Companion;
.super Ljava/lang/Object;
.source "IntermissionHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/intermission/IntermissionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/intermission/IntermissionHelper$Companion;",
        "",
        "()V",
        "MIN_SERVICE_DURATION_WITH_GAP_TIME",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/intermission/IntermissionHelper$Companion;

.field public static final MIN_SERVICE_DURATION_WITH_GAP_TIME:J = 0xfL


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/intermission/IntermissionHelper$Companion;

    invoke-direct {v0}, Lcom/squareup/intermission/IntermissionHelper$Companion;-><init>()V

    sput-object v0, Lcom/squareup/intermission/IntermissionHelper$Companion;->$$INSTANCE:Lcom/squareup/intermission/IntermissionHelper$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
