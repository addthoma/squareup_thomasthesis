.class public final Lcom/squareup/invoices/analytics/PaymentScheduleSaved;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "EditPaymentScheduleEvents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000f\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\tH\u00c6\u0003J;\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\tH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/invoices/analytics/PaymentScheduleSaved;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "isNew",
        "",
        "deposit",
        "splitBalanceCount",
        "",
        "isValid",
        "validationErrorMessage",
        "",
        "(ZZIZLjava/lang/String;)V",
        "getDeposit",
        "()Z",
        "getSplitBalanceCount",
        "()I",
        "getValidationErrorMessage",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deposit:Z

.field private final isNew:Z

.field private final isValid:Z

.field private final splitBalanceCount:I

.field private final validationErrorMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZZIZLjava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "validationErrorMessage"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Invoices: Save Payment Schedule"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    iput-boolean p2, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    iput p3, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    iput-boolean p4, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    iput-object p5, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/analytics/PaymentScheduleSaved;ZZIZLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/analytics/PaymentScheduleSaved;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->copy(ZZIZLjava/lang/String;)Lcom/squareup/invoices/analytics/PaymentScheduleSaved;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZZIZLjava/lang/String;)Lcom/squareup/invoices/analytics/PaymentScheduleSaved;
    .locals 7

    const-string/jumbo v0, "validationErrorMessage"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;-><init>(ZZIZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    iget v1, p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDeposit()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    return v0
.end method

.method public final getSplitBalanceCount()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    return v0
.end method

.method public final getValidationErrorMessage()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isNew()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    return v0
.end method

.method public final isValid()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentScheduleSaved(isNew="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isNew:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->deposit:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", splitBalanceCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->splitBalanceCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->isValid:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", validationErrorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/analytics/PaymentScheduleSaved;->validationErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
