.class public final Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;
.super Ljava/lang/Object;
.source "RealInvoicesAppletRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceToDeepLinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->appletProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->invoiceToDeepLinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;)",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoicesappletapi/InvoicesApplet;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ")",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;-><init>(Lcom/squareup/invoicesappletapi/InvoicesApplet;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->appletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesApplet;

    iget-object v1, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->invoiceToDeepLinkProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->newInstance(Lcom/squareup/invoicesappletapi/InvoicesApplet;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner_Factory;->get()Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

    move-result-object v0

    return-object v0
.end method
