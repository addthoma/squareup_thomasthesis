.class public Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;
.super Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;
.source "InvoiceDetailReadOnlyView.java"


# instance fields
.field presenter:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const-class p2, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$Component;->inject(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 21
    invoke-super {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->onAttachedToWindow()V

    .line 22
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;->dropView(Ljava/lang/Object;)V

    .line 27
    invoke-super {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->onDetachedFromWindow()V

    return-void
.end method

.method showFileAttachmentRows(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)V"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->fileAttachmentsContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 32
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 33
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->fileAttachmentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->createFileAttachmentRow(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Z)Lcom/squareup/ui/account/view/LineRow;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method
