.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;
.super Ljava/lang/Object;
.source "RecordPaymentScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;
.implements Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;
.implements Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B?\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0008\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u001bH\u0016J\u0010\u0010\"\u001a\u00020\u001f2\u0006\u0010#\u001a\u00020$H\u0016J\u0008\u0010%\u001a\u00020\u001fH\u0016J\u000e\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'H\u0007J\u0010\u0010)\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020+H\u0016J\u0008\u0010,\u001a\u00020\u001fH\u0016J\u0008\u0010-\u001a\u00020\u001fH\u0016J\u000e\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u00160\'H\u0016J\u000e\u0010/\u001a\u0008\u0012\u0004\u0012\u0002000\'H\u0016J\u000e\u00101\u001a\u0008\u0012\u0004\u0012\u0002020\'H\u0016J\u0010\u00103\u001a\u00020\u001f2\u0006\u00104\u001a\u00020(H\u0016J\u0010\u00105\u001a\u00020\u001f2\u0006\u00106\u001a\u000207H\u0016J\u0010\u00108\u001a\u00020\u001f2\u0006\u0010#\u001a\u00020$H\u0007J\u0010\u00109\u001a\u00020\u001f2\u0006\u0010:\u001a\u00020\u001bH\u0016R2\u0010\u0014\u001a&\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00160\u0016 \u0017*\u0012\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00160\u0016\u0018\u00010\u00150\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0018\u001a&\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00190\u0019 \u0017*\u0012\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u00190\u0019\u0018\u00010\u00150\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u001a\u001a&\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u001b0\u001b \u0017*\u0012\u0012\u000c\u0012\n \u0017*\u0004\u0018\u00010\u001b0\u001b\u0018\u00010\u00150\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$Runner;",
        "invoicesAppletScopeRunner",
        "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
        "flow",
        "Lflow/Flow;",
        "recordPaymentScreenDataFactory",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;",
        "recordPaymentConfirmationScreenDataFactory",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;",
        "invoiceService",
        "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "defaultRecordPaymentAmountCalculator",
        "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
        "(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lflow/Flow;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;)V",
        "confirmationScreenData",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;",
        "kotlin.jvm.PlatformType",
        "currentRecordPaymentInfo",
        "Lcom/squareup/invoices/RecordPaymentInfo;",
        "isBusy",
        "",
        "recordPaymentSubscription",
        "Lrx/subscriptions/SerialSubscription;",
        "choosePaymentMethod",
        "",
        "goBackFromRecordPaymentConfirmationScreen",
        "successful",
        "goBackFromRecordPaymentMethodScreen",
        "method",
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "goBackFromRecordPaymentScreen",
        "maxAmountToRecordObservable",
        "Lrx/Observable;",
        "Lcom/squareup/protos/common/Money;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "recordPayment",
        "recordPaymentConfirmationScreenData",
        "recordPaymentMethodScreenData",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
        "recordPaymentScreenData",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        "updateAmount",
        "amount",
        "updateNote",
        "note",
        "",
        "updatePaymentMethod",
        "updateSendReceipt",
        "sendReceipt",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final confirmationScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/RecordPaymentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultRecordPaymentAmountCalculator:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

.field private final flow:Lflow/Flow;

.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final invoicesAppletScopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

.field private final isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final recordPaymentConfirmationScreenDataFactory:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;

.field private final recordPaymentScreenDataFactory:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;

.field private final recordPaymentSubscription:Lrx/subscriptions/SerialSubscription;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lflow/Flow;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoicesAppletScopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recordPaymentScreenDataFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recordPaymentConfirmationScreenDataFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceService"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultRecordPaymentAmountCalculator"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->invoicesAppletScopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    iput-object p3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentScreenDataFactory:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;

    iput-object p4, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentConfirmationScreenDataFactory:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;

    iput-object p5, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iput-object p6, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p7, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->defaultRecordPaymentAmountCalculator:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    .line 50
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p1, 0x0

    .line 53
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 55
    new-instance p1, Lrx/subscriptions/SerialSubscription;

    invoke-direct {p1}, Lrx/subscriptions/SerialSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentSubscription:Lrx/subscriptions/SerialSubscription;

    return-void
.end method

.method public static final synthetic access$getConfirmationScreenData$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getDefaultRecordPaymentAmountCalculator$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->defaultRecordPaymentAmountCalculator:Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lflow/Flow;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getInvoicesAppletScopeRunner$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->invoicesAppletScopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getRecordPaymentConfirmationScreenDataFactory$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentConfirmationScreenDataFactory:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;

    return-object p0
.end method

.method public static final synthetic access$getRecordPaymentScreenDataFactory$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentScreenDataFactory:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;

    return-object p0
.end method

.method public static final synthetic access$isBusy$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public choosePaymentMethod()V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goBackFromRecordPaymentConfirmationScreen(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 161
    iget-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/container/CalculatedKey;

    sget-object v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$goBackFromRecordPaymentConfirmationScreen$1;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$goBackFromRecordPaymentConfirmationScreen$1;

    check-cast v1, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    invoke-direct {v0, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 167
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method public goBackFromRecordPaymentMethodScreen(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V
    .locals 1

    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->updatePaymentMethod(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goBackFromRecordPaymentScreen()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public final maxAmountToRecordObservable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->invoicesAppletScopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails()Lrx/Observable;

    move-result-object v0

    .line 177
    const-class v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0, v1}, Lrx/Observable;->cast(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    .line 178
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$maxAmountToRecordObservable$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$maxAmountToRecordObservable$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "invoicesAppletScopeRunne\u2026ip, paidAmount)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->invoicesAppletScopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails()Lrx/Observable;

    move-result-object v0

    .line 59
    const-class v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0, v1}, Lrx/Observable;->cast(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "invoicesAppletScopeRunne\u2026currentRecordPaymentInfo)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnExit(Lrx/Subscription;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentSubscription:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/SerialSubscription;->unsubscribe()V

    return-void
.end method

.method public recordPayment()V
    .locals 4

    .line 85
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "currentRecordPaymentInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/RecordPaymentInfo;

    .line 86
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPaymentSubscription:Lrx/subscriptions/SerialSubscription;

    .line 87
    iget-object v2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v2, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->recordPayment(Lcom/squareup/invoices/RecordPaymentInfo;)Lrx/Observable;

    move-result-object v2

    .line 88
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V

    check-cast v3, Lrx/functions/Action0;

    invoke-virtual {v2, v3}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v2

    .line 89
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$2;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$2;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V

    check-cast v3, Lrx/functions/Action0;

    invoke-virtual {v2, v3}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v2

    .line 90
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;

    invoke-direct {v3, p0, v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;Lcom/squareup/invoices/RecordPaymentInfo;)V

    check-cast v3, Lrx/functions/Action1;

    invoke-virtual {v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 86
    invoke-virtual {v1, v0}, Lrx/subscriptions/SerialSubscription;->set(Lrx/Subscription;)V

    return-void
.end method

.method public recordPaymentConfirmationScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "confirmationScreenData"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public recordPaymentMethodScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentMethodScreenData$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "currentRecordPaymentInfo\u2026ymentMethod\n      )\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public recordPaymentScreenData()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v0, Lrx/Observable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->isBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/Observable;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->maxAmountToRecordObservable()Lrx/Observable;

    move-result-object v2

    .line 114
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentScreenData$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPaymentScreenData$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)V

    check-cast v3, Lrx/functions/Func3;

    .line 112
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026nfo, isBusy, maxAmount) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public updateAmount(Lcom/squareup/protos/common/Money;)V
    .locals 10

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "currentRecordPaymentInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/RecordPaymentInfo;

    .line 119
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3d

    const/4 v9, 0x0

    move-object v3, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/RecordPaymentInfo;->copy$default(Lcom/squareup/invoices/RecordPaymentInfo;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/RecordPaymentInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public updateNote(Ljava/lang/String;)V
    .locals 10

    const-string v0, "note"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "currentRecordPaymentInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/RecordPaymentInfo;

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v4, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/RecordPaymentInfo;->copy$default(Lcom/squareup/invoices/RecordPaymentInfo;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/RecordPaymentInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final updatePaymentMethod(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V
    .locals 10

    const-string v0, "method"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "currentRecordPaymentInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/RecordPaymentInfo;

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/RecordPaymentInfo;->copy$default(Lcom/squareup/invoices/RecordPaymentInfo;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/RecordPaymentInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public updateSendReceipt(Z)V
    .locals 10

    .line 145
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "currentRecordPaymentInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/RecordPaymentInfo;

    .line 146
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->currentRecordPaymentInfo:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x37

    const/4 v9, 0x0

    move v5, p1

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/RecordPaymentInfo;->copy$default(Lcom/squareup/invoices/RecordPaymentInfo;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/RecordPaymentInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
