.class public final Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;
.super Ljava/lang/Object;
.source "DefaultInvoicesStateFilter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0005J\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0008R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
        "",
        "()V",
        "stateFilter",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/invoices/ui/GenericListFilter;",
        "setStateFilter",
        "",
        "Lio/reactivex/Single;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final stateFilter:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 10
    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefa\u2026eStateFilter.ALL_SENT\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;->stateFilter:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getStateFilter$p(Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;->stateFilter:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public final setStateFilter(Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 1

    const-string v0, "stateFilter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;->stateFilter:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final stateFilter()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;->stateFilter:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 18
    new-instance v1, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter$stateFilter$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter$stateFilter$1;-><init>(Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "stateFilter.distinctUnti\u2026ilter.ALL_SENT)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
