.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->resultHandlerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;",
            ">;)",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;Lflow/Flow;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;
    .locals 7

    .line 57
    new-instance v6, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;-><init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;Lflow/Flow;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Flow;

    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->resultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;

    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/main/PosContainer;

    iget-object v4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->newInstance(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;Lflow/Flow;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner_Factory;->get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
