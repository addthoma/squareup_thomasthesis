.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "InvoicePaymentAmountWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$ParentComponent;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$Scope;,
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$BootstrapScreen;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003\u0018\u0019\u001aB/\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0014J\u000e\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;",
        "workflow",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
        "flow",
        "Lflow/Flow;",
        "resultHandler",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "viewFactory",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;",
        "(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;Lflow/Flow;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;)V",
        "getWorkflow",
        "()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "invoice",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "BootstrapScreen",
        "ParentComponent",
        "Scope",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final resultHandler:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;

.field private final workflow:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;Lflow/Flow;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    const-class v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "InvoicePaymentAmountWork\u2026owRunner::class.java.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-interface {p4}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 28
    move-object v4, p5

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 25
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->workflow:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->flow:Lflow/Flow;

    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->resultHandler:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;

    return-void
.end method

.method public static final synthetic access$getResultHandler$p(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->resultHandler:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->workflow:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->getWorkflow()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->flow:Lflow/Flow;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$onEnterScope$1;-><init>(Lflow/Flow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunnerKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunnerKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens()\n      \u2026ubscribe(flow::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n        .subs\u2026er.handleResult(result) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public final start(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 1

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountProps;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
