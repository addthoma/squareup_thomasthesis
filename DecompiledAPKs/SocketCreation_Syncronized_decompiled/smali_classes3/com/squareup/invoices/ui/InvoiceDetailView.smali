.class public Lcom/squareup/invoices/ui/InvoiceDetailView;
.super Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;
.source "InvoiceDetailView.java"


# instance fields
.field private addPaymentButton:Landroid/view/View;

.field private bottomButtonsContainer:Landroid/widget/LinearLayout;

.field private frequencyRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

.field private paymentScheduleContainer:Landroid/view/ViewGroup;

.field private paymentScheduleDivider:Landroid/view/View;

.field private paymentScheduleList:Landroid/view/ViewGroup;

.field private paymentsContainer:Landroid/view/ViewGroup;

.field private paymentsDivider:Landroid/view/View;

.field private paymentsList:Landroid/view/ViewGroup;

.field presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const-class p2, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Component;->inject(Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    return-void
.end method

.method private addBottomButton(Landroid/view/View;)V
    .locals 3

    .line 136
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 139
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 140
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->bottomButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private createButtonView(Lcom/squareup/features/invoices/widgets/SectionElement;)Landroid/view/View;
    .locals 2

    .line 145
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    if-eqz v0, :cond_0

    .line 146
    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 147
    new-instance v0, Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    .line 148
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 149
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailView$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailView$2;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0

    .line 158
    :cond_0
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    if-eqz v0, :cond_1

    .line 159
    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    .line 161
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_confirm_button_container:I

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->bottomButtonsContainer:Landroid/widget/LinearLayout;

    .line 162
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    .line 163
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getInitialText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getConfirmText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    .line 165
    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailView$EblTjoX9QbOIFoztBXsCYOJSpqA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceDetailView$EblTjoX9QbOIFoztBXsCYOJSpqA;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private createPaymentScheduleRow(Lcom/squareup/invoices/PaymentRequestData;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    .line 244
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestData;->getRequestType()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestData;->getFormattedDueDate()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 246
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestData;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 247
    sget v1, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray_disabled_medium_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 249
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestData;->getStatusText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleText(Ljava/lang/CharSequence;)V

    .line 250
    invoke-virtual {p1}, Lcom/squareup/invoices/PaymentRequestData;->getStatusColor()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleColor(I)V

    const/4 p1, 0x1

    .line 252
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleEnabled(Z)V

    .line 253
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 254
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 255
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleVisible(Z)V

    return-object v0
.end method


# virtual methods
.method addButtons(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->bottomButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 130
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 131
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->createButtonView(Lcom/squareup/features/invoices/widgets/SectionElement;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->addBottomButton(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public hidePartialTransactions()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsList:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public hidePaymentRequests()V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleDivider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public hideRecurringRow()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->recurringRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$createButtonView$0$InvoiceDetailView(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;)V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getEvent()Ljava/lang/Object;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 78
    invoke-super {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->onAttachedToWindow()V

    .line 79
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->dropView(Ljava/lang/Object;)V

    .line 84
    invoke-super {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 59
    invoke-super {p0}, Lcom/squareup/invoices/ui/AbstractInvoiceDetailView;->onFinishInflate()V

    .line 61
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_frequency_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->frequencyRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    .line 62
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_payment_schedule_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    .line 64
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_payment_schedule_divider:I

    .line 65
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleDivider:Landroid/view/View;

    .line 66
    sget v0, Lcom/squareup/common/invoices/R$id;->payment_schedule_list:I

    .line 67
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleList:Landroid/view/ViewGroup;

    .line 68
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_payments_container:I

    .line 69
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsContainer:Landroid/view/ViewGroup;

    .line 70
    sget v0, Lcom/squareup/common/invoices/R$id;->invoice_payments_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsDivider:Landroid/view/View;

    .line 71
    sget v0, Lcom/squareup/common/invoices/R$id;->payment_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsList:Landroid/view/ViewGroup;

    .line 72
    sget v0, Lcom/squareup/common/invoices/R$id;->bottom_buttons_container:I

    .line 73
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->bottomButtonsContainer:Landroid/widget/LinearLayout;

    .line 74
    sget v0, Lcom/squareup/common/invoices/R$id;->add_payment_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->addPaymentButton:Landroid/view/View;

    return-void
.end method

.method public setFrequencyRow(Lcom/squareup/protos/client/invoice/RecurringSchedule;Ljava/text/DateFormat;)V
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->frequencyRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;->setVisibility(I)V

    .line 113
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRuleKt;->fromRecurringSchedule(Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    .line 114
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->prettyString(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_frequency_repeat_every:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "duration"

    .line 117
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 120
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_frequency_end:I

    .line 121
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 122
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v3, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;->prettyString(Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "count_or_date"

    invoke-virtual {v2, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 125
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->frequencyRow:Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v1

    const/4 v0, 0x1

    aput-object p1, v2, v0

    invoke-static {p2, v2}, Lcom/squareup/invoices/ui/FloatingHeaderBodyRowsKt;->setBodyAndVisibility(Lcom/squareup/invoices/ui/FloatingHeaderBodyRow;[Ljava/lang/CharSequence;)V

    return-void
.end method

.method setLoadingContactProgressVisible(Z)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->loadingInstrumentsProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public setPaymentsContainerVisibility(Z)V
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsDivider:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 192
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setVisibilityAddPaymentButton(Z)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->addPaymentButton:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 234
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->addPaymentButton:Landroid/view/View;

    new-instance v0, Lcom/squareup/invoices/ui/InvoiceDetailView$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailView$3;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showFileAttachmentRows(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)V"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->fileAttachmentsContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->fileAttachmentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 91
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    const/4 v1, 0x1

    .line 92
    invoke-virtual {p0, v0, v1}, Lcom/squareup/invoices/ui/InvoiceDetailView;->createFileAttachmentRow(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Z)Lcom/squareup/ui/account/view/LineRow;

    move-result-object v1

    .line 93
    new-instance v2, Lcom/squareup/invoices/ui/InvoiceDetailView$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView$1;-><init>(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->fileAttachmentsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public showPartialTransactions(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PartialTransactionData;",
            ">;)V"
        }
    .end annotation

    .line 200
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsList:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsList:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 203
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/PartialTransactionData;

    .line 204
    invoke-virtual {v0}, Lcom/squareup/invoices/PartialTransactionData;->getNote()Ljava/lang/CharSequence;

    move-result-object v1

    .line 205
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    .line 207
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsList:Landroid/view/ViewGroup;

    invoke-static {v3}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v3

    .line 209
    invoke-virtual {v0}, Lcom/squareup/invoices/PartialTransactionData;->getFormattedValue()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-virtual {v0}, Lcom/squareup/invoices/PartialTransactionData;->getStatusText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v0}, Lcom/squareup/invoices/PartialTransactionData;->getNote()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 216
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/PartialTransactionData;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 217
    invoke-virtual {v0}, Lcom/squareup/invoices/PartialTransactionData;->getBadgeRes()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeImage(I)V

    .line 219
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray_disabled_medium_gray:I

    invoke-virtual {v3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 222
    invoke-virtual {v3, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 223
    invoke-virtual {v3, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 224
    invoke-virtual {v3, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisible(Z)V

    .line 225
    invoke-virtual {v3, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeVisible(Z)V

    .line 227
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentsList:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public showPaymentRequests(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PaymentRequestData;",
            ">;)V"
        }
    .end annotation

    .line 180
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 185
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/PaymentRequestData;

    .line 186
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceDetailView;->createPaymentScheduleRow(Lcom/squareup/invoices/PaymentRequestData;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method
