.class Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler$1;
.super Ljava/lang/Object;
.source "InvoicesDeepLinkHandler.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->handleInvoiceDeepLink(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler$1;->this$0:Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler$1;->this$0:Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->access$000(Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;)Lcom/squareup/invoicesappletapi/InvoicesApplet;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->createInvoiceHistoryToSingleInvoiceDetail(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler$1;->this$0:Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->access$000(Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;)Lcom/squareup/invoicesappletapi/InvoicesApplet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->getPermissions()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
