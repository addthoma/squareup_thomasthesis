.class final Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;
.super Ljava/lang/Object;
.source "RealInvoiceUnitCache.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/RealInvoiceUnitCache;->listenForPushes()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
        "it",
        "Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;->this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;->this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;

    invoke-static {p1}, Lcom/squareup/invoices/RealInvoiceUnitCache;->access$fetchUnitSettings(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/Single;

    move-result-object v0

    .line 106
    invoke-static {}, Lcom/squareup/invoices/RealInvoiceUnitCache;->access$Companion()Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;

    invoke-static {}, Lcom/squareup/invoices/RealInvoiceUnitCache;->access$Companion()Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;->this$0:Lcom/squareup/invoices/RealInvoiceUnitCache;

    invoke-static {p1}, Lcom/squareup/invoices/RealInvoiceUnitCache;->access$getMainScheduler$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/Scheduler;

    move-result-object v5

    const/4 v1, 0x3

    const-wide/16 v2, 0x2

    .line 105
    invoke-static/range {v0 .. v5}, Lcom/squareup/receiving/StandardReceiverKt;->retryExponentialBackoff(Lio/reactivex/Single;IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;->apply(Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
