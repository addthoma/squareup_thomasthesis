.class public Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;
.super Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;
.source "NewInvoiceFeaturesTutorial.java"

# interfaces
.implements Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;


# instance fields
.field private final application:Landroid/app/Application;

.field private final dismissedTips:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;

.field private final features:Lcom/squareup/settings/server/Features;

.field private invoicesAppletActive:Z

.field private final presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Landroid/app/Application;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/util/Res;",
            "Landroid/app/Application;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;-><init>()V

    .line 38
    iput-object p2, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->features:Lcom/squareup/settings/server/Features;

    .line 39
    iput-object p1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    .line 40
    iput-object p3, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->res:Lcom/squareup/util/Res;

    .line 41
    iput-object p4, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->application:Landroid/app/Application;

    .line 42
    new-instance p1, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;

    invoke-direct {p1, p5}, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;-><init>(Lcom/squareup/settings/LocalSetting;)V

    iput-object p1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->dismissedTips:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;

    return-void
.end method

.method private formatFileAttachmentBannerText()Ljava/lang/CharSequence;
    .locals 5

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->scroll_down_to_try_out_new_feature:I

    sget v3, Lcom/squareup/pos/tutorials/R$string;->new_feature_automatic_reminders:I

    const-string v4, "new_feature"

    invoke-static {v0, v1, v2, v4, v3}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private showAutomaticRemindersFeatureBanner()Z
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_AUTOMATIC_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->dismissedTips:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;

    sget-object v1, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->AUTOMATIC_REMINDER_INVOICE_FEATURE:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    .line 63
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;->isDismissed(Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 0

    return-void
.end method

.method public isTriggered()Z
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->shouldActivateTutorial()Z

    move-result v0

    return v0
.end method

.method public onRequestExitTutorial()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->dismissedTips:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;

    sget-object v1, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;->AUTOMATIC_REMINDER_INVOICE_FEATURE:Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips;->setDismissed(Lcom/squareup/invoices/tutorial/DismissedNewInvoiceFeatureTips$NewInvoiceFeatureTip;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 89
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->endTutorial()V

    return-void
.end method

.method public onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 76
    instance-of p1, p1, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    if-eqz p1, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->showAutomaticRemindersFeatureBanner()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 78
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->formatFileAttachmentBannerText()Ljava/lang/CharSequence;

    move-result-object v0

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->invoice_new_feature_background_selector:I

    .line 79
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 78
    invoke-virtual {p1, v0, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    return-void

    .line 83
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    return-void
.end method

.method public onShowingThanks()V
    .locals 0

    return-void
.end method

.method public setInvoicesAppletActive(Z)V
    .locals 0

    .line 54
    iput-boolean p1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->invoicesAppletActive:Z

    return-void
.end method

.method public shouldActivateTutorial()Z
    .locals 1

    .line 58
    iget-boolean v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->invoicesAppletActive:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->showAutomaticRemindersFeatureBanner()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public startAndActivateTutorial()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hasActiveTutorial()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->replaceTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->maybeLookForNewTutorial()V

    :goto_0
    return-void
.end method
