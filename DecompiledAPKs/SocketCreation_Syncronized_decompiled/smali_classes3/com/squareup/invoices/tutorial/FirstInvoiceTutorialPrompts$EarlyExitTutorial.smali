.class Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;
.super Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;
.source "FirstInvoiceTutorialPrompts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EarlyExitTutorial"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 66
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial$1;

    invoke-direct {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$FirstInvoiceTutorialPrompt;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 63
    :cond_1
    const-class v2, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getContent(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 34
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_content:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPrimaryButton()I
    .locals 1

    .line 38
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_continue:I

    return v0
.end method

.method public getSecondaryButton()I
    .locals 1

    .line 42
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_end:I

    return v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 30
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method handleTap(Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    if-eq p1, v0, :cond_0

    .line 47
    invoke-virtual {p2}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->exitQuietly()V

    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorialPrompts$EarlyExitTutorial;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method
