.class public final Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;
.super Ljava/lang/Object;
.source "PaymentRequestReadOnlyInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestReadOnlyInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestReadOnlyInfo.kt\ncom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory\n*L\n1#1,70:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Clock;)V",
        "fromInvoiceDisplayDetails",
        "",
        "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public final fromInvoiceDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v1, "invoice"

    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 39
    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p0, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v2, v3}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 41
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string v3, "invoice.payment_request"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 49
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lkotlin/Pair;

    .line 50
    invoke-virtual {v3}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 51
    invoke-virtual {v3}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    .line 53
    iget-object v5, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    invoke-static {v4, v5}, Lcom/squareup/invoices/PaymentRequestsKt;->findPaymentRequestState(Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 59
    invoke-static {v4, v2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v6

    .line 60
    new-instance v7, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;

    invoke-direct {v7, v4, v5, v3, v6}, Lcom/squareup/invoices/PaymentRequestReadOnlyInfo;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    .line 63
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Could not find payment request state for payment request."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    return-object v1

    .line 43
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot compute payment request amounts."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
