.class public final Lcom/squareup/invoices/PartialTransactionData;
.super Ljava/lang/Object;
.source "PartialTransactionData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/PartialTransactionData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001:\u0001\u0013B3\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000e\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/PartialTransactionData;",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "formattedValue",
        "",
        "statusText",
        "note",
        "badgeRes",
        "",
        "(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V",
        "getBadgeRes",
        "()I",
        "getFormattedValue",
        "()Ljava/lang/CharSequence;",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getNote",
        "getStatusText",
        "Factory",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badgeRes:I

.field private final formattedValue:Ljava/lang/CharSequence;

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final note:Ljava/lang/CharSequence;

.field private final statusText:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PartialTransactionData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p2, p0, Lcom/squareup/invoices/PartialTransactionData;->formattedValue:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/invoices/PartialTransactionData;->statusText:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/squareup/invoices/PartialTransactionData;->note:Ljava/lang/CharSequence;

    iput p5, p0, Lcom/squareup/invoices/PartialTransactionData;->badgeRes:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 32
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/PartialTransactionData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/squareup/invoices/PartialTransactionData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    return-void
.end method


# virtual methods
.method public final getBadgeRes()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/invoices/PartialTransactionData;->badgeRes:I

    return v0
.end method

.method public final getFormattedValue()Ljava/lang/CharSequence;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData;->formattedValue:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getNote()Ljava/lang/CharSequence;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData;->note:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getStatusText()Ljava/lang/CharSequence;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData;->statusText:Ljava/lang/CharSequence;

    return-object v0
.end method
