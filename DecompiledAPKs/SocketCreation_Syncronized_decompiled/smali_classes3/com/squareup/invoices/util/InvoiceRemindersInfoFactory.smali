.class public final Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;
.super Ljava/lang/Object;
.source "InvoiceRemindersInfoFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceRemindersInfoFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceRemindersInfoFactory.kt\ncom/squareup/invoices/util/InvoiceRemindersInfoFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,143:1\n1360#2:144\n1429#2,3:145\n1360#2:148\n1429#2,3:149\n1360#2:152\n1429#2,3:153\n1360#2:156\n1429#2,3:157\n1360#2:160\n1429#2,3:161\n1360#2:164\n1429#2,3:165\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceRemindersInfoFactory.kt\ncom/squareup/invoices/util/InvoiceRemindersInfoFactory\n*L\n53#1:144\n53#1,3:145\n64#1:148\n64#1,3:149\n97#1:152\n97#1,3:153\n105#1:156\n105#1,3:157\n117#1:160\n117#1,3:161\n134#1:164\n134#1,3:165\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J.\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011J$\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0016\u001a\u00020\u0017J \u0010\u0018\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011J\u0016\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V",
        "createDefaultListForInvoice",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "configs",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "isRecurring",
        "",
        "workingInvoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails;",
        "createDefaultListForPaymentRequest",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "createForInvoice",
        "createForPaymentRequest",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->clock:Lcom/squareup/util/Clock;

    iput-object p2, p0, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final createDefaultListForInvoice(Ljava/util/List;ZLcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;Z",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/invoices/DisplayDetails;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;"
        }
    .end annotation

    const-string v0, "configs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingInvoice"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/16 v0, 0xa

    if-eqz p2, :cond_7

    .line 39
    sget-object p2, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    iget-object v3, p3, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string/jumbo v4, "workingInvoice.payment_request"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object p2

    sget-object v3, Lcom/squareup/invoices/PaymentRequestsConfig;->FULL:Lcom/squareup/invoices/PaymentRequestsConfig;

    if-ne p2, v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_6

    const/4 p2, 0x0

    if-eqz p4, :cond_4

    .line 44
    instance-of v1, p4, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v1, :cond_2

    check-cast p4, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p4}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p2

    goto :goto_2

    .line 45
    :cond_2
    instance-of p4, p4, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz p4, :cond_3

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 48
    :cond_4
    :goto_2
    iget-object p4, p3, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 49
    iget-object v1, p0, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->clock:Lcom/squareup/util/Clock;

    .line 41
    invoke-static {p2, p4, v1}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 51
    invoke-static {p3, p2}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p3

    .line 53
    check-cast p1, Ljava/lang/Iterable;

    .line 144
    new-instance p4, Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p4, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p4, Ljava/util/Collection;

    .line 145
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 146
    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 54
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v0

    .line 55
    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 147
    :cond_5
    check-cast p4, Ljava/util/List;

    .line 58
    new-instance p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-direct {p1, p4, p2, p3}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    .line 40
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    goto :goto_5

    .line 60
    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Can only call "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "createForInvoice"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " with a full invoice."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 59
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 64
    :cond_7
    check-cast p1, Ljava/lang/Iterable;

    .line 148
    new-instance p2, Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 149
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 150
    check-cast p3, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 64
    invoke-static {p3}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 151
    :cond_8
    check-cast p2, Ljava/util/List;

    .line 63
    new-instance p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :goto_5
    return-object p1
.end method

.method public final createDefaultListForPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;"
        }
    .end annotation

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-static {p1, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 134
    check-cast p2, Ljava/lang/Iterable;

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 165
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 166
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 135
    invoke-static {v1}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v1

    .line 136
    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 167
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 138
    new-instance p2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-direct {p2, v0, p3, p1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object p2
.end method

.method public final createForInvoice(ZLcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 6

    const-string/jumbo v0, "workingInvoice"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v0, "it"

    const/16 v3, 0xa

    if-eqz p1, :cond_7

    .line 81
    sget-object p1, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    iget-object v4, p2, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string/jumbo v5, "workingInvoice.payment_request"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object p1

    sget-object v4, Lcom/squareup/invoices/PaymentRequestsConfig;->FULL:Lcom/squareup/invoices/PaymentRequestsConfig;

    if-ne p1, v4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_6

    const/4 p1, 0x0

    if-eqz p3, :cond_4

    .line 86
    instance-of v1, p3, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v1, :cond_2

    check-cast p3, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p3}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    goto :goto_2

    .line 87
    :cond_2
    instance-of p3, p3, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz p3, :cond_3

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 90
    :cond_4
    :goto_2
    iget-object p3, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 91
    iget-object v1, p0, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->clock:Lcom/squareup/util/Clock;

    .line 83
    invoke-static {p1, p3, v1}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 93
    invoke-static {p2, p1}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p3

    .line 95
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string/jumbo v1, "workingInvoice.payment_r\u2026()\n            .reminders"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Iterable;

    .line 152
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 153
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 154
    check-cast v2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    .line 97
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 155
    :cond_5
    check-cast v1, Ljava/util/List;

    .line 99
    new-instance p2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-direct {p2, v1, p1, p3}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    .line 82
    check-cast p2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    goto :goto_5

    .line 101
    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Can only call "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "createForInvoice"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " with a full invoice."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 100
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 105
    :cond_7
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    const-string/jumbo p2, "workingInvoice.automatic_reminder_config"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 156
    new-instance p2, Ljava/util/ArrayList;

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 157
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 158
    check-cast p3, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 105
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 159
    :cond_8
    check-cast p2, Ljava/util/List;

    .line 104
    new-instance p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;-><init>(Ljava/util/List;)V

    move-object p2, p1

    check-cast p2, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    :goto_5
    return-object p2
.end method

.method public final createForPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;
    .locals 4

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceFirstSentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    .line 117
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string v1, "paymentRequest.reminders"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 160
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 161
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 162
    check-cast v2, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    const-string v3, "it"

    .line 118
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 120
    new-instance p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-direct {p1, v1, p2, v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object p1
.end method
