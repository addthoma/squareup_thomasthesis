.class public abstract Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;
.super Lcom/squareup/invoices/editv2/validation/ValidationResult;
.source "InvoiceValidator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/validation/ValidationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "InvalidWithMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;,
        Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$AmountOutOfRange;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;",
        "Lcom/squareup/invoices/editv2/validation/ValidationResult;",
        "errorMessage",
        "",
        "(Ljava/lang/String;)V",
        "getErrorMessage",
        "()Ljava/lang/String;",
        "AmountOutOfRange",
        "InvalidCustomer",
        "Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$InvalidCustomer;",
        "Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage$AmountOutOfRange;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final errorMessage:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/validation/ValidationResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;->errorMessage:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;->errorMessage:Ljava/lang/String;

    return-object v0
.end method
