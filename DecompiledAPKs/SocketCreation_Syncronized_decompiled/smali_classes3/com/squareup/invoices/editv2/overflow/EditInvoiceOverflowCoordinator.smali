.class public final Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditInvoiceOverflowCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceOverflowCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceOverflowCoordinator.kt\ncom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,43:1\n1103#2,7:44\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceOverflowCoordinator.kt\ncom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator\n*L\n33#1,7:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;",
        "factory",
        "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
        "(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;)V",
        "buttonContainer",
        "Landroid/view/ViewGroup;",
        "dismissButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buttonContainer:Landroid/view/ViewGroup;

.field private dismissButton:Lcom/squareup/marketfont/MarketButton;

.field private final factory:Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;

.field private final runner:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->runner:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->factory:Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;

    return-void
.end method

.method public static final synthetic access$getButtonContainer$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)Landroid/view/ViewGroup;
    .locals 1

    .line 15
    iget-object p0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->buttonContainer:Landroid/view/ViewGroup;

    if-nez p0, :cond_0

    const-string v0, "buttonContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getFactory$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->factory:Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->runner:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;

    return-object p0
.end method

.method public static final synthetic access$setButtonContainer$p(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;Landroid/view/ViewGroup;)V
    .locals 0

    .line 15
    iput-object p1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->buttonContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 39
    sget v0, Lcom/squareup/features/invoices/R$id;->button_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->buttonContainer:Landroid/view/ViewGroup;

    .line 40
    sget v0, Lcom/squareup/features/invoices/R$id;->dismiss_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.dismiss_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->dismissButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->bindViews(Landroid/view/View;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->runner:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;->overflowDialogScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$1;-><init>(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.overflowDialogScr\u2026actory, runner)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 33
    iget-object p1, p0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;->dismissButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_0

    const-string v0, "dismissButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    .line 44
    new-instance v0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
