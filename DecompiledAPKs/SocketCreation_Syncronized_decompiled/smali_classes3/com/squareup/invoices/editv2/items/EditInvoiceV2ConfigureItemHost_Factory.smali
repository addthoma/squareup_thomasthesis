.class public final Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceV2ConfigureItemHost_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;->orderEditorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;)",
            "Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/order/WorkingOrderEditor;)Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/order/WorkingOrderEditor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;->orderEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/order/WorkingOrderEditor;

    invoke-static {v0, v1}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;->newInstance(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/order/WorkingOrderEditor;)Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost_Factory;->get()Lcom/squareup/invoices/editv2/items/EditInvoiceV2ConfigureItemHost;

    move-result-object v0

    return-object v0
.end method
