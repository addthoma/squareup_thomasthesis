.class public abstract Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;
.super Ljava/lang/Object;
.source "SendInvoiceResponse.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;,
        Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        "",
        "()V",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "getInvoice",
        "()Lcom/squareup/protos/client/invoice/Invoice;",
        "Recurring",
        "SingleInvoice",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInvoice()Lcom/squareup/protos/client/invoice/Invoice;
    .locals 3

    .line 24
    instance-of v0, p0, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 25
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    goto :goto_0

    .line 28
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;

    if-eqz v0, :cond_2

    .line 29
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v2, :cond_1

    .line 30
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v1, v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->template:Lcom/squareup/protos/client/invoice/Invoice;

    :cond_1
    :goto_0
    return-object v1

    .line 29
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
