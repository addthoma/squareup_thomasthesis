.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleDueDateResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "workingInvoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dueDateResult:Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->$dueDateResult:Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 3

    .line 1129
    sget-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;->Converter:Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->$dueDateResult:Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;->getResult()Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;->selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    .line 1132
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    const-string/jumbo v2, "workingInvoice"

    .line 1133
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1134
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getCurrentTime$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/time/CurrentTime;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 1131
    invoke-static {v1, p1, v2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    if-eqz v0, :cond_0

    .line 1138
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingInvoiceEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateDueDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleDueDateResult$1;->accept(Lcom/squareup/protos/client/invoice/Invoice;)V

    return-void
.end method
