.class public final Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceV2BottomButtonTextFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    invoke-static {v0, v1}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory_Factory;->get()Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    move-result-object v0

    return-object v0
.end method
