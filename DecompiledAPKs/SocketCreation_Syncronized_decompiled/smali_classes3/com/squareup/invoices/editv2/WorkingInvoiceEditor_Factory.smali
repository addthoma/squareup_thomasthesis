.class public final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;
.super Ljava/lang/Object;
.source "WorkingInvoiceEditor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final dateSanitizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final instrumentsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUnitCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEditorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;"
        }
    .end annotation
.end field

.field private final ruleEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->ruleEditorProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->orderEditorProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->orderEditorFactoryProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->instrumentsStoreProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->dateSanitizerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/order/WorkingOrderEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;",
            ">;)",
            "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/order/WorkingOrderEditor;Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;
    .locals 11

    .line 79
    new-instance v10, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/order/WorkingOrderEditor;Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->ruleEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->orderEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/order/WorkingOrderEditor;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->orderEditorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->instrumentsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->dateSanitizerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;

    invoke-static/range {v1 .. v9}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/settings/server/Features;Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Lcom/squareup/invoices/order/WorkingOrderEditor;Lcom/squareup/invoices/editv2/EditInvoiceOrderFactory;Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Lcom/squareup/invoices/editv2/EditInvoiceDateSanitizer;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor_Factory;->get()Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object v0

    return-object v0
.end method
