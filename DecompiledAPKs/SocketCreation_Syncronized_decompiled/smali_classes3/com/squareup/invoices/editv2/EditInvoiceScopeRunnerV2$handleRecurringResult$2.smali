.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleRecurringResult(Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000126\u0010\u0002\u001a2\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $recurringOutput:Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->$recurringOutput:Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 1091
    sget-object v1, Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;->Converter:Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput$Converter;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->$recurringOutput:Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput$Converter;->rRuleFromOutput(Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v3, "isRecurring"

    .line 1096
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v2, :cond_1

    .line 1097
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingInvoiceEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object p1

    .line 1098
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    const-string/jumbo v2, "workingInvoice.automatic_reminder_config"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstances(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1097
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateAutomaticRemindersInstances(Ljava/util/List;)V

    goto :goto_1

    .line 1103
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    if-eqz v2, :cond_2

    .line 1105
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingInvoiceEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object p1

    .line 1106
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string/jumbo v2, "workingInvoice.payment_request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string/jumbo v2, "workingInvoice.payment_request.last().reminders"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toConfigs(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1105
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->updateAutomaticRemindersConfig(Ljava/util/List;)V

    .line 1111
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleRecurringResult$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getWorkingRecurrenceRuleEditor$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->set(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)V

    return-void
.end method
