.class public final Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;
.super Ljava/lang/Object;
.source "InvoiceUnitCacheDefaults.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u001a\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000f\u001a\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000fH\u0002\u001a\u0016\u0010\u0012\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000f\u001a\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u000e\u001a\u00020\u000fH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "MAX_ATTACHMENTS_TOTAL_SIZE_BYTES",
        "",
        "MAX_ATTACHMENT_COUNT",
        "MAX_REMINDER_CHARACTERS",
        "MAX_REMINDER_COUNT",
        "defaultAutomaticReminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "defaultInvoiceDefaults",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "res",
        "Lcom/squareup/util/Res;",
        "defaultInvoiceMetrics",
        "",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "defaultMetricValue",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;",
        "defaultUnitMetadataDisplayDetails",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "zeroAmountMoney",
        "Lcom/squareup/protos/common/Money;",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MAX_ATTACHMENTS_TOTAL_SIZE_BYTES:I = 0xa00000

.field private static final MAX_ATTACHMENT_COUNT:I = 0xa

.field private static final MAX_REMINDER_CHARACTERS:I = 0x3e8

.field private static final MAX_REMINDER_COUNT:I = 0x5


# direct methods
.method public static final defaultAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 2

    .line 100
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;-><init>()V

    const/4 v1, 0x5

    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->automatic_reminder_count_limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;

    move-result-object v0

    const/16 v1, 0x3e8

    .line 102
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->custom_message_char_limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;

    move-result-object v0

    .line 103
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->default_reminder(Ljava/util/List;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    const-string v1, "InvoiceAutomaticReminder\u2026ptyList())\n      .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final defaultInvoiceDefaults(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/invoice/InvoiceDefaults;
    .locals 4

    const-string v0, "res"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;-><init>()V

    const-string v1, ""

    .line 109
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object v0

    .line 110
    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_default_message:I

    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    const/4 v0, 0x0

    .line 111
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    .line 112
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->relative_send_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    .line 113
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->shipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    .line 116
    new-instance v2, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;-><init>()V

    .line 117
    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;

    move-result-object v2

    .line 118
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->relative_due_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;

    move-result-object v1

    .line 119
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;

    move-result-object v1

    .line 115
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 114
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->payment_request_defaults(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    .line 123
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminders_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    .line 124
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p0

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object p0

    const-string v0, "InvoiceDefaults.Builder(\u2026ptyList())\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final defaultInvoiceMetrics(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;"
        }
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;-><init>()V

    .line 68
    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 69
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultMetricValue(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;-><init>()V

    .line 74
    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 75
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultMetricValue(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;-><init>()V

    .line 80
    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 81
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultMetricValue(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;-><init>()V

    .line 86
    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->PENDING_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 87
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultMetricValue(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;-><init>()V

    .line 92
    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->ACCEPTED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object v1

    .line 93
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultMetricValue(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;

    move-result-object p0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    move-result-object p0

    .line 90
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static final defaultMetricValue(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;
    .locals 1

    .line 136
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;-><init>()V

    .line 137
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->zeroAmountMoney(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->money_value(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;

    move-result-object p0

    .line 138
    sget-object v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->MONEY_VALUE:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->value_type(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;

    move-result-object p0

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object p0

    const-string v0, "Metric.Value.Builder()\n \u2026NEY_VALUE)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final defaultUnitMetadataDisplayDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
    .locals 4

    const-string v0, "res"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;-><init>()V

    .line 41
    new-instance v1, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;-><init>()V

    .line 42
    sget v2, Lcom/squareup/common/invoices/R$string;->invoice_default_message:I

    invoke-interface {p0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->default_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;

    move-result-object p0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadata;

    move-result-object p0

    .line 40
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p0

    const/4 v0, 0x1

    .line 45
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_invoices(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p0

    .line 46
    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_estimates(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p0

    .line 48
    new-instance v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;-><init>()V

    const/16 v2, 0xa

    .line 49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    move-result-object v1

    const/high16 v2, 0xa00000

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_total_size_bytes(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    move-result-object v1

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    .line 51
    sget-object v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->JPEG:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->allowed_mime_type(Ljava/util/List;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p0

    .line 54
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->zeroAmountMoney(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->recent_paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;

    move-result-object v0

    .line 56
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->zeroAmountMoney(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->total_draft_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;

    move-result-object v0

    .line 57
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->zeroAmountMoney(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->total_unpaid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->build()Lcom/squareup/protos/client/invoice/UnitAnalytics;

    move-result-object p1

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics(Lcom/squareup/protos/client/invoice/UnitAnalytics;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p0

    .line 60
    invoke-static {}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p0

    const-string p1, "UnitMetadataDisplayDetai\u2026ettings())\n      .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final zeroAmountMoney(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    .line 130
    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p0

    const-wide/16 v0, 0x0

    .line 131
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p0

    .line 132
    invoke-virtual {p0}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string v0, "Money.Builder()\n      .c\u2026amount(0L)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
