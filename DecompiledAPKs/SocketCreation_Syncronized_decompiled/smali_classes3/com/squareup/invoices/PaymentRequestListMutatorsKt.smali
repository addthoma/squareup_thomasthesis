.class public final Lcom/squareup/invoices/PaymentRequestListMutatorsKt;
.super Ljava/lang/Object;
.source "PaymentRequestListMutators.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestListMutators.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestListMutators.kt\ncom/squareup/invoices/PaymentRequestListMutatorsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,115:1\n1360#2:116\n1429#2,3:117\n1360#2:123\n1429#2,3:124\n1847#2,3:130\n215#2,2:133\n1847#2,3:135\n1360#2:138\n1429#2,2:139\n1431#2:144\n132#3,3:120\n132#3,3:127\n132#3,3:141\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentRequestListMutators.kt\ncom/squareup/invoices/PaymentRequestListMutatorsKt\n*L\n29#1:116\n29#1,3:117\n42#1:123\n42#1,3:124\n93#1,3:130\n95#1,2:133\n97#1,3:135\n97#1:138\n97#1,2:139\n97#1:144\n42#1,3:120\n42#1,3:127\n97#1,3:141\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a.\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0001\u001a$\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0001\u001a\u0016\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u001a\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00020\u0001\u00a8\u0006\n"
    }
    d2 = {
        "addDefaultInstallments",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "installmentCount",
        "",
        "reminderConfigs",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "addDeposit",
        "removeDeposit",
        "removeInstallments",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final addDefaultInstallments(Ljava/util/List;ILjava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$addDefaultInstallments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderConfigs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    const/4 v0, 0x1

    if-gt v0, p1, :cond_8

    const/4 v1, 0x1

    .line 44
    :goto_0
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/wire/Message;

    .line 121
    invoke-virtual {v2}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type B"

    if-eqz v2, :cond_7

    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    const/4 v5, 0x0

    .line 45
    move-object v6, v5

    check-cast v6, Ljava/lang/String;

    iput-object v6, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->token:Ljava/lang/String;

    .line 46
    move-object v6, p2

    check-cast v6, Ljava/lang/Iterable;

    .line 123
    new-instance v7, Ljava/util/ArrayList;

    const/16 v8, 0xa

    invoke-static {v6, v8}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 124
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 125
    check-cast v8, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 46
    invoke-static {v8}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 126
    :cond_0
    check-cast v7, Ljava/util/List;

    iput-object v7, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders:Ljava/util/List;

    .line 47
    iget-object v6, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v6, :cond_6

    sget-object v7, Lcom/squareup/invoices/PaymentRequestListMutatorsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v6}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v6

    aget v6, v7, v6

    const-wide/16 v7, 0x1e

    const-wide/16 v9, 0x0

    if-eq v6, v0, :cond_5

    const/4 v11, 0x2

    if-eq v6, v11, :cond_3

    const/4 v3, 0x3

    if-eq v6, v3, :cond_2

    const/4 p0, 0x4

    if-eq v6, p0, :cond_1

    const/4 p0, 0x5

    if-eq v6, p0, :cond_1

    goto :goto_3

    .line 61
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Cannot add installment(s) directly after deposit. List must either end in an installment or remainder."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 57
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iput-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 58
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 59
    check-cast v5, Lcom/squareup/protos/common/Money;

    iput-object v5, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    goto :goto_2

    .line 53
    :cond_3
    iget-object v5, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    check-cast v5, Lcom/squareup/wire/Message;

    .line 128
    invoke-virtual {v5}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v5

    if-eqz v5, :cond_4

    move-object v3, v5

    check-cast v3, Lcom/squareup/protos/common/Money$Builder;

    .line 53
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v3, Lcom/squareup/protos/common/Money$Builder;->amount:Ljava/lang/Long;

    .line 129
    invoke-virtual {v5}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    iput-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    .line 54
    iget-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    goto :goto_2

    .line 128
    :cond_4
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 49
    :cond_5
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    .line 50
    iget-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v4, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    .line 122
    :goto_2
    invoke-virtual {v2}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v2

    .line 44
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const-string v3, "installment"

    .line 69
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eq v1, p1, :cond_8

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 65
    :cond_6
    :goto_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "payment requests must have amount type"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 121
    :cond_7
    new-instance p0, Lkotlin/TypeCastException;

    invoke-direct {p0, v3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 73
    :cond_8
    sget-object p1, Lcom/squareup/invoices/PaymentRequestListMutatorsKt$addDefaultInstallments$1$1;->INSTANCE:Lcom/squareup/invoices/PaymentRequestListMutatorsKt$addDefaultInstallments$1$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p1}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Z

    return-object p0
.end method

.method public static synthetic addDefaultInstallments$default(Ljava/util/List;ILjava/util/List;ILjava/lang/Object;)Ljava/util/List;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p1, 0x2

    .line 39
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/invoices/PaymentRequestListMutatorsKt;->addDefaultInstallments(Ljava/util/List;ILjava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final addDeposit(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$addDeposit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderConfigs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;-><init>()V

    .line 23
    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 25
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 26
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 27
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 29
    check-cast p1, Ljava/lang/Iterable;

    .line 116
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 117
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 118
    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 29
    invoke-static {v3}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 29
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object p1

    .line 30
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    .line 32
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    const-string v0, "depositRequest"

    .line 34
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public static final removeDeposit(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$removeDeposit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    .line 83
    sget-object v0, Lcom/squareup/invoices/PaymentRequestListMutatorsKt$removeDeposit$1$1;->INSTANCE:Lcom/squareup/invoices/PaymentRequestListMutatorsKt$removeDeposit$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Z

    return-object p0
.end method

.method public static final removeInstallments(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$removeInstallments"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 130
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 131
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 93
    invoke-static {v4}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    return-object p0

    .line 133
    :cond_3
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 95
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 97
    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    .line 98
    sget-object v0, Lcom/squareup/invoices/PaymentRequestListMutatorsKt$removeInstallments$2$1;->INSTANCE:Lcom/squareup/invoices/PaymentRequestListMutatorsKt$removeInstallments$2$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Z

    .line 101
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 135
    instance-of v4, v0, Ljava/util/Collection;

    if-eqz v4, :cond_5

    move-object v4, v0

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    goto :goto_2

    .line 136
    :cond_5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 101
    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v5, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v4, v5, :cond_7

    const/4 v4, 0x1

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_6

    const/4 v3, 0x0

    :cond_8
    :goto_2
    if-eqz v3, :cond_b

    .line 102
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;-><init>()V

    .line 103
    sget-object v3, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 104
    iget-object v3, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 105
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 106
    iget-object v2, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 108
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    const-string v2, "installment1.reminders"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 138
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 139
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 140
    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    .line 108
    check-cast v3, Lcom/squareup/wire/Message;

    .line 142
    invoke-virtual {v3}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object v3

    if-eqz v3, :cond_9

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    const/4 v5, 0x0

    .line 108
    check-cast v5, Ljava/lang/String;

    iput-object v5, v4, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->token:Ljava/lang/String;

    .line 143
    invoke-virtual {v3}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    .line 108
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 142
    :cond_9
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 144
    :cond_a
    check-cast v2, Ljava/util/List;

    .line 108
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    const-string v1, "remainder"

    .line 111
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    return-object p0

    .line 134
    :cond_c
    new-instance p0, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p0, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
