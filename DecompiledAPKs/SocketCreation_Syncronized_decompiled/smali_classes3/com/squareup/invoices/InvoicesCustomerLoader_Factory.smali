.class public final Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;
.super Ljava/lang/Object;
.source "InvoicesCustomerLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/InvoicesCustomerLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;)",
            "Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/invoices/ClientInvoiceServiceHelper;)Lcom/squareup/invoices/InvoicesCustomerLoader;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/InvoicesCustomerLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/invoices/ClientInvoiceServiceHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/InvoicesCustomerLoader;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, p0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/Scheduler;

    iget-object v2, p0, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/invoices/ClientInvoiceServiceHelper;)Lcom/squareup/invoices/InvoicesCustomerLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/invoices/InvoicesCustomerLoader_Factory;->get()Lcom/squareup/invoices/InvoicesCustomerLoader;

    move-result-object v0

    return-object v0
.end method
