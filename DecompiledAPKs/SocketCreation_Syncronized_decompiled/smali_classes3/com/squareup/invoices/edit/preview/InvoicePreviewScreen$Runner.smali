.class public interface abstract Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;
.super Ljava/lang/Object;
.source "InvoicePreviewScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;",
        "",
        "goBackFromPreview",
        "",
        "invoicePreviewScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
        "onSaveDraftFromPreviewClicked",
        "onSendFromPreviewClicked",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract goBackFromPreview()V
.end method

.method public abstract invoicePreviewScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onSaveDraftFromPreviewClicked()V
.end method

.method public abstract onSendFromPreviewClicked()V
.end method
