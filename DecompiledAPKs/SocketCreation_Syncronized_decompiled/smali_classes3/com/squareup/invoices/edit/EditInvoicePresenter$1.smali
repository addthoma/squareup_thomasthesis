.class Lcom/squareup/invoices/edit/EditInvoicePresenter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditInvoicePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/EditInvoicePresenter;->presentCart(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoicePresenter;

.field final synthetic val$finalI:I


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;I)V
    .locals 0

    .line 544
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter$1;->this$0:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    iput p2, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter$1;->val$finalI:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 546
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter$1;->this$0:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->access$000(Lcom/squareup/invoices/edit/EditInvoicePresenter;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    move-result-object p1

    iget v0, p0, Lcom/squareup/invoices/edit/EditInvoicePresenter$1;->val$finalI:I

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startEditingOrder(I)V

    return-void
.end method
