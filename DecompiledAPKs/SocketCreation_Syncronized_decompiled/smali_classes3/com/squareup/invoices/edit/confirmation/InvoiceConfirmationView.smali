.class public Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;
.super Landroid/widget/LinearLayout;
.source "InvoiceConfirmationView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private copyLinkButton:Lcom/squareup/marketfont/MarketButton;

.field private glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private moreOptionButton:Lcom/squareup/marketfont/MarketButton;

.field presenter:Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progress:Lcom/squareup/ui/DelayedLoadingProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const-class p2, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen$Component;->inject(Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 64
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->presenter:Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->presenter:Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->dropView(Ljava/lang/Object;)V

    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 43
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 44
    sget v0, Lcom/squareup/features/invoices/R$id;->loading_circle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 45
    sget v0, Lcom/squareup/features/invoices/R$id;->glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 46
    sget v0, Lcom/squareup/features/invoices/R$id;->copy_link:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->copyLinkButton:Lcom/squareup/marketfont/MarketButton;

    .line 47
    sget v0, Lcom/squareup/features/invoices/R$id;->more_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->moreOptionButton:Lcom/squareup/marketfont/MarketButton;

    .line 48
    sget v0, Lcom/squareup/features/invoices/R$id;->share_link_email_sent_helper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 50
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->moreOptionButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView$1;-><init>(Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->copyLinkButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView$2;-><init>(Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public setGlyphError(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 111
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphTitleMsg(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->presenter:Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->errored:Z

    return-void
.end method

.method public setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method

.method public setGlyphSuccess(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p3}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 106
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->setGlyphTitleMsg(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method setGlyphTitleMsg(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showGlyph()V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->progress:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    .line 79
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->presenter:Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationPresenter;->startFinishTimer()V

    .line 83
    sget v0, Lcom/squareup/features/invoices/R$id;->content_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 84
    new-instance v1, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView$3;-><init>(Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public showShareLinkButtons(Z)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->copyLinkButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->moreOptionButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showShareLinkHelper()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationView;->messageView:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method
