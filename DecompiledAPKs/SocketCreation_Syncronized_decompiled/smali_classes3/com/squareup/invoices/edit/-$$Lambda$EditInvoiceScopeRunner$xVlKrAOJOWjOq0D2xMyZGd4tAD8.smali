.class public final synthetic Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;

    invoke-direct {v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;-><init>()V

    sput-object v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->lambda$goToAutomaticRemindersScreen$10(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    return-object p1
.end method
