.class public Lcom/squareup/invoices/edit/EditInvoiceScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EditInvoiceScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/edit/EditInvoiceScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/EditInvoiceScope$Component;,
        Lcom/squareup/invoices/edit/EditInvoiceScope$ParentComponent;,
        Lcom/squareup/invoices/edit/EditInvoiceScope$EditInvoiceScopeModule;,
        Lcom/squareup/invoices/edit/EditInvoiceScope$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/EditInvoiceScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

.field private final type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 137
    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScope$eZAANYRtJwQtPSiqdPBCRfopzNQ;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScope$eZAANYRtJwQtPSiqdPBCRfopzNQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScope;->type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    .line 53
    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScope;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/EditInvoiceScope;
    .locals 2

    .line 138
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->valueOf(Ljava/lang/String;)Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    move-result-object v0

    .line 139
    invoke-static {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->readEditInvoiceContext(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object p0

    .line 140
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v0, p0}, Lcom/squareup/invoices/edit/EditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-object v1
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 67
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;

    .line 68
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;

    .line 71
    invoke-interface {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;->editInvoiceWorkflowRunner()Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object p1

    .line 72
    invoke-interface {p1, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 132
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 133
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScope;->type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScope;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-static {p1, p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->writeEditInvoiceContext(Landroid/os/Parcel;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method getEditInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScope;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    return-object v0
.end method

.method public getType()Lcom/squareup/invoices/edit/EditInvoiceScope$Type;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScope;->type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 79
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 80
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 81
    invoke-interface {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;->editInvoiceScopeRunner()Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
