.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;
.super Ljava/lang/Object;
.source "AddInvoiceImageAttachmentDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0005\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000c0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "build",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogScreen;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;>;)",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;->features:Lcom/squareup/settings/server/Features;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory;-><init>(Lcom/squareup/settings/server/Features;Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
