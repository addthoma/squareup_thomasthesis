.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;
.super Ljava/lang/Object;
.source "PaymentRequestValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;

    move-result-object v0

    return-object v0
.end method
