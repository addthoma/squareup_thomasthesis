.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "EditInvoiceViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "frequencyCoordinatorFactory",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;",
        "endsFactory",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;",
        "endsDateFactory",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;",
        "repeatEveryFactory",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;",
        "remindersListCoordinatorFactory",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;",
        "editReminderCoordinatorFactory",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;",
        "editAutomaticPaymentsCoordinatorFactory",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;",
        "(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;)V
    .locals 26
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    const-string v7, "frequencyCoordinatorFactory"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "endsFactory"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "endsDateFactory"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "repeatEveryFactory"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "remindersListCoordinatorFactory"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "editReminderCoordinatorFactory"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "editAutomaticPaymentsCoordinatorFactory"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v7, 0xa

    new-array v7, v7, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 65
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 66
    sget-object v9, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequency;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v10, Lcom/squareup/features/invoices/R$layout;->recurring_frequency_view:I

    .line 67
    new-instance v11, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$1;

    invoke-direct {v11, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;)V

    move-object v13, v11

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v14, 0xc

    const/4 v15, 0x0

    .line 65
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v8, 0x0

    aput-object v0, v7, v8

    .line 70
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 71
    sget-object v10, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEvery;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v11, Lcom/squareup/features/invoices/R$layout;->repeat_every_view:I

    .line 72
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$2;

    invoke-direct {v0, v3}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$2;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;)V

    move-object v14, v0

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/4 v13, 0x0

    const/16 v15, 0xc

    const/16 v16, 0x0

    .line 70
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v3, 0x1

    aput-object v0, v7, v3

    .line 75
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 76
    sget-object v9, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEnds;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v10, Lcom/squareup/features/invoices/R$layout;->ends_view:I

    .line 77
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$3;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$3;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/16 v14, 0xc

    const/4 v15, 0x0

    .line 75
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v7, v1

    .line 80
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 81
    sget-object v9, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDate;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v10, Lcom/squareup/features/invoices/R$layout;->ends_date_view:I

    .line 82
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$4;

    invoke-direct {v0, v2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$4;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 80
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v7, v1

    .line 85
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 86
    sget-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessage;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessage;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    sget v10, Lcom/squareup/features/invoices/R$layout;->invoice_message_view:I

    .line 87
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$5;

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 85
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v7, v1

    .line 90
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/DefaultMessageErrorDialog;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/DefaultMessageErrorDialog;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/message/DefaultMessageErrorDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x5

    aput-object v0, v7, v1

    .line 92
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 93
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 94
    sget v10, Lcom/squareup/features/invoices/R$layout;->invoice_automatic_reminders_list_view:I

    .line 95
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$7;

    invoke-direct {v0, v4}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$7;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 96
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v20, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x1df

    const/16 v25, 0x0

    move-object v14, v0

    invoke-direct/range {v14 .. v25}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v14, 0x8

    move-object v11, v0

    .line 92
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x6

    aput-object v0, v7, v1

    .line 99
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 100
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 101
    sget v10, Lcom/squareup/features/invoices/R$layout;->invoice_automatic_reminder_edit_view:I

    .line 102
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$8;

    invoke-direct {v0, v5}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$8;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 103
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v20, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    move-object v14, v0

    invoke-direct/range {v14 .. v25}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v14, 0x8

    move-object v11, v0

    .line 99
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x7

    aput-object v0, v7, v1

    .line 106
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$9;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$9;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x8

    aput-object v0, v7, v1

    .line 108
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 109
    sget-object v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;->Companion:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 110
    sget v10, Lcom/squareup/features/invoices/R$layout;->invoice_automatic_payment_view:I

    .line 111
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$10;

    invoke-direct {v0, v6}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory$10;-><init>(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/16 v14, 0xc

    .line 108
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x9

    aput-object v0, v7, v1

    move-object/from16 v0, p0

    .line 64
    invoke-direct {v0, v7}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
