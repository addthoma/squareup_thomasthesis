.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "EditPaymentScheduleViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "editPaymentScheduleViewFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;",
        "editPaymentRequestV2ViewFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;",
        "(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2ViewFactory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editPaymentScheduleViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestV2ViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 13
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
