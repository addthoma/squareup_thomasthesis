.class public final Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateDataConverter;
.super Ljava/lang/Object;
.source "InvoiceChooseDateDataConverter.kt"

# interfaces
.implements Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceChooseDateDataConverter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceChooseDateDataConverter.kt\ncom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateDataConverter\n*L\n1#1,46:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0010\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u0007H\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateDataConverter;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;",
        "()V",
        "getMaxDateForCustomDateInfo",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "startDate",
        "toChooseDateInfo",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "customDateInfo",
        "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
        "toCustomDateInfo",
        "chooseDateInfo",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getMaxDateForCustomDateInfo(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    .line 40
    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v1, 0x2

    .line 41
    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->add(II)V

    const/4 v1, 0x5

    .line 42
    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 43
    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    const-string v0, "ProtoDates.calendarToYmd(it)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ProtoDates.calendarFromY\u2026s.calendarToYmd(it)\n    }"

    .line 40
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public toChooseDateInfo(Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 8

    const-string v0, "customDateInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    .line 34
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getDateOptions()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v5

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v6

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getShouldShowActualDate()Z

    move-result v7

    move-object v1, v0

    .line 33
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object v0
.end method

.method public toCustomDateInfo(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
    .locals 8

    const-string v0, "chooseDateInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateDataConverter;->getMaxDateForCustomDateInfo(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v6

    .line 17
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 20
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/ProtoDates;->compareYmd(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result v1

    if-gez v1, :cond_0

    goto :goto_0

    .line 21
    :cond_0
    invoke-static {v0, v6}, Lcom/squareup/util/ProtoDates;->compareYmd(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result v1

    if-lez v1, :cond_2

    const/4 v0, -0x1

    invoke-static {v6, v0}, Lcom/squareup/util/ProtoDates;->addDays(Lcom/squareup/protos/common/time/YearMonthDay;I)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    :cond_2
    :goto_1
    move-object v5, v0

    .line 27
    new-instance v0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getDateOptions()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    const-string v1, "selectedDate"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getShouldShowActualDate()Z

    move-result v7

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object v0
.end method
