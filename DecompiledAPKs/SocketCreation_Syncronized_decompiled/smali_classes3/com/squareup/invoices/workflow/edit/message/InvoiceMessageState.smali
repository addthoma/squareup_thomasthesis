.class public abstract Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;
.super Ljava/lang/Object;
.source "InvoiceMessageState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;,
        Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;,
        Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;,
        Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \n2\u00020\u0001:\u0004\t\n\u000b\u000cB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\r\u0010\u0006\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\u0008\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0005\u0082\u0001\u0003\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "",
        "isDefaultMessageEnabled",
        "Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;",
        "(Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V",
        "()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "takeSnapshot$invoices_hairball_release",
        "CommittingMessage",
        "Companion",
        "ErrorSavingDefaultMessage",
        "UpdatingMessage",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;


# instance fields
.field private final isDefaultMessageEnabled:Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled:Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;-><init>(Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    return-void
.end method


# virtual methods
.method public final isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled:Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    return-object v0
.end method

.method public final takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 43
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
