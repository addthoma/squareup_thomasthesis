.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "EditPaymentRequestCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;-><init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator_Factory_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
