.class public final Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
.super Ljava/lang/Object;
.source "RecurrenceRule.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u001eB\u0017\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003J\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\u001a\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0008\u0008\u0002\u0010\u001c\u001a\u00020\u0015H\u0007J\t\u0010\u001d\u001a\u00020\u0019H\u00d6\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;",
        "",
        "interval",
        "",
        "unit",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;",
        "(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V",
        "getInterval",
        "()I",
        "setInterval",
        "(I)V",
        "getUnit",
        "()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;",
        "setUnit",
        "(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V",
        "changeInterval",
        "changeUnit",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "prettyString",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "withSingularDigit",
        "toString",
        "IntervalUnit",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private interval:I

.field private unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;


# direct methods
.method public constructor <init>(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V
    .locals 1

    const-string/jumbo v0, "unit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    const/4 p4, 0x1

    and-int/2addr p3, p4

    if-eqz p3, :cond_0

    const/4 p1, 0x1

    .line 169
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;-><init>(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->copy(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic prettyString$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/util/Res;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 193
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->prettyString(Lcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final changeInterval(I)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 211
    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p1

    return-object p1
.end method

.method public final changeUnit(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
    .locals 3

    const-string/jumbo v0, "unit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    invoke-static {p0, v0, p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p1

    return-object p1
.end method

.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    return v0
.end method

.method public final component2()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    return-object v0
.end method

.method public final copy(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
    .locals 1

    const-string/jumbo v0, "unit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;-><init>(ILcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    iget v1, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInterval()I
    .locals 1

    .line 169
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    return v0
.end method

.method public final getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final prettyString(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->prettyString$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/util/Res;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final prettyString(Lcom/squareup/util/Res;Z)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_0

    .line 197
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->getStringResSingle()I

    move-result p2

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 199
    :cond_0
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->getStringResUnit()I

    move-result p2

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 202
    :cond_1
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->getStringResPlural()I

    move-result p2

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 203
    iget p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    const-string v0, "count"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 204
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 205
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final setInterval(I)V
    .locals 0

    .line 169
    iput p1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    return-void
.end method

.method public final setUnit(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecurrenceInterval(interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->interval:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->unit:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
