.class public final Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0003J>\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000c\u001a\u00020\r2\u0014\u0008\u0002\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u0014R\'\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;",
        "",
        "()V",
        "KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapperKey;",
        "getKEY",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "actionBarTitle",
        "",
        "forEstimate",
        "",
        "fromDetailsInfo",
        "invoiceDetailsInfo",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "showProgress",
        "disableId",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;-><init>()V

    return-void
.end method

.method private final actionBarTitle(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 80
    sget p1, Lcom/squareup/features/invoices/R$string;->estimate_details_title:I

    goto :goto_0

    .line 82
    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_details_title:I

    :goto_0
    return p1
.end method

.method public static synthetic fromDetailsInfo$default(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;
    .locals 6

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    const/4 p2, 0x0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    move v2, p2

    :goto_0
    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_1

    .line 63
    sget-object p2, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion$fromDetailsInfo$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion$fromDetailsInfo$1;

    move-object p5, p2

    check-cast p5, Lkotlin/jvm/functions/Function1;

    :cond_1
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;->fromDetailsInfo(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZZLkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final fromDetailsInfo(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZZLkotlin/jvm/functions/Function1;)Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
            "ZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;"
        }
    .end annotation

    const-string v0, "invoiceDetailsInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 67
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getId()Ljava/lang/String;

    move-result-object v3

    .line 68
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;->getMessage()Ljava/lang/String;

    move-result-object v4

    xor-int/lit8 v7, p4, 0x1

    .line 72
    move-object p1, p0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;

    invoke-direct {p1, p4}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen$Companion;->actionBarTitle(Z)I

    move-result v8

    move-object v1, v0

    move v5, p2

    move v6, p3

    move-object v9, p5

    .line 65
    invoke-direct/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 86
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsScreen;->access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method
