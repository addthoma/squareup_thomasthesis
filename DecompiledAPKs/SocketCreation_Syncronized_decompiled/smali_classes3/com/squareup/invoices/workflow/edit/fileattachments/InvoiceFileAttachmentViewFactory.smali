.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "InvoiceFileAttachmentViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "invoiceImageCoordinatorFactory",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;",
        "(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceImageCoordinatorFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 12
    sget-object v2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImage;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImage;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImage;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$layout;->invoice_attachment_view:I

    .line 13
    new-instance v4, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 11
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 16
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 17
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/ErrorDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 18
    sget-object v2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 16
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 21
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 22
    sget-object p1, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmation;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmation;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmation;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    sget v4, Lcom/squareup/features/invoices/R$layout;->confirmation_view:I

    .line 23
    sget-object p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentViewFactory$3;

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 21
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
