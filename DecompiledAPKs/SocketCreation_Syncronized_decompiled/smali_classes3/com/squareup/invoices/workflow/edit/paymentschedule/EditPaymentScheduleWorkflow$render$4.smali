.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "clicked",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$4;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$4;->invoke(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked;)V
    .locals 1

    const-string v0, "clicked"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 417
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$DepositClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$DepositClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;-><init>(Z)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    goto :goto_0

    .line 418
    :cond_0
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$BalanceClicked;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$BalanceClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingDepositBalanceRequest;-><init>(Z)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    goto :goto_0

    .line 419
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$InstallmentClicked;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$InstallmentClicked;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$InstallmentClicked;->getIndex()I

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$StartEditingInstallmentRequest;-><init>(I)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    .line 421
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$4;->$sink:Lcom/squareup/workflow/Sink;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 419
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
