.class final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentRequestWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;->invoke(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;->this$0:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;->$event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;->invoke(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)V
    .locals 4

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;->$event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;->getAmountType()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 152
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;->$event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$AmountTypeChanged;->getAmountType()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 p1, 0x5

    goto :goto_1

    .line 160
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    goto :goto_1

    .line 155
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 156
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v0, :cond_4

    goto :goto_0

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$1;->this$0:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;->$state:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->getPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v1, "state.paymentRequestInfo\u2026voiceAmount.currency_code"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    :goto_0
    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    :goto_1
    return-void
.end method
