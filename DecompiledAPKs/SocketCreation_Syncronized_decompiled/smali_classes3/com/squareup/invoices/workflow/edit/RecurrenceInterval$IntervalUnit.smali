.class public final enum Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
.super Ljava/lang/Enum;
.source "RecurrenceRule.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IntervalUnit"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecurrenceRule.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,242:1\n7476#2,2:243\n7736#2,4:245\n*E\n*S KotlinDebug\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit\n*L\n185#1,2:243\n185#1,4:245\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\r\u0008\u0086\u0001\u0018\u0000 \u00132\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0013B\'\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cj\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;",
        "",
        "stringResUnit",
        "",
        "stringResSingle",
        "stringResPlural",
        "rfcString",
        "",
        "(Ljava/lang/String;IIIILjava/lang/String;)V",
        "getRfcString",
        "()Ljava/lang/String;",
        "getStringResPlural",
        "()I",
        "getStringResSingle",
        "getStringResUnit",
        "DAYS",
        "WEEKS",
        "MONTHS",
        "YEARS",
        "Companion",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

.field public static final Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;

.field public static final enum DAYS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

.field public static final enum MONTHS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

.field public static final enum WEEKS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

.field public static final enum YEARS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

.field private static final map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final rfcString:Ljava/lang/String;

.field private final stringResPlural:I

.field private final stringResSingle:I

.field private final stringResUnit:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    new-instance v8, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    .line 178
    sget v4, Lcom/squareup/invoicesappletapi/R$string;->day:I

    sget v5, Lcom/squareup/invoicesappletapi/R$string;->one_day:I

    sget v6, Lcom/squareup/invoicesappletapi/R$string;->plural_days:I

    const-string v2, "DAYS"

    const/4 v3, 0x0

    const-string v7, "DAILY"

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v8, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->DAYS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    const/4 v1, 0x0

    aput-object v8, v0, v1

    new-instance v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    .line 179
    sget v12, Lcom/squareup/invoicesappletapi/R$string;->week:I

    sget v13, Lcom/squareup/invoicesappletapi/R$string;->one_week:I

    sget v14, Lcom/squareup/invoicesappletapi/R$string;->plural_weeks:I

    const-string v10, "WEEKS"

    const/4 v11, 0x1

    const-string v15, "WEEKLY"

    move-object v9, v2

    invoke-direct/range {v9 .. v15}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->WEEKS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    new-instance v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    .line 180
    sget v7, Lcom/squareup/invoicesappletapi/R$string;->month:I

    sget v8, Lcom/squareup/invoicesappletapi/R$string;->one_month:I

    sget v9, Lcom/squareup/invoicesappletapi/R$string;->plural_months:I

    const-string v5, "MONTHS"

    const/4 v6, 0x2

    const-string v10, "MONTHLY"

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->MONTHS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    const/4 v3, 0x2

    aput-object v2, v0, v3

    new-instance v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    .line 181
    sget v7, Lcom/squareup/invoicesappletapi/R$string;->year:I

    sget v8, Lcom/squareup/invoicesappletapi/R$string;->one_year:I

    sget v9, Lcom/squareup/invoicesappletapi/R$string;->plural_years:I

    const-string v5, "YEARS"

    const/4 v6, 0x3

    const-string v10, "YEARLY"

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;-><init>(Ljava/lang/String;IIIILjava/lang/String;)V

    sput-object v2, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->YEARS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    const/4 v3, 0x3

    aput-object v2, v0, v3

    sput-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->$VALUES:[Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit$Companion;

    .line 184
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->values()[Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v0

    .line 243
    array-length v2, v0

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v2

    .line 244
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v3, Ljava/util/Map;

    .line 245
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 185
    iget-object v5, v4, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->rfcString:Ljava/lang/String;

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 248
    :cond_0
    sput-object v3, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->map:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 172
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->stringResUnit:I

    iput p4, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->stringResSingle:I

    iput p5, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->stringResPlural:I

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->rfcString:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getMap$cp()Ljava/util/Map;
    .locals 1

    .line 172
    sget-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->map:Ljava/util/Map;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    .locals 1

    const-class v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->$VALUES:[Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    invoke-virtual {v0}, [Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    return-object v0
.end method


# virtual methods
.method public final getRfcString()Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->rfcString:Ljava/lang/String;

    return-object v0
.end method

.method public final getStringResPlural()I
    .locals 1

    .line 175
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->stringResPlural:I

    return v0
.end method

.method public final getStringResSingle()I
    .locals 1

    .line 174
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->stringResSingle:I

    return v0
.end method

.method public final getStringResUnit()I
    .locals 1

    .line 173
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->stringResUnit:I

    return v0
.end method
