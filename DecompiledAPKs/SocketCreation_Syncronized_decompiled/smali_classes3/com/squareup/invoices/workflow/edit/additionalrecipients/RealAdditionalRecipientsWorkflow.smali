.class public final Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAdditionalRecipientsWorkflow.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAdditionalRecipientsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAdditionalRecipientsWorkflow.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,99:1\n149#2,5:100\n*E\n*S KotlinDebug\n*F\n+ 1 RealAdditionalRecipientsWorkflow.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow\n*L\n63#1,5:100\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001b2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\u001bB\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJt\u0010\u000e\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0003\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u00060\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00050\u000fj6\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n`\u0011J\u001a\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016JN\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;

.field private static final MAX_RECIPIENT_COUNT:I = 0xa


# instance fields
.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .param p1    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method


# virtual methods
.method public final asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation

    .line 75
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public initialState(Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 42
    sget-object v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;->Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;->restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;

    .line 43
    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;->getRecipients()Ljava/util/List;

    move-result-object p1

    .line 42
    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;-><init>(Ljava/util/List;)V

    check-cast p2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    :goto_0
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->initialState(Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->render(Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;

    .line 54
    check-cast p2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;->getRecipients()Ljava/util/List;

    move-result-object p2

    const/16 v0, 0xa

    .line 56
    sget-object v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$render$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$render$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 53
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;-><init>(Ljava/util/List;ILkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 101
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 102
    const-class p3, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 103
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 101
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 64
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->snapshotState(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
