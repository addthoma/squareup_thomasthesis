.class public final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;
.super Ljava/lang/Object;
.source "InvoiceMessageReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u0000 \u001b2\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001\u001bB\u0017\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ*\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000b2\u0006\u0010\u000c\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000eH\u0016J:\u0010\u000f\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00110\u00102\u0006\u0010\u0012\u001a\u00020\u00022\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00142\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u001c\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00180\u00162\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "invoiceDefaultMessageUpdater",
        "Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;",
        "(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)V",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "updateDefaultMessageWorker",
        "Lcom/squareup/workflow/legacy/Worker;",
        "",
        "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
        "message",
        "",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;


# instance fields
.field private final invoiceDefaultMessageUpdater:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceUnitCache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceDefaultMessageUpdater"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->invoiceDefaultMessageUpdater:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    return-void
.end method

.method public static final synthetic access$updateDefaultMessageWorker(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->updateDefaultMessageWorker(Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method private final updateDefaultMessageWorker(Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->invoiceDefaultMessageUpdater:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->update(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 139
    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt;->asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public launch(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 49
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p2

    .line 51
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    if-eqz p1, :cond_0

    .line 52
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->invoiceDefaultsList()Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$launch$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$launch$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$launch$2;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$launch$2;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "invoiceUnitCache.invoice\u2026aultMessageChanged(it)) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 57
    invoke-static {p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$launch$3;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$launch$3;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string/jumbo v1, "workflow.toCompletable()\u2026e { disposables.clear() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-object p2
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->launch(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->onReact(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
