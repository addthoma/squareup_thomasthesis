.class public abstract Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;
.super Ljava/lang/Object;
.source "EditRecurringState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;,
        Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;,
        Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;,
        Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;,
        Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00082\u00020\u0001:\u0005\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0002J\r\u0010\u0005\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\u0008\u0007\u0082\u0001\u0004\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
        "",
        "()V",
        "recurrenceInfo",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "takeSnapshot$invoices_hairball_release",
        "Companion",
        "Ends",
        "EndsDate",
        "Frequency",
        "RepeatEvery",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;->Companion:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;-><init>()V

    return-void
.end method

.method private final recurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;
    .locals 1

    .line 46
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v0

    goto :goto_0

    .line 47
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_1
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
    .locals 4

    .line 54
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;->recurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->isSeriesActive()Z

    move-result v0

    .line 55
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;->recurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v1

    .line 57
    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;ZLcom/squareup/invoices/workflow/edit/RecurrenceRule;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v3}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
