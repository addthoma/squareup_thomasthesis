.class public abstract Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;
.super Ljava/lang/Object;
.source "EditPaymentScheduleScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TextUpdatedEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Deposit;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u0082\u0001\u0002\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;",
        "",
        "()V",
        "isPercentage",
        "",
        "()Z",
        "text",
        "",
        "getText",
        "()Ljava/lang/String;",
        "Deposit",
        "Installment",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Deposit;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 191
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract isPercentage()Z
.end method
