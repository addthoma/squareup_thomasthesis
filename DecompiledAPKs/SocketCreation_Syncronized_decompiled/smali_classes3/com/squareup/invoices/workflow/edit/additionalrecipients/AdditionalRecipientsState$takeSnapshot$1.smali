.class final Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AdditionalRecipientsState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAdditionalRecipientsState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AdditionalRecipientsState.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,31:1\n158#2,3:32\n161#2:37\n1591#3,2:35\n*E\n*S KotlinDebug\n*F\n+ 1 AdditionalRecipientsState.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1\n*L\n18#1,3:32\n18#1:37\n18#1,2:35\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;

    .line 18
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;->getRecipients()Ljava/util/List;

    move-result-object v0

    .line 33
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 34
    check-cast v0, Ljava/lang/Iterable;

    .line 35
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 34
    check-cast v1, Ljava/lang/String;

    .line 18
    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    :cond_0
    return-void
.end method
