.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;
.super Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
.source "EditPaymentRequestInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Balance"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "balanceAmount",
        "Lcom/squareup/protos/common/Money;",
        "depositDueDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V",
        "getBalanceAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getDepositDueDate",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "getPaymentRequest",
        "()Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceAmount:Lcom/squareup/protos/common/Money;

.field private final depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 1

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositDueDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->copy(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;
    .locals 1

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balanceAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositDueDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBalanceAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getDepositDueDate()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object v0
.end method

.method public getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Balance(paymentRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", balanceAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->balanceAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", depositDueDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->depositDueDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
