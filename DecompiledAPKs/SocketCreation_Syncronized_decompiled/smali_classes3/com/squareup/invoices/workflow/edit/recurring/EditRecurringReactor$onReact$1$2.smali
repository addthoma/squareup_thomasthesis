.class final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EditRecurringReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;->isRecurring()Z

    move-result p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    .line 47
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v3

    .line 48
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->getCachedRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    .line 47
    invoke-static/range {v3 .. v8}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/util/Date;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v2

    .line 46
    invoke-static {p1, v2, v1, v0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->copy$default(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/util/Date;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v2

    invoke-static {p1, v2, v1, v0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->copy$default(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    move-result-object p1

    .line 54
    :goto_0
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$2;->invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$FrequencySelected;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
