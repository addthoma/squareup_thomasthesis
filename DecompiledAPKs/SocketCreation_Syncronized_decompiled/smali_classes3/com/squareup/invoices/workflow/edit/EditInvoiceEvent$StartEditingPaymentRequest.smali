.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;
.super Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;
.source "EditInvoiceEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartEditingPaymentRequest"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "editPaymentRequestInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "index",
        "",
        "(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V",
        "getEditPaymentRequestInfo",
        "()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "getIndex",
        "()I",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

.field private final index:I


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V
    .locals 1

    const-string v0, "editPaymentRequestInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    iput p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;IILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->copy(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;
    .locals 1

    const-string v0, "editPaymentRequestInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    iget p1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEditPaymentRequestInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    return-object v0
.end method

.method public final getIndex()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StartEditingPaymentRequest(editPaymentRequestInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->editPaymentRequestInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent$StartEditingPaymentRequest;->index:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
