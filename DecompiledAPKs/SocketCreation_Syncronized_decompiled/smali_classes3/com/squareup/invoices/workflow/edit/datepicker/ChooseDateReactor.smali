.class public final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;
.super Ljava/lang/Object;
.source "ChooseDateReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00122\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0001\u0012B\u000f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u0008\u001a\u00020\tJ:\u0010\n\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u000c0\u000b2\u0006\u0010\r\u001a\u00020\u00022\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "chooseDateDataConverter",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;",
        "(Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;)V",
        "asWorkflow",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateWorkflow;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;


# instance fields
.field private final chooseDateDataConverter:Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->Companion:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chooseDateDataConverter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->chooseDateDataConverter:Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;

    return-void
.end method

.method public static final synthetic access$getChooseDateDataConverter$p(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->chooseDateDataConverter:Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;

    return-object p0
.end method


# virtual methods
.method public final asWorkflow()Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateWorkflow;
    .locals 1

    .line 67
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$asWorkflow$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$asWorkflow$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateWorkflow;

    return-object v0
.end method

.method public launch(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/Reactor$DefaultImpls;->launch(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->launch(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    instance-of p3, p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    if-eqz p3, :cond_0

    new-instance p3, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;

    invoke-direct {p3, p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 55
    :cond_0
    instance-of p3, p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    if-eqz p3, :cond_1

    new-instance p3, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$2;

    invoke-direct {p3, p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$2;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->onReact(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
