.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "isPercentage",
        "",
        "isDeposit",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;->invoke(ZZ)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ZZ)V
    .locals 3

    .line 424
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {v1, p1, v2, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$AmountTypeChanged;-><init>(ZLcom/squareup/protos/common/Money;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
