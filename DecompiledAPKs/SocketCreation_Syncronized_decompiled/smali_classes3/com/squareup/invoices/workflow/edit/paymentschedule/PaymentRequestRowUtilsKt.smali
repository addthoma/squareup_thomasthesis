.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowUtilsKt;
.super Ljava/lang/Object;
.source "PaymentRequestRowUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestRowUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestRowUtils.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowUtilsKt\n+ 2 _Strings.kt\nkotlin/text/StringsKt___StringsKt\n*L\n1#1,7:1\n126#2,6:8\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentRequestRowUtils.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowUtilsKt\n*L\n6#1,6:8\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0008\n\u0002\u0010\r\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "lastDigitIndex",
        "",
        "",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final lastDigitIndex(Ljava/lang/CharSequence;)I
    .locals 3

    const-string v0, "$this$lastDigitIndex"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, -0x1

    add-int/2addr v0, v1

    :goto_0
    if-ltz v0, :cond_1

    .line 9
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 6
    invoke-static {v2}, Lcom/squareup/util/Characters;->isLatinDigit(C)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    return v0
.end method
