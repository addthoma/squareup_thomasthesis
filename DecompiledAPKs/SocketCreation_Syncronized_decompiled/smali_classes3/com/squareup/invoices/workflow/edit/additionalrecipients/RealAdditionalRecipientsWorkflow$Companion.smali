.class public final Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;
.super Ljava/lang/Object;
.source "RealAdditionalRecipientsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAdditionalRecipientsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAdditionalRecipientsWorkflow.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion\n+ 2 LegacyState.kt\ncom/squareup/workflow/legacyintegration/LegacyStateKt\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 4 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,99:1\n74#2,6:100\n74#2,6:108\n335#3:106\n335#3:114\n412#4:107\n412#4:115\n*E\n*S KotlinDebug\n*F\n+ 1 RealAdditionalRecipientsWorkflow.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion\n*L\n85#1,6:100\n92#1,6:108\n85#1:106\n92#1:114\n85#1:107\n92#1:115\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J|\u0010\u0005\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0008\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\t0\u0007\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r0\u0006j6\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r\u0012&\u0012$\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\tj\u0008\u0012\u0004\u0012\u00020\n`\u000f`\u000e2\u0006\u0010\u0010\u001a\u00020\u0008J|\u0010\u0011\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0008\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\t0\u0007\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r0\u0006j6\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\r\u0012&\u0012$\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\tj\u0008\u0012\u0004\u0012\u00020\n`\u000f`\u000e2\u0006\u0010\u0012\u001a\u00020\u0013R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;",
        "",
        "()V",
        "MAX_RECIPIENT_COUNT",
        "",
        "legacyHandleFromInput",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "input",
        "legacyHandleFromSnapshot",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final legacyHandleFromInput(Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 103
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 104
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 107
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p1, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 106
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v2, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    .line 94
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 93
    invoke-direct {v2, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;-><init>(Ljava/util/List;)V

    .line 110
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 111
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 112
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 115
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p1, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 114
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method
