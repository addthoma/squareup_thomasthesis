.class public final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;
.super Ljava/lang/Object;
.source "DeliveryMethodReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final cnpFeesMessageHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cnp/CnpFeesMessageHelper;)Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;-><init>(Lcom/squareup/cnp/CnpFeesMessageHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;->newInstance(Lcom/squareup/cnp/CnpFeesMessageHelper;)Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor_Factory;->get()Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    move-result-object v0

    return-object v0
.end method
