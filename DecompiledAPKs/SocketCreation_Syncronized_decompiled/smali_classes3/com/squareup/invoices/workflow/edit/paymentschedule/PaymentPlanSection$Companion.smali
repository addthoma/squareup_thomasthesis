.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;
.super Ljava/lang/Object;
.source "PaymentPlanSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentPlanSection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentPlanSection.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion\n*L\n1#1,194:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J6\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;",
        "",
        "()V",
        "inflateFromParent",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;",
        "parent",
        "Landroid/view/ViewGroup;",
        "moneyKeyListenerFactory",
        "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
        "unitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "rowCount",
        "",
        "headerText",
        "",
        "currencySymbol",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 174
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final inflateFromParent(Landroid/view/ViewGroup;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;ILjava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;
    .locals 6

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyKeyListenerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "headerText"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencySymbol"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 184
    sget v1, Lcom/squareup/features/invoices/R$layout;->payment_plan_section:I

    const/4 v2, 0x0

    .line 183
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;

    move-object v0, p1

    move v1, p4

    move-object v2, p5

    move-object v3, p6

    move-object v4, p2

    move-object v5, p3

    .line 186
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->initialize(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V

    return-object p1

    .line 183
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.paymentschedule.PaymentPlanSection"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
