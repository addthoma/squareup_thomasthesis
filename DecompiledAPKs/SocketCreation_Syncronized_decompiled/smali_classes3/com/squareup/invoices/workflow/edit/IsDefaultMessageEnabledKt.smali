.class public final Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;
.super Ljava/lang/Object;
.source "IsDefaultMessageEnabled.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "readDefaultMessageEnabled",
        "Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;",
        "Lokio/BufferedSource;",
        "writeDefaultMessageEnabled",
        "",
        "Lokio/BufferedSink;",
        "data",
        "invoices-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final readDefaultMessageEnabled(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;
    .locals 3

    const-string v0, "$this$readDefaultMessageEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 32
    const-class v1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p0, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Disabled;

    check-cast p0, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    goto :goto_0

    .line 33
    :cond_0
    const-class v1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 34
    new-instance v0, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    :goto_0
    return-object p0

    .line 36
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final writeDefaultMessageEnabled(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V
    .locals 2

    const-string v0, "$this$writeDefaultMessageEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "data::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 24
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    if-eqz v0, :cond_0

    .line 25
    check-cast p1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method
