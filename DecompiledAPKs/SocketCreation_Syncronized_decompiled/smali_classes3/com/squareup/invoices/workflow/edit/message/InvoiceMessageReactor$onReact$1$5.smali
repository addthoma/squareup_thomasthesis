.class final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceMessageReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        "it",
        "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/DefaultMessageUpdatedResult;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    instance-of v0, p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Success;

    if-eqz v0, :cond_0

    .line 102
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 103
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getMessage()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v3, v3, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v3, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    invoke-direct {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;-><init>(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    .line 102
    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 105
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Failed;

    if-eqz v0, :cond_1

    .line 106
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 107
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    .line 108
    check-cast p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Failed;

    invoke-virtual {p1}, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Failed;->getErrorTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Failed;->getErrorBody()Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v3, v3, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v3, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getMessage()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v4, v4, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v4, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    move-result-object v4

    .line 107
    invoke-direct {v1, v2, p1, v3, v4}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;)V

    .line 106
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;->invoke(Lcom/squareup/invoices/DefaultMessageUpdatedResult;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
