.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;
.super Ljava/lang/Object;
.source "RealEditPaymentRequestV2Workflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseDateInfoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final dateReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentRequestScreenFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUnitCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final remindersInfoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final remindersWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->remindersWorkflowProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->editPaymentRequestScreenFactoryProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->chooseDateInfoFactoryProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->remindersInfoFactoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->dateReactorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;"
        }
    .end annotation

    .line 64
    new-instance v8, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;
    .locals 9

    .line 73
    new-instance v8, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->remindersWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->editPaymentRequestScreenFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->chooseDateInfoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->remindersInfoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->dateReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    invoke-static/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;

    move-result-object v0

    return-object v0
.end method
