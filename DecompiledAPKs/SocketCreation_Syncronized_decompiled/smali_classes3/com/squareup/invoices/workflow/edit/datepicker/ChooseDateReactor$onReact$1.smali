.class final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseDateReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;->onReact(Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDateReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDateReactor.kt\ncom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,86:1\n85#2,2:87\n85#2,2:89\n85#2,2:91\n85#2,2:93\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDateReactor.kt\ncom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1\n*L\n41#1,2:87\n45#1,2:89\n48#1,2:91\n51#1,2:93\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 87
    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 45
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$2;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 89
    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 48
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$3;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 91
    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 51
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$4;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$4;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 93
    sget-object v1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor$onReact$1$$special$$inlined$onEvent$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
