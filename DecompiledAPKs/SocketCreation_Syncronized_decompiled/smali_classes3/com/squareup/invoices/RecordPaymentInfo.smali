.class public final Lcom/squareup/invoices/RecordPaymentInfo;
.super Ljava/lang/Object;
.source "RecordPaymentInfo.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0017\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00c6\u0003JE\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\t2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u000b\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/RecordPaymentInfo;",
        "",
        "paymentMethod",
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "note",
        "",
        "sendReceipt",
        "",
        "invoiceToken",
        "invoiceVersion",
        "(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getInvoiceToken",
        "()Ljava/lang/String;",
        "getInvoiceVersion",
        "getNote",
        "getPaymentMethod",
        "()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "getSendReceipt",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final invoiceToken:Ljava/lang/String;

.field private final invoiceVersion:Ljava/lang/String;

.field private final note:Ljava/lang/String;

.field private final paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

.field private final sendReceipt:Z


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "paymentMethod"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "note"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceToken"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceVersion"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    iput-object p2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    iput-object p5, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/RecordPaymentInfo;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/RecordPaymentInfo;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/invoices/RecordPaymentInfo;->copy(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/RecordPaymentInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/RecordPaymentInfo;
    .locals 8

    const-string v0, "paymentMethod"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "note"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceToken"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceVersion"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/RecordPaymentInfo;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/RecordPaymentInfo;-><init>(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;Lcom/squareup/protos/common/Money;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/RecordPaymentInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/RecordPaymentInfo;

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    iget-object v1, p1, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getInvoiceToken()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getInvoiceVersion()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    return-object v0
.end method

.method public final getNote()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final getPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    return-object v0
.end method

.method public final getSendReceipt()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecordPaymentInfo(paymentMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->paymentMethod:Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sendReceipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->sendReceipt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/RecordPaymentInfo;->invoiceVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
