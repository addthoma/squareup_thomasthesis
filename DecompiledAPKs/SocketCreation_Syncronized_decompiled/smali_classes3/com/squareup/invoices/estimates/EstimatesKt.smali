.class public final Lcom/squareup/invoices/estimates/EstimatesKt;
.super Ljava/lang/Object;
.source "Estimates.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0002*\u00020\u0001\u00a8\u0006\u0004"
    }
    d2 = {
        "toDeliveryMethod",
        "Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "toPaymentMethod",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toDeliveryMethod(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
    .locals 3

    const-string v0, "$this$toDeliveryMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/invoices/estimates/EstimatesKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 16
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert PaymentMethod "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " to a DeliveryMethod."

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 16
    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 15
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->SHARE_LINK:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    goto :goto_0

    .line 14
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->EMAIL:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    goto :goto_0

    .line 13
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNSELECTED:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    goto :goto_0

    .line 12
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static final toPaymentMethod(Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .locals 1

    const-string v0, "$this$toPaymentMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/invoices/estimates/EstimatesKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    .line 27
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 26
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    goto :goto_0

    .line 25
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNSELECTED:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    goto :goto_0

    .line 24
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    :goto_0
    return-object p0
.end method
