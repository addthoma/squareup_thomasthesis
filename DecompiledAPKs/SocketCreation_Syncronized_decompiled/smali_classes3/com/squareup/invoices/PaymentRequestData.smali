.class public final Lcom/squareup/invoices/PaymentRequestData;
.super Ljava/lang/Object;
.source "PaymentRequestData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/PaymentRequestData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001:\u0001\u0014B9\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\rR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\r\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestData;",
        "",
        "requestType",
        "",
        "formattedDueDate",
        "formattedAmount",
        "statusText",
        "isEditable",
        "",
        "statusColor",
        "",
        "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)V",
        "getFormattedAmount",
        "()Ljava/lang/CharSequence;",
        "getFormattedDueDate",
        "()Z",
        "getRequestType",
        "getStatusColor",
        "()I",
        "getStatusText",
        "Factory",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final formattedAmount:Ljava/lang/CharSequence;

.field private final formattedDueDate:Ljava/lang/CharSequence;

.field private final isEditable:Z

.field private final requestType:Ljava/lang/CharSequence;

.field private final statusColor:I

.field private final statusText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)V
    .locals 1

    const-string v0, "requestType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedDueDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statusText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PaymentRequestData;->requestType:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/invoices/PaymentRequestData;->formattedDueDate:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/invoices/PaymentRequestData;->formattedAmount:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/squareup/invoices/PaymentRequestData;->statusText:Ljava/lang/CharSequence;

    iput-boolean p5, p0, Lcom/squareup/invoices/PaymentRequestData;->isEditable:Z

    iput p6, p0, Lcom/squareup/invoices/PaymentRequestData;->statusColor:I

    return-void
.end method


# virtual methods
.method public final getFormattedAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestData;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getFormattedDueDate()Ljava/lang/CharSequence;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestData;->formattedDueDate:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRequestType()Ljava/lang/CharSequence;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestData;->requestType:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getStatusColor()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/invoices/PaymentRequestData;->statusColor:I

    return v0
.end method

.method public final getStatusText()Ljava/lang/CharSequence;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/PaymentRequestData;->statusText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final isEditable()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/invoices/PaymentRequestData;->isEditable:Z

    return v0
.end method
