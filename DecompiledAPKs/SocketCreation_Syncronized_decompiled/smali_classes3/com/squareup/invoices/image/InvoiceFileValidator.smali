.class public final Lcom/squareup/invoices/image/InvoiceFileValidator;
.super Ljava/lang/Object;
.source "InvoiceFileValidator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000bJ\u001c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/image/InvoiceFileValidator;",
        "",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;)V",
        "addAttachmentValidation",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/FileValidationResult;",
        "totalAttachmentCountOnInvoice",
        "",
        "validateFile",
        "validationInfo",
        "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
        "file",
        "Ljava/io/File;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceUnitCache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iput-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/util/Res;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/invoices/image/InvoiceFileValidator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public final addAttachmentValidation(I)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileValidationResult;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileValidator;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/image/InvoiceFileValidator$addAttachmentValidation$1;-><init>(Lcom/squareup/invoices/image/InvoiceFileValidator;I)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "invoiceUnitCache.unitMet\u2026ess\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final validateFile(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Ljava/io/File;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileValidationResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "validationInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "file"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileValidator;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;-><init>(Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Ljava/io/File;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "invoiceUnitCache.unitMet\u2026ess\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
