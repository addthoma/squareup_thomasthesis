.class final Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;
.super Ljava/lang/Object;
.source "InvoiceFileHelper.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/InvoiceFileHelper;->createFileFromUri(Landroid/net/Uri;Ljava/lang/String;Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceFileHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceFileHelper.kt\ncom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1\n*L\n1#1,222:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Ljava/io/File;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $createFileInExternalDir:Z

.field final synthetic $extension:Ljava/lang/String;

.field final synthetic $uri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/InvoiceFileHelper;ZLjava/lang/String;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-boolean p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->$createFileInExternalDir:Z

    iput-object p3, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->$extension:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->$uri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lcom/squareup/util/Optional;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 80
    iget-boolean v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->$createFileInExternalDir:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-static {v0}, Lcom/squareup/invoices/image/InvoiceFileHelper;->access$getApplication$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-static {v0}, Lcom/squareup/invoices/image/InvoiceFileHelper;->access$getTempPhotoDir$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Ljava/io/File;

    move-result-object v0

    .line 86
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->$extension:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TEMP"

    .line 84
    invoke-static {v3, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 90
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    check-cast v2, Ljava/io/Closeable;

    .line 91
    move-object v3, v1

    check-cast v3, Ljava/lang/Throwable;

    :try_start_0
    move-object v4, v2

    check-cast v4, Ljava/io/FileOutputStream;

    .line 92
    iget-object v5, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-static {v5}, Lcom/squareup/invoices/image/InvoiceFileHelper;->access$getApplication$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Landroid/app/Application;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->$uri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    if-eqz v5, :cond_1

    check-cast v5, Ljava/io/Closeable;

    .line 93
    move-object v6, v1

    check-cast v6, Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    move-object v7, v5

    check-cast v7, Ljava/io/InputStream;

    const-string v8, "input"

    .line 94
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/io/OutputStream;

    const/4 v8, 0x0

    const/4 v9, 0x2

    invoke-static {v7, v4, v8, v9, v1}, Lkotlin/io/ByteStreamsKt;->copyTo$default(Ljava/io/InputStream;Ljava/io/OutputStream;IILjava/lang/Object;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    :try_start_2
    invoke-static {v5, v6}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    :try_start_4
    invoke-static {v5, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 91
    :cond_1
    :goto_1
    invoke-static {v2, v3}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 99
    sget-object v1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v1, v0}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0

    :catchall_2
    move-exception v0

    .line 91
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :catchall_3
    move-exception v1

    invoke-static {v2, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/invoices/image/InvoiceFileHelper$createFileFromUri$1;->call()Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0
.end method
