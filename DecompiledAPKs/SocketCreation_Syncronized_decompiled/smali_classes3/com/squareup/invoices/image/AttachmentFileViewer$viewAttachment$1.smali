.class final Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;
.super Ljava/lang/Object;
.source "AttachmentFileViewer.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/AttachmentFileViewer;->viewAttachment(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "downloadResult",
        "Lcom/squareup/invoices/image/FileDownloadResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $mimeType:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/image/AttachmentFileViewer;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/AttachmentFileViewer;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->this$0:Lcom/squareup/invoices/image/AttachmentFileViewer;

    iput-object p2, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->$mimeType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/image/FileDownloadResult;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/image/FileDownloadResult;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "downloadResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p1, Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 30
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/image/FileDownloadResult$Success;

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->this$0:Lcom/squareup/invoices/image/AttachmentFileViewer;

    invoke-static {v0}, Lcom/squareup/invoices/image/AttachmentFileViewer;->access$getFileViewer$p(Lcom/squareup/invoices/image/AttachmentFileViewer;)Lcom/squareup/invoices/image/FileViewer;

    move-result-object v0

    move-object v1, p1

    check-cast v1, Lcom/squareup/invoices/image/FileDownloadResult$Success;

    invoke-virtual {v1}, Lcom/squareup/invoices/image/FileDownloadResult$Success;->getFile()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->$mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/image/FileViewer;->viewFileExternally(Ljava/io/File;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1$1;-><init>(Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;Lcom/squareup/invoices/image/FileDownloadResult;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/invoices/image/FileDownloadResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/AttachmentFileViewer$viewAttachment$1;->apply(Lcom/squareup/invoices/image/FileDownloadResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
