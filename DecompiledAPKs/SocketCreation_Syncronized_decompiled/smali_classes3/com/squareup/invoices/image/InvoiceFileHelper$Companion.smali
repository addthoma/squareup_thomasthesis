.class public final Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;
.super Ljava/lang/Object;
.source "InvoiceFileHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/image/InvoiceFileHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0006\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;",
        "",
        "()V",
        "fileMetadata",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "invoiceToken",
        "",
        "file",
        "Ljava/io/File;",
        "attachmentToken",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/invoices/image/InvoiceFileHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fileMetadata(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
    .locals 1

    const-string v0, "invoiceToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "file"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;-><init>()V

    .line 193
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object p3

    .line 194
    invoke-static {p2}, Lkotlin/io/FilesKt;->getNameWithoutExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object p3

    .line 195
    invoke-virtual {p3, p1}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->invoice_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object p1

    .line 196
    invoke-static {p2}, Lkotlin/io/FilesKt;->getExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->extension(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object p1

    .line 197
    invoke-static {p2}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->getMimeType(Ljava/io/File;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->mime_type(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object p1

    .line 198
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide p2

    long-to-int p3, p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->size_bytes(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object p1

    .line 199
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object p1

    const-string p2, "FileAttachmentMetadata.B\u2026Int())\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
