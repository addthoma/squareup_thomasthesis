.class public final Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;
.super Ljava/lang/Object;
.source "EstimateFileAttachmentServiceHelper.kt"

# interfaces
.implements Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoEstimateFileAttachmentServiceHelper"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEstimateFileAttachmentServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EstimateFileAttachmentServiceHelper.kt\ncom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,30:1\n151#2,2:31\n*E\n*S KotlinDebug\n*F\n+ 1 EstimateFileAttachmentServiceHelper.kt\ncom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper\n*L\n28#1,2:31\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J%\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H\u0096\u0001J1\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000b0\u00050\u00042\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0096\u0001\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;",
        "Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;",
        "()V",
        "downloadFile",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lokhttp3/ResponseBody;",
        "estimateToken",
        "",
        "attachmentToken",
        "uploadFile",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "file",
        "Ljava/io/File;",
        "mediaType",
        "Lokhttp3/MediaType;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 31
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 32
    const-class v2, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    iput-object v0, p0, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;->$$delegate_0:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    return-void
.end method


# virtual methods
.method public downloadFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation

    const-string v0, "estimateToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;->$$delegate_0:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    invoke-interface {v0, p1, p2}, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;->downloadFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public uploadFile(Ljava/lang/String;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Lokhttp3/MediaType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "estimateToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper$NoEstimateFileAttachmentServiceHelper;->$$delegate_0:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;->uploadFile(Ljava/lang/String;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
