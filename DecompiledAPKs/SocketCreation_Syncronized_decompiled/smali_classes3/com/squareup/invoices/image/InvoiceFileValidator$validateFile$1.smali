.class final Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;
.super Ljava/lang/Object;
.source "InvoiceFileValidator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/InvoiceFileValidator;->validateFile(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Ljava/io/File;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/image/FileValidationResult;",
        "it",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic $validationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

.field final synthetic this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/InvoiceFileValidator;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;

    iput-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->$validationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    iput-object p3, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/invoices/image/FileValidationResult;
    .locals 5

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_total_size_bytes:Ljava/lang/Integer;

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->$validationInfo:Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;->getTotalSizeUploaded()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    add-long/2addr v0, v2

    const-string v2, "sizeLimit"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 29
    new-instance v0, Lcom/squareup/invoices/image/FileValidationResult$Failure;

    .line 30
    iget-object v1, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-static {v1}, Lcom/squareup/invoices/image/InvoiceFileValidator;->access$getRes$p(Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_file_attachment_size_limit_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 31
    iget-object v2, p0, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileValidator;

    invoke-static {v2}, Lcom/squareup/invoices/image/InvoiceFileValidator;->access$getRes$p(Lcom/squareup/invoices/image/InvoiceFileValidator;)Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_file_attachment_size_limit_description:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 32
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v3, p1

    invoke-static {v3, v4}, Lcom/squareup/util/Files;->readableFileSizeFromBytes(J)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v3, "max"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 29
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/image/FileValidationResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/image/FileValidationResult;

    goto :goto_0

    .line 37
    :cond_0
    sget-object p1, Lcom/squareup/invoices/image/FileValidationResult$Success;->INSTANCE:Lcom/squareup/invoices/image/FileValidationResult$Success;

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/image/FileValidationResult;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/InvoiceFileValidator$validateFile$1;->apply(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/invoices/image/FileValidationResult;

    move-result-object p1

    return-object p1
.end method
