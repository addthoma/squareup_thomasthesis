.class final Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1;
.super Ljava/lang/Object;
.source "LocationManager.kt"

# interfaces
.implements Landroid/location/OnNmeaMessageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/core/location/providers/LocationManagerKt;->addNmeaListener24(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "message",
        "",
        "kotlin.jvm.PlatformType",
        "timestamp",
        "",
        "onNmeaMessage"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $listener:Lcom/squareup/core/location/providers/NMEAEventListener;


# direct methods
.method constructor <init>(Lcom/squareup/core/location/providers/NMEAEventListener;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1;->$listener:Lcom/squareup/core/location/providers/NMEAEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNmeaMessage(Ljava/lang/String;J)V
    .locals 2

    .line 46
    invoke-static {}, Lcom/squareup/core/location/providers/LocationManagerKt;->access$getNmeaThreadExecutor$p()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1$1;-><init>(Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1;Ljava/lang/String;J)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method
