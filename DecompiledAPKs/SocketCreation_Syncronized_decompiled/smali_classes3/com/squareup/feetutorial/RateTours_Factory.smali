.class public final Lcom/squareup/feetutorial/RateTours_Factory;
.super Ljava/lang/Object;
.source "RateTours_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/feetutorial/RateTours;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/feetutorial/RateTours_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/feetutorial/RateTours_Factory;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/feetutorial/RateTours_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/feetutorial/RateTours_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/feetutorial/RateTours_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/feetutorial/RateTours_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)Lcom/squareup/feetutorial/RateTours;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/feetutorial/RateTours;

    invoke-direct {v0, p0, p1}, Lcom/squareup/feetutorial/RateTours;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/feetutorial/RateTours;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/feetutorial/RateTours_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/feetutorial/RateTours_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    invoke-static {v0, v1}, Lcom/squareup/feetutorial/RateTours_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;)Lcom/squareup/feetutorial/RateTours;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/feetutorial/RateTours_Factory;->get()Lcom/squareup/feetutorial/RateTours;

    move-result-object v0

    return-object v0
.end method
