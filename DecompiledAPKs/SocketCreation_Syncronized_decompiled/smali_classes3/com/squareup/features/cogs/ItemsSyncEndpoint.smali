.class public final Lcom/squareup/features/cogs/ItemsSyncEndpoint;
.super Lcom/squareup/shared/catalog/CatalogEndpoint;
.source "ItemsSyncEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemsSyncEndpoint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemsSyncEndpoint.kt\ncom/squareup/features/cogs/ItemsSyncEndpoint\n*L\n1#1,73:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0016\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u001c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/features/cogs/ItemsSyncEndpoint;",
        "Lcom/squareup/shared/catalog/CatalogEndpoint;",
        "cogsService",
        "Lcom/squareup/cogs/CogsService;",
        "supportedFeaturesProvider",
        "Lcom/squareup/features/cogs/SupportedFeaturesProvider;",
        "userToken",
        "",
        "canUpdateItems",
        "",
        "(Lcom/squareup/cogs/CogsService;Lcom/squareup/features/cogs/SupportedFeaturesProvider;Ljava/lang/String;Z)V",
        "executeRequest",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Ljava/io/InputStream;",
        "requestBatch",
        "Lcom/squareup/api/rpc/RequestBatch;",
        "mapSyncErrorType",
        "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;",
        "received",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lokhttp3/ResponseBody;",
        "onSyncFailure",
        "cogs_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cogsService:Lcom/squareup/cogs/CogsService;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/CogsService;Lcom/squareup/features/cogs/SupportedFeaturesProvider;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "cogsService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "supportedFeaturesProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object p2, p2, Lcom/squareup/features/cogs/SupportedFeaturesProvider;->featuresToSync:Ljava/util/List;

    const-string v0, "items"

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/squareup/shared/catalog/CatalogEndpoint;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    iput-object p1, p0, Lcom/squareup/features/cogs/ItemsSyncEndpoint;->cogsService:Lcom/squareup/cogs/CogsService;

    return-void
.end method

.method private final mapSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+",
            "Lokhttp3/ResponseBody;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;"
        }
    .end annotation

    .line 59
    move-object v0, p0

    check-cast v0, Lcom/squareup/features/cogs/ItemsSyncEndpoint;

    .line 61
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay;

    if-nez v0, :cond_4

    .line 64
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 65
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 66
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 67
    :cond_2
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 68
    :cond_3
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    :goto_0
    return-object p1

    .line 62
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot map successful response to error"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final onSyncFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+",
            "Lokhttp3/ResponseBody;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-direct {p0, p1}, Lcom/squareup/features/cogs/ItemsSyncEndpoint;->mapSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v1

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occurred while attempting to sync: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 52
    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 54
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.error(syncError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public executeRequest(Lcom/squareup/api/rpc/RequestBatch;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/RequestBatch;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    const-string v0, "requestBatch"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    :try_start_0
    iget-object v0, p0, Lcom/squareup/features/cogs/ItemsSyncEndpoint;->cogsService:Lcom/squareup/cogs/CogsService;

    invoke-interface {v0, p1}, Lcom/squareup/cogs/CogsService;->syncItems(Lcom/squareup/api/rpc/RequestBatch;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 41
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lokhttp3/ResponseBody;

    invoke-virtual {p1}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.of(it.response.byteStream())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/features/cogs/ItemsSyncEndpoint;->onSyncFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    .line 46
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    :goto_0
    return-object p1
.end method
