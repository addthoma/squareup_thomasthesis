.class public final Lcom/squareup/features/invoices/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_another_payment_button:I = 0x7f0a016b

.field public static final allow_automatic_payment:I = 0x7f0a01b0

.field public static final allow_automatic_payment_toggle:I = 0x7f0a01b1

.field public static final allow_save_cof_helper:I = 0x7f0a01b2

.field public static final automatic_payment_glyph:I = 0x7f0a01e0

.field public static final automatic_payment_subtitle:I = 0x7f0a01e1

.field public static final automatic_payment_title:I = 0x7f0a01e2

.field public static final automatic_reminders_container:I = 0x7f0a01e3

.field public static final automatic_reminders_explanation:I = 0x7f0a01e4

.field public static final balance_amount:I = 0x7f0a020e

.field public static final banner_close_button:I = 0x7f0a0218

.field public static final banner_download_app:I = 0x7f0a0219

.field public static final banner_icon:I = 0x7f0a021a

.field public static final banner_message:I = 0x7f0a021b

.field public static final banner_title:I = 0x7f0a021c

.field public static final big_text_header:I = 0x7f0a0229

.field public static final blank_icon:I = 0x7f0a0236

.field public static final button_container:I = 0x7f0a0277

.field public static final buyer_email:I = 0x7f0a0289

.field public static final buyer_name:I = 0x7f0a028d

.field public static final calendar_view:I = 0x7f0a0296

.field public static final cancel_invoice_button:I = 0x7f0a029a

.field public static final choose_days:I = 0x7f0a0334

.field public static final choose_days_offset:I = 0x7f0a0335

.field public static final choose_interval_unit:I = 0x7f0a0337

.field public static final confirmation_glyph_message:I = 0x7f0a038e

.field public static final confirmation_text:I = 0x7f0a038f

.field public static final content_frame:I = 0x7f0a03a6

.field public static final copy_link:I = 0x7f0a03af

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final customer_info_title:I = 0x7f0a0538

.field public static final date_options:I = 0x7f0a0547

.field public static final delivery_method_container:I = 0x7f0a0566

.field public static final delivery_method_options:I = 0x7f0a0567

.field public static final description_row:I = 0x7f0a058a

.field public static final disabled_text_box:I = 0x7f0a05c2

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final draft_amount:I = 0x7f0a05eb

.field public static final draft_container:I = 0x7f0a05ec

.field public static final draft_header:I = 0x7f0a05ed

.field public static final due_date:I = 0x7f0a0607

.field public static final edit_invoice_1_container:I = 0x7f0a061e

.field public static final edit_invoice_2_container:I = 0x7f0a061f

.field public static final edit_invoice_bottom_button:I = 0x7f0a0620

.field public static final edit_invoice_file_attachment_header:I = 0x7f0a0621

.field public static final edit_invoice_file_attachments_container:I = 0x7f0a0622

.field public static final edit_invoice_frequency:I = 0x7f0a0623

.field public static final edit_invoice_progress_bar:I = 0x7f0a0624

.field public static final edit_invoice_scroll_view:I = 0x7f0a0625

.field public static final edit_payment_request_due_row:I = 0x7f0a0656

.field public static final edit_payment_request_header_subtitle:I = 0x7f0a0657

.field public static final edit_payment_request_header_title:I = 0x7f0a0658

.field public static final edit_payment_request_reminder_row:I = 0x7f0a0659

.field public static final edit_photo_error_message:I = 0x7f0a065b

.field public static final edit_photo_image:I = 0x7f0a065d

.field public static final edit_photo_progress:I = 0x7f0a065e

.field public static final email_option:I = 0x7f0a06c3

.field public static final emails_container:I = 0x7f0a06cf

.field public static final enable_automatic_reminders:I = 0x7f0a06fa

.field public static final ends_date_calendar_view:I = 0x7f0a0710

.field public static final ends_never:I = 0x7f0a0711

.field public static final ends_number:I = 0x7f0a0712

.field public static final ends_options:I = 0x7f0a0713

.field public static final ends_set_date:I = 0x7f0a0714

.field public static final file_name:I = 0x7f0a074f

.field public static final fixed_amount_button:I = 0x7f0a075d

.field public static final fixed_amount_text:I = 0x7f0a075e

.field public static final fixed_text_box:I = 0x7f0a0762

.field public static final frequency_options:I = 0x7f0a0775

.field public static final glyph_message:I = 0x7f0a07b1

.field public static final image_container:I = 0x7f0a0822

.field public static final image_icon_animator:I = 0x7f0a0823

.field public static final interval_day:I = 0x7f0a0868

.field public static final interval_month:I = 0x7f0a0869

.field public static final interval_options:I = 0x7f0a086a

.field public static final interval_week:I = 0x7f0a086b

.field public static final interval_year:I = 0x7f0a086c

.field public static final invoice_add_payment_schedule_button:I = 0x7f0a0871

.field public static final invoice_add_reminder:I = 0x7f0a0872

.field public static final invoice_additional_emails_container:I = 0x7f0a0873

.field public static final invoice_allow_buyer_save_cof:I = 0x7f0a0874

.field public static final invoice_attach_file:I = 0x7f0a0875

.field public static final invoice_attach_file_divider:I = 0x7f0a0876

.field public static final invoice_attachment_progress_bar:I = 0x7f0a0877

.field public static final invoice_automatic_reminders_list_container:I = 0x7f0a0878

.field public static final invoice_automatic_reminders_toggle:I = 0x7f0a0879

.field public static final invoice_bill_history_view:I = 0x7f0a087a

.field public static final invoice_custom_message:I = 0x7f0a087c

.field public static final invoice_customer:I = 0x7f0a087d

.field public static final invoice_date_options:I = 0x7f0a087e

.field public static final invoice_delivery:I = 0x7f0a0880

.field public static final invoice_detail_timeline_content:I = 0x7f0a088a

.field public static final invoice_details_id:I = 0x7f0a088b

.field public static final invoice_details_message:I = 0x7f0a088c

.field public static final invoice_details_title:I = 0x7f0a088d

.field public static final invoice_due_date:I = 0x7f0a088e

.field public static final invoice_edit_action:I = 0x7f0a088f

.field public static final invoice_edit_add_line_item:I = 0x7f0a0890

.field public static final invoice_edit_line_items_container:I = 0x7f0a0891

.field public static final invoice_edit_payment_schedule_button:I = 0x7f0a0892

.field public static final invoice_edit_request_deposit:I = 0x7f0a0893

.field public static final invoice_end_helper:I = 0x7f0a0894

.field public static final invoice_filter_container:I = 0x7f0a0898

.field public static final invoice_filter_drop_down_arrow:I = 0x7f0a0899

.field public static final invoice_filter_dropdown:I = 0x7f0a089a

.field public static final invoice_filter_listview:I = 0x7f0a089b

.field public static final invoice_filter_options_container:I = 0x7f0a089c

.field public static final invoice_filter_title:I = 0x7f0a089d

.field public static final invoice_frequency_helper:I = 0x7f0a089e

.field public static final invoice_history_animator:I = 0x7f0a08a1

.field public static final invoice_history_progress_bar:I = 0x7f0a08a2

.field public static final invoice_item_search:I = 0x7f0a08a4

.field public static final invoice_list:I = 0x7f0a08a5

.field public static final invoice_name:I = 0x7f0a08a7

.field public static final invoice_number:I = 0x7f0a08a8

.field public static final invoice_options_title:I = 0x7f0a08a9

.field public static final invoice_payment_schedule_container:I = 0x7f0a08aa

.field public static final invoice_payment_schedule_divider:I = 0x7f0a08ab

.field public static final invoice_payment_schedule_list:I = 0x7f0a08ac

.field public static final invoice_preview_action:I = 0x7f0a08af

.field public static final invoice_recurring_period_helper:I = 0x7f0a08b2

.field public static final invoice_reminder_confirmation_message:I = 0x7f0a08b3

.field public static final invoice_reminder_custom_message:I = 0x7f0a08b4

.field public static final invoice_reminder_custom_message_title:I = 0x7f0a08b5

.field public static final invoice_remove_reminder:I = 0x7f0a08b6

.field public static final invoice_repeat_helper:I = 0x7f0a08b7

.field public static final invoice_request_shipping_address:I = 0x7f0a08b8

.field public static final invoice_search:I = 0x7f0a08b9

.field public static final invoice_search_activator:I = 0x7f0a08ba

.field public static final invoice_send_date:I = 0x7f0a08bd

.field public static final invoice_tippable:I = 0x7f0a08bf

.field public static final invoices_list_error_message:I = 0x7f0a08c2

.field public static final legal_message:I = 0x7f0a0927

.field public static final line_items_divider:I = 0x7f0a093d

.field public static final line_items_section:I = 0x7f0a093e

.field public static final line_items_title:I = 0x7f0a093f

.field public static final loading_circle:I = 0x7f0a094f

.field public static final loading_instruments:I = 0x7f0a0952

.field public static final manual_option:I = 0x7f0a09a9

.field public static final message:I = 0x7f0a09d3

.field public static final message_attachment_disabled:I = 0x7f0a09d4

.field public static final more_options:I = 0x7f0a09f4

.field public static final no_search_results:I = 0x7f0a0a2e

.field public static final notify_recipients:I = 0x7f0a0a71

.field public static final null_state_image:I = 0x7f0a0a78

.field public static final null_state_message:I = 0x7f0a0a79

.field public static final null_state_title:I = 0x7f0a0a7b

.field public static final number_box:I = 0x7f0a0a7e

.field public static final one_time:I = 0x7f0a0aa2

.field public static final opt_in_message:I = 0x7f0a0ab9

.field public static final option_after_date:I = 0x7f0a0aba

.field public static final option_before_date:I = 0x7f0a0abb

.field public static final option_on_date:I = 0x7f0a0abd

.field public static final outstanding_amount:I = 0x7f0a0b74

.field public static final outstanding_container:I = 0x7f0a0b75

.field public static final outstanding_header:I = 0x7f0a0b76

.field public static final overview_header:I = 0x7f0a0b7b

.field public static final paid_amount:I = 0x7f0a0b87

.field public static final paid_container:I = 0x7f0a0b88

.field public static final paid_header:I = 0x7f0a0b89

.field public static final payment_amount_options_container:I = 0x7f0a0be3

.field public static final payment_plan_section_helper:I = 0x7f0a0bec

.field public static final payment_request_summary_text:I = 0x7f0a0bf2

.field public static final payment_type_button:I = 0x7f0a0bf5

.field public static final payment_type_container:I = 0x7f0a0bf6

.field public static final payment_type_helper_text:I = 0x7f0a0bf7

.field public static final payment_type_image:I = 0x7f0a0bf9

.field public static final payment_type_subtitle:I = 0x7f0a0bfd

.field public static final payment_type_title:I = 0x7f0a0bff

.field public static final pdf_icon:I = 0x7f0a0c07

.field public static final percentage_button:I = 0x7f0a0c0c

.field public static final percentage_text:I = 0x7f0a0c0f

.field public static final percentage_text_box:I = 0x7f0a0c10

.field public static final portable_library_list:I = 0x7f0a0c3c

.field public static final preview_footer_row:I = 0x7f0a0c4a

.field public static final preview_invoice_progress_bar:I = 0x7f0a0c4b

.field public static final preview_save_draft:I = 0x7f0a0c4c

.field public static final preview_web_progress_bar:I = 0x7f0a0c4d

.field public static final preview_webview:I = 0x7f0a0c4e

.field public static final progress_bar_load_invoice:I = 0x7f0a0c7d

.field public static final progress_container:I = 0x7f0a0c7f

.field public static final radio_group:I = 0x7f0a0cda

.field public static final record_payment_amount:I = 0x7f0a0d17

.field public static final record_payment_method:I = 0x7f0a0d18

.field public static final record_payment_method_container:I = 0x7f0a0d19

.field public static final record_payment_note:I = 0x7f0a0d1a

.field public static final record_payment_send_receipt:I = 0x7f0a0d1b

.field public static final recurrence_end:I = 0x7f0a0d1c

.field public static final recurrence_options:I = 0x7f0a0d1d

.field public static final recurring:I = 0x7f0a0d1e

.field public static final remove_attachment_button:I = 0x7f0a0d58

.field public static final remove_payment_request:I = 0x7f0a0d59

.field public static final remove_payment_request_button:I = 0x7f0a0d5a

.field public static final repeat_every:I = 0x7f0a0d5e

.field public static final request_deposit_toggle:I = 0x7f0a0d6b

.field public static final request_type_radio_group:I = 0x7f0a0d6c

.field public static final row_container:I = 0x7f0a0da1

.field public static final save_default_message:I = 0x7f0a0e01

.field public static final save_message_as_default_toggle:I = 0x7f0a0e03

.field public static final section_container:I = 0x7f0a0e37

.field public static final section_header:I = 0x7f0a0e38

.field public static final set_default_message_helper:I = 0x7f0a0e69

.field public static final share_link_email_sent_helper:I = 0x7f0a0e75

.field public static final split_balance_toggle:I = 0x7f0a0ece

.field public static final tap_to_preview_container:I = 0x7f0a0f6f

.field public static final upload_progress_bar:I = 0x7f0a10d2

.field public static final uploaded_time_stamp:I = 0x7f0a10d4

.field public static final uploading_text:I = 0x7f0a10d5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
