.class public final Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;
.super Ljava/lang/Object;
.source "InvoiceV1SectionRenderer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/text/Formatter;)Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;-><init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartEntryViewsFactory;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1, v2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->newInstance(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;Lcom/squareup/text/Formatter;)Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer_Factory;->get()Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    move-result-object v0

    return-object v0
.end method
