.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;
.super Ljava/lang/Object;
.source "InvoiceSectionContainerViewFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceSectionContainerViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceSectionContainerViewFactory.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,75:1\n1651#2,3:76\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceSectionContainerViewFactory.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory\n*L\n43#1,3:76\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J(\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;",
        "renderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "(Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;)V",
        "create",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;",
        "invoiceSectionData",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "context",
        "Landroid/content/Context;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "useV2Widgets",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final renderer:Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "renderer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;->renderer:Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;
    .locals 7

    const-string v0, "invoiceSectionData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    .line 27
    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$style;->Theme_Invoices_BigNohoRow:I

    invoke-direct {v0, p2, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object p2, v0

    check-cast p2, Landroid/content/Context;

    .line 32
    :cond_0
    new-instance v0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;

    invoke-direct {v0, p2, p4}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;-><init>(Landroid/content/Context;Z)V

    .line 34
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->getHeader()Lcom/squareup/features/invoices/widgets/Header;

    move-result-object v1

    .line 35
    instance-of v2, v1, Lcom/squareup/features/invoices/widgets/Header$Show;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    check-cast v1, Lcom/squareup/features/invoices/widgets/Header$Show;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/Header$Show;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->setHeaderTextAndVisibility(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_1
    sget-object v2, Lcom/squareup/features/invoices/widgets/Header$None;->INSTANCE:Lcom/squareup/features/invoices/widgets/Header$None;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v3}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->setHeaderTextAndVisibility(Ljava/lang/String;)V

    .line 39
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->getShowTopDivider()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->setTopDividerVisibleOrGone(Z)V

    .line 40
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->getShowBottomDivider()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->setBottomDividerVisibleOrGone(Z)V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->getElements()Ljava/util/List;

    move-result-object p1

    .line 43
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;->renderer:Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    .line 44
    check-cast p1, Ljava/lang/Iterable;

    const/4 v2, 0x0

    .line 77
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v2, 0x1

    if-gez v2, :cond_3

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    check-cast v4, Lcom/squareup/features/invoices/widgets/SectionElement;

    if-eqz p4, :cond_5

    if-eqz v2, :cond_5

    .line 48
    instance-of v2, v4, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    const/4 v6, 0x2

    if-eqz v2, :cond_4

    .line 49
    new-instance v2, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;

    invoke-direct {v2, p2, v3, v6, v3}, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    goto :goto_2

    .line 51
    :cond_4
    new-instance v2, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    invoke-direct {v2, p2, v3, v6, v3}, Lcom/squareup/features/invoices/widgets/InvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 53
    :goto_2
    check-cast v2, Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->addViewToContainer(Landroid/view/View;)V

    .line 60
    :cond_5
    move-object v2, v0

    check-cast v2, Landroid/view/ViewGroup;

    .line 58
    invoke-interface {v1, v4, p3, v2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 57
    invoke-virtual {v0, v2}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->addViewToContainer(Landroid/view/View;)V

    move v2, v5

    goto :goto_1

    .line 66
    :cond_6
    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;

    return-object v0
.end method
