.class public final Lcom/squareup/features/invoices/widgets/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final cart_entry_image:I = 0x7f0a02cd

.field public static final content_cont:I = 0x7f0a03a5

.field public static final description:I = 0x7f0a0589

.field public static final event_detailed_full:I = 0x7f0a072b

.field public static final event_detailed_truncated:I = 0x7f0a072c

.field public static final event_subtitle:I = 0x7f0a072d

.field public static final event_title:I = 0x7f0a072e

.field public static final icon:I = 0x7f0a0818

.field public static final invoice_bottom_divider:I = 0x7f0a087b

.field public static final invoice_event_bullet:I = 0x7f0a0895

.field public static final invoice_event_bullet_highlight:I = 0x7f0a0896

.field public static final invoice_event_line:I = 0x7f0a0897

.field public static final invoice_help_text:I = 0x7f0a08a0

.field public static final invoice_section_container:I = 0x7f0a08bb

.field public static final invoice_section_header:I = 0x7f0a08bc

.field public static final invoice_top_divider:I = 0x7f0a08c0

.field public static final message:I = 0x7f0a09d3

.field public static final preview_webview:I = 0x7f0a0c4e

.field public static final timeline_content:I = 0x7f0a1030

.field public static final title:I = 0x7f0a103f

.field public static final toggle:I = 0x7f0a1049

.field public static final view_all_activity:I = 0x7f0a10ed


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
