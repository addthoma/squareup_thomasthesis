.class public final Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;
.super Lcom/squareup/ui/cart/CartEntryViewsFactory;
.source "InvoiceV2CartEntryViewsFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;",
        "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
        "factory",
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "inflater",
        "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;",
        "(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inflater"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-static {p2}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactoryKt;->access$config(Lcom/squareup/util/Res;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;->create(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    check-cast p4, Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/CartEntryViewsFactory;-><init>(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;)V

    return-void
.end method
