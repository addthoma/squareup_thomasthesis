.class public interface abstract Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;
.super Ljava/lang/Object;
.source "InvoiceSectionRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J(\u0010\u0002\u001a\u00020\u0003*\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u00102\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u00112\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u00122\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J\"\u0010\u0002\u001a\u00020\u0013*\u00020\u00142\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0016H&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u00172\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u00182\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J\u0014\u0010\u0002\u001a\u00020\u0003*\u00020\u00192\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0014\u0010\u0002\u001a\u00020\u0003*\u00020\u001a2\u0006\u0010\n\u001a\u00020\u000bH&J\u0014\u0010\u0002\u001a\u00020\u0003*\u00020\u001b2\u0006\u0010\n\u001a\u00020\u000bH&J\u0014\u0010\u0002\u001a\u00020\u0003*\u00020\u001c2\u0006\u0010\n\u001a\u00020\u000bH&J\u0014\u0010\u0002\u001a\u00020\u0003*\u00020\u001d2\u0006\u0010\n\u001a\u00020\u000bH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u001e2\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\u001f2\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J\u0014\u0010\u0002\u001a\u00020 *\u00020!2\u0006\u0010\n\u001a\u00020\u000bH&J(\u0010\u0002\u001a\u00020\u0003*\u00020\"2\u0006\u0010\u0007\u001a\u00020\u00082\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J(\u0010\u0002\u001a\u00020\u0003*\u00020#2\u0006\u0010\u0007\u001a\u00020\u00082\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&J\u0014\u0010\u0002\u001a\u00020\u0003*\u00020$2\u0006\u0010\n\u001a\u00020\u000bH&J\u001c\u0010\u0002\u001a\u00020\u0003*\u00020%2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006H&J(\u0010\u0002\u001a\u00020\u0003*\u00020&2\u0006\u0010\u0007\u001a\u00020\u00082\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH&\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "",
        "render",
        "Landroid/view/View;",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
        "context",
        "Landroid/content/Context;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
        "",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
        "Lcom/squareup/noho/NohoRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
        "onEditClicked",
        "Lkotlin/Function0;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
        "Landroid/webkit/WebView;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;Landroid/content/Context;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;Landroid/content/Context;)Landroid/webkit/WebView;
.end method

.method public abstract render(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoRow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoRow;"
        }
    .end annotation
.end method
