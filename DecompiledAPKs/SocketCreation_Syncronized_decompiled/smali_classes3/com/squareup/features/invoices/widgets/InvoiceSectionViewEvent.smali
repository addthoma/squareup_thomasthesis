.class public abstract Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;
.super Ljava/lang/Object;
.source "InvoiceSectionViewEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;,
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$EditTextChanged;,
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;,
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;,
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;,
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;,
        Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0006\u0007\u0008\t\n\u000b\u000cB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\u0082\u0001\u0007\r\u000e\u000f\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
        "",
        "key",
        "(Ljava/lang/Object;)V",
        "getKey",
        "()Ljava/lang/Object;",
        "AttachmentClicked",
        "EditTextChanged",
        "LineItemClicked",
        "PaymentRequestClicked",
        "Simple",
        "TimelineCtaClicked",
        "ToggleClicked",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$EditTextChanged;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$LineItemClicked;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final key:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;->key:Ljava/lang/Object;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;->key:Ljava/lang/Object;

    return-object v0
.end method
