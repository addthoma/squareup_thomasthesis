.class public final Lcom/squareup/features/invoices/widgets/impl/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final cart_entry_image_wrapper_layout:I = 0x7f0d00c6

.field public static final cart_entry_indented_wrapper_layout:I = 0x7f0d00c7

.field public static final expandable_view:I = 0x7f0d0266

.field public static final info_box:I = 0x7f0d02d0

.field public static final invoice_bottom_dialog_button:I = 0x7f0d02df

.field public static final invoice_bottom_dialog_confirm_button:I = 0x7f0d02e0

.field public static final invoice_cart_entry_view:I = 0x7f0d02e2

.field public static final invoice_confirm_button:I = 0x7f0d02e4

.field public static final invoice_container_view:I = 0x7f0d02e7

.field public static final invoice_container_view_v2:I = 0x7f0d02e8

.field public static final invoice_event_row:I = 0x7f0d02ea

.field public static final invoice_help_text:I = 0x7f0d02ec

.field public static final invoice_preview_webview:I = 0x7f0d02f5

.field public static final invoice_section_container_header:I = 0x7f0d02f8

.field public static final invoice_timeline_view:I = 0x7f0d02fd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
