.class public final Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;
.super Ljava/lang/Object;
.source "InvoiceV2SectionRenderer.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceV2SectionRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceV2SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,294:1\n1103#2,7:295\n1103#2,7:302\n1103#2,7:313\n1103#2,7:321\n1103#2,7:329\n1642#3,2:309\n1651#3,2:311\n1653#3:320\n1642#3:328\n1643#3:336\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceV2SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer\n*L\n84#1,7:295\n100#1,7:302\n211#1,7:313\n244#1,7:321\n265#1,7:329\n186#1,2:309\n211#1,2:311\n211#1:320\n265#1:328\n265#1:336\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00da\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ>\u0010\u000f\u001a\u00020\u0010*\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00152\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u00182\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0014\u0010\u001c\u001a\u00020\u0010*\u00020\u001d2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u001a\u0010\u001c\u001a\u00020\u0010*\u0008\u0012\u0004\u0012\u00020\u001d0\u00152\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u001c\u0010\u001e\u001a\u00020\u001f*\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0016J(\u0010\u001e\u001a\u00020\u001f*\u00020%2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016J(\u0010\u001e\u001a\u00020\u001f*\u00020(2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016J(\u0010\u001e\u001a\u00020\u001f*\u00020)2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016J)\u0010\u001e\u001a\u00020\u001f*\u00020*2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0096\u0001J#\u0010\u001e\u001a\u00020+*\u00020,2\u0006\u0010#\u001a\u00020$2\u000c\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u00100.H\u0096\u0001J(\u0010\u001e\u001a\u00020\u001f*\u00020/2\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016J)\u0010\u001e\u001a\u00020\u001f*\u0002002\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0096\u0001J\u0015\u0010\u001e\u001a\u00020\u001f*\u0002012\u0006\u0010#\u001a\u00020$H\u0096\u0001J\u0015\u0010\u001e\u001a\u00020\u001f*\u0002022\u0006\u0010&\u001a\u00020\'H\u0096\u0001J\u0015\u0010\u001e\u001a\u00020\u001f*\u0002032\u0006\u0010&\u001a\u00020\'H\u0096\u0001J\u0015\u0010\u001e\u001a\u00020\u001f*\u0002042\u0006\u0010&\u001a\u00020\'H\u0096\u0001J\u0015\u0010\u001e\u001a\u00020\u001f*\u0002052\u0006\u0010&\u001a\u00020\'H\u0096\u0001J)\u0010\u001e\u001a\u00020\u001f*\u0002062\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0096\u0001J)\u0010\u001e\u001a\u00020\u001f*\u0002072\u0006\u0010&\u001a\u00020\'2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0096\u0001J\u0015\u0010\u001e\u001a\u000208*\u0002092\u0006\u0010&\u001a\u00020\'H\u0096\u0001J(\u0010\u001e\u001a\u00020+*\u00020:2\u0006\u0010#\u001a\u00020$2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016J(\u0010\u001e\u001a\u00020\u001f*\u00020;2\u0006\u0010#\u001a\u00020$2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016J\u0015\u0010\u001e\u001a\u00020\u001f*\u00020<2\u0006\u0010&\u001a\u00020\'H\u0096\u0001J\u001d\u0010\u001e\u001a\u00020\u001f*\u00020=2\u0006\u0010&\u001a\u00020\'2\u0006\u0010!\u001a\u00020\"H\u0096\u0001J(\u0010\u001e\u001a\u00020\u001f*\u00020>2\u0006\u0010#\u001a\u00020$2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u0018H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "v1Renderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
        "resources",
        "Landroid/content/res/Resources;",
        "cartEntryViewsFactory",
        "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "editPaymentRequestsViewFactory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;",
        "(Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Landroid/content/res/Resources;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;)V",
        "cartImageSize",
        "",
        "addCartItemsTo",
        "",
        "Lcom/squareup/ui/cart/CartEntryViews;",
        "lineItemsContainer",
        "Landroid/widget/LinearLayout;",
        "itemizations",
        "",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
        "event",
        "",
        "addIndentedTo",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "render",
        "Landroid/view/View;",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
        "context",
        "Landroid/content/Context;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
        "Lcom/squareup/noho/NohoRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
        "onEditClicked",
        "Lkotlin/Function0;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
        "Landroid/webkit/WebView;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

.field private final cartEntryViewsFactory:Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

.field private final cartImageSize:I

.field private final editPaymentRequestsViewFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Landroid/content/res/Resources;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "v1Renderer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartEntryViewsFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestsViewFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->cartEntryViewsFactory:Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p5, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->editPaymentRequestsViewFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;

    .line 56
    sget p1, Lcom/squareup/features/invoices/widgets/R$dimen;->v2_row_image_size:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->cartImageSize:I

    return-void
.end method

.method private final addCartItemsTo(Lcom/squareup/ui/cart/CartEntryViews;Landroid/widget/LinearLayout;Ljava/util/List;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViews;",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    move-object v7, p0

    .line 211
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/cart/CartEntryViews;->getOrderItemViews()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 312
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v9, v1, 0x1

    if-gez v1, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v0, Lcom/squareup/ui/cart/CartEntryView;

    .line 213
    invoke-virtual/range {p2 .. p2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 214
    sget v3, Lcom/squareup/features/invoices/widgets/impl/R$layout;->cart_entry_image_wrapper_layout:I

    const/4 v4, 0x0

    .line 212
    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object v10, v2

    check-cast v10, Landroid/widget/LinearLayout;

    .line 216
    sget v2, Lcom/squareup/features/invoices/widgets/impl/R$id;->cart_entry_image:I

    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v11, p3

    .line 217
    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization;

    .line 219
    iget v4, v7, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->cartImageSize:I

    const-string v5, "image"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 218
    invoke-static {v3, v4, v5}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItemization(Lcom/squareup/protos/client/bills/Itemization;ILandroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object v4

    .line 221
    iget-object v5, v7, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {v5, v3}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object v3

    .line 222
    iget v5, v7, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->cartImageSize:I

    check-cast v4, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v2, v4}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 223
    check-cast v0, Landroid/view/View;

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 224
    sget-object v0, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    move-object/from16 v12, p5

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 225
    move-object v13, v10

    check-cast v13, Landroid/view/View;

    .line 313
    new-instance v14, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;

    move-object v0, v14

    move-object v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$addCartItemsTo$$inlined$forEachIndexed$lambda$1;-><init>(ILcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;Landroid/widget/LinearLayout;Ljava/util/List;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V

    check-cast v14, Landroid/view/View$OnClickListener;

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    :cond_1
    check-cast v10, Landroid/view/View;

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v1, v9

    goto :goto_0

    .line 212
    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.LinearLayout"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method

.method private final addIndentedTo(Lcom/squareup/ui/cart/CartEntryView;Landroid/widget/LinearLayout;)V
    .locals 3

    .line 195
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$layout;->cart_entry_indented_wrapper_layout:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 196
    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 197
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    .line 195
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.widget.LinearLayout"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final addIndentedTo(Ljava/util/List;Landroid/widget/LinearLayout;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;",
            "Landroid/widget/LinearLayout;",
            ")V"
        }
    .end annotation

    .line 186
    check-cast p1, Ljava/lang/Iterable;

    .line 309
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartEntryView;

    .line 186
    invoke-direct {p0, v0, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addIndentedTo(Lcom/squareup/ui/cart/CartEntryView;Landroid/widget/LinearLayout;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    move-object/from16 v6, p2

    const-string v0, "$this$render"

    move-object v7, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    move-object/from16 v8, p3

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    new-instance v9, Landroid/widget/LinearLayout;

    invoke-direct {v9, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 266
    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 267
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 269
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;->getAttachmentList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 328
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 271
    new-instance v12, Lcom/squareup/noho/NohoRow;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, v12

    move-object/from16 v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v11, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, v11, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v12, v0}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, v11, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/squareup/util/Files;->readableFileSizeFromBytes(J)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v12, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 274
    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    sget v1, Lcom/squareup/features/invoices/widgets/R$drawable;->add_attachment_row_icon:I

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {v12, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 275
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_Button_Label:I

    invoke-virtual {v12, v0}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 276
    check-cast v12, Landroid/view/View;

    .line 329
    new-instance v13, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$2;

    move-object v0, v13

    move-object v1, v11

    move-object v2, v9

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$2;-><init>(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;Landroid/widget/LinearLayout;Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V

    check-cast v13, Landroid/view/View$OnClickListener;

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    invoke-virtual {v9, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 265
    :cond_0
    check-cast v9, Landroid/view/View;

    return-object v9
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    new-instance v0, Lcom/squareup/noho/NohoRow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 97
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;->getTitle()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 98
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_Button_Label:I

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;->getIcon()Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    new-instance v1, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    invoke-direct {v1, p2}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v1, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 100
    :cond_0
    check-cast v0, Landroid/view/View;

    .line 302
    new-instance p2, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$2;

    invoke-direct {p2, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x1

    .line 140
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 141
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->cartEntryViewsFactory:Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v2

    move-object v3, v0

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;->buildCartEntries(Lcom/squareup/protos/client/bills/Cart;Landroid/view/ViewGroup;)Lcom/squareup/ui/cart/CartEntryViews;

    move-result-object v8

    .line 146
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v4, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    const-string v1, "cart.line_items.itemization"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getLineItemClickedEvent()Ljava/lang/Object;

    move-result-object v6

    move-object v1, p0

    move-object v2, v8

    move-object v3, v0

    move-object v5, p3

    .line 145
    invoke-direct/range {v1 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addCartItemsTo(Lcom/squareup/ui/cart/CartEntryViews;Landroid/widget/LinearLayout;Ljava/util/List;Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 148
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getAddItemClickedEvent()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v7

    if-eqz v1, :cond_0

    .line 150
    new-instance v1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 151
    sget v2, Lcom/squareup/features/invoices/widgets/R$string;->add_line_item:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(com.sq\u2026s.R.string.add_line_item)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;->getAddItemClickedEvent()Ljava/lang/Object;

    move-result-object p1

    sget v3, Lcom/squareup/features/invoices/widgets/R$drawable;->add_row_icon:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 150
    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    .line 153
    invoke-virtual {p0, v1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    .line 149
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 156
    :cond_0
    invoke-virtual {v8}, Lcom/squareup/ui/cart/CartEntryViews;->getSubTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addIndentedTo(Lcom/squareup/ui/cart/CartEntryView;Landroid/widget/LinearLayout;)V

    .line 157
    :cond_1
    invoke-virtual {v8}, Lcom/squareup/ui/cart/CartEntryViews;->getDiscountViews()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addIndentedTo(Ljava/util/List;Landroid/widget/LinearLayout;)V

    .line 158
    invoke-virtual {v8}, Lcom/squareup/ui/cart/CartEntryViews;->getTaxViews()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addIndentedTo(Ljava/util/List;Landroid/widget/LinearLayout;)V

    .line 159
    invoke-virtual {v8}, Lcom/squareup/ui/cart/CartEntryViews;->getTipView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addIndentedTo(Lcom/squareup/ui/cart/CartEntryView;Landroid/widget/LinearLayout;)V

    .line 160
    :cond_2
    invoke-virtual {v8}, Lcom/squareup/ui/cart/CartEntryViews;->getTotalView()Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->addIndentedTo(Lcom/squareup/ui/cart/CartEntryView;Landroid/widget/LinearLayout;)V

    :cond_3
    const/4 p1, 0x0

    .line 166
    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p3

    if-ge p1, p3, :cond_6

    .line 167
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    const-string v1, "lineItemsContainer.getChildAt(childIndex)"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    sub-int/2addr v1, v7

    if-eq p1, v1, :cond_5

    instance-of p3, p3, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    if-nez p3, :cond_5

    .line 171
    invoke-virtual {v8}, Lcom/squareup/ui/cart/CartEntryViews;->getOrderItemViews()Ljava/util/List;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    add-int/2addr p3, v7

    const/4 v1, 0x2

    mul-int/lit8 p3, p3, 0x2

    const/4 v2, 0x0

    if-ge p1, p3, :cond_4

    .line 172
    new-instance p3, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    invoke-direct {p3, p2, v2, v1, v2}, Lcom/squareup/features/invoices/widgets/InvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 174
    :cond_4
    new-instance p3, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;

    invoke-direct {p3, p2, v2, v1, v2}, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p3, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    .line 176
    :goto_1
    check-cast p3, Landroid/view/View;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_5
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 181
    :cond_6
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->editPaymentRequestsViewFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;->getEditPaymentRequestsSectionData()Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    move-result-object v2

    .line 256
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$7;

    invoke-direct {v0, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$7;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Lkotlin/jvm/functions/Function1;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 257
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$8;

    invoke-direct {v0, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$8;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Lkotlin/jvm/functions/Function1;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 258
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$9;

    invoke-direct {v0, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$9;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;Lkotlin/jvm/functions/Function1;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v3, p2

    .line 255
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->create(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;Landroid/content/Context;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 0

    .line 48
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/noho/NohoRow;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string p2, "parent.context"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 73
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getTitle()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getValue()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 75
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getIcon()Lcom/squareup/noho/NohoRow$Icon;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 76
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 77
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getIconStyleId()Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setIconStyleId(I)V

    .line 80
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;->getEvent()Ljava/lang/Object;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/features/invoices/widgets/NoOp;

    if-eqz p2, :cond_1

    .line 81
    sget-object p1, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    goto :goto_0

    .line 83
    :cond_1
    sget-object p2, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 84
    move-object p2, v0

    check-cast p2, Landroid/view/View;

    .line 295
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$RowData;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :goto_0
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Landroid/content/Context;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    new-instance v0, Lcom/squareup/noho/NohoCheckableRow;

    .line 111
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    sget v2, Lcom/squareup/features/invoices/widgets/impl/R$style;->Theme_Invoices_SmallNohoRow:I

    invoke-direct {v1, p2, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v2, v1

    check-cast v2, Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    .line 110
    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 114
    sget-object p2, Lcom/squareup/noho/CheckType$SWITCH;->INSTANCE:Lcom/squareup/noho/CheckType$SWITCH;

    check-cast p2, Lcom/squareup/noho/CheckType;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setType(Lcom/squareup/noho/CheckType;)V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getLabel()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getDescription()Ljava/lang/CharSequence;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 117
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 118
    move-object p2, v0

    check-cast p2, Lcom/squareup/noho/NohoRow;

    invoke-static {p2}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRendererKt;->enableLinksInDescription(Lcom/squareup/noho/NohoRow;)V

    .line 120
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getChecked()Z

    move-result p2

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 121
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getEvent()Ljava/lang/Object;

    move-result-object p2

    sget-object v1, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setEnabled(Z)V

    .line 122
    new-instance p2, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;

    invoke-direct {p2, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$apply$lambda$1;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 125
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_ToggleRow_Label:I

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabelAppearanceId(I)V

    .line 128
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "context"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 129
    sget p2, Lcom/squareup/features/invoices/widgets/R$dimen;->v2_indented_row_margin_start:I

    .line 128
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 130
    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->getInternalPaddingTop()I

    move-result p2

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->getPaddingRight()I

    move-result p3

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->getInternalPaddingBottom()I

    move-result v1

    .line 127
    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/squareup/noho/NohoCheckableRow;->setPadding(IIII)V

    .line 113
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/squareup/features/invoices/widgets/RenderDelegate;->INSTANCE:Lcom/squareup/features/invoices/widgets/RenderDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate;->render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;Landroid/content/Context;)Landroid/webkit/WebView;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;Landroid/content/Context;)Landroid/webkit/WebView;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoRow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoRow;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditClicked"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->$$delegate_0:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoRow;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/noho/NohoRow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoRow;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    new-instance v0, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string p2, "parent.context"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 236
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getRepeatEveryString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 237
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getEndOnString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 238
    new-instance p2, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    sget v1, Lcom/squareup/features/invoices/widgets/R$drawable;->recurring_row_icon:I

    invoke-direct {p2, v1}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast p2, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 240
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getEvent()Ljava/lang/Object;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/features/invoices/widgets/NoOp;

    if-eqz p2, :cond_0

    .line 241
    sget-object p1, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    goto :goto_0

    .line 243
    :cond_0
    sget-object p2, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 244
    move-object p2, v0

    check-cast p2, Landroid/view/View;

    .line 321
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;

    invoke-direct {v1, p1, p3}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-object v0
.end method
