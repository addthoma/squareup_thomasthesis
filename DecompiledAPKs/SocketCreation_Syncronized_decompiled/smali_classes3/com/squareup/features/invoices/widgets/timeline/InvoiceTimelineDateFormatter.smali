.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;
.super Ljava/lang/Object;
.source "InvoiceTimelineDateFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B+\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;",
        "",
        "withYearFormat",
        "Ljava/text/DateFormat;",
        "withoutYearFormat",
        "clock",
        "Lcom/squareup/util/Clock;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;)V",
        "format",
        "",
        "date",
        "Lcom/squareup/protos/client/ISO8601Date;",
        "timeZone",
        "Ljava/util/TimeZone;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final res:Lcom/squareup/util/Res;

.field private final withYearFormat:Ljava/text/DateFormat;

.field private final withoutYearFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;)V
    .locals 1
    .param p1    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumFormNoYear;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "withYearFormat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "withoutYearFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->withYearFormat:Ljava/text/DateFormat;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->withoutYearFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->clock:Lcom/squareup/util/Clock;

    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final format(Lcom/squareup/protos/client/ISO8601Date;Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 5

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeZone"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 30
    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/squareup/util/Times;->Iso8601ToNormalizedDate(Ljava/lang/String;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object p1

    .line 32
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const-string v2, "target"

    .line 33
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v2, 0x1

    .line 36
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->withYearFormat:Ljava/text/DateFormat;

    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->withoutYearFormat:Ljava/text/DateFormat;

    .line 43
    :goto_0
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    .line 42
    invoke-static {p1, v3, v4, p2}, Lcom/squareup/util/Times;->getRelativeDate(Ljava/util/Date;JLjava/util/TimeZone;)Lcom/squareup/util/Times$RelativeDate;

    move-result-object p2

    if-nez p2, :cond_1

    goto :goto_1

    .line 47
    :cond_1
    sget-object v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/util/Times$RelativeDate;->ordinal()I

    move-result p2

    aget p2, v1, p2

    if-eq p2, v2, :cond_3

    const/4 v1, 0x2

    if-eq p2, v1, :cond_2

    .line 50
    :goto_1
    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "formatter.format(javaDate)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 49
    :cond_2
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->yesterday:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 48
    :cond_3
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->today:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    return-object p1
.end method
