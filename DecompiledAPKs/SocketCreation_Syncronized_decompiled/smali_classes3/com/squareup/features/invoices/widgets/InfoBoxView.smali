.class public final Lcom/squareup/features/invoices/widgets/InfoBoxView;
.super Landroid/widget/LinearLayout;
.source "InfoBoxView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R$\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InfoBoxView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "value",
        "",
        "message",
        "getMessage",
        "()Ljava/lang/CharSequence;",
        "setMessage",
        "(Ljava/lang/CharSequence;)V",
        "messageView",
        "Lcom/squareup/noho/NohoLabel;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final messageView:Lcom/squareup/noho/NohoLabel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 24
    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->setOrientation(I)V

    .line 25
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$layout;->info_box:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$drawable;->info_box_background:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$dimen;->noho_gap_20:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 30
    invoke-virtual {p0, p1, p1, p1, p1}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->setPadding(IIII)V

    .line 31
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/InfoBoxView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 33
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$id;->message:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InfoBoxView;->messageView:Lcom/squareup/noho/NohoLabel;

    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/CharSequence;
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InfoBoxView;->messageView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "messageView.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setMessage(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InfoBoxView;->messageView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
