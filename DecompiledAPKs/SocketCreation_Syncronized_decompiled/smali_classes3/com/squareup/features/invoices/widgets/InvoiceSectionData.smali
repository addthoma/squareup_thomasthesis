.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
.super Ljava/lang/Object;
.source "InvoiceSectionData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0008H\u00c6\u0003J7\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00082\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "",
        "header",
        "Lcom/squareup/features/invoices/widgets/Header;",
        "elements",
        "",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "showTopDivider",
        "",
        "showBottomDivider",
        "(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZ)V",
        "getElements",
        "()Ljava/util/List;",
        "getHeader",
        "()Lcom/squareup/features/invoices/widgets/Header;",
        "getShowBottomDivider",
        "()Z",
        "getShowTopDivider",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation
.end field

.field private final header:Lcom/squareup/features/invoices/widgets/Header;

.field private final showBottomDivider:Z

.field private final showTopDivider:Z


# direct methods
.method public constructor <init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/Header;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "header"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    iput-boolean p4, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 20
    sget-object p1, Lcom/squareup/features/invoices/widgets/Header$None;->INSTANCE:Lcom/squareup/features/invoices/widgets/Header$None;

    check-cast p1, Lcom/squareup/features/invoices/widgets/Header;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 23
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILjava/lang/Object;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->copy(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZ)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/features/invoices/widgets/Header;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    return v0
.end method

.method public final copy(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZ)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/Header;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;ZZ)",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;"
        }
    .end annotation

    const-string v0, "header"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    iget-boolean v1, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    iget-boolean p1, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    return-object v0
.end method

.method public final getHeader()Lcom/squareup/features/invoices/widgets/Header;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    return-object v0
.end method

.method public final getShowBottomDivider()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    return v0
.end method

.method public final getShowTopDivider()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InvoiceSectionData(header="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->header:Lcom/squareup/features/invoices/widgets/Header;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", elements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->elements:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showTopDivider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showTopDivider:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showBottomDivider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->showBottomDivider:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
