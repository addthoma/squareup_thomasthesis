.class public abstract Lcom/squareup/features/invoices/widgets/SectionElement;
.super Ljava/lang/Object;
.source "InvoiceSectionData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/SectionElement$RowData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;,
        Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;,
        Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;,
        Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;,
        Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;,
        Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0015\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0014\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0014\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&\'()*\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "",
        "()V",
        "AttachmentRows",
        "ButtonData",
        "CartEntryData",
        "ConfirmButtonData",
        "EditButtonRowData",
        "EditPaymentRequestRows",
        "EditTextData",
        "FloatingHeaderRowData",
        "HelperTextData",
        "InfoBoxData",
        "InvoiceDetailHeaderData",
        "MessageData",
        "PackageData",
        "PaymentRequestRows",
        "PreviewData",
        "RecurringRow",
        "RowData",
        "SubheaderData",
        "TimelineData",
        "ToggleData",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$FloatingHeaderRowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$SubheaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$MessageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InvoiceDetailHeaderData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PackageData;",
        "Lcom/squareup/features/invoices/widgets/SectionElement$InfoBoxData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/features/invoices/widgets/SectionElement;-><init>()V

    return-void
.end method
