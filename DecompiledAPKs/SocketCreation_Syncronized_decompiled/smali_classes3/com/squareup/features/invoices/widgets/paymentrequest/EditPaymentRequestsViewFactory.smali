.class public final Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;
.super Ljava/lang/Object;
.source "EditPaymentRequestsViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestsViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestsViewFactory.kt\ncom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,200:1\n1642#2,2:201\n1642#2,2:203\n1651#2,3:219\n704#2:229\n777#2,2:230\n1103#3,7:205\n1103#3,7:212\n1103#3,7:222\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestsViewFactory.kt\ncom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory\n*L\n46#1,2:201\n46#1,2:203\n140#1,3:219\n198#1:229\n198#1,2:230\n85#1,7:205\n111#1,7:212\n158#1,7:222\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J@\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nJ,\u0010\u000e\u001a\u00020\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J \u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J,\u0010\u0016\u001a\u00020\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J2\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00102\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J4\u0010\u0019\u001a\u00020\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u001a\u001a\u00020\u001b2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0002J\u0018\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010*\u0008\u0012\u0004\u0012\u00020\u00110\u0010H\u0002\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;",
        "",
        "()V",
        "create",
        "Landroid/view/View;",
        "sectionData",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;",
        "context",
        "Landroid/content/Context;",
        "onEditPaymentScheduleClicked",
        "Lkotlin/Function0;",
        "",
        "onDepositClicked",
        "onRemainderClicked",
        "depositRow",
        "requests",
        "",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
        "installmentRow",
        "request",
        "index",
        "",
        "installmentsBalanceRow",
        "onClick",
        "installmentsSection",
        "remainderRow",
        "remainderClickable",
        "",
        "installments",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final depositRow(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .line 85
    new-instance v6, Lcom/squareup/noho/NohoRow;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 87
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    .line 88
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getPercent()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    .line 89
    sget v3, Lcom/squareup/features/invoices/widgets/impl/R$string;->deposit_percent:I

    invoke-static {p3, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 90
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "percent"

    invoke-virtual {v3, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$string;->deposit:I

    invoke-virtual {p3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 93
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_Button_Label:I

    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 94
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getFormattedDueDate()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 95
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 96
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_PaymentSchedule_Value:I

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 97
    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    sget v1, Lcom/squareup/features/invoices/widgets/R$drawable;->due_row_icon:I

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 98
    check-cast v6, Landroid/view/View;

    .line 205
    new-instance v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$depositRow$$inlined$apply$lambda$1;

    invoke-direct {v0, p1, p3, p2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$depositRow$$inlined$apply$lambda$1;-><init>(Ljava/util/List;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v6
.end method

.method private final installmentRow(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;ILandroid/content/Context;)Landroid/view/View;
    .locals 7

    .line 178
    new-instance v6, Lcom/squareup/noho/NohoRow;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$style;->Theme_Invoices_SmallNohoRow:I

    invoke-direct {v0, p3, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$string;->payment_number:I

    invoke-static {p3, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    const-string v2, "number"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 182
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_PaymentSchedule_InstallmentLabel:I

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 183
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getFormattedDueDate()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 184
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 185
    sget p1, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_PaymentSchedule_Value:I

    invoke-virtual {v6, p1}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    .line 188
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 189
    sget p3, Lcom/squareup/features/invoices/widgets/R$dimen;->v2_indented_row_margin_start:I

    .line 188
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    if-nez p2, :cond_1

    .line 190
    invoke-virtual {v6}, Lcom/squareup/noho/NohoRow;->getInternalPaddingTop()I

    move-result p2

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    .line 191
    :goto_1
    invoke-virtual {v6}, Lcom/squareup/noho/NohoRow;->getPaddingRight()I

    move-result p3

    .line 192
    invoke-virtual {v6}, Lcom/squareup/noho/NohoRow;->getInternalPaddingBottom()I

    move-result v0

    .line 187
    invoke-virtual {v6, p1, p2, p3, v0}, Lcom/squareup/noho/NohoRow;->setPadding(IIII)V

    .line 178
    check-cast v6, Landroid/view/View;

    return-object v6
.end method

.method private final installments(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;"
        }
    .end annotation

    .line 198
    check-cast p1, Ljava/lang/Iterable;

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 230
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    .line 198
    invoke-virtual {v2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getType()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    move-result-object v3

    sget-object v4, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq v3, v4, :cond_2

    invoke-virtual {v2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getType()Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_INSTALLMENT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 231
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final installmentsBalanceRow(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .line 158
    new-instance v6, Lcom/squareup/noho/NohoRow;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 159
    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->installments(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 160
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$string;->balance:I

    invoke-virtual {p3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 161
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_Button_Label:I

    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 162
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$string;->payment_request_due_over:I

    invoke-static {p3, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 163
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v2, "number"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 165
    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    sget v1, Lcom/squareup/features/invoices/widgets/R$drawable;->due_row_icon:I

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 166
    check-cast v6, Landroid/view/View;

    .line 222
    new-instance v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$installmentsBalanceRow$$inlined$apply$lambda$1;

    invoke-direct {v0, p0, p1, p3, p2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$installmentsBalanceRow$$inlined$apply$lambda$1;-><init>(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;Ljava/util/List;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v6
.end method

.method private final installmentsSection(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 141
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->installmentsBalanceRow(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    new-instance p2, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p2, p3, v1, v2, v1}, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-direct {p0, p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->installments(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 220
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, p2, 0x1

    if-gez p2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    .line 145
    invoke-direct {p0, v1, p2, p3}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->installmentRow(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;ILandroid/content/Context;)Landroid/view/View;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move p2, v2

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private final remainderRow(Ljava/util/List;Landroid/content/Context;ZLkotlin/jvm/functions/Function0;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;",
            "Landroid/content/Context;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 111
    new-instance v6, Lcom/squareup/noho/NohoRow;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v0, 0x1

    .line 113
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    .line 114
    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$string;->balance:I

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getFormattedDueDate()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 117
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_PaymentSchedule_Value:I

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setValueAppearanceId(I)V

    if-eqz p3, :cond_0

    .line 120
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$style;->TextAppearance_Invoices_V2_Button_Label:I

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoRow;->setLabelAppearanceId(I)V

    .line 121
    move-object v0, v6

    check-cast v0, Landroid/view/View;

    .line 212
    new-instance v1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$remainderRow$$inlined$apply$lambda$1;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$remainderRow$$inlined$apply$lambda$1;-><init>(Ljava/util/List;Landroid/content/Context;ZLkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 126
    sget p2, Lcom/squareup/features/invoices/widgets/R$dimen;->v2_indented_row_margin_start:I

    .line 125
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 127
    invoke-virtual {v6}, Lcom/squareup/noho/NohoRow;->getInternalPaddingTop()I

    move-result p2

    invoke-virtual {v6}, Lcom/squareup/noho/NohoRow;->getPaddingRight()I

    move-result p3

    invoke-virtual {v6}, Lcom/squareup/noho/NohoRow;->getInternalPaddingBottom()I

    move-result p4

    .line 124
    invoke-virtual {v6, p1, p2, p3, p4}, Lcom/squareup/noho/NohoRow;->setPadding(IIII)V

    .line 111
    check-cast v6, Landroid/view/View;

    return-object v6
.end method


# virtual methods
.method public final create(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;Landroid/content/Context;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "sectionData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEditPaymentScheduleClicked"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDepositClicked"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onRemainderClicked"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->getRequests()Ljava/util/List;

    move-result-object v0

    .line 46
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    .line 47
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 48
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->getConfig()Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v3

    sget-object v4, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eq v3, v2, :cond_4

    if-eq v3, v4, :cond_3

    const/4 p1, 0x3

    if-eq v3, p1, :cond_2

    const/4 p1, 0x4

    if-eq v3, p1, :cond_1

    const/4 p1, 0x5

    if-eq v3, p1, :cond_0

    goto/16 :goto_2

    .line 72
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Invalid payment request configuration."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 71
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Showing a single payment request is not implemented."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 69
    :cond_2
    invoke-direct {p0, v0, p3, p2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->installmentsSection(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 203
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 69
    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 64
    :cond_3
    invoke-direct {p0, v0, p3, p2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->depositRow(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 65
    new-instance p1, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    invoke-direct {p1, p2, v5, v4, v5}, Lcom/squareup/features/invoices/widgets/InvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 66
    invoke-direct {p0, v0, p3, p2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->installmentsSection(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 201
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 66
    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 55
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->getUseDepositRemainderClickListeners()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object p3, p4

    .line 53
    :cond_5
    invoke-direct {p0, v0, p3, p2}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->depositRow(Ljava/util/List;Lkotlin/jvm/functions/Function0;Landroid/content/Context;)Landroid/view/View;

    move-result-object p3

    .line 52
    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 60
    new-instance p3, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;

    invoke-direct {p3, p2, v5, v4, v5}, Lcom/squareup/features/invoices/widgets/IndentedInvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p3, Landroid/view/View;

    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 61
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->getRemainderClickable()Z

    move-result p1

    invoke-direct {p0, v0, p2, p1, p5}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;->remainderRow(Ljava/util/List;Landroid/content/Context;ZLkotlin/jvm/functions/Function0;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 46
    :cond_6
    :goto_2
    check-cast v1, Landroid/view/View;

    return-object v1
.end method
