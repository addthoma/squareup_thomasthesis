.class public final Lcom/squareup/noho/NightModeUtilsKt;
.super Ljava/lang/Object;
.source "NightModeUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNightModeUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NightModeUtils.kt\ncom/squareup/noho/NightModeUtilsKt\n*L\n1#1,29:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u001a\n\u0010\u0004\u001a\u00020\u0005*\u00020\u0005\u001a\n\u0010\u0006\u001a\u00020\u0005*\u00020\u0005\u00a8\u0006\u0007"
    }
    d2 = {
        "inflateViewInNightMode",
        "Landroid/view/View;",
        "layoutId",
        "",
        "nightMode",
        "Landroid/content/Context;",
        "nohoNightMode",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final inflateViewInNightMode(Landroid/view/View;I)Landroid/view/View;
    .locals 1

    const-string v0, "$this$inflateViewInNightMode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/noho/NightModeUtilsKt;->nohoNightMode(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p0

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    const-string p1, "View.inflate(context.noh\u2026htMode(), layoutId, null)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final nightMode(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    const-string v0, "$this$nightMode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    const/16 v1, 0x20

    iput v1, v0, Landroid/content/res/Configuration;->uiMode:I

    .line 12
    invoke-virtual {p0, v0}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object p0

    const-string v0, "createConfigurationConte\u2026= UI_MODE_NIGHT_YES }\n  )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final nohoNightMode(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    const-string v0, "$this$nohoNightMode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/squareup/noho/R$style;->Theme_Noho:I

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 21
    new-instance p0, Landroid/content/res/Configuration;

    invoke-direct {p0}, Landroid/content/res/Configuration;-><init>()V

    const/16 v1, 0x20

    iput v1, p0, Landroid/content/res/Configuration;->uiMode:I

    .line 20
    invoke-virtual {v0, p0}, Landroid/view/ContextThemeWrapper;->applyOverrideConfiguration(Landroid/content/res/Configuration;)V

    .line 19
    check-cast v0, Landroid/content/Context;

    return-object v0
.end method
