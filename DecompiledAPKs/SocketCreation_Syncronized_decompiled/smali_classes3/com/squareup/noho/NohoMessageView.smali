.class public final Lcom/squareup/noho/NohoMessageView;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "NohoMessageView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoMessageView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoMessageView.kt\ncom/squareup/noho/NohoMessageView\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,232:1\n1261#2:233\n*E\n*S KotlinDebug\n*F\n+ 1 NohoMessageView.kt\ncom/squareup/noho/NohoMessageView\n*L\n56#1:233\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001b\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0002J\u0008\u0010\u0015\u001a\u00020\u0014H\u0002J\u0006\u0010\u0016\u001a\u00020\u0014J\u0006\u0010\u0017\u001a\u00020\u0014J\u0006\u0010\u0018\u001a\u00020\u0014J\u0006\u0010\u0019\u001a\u00020\u0014J\u0010\u0010\u001a\u001a\u00020\u00142\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cJ\u0010\u0010\u001a\u001a\u00020\u00142\u0008\u0008\u0001\u0010\u001d\u001a\u00020\u001eJ\u0010\u0010\u001f\u001a\u00020\u00142\u0008\u0008\u0001\u0010 \u001a\u00020\u001eJ\u0010\u0010!\u001a\u00020\u00142\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u0010!\u001a\u00020\u00142\u0008\u0008\u0001\u0010$\u001a\u00020\u001eJ\u0010\u0010%\u001a\u00020\u00142\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u0010%\u001a\u00020\u00142\u0008\u0008\u0001\u0010$\u001a\u00020\u001eJ\u000e\u0010&\u001a\u00020\u00142\u0006\u0010\'\u001a\u00020(J\u0010\u0010)\u001a\u00020\u00142\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u0010)\u001a\u00020\u00142\u0008\u0008\u0001\u0010$\u001a\u00020\u001eJ\u000e\u0010*\u001a\u00020\u00142\u0006\u0010+\u001a\u00020\u001eJ\u000e\u0010,\u001a\u00020\u00142\u0006\u0010\'\u001a\u00020(J\u0010\u0010-\u001a\u00020\u00142\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u0010-\u001a\u00020\u00142\u0008\u0008\u0001\u0010$\u001a\u00020\u001eJ\u000e\u0010.\u001a\u00020\u00142\u0006\u0010\'\u001a\u00020(J\u0010\u0010/\u001a\u00020\u00142\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u0010/\u001a\u00020\u00142\u0008\u0008\u0001\u0010$\u001a\u00020\u001eJ\u000e\u00100\u001a\u00020\u00142\u0006\u00101\u001a\u000202J\u0010\u00103\u001a\u00020\u00142\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0010\u00103\u001a\u00020\u00142\u0008\u0008\u0001\u0010$\u001a\u00020\u001eJ\u0010\u00104\u001a\u00020\u00142\u0008\u0008\u0001\u00105\u001a\u00020\u001eJ\u0006\u00106\u001a\u00020\u0014J\u0006\u00107\u001a\u00020\u0014J\u0006\u00108\u001a\u00020\u0014R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/noho/NohoMessageView;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "help",
        "Landroid/widget/TextView;",
        "image",
        "Landroid/widget/ImageView;",
        "linkText",
        "message",
        "Lcom/squareup/widgets/MessageView;",
        "primaryButton",
        "Landroid/widget/Button;",
        "secondaryButton",
        "Lcom/squareup/noho/NohoButton;",
        "title",
        "applyAttributes",
        "",
        "bindViews",
        "clearDrawable",
        "hideLinkText",
        "hidePrimaryButton",
        "hideSecondaryButton",
        "setDrawable",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "drawableRes",
        "",
        "setDrawableTint",
        "color",
        "setHelp",
        "text",
        "",
        "res",
        "setLinkText",
        "setLinkTextOnClickListener",
        "onClickListener",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "setMessage",
        "setMessageTextColor",
        "textColor",
        "setPrimaryButtonOnClickListener",
        "setPrimaryButtonText",
        "setSecondaryButtonOnClickListener",
        "setSecondaryButtonText",
        "setSecondaryButtonType",
        "nohoButtonType",
        "Lcom/squareup/noho/NohoButtonType;",
        "setTitle",
        "setTitleTextAppearance",
        "textAppearance",
        "showLinkText",
        "showPrimaryButton",
        "showSecondaryButton",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private help:Landroid/widget/TextView;

.field private image:Landroid/widget/ImageView;

.field private linkText:Landroid/widget/TextView;

.field private message:Lcom/squareup/widgets/MessageView;

.field private primaryButton:Landroid/widget/Button;

.field private secondaryButton:Lcom/squareup/noho/NohoButton;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/noho/NohoMessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    .line 56
    sget v0, Lcom/squareup/noho/R$attr;->nohoMessageViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$layout;->noho_message_view:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoMessageView;->setOrientation(I)V

    const/16 p1, 0x11

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoMessageView;->setGravity(I)V

    .line 70
    invoke-direct {p0}, Lcom/squareup/noho/NohoMessageView;->bindViews()V

    .line 71
    invoke-direct {p0, p2}, Lcom/squareup/noho/NohoMessageView;->applyAttributes(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 55
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoMessageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final applyAttributes(Landroid/util/AttributeSet;)V
    .locals 3

    .line 216
    invoke-virtual {p0}, Lcom/squareup/noho/NohoMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoMessageView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 218
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_sqDrawable:I

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 219
    move-object v2, v0

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    if-eq v2, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 220
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoMessageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 221
    iget-object v1, p0, Lcom/squareup/noho/NohoMessageView;->image:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    const-string v2, "image"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 222
    :cond_3
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_android_title:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 223
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_sqMessageText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 224
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_sqPrimaryButtonText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 225
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_sqSecondaryButtonText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    .line 226
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_sqLinkText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setLinkText(Ljava/lang/CharSequence;)V

    .line 227
    sget v0, Lcom/squareup/noho/R$styleable;->NohoMessageView_sqHelpText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setHelp(Ljava/lang/CharSequence;)V

    .line 229
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 206
    sget v0, Lcom/squareup/noho/R$id;->noho_message_image:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->image:Landroid/widget/ImageView;

    .line 207
    sget v0, Lcom/squareup/noho/R$id;->noho_message_title:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->title:Landroid/widget/TextView;

    .line 208
    sget v0, Lcom/squareup/noho/R$id;->noho_message_message:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->message:Lcom/squareup/widgets/MessageView;

    .line 209
    sget v0, Lcom/squareup/noho/R$id;->noho_message_primary_button:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_primary_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->primaryButton:Landroid/widget/Button;

    .line 210
    sget v0, Lcom/squareup/noho/R$id;->noho_message_secondary_button:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_secondary_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    .line 211
    sget v0, Lcom/squareup/noho/R$id;->noho_message_link_text:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_link_text)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->linkText:Landroid/widget/TextView;

    .line 212
    sget v0, Lcom/squareup/noho/R$id;->noho_message_help:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.noho_message_help)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/noho/NohoMessageView;->help:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final clearDrawable()V
    .locals 1

    const/4 v0, 0x0

    .line 89
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMessageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final hideLinkText()V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->linkText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "linkText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final hidePrimaryButton()V
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->primaryButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "primaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public final hideSecondaryButton()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "secondaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    return-void
.end method

.method public final setDrawable(I)V
    .locals 2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/squareup/noho/NohoMessageView;->clearDrawable()V

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->image:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const-string v1, "image"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void
.end method

.method public final setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->image:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "image"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final setDrawableTint(I)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->image:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "image"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public final setHelp(I)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->help:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "help"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    return-void
.end method

.method public final setHelp(Ljava/lang/CharSequence;)V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->help:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "help"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setLinkText(I)V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->linkText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "linkText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    return-void
.end method

.method public final setLinkText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->linkText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "linkText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setLinkTextOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 2

    const-string v0, "onClickListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->linkText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "linkText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setMessage(I)V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->message:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "message"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(I)V

    return-void
.end method

.method public final setMessage(Ljava/lang/CharSequence;)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->message:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "message"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setMessageTextColor(I)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->message:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "message"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    return-void
.end method

.method public final setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 2

    const-string v0, "onClickListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->primaryButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "primaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setPrimaryButtonText(I)V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->primaryButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "primaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    return-void
.end method

.method public final setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->primaryButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "primaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 2

    const-string v0, "onClickListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "secondaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setSecondaryButtonText(I)V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "secondaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    return-void
.end method

.method public final setSecondaryButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "secondaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setSecondaryButtonType(Lcom/squareup/noho/NohoButtonType;)V
    .locals 2

    const-string v0, "nohoButtonType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "secondaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    return-void
.end method

.method public final setTitle(I)V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->title:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->title:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setTitleTextAppearance(I)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->title:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    return-void
.end method

.method public final showLinkText()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->linkText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "linkText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final showPrimaryButton()V
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->primaryButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "primaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public final showSecondaryButton()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageView;->secondaryButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "secondaryButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    return-void
.end method
