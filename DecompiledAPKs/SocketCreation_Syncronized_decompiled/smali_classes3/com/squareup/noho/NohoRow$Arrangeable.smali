.class public interface abstract Lcom/squareup/noho/NohoRow$Arrangeable;
.super Ljava/lang/Object;
.source "NohoRow.kt"

# interfaces
.implements Lcom/squareup/noho/NohoRow$ArrangeableProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "Arrangeable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoRow$Arrangeable$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0008d\u0018\u00002\u00020\u0001J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0008R\u0012\u0010\t\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0005\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$Arrangeable;",
        "Lcom/squareup/noho/NohoRow$ArrangeableProvider;",
        "id",
        "",
        "getId",
        "()I",
        "isEnabled",
        "",
        "()Z",
        "margin",
        "getMargin",
        "get",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;
.end method

.method public abstract getId()I
.end method

.method public abstract getMargin()I
.end method

.method public abstract isEnabled()Z
.end method
