.class public final enum Lcom/squareup/noho/UpIcon;
.super Ljava/lang/Enum;
.source "NohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/UpIcon;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/noho/UpIcon;",
        "",
        "iconRes",
        "",
        "tooltipRes",
        "(Ljava/lang/String;III)V",
        "getIconRes",
        "()I",
        "getTooltipRes",
        "BACK_ARROW",
        "X",
        "BURGER",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/UpIcon;

.field public static final enum BACK_ARROW:Lcom/squareup/noho/UpIcon;

.field public static final enum BURGER:Lcom/squareup/noho/UpIcon;

.field public static final enum X:Lcom/squareup/noho/UpIcon;


# instance fields
.field private final iconRes:I

.field private final tooltipRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/noho/UpIcon;

    new-instance v1, Lcom/squareup/noho/UpIcon;

    .line 361
    sget v2, Lcom/squareup/noho/R$drawable;->noho_actionbar_icon_back_arrow:I

    .line 362
    sget v3, Lcom/squareup/containerconstants/R$string;->content_description_back:I

    const/4 v4, 0x0

    const-string v5, "BACK_ARROW"

    .line 360
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/UpIcon;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/UpIcon;

    .line 365
    sget v2, Lcom/squareup/noho/R$drawable;->noho_actionbar_icon_x:I

    .line 366
    sget v3, Lcom/squareup/containerconstants/R$string;->content_description_close:I

    const/4 v4, 0x1

    const-string v5, "X"

    .line 364
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/UpIcon;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/UpIcon;

    .line 369
    sget v2, Lcom/squareup/noho/R$drawable;->noho_actionbar_icon_burger:I

    .line 370
    sget v3, Lcom/squareup/containerconstants/R$string;->content_description_drawer:I

    const/4 v4, 0x2

    const-string v5, "BURGER"

    .line 368
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/noho/UpIcon;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/squareup/noho/UpIcon;->BURGER:Lcom/squareup/noho/UpIcon;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/noho/UpIcon;->$VALUES:[Lcom/squareup/noho/UpIcon;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 356
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/noho/UpIcon;->iconRes:I

    iput p4, p0, Lcom/squareup/noho/UpIcon;->tooltipRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/UpIcon;
    .locals 1

    const-class v0, Lcom/squareup/noho/UpIcon;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/UpIcon;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/UpIcon;
    .locals 1

    sget-object v0, Lcom/squareup/noho/UpIcon;->$VALUES:[Lcom/squareup/noho/UpIcon;

    invoke-virtual {v0}, [Lcom/squareup/noho/UpIcon;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/UpIcon;

    return-object v0
.end method


# virtual methods
.method public final getIconRes()I
    .locals 1

    .line 357
    iget v0, p0, Lcom/squareup/noho/UpIcon;->iconRes:I

    return v0
.end method

.method public final getTooltipRes()I
    .locals 1

    .line 358
    iget v0, p0, Lcom/squareup/noho/UpIcon;->tooltipRes:I

    return v0
.end method
