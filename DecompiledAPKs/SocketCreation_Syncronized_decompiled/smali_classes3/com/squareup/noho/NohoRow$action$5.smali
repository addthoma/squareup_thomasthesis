.class final synthetic Lcom/squareup/noho/NohoRow$action$5;
.super Lkotlin/jvm/internal/MutablePropertyReference0;
.source "NohoRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoRow;)V
    .locals 0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/MutablePropertyReference0;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoRow$action$5;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/noho/NohoRow;

    .line 232
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getAction()Lcom/squareup/noho/NohoRow$Action;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "action"

    return-object v0
.end method

.method public getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/noho/NohoRow;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "getAction()Lcom/squareup/noho/NohoRow$Action;"

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoRow$action$5;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/noho/NohoRow;

    .line 232
    check-cast p1, Lcom/squareup/noho/NohoRow$Action;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setAction(Lcom/squareup/noho/NohoRow$Action;)V

    return-void
.end method
