.class final Lcom/squareup/noho/BadgeParceler$write$snapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NohoActionBar.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/BadgeParceler;->write(Lcom/squareup/noho/Badge;Landroid/os/Parcel;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_write:Lcom/squareup/noho/Badge;


# direct methods
.method constructor <init>(Lcom/squareup/noho/Badge;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/BadgeParceler$write$snapshot$1;->$this_write:Lcom/squareup/noho/Badge;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 391
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/BadgeParceler$write$snapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 410
    iget-object v0, p0, Lcom/squareup/noho/BadgeParceler$write$snapshot$1;->$this_write:Lcom/squareup/noho/Badge;

    invoke-virtual {v0}, Lcom/squareup/noho/Badge;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 411
    iget-object v0, p0, Lcom/squareup/noho/BadgeParceler$write$snapshot$1;->$this_write:Lcom/squareup/noho/Badge;

    invoke-virtual {v0}, Lcom/squareup/noho/Badge;->getType()Lcom/squareup/noho/Badge$Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    return-void
.end method
