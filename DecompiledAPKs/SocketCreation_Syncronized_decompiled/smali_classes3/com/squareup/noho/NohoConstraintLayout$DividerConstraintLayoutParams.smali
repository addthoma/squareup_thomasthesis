.class public Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;
.super Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;
.source "NohoConstraintLayout.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoConstraintLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DividerConstraintLayoutParams"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\u0008\u0016\u0018\u00002\u00020\u0001B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u0017\u0008\u0016\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nB\u001f\u0008\u0016\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\u000cB\'\u0008\u0016\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\u000b\u001a\u00020\u0008\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fB\u000f\u0008\u0016\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012R \u0010\u0013\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001a\u0010\r\u001a\u00020\u000eX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001d\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;",
        "Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "width",
        "",
        "height",
        "(II)V",
        "edgesToPaint",
        "(III)V",
        "insetEdges",
        "",
        "(IIIZ)V",
        "source",
        "Landroid/view/ViewGroup$LayoutParams;",
        "(Landroid/view/ViewGroup$LayoutParams;)V",
        "edges",
        "edges$annotations",
        "()V",
        "getEdges",
        "()I",
        "setEdges",
        "(I)V",
        "getInsetEdges$noho_release",
        "()Z",
        "setInsetEdges$noho_release",
        "(Z)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private edges:I

.field private insetEdges:Z


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 108
    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    const/4 p1, 0x0

    .line 109
    iput p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    const/4 p1, 0x1

    .line 110
    iput-boolean p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 117
    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    .line 118
    iput p3, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    const/4 p1, 0x1

    .line 119
    iput-boolean p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0

    .line 127
    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    .line 128
    iput p3, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    .line 129
    iput-boolean p4, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 100
    sget p2, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout_layout_edges:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    .line 101
    sget p2, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout_layout_insetEdges:I

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    .line 102
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-direct {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p1, 0x0

    .line 133
    iput p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    const/4 p1, 0x1

    .line 134
    iput-boolean p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    return-void
.end method

.method public static synthetic edges$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getEdges()I
    .locals 1

    .line 90
    iget v0, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    return v0
.end method

.method public final getInsetEdges$noho_release()Z
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    return v0
.end method

.method public final setEdges(I)V
    .locals 0

    .line 90
    iput p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->edges:I

    return-void
.end method

.method public final setInsetEdges$noho_release(Z)V
    .locals 0

    .line 93
    iput-boolean p1, p0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->insetEdges:Z

    return-void
.end method
