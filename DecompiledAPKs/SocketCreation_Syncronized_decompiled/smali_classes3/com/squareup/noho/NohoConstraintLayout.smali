.class public Lcom/squareup/noho/NohoConstraintLayout;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoConstraintLayout.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\u001aB%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u000c\u0010\r\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/noho/NohoConstraintLayout;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "edgePainter",
        "Lcom/squareup/noho/EdgePainter;",
        "selfEdges",
        "selfEdges$annotations",
        "()V",
        "checkLayoutParams",
        "",
        "params",
        "Landroid/view/ViewGroup$LayoutParams;",
        "dispatchDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "generateDefaultLayoutParams",
        "Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;",
        "generateLayoutParams",
        "onDraw",
        "DividerConstraintLayoutParams",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edgePainter:Lcom/squareup/noho/EdgePainter;

.field private final selfEdges:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    .line 28
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoConstraintLayout;->setWillNotDraw(Z)V

    .line 31
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout:[I

    .line 32
    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_ConstraintLayout:I

    .line 30
    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 36
    sget p2, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout_sqEdgeWidth:I

    .line 37
    invoke-virtual {p0}, Lcom/squareup/noho/NohoConstraintLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v1, Lcom/squareup/noho/R$dimen;->noho_divider_hairline:I

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 35
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 41
    sget p3, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout_sqEdgeColor:I

    .line 42
    invoke-virtual {p0}, Lcom/squareup/noho/NohoConstraintLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$color;->noho_divider_hairline:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 40
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    .line 45
    new-instance v1, Lcom/squareup/noho/EdgePainter;

    move-object v2, p0

    check-cast v2, Landroid/view/View;

    invoke-direct {v1, v2, p2, p3}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v1, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    .line 47
    sget p2, Lcom/squareup/noho/R$styleable;->NohoConstraintLayout_layout_edges:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/noho/NohoConstraintLayout;->selfEdges:I

    .line 48
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 20
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 21
    sget p3, Lcom/squareup/noho/R$attr;->nohoConstraintLayoutStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private static synthetic selfEdges$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    instance-of p1, p1, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    return p1
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/noho/NohoConstraintLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    .line 66
    invoke-virtual {p0, v2}, Lcom/squareup/noho/NohoConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "childView"

    .line 67
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_2

    .line 70
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-eqz v4, :cond_2

    check-cast v4, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    .line 71
    iget-object v5, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->getEdges()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 73
    invoke-virtual {v4}, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->getInsetEdges$noho_release()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 74
    iget-object v4, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/squareup/noho/EdgePainter;->setHorizontalInsets(II)V

    goto :goto_1

    .line 76
    :cond_1
    iget-object v4, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v4, v1, v1}, Lcom/squareup/noho/EdgePainter;->setHorizontalInsets(II)V

    .line 78
    :goto_1
    iget-object v4, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v4, p1, v3}, Lcom/squareup/noho/EdgePainter;->drawChildEdges(Landroid/graphics/Canvas;Landroid/view/View;)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.noho.NohoConstraintLayout.DividerConstraintLayoutParams"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    return-void
.end method

.method public bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/noho/NohoConstraintLayout;->generateDefaultLayoutParams()Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public bridge synthetic generateDefaultLayoutParams()Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/noho/NohoConstraintLayout;->generateDefaultLayoutParams()Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;
    .locals 3

    .line 55
    new-instance v0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoConstraintLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    return-object p1
.end method

.method public bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoConstraintLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    return-object p1
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoConstraintLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    return-object p1
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;
    .locals 3

    const-string v0, "attrs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;
    .locals 1

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    invoke-direct {v0, p1}, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    iget v1, p0, Lcom/squareup/noho/NohoConstraintLayout;->selfEdges:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/noho/NohoConstraintLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0}, Lcom/squareup/noho/EdgePainter;->clearEdges()V

    .line 86
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method
