.class public Lcom/squareup/noho/NohoListView;
.super Landroid/widget/ListView;
.source "NohoListView.java"


# instance fields
.field private final paddingDelegate:Lcom/squareup/noho/NohoPaddingDelegate;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 18
    sget v0, Lcom/squareup/noho/R$attr;->nohoListViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoListView:[I

    sget v1, Lcom/squareup/noho/R$style;->Widget_Noho_ListView:I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 25
    sget p2, Lcom/squareup/noho/R$styleable;->NohoListView_sqContentPaddingType:I

    invoke-static {p1, p2}, Lcom/squareup/noho/NohoPaddingDelegate;->getEnum(Landroid/content/res/TypedArray;I)Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    move-result-object p2

    .line 26
    sget p3, Lcom/squareup/noho/R$styleable;->NohoListView_sqIsAlert:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    .line 27
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 28
    new-instance p1, Lcom/squareup/noho/NohoPaddingDelegate;

    invoke-direct {p1, p0}, Lcom/squareup/noho/NohoPaddingDelegate;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/squareup/noho/NohoListView;->paddingDelegate:Lcom/squareup/noho/NohoPaddingDelegate;

    .line 29
    invoke-virtual {p0, p2, p3}, Lcom/squareup/noho/NohoListView;->setContentPadding(Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;Z)V

    return-void
.end method


# virtual methods
.method public setContentPadding(Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;Z)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/noho/NohoListView;->paddingDelegate:Lcom/squareup/noho/NohoPaddingDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/noho/NohoPaddingDelegate;->setContentPadding(Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;Z)V

    return-void
.end method
