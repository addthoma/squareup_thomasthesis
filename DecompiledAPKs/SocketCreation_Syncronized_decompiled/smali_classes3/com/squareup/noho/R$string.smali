.class public final Lcom/squareup/noho/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final noho_editrow_plugin_clear:I = 0x7f121094

.field public static final noho_editrow_plugin_view_password:I = 0x7f121095

.field public static final noho_standard_black:I = 0x7f121096

.field public static final noho_standard_blue:I = 0x7f121097

.field public static final noho_standard_brown:I = 0x7f121098

.field public static final noho_standard_cyan:I = 0x7f121099

.field public static final noho_standard_cyan_dark:I = 0x7f12109a

.field public static final noho_standard_gold:I = 0x7f12109b

.field public static final noho_standard_green:I = 0x7f12109c

.field public static final noho_standard_indigo:I = 0x7f12109d

.field public static final noho_standard_lilac:I = 0x7f12109e

.field public static final noho_standard_lime:I = 0x7f12109f

.field public static final noho_standard_maroon:I = 0x7f1210a0

.field public static final noho_standard_orange:I = 0x7f1210a1

.field public static final noho_standard_pink:I = 0x7f1210a2

.field public static final noho_standard_plum:I = 0x7f1210a3

.field public static final noho_standard_purple:I = 0x7f1210a4

.field public static final noho_standard_teal:I = 0x7f1210a5

.field public static final noho_standard_white:I = 0x7f1210a6

.field public static final noho_time_picker_hour_minute_separator:I = 0x7f1210a7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
