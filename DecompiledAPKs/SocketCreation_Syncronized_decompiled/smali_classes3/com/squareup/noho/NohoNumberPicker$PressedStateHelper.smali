.class Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;
.super Ljava/lang/Object;
.source "NohoNumberPicker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoNumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PressedStateHelper"
.end annotation


# static fields
.field public static final BUTTON_DECREMENT:I = 0x2

.field public static final BUTTON_INCREMENT:I = 0x1


# instance fields
.field private final MODE_PRESS:I

.field private final MODE_TAPPED:I

.field private managedButton:I

.field private mode:I

.field final synthetic this$0:Lcom/squareup/noho/NohoNumberPicker;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoNumberPicker;)V
    .locals 0

    .line 1106
    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x1

    .line 1110
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->MODE_PRESS:I

    const/4 p1, 0x2

    .line 1111
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->MODE_TAPPED:I

    return-void
.end method


# virtual methods
.method public buttonPressDelayed(I)V
    .locals 2

    .line 1131
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->cancel()V

    const/4 v0, 0x1

    .line 1132
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->mode:I

    .line 1133
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->managedButton:I

    .line 1134
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, p0, v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public buttonTapped(I)V
    .locals 1

    .line 1138
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->cancel()V

    const/4 v0, 0x2

    .line 1139
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->mode:I

    .line 1140
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->managedButton:I

    .line 1141
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {p1, p0}, Lcom/squareup/noho/NohoNumberPicker;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public cancel()V
    .locals 5

    const/4 v0, 0x0

    .line 1117
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->mode:I

    .line 1118
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->managedButton:I

    .line 1119
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1, p0}, Lcom/squareup/noho/NohoNumberPicker;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1120
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v1}, Lcom/squareup/noho/NohoNumberPicker;->access$000(Lcom/squareup/noho/NohoNumberPicker;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1121
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->access$002(Lcom/squareup/noho/NohoNumberPicker;Z)Z

    .line 1122
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v1}, Lcom/squareup/noho/NohoNumberPicker;->access$100(Lcom/squareup/noho/NohoNumberPicker;)I

    move-result v2

    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoNumberPicker;->getBottom()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/squareup/noho/NohoNumberPicker;->invalidate(IIII)V

    .line 1124
    :cond_0
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->access$202(Lcom/squareup/noho/NohoNumberPicker;Z)Z

    .line 1125
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v1}, Lcom/squareup/noho/NohoNumberPicker;->access$200(Lcom/squareup/noho/NohoNumberPicker;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1126
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v3}, Lcom/squareup/noho/NohoNumberPicker;->access$300(Lcom/squareup/noho/NohoNumberPicker;)I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Lcom/squareup/noho/NohoNumberPicker;->invalidate(IIII)V

    :cond_1
    return-void
.end method

.method public run()V
    .locals 6

    .line 1146
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->mode:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_5

    if-eq v0, v1, :cond_0

    goto/16 :goto_0

    .line 1162
    :cond_0
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->managedButton:I

    if-eq v0, v3, :cond_3

    if-eq v0, v1, :cond_1

    goto/16 :goto_0

    .line 1172
    :cond_1
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoNumberPicker;->access$200(Lcom/squareup/noho/NohoNumberPicker;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1173
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, p0, v4, v5}, Lcom/squareup/noho/NohoNumberPicker;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1175
    :cond_2
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoNumberPicker;->access$200(Lcom/squareup/noho/NohoNumberPicker;)Z

    move-result v1

    xor-int/2addr v1, v3

    invoke-static {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->access$202(Lcom/squareup/noho/NohoNumberPicker;Z)Z

    .line 1176
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v1

    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v3}, Lcom/squareup/noho/NohoNumberPicker;->access$300(Lcom/squareup/noho/NohoNumberPicker;)I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Lcom/squareup/noho/NohoNumberPicker;->invalidate(IIII)V

    goto :goto_0

    .line 1164
    :cond_3
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoNumberPicker;->access$000(Lcom/squareup/noho/NohoNumberPicker;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1165
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, p0, v4, v5}, Lcom/squareup/noho/NohoNumberPicker;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1167
    :cond_4
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoNumberPicker;->access$000(Lcom/squareup/noho/NohoNumberPicker;)Z

    move-result v1

    xor-int/2addr v1, v3

    invoke-static {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->access$002(Lcom/squareup/noho/NohoNumberPicker;Z)Z

    .line 1168
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoNumberPicker;->access$100(Lcom/squareup/noho/NohoNumberPicker;)I

    move-result v1

    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoNumberPicker;->getBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/squareup/noho/NohoNumberPicker;->invalidate(IIII)V

    goto :goto_0

    .line 1148
    :cond_5
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->managedButton:I

    if-eq v0, v3, :cond_7

    if-eq v0, v1, :cond_6

    goto :goto_0

    .line 1155
    :cond_6
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0, v3}, Lcom/squareup/noho/NohoNumberPicker;->access$202(Lcom/squareup/noho/NohoNumberPicker;Z)Z

    .line 1156
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v1

    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v3}, Lcom/squareup/noho/NohoNumberPicker;->access$300(Lcom/squareup/noho/NohoNumberPicker;)I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Lcom/squareup/noho/NohoNumberPicker;->invalidate(IIII)V

    goto :goto_0

    .line 1150
    :cond_7
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0, v3}, Lcom/squareup/noho/NohoNumberPicker;->access$002(Lcom/squareup/noho/NohoNumberPicker;Z)Z

    .line 1151
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoNumberPicker;->access$100(Lcom/squareup/noho/NohoNumberPicker;)I

    move-result v1

    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->this$0:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoNumberPicker;->getBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/squareup/noho/NohoNumberPicker;->invalidate(IIII)V

    :goto_0
    return-void
.end method
