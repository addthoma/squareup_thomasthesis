.class public Lcom/squareup/notifications/AutoVoidNotifier$NoAutoVoidNotifier;
.super Ljava/lang/Object;
.source "AutoVoidNotifier.java"

# interfaces
.implements Lcom/squareup/notifications/AutoVoidNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notifications/AutoVoidNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoAutoVoidNotifier"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyDanglingAuthVoidedAfterCrash(Lcom/squareup/protos/common/Money;)V
    .locals 0

    return-void
.end method

.method public notifyDanglingMiryo()V
    .locals 0

    return-void
.end method

.method public notifyVoidOnTimeout(Lcom/squareup/protos/common/Money;)V
    .locals 0

    return-void
.end method
