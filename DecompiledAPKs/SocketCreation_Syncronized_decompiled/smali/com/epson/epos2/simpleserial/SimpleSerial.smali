.class public Lcom/epson/epos2/simpleserial/SimpleSerial;
.super Ljava/lang/Object;
.source "SimpleSerial.java"


# static fields
.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

.field private mSerialHandle:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 14
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 58
    sput v0, Lcom/epson/epos2/simpleserial/SimpleSerial;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 21
    iput-wide v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mContext:Landroid/content/Context;

    .line 23
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

    .line 24
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 26
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    .line 27
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 28
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 29
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 30
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 31
    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 74
    invoke-direct {p0, p1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "SimpleSerial"

    .line 76
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 79
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 80
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 81
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    .line 82
    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 85
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 86
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 88
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mContext:Landroid/content/Context;

    .line 90
    invoke-virtual {p0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->initializeSerialInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 91
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 492
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    .line 494
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 495
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 496
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 497
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 498
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 499
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 500
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 501
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 502
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 503
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 505
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 508
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 473
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 475
    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 479
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 478
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 481
    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 485
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 532
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 514
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 541
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 523
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 244
    iget-wide v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 245
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 179
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 187
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->checkHandle()V

    .line 189
    iget-wide v6, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 200
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 191
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 183
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 195
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 196
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 197
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 215
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->checkHandle()V

    .line 221
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 232
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 223
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 227
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 228
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 229
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 121
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 123
    iput-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

    .line 124
    iput-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 127
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 129
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2Disconnect(J)I

    .line 130
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2DestroyHandle(J)I

    .line 131
    iput-wide v5, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 138
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 135
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 136
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 401
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 403
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 404
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 408
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 410
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 413
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 432
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 434
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 435
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 439
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 441
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 444
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getStatus()Lcom/epson/epos2/simpleserial/SerialStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 374
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 376
    iget-wide v3, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2GetStatus(J)Lcom/epson/epos2/simpleserial/SerialStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {v1}, Lcom/epson/epos2/simpleserial/SerialStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/simpleserial/SimpleSerial;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 379
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/simpleserial/SimpleSerial;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 382
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializeSerialInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 105
    invoke-direct {p0, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 110
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 108
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/simpleserial/SerialStatusInfo;
.end method

.method protected native nativeEpos2SendCommand(J[B)I
.end method

.method protected onSimpleSerialReceive([B)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object p0, v1, v3

    const-string v4, "onSimpleSerialReceive"

    .line 339
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "data->"

    aput-object v5, v1, v2

    aput-object p1, v1, v3

    .line 344
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    iget-object v1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/simpleserial/ReceiveListener;->onSimpleSerialReceive(Lcom/epson/epos2/simpleserial/SimpleSerial;[B)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p0, v0, v3

    .line 351
    invoke-direct {p0, v4, v2, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public sendCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "sendCommand"

    .line 266
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->checkHandle()V

    if-eqz p1, :cond_1

    .line 276
    iget-wide v4, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->nativeEpos2SendCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 287
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 278
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4

    .line 273
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 282
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 283
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 284
    throw v1
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 457
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    iget-wide v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 463
    iput-object p1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 466
    iput-object p1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :goto_0
    return-void
.end method

.method public setReceiveEventListener(Lcom/epson/epos2/simpleserial/ReceiveListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setReceiveEventListener"

    .line 317
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    iget-wide v0, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mSerialHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 321
    iput-object p1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 324
    iput-object p1, p0, Lcom/epson/epos2/simpleserial/SimpleSerial;->mReceiveListener:Lcom/epson/epos2/simpleserial/ReceiveListener;

    :cond_1
    :goto_0
    return-void
.end method
