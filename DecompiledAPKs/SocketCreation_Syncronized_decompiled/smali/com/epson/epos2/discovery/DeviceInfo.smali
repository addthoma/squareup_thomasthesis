.class public Lcom/epson/epos2/discovery/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# instance fields
.field private bdAddress:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private deviceType:I

.field private ipAddress:Ljava/lang/String;

.field private macAddress:Ljava/lang/String;

.field private target:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBdAddress()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/epson/epos2/discovery/DeviceInfo;->bdAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/epson/epos2/discovery/DeviceInfo;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/epson/epos2/discovery/DeviceInfo;->deviceType:I

    return v0
.end method

.method public getIpAddress()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/epson/epos2/discovery/DeviceInfo;->ipAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/epson/epos2/discovery/DeviceInfo;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getTarget()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/epson/epos2/discovery/DeviceInfo;->target:Ljava/lang/String;

    return-object v0
.end method

.method protected setBdAddress(Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/epson/epos2/discovery/DeviceInfo;->bdAddress:Ljava/lang/String;

    return-void
.end method

.method protected setDeviceName(Ljava/lang/String;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/epson/epos2/discovery/DeviceInfo;->deviceName:Ljava/lang/String;

    return-void
.end method

.method protected setDeviceType(I)V
    .locals 0

    .line 59
    iput p1, p0, Lcom/epson/epos2/discovery/DeviceInfo;->deviceType:I

    return-void
.end method

.method protected setIpAddress(Ljava/lang/String;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/epson/epos2/discovery/DeviceInfo;->ipAddress:Ljava/lang/String;

    return-void
.end method

.method protected setMacAddress(Ljava/lang/String;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/epson/epos2/discovery/DeviceInfo;->macAddress:Ljava/lang/String;

    return-void
.end method

.method protected setTarget(Ljava/lang/String;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/epson/epos2/discovery/DeviceInfo;->target:Ljava/lang/String;

    return-void
.end method
