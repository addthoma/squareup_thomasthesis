.class public Lcom/epson/epos2/keyboard/Keyboard;
.super Ljava/lang/Object;
.source "Keyboard.java"


# static fields
.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final TRUE:I = 0x1

.field public static final VK_0:I = 0x30

.field public static final VK_1:I = 0x31

.field public static final VK_2:I = 0x32

.field public static final VK_3:I = 0x33

.field public static final VK_4:I = 0x34

.field public static final VK_5:I = 0x35

.field public static final VK_6:I = 0x36

.field public static final VK_7:I = 0x37

.field public static final VK_8:I = 0x38

.field public static final VK_9:I = 0x39

.field public static final VK_A:I = 0x41

.field public static final VK_ADD:I = 0x6b

.field public static final VK_B:I = 0x42

.field public static final VK_BACK:I = 0x8

.field public static final VK_C:I = 0x43

.field public static final VK_CAPITAL:I = 0x14

.field public static final VK_CONTROL:I = 0x11

.field public static final VK_CONVERT:I = 0x1c

.field public static final VK_D:I = 0x44

.field public static final VK_DELETE:I = 0x2e

.field public static final VK_DOWN:I = 0x28

.field public static final VK_E:I = 0x45

.field public static final VK_END:I = 0x23

.field public static final VK_ESCAPE:I = 0x1b

.field public static final VK_F:I = 0x46

.field public static final VK_F1:I = 0x70

.field public static final VK_F10:I = 0x79

.field public static final VK_F11:I = 0x7a

.field public static final VK_F12:I = 0x7b

.field public static final VK_F2:I = 0x71

.field public static final VK_F3:I = 0x72

.field public static final VK_F4:I = 0x73

.field public static final VK_F5:I = 0x74

.field public static final VK_F6:I = 0x75

.field public static final VK_F7:I = 0x76

.field public static final VK_F8:I = 0x77

.field public static final VK_F9:I = 0x78

.field public static final VK_G:I = 0x47

.field public static final VK_H:I = 0x48

.field public static final VK_HOME:I = 0x24

.field public static final VK_I:I = 0x49

.field public static final VK_INSERT:I = 0x2d

.field public static final VK_J:I = 0x4a

.field public static final VK_K:I = 0x4b

.field public static final VK_L:I = 0x4c

.field public static final VK_LEFT:I = 0x25

.field public static final VK_M:I = 0x4d

.field public static final VK_MENU:I = 0x12

.field public static final VK_MULTIPLY:I = 0x6a

.field public static final VK_N:I = 0x4e

.field public static final VK_NEXT:I = 0x22

.field public static final VK_NONCONVERT:I = 0x1d

.field public static final VK_O:I = 0x4f

.field public static final VK_OEM_1:I = 0xba

.field public static final VK_OEM_2:I = 0xbf

.field public static final VK_OEM_3:I = 0xc0

.field public static final VK_OEM_4:I = 0xdb

.field public static final VK_OEM_5:I = 0xdc

.field public static final VK_OEM_6:I = 0xdd

.field public static final VK_OEM_7:I = 0xde

.field public static final VK_OEM_ATTN:I = 0xf0

.field public static final VK_OEM_COMMA:I = 0xbc

.field public static final VK_OEM_MINUS:I = 0xbd

.field public static final VK_OEM_PERIOD:I = 0xbe

.field public static final VK_OEM_PLUS:I = 0xbb

.field public static final VK_P:I = 0x50

.field public static final VK_PRIOR:I = 0x21

.field public static final VK_Q:I = 0x51

.field public static final VK_R:I = 0x52

.field public static final VK_RETURN:I = 0xd

.field public static final VK_RIGHT:I = 0x27

.field public static final VK_S:I = 0x53

.field public static final VK_SHIFT:I = 0x10

.field public static final VK_SPACE:I = 0x20

.field public static final VK_SUBTRACT:I = 0x6d

.field public static final VK_T:I = 0x54

.field public static final VK_TAB:I = 0x9

.field public static final VK_U:I = 0x55

.field public static final VK_UP:I = 0x26

.field public static final VK_V:I = 0x56

.field public static final VK_W:I = 0x57

.field public static final VK_X:I = 0x58

.field public static final VK_Y:I = 0x59

.field public static final VK_Z:I = 0x5a

.field private static connection:I


# instance fields
.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

.field private mKeyboardHandle:J

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 14
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 145
    sput v0, Lcom/epson/epos2/keyboard/Keyboard;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 22
    iput-wide v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mContext:Landroid/content/Context;

    .line 134
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

    .line 135
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;

    .line 136
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 138
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    .line 139
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 140
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 141
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 142
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 143
    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 159
    invoke-direct {p0, p1}, Lcom/epson/epos2/keyboard/Keyboard;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "Keyboard"

    .line 160
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 163
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 164
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 165
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    .line 166
    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 169
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 170
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 173
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mContext:Landroid/content/Context;

    .line 174
    invoke-virtual {p0}, Lcom/epson/epos2/keyboard/Keyboard;->initializeKeyboardInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 175
    invoke-direct {p0, v3, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 660
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    .line 662
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 663
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 664
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 665
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 666
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 667
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 668
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 669
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 670
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 671
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 673
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 676
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 641
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 643
    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 647
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 646
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 649
    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 653
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 700
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 682
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 709
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 691
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 323
    iget-wide v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 324
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 261
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 267
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/keyboard/Keyboard;->checkHandle()V

    .line 269
    iget-wide v6, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/keyboard/Keyboard;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 280
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 271
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 265
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 275
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 276
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 277
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 294
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/keyboard/Keyboard;->checkHandle()V

    .line 299
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 310
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 301
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 305
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 306
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 307
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 204
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 206
    iput-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

    .line 207
    iput-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;

    .line 208
    iput-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 210
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 212
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2Disconnect(J)I

    .line 213
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2DestroyHandle(J)I

    .line 214
    iput-wide v5, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 221
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 218
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 219
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 569
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 571
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 572
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 576
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 578
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 581
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 599
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 601
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 602
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 606
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 608
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 611
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getPrefix()[I
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getPrefix"

    .line 369
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 371
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    const/16 v1, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 372
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-array v1, v0, [Ljava/lang/Object;

    .line 376
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 377
    iget-wide v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-virtual {p0, v0, v1}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2GetPrefix(J)[I

    move-result-object v0

    return-object v0
.end method

.method public getStatus()Lcom/epson/epos2/keyboard/KeyboardStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 542
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 544
    iget-wide v3, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2GetStatus(J)Lcom/epson/epos2/keyboard/KeyboardStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 546
    invoke-virtual {v1}, Lcom/epson/epos2/keyboard/KeyboardStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/keyboard/Keyboard;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 548
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/keyboard/Keyboard;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 551
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializeKeyboardInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 189
    invoke-direct {p0, v0}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 193
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 191
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetPrefix(J)[I
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/keyboard/KeyboardStatusInfo;
.end method

.method protected native nativeEpos2SetPrefix(J[I)I
.end method

.method protected onKbdKeyPress(ILjava/lang/String;)V
    .locals 7

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 468
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v4, 0x2

    aput-object p0, v1, v4

    const-string v5, "onKbdKeyPress"

    invoke-direct {p0, v5, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v6, "keyCode->"

    aput-object v6, v1, v3

    .line 480
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    const-string v6, "ascii->"

    aput-object v6, v1, v4

    aput-object p2, v1, v0

    .line 479
    invoke-direct {p0, v5, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 483
    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

    invoke-interface {v1, p0, p1, p2}, Lcom/epson/epos2/keyboard/KeyPressListener;->onKbdKeyPress(Lcom/epson/epos2/keyboard/Keyboard;ILjava/lang/String;)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 487
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p2, v0, v2

    aput-object p0, v0, v4

    invoke-direct {p0, v5, v3, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method protected onKbdReadString(Ljava/lang/String;I)V
    .locals 7

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 501
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const/4 v3, 0x2

    aput-object p0, v1, v3

    const-string v5, "onKbdReadString"

    invoke-direct {p0, v5, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 509
    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v6, "readString->"

    aput-object v6, v1, v2

    aput-object p1, v1, v4

    const-string v6, "prefix->"

    aput-object v6, v1, v3

    .line 514
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v0

    .line 512
    invoke-direct {p0, v5, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 516
    iget-object v1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;

    invoke-interface {v1, p0, p1, p2}, Lcom/epson/epos2/keyboard/ReadStringListener;->onKbdReadString(Lcom/epson/epos2/keyboard/Keyboard;Ljava/lang/String;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 520
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    aput-object p0, v0, v3

    invoke-direct {p0, v5, v2, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 625
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 627
    iget-wide v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 629
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 632
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setKeyPressEventListener(Lcom/epson/epos2/keyboard/KeyPressListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setKeyPressEventListener"

    .line 406
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 408
    iget-wide v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 410
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 413
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/epos2/keyboard/KeyPressListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setPrefix([I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "setPrefix"

    .line 345
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/keyboard/Keyboard;->checkHandle()V

    .line 349
    iget-wide v4, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/keyboard/Keyboard;->nativeEpos2SetPrefix(J[I)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 360
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 351
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 355
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/keyboard/Keyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 356
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 357
    throw v1
.end method

.method public setReadStringEventListener(Lcom/epson/epos2/keyboard/ReadStringListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setReadStringEventListener"

    .line 446
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 448
    iget-wide v0, p0, Lcom/epson/epos2/keyboard/Keyboard;->mKeyboardHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 450
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 453
    iput-object p1, p0, Lcom/epson/epos2/keyboard/Keyboard;->mStringListener:Lcom/epson/epos2/keyboard/ReadStringListener;

    :cond_1
    :goto_0
    return-void
.end method
