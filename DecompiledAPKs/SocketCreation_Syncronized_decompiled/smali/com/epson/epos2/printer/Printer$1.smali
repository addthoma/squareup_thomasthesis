.class Lcom/epson/epos2/printer/Printer$1;
.super Lcom/epson/epos2/printer/Printer$InnerThread;
.source "Printer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/epos2/printer/Printer;->getMaintenanceCounter(IILcom/epson/epos2/printer/MaintenanceCounterListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/epos2/printer/Printer;


# direct methods
.method constructor <init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V
    .locals 0

    .line 1131
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$1;->this$0:Lcom/epson/epos2/printer/Printer;

    invoke-direct/range {p0 .. p9}, Lcom/epson/epos2/printer/Printer$InnerThread;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1133
    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$1;->mPrn:Lcom/epson/epos2/printer/Printer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$1;->mListener:Ljava/lang/Object;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v7, v0, [I

    const/4 v8, 0x0

    aput v8, v7, v8

    .line 1138
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer$1;->mPrn:Lcom/epson/epos2/printer/Printer;

    iget-wide v2, p0, Lcom/epson/epos2/printer/Printer$1;->mHandle:J

    iget v4, p0, Lcom/epson/epos2/printer/Printer$1;->mTimeout:I

    iget v5, p0, Lcom/epson/epos2/printer/Printer$1;->mType:I

    move-object v6, v7

    invoke-virtual/range {v1 .. v6}, Lcom/epson/epos2/printer/Printer;->nativeEpos2GetMaintenanceCounter(JII[I)I

    move-result v1

    .line 1140
    iget-object v2, p0, Lcom/epson/epos2/printer/Printer$1;->this$0:Lcom/epson/epos2/printer/Printer;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "code->"

    aput-object v4, v3, v8

    .line 1141
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string v4, "type->"

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget v4, p0, Lcom/epson/epos2/printer/Printer$1;->mType:I

    .line 1142
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    const-string v4, "value->"

    aput-object v4, v3, v0

    const/4 v0, 0x5

    aget v4, v7, v8

    .line 1143
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const-string v0, "onGetMaintenanceCounter"

    .line 1140
    invoke-virtual {v2, v0, v3}, Lcom/epson/epos2/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1144
    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$1;->mListener:Ljava/lang/Object;

    check-cast v0, Lcom/epson/epos2/printer/MaintenanceCounterListener;

    iget v2, p0, Lcom/epson/epos2/printer/Printer$1;->mType:I

    aget v3, v7, v8

    invoke-interface {v0, v1, v2, v3}, Lcom/epson/epos2/printer/MaintenanceCounterListener;->onGetMaintenanceCounter(III)V

    :cond_1
    :goto_0
    return-void
.end method
