.class public Lcom/epson/epos2/cat/Epos2CallbackCode;
.super Ljava/lang/Object;
.source "Epos2CallbackCode.java"


# static fields
.field public static final CODE_BUSY:I = 0x1

.field public static final CODE_DISAGREEMENT:I = 0x3

.field public static final CODE_ERR_CENTER:I = 0x6

.field public static final CODE_ERR_DEVICE:I = 0x9

.field public static final CODE_ERR_FAILURE:I = 0xc

.field public static final CODE_ERR_OPOSCODE:I = 0x7

.field public static final CODE_ERR_PARAM:I = 0x8

.field public static final CODE_ERR_SYSTEM:I = 0xa

.field public static final CODE_ERR_TIMEOUT:I = 0xb

.field public static final CODE_EXCEEDING_LIMIT:I = 0x2

.field public static final CODE_INVALID_CARD:I = 0x4

.field public static final CODE_RESET:I = 0x5

.field public static final CODE_SUCCESS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
