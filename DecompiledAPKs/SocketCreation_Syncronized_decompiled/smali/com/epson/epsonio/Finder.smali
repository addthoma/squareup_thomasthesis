.class public Lcom/epson/epsonio/Finder;
.super Ljava/lang/Object;
.source "Finder.java"


# static fields
.field public static final FILTER_NAME:I = 0x1

.field public static final FILTER_NONE:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static mDeviceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    :try_start_0
    const-string v0, "epos2"

    .line 28
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "epsonio"

    .line 32
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_2

    :catch_1
    :goto_0
    const/4 v0, 0x0

    .line 44
    sput v0, Lcom/epson/epsonio/Finder;->mDeviceType:I

    return-void

    .line 35
    :catch_2
    throw v0
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final native findStart(Landroid/content/Context;ILjava/lang/String;)I
.end method

.method private static final native findStop()I
.end method

.method private static final native getDevList([II)[Lcom/epson/epsonio/DeviceInfo;
.end method

.method public static final declared-synchronized getDeviceInfoList(I)[Lcom/epson/epsonio/DeviceInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const-class v0, Lcom/epson/epsonio/Finder;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [I

    .line 201
    sget v3, Lcom/epson/epsonio/Finder;->mDeviceType:I

    if-eqz v3, :cond_3

    if-eq v1, p0, :cond_1

    if-eqz p0, :cond_1

    const/4 v3, -0x2

    if-ne v3, p0, :cond_0

    goto :goto_0

    .line 208
    :cond_0
    new-instance p0, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p0, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p0

    .line 211
    :cond_1
    :goto_0
    sget v1, Lcom/epson/epsonio/Finder;->mDeviceType:I

    const/4 v3, 0x0

    const/16 v4, 0xff

    const/4 v5, 0x0

    packed-switch v1, :pswitch_data_0

    .line 247
    new-instance p0, Lcom/epson/epsonio/EpsonIoException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 236
    :pswitch_0
    :try_start_1
    invoke-static {v2, v5}, Lcom/epson/epsonio/SupportUsb;->getResult([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 240
    :try_start_2
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v1, v3, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 241
    invoke-virtual {v1, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 242
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 225
    :pswitch_1
    :try_start_3
    invoke-static {v2, p0}, Lcom/epson/epsonio/bluetooth/DevBt;->getResult([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p0

    .line 228
    :try_start_4
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v1, v3, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 229
    invoke-virtual {v1, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 230
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 214
    :pswitch_2
    :try_start_5
    invoke-static {v2, v5}, Lcom/epson/epsonio/Finder;->getDevList([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object p0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 250
    :goto_1
    :try_start_6
    aget v1, v2, v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v1, :cond_2

    .line 254
    monitor-exit v0

    return-object p0

    .line 251
    :cond_2
    :try_start_7
    new-instance p0, Lcom/epson/epsonio/EpsonIoException;

    aget v1, v2, v5

    invoke-direct {p0, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p0

    :catch_2
    move-exception p0

    .line 217
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v1, v3, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 218
    invoke-virtual {v1, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 219
    throw v1

    .line 247
    :goto_2
    invoke-direct {p0, v4}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p0

    .line 202
    :cond_3
    new-instance p0, Lcom/epson/epsonio/EpsonIoException;

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final declared-synchronized getResult()[Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const-class v0, Lcom/epson/epsonio/Finder;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    new-array v1, v1, [I

    .line 109
    sget v2, Lcom/epson/epsonio/Finder;->mDeviceType:I

    if-eqz v2, :cond_3

    .line 113
    sget v2, Lcom/epson/epsonio/Finder;->mDeviceType:I

    const/4 v3, 0x0

    const/16 v4, 0xff

    const/4 v5, 0x0

    packed-switch v2, :pswitch_data_0

    .line 156
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 146
    :pswitch_0
    :try_start_1
    invoke-static {v1, v5}, Lcom/epson/epsonio/SupportUsb;->getResult([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 149
    :try_start_2
    new-instance v2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v2, v3, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 150
    invoke-virtual {v2, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 151
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    :pswitch_1
    :try_start_3
    invoke-static {v1, v5}, Lcom/epson/epsonio/bluetooth/DevBt;->getResult([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v1

    .line 136
    :try_start_4
    new-instance v2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v2, v3, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 137
    invoke-virtual {v2, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 138
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 118
    :pswitch_2
    :try_start_5
    invoke-static {v1, v5}, Lcom/epson/epsonio/Finder;->getDevList([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 159
    :goto_0
    :try_start_6
    aget v6, v1, v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v6, :cond_2

    .line 166
    :try_start_7
    array-length v1, v2

    new-array v1, v1, [Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 176
    :goto_1
    :try_start_8
    array-length v3, v2

    if-ge v5, v3, :cond_1

    const/16 v3, 0x101

    .line 177
    sget v4, Lcom/epson/epsonio/Finder;->mDeviceType:I

    if-ne v3, v4, :cond_0

    .line 178
    aget-object v3, v2, v5

    invoke-virtual {v3}, Lcom/epson/epsonio/DeviceInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5

    goto :goto_2

    .line 181
    :cond_0
    aget-object v3, v2, v5

    invoke-virtual {v3}, Lcom/epson/epsonio/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 186
    :cond_1
    monitor-exit v0

    return-object v1

    :catch_2
    move-exception v1

    .line 169
    :try_start_9
    new-instance v2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v2, v3, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    invoke-virtual {v2, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 171
    throw v2

    .line 160
    :cond_2
    new-instance v2, Lcom/epson/epsonio/EpsonIoException;

    aget v1, v1, v5

    invoke-direct {v2, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v2

    :catch_3
    move-exception v1

    .line 122
    new-instance v2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v2, v3, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 123
    invoke-virtual {v2, v4}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 124
    throw v2

    .line 156
    :goto_3
    invoke-direct {v1, v4}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v1

    .line 110
    :cond_3
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static declared-synchronized start(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const-class v0, Lcom/epson/epsonio/Finder;

    monitor-enter v0

    .line 50
    :try_start_0
    sget v1, Lcom/epson/epsonio/Finder;->mDeviceType:I

    if-nez v1, :cond_1

    const/16 v1, 0xff

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    .line 91
    new-instance p0, Lcom/epson/epsonio/EpsonIoException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 81
    :pswitch_0
    :try_start_1
    invoke-static {p0, p1, p2}, Lcom/epson/epsonio/SupportUsb;->start(Landroid/content/Context;ILjava/lang/String;)I

    move-result p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 84
    :try_start_2
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v2, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 85
    invoke-virtual {p1, v1}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 86
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :pswitch_1
    const/4 v3, 0x0

    .line 68
    :try_start_3
    invoke-static {p0, p1, p2, v3}, Lcom/epson/epsonio/bluetooth/DevBt;->start(Landroid/content/Context;ILjava/lang/String;I)I

    move-result p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p0

    .line 71
    :try_start_4
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v2, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    invoke-virtual {p1, v1}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 73
    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 57
    :pswitch_2
    :try_start_5
    invoke-static {p0, p1, p2}, Lcom/epson/epsonio/Finder;->findStart(Landroid/content/Context;ILjava/lang/String;)I

    move-result p0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_0
    if-nez p0, :cond_0

    .line 98
    :try_start_6
    sput p1, Lcom/epson/epsonio/Finder;->mDeviceType:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 100
    monitor-exit v0

    return-void

    .line 95
    :cond_0
    :try_start_7
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    :catch_2
    move-exception p0

    .line 60
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v2, p0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    invoke-virtual {p1, v1}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 62
    throw p1

    :goto_1
    const/4 p1, 0x1

    .line 91
    invoke-direct {p0, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p0

    .line 51
    :cond_1
    new-instance p0, Lcom/epson/epsonio/EpsonIoException;

    const/4 p1, 0x6

    invoke-direct {p0, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static declared-synchronized stop()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const-class v0, Lcom/epson/epsonio/Finder;

    monitor-enter v0

    .line 261
    :try_start_0
    sget v1, Lcom/epson/epsonio/Finder;->mDeviceType:I

    if-eqz v1, :cond_1

    .line 265
    sget v1, Lcom/epson/epsonio/Finder;->mDeviceType:I

    const/4 v2, 0x0

    const/16 v3, 0xff

    packed-switch v1, :pswitch_data_0

    .line 302
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 292
    :pswitch_0
    :try_start_1
    invoke-static {}, Lcom/epson/epsonio/SupportUsb;->stop()I

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 295
    :try_start_2
    new-instance v4, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v4, v2, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296
    invoke-virtual {v4, v3}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 297
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 279
    :pswitch_1
    :try_start_3
    invoke-static {}, Lcom/epson/epsonio/bluetooth/DevBt;->stop()I

    move-result v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v1

    .line 282
    :try_start_4
    new-instance v4, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v4, v2, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 283
    invoke-virtual {v4, v3}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 284
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 268
    :pswitch_2
    :try_start_5
    invoke-static {}, Lcom/epson/epsonio/Finder;->findStop()I

    move-result v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_0
    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 309
    :try_start_6
    sput v1, Lcom/epson/epsonio/Finder;->mDeviceType:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 311
    monitor-exit v0

    return-void

    .line 306
    :cond_0
    :try_start_7
    new-instance v2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v2, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v2

    :catch_2
    move-exception v1

    .line 271
    new-instance v4, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v4, v2, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 272
    invoke-virtual {v4, v3}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 273
    throw v4

    .line 302
    :goto_1
    invoke-direct {v1, v3}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v1

    .line 262
    :cond_1
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
