.class public abstract Lcom/epson/eposdevice/display/Display;
.super Lcom/epson/eposdevice/display/NativeDisplay;
.source "Display.java"


# static fields
.field public static final BRIGHTNESS_100:I = 0x3

.field public static final BRIGHTNESS_20:I = 0x0

.field public static final BRIGHTNESS_40:I = 0x1

.field public static final BRIGHTNESS_60:I = 0x2

.field public static final CURSOR_NONE:I = 0x0

.field public static final CURSOR_UNDERLINE:I = 0x1

.field public static final FALSE:I = 0x0

.field public static final LANG_EN:I = 0x0

.field public static final LANG_JA:I = 0x1

.field public static final MARQUEE_PLACE:I = 0x1

.field public static final MARQUEE_WALK:I = 0x0

.field public static final MOVE_BOTTOM_LEFT:I = 0x2

.field public static final MOVE_BOTTOM_RIGHT:I = 0x3

.field public static final MOVE_TOP_LEFT:I = 0x0

.field public static final MOVE_TOP_RIGHT:I = 0x1

.field public static final SCROLL_HORIZONTAL:I = 0x2

.field public static final SCROLL_OVERWRITE:I = 0x0

.field public static final SCROLL_VERTICAL:I = 0x1

.field public static final TRUE:I = 0x1


# instance fields
.field private mDspHandle:J

.field private mReciveListener:Lcom/epson/eposdevice/display/ReceiveListener;


# direct methods
.method protected constructor <init>(J)V
    .locals 2

    .line 51
    invoke-direct {p0}, Lcom/epson/eposdevice/display/NativeDisplay;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lcom/epson/eposdevice/display/Display;->mReciveListener:Lcom/epson/eposdevice/display/ReceiveListener;

    const-wide/16 v0, 0x0

    .line 446
    iput-wide v0, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    .line 52
    iput-wide p1, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    return-void
.end method

.method private OnDspReceive(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/epson/eposdevice/display/Display;->mReciveListener:Lcom/epson/eposdevice/display/ReceiveListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    .line 46
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "onDspReceive"

    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/display/Display;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/epson/eposdevice/display/Display;->mReciveListener:Lcom/epson/eposdevice/display/ReceiveListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/epson/eposdevice/display/ReceiveListener;->onDspReceive(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addCommand"

    .line 413
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 415
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 416
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/display/Display;->nativeDspAddCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 425
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 418
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 422
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 423
    throw p1
.end method

.method public addMarquee(Ljava/lang/String;IIIII)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p1, v1, v14

    .line 349
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v15, 0x1

    aput-object v2, v1, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x2

    aput-object v2, v1, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x3

    aput-object v2, v1, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v18, 0x4

    aput-object v2, v1, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v19, 0x5

    aput-object v2, v1, v19

    const-string v12, "addMarquee"

    invoke-virtual {v13, v12, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 352
    iget-wide v2, v13, Lcom/epson/eposdevice/display/Display;->mDspHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v10, p3

    int-to-long v6, v10

    move/from16 v11, p4

    int-to-long v8, v11

    move/from16 v5, p5

    int-to-long v14, v5

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-wide v10, v14

    move-object v14, v12

    move/from16 v12, p6

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/eposdevice/display/Display;->nativeDspAddMarquee(JLjava/lang/String;IJJJI)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 361
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-virtual {v13, v14, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 354
    :cond_0
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v14, v12

    .line 358
    :goto_0
    invoke-virtual {v13, v14, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 359
    throw v0
.end method

.method public addReverseText(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addReverseText"

    .line 285
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 288
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/display/Display;->nativeDspAddReverseText(JLjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 297
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 290
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 294
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 295
    throw p1
.end method

.method public addReverseText(Ljava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 301
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "addReverseText"

    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 304
    iget-wide v5, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/eposdevice/display/Display;->nativeDspAddReverseTextLang(JLjava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 313
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 306
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 310
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 311
    throw p1
.end method

.method public addReverseText(Ljava/lang/String;II)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v1, v10

    .line 317
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x1

    aput-object v2, v1, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x2

    aput-object v2, v1, v12

    const-string v13, "addReverseText"

    invoke-virtual {v9, v13, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 320
    iget-wide v2, v9, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    move/from16 v14, p2

    int-to-long v5, v14

    move/from16 v15, p3

    int-to-long v7, v15

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v1 .. v8}, Lcom/epson/eposdevice/display/Display;->nativeDspAddReverseTextPosition(JLjava/lang/String;JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v10

    .line 329
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 322
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 326
    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 327
    throw v0
.end method

.method public addReverseText(Ljava/lang/String;III)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v10, p0

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v1, v11

    .line 333
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x1

    aput-object v2, v1, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v13, 0x2

    aput-object v2, v1, v13

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x3

    aput-object v2, v1, v14

    const-string v15, "addReverseText"

    invoke-virtual {v10, v15, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 336
    iget-wide v2, v10, Lcom/epson/eposdevice/display/Display;->mDspHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v9, p2

    int-to-long v5, v9

    move/from16 v7, p3

    move-object/from16 v16, v15

    int-to-long v14, v7

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide v7, v14

    move/from16 v9, p4

    :try_start_1
    invoke-virtual/range {v1 .. v9}, Lcom/epson/eposdevice/display/Display;->nativeDspAddReverseTextPositionLang(JLjava/lang/String;JJI)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v11

    .line 345
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    move-object/from16 v2, v16

    invoke-virtual {v10, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, v16

    .line 338
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v2, v16

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v2, v15

    .line 342
    :goto_0
    invoke-virtual {v10, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 343
    throw v0
.end method

.method public addText(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addText"

    .line 221
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 224
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/display/Display;->nativeDspAddText(JLjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 233
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 226
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 230
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 231
    throw p1
.end method

.method public addText(Ljava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 237
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "addText"

    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 240
    iget-wide v5, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/eposdevice/display/Display;->nativeDspAddTextLang(JLjava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 249
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 242
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 246
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 247
    throw p1
.end method

.method public addText(Ljava/lang/String;II)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v1, v10

    .line 253
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x1

    aput-object v2, v1, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x2

    aput-object v2, v1, v12

    const-string v13, "addText"

    invoke-virtual {v9, v13, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 256
    iget-wide v2, v9, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    move/from16 v14, p2

    int-to-long v5, v14

    move/from16 v15, p3

    int-to-long v7, v15

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v1 .. v8}, Lcom/epson/eposdevice/display/Display;->nativeDspAddTextPosition(JLjava/lang/String;JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v10

    .line 265
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 258
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 262
    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 263
    throw v0
.end method

.method public addText(Ljava/lang/String;III)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v10, p0

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v1, v11

    .line 269
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x1

    aput-object v2, v1, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v13, 0x2

    aput-object v2, v1, v13

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x3

    aput-object v2, v1, v14

    const-string v15, "addText"

    invoke-virtual {v10, v15, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 272
    iget-wide v2, v10, Lcom/epson/eposdevice/display/Display;->mDspHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v9, p2

    int-to-long v5, v9

    move/from16 v7, p3

    move-object/from16 v16, v15

    int-to-long v14, v7

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide v7, v14

    move/from16 v9, p4

    :try_start_1
    invoke-virtual/range {v1 .. v9}, Lcom/epson/eposdevice/display/Display;->nativeDspAddTextPositionLang(JLjava/lang/String;JJI)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v11

    .line 281
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    move-object/from16 v2, v16

    invoke-virtual {v10, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, v16

    .line 274
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v2, v16

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v2, v15

    .line 278
    :goto_0
    invoke-virtual {v10, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 279
    throw v0
.end method

.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    .line 63
    iget-wide v0, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 64
    :cond_0
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
.end method

.method public clearCommandBuffer()V
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "clearCommandBuffer"

    .line 85
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    iget-wide v3, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/display/Display;->nativeDspClearCommandBuffer(J)I

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 89
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public clearWindow()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "clearWindow"

    .line 157
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 160
    iget-wide v3, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/display/Display;->nativeDspAddClearWindow(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 169
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 162
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 166
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 167
    throw v0
.end method

.method public createWindow(IIIIII)V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v15, p0

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/Object;

    .line 109
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x0

    aput-object v2, v1, v16

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x1

    aput-object v2, v1, v17

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v18, 0x2

    aput-object v2, v1, v18

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v19, 0x3

    aput-object v2, v1, v19

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v20, 0x4

    aput-object v2, v1, v20

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v21, 0x5

    aput-object v2, v1, v21

    const-string v14, "createWindow"

    invoke-virtual {v15, v14, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 112
    iget-wide v2, v15, Lcom/epson/eposdevice/display/Display;->mDspHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v12, p1

    int-to-long v4, v12

    move/from16 v13, p2

    int-to-long v6, v13

    move/from16 v10, p3

    int-to-long v8, v10

    move/from16 v11, p4

    int-to-long v12, v11

    move/from16 v1, p5

    move-object/from16 v22, v14

    int-to-long v14, v1

    move-object/from16 v1, p0

    move-wide v10, v12

    move-wide v12, v14

    move-object/from16 v15, v22

    move/from16 v14, p6

    :try_start_1
    invoke-virtual/range {v1 .. v14}, Lcom/epson/eposdevice/display/Display;->nativeDspAddCreateWindow(JJJJJJI)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 121
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v20

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v21

    move-object/from16 v2, p0

    invoke-virtual {v2, v15, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, p0

    .line 114
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v2, p0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v2, v15

    move-object v15, v14

    .line 118
    :goto_0
    invoke-virtual {v2, v15, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 119
    throw v0
.end method

.method public destroyWindow(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 125
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "destroyWindow"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 128
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/display/Display;->nativeDspAddDestroyWindow(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 137
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 130
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 134
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 135
    throw p1
.end method

.method protected getInnerHandle()J
    .locals 2

    .line 55
    iget-wide v0, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    return-wide v0
.end method

.method protected innerDeleteInstance()V
    .locals 3

    .line 58
    iget-wide v0, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/display/Display;->nativeSetDspReceiveCallback(JLcom/epson/eposdevice/display/NativeDisplay;)I

    const-wide/16 v0, 0x0

    .line 59
    iput-wide v0, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    return-void
.end method

.method public moveCursorPosition(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 189
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "moveCursorPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 192
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/display/Display;->nativeDspAddMoveCursorPosition(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 201
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 194
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 198
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 199
    throw p1
.end method

.method protected nativeOnDspReceive(Ljava/lang/String;Ljava/lang/String;IIJJ)V
    .locals 0

    .line 443
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/epson/eposdevice/display/Display;->OnDspReceive(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method protected abstract outputException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public reset()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "reset"

    .line 93
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 96
    iget-wide v3, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/display/Display;->nativeDspAddReset(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 105
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 98
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 102
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 103
    throw v0
.end method

.method public sendData()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "sendData"

    .line 69
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 72
    iget-wide v3, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/display/Display;->nativeDspSendData(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 81
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 74
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 79
    throw v0
.end method

.method public setBlink(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 365
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setBlink"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 368
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/display/Display;->nativeDspAddBlink(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 377
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 370
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 374
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 375
    throw p1
.end method

.method public setBrightness(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 381
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setBrightness"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 384
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/display/Display;->nativeDspAddBrightness(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 393
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 386
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 390
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 391
    throw p1
.end method

.method public setCurrentWindow(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 141
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setCurrentWindow"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 144
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/display/Display;->nativeDspAddSetCurrentWindow(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 153
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 146
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 150
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 151
    throw p1
.end method

.method public setCursorPosition(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 173
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "setCursorPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 176
    iget-wide v6, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/eposdevice/display/Display;->nativeDspAddSetCursorPosition(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 185
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 178
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 182
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 183
    throw p1
.end method

.method public setCursorType(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 205
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setCursorType"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 208
    iget-wide v4, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/display/Display;->nativeDspAddSetCursorType(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 217
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 210
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 214
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 215
    throw p1
.end method

.method public setReceiveEventCallback(Lcom/epson/eposdevice/display/ReceiveListener;)V
    .locals 5

    .line 429
    iget-wide v0, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 431
    iput-object p1, p0, Lcom/epson/eposdevice/display/Display;->mReciveListener:Lcom/epson/eposdevice/display/ReceiveListener;

    .line 432
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/display/Display;->nativeSetDspReceiveCallback(JLcom/epson/eposdevice/display/NativeDisplay;)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 435
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/display/Display;->nativeSetDspReceiveCallback(JLcom/epson/eposdevice/display/NativeDisplay;)I

    .line 436
    iput-object p1, p0, Lcom/epson/eposdevice/display/Display;->mReciveListener:Lcom/epson/eposdevice/display/ReceiveListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public showClock()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "showClock"

    .line 397
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/display/Display;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 399
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/display/Display;->checkHandle()V

    .line 400
    iget-wide v3, p0, Lcom/epson/eposdevice/display/Display;->mDspHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/display/Display;->nativeDspAddShowClock(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 409
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 402
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 406
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/display/Display;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 407
    throw v0
.end method
