.class public Lcom/epson/eposdevice/EposCallbackCode;
.super Ljava/lang/Object;
.source "EposCallbackCode.java"


# static fields
.field public static final ERR_ALREADY_OPENED:I = 0x16

.field public static final ERR_ALREADY_USED:I = 0x17

.field public static final ERR_AUTOMATICAL:I = 0xd

.field public static final ERR_BATTERY_LOW:I = 0x1d

.field public static final ERR_BOX_CLIENT_OVER:I = 0x19

.field public static final ERR_BOX_COUNT_OVER:I = 0x18

.field public static final ERR_CLOSE:I = 0xb

.field public static final ERR_COVER_OPEN:I = 0xe

.field public static final ERR_CUTTER:I = 0xf

.field public static final ERR_EMPTY:I = 0x11

.field public static final ERR_FAILURE:I = 0xff

.field public static final ERR_ILLEGAL:I = 0x5

.field public static final ERR_INVALID_WINDOW:I = 0x15

.field public static final ERR_IN_USE:I = 0x8

.field public static final ERR_JOB_NOT_FOUND:I = 0x1a

.field public static final ERR_MECHANICAL:I = 0x10

.field public static final ERR_MEMORY:I = 0xc

.field public static final ERR_NOT_FOUND:I = 0x6

.field public static final ERR_NOT_OPENED:I = 0xa

.field public static final ERR_OPEN:I = 0x9

.field public static final ERR_PARAM:I = 0x1

.field public static final ERR_PORT:I = 0x14

.field public static final ERR_SPOOLER:I = 0x1c

.field public static final ERR_SYSTEM:I = 0x13

.field public static final ERR_TIMEOUT:I = 0x3

.field public static final ERR_TYPE_INVALID:I = 0x7

.field public static final ERR_UNRECOVERABLE:I = 0x12

.field public static final PRINTING:I = 0x1b

.field public static final SUCCESS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
