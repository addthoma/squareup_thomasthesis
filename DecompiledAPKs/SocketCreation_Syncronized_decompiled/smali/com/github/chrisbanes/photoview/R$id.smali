.class public final Lcom/github/chrisbanes/photoview/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/github/chrisbanes/photoview/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0a0135

.field public static final action_bar_activity_content:I = 0x7f0a0136

.field public static final action_bar_container:I = 0x7f0a0137

.field public static final action_bar_root:I = 0x7f0a0139

.field public static final action_bar_spinner:I = 0x7f0a013a

.field public static final action_bar_subtitle:I = 0x7f0a013b

.field public static final action_bar_title:I = 0x7f0a013c

.field public static final action_container:I = 0x7f0a0140

.field public static final action_context_bar:I = 0x7f0a0141

.field public static final action_divider:I = 0x7f0a0143

.field public static final action_image:I = 0x7f0a0146

.field public static final action_menu_divider:I = 0x7f0a0147

.field public static final action_menu_presenter:I = 0x7f0a0148

.field public static final action_mode_bar:I = 0x7f0a0149

.field public static final action_mode_bar_stub:I = 0x7f0a014a

.field public static final action_mode_close_button:I = 0x7f0a014b

.field public static final action_text:I = 0x7f0a014c

.field public static final actions:I = 0x7f0a014e

.field public static final activity_chooser_view_content:I = 0x7f0a0160

.field public static final add:I = 0x7f0a0166

.field public static final alertTitle:I = 0x7f0a01ad

.field public static final async:I = 0x7f0a01d1

.field public static final blocking:I = 0x7f0a0239

.field public static final bottom:I = 0x7f0a023f

.field public static final buttonPanel:I = 0x7f0a0276

.field public static final checkbox:I = 0x7f0a0318

.field public static final chronometer:I = 0x7f0a033a

.field public static final content:I = 0x7f0a03a3

.field public static final contentPanel:I = 0x7f0a03a4

.field public static final custom:I = 0x7f0a0524

.field public static final customPanel:I = 0x7f0a0525

.field public static final decor_content_parent:I = 0x7f0a055d

.field public static final default_activity_button:I = 0x7f0a055e

.field public static final edit_query:I = 0x7f0a0660

.field public static final end:I = 0x7f0a0707

.field public static final expand_activities_button:I = 0x7f0a0732

.field public static final expanded_menu:I = 0x7f0a0733

.field public static final forever:I = 0x7f0a076d

.field public static final group_divider:I = 0x7f0a07be

.field public static final home:I = 0x7f0a07df

.field public static final icon:I = 0x7f0a0818

.field public static final icon_group:I = 0x7f0a081a

.field public static final image:I = 0x7f0a0821

.field public static final info:I = 0x7f0a082d

.field public static final italic:I = 0x7f0a08c8

.field public static final left:I = 0x7f0a0920

.field public static final line1:I = 0x7f0a093a

.field public static final line3:I = 0x7f0a093b

.field public static final listMode:I = 0x7f0a094a

.field public static final list_item:I = 0x7f0a094c

.field public static final message:I = 0x7f0a09d3

.field public static final multiply:I = 0x7f0a0a0b

.field public static final none:I = 0x7f0a0a45

.field public static final normal:I = 0x7f0a0a46

.field public static final notification_background:I = 0x7f0a0a4c

.field public static final notification_main_column:I = 0x7f0a0a60

.field public static final notification_main_column_container:I = 0x7f0a0a61

.field public static final parentPanel:I = 0x7f0a0bae

.field public static final progress_circular:I = 0x7f0a0c7e

.field public static final progress_horizontal:I = 0x7f0a0c81

.field public static final radio:I = 0x7f0a0cd9

.field public static final right:I = 0x7f0a0d8d

.field public static final right_icon:I = 0x7f0a0d8f

.field public static final right_side:I = 0x7f0a0d92

.field public static final screen:I = 0x7f0a0e0a

.field public static final scrollIndicatorDown:I = 0x7f0a0e10

.field public static final scrollIndicatorUp:I = 0x7f0a0e11

.field public static final scrollView:I = 0x7f0a0e12

.field public static final search_badge:I = 0x7f0a0e19

.field public static final search_bar:I = 0x7f0a0e1a

.field public static final search_button:I = 0x7f0a0e1b

.field public static final search_close_btn:I = 0x7f0a0e1c

.field public static final search_edit_frame:I = 0x7f0a0e1d

.field public static final search_go_btn:I = 0x7f0a0e1f

.field public static final search_mag_icon:I = 0x7f0a0e22

.field public static final search_plate:I = 0x7f0a0e23

.field public static final search_src_text:I = 0x7f0a0e26

.field public static final search_voice_btn:I = 0x7f0a0e27

.field public static final select_dialog_listview:I = 0x7f0a0e41

.field public static final shortcut:I = 0x7f0a0e7d

.field public static final spacer:I = 0x7f0a0eb8

.field public static final split_action_bar:I = 0x7f0a0ecd

.field public static final src_atop:I = 0x7f0a0f19

.field public static final src_in:I = 0x7f0a0f1a

.field public static final src_over:I = 0x7f0a0f1b

.field public static final start:I = 0x7f0a0f2a

.field public static final submenuarrow:I = 0x7f0a0f44

.field public static final submit_area:I = 0x7f0a0f46

.field public static final tabMode:I = 0x7f0a0f5e

.field public static final tag_transition_group:I = 0x7f0a0f6b

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0f6c

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0f6d

.field public static final text:I = 0x7f0a0f94

.field public static final text2:I = 0x7f0a0f95

.field public static final textSpacerNoButtons:I = 0x7f0a0f98

.field public static final textSpacerNoTitle:I = 0x7f0a0f99

.field public static final time:I = 0x7f0a0fde

.field public static final title:I = 0x7f0a103f

.field public static final titleDividerNoCustom:I = 0x7f0a1041

.field public static final title_template:I = 0x7f0a1046

.field public static final top:I = 0x7f0a104e

.field public static final topPanel:I = 0x7f0a104f

.field public static final uniform:I = 0x7f0a10b2

.field public static final up:I = 0x7f0a10bf

.field public static final wrap_content:I = 0x7f0a1125


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
