.class public Lcom/github/mikephil/charting/utils/Highlight;
.super Ljava/lang/Object;
.source "Highlight.java"


# instance fields
.field private mDataSetIndex:I

.field private mStackIndex:I

.field private mXIndex:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 18
    iput v0, p0, Lcom/github/mikephil/charting/utils/Highlight;->mStackIndex:I

    .line 28
    iput p1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mXIndex:I

    .line 29
    iput p2, p0, Lcom/github/mikephil/charting/utils/Highlight;->mDataSetIndex:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/github/mikephil/charting/utils/Highlight;-><init>(II)V

    .line 43
    iput p3, p0, Lcom/github/mikephil/charting/utils/Highlight;->mStackIndex:I

    return-void
.end method


# virtual methods
.method public equalTo(Lcom/github/mikephil/charting/utils/Highlight;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 86
    :cond_0
    iget v1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mDataSetIndex:I

    iget v2, p1, Lcom/github/mikephil/charting/utils/Highlight;->mDataSetIndex:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mXIndex:I

    iget v2, p1, Lcom/github/mikephil/charting/utils/Highlight;->mXIndex:I

    if-ne v1, v2, :cond_1

    .line 87
    iget v1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mStackIndex:I

    iget p1, p1, Lcom/github/mikephil/charting/utils/Highlight;->mStackIndex:I

    if-ne v1, p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    return v0
.end method

.method public getDataSetIndex()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/github/mikephil/charting/utils/Highlight;->mDataSetIndex:I

    return v0
.end method

.method public getStackIndex()I
    .locals 1

    .line 71
    iget v0, p0, Lcom/github/mikephil/charting/utils/Highlight;->mStackIndex:I

    return v0
.end method

.method public getXIndex()I
    .locals 1

    .line 61
    iget v0, p0, Lcom/github/mikephil/charting/utils/Highlight;->mXIndex:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Highlight, xIndex: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mXIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", dataSetIndex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mDataSetIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", stackIndex (only stacked barentry): "

    .line 97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/github/mikephil/charting/utils/Highlight;->mStackIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
