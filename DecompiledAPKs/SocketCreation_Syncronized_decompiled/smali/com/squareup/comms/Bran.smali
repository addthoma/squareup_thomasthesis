.class public interface abstract Lcom/squareup/comms/Bran;
.super Ljava/lang/Object;
.source "Bran.java"


# virtual methods
.method public abstract abortSecureSession(Lcom/squareup/comms/protos/seller/AbortSecureSession;)V
.end method

.method public abstract activateEGiftCardBranState(Lcom/squareup/comms/protos/seller/ActivateEGiftCardBranState;)V
.end method

.method public abstract activitySearchCardRead(Lcom/squareup/comms/protos/seller/ActivitySearchCardRead;)V
.end method

.method public abstract activitySearchDisableContactlessField(Lcom/squareup/comms/protos/seller/ActivitySearchDisableContactlessField;)V
.end method

.method public abstract activitySearchEnableContactlessField(Lcom/squareup/comms/protos/seller/ActivitySearchEnableContactlessField;)V
.end method

.method public abstract blockingCartDiff(Lcom/squareup/comms/protos/seller/BlockingCartDiff;)V
.end method

.method public abstract branDisplay(Lcom/squareup/comms/protos/seller/BranDisplay;)V
.end method

.method public abstract brightnessSetting(Lcom/squareup/comms/protos/seller/BrightnessSetting;)V
.end method

.method public abstract cancelProcessingDippedCardForCardOnFile(Lcom/squareup/comms/protos/seller/CancelProcessingDippedCardForCardOnFile;)V
.end method

.method public abstract cartShowCartBanner(Lcom/squareup/comms/protos/seller/CartShowCartBanner;)V
.end method

.method public abstract cashOrOtherShowAuthorizingCashOrOther(Lcom/squareup/comms/protos/seller/CashOrOtherShowAuthorizingCashOrOther;)V
.end method

.method public abstract cashPaymentApproved(Lcom/squareup/comms/protos/seller/CashPaymentApproved;)V
.end method

.method public abstract checkingGiftCardBalance(Lcom/squareup/comms/protos/seller/CheckingGiftCardBalance;)V
.end method

.method public abstract configuringSwipedGiftCard(Lcom/squareup/comms/protos/seller/ConfiguringSwipedGiftCard;)V
.end method

.method public abstract configuringTender(Lcom/squareup/comms/protos/seller/ConfiguringTender;)V
.end method

.method public abstract discount(Lcom/squareup/comms/protos/seller/Discount;)V
.end method

.method public abstract dismissBranDisplay(Lcom/squareup/comms/protos/seller/DismissBranDisplay;)V
.end method

.method public abstract dismissDisplayingCustomerDetails(Lcom/squareup/comms/protos/seller/DismissDisplayingCustomerDetails;)V
.end method

.method public abstract displayBanner(Lcom/squareup/comms/protos/seller/DisplayBanner;)V
.end method

.method public abstract displayCardOnFileAuth(Lcom/squareup/comms/protos/seller/DisplayCardOnFileAuth;)V
.end method

.method public abstract displayCardOnFileSpinner(Lcom/squareup/comms/protos/seller/DisplayCardOnFileSpinner;)V
.end method

.method public abstract displayCart(Lcom/squareup/comms/protos/seller/DisplayCart;)V
.end method

.method public abstract displayCustomerDetails(Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;)V
.end method

.method public abstract displayGiftCardActivation(Lcom/squareup/comms/protos/seller/DisplayGiftCardActivation;)V
.end method

.method public abstract displayGiftCardBalance(Lcom/squareup/comms/protos/seller/DisplayGiftCardBalance;)V
.end method

.method public abstract displayGiftCardBalanceCheck(Lcom/squareup/comms/protos/seller/DisplayGiftCardBalanceCheck;)V
.end method

.method public abstract displayIdleScreen(Lcom/squareup/comms/protos/seller/DisplayIdleScreen;)V
.end method

.method public abstract displayItem(Lcom/squareup/comms/protos/seller/DisplayItem;)V
.end method

.method public abstract displayLoyaltyEarnRewards(Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;)V
.end method

.method public abstract displayLoyaltyEarnedReward(Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;)V
.end method

.method public abstract displayLoyaltyLoadingScreen(Lcom/squareup/comms/protos/seller/DisplayLoyaltyLoadingScreen;)V
.end method

.method public abstract displayLoyaltySignup(Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;)V
.end method

.method public abstract displaySellerCanceling(Lcom/squareup/comms/protos/seller/DisplaySellerCanceling;)V
.end method

.method public abstract displayWaitingForCardOnFile(Lcom/squareup/comms/protos/seller/DisplayWaitingForCardOnFile;)V
.end method

.method public abstract displayWaitingForSwipeCardOnFile(Lcom/squareup/comms/protos/seller/DisplayWaitingForSwipeCardOnFile;)V
.end method

.method public abstract displayWaitingForSwipeOrDipCardOnFile(Lcom/squareup/comms/protos/seller/DisplayWaitingForSwipeOrDipCardOnFile;)V
.end method

.method public abstract dropTenders(Lcom/squareup/comms/protos/seller/DropTenders;)V
.end method

.method public abstract emvPaymentFailureResult(Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;)V
.end method

.method public abstract emvPaymentPartiallyApproved(Lcom/squareup/comms/protos/seller/EmvPaymentPartiallyApproved;)V
.end method

.method public abstract emvPaymentRemoteError(Lcom/squareup/comms/protos/seller/EmvPaymentRemoteError;)V
.end method

.method public abstract emvPaymentSuccessResult(Lcom/squareup/comms/protos/seller/EmvPaymentSuccessResult;)V
.end method

.method public abstract emvRetryAuthorize(Lcom/squareup/comms/protos/seller/EmvRetryAuthorize;)V
.end method

.method public abstract enteringActivitySearch(Lcom/squareup/comms/protos/seller/EnteringActivitySearch;)V
.end method

.method public abstract enteringCustomersApplet(Lcom/squareup/comms/protos/seller/EnteringCustomersApplet;)V
.end method

.method public abstract enteringEarnRewards(Lcom/squareup/comms/protos/seller/EnteringEarnRewards;)V
.end method

.method public abstract enteringOpenTickets(Lcom/squareup/comms/protos/seller/EnteringOpenTickets;)V
.end method

.method public abstract enteringOrderTicketName(Lcom/squareup/comms/protos/seller/EnteringOrderTicketName;)V
.end method

.method public abstract enteringPaymentDone(Lcom/squareup/comms/protos/seller/EnteringPaymentDone;)V
.end method

.method public abstract enteringReceipt(Lcom/squareup/comms/protos/seller/EnteringReceipt;)V
.end method

.method public abstract enteringSaveCard(Lcom/squareup/comms/protos/seller/EnteringSaveCard;)V
.end method

.method public abstract enteringSaveCustomer(Lcom/squareup/comms/protos/seller/EnteringSaveCustomer;)V
.end method

.method public abstract enteringTenderFromSaveCustomer(Lcom/squareup/comms/protos/seller/EnteringTenderFromSaveCustomer;)V
.end method

.method public abstract errorCheckingGiftCardBalance(Lcom/squareup/comms/protos/seller/ErrorCheckingGiftCardBalance;)V
.end method

.method public abstract exitingActivitySearch(Lcom/squareup/comms/protos/seller/ExitingActivitySearch;)V
.end method

.method public abstract exitingCustomersApplet(Lcom/squareup/comms/protos/seller/ExitingCustomersApplet;)V
.end method

.method public abstract goToCartMonitor(Lcom/squareup/comms/protos/seller/GoToCartMonitor;)V
.end method

.method public abstract hideSellerCanceling(Lcom/squareup/comms/protos/seller/HideSellerCanceling;)V
.end method

.method public abstract idleScreenConfig(Lcom/squareup/comms/protos/seller/IdleScreenConfig;)V
.end method

.method public abstract imageResponse(Lcom/squareup/comms/protos/seller/ImageResponse;)V
.end method

.method public abstract loadingSwipedGiftCard(Lcom/squareup/comms/protos/seller/LoadingSwipedGiftCard;)V
.end method

.method public abstract loyaltyCartBanner(Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;)V
.end method

.method public abstract merchantData(Lcom/squareup/comms/protos/seller/MerchantData;)V
.end method

.method public abstract onAppCrashLogSent(Lcom/squareup/comms/protos/seller/OnAppCrashLogSent;)V
.end method

.method public abstract onCapturePaymentForPasscodeLock(Lcom/squareup/comms/protos/seller/OnCapturePaymentForPasscodeLock;)V
.end method

.method public abstract onCapturePaymentForSleep(Lcom/squareup/comms/protos/seller/OnCapturePaymentForSleep;)V
.end method

.method public abstract onConfirmCancelPayment(Lcom/squareup/comms/protos/seller/OnConfirmCancelPayment;)V
.end method

.method public abstract onCoreDumpDataSent(Lcom/squareup/comms/protos/seller/OnCoreDumpDataSent;)V
.end method

.method public abstract onCreateOrEditCustomer(Lcom/squareup/comms/protos/seller/OnCreateOrEditCustomer;)V
.end method

.method public abstract onDisplayCardOnFileEmail(Lcom/squareup/comms/protos/seller/OnDisplayCardOnFileEmail;)V
.end method

.method public abstract onDisplayCardOnFilePostalCode(Lcom/squareup/comms/protos/seller/OnDisplayCardOnFilePostalCode;)V
.end method

.method public abstract onReceiptAcknowledged(Lcom/squareup/comms/protos/seller/OnReceiptAcknowledged;)V
.end method

.method public abstract onTamperDataSent(Lcom/squareup/comms/protos/seller/OnTamperDataSent;)V
.end method

.method public abstract passcodeLock(Lcom/squareup/comms/protos/seller/PasscodeLock;)V
.end method

.method public abstract paymentCancellationAcknowledged(Lcom/squareup/comms/protos/seller/PaymentCancellationAcknowledged;)V
.end method

.method public abstract paymentError(Lcom/squareup/comms/protos/seller/PaymentError;)V
.end method

.method public abstract performAutoCapture(Lcom/squareup/comms/protos/seller/PerformAutoCapture;)V
.end method

.method public abstract postTransactionButtonClicked(Lcom/squareup/comms/protos/seller/PostTransactionButtonClicked;)V
.end method

.method public abstract processSecureSessionMessageFromServer(Lcom/squareup/comms/protos/seller/ProcessSecureSessionMessageFromServer;)V
.end method

.method public abstract promptForPayment(Lcom/squareup/comms/protos/seller/PromptForPayment;)V
.end method

.method public abstract receiptOptions(Lcom/squareup/comms/protos/seller/ReceiptOptions;)V
.end method

.method public abstract removeCompletedTender(Lcom/squareup/comms/protos/seller/RemoveCompletedTender;)V
.end method

.method public abstract resetCustomerInformation(Lcom/squareup/comms/protos/seller/ResetCustomerInformation;)V
.end method

.method public abstract saveCardOnFileFailure(Lcom/squareup/comms/protos/seller/SaveCardOnFileFailure;)V
.end method

.method public abstract saveCardOnFileSuccess(Lcom/squareup/comms/protos/seller/SaveCardOnFileSuccess;)V
.end method

.method public abstract screenState(Lcom/squareup/comms/protos/seller/ScreenState;)V
.end method

.method public abstract sellerCreatingCustomer(Lcom/squareup/comms/protos/seller/SellerCreatingCustomer;)V
.end method

.method public abstract sellerDevice(Lcom/squareup/comms/protos/seller/SellerDevice;)V
.end method

.method public abstract sellerSkippingSignature(Lcom/squareup/comms/protos/seller/SellerSkippingSignature;)V
.end method

.method public abstract sellerSkippingTip(Lcom/squareup/comms/protos/seller/SellerSkippingTip;)V
.end method

.method public abstract sendBugReport(Lcom/squareup/comms/protos/seller/SendBugReport;)V
.end method

.method public abstract sendMessageToReader(Lcom/squareup/comms/protos/seller/SendMessageToReader;)V
.end method

.method public abstract signatureRequirements(Lcom/squareup/comms/protos/seller/SignatureRequirements;)V
.end method

.method public abstract sleep(Lcom/squareup/comms/protos/seller/Sleep;)V
.end method

.method public abstract startProcessingDippedCardForCardOnFile(Lcom/squareup/comms/protos/seller/StartProcessingDippedCardForCardOnFile;)V
.end method

.method public abstract swipePaymentApproved(Lcom/squareup/comms/protos/seller/SwipePaymentApproved;)V
.end method

.method public abstract swipePaymentDeclined(Lcom/squareup/comms/protos/seller/SwipePaymentDeclined;)V
.end method

.method public abstract swipePaymentPartiallyApproved(Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;)V
.end method

.method public abstract swipePaymentRemoteError(Lcom/squareup/comms/protos/seller/SwipePaymentRemoteError;)V
.end method

.method public abstract swipeRetryAuthorize(Lcom/squareup/comms/protos/seller/SwipeRetryAuthorize;)V
.end method

.method public abstract tax(Lcom/squareup/comms/protos/seller/Tax;)V
.end method

.method public abstract tenderCash(Lcom/squareup/comms/protos/seller/TenderCash;)V
.end method

.method public abstract tenderInstrumentOnFile(Lcom/squareup/comms/protos/seller/TenderInstrumentOnFile;)V
.end method

.method public abstract tenderKeyedCard(Lcom/squareup/comms/protos/seller/TenderKeyedCard;)V
.end method

.method public abstract tenderOther(Lcom/squareup/comms/protos/seller/TenderOther;)V
.end method

.method public abstract tenderShowAboveCreditCardMaximum(Lcom/squareup/comms/protos/seller/TenderShowAboveCreditCardMaximum;)V
.end method

.method public abstract tenderShowBelowCreditCardMinimum(Lcom/squareup/comms/protos/seller/TenderShowBelowCreditCardMinimum;)V
.end method

.method public abstract tenderShowOffline(Lcom/squareup/comms/protos/seller/TenderShowOffline;)V
.end method

.method public abstract tenderShowOfflineAboveCreditCardMaximum(Lcom/squareup/comms/protos/seller/TenderShowOfflineAboveCreditCardMaximum;)V
.end method

.method public abstract tenderShowOnlinePromptForPayment(Lcom/squareup/comms/protos/seller/TenderShowOnlinePromptForPayment;)V
.end method

.method public abstract tenderShowWaitingForNextTender(Lcom/squareup/comms/protos/seller/TenderShowWaitingForNextTender;)V
.end method

.method public abstract tenderShowZeroDollarPayment(Lcom/squareup/comms/protos/seller/TenderShowZeroDollarPayment;)V
.end method

.method public abstract tenderSwipedCard(Lcom/squareup/comms/protos/seller/TenderSwipedCard;)V
.end method

.method public abstract time(Lcom/squareup/comms/protos/seller/Time;)V
.end method

.method public abstract tipOption(Lcom/squareup/comms/protos/seller/TipOption;)V
.end method

.method public abstract tipRequirements(Lcom/squareup/comms/protos/seller/TipRequirements;)V
.end method

.method public abstract updateBlockingCartDiff(Lcom/squareup/comms/protos/seller/UpdateBlockingCartDiff;)V
.end method

.method public abstract updateLoyaltyCartBanner(Lcom/squareup/comms/protos/seller/UpdateLoyaltyCartBanner;)V
.end method
