.class Lcom/squareup/comms/RemoteBusImpl$2;
.super Ljava/lang/Object;
.source "RemoteBusImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/RemoteBusImpl;->post(Lcom/squareup/wire/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/RemoteBusImpl;

.field final synthetic val$message:Lcom/squareup/wire/Message;


# direct methods
.method constructor <init>(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/wire/Message;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$2;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    iput-object p2, p0, Lcom/squareup/comms/RemoteBusImpl$2;->val$message:Lcom/squareup/wire/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 67
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$2;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$200(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/Serializer;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusImpl$2;->val$message:Lcom/squareup/wire/Message;

    invoke-interface {v0, v1}, Lcom/squareup/comms/Serializer;->serialize(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/comms/RemoteBusImpl$2;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v1}, Lcom/squareup/comms/RemoteBusImpl;->access$300(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/net/Channel;

    move-result-object v1

    array-length v2, v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/squareup/comms/net/Channel;->send([BII)V

    return-void
.end method
