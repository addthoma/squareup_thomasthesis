.class Lcom/squareup/comms/RemoteBusImpl$ChannelListener;
.super Ljava/lang/Object;
.source "RemoteBusImpl.java"

# interfaces
.implements Lcom/squareup/comms/net/ChannelCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/RemoteBusImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChannelListener"
.end annotation


# instance fields
.field private state:Lcom/squareup/comms/RemoteBusImpl$State;

.field final synthetic this$0:Lcom/squareup/comms/RemoteBusImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/comms/RemoteBusImpl;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object p1, Lcom/squareup/comms/RemoteBusImpl$State;->START:Lcom/squareup/comms/RemoteBusImpl$State;

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->state:Lcom/squareup/comms/RemoteBusImpl$State;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusImpl$1;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;-><init>(Lcom/squareup/comms/RemoteBusImpl;)V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;)Lcom/squareup/comms/RemoteBusImpl$State;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->state:Lcom/squareup/comms/RemoteBusImpl$State;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;
    .locals 0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->transitionTo(Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;

    move-result-object p0

    return-object p0
.end method

.method private transitionTo(Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 143
    iget-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->state:Lcom/squareup/comms/RemoteBusImpl$State;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    aput-object p2, v0, p1

    const-string p1, "%s: %s -> %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->state:Lcom/squareup/comms/RemoteBusImpl$State;

    .line 145
    iput-object p2, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->state:Lcom/squareup/comms/RemoteBusImpl$State;

    return-object p1
.end method


# virtual methods
.method public onConnected(Lcom/squareup/comms/net/Device;)V
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0, p1}, Lcom/squareup/comms/RemoteBusImpl;->access$402(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/net/Device;)Lcom/squareup/comms/net/Device;

    .line 84
    sget-object v0, Lcom/squareup/comms/RemoteBusImpl$State;->CONNECTED:Lcom/squareup/comms/RemoteBusImpl$State;

    const-string v1, "RemoteBusImpl.onConnected"

    invoke-direct {p0, v1, v0}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->transitionTo(Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;

    .line 85
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$200(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/Serializer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/Serializer;->reset()V

    .line 89
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$100(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/HealthChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    .line 90
    invoke-static {v1}, Lcom/squareup/comms/RemoteBusImpl;->access$500(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/common/IoThread;

    move-result-object v2

    new-instance v3, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;-><init>(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;Lcom/squareup/comms/net/Device;)V

    invoke-static {v2, v3}, Lcom/squareup/comms/net/Callbacks;->serializedHealthCheckerCallback(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/HealthChecker$Callback;)Lcom/squareup/comms/HealthChecker$Callback;

    move-result-object p1

    .line 89
    invoke-interface {v0, v1, p1}, Lcom/squareup/comms/HealthChecker;->start(Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/HealthChecker$Callback;)V

    return-void
.end method

.method public onDisconnected()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "RemoteBusImpl stopping HealthChecker from onDisconnected"

    .line 111
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$100(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/HealthChecker;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/HealthChecker;->stop()V

    .line 114
    sget-object v0, Lcom/squareup/comms/RemoteBusImpl$State;->DISCONNECTED:Lcom/squareup/comms/RemoteBusImpl$State;

    const-string v1, "RemoteBusImpl.onDisconnected"

    invoke-direct {p0, v1, v0}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->transitionTo(Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/comms/RemoteBusImpl$State;->HEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    if-ne v0, v1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$800(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/comms/RemoteBusConnectionImpl;->onDisconnected()V

    .line 116
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/comms/RemoteBusImpl;->access$802(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusConnectionImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Comms error"

    .line 139
    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onReceive([BII)V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$200(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/Serializer;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/comms/Serializer;->consume([BII)Ljava/util/List;

    move-result-object p1

    .line 123
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    .line 124
    iget-object p3, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {p3}, Lcom/squareup/comms/RemoteBusImpl;->access$100(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/HealthChecker;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-interface {p3, v0, p2}, Lcom/squareup/comms/HealthChecker;->tryConsumeMessage(Lcom/squareup/comms/MessagePoster;Lcom/squareup/wire/Message;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    iget-object p3, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->state:Lcom/squareup/comms/RemoteBusImpl$State;

    sget-object v0, Lcom/squareup/comms/RemoteBusImpl$State;->HEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    if-eq p3, v0, :cond_1

    .line 129
    sget-object p3, Lcom/squareup/comms/RemoteBusImpl$State;->HEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    const-string v0, "RemoteBusImpl.onReceive"

    invoke-direct {p0, v0, p3}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->transitionTo(Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;

    .line 130
    iget-object p3, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    new-instance v0, Lcom/squareup/comms/RemoteBusConnectionImpl;

    invoke-static {p3}, Lcom/squareup/comms/RemoteBusImpl;->access$900(Lcom/squareup/comms/RemoteBusImpl;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, p3, v1}, Lcom/squareup/comms/RemoteBusConnectionImpl;-><init>(Lcom/squareup/comms/MessagePoster;Ljava/util/concurrent/Executor;)V

    invoke-static {p3, v0}, Lcom/squareup/comms/RemoteBusImpl;->access$802(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusConnectionImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    .line 131
    iget-object p3, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {p3}, Lcom/squareup/comms/RemoteBusImpl;->access$1000(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/ConnectionListener;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$800(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v1}, Lcom/squareup/comms/RemoteBusImpl;->access$400(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/net/Device;

    move-result-object v1

    invoke-interface {p3, v0, v1}, Lcom/squareup/comms/ConnectionListener;->onConnected(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V

    .line 134
    :cond_1
    iget-object p3, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {p3}, Lcom/squareup/comms/RemoteBusImpl;->access$800(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/squareup/comms/RemoteBusConnectionImpl;->onNext(Lcom/squareup/wire/Message;)V

    goto :goto_0

    :cond_2
    return-void
.end method
