.class public final Lcom/squareup/comms/net/socket/ServerConnectionFactory;
.super Ljava/lang/Object;
.source "ServerConnectionFactory.java"

# interfaces
.implements Lcom/squareup/comms/net/ConnectionFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;
    }
.end annotation


# static fields
.field private static final MIN_RECONNECT_TIMEOUT_MS:I = 0x64


# instance fields
.field private callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

.field private closed:Z

.field private host:Ljava/lang/String;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;

.field private listenSocket:Ljava/nio/channels/ServerSocketChannel;

.field private final maxReconnectTimeoutMs:I

.field private port:I

.field private started:Z


# direct methods
.method public constructor <init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;II)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;II)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 65
    iput-object p2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->host:Ljava/lang/String;

    .line 66
    iput p3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->port:I

    .line 67
    iput p4, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->maxReconnectTimeoutMs:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Z
    .locals 0

    .line 24
    iget-boolean p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->closed:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->host:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->port:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->listenSocket:Ljava/nio/channels/ServerSocketChannel;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/comms/net/socket/ServerConnectionFactory;Ljava/nio/channels/ServerSocketChannel;)Ljava/nio/channels/ServerSocketChannel;
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->listenSocket:Ljava/nio/channels/ServerSocketChannel;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/comms/net/socket/ServerConnectionFactory;I)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->createAcceptor(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Lcom/squareup/comms/common/IoThread;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-object p0
.end method

.method private createAcceptor(I)V
    .locals 3

    .line 109
    iget-boolean v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->closed:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Ignoring acceptor request, factory is closed"

    .line 110
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 114
    :cond_0
    iget v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->maxReconnectTimeoutMs:I

    mul-int/lit8 v1, p1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 116
    new-instance v1, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;-><init>(Lcom/squareup/comms/net/socket/ServerConnectionFactory;ILcom/squareup/comms/net/ConnectionFactory$Callback;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->ioThread:Lcom/squareup/comms/common/IoThread;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/comms/common/IoThread;->schedule(Ljava/lang/Runnable;ILjava/util/concurrent/TimeUnit;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 87
    iget-boolean v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->closed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 88
    iput-boolean v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->closed:Z

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->listenSocket:Ljava/nio/channels/ServerSocketChannel;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->listenSocket:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->close()V

    const/4 v0, 0x0

    .line 95
    iput-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->listenSocket:Ljava/nio/channels/ServerSocketChannel;

    :cond_1
    const-string v0, "Server connection factory shut down"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 97
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 99
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public requestConnection()V
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->started:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 81
    iput-boolean v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->started:Z

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0, v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->createAcceptor(I)V

    return-void
.end method

.method public setCallback(Lcom/squareup/comms/net/ConnectionFactory$Callback;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    return-void
.end method
