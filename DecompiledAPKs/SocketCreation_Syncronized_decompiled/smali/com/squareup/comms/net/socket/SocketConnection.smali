.class final Lcom/squareup/comms/net/socket/SocketConnection;
.super Ljava/lang/Object;
.source "SocketConnection.java"

# interfaces
.implements Lcom/squareup/comms/net/Connection;


# static fields
.field private static final BUFFER_SIZE:I = 0x1e8480


# instance fields
.field private callback:Lcom/squareup/comms/net/ConnectionCallback;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;

.field private final readBuffer:Ljava/nio/ByteBuffer;

.field private final sendQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final socket:Ljava/nio/channels/SocketChannel;


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;Ljava/nio/channels/SocketChannel;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 45
    iput-object p2, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    .line 46
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->sendQueue:Ljava/util/Queue;

    .line 47
    new-instance p1, Lcom/squareup/comms/net/ConnectionCallback$Null;

    invoke-direct {p1}, Lcom/squareup/comms/net/ConnectionCallback$Null;-><init>()V

    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->callback:Lcom/squareup/comms/net/ConnectionCallback;

    const p1, 0x1e8480

    .line 48
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->readBuffer:Ljava/nio/ByteBuffer;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/util/Queue;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->sendQueue:Ljava/util/Queue;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/channels/SocketChannel;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/comms/net/socket/SocketConnection;)Lcom/squareup/comms/net/ConnectionCallback;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->callback:Lcom/squareup/comms/net/ConnectionCallback;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/comms/net/socket/SocketConnection;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/comms/net/socket/SocketConnection;->postReadAndWrite()V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/ByteBuffer;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->readBuffer:Ljava/nio/ByteBuffer;

    return-object p0
.end method

.method private postReadAndWrite()V
    .locals 4

    .line 105
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->ioThread:Lcom/squareup/comms/common/IoThread;

    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    iget-object v2, p0, Lcom/squareup/comms/net/socket/SocketConnection;->sendQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x5

    :goto_0
    new-instance v3, Lcom/squareup/comms/net/socket/SocketConnection$2;

    invoke-direct {v3, p0}, Lcom/squareup/comms/net/socket/SocketConnection$2;-><init>(Lcom/squareup/comms/net/socket/SocketConnection;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/comms/common/IoThread;->register(Ljava/nio/channels/SelectableChannel;ILcom/squareup/comms/common/IoCompletion;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/socket/-$$Lambda$SocketConnection$9g0TmN1Ijo-QpDBUkbqu2G0cQzg;

    invoke-direct {v1, p0}, Lcom/squareup/comms/net/socket/-$$Lambda$SocketConnection$9g0TmN1Ijo-QpDBUkbqu2G0cQzg;-><init>(Lcom/squareup/comms/net/socket/SocketConnection;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->postAndWait(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$close$0$SocketConnection()V
    .locals 2

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownInput()V

    .line 87
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->shutdownOutput()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :try_start_1
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    :catch_0
    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    :goto_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string v1, "SocketConnection closed: %s"

    .line 95
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public send([BII)V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/socket/SocketConnection$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/comms/net/socket/SocketConnection$1;-><init>(Lcom/squareup/comms/net/socket/SocketConnection;[BII)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setCallback(Lcom/squareup/comms/net/ConnectionCallback;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection;->callback:Lcom/squareup/comms/net/ConnectionCallback;

    return-void
.end method

.method public start()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting read loop"

    .line 56
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/comms/net/socket/SocketConnection;->postReadAndWrite()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
