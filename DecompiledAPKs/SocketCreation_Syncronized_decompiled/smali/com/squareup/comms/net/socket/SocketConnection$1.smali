.class Lcom/squareup/comms/net/socket/SocketConnection$1;
.super Ljava/lang/Object;
.source "SocketConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/socket/SocketConnection;->send([BII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/socket/SocketConnection;

.field final synthetic val$buffer:[B

.field final synthetic val$count:I

.field final synthetic val$offset:I


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/socket/SocketConnection;[BII)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    iput-object p2, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->val$buffer:[B

    iput p3, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->val$offset:I

    iput p4, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->val$count:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->val$buffer:[B

    iget v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->val$offset:I

    iget v2, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->val$count:I

    invoke-static {v0, v1, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$000(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$100(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/channels/SocketChannel;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 69
    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$200(Lcom/squareup/comms/net/socket/SocketConnection;)Lcom/squareup/comms/net/ConnectionCallback;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/comms/net/ConnectionCallback;->onError(Ljava/lang/Exception;)V

    return-void

    .line 74
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-lez v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$000(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$1;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$300(Lcom/squareup/comms/net/socket/SocketConnection;)V

    :cond_1
    return-void
.end method
