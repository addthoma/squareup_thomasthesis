.class final Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;
.super Ljava/lang/Object;
.source "ServerConnectionFactory.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/socket/ServerConnectionFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Acceptor"
.end annotation


# instance fields
.field private final callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

.field private final retry:I

.field final synthetic this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/socket/ServerConnectionFactory;ILcom/squareup/comms/net/ConnectionFactory$Callback;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput p2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->retry:I

    .line 132
    iput-object p3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)Lcom/squareup/comms/net/ConnectionFactory$Callback;
    .locals 0

    .line 126
    iget-object p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->postAccept()V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)I
    .locals 0

    .line 126
    iget p0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->retry:I

    return p0
.end method

.method private postAccept()V
    .locals 4

    .line 166
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$500(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Lcom/squareup/comms/common/IoThread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;

    move-result-object v1

    new-instance v2, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;

    invoke-direct {v2, p0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor$1;-><init>(Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;)V

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/comms/common/IoThread;->register(Ljava/nio/channels/SelectableChannel;ILcom/squareup/comms/common/IoCompletion;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 136
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$000(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v2}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$100(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    .line 144
    invoke-static {v3}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$200(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/net/InetSocketAddress;-><init>(I)V

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    .line 145
    invoke-static {v3}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$100(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v4}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$200(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 147
    :goto_0
    iget-object v3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$302(Lcom/squareup/comms/net/socket/ServerConnectionFactory;Ljava/nio/channels/ServerSocketChannel;)Ljava/nio/channels/ServerSocketChannel;

    .line 148
    iget-object v3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v3}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 149
    iget-object v3, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v3}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    const-string v2, "Server starting on port: %d"

    new-array v3, v1, [Ljava/lang/Object;

    .line 150
    iget-object v4, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    invoke-static {v4}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/nio/channels/ServerSocketChannel;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-direct {p0}, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->postAccept()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-array v3, v1, [Ljava/lang/Object;

    .line 153
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    const-string v0, "Failed to start acceptor: [%s], retrying ..."

    invoke-static {v0, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    iget v2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->retry:I

    add-int/2addr v2, v1

    invoke-static {v0, v2}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$400(Lcom/squareup/comms/net/socket/ServerConnectionFactory;I)V

    :goto_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 159
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 160
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    .line 161
    invoke-static {v2}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$100(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ServerConnectionFactory$Acceptor;->this$0:Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    .line 162
    invoke-static {v2}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;->access$200(Lcom/squareup/comms/net/socket/ServerConnectionFactory;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string v2, "Acceptor[%d]: %s:%d"

    .line 159
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
