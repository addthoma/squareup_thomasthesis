.class final Lcom/squareup/comms/net/Callbacks$1;
.super Ljava/lang/Object;
.source "Callbacks.java"

# interfaces
.implements Lcom/squareup/comms/ConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Callbacks;->serializedConnectionListener(Ljava/util/concurrent/Executor;Lcom/squareup/comms/ConnectionListener;)Lcom/squareup/comms/ConnectionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$delegate:Lcom/squareup/comms/ConnectionListener;

.field final synthetic val$executor:Ljava/util/concurrent/Executor;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/squareup/comms/ConnectionListener;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/squareup/comms/net/Callbacks$1;->val$executor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/squareup/comms/net/Callbacks$1;->val$delegate:Lcom/squareup/comms/ConnectionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$1;->val$executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$1$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/comms/net/Callbacks$1$1;-><init>(Lcom/squareup/comms/net/Callbacks$1;Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
