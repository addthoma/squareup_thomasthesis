.class Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Lcom/squareup/comms/net/ConnectionFactory$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionFactoryListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Channel;


# direct methods
.method private constructor <init>(Lcom/squareup/comms/net/Channel;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Channel$1;)V
    .locals 0

    .line 118
    invoke-direct {p0, p1}, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;-><init>(Lcom/squareup/comms/net/Channel;)V

    return-void
.end method


# virtual methods
.method public onNewConnection(Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "New connection established: %s"

    .line 120
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    iget-object v1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v1}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v1

    if-eqz v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 123
    iget-object v1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v1}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "Closing connection: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/Connection;->close()V

    .line 125
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ChannelCallback;->onDisconnected()V

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0, p1}, Lcom/squareup/comms/net/Channel;->access$402(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)Lcom/squareup/comms/net/Connection;

    .line 129
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    .line 130
    invoke-static {v1}, Lcom/squareup/comms/net/Channel;->access$100(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/common/IoThread;

    move-result-object v1

    new-instance v2, Lcom/squareup/comms/net/Channel$ConnectionListener;

    iget-object v3, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-direct {v2, v3, p1}, Lcom/squareup/comms/net/Channel$ConnectionListener;-><init>(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)V

    .line 129
    invoke-static {v1, v2}, Lcom/squareup/comms/net/Callbacks;->serializedConnectionCallback(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionCallback;)Lcom/squareup/comms/net/ConnectionCallback;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/comms/net/Connection;->setCallback(Lcom/squareup/comms/net/ConnectionCallback;)V

    .line 133
    iget-object p1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {p1}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/comms/net/ChannelCallback;->onConnected(Lcom/squareup/comms/net/Device;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {p1}, Lcom/squareup/comms/net/Channel;->access$500(Lcom/squareup/comms/net/Channel;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 136
    iget-object p1, p0, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {p1}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/comms/net/Connection;->start()V

    :cond_1
    return-void
.end method
