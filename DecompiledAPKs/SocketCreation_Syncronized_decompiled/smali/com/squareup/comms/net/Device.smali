.class public Lcom/squareup/comms/net/Device;
.super Ljava/lang/Object;
.source "Device.java"


# static fields
.field private static final ADDRESS_VOID:Ljava/lang/String; = "void_address"

.field private static final NAME_VOID:Ljava/lang/String; = "void_name"

.field public static final VOID_DEVICE:Lcom/squareup/comms/net/Device;


# instance fields
.field public final address:Ljava/lang/String;

.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 14
    new-instance v0, Lcom/squareup/comms/net/Device;

    const-string v1, "void_address"

    const-string v2, "void_name"

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/net/Device;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/comms/net/Device;->VOID_DEVICE:Lcom/squareup/comms/net/Device;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/comms/net/Device;->address:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/squareup/comms/net/Device;->name:Ljava/lang/String;

    return-void
.end method
