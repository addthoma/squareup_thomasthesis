.class Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;
.super Ljava/lang/Object;
.source "Callbacks.java"

# interfaces
.implements Lcom/squareup/comms/net/ConnectionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/Callbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SerialConnectionCallback"
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/comms/net/ConnectionCallback;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionCallback;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 97
    iput-object p2, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->delegate:Lcom/squareup/comms/net/ConnectionCallback;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;)Lcom/squareup/comms/net/ConnectionCallback;
    .locals 0

    .line 91
    iget-object p0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->delegate:Lcom/squareup/comms/net/ConnectionCallback;

    return-object p0
.end method


# virtual methods
.method public onDisconnect()V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$3;

    invoke-direct {v1, p0}, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$3;-><init>(Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$2;-><init>(Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onReceive([BII)V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback$1;-><init>(Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;[BII)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method
