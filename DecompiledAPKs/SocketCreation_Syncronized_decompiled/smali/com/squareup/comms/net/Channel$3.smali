.class Lcom/squareup/comms/net/Channel$3;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Channel;->send([BII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Channel;

.field final synthetic val$buffer:[B

.field final synthetic val$count:I

.field final synthetic val$offset:I


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Channel;[BII)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/comms/net/Channel$3;->this$0:Lcom/squareup/comms/net/Channel;

    iput-object p2, p0, Lcom/squareup/comms/net/Channel$3;->val$buffer:[B

    iput p3, p0, Lcom/squareup/comms/net/Channel$3;->val$offset:I

    iput p4, p0, Lcom/squareup/comms/net/Channel$3;->val$count:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 88
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$3;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$3;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Can\'t send messages, not connected"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/comms/net/ChannelCallback;->onError(Ljava/lang/Exception;)V

    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$3;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/Channel$3;->val$buffer:[B

    iget v2, p0, Lcom/squareup/comms/net/Channel$3;->val$offset:I

    iget v3, p0, Lcom/squareup/comms/net/Channel$3;->val$count:I

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/comms/net/Connection;->send([BII)V

    return-void
.end method
