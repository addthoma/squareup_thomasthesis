.class Lcom/squareup/comms/net/Channel$4;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Channel;->disconnect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Channel;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Channel;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$500(Lcom/squareup/comms/net/Channel;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/Connection;->close()V

    .line 106
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/comms/net/Channel;->access$402(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)Lcom/squareup/comms/net/Connection;

    .line 107
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ChannelCallback;->onDisconnected()V

    .line 108
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$4;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$300(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ConnectionFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ConnectionFactory;->requestConnection()V

    :cond_0
    return-void
.end method
