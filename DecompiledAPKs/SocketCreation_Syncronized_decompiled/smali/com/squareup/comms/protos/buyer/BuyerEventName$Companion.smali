.class public final Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;
.super Ljava/lang/Object;
.source "BuyerEventName.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BuyerEventName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;",
        "",
        "()V",
        "ADAPTER",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
        "fromValue",
        "value",
        "",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromValue(I)Lcom/squareup/comms/protos/buyer/BuyerEventName;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    packed-switch p1, :pswitch_data_0

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 147
    :pswitch_0
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_PROCESSING_ERROR:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 146
    :pswitch_1
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_REQUEST_TAP_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 145
    :pswitch_2
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COLLISION_DETECTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 144
    :pswitch_3
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_CARD_BLOCKED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 143
    :pswitch_4
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_INTERFACE_UNAVAILABLE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 142
    :pswitch_5
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 141
    :pswitch_6
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_MUST_TAP:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 140
    :pswitch_7
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 139
    :pswitch_8
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 138
    :pswitch_9
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 137
    :pswitch_a
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 136
    :pswitch_b
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 135
    :pswitch_c
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 134
    :pswitch_d
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto/16 :goto_0

    .line 133
    :pswitch_e
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 132
    :pswitch_f
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 131
    :pswitch_10
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 130
    :pswitch_11
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 129
    :pswitch_12
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_COMPLETE_DECLINED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 128
    :pswitch_13
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 127
    :pswitch_14
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_COMPLETE_APPROVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 126
    :pswitch_15
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 125
    :pswitch_16
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 124
    :pswitch_17
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_APPLICATION_SELECTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 123
    :pswitch_18
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_APPLICATION_SELECTION:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 122
    :pswitch_19
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_SCHEME_FALLBACK:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 121
    :pswitch_1a
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 120
    :pswitch_1b
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 119
    :pswitch_1c
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 118
    :pswitch_1d
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 117
    :pswitch_1e
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_ERROR:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 116
    :pswitch_1f
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 115
    :pswitch_20
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_REMOVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 114
    :pswitch_21
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 113
    :pswitch_22
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_INSERTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 112
    :pswitch_23
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_USE_CHIP_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 111
    :pswitch_24
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_FULL_COMMS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 110
    :pswitch_25
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_READY_FOR_PAYMENTS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 109
    :pswitch_26
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_TAMPER_DATA:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0

    .line 108
    :pswitch_27
    sget-object p1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_CORE_DUMP_FOUND:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
