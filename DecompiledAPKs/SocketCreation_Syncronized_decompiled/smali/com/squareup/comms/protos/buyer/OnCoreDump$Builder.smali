.class public final Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OnCoreDump.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnCoreDump;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/OnCoreDump;",
        "Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/OnCoreDump;",
        "()V",
        "data_bytes",
        "Lokio/ByteString;",
        "hardware_serial_number",
        "",
        "key_bytes",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public data_bytes:Lokio/ByteString;

.field public hardware_serial_number:Ljava/lang/String;

.field public key_bytes:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/OnCoreDump;
    .locals 7

    .line 119
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnCoreDump;

    .line 120
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->hardware_serial_number:Ljava/lang/String;

    .line 121
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->key_bytes:Lokio/ByteString;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-eqz v2, :cond_1

    .line 122
    iget-object v6, p0, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->data_bytes:Lokio/ByteString;

    if-eqz v6, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 119
    invoke-direct {v0, v1, v2, v6, v3}, Lcom/squareup/comms/protos/buyer/OnCoreDump;-><init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v5, [Ljava/lang/Object;

    aput-object v6, v0, v4

    const-string v1, "data_bytes"

    aput-object v1, v0, v3

    .line 122
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v5, [Ljava/lang/Object;

    aput-object v2, v0, v4

    const-string v1, "key_bytes"

    aput-object v1, v0, v3

    .line 121
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->build()Lcom/squareup/comms/protos/buyer/OnCoreDump;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final data_bytes(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;
    .locals 1

    const-string v0, "data_bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->data_bytes:Lokio/ByteString;

    return-object p0
.end method

.method public final hardware_serial_number(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->hardware_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public final key_bytes(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;
    .locals 1

    const-string v0, "key_bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnCoreDump$Builder;->key_bytes:Lokio/ByteString;

    return-object p0
.end method
