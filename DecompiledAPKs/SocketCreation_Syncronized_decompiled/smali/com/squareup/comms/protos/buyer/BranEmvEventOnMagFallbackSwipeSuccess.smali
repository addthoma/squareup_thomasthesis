.class public final Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;
.super Lcom/squareup/wire/AndroidMessage;
.source "BranEmvEventOnMagFallbackSwipeSuccess.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;,
        Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBranEmvEventOnMagFallbackSwipeSuccess.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BranEmvEventOnMagFallbackSwipeSuccess.kt\ncom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess\n*L\n1#1,212:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00172\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0016\u0017B/\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ8\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bJ\u0013\u0010\u000e\u001a\u00020\u00082\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0002H\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;",
        "fallback_type",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;",
        "swipe",
        "Lcom/squareup/comms/protos/buyer/MagSwipe;",
        "has_track_1_data",
        "",
        "has_track_2_data",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion;


# instance fields
.field public final fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.FallbackSwipe$ChipCardFallbackIndicator#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final has_track_1_data:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field

.field public final has_track_2_data:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field

.field public final swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.MagSwipe#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->Companion:Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion;

    .line 153
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion$ADAPTER$1;

    .line 155
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 156
    const-class v2, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 209
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)V
    .locals 1

    const-string v0, "fallback_type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipe"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 52
    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    iput-object p2, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    iput-boolean p3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    iput-boolean p4, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 51
    sget-object p5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;-><init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    .line 98
    iget-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 99
    iget-object p2, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    .line 100
    iget-boolean p3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 101
    iget-boolean p4, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 102
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->unknownFields()Lokio/ByteString;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->copy(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;
    .locals 7

    const-string v0, "fallback_type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipe"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;-><init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 65
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 66
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    iget-boolean p1, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 75
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/buyer/MagSwipe;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    .line 57
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    .line 58
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->has_track_1_data:Ljava/lang/Boolean;

    .line 59
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->has_track_2_data:Ljava/lang/Boolean;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->newBuilder()Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 89
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fallback_type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swipe="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "has_track_1_data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_1_data:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "has_track_2_data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;->has_track_2_data:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 93
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "BranEmvEventOnMagFallbackSwipeSuccess{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    .line 94
    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    .line 93
    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
