.class public final Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;
.super Lcom/squareup/wire/AndroidMessage;
.source "RequestBranDisplayUpdate.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;,
        Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;",
        "Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00112\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0010\u0011B\u000f\u0012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004J\u0013\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0096\u0002J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u0002H\u0016J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->Companion:Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion;

    .line 53
    new-instance v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion$ADAPTER$1;

    .line 55
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 56
    const-class v2, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 78
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;-><init>(Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method

.method public synthetic constructor <init>(Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 23
    sget-object p1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->unknownFields()Lokio/ByteString;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->copy(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;

    invoke-direct {v0, p1}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 33
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 34
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;-><init>()V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;->newBuilder()Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "RequestBranDisplayUpdate{}"

    return-object v0
.end method
