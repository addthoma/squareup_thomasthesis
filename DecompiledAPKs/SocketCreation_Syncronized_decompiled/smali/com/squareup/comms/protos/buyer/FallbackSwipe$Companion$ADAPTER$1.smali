.class public final Lcom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FallbackSwipe.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/FallbackSwipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFallbackSwipe.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FallbackSwipe.kt\ncom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1\n+ 2 ProtoReader.kt\ncom/squareup/wire/ProtoReader\n*L\n1#1,180:1\n415#2,7:181\n*E\n*S KotlinDebug\n*F\n+ 1 FallbackSwipe.kt\ncom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1\n*L\n127#1,7:181\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe;",
        "decode",
        "reader",
        "Lcom/squareup/wire/ProtoReader;",
        "encode",
        "",
        "writer",
        "Lcom/squareup/wire/ProtoWriter;",
        "value",
        "encodedSize",
        "",
        "redact",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V
    .locals 0

    .line 109
    invoke-direct {p0, p1, p2}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/FallbackSwipe;
    .locals 6

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 125
    move-object v1, v0

    check-cast v1, Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    .line 126
    check-cast v0, Lcom/squareup/comms/protos/common/Card;

    .line 181
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v2

    .line 183
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 187
    invoke-virtual {p1, v2, v3}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    .line 134
    new-instance v2, Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    invoke-direct {v2, v1, v0, p1}, Lcom/squareup/comms/protos/buyer/FallbackSwipe;-><init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/common/Card;Lokio/ByteString;)V

    return-object v2

    :cond_0
    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    .line 131
    invoke-virtual {p1, v4}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 130
    :cond_1
    sget-object v0, Lcom/squareup/comms/protos/common/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/common/Card;

    goto :goto_0

    .line 129
    :cond_2
    sget-object v1, Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    goto :goto_0
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0

    .line 109
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/FallbackSwipe;)V
    .locals 3

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v0, Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 120
    sget-object v0, Lcom/squareup/comms/protos/common/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->card:Lcom/squareup/comms/protos/common/Card;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 121
    invoke-virtual {p2}, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0

    .line 109
    check-cast p2, Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/FallbackSwipe;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/comms/protos/buyer/FallbackSwipe;)I
    .locals 4

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget-object v0, Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 115
    sget-object v1, Lcom/squareup/comms/protos/common/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->card:Lcom/squareup/comms/protos/common/Card;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 109
    check-cast p1, Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1;->encodedSize(Lcom/squareup/comms/protos/buyer/FallbackSwipe;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/comms/protos/buyer/FallbackSwipe;)Lcom/squareup/comms/protos/buyer/FallbackSwipe;
    .locals 7

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iget-object v0, p1, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->card:Lcom/squareup/comms/protos/common/Card;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/comms/protos/common/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/common/Card;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v3, v0

    .line 143
    sget-object v4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    move-object v1, p1

    .line 141
    invoke-static/range {v1 .. v6}, Lcom/squareup/comms/protos/buyer/FallbackSwipe;->copy$default(Lcom/squareup/comms/protos/buyer/FallbackSwipe;Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/common/Card;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 109
    check-cast p1, Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Companion$ADAPTER$1;->redact(Lcom/squareup/comms/protos/buyer/FallbackSwipe;)Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    move-result-object p1

    return-object p1
.end method
