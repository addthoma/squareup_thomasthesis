.class public final Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SendAuthorization.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/SendAuthorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSendAuthorization.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SendAuthorization.kt\ncom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1\n+ 2 ProtoReader.kt\ncom/squareup/wire/ProtoReader\n*L\n1#1,370:1\n415#2,7:371\n*E\n*S KotlinDebug\n*F\n+ 1 SendAuthorization.kt\ncom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1\n*L\n185#1,7:371\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
        "decode",
        "reader",
        "Lcom/squareup/wire/ProtoReader;",
        "encode",
        "",
        "writer",
        "Lcom/squareup/wire/ProtoWriter;",
        "value",
        "encodedSize",
        "",
        "redact",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V
    .locals 0

    .line 161
    invoke-direct {p0, p1, p2}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/SendAuthorization;
    .locals 12

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 181
    move-object v1, v0

    check-cast v1, Lokio/ByteString;

    .line 182
    move-object v2, v0

    check-cast v2, Lcom/squareup/comms/protos/buyer/EntryMethod;

    .line 183
    move-object v3, v0

    check-cast v3, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 184
    check-cast v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    .line 371
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v4

    move-object v10, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    .line 373
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 377
    invoke-virtual {p1, v4, v5}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object v11

    .line 194
    new-instance p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-object v6, p1

    invoke-direct/range {v6 .. v11}, Lcom/squareup/comms/protos/buyer/SendAuthorization;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)V

    return-object p1

    :cond_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 191
    invoke-virtual {p1, v0}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 190
    :cond_1
    sget-object v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    move-object v10, v0

    goto :goto_0

    .line 189
    :cond_2
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    move-object v9, v0

    goto :goto_0

    .line 188
    :cond_3
    sget-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/buyer/EntryMethod;

    move-object v8, v0

    goto :goto_0

    .line 187
    :cond_4
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokio/ByteString;

    move-object v7, v0

    goto :goto_0
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0

    .line 161
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/SendAuthorization;)V
    .locals 3

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 175
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 176
    sget-object v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 177
    invoke-virtual {p2}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0

    .line 161
    check-cast p2, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/SendAuthorization;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/comms/protos/buyer/SendAuthorization;)I
    .locals 4

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 167
    sget-object v1, Lcom/squareup/comms/protos/buyer/EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    sget-object v1, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    sget-object v1, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 161
    check-cast p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;->encodedSize(Lcom/squareup/comms/protos/buyer/SendAuthorization;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/comms/protos/buyer/SendAuthorization;)Lcom/squareup/comms/protos/buyer/SendAuthorization;
    .locals 10

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    iget-object v0, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v0}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, v1

    .line 205
    :goto_0
    iget-object v0, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    :cond_1
    move-object v6, v1

    .line 206
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v8, 0x3

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    .line 203
    invoke-static/range {v2 .. v9}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->copy$default(Lcom/squareup/comms/protos/buyer/SendAuthorization;Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 161
    check-cast p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;->redact(Lcom/squareup/comms/protos/buyer/SendAuthorization;)Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-result-object p1

    return-object p1
.end method
