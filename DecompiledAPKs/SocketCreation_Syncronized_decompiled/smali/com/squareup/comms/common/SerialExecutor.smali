.class public interface abstract Lcom/squareup/comms/common/SerialExecutor;
.super Ljava/lang/Object;
.source "SerialExecutor.java"

# interfaces
.implements Ljava/io/Closeable;


# virtual methods
.method public abstract post(Ljava/lang/Runnable;)V
.end method

.method public abstract postDelayed(Ljava/lang/Runnable;ILjava/util/concurrent/TimeUnit;)V
.end method
