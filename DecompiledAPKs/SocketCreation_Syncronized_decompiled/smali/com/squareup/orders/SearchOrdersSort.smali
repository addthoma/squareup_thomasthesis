.class public final Lcom/squareup/orders/SearchOrdersSort;
.super Lcom/squareup/wire/Message;
.source "SearchOrdersSort.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/SearchOrdersSort$ProtoAdapter_SearchOrdersSort;,
        Lcom/squareup/orders/SearchOrdersSort$Field;,
        Lcom/squareup/orders/SearchOrdersSort$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/SearchOrdersSort;",
        "Lcom/squareup/orders/SearchOrdersSort$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/SearchOrdersSort;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SORT_FIELD:Lcom/squareup/orders/SearchOrdersSort$Field;

.field public static final DEFAULT_SORT_ORDER:Lcom/squareup/protos/connect/v2/common/SortOrder;

.field private static final serialVersionUID:J


# instance fields
.field public final sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.SearchOrdersSort$Field#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.SortOrder#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$ProtoAdapter_SearchOrdersSort;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersSort$ProtoAdapter_SearchOrdersSort;-><init>()V

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->DO_NOT_USE:Lcom/squareup/orders/SearchOrdersSort$Field;

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort;->DEFAULT_SORT_FIELD:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 35
    sget-object v0, Lcom/squareup/protos/connect/v2/common/SortOrder;->DESC:Lcom/squareup/protos/connect/v2/common/SortOrder;

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort;->DEFAULT_SORT_ORDER:Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/SearchOrdersSort$Field;Lcom/squareup/protos/connect/v2/common/SortOrder;)V
    .locals 1

    .line 69
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/orders/SearchOrdersSort;-><init>(Lcom/squareup/orders/SearchOrdersSort$Field;Lcom/squareup/protos/connect/v2/common/SortOrder;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/SearchOrdersSort$Field;Lcom/squareup/protos/connect/v2/common/SortOrder;Lokio/ByteString;)V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/orders/SearchOrdersSort;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 74
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 75
    iput-object p2, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 90
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/SearchOrdersSort;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 91
    :cond_1
    check-cast p1, Lcom/squareup/orders/SearchOrdersSort;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersSort;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersSort;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    iget-object v3, p1, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 93
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    iget-object p1, p1, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    .line 94
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 99
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 101
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersSort;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/orders/SearchOrdersSort$Field;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/SortOrder;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/SearchOrdersSort$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersSort$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 82
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersSort;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/SearchOrdersSort$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersSort;->newBuilder()Lcom/squareup/orders/SearchOrdersSort$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    if-eqz v1, :cond_0

    const-string v1, ", sort_field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    if-eqz v1, :cond_1

    const-string v1, ", sort_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SearchOrdersSort{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
