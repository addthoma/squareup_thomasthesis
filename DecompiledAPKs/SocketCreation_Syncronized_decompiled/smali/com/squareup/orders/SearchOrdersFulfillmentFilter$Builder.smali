.class public final Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchOrdersFulfillmentFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/SearchOrdersFulfillmentFilter;",
        "Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fulfillment_states:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            ">;"
        }
    .end annotation
.end field

.field public fulfillment_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 116
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_types:Ljava/util/List;

    .line 117
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_states:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
    .locals 4

    .line 148
    new-instance v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_types:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_states:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    move-result-object v0

    return-object v0
.end method

.method public fulfillment_states(Ljava/util/List;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            ">;)",
            "Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;"
        }
    .end annotation

    .line 141
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_states:Ljava/util/List;

    return-object p0
.end method

.method public fulfillment_types(Ljava/util/List;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            ">;)",
            "Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;"
        }
    .end annotation

    .line 128
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_types:Ljava/util/List;

    return-object p0
.end method
