.class public final Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accepted_at:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public picked_up_at:Ljava/lang/String;

.field public pickup_at:Ljava/lang/String;

.field public placed_at:Ljava/lang/String;

.field public ready_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8989
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public accepted_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 0

    .line 9008
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->accepted_at:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
    .locals 9

    .line 9024
    new-instance v8, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->note:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->placed_at:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->pickup_at:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->accepted_at:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->ready_at:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->picked_up_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8976
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    move-result-object v0

    return-object v0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 0

    .line 8993
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public picked_up_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 0

    .line 9018
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->picked_up_at:Ljava/lang/String;

    return-object p0
.end method

.method public pickup_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 0

    .line 9003
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->pickup_at:Ljava/lang/String;

    return-object p0
.end method

.method public placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 0

    .line 8998
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->placed_at:Ljava/lang/String;

    return-object p0
.end method

.method public ready_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;
    .locals 0

    .line 9013
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->ready_at:Ljava/lang/String;

    return-object p0
.end method
