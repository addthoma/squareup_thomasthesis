.class public final Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cancel_reason:Ljava/lang/String;

.field public canceled_at:Ljava/lang/String;

.field public completed_at:Ljava/lang/String;

.field public placed_at:Ljava/lang/String;

.field public recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9917
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
    .locals 8

    .line 9962
    new-instance v7, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->placed_at:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->completed_at:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->canceled_at:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->cancel_reason:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;-><init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 9906
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    .locals 0

    .line 9956
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    .locals 0

    .line 9948
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->canceled_at:Ljava/lang/String;

    return-object p0
.end method

.method public completed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    .locals 0

    .line 9940
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->completed_at:Ljava/lang/String;

    return-object p0
.end method

.method public placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    .locals 0

    .line 9932
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->placed_at:Ljava/lang/String;

    return-object p0
.end method

.method public recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    .locals 0

    .line 9924
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    return-object p0
.end method
