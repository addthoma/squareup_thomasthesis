.class public final Lcom/squareup/orders/model/Order$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order;",
        "Lcom/squareup/orders/model/Order$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public closed_at:Ljava/lang/String;

.field public created_at:Ljava/lang/String;

.field public creator_app_id:Ljava/lang/String;

.field public customer_id:Ljava/lang/String;

.field public discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

.field public fulfillments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;"
        }
    .end annotation
.end field

.field public id:Ljava/lang/String;

.field public line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;"
        }
    .end annotation
.end field

.field public location_id:Ljava/lang/String;

.field public merchant_id:Ljava/lang/String;

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

.field public old_metadata_map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public payment_groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$PaymentGroup;",
            ">;"
        }
    .end annotation
.end field

.field public pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

.field public reference_id:Ljava/lang/String;

.field public refund_groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$RefundGroup;",
            ">;"
        }
    .end annotation
.end field

.field public refunds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Refund;",
            ">;"
        }
    .end annotation
.end field

.field public reserved_discount_codes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReservedDiscountCode;",
            ">;"
        }
    .end annotation
.end field

.field public reserved_rewards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReservedReward;",
            ">;"
        }
    .end annotation
.end field

.field public return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

.field public returns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Return;",
            ">;"
        }
    .end annotation
.end field

.field public rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

.field public service_charges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ServiceCharge;",
            ">;"
        }
    .end annotation
.end field

.field public short_reference_id:Ljava/lang/String;

.field public source:Lcom/squareup/orders/model/Order$Source;

.field public state:Lcom/squareup/orders/model/Order$State;

.field public substatus:Ljava/lang/String;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Tender;",
            ">;"
        }
    .end annotation
.end field

.field public tenders_finalized:Ljava/lang/Boolean;

.field public tips:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Tip;",
            ">;"
        }
    .end annotation
.end field

.field public total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public updated_at:Ljava/lang/String;

.field public vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

.field public version:Ljava/lang/Integer;

.field public was_status:Lcom/squareup/orders/model/Order$State;

.field public workflow:Ljava/lang/String;

.field public workflow_version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1045
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1046
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->line_items:Ljava/util/List;

    .line 1047
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->taxes:Ljava/util/List;

    .line 1048
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->discounts:Ljava/util/List;

    .line 1049
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->service_charges:Ljava/util/List;

    .line 1050
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->tips:Ljava/util/List;

    .line 1051
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->fulfillments:Ljava/util/List;

    .line 1052
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->returns:Ljava/util/List;

    .line 1053
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->tenders:Ljava/util/List;

    .line 1054
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->payment_groups:Ljava/util/List;

    .line 1055
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->refunds:Ljava/util/List;

    .line 1056
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->refund_groups:Ljava/util/List;

    .line 1057
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->metadata:Ljava/util/Map;

    .line 1058
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->old_metadata_map:Ljava/util/Map;

    .line 1059
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->reserved_discount_codes:Ljava/util/List;

    .line 1060
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Builder;->reserved_rewards:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order;
    .locals 2

    .line 1620
    new-instance v0, Lcom/squareup/orders/model/Order;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/orders/model/Order;-><init>(Lcom/squareup/orders/model/Order$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 954
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Builder;->build()Lcom/squareup/orders/model/Order;

    move-result-object v0

    return-object v0
.end method

.method public closed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1430
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->closed_at:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1406
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public creator_app_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1108
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->creator_app_id:Ljava/lang/String;

    return-object p0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1128
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public discounts(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1201
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1202
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->discounts:Ljava/util/List;

    return-object p0
.end method

.method public ext_order_client_details(Lcom/squareup/protos/client/orders/OrderClientDetails;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1614
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    return-object p0
.end method

.method public fulfillments(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1235
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1236
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->fulfillments:Ljava/util/List;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1071
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public line_items(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1158
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1159
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->line_items:Ljava/util/List;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1082
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1147
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1381
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 1382
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1137
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public net_amounts(Lcom/squareup/orders/model/Order$MoneyAmounts;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1275
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    return-object p0
.end method

.method public old_metadata_map(Ljava/util/Map;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1393
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 1394
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->old_metadata_map:Ljava/util/Map;

    return-object p0
.end method

.method public payment_groups(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$PaymentGroup;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1324
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1325
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->payment_groups:Ljava/util/List;

    return-object p0
.end method

.method public pricing_options(Lcom/squareup/orders/model/Order$PricingOptions;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1561
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1094
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public refund_groups(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$RefundGroup;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1353
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1354
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->refund_groups:Ljava/util/List;

    return-object p0
.end method

.method public refunds(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Refund;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1339
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1340
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->refunds:Ljava/util/List;

    return-object p0
.end method

.method public reserved_discount_codes(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReservedDiscountCode;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1573
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1574
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->reserved_discount_codes:Ljava/util/List;

    return-object p0
.end method

.method public reserved_rewards(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReservedReward;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1586
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1587
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->reserved_rewards:Ljava/util/List;

    return-object p0
.end method

.method public return_amounts(Lcom/squareup/orders/model/Order$MoneyAmounts;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1263
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    return-object p0
.end method

.method public returns(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Return;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1250
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1251
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->returns:Ljava/util/List;

    return-object p0
.end method

.method public rounding_adjustment(Lcom/squareup/orders/model/Order$RoundingAdjustment;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1289
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    return-object p0
.end method

.method public service_charges(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ServiceCharge;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1212
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1213
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->service_charges:Ljava/util/List;

    return-object p0
.end method

.method public short_reference_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1549
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->short_reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public source(Lcom/squareup/orders/model/Order$Source;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1118
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->source:Lcom/squareup/orders/model/Order$Source;

    return-object p0
.end method

.method public state(Lcom/squareup/orders/model/Order$State;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1440
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->state:Lcom/squareup/orders/model/Order$State;

    return-object p0
.end method

.method public substatus(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1445
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->substatus:Ljava/lang/String;

    return-object p0
.end method

.method public taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1180
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1181
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method

.method public tenders(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Tender;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1310
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1311
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->tenders:Ljava/util/List;

    return-object p0
.end method

.method public tenders_finalized(Ljava/lang/Boolean;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1298
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->tenders_finalized:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tips(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Tip;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Builder;"
        }
    .end annotation

    .line 1221
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1222
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->tips:Ljava/util/List;

    return-object p0
.end method

.method public total_discount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1511
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1487
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_service_charge_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1539
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1499
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1523
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1418
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method

.method public vaulted_data(Lcom/squareup/protos/common/privacyvault/VaultedData;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1597
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1461
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method

.method public was_status(Lcom/squareup/orders/model/Order$State;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1609
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->was_status:Lcom/squareup/orders/model/Order$State;

    return-object p0
.end method

.method public workflow(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1469
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->workflow:Ljava/lang/String;

    return-object p0
.end method

.method public workflow_version(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$Builder;
    .locals 0

    .line 1474
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Builder;->workflow_version:Ljava/lang/Integer;

    return-object p0
.end method
