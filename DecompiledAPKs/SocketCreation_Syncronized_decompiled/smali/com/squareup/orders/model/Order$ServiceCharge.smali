.class public final Lcom/squareup/orders/model/Order$ServiceCharge;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServiceCharge"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ServiceCharge$ProtoAdapter_ServiceCharge;,
        Lcom/squareup/orders/model/Order$ServiceCharge$Type;,
        Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;,
        Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ServiceCharge;",
        "Lcom/squareup/orders/model/Order$ServiceCharge$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ServiceCharge;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TAXABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final applied_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$AppliedTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ServiceCharge$CalculationPhase#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0xd
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final taxable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ServiceCharge$Type#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 5828
    new-instance v0, Lcom/squareup/orders/model/Order$ServiceCharge$ProtoAdapter_ServiceCharge;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ServiceCharge$ProtoAdapter_ServiceCharge;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 5838
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 5842
    sget-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    const/4 v0, 0x0

    .line 5844
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge;->DEFAULT_TAXABLE:Ljava/lang/Boolean;

    .line 5846
    sget-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$Type;->CUSTOM:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$ServiceCharge$Type;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/model/Order$ServiceCharge$Type;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 6076
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/orders/model/Order$ServiceCharge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$ServiceCharge$Type;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$ServiceCharge$Type;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/model/Order$ServiceCharge$Type;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 6084
    sget-object v1, Lcom/squareup/orders/model/Order$ServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 6085
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    move-object v1, p2

    .line 6086
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    move-object v1, p3

    .line 6087
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    move-object v1, p4

    .line 6088
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    move-object v1, p5

    .line 6089
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    move-object v1, p6

    .line 6090
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p7

    .line 6091
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p8

    .line 6092
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p9

    .line 6093
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p10

    .line 6094
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    move-object v1, p11

    .line 6095
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    const-string v1, "taxes"

    move-object v2, p12

    .line 6096
    invoke-static {v1, p12}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    const-string v1, "applied_taxes"

    move-object/from16 v2, p13

    .line 6097
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    const-string v1, "metadata"

    move-object/from16 v2, p14

    .line 6098
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->metadata:Ljava/util/Map;

    move-object/from16 v1, p15

    .line 6099
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6127
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ServiceCharge;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6128
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ServiceCharge;

    .line 6129
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    .line 6130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    .line 6131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    .line 6132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    .line 6133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    .line 6134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 6139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    .line 6140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    .line 6141
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    .line 6142
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->metadata:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->metadata:Ljava/util/Map;

    .line 6143
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    .line 6144
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6149
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 6151
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6152
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6153
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6154
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6155
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6156
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6157
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6158
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6159
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6160
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6161
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6162
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6163
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6164
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6165
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6166
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$ServiceCharge$Type;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 6167
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 2

    .line 6104
    new-instance v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;-><init>()V

    .line 6105
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->uid:Ljava/lang/String;

    .line 6106
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->name:Ljava/lang/String;

    .line 6107
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->catalog_object_id:Ljava/lang/String;

    .line 6108
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 6109
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->percentage:Ljava/lang/String;

    .line 6110
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6111
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6112
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6113
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 6114
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 6115
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxable:Ljava/lang/Boolean;

    .line 6116
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxes:Ljava/util/List;

    .line 6117
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    .line 6118
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->metadata:Ljava/util/Map;

    .line 6119
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    .line 6120
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5827
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ServiceCharge;->newBuilder()Lcom/squareup/orders/model/Order$ServiceCharge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6175
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6176
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6177
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6178
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6179
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6180
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6181
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6182
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6183
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", total_tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6184
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    if-eqz v1, :cond_9

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6185
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", taxable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6186
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6187
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", applied_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6188
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6189
    :cond_d
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    if-eqz v1, :cond_e

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ServiceCharge;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ServiceCharge{"

    .line 6190
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
