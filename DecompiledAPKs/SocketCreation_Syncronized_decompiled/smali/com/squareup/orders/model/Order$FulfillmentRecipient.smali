.class public final Lcom/squareup/orders/model/Order$FulfillmentRecipient;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentRecipient"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;,
        Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.privacyvault.VaultedData#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8535
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Lcom/squareup/protos/common/privacyvault/VaultedData;)V
    .locals 8

    .line 8629
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Lcom/squareup/protos/common/privacyvault/VaultedData;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Lcom/squareup/protos/common/privacyvault/VaultedData;Lokio/ByteString;)V
    .locals 1

    .line 8634
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8635
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    .line 8636
    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    .line 8637
    iput-object p3, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    .line 8638
    iput-object p4, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    .line 8639
    iput-object p5, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 8640
    iput-object p6, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8659
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8660
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 8661
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    .line 8662
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    .line 8663
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    .line 8664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    .line 8665
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 8666
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 8667
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8672
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 8674
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8675
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8676
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8677
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8678
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8679
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8680
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/privacyvault/VaultedData;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 8681
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 2

    .line 8645
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;-><init>()V

    .line 8646
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    .line 8647
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    .line 8648
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    .line 8649
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    .line 8650
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 8651
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 8652
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 8534
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8689
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8690
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8691
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8692
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8693
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_4

    const-string v1, ", address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8694
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v1, :cond_5

    const-string v1, ", vaulted_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentRecipient{"

    .line 8695
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
