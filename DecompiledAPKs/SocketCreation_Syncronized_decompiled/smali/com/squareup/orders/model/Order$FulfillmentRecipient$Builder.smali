.class public final Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentRecipient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public customer_id:Ljava/lang/String;

.field public display_name:Ljava/lang/String;

.field public email_address:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8711
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 0

    .line 8775
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .locals 9

    .line 8786
    new-instance v8, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Address;Lcom/squareup/protos/common/privacyvault/VaultedData;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8698
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object v0

    return-object v0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 0

    .line 8727
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 0

    .line 8739
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 0

    .line 8751
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 0

    .line 8763
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public vaulted_data(Lcom/squareup/protos/common/privacyvault/VaultedData;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;
    .locals 0

    .line 8780
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    return-object p0
.end method
