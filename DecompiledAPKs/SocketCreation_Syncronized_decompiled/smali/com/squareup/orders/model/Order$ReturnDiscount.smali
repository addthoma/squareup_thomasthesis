.class public final Lcom/squareup/orders/model/Order$ReturnDiscount;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnDiscount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReturnDiscount$ProtoAdapter_ReturnDiscount;,
        Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReturnDiscount;",
        "Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public static final DEFAULT_SOURCE_DISCOUNT_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Discount$Scope#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final source_discount_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Discount$Type#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 14484
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnDiscount$ProtoAdapter_ReturnDiscount;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnDiscount$ProtoAdapter_ReturnDiscount;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 14494
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 14498
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->UNKNOWN_DISCOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 14502
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->OTHER_DISCOUNT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;)V
    .locals 12

    .line 14625
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/orders/model/Order$ReturnDiscount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Lokio/ByteString;)V
    .locals 1

    .line 14632
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 14633
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    .line 14634
    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    .line 14635
    iput-object p3, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    .line 14636
    iput-object p4, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    .line 14637
    iput-object p5, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    .line 14638
    iput-object p6, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 14639
    iput-object p7, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    .line 14640
    iput-object p8, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14641
    iput-object p9, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14642
    iput-object p10, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 14665
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 14666
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnDiscount;

    .line 14667
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    .line 14668
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    .line 14669
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    .line 14670
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    .line 14671
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    .line 14672
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 14673
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    .line 14674
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14675
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14676
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 14677
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 14682
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 14684
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 14685
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14686
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14687
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14688
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14689
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14690
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14691
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14692
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14693
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14694
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 14695
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 2

    .line 14647
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;-><init>()V

    .line 14648
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->uid:Ljava/lang/String;

    .line 14649
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->source_discount_uid:Ljava/lang/String;

    .line 14650
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->catalog_object_id:Ljava/lang/String;

    .line 14651
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 14652
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->name:Ljava/lang/String;

    .line 14653
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 14654
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->percentage:Ljava/lang/String;

    .line 14655
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14656
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14657
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 14658
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 14483
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnDiscount;->newBuilder()Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 14702
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14703
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14704
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_discount_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->source_discount_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14705
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14706
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14707
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14708
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    if-eqz v1, :cond_5

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14709
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14710
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14711
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14712
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    if-eqz v1, :cond_9

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnDiscount{"

    .line 14713
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
