.class public final Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cancel_reason:Ljava/lang/String;

.field public canceled_at:Ljava/lang/String;

.field public carrier:Ljava/lang/String;

.field public delivered_at:Ljava/lang/String;

.field public expected_delivered_at:Ljava/lang/String;

.field public expected_shipped_at:Ljava/lang/String;

.field public failed_at:Ljava/lang/String;

.field public failure_reason:Ljava/lang/String;

.field public in_progress_at:Ljava/lang/String;

.field public packaged_at:Ljava/lang/String;

.field public placed_at:Ljava/lang/String;

.field public recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

.field public shipped_at:Ljava/lang/String;

.field public shipping_note:Ljava/lang/String;

.field public shipping_type:Ljava/lang/String;

.field public tracking_number:Ljava/lang/String;

.field public tracking_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9491
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
    .locals 2

    .line 9687
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;-><init>(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 9456
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9659
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9651
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->canceled_at:Ljava/lang/String;

    return-object p0
.end method

.method public carrier(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9511
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->carrier:Ljava/lang/String;

    return-object p0
.end method

.method public delivered_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9640
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->delivered_at:Ljava/lang/String;

    return-object p0
.end method

.method public expected_delivered_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9628
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_delivered_at:Ljava/lang/String;

    return-object p0
.end method

.method public expected_shipped_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9602
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_shipped_at:Ljava/lang/String;

    return-object p0
.end method

.method public failed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9673
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failed_at:Ljava/lang/String;

    return-object p0
.end method

.method public failure_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9681
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failure_reason:Ljava/lang/String;

    return-object p0
.end method

.method public in_progress_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9576
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->in_progress_at:Ljava/lang/String;

    return-object p0
.end method

.method public packaged_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9590
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->packaged_at:Ljava/lang/String;

    return-object p0
.end method

.method public placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9562
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->placed_at:Ljava/lang/String;

    return-object p0
.end method

.method public recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9500
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    return-object p0
.end method

.method public shipped_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9616
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipped_at:Ljava/lang/String;

    return-object p0
.end method

.method public shipping_note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9521
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_note:Ljava/lang/String;

    return-object p0
.end method

.method public shipping_type(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9532
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_type:Ljava/lang/String;

    return-object p0
.end method

.method public tracking_number(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9540
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_number:Ljava/lang/String;

    return-object p0
.end method

.method public tracking_url(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;
    .locals 0

    .line 9548
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_url:Ljava/lang/String;

    return-object p0
.end method
