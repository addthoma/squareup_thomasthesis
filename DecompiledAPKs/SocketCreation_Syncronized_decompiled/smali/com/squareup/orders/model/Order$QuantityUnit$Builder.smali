.class public final Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$QuantityUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "Lcom/squareup/orders/model/Order$QuantityUnit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

.field public precision:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2047
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$QuantityUnit;
    .locals 7

    .line 2105
    new-instance v6, Lcom/squareup/orders/model/Order$QuantityUnit;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->precision:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$QuantityUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2038
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->build()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
    .locals 0

    .line 2085
    iput-object p1, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
    .locals 0

    .line 2099
    iput-object p1, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
    .locals 0

    .line 2057
    iput-object p1, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object p0
.end method

.method public precision(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
    .locals 0

    .line 2072
    iput-object p1, p0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->precision:Ljava/lang/Integer;

    return-object p0
.end method
