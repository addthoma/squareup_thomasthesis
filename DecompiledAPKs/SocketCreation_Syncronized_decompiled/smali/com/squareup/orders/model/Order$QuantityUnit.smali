.class public final Lcom/squareup/orders/model/Order$QuantityUnit;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QuantityUnit"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;,
        Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "Lcom/squareup/orders/model/Order$QuantityUnit$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$QuantityUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_PRECISION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final precision:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1910
    new-instance v0, Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 1914
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->DEFAULT_PRECISION:Ljava/lang/Integer;

    const-wide/16 v0, 0x0

    .line 1918
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    .line 1979
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$QuantityUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 1984
    sget-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1985
    iput-object p1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 1986
    iput-object p2, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    .line 1987
    iput-object p3, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    .line 1988
    iput-object p4, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2005
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$QuantityUnit;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2006
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 2007
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$QuantityUnit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$QuantityUnit;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 2008
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    .line 2009
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    .line 2010
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    .line 2011
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2016
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 2018
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$QuantityUnit;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2019
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2020
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2021
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2022
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 2023
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$QuantityUnit$Builder;
    .locals 2

    .line 1993
    new-instance v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;-><init>()V

    .line 1994
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 1995
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->precision:Ljava/lang/Integer;

    .line 1996
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_id:Ljava/lang/String;

    .line 1997
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 1998
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$QuantityUnit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1909
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$QuantityUnit;->newBuilder()Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2030
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2031
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v1, :cond_0

    const-string v1, ", measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2032
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", precision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2033
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2034
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "QuantityUnit{"

    .line 2035
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
