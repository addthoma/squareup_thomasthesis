.class public final Lcom/squareup/orders/model/Order$Return;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Return"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;,
        Lcom/squareup/orders/model/Order$Return$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$Return;",
        "Lcom/squareup/orders/model/Order$Return$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$Return;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SOURCE_ORDER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$MoneyAmounts#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final return_discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnDiscount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public final return_line_items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final return_service_charges:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnServiceCharge#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
            ">;"
        }
    .end annotation
.end field

.field public final return_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public final return_tips:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnTip#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTip;",
            ">;"
        }
    .end annotation
.end field

.field public final rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$RoundingAdjustment#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final source_order_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11082
    new-instance v0, Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Return$ProtoAdapter_Return;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$Return;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/orders/model/Order$RoundingAdjustment;Lcom/squareup/orders/model/Order$MoneyAmounts;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTip;",
            ">;",
            "Lcom/squareup/orders/model/Order$RoundingAdjustment;",
            "Lcom/squareup/orders/model/Order$MoneyAmounts;",
            ")V"
        }
    .end annotation

    .line 11205
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/orders/model/Order$Return;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/orders/model/Order$RoundingAdjustment;Lcom/squareup/orders/model/Order$MoneyAmounts;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/orders/model/Order$RoundingAdjustment;Lcom/squareup/orders/model/Order$MoneyAmounts;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTip;",
            ">;",
            "Lcom/squareup/orders/model/Order$RoundingAdjustment;",
            "Lcom/squareup/orders/model/Order$MoneyAmounts;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 11213
    sget-object v0, Lcom/squareup/orders/model/Order$Return;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 11214
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    .line 11215
    iput-object p2, p0, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    const-string p1, "return_line_items"

    .line 11216
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    const-string p1, "return_service_charges"

    .line 11217
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    const-string p1, "return_taxes"

    .line 11218
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    const-string p1, "return_discounts"

    .line 11219
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    const-string p1, "return_tips"

    .line 11220
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    .line 11221
    iput-object p8, p0, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 11222
    iput-object p9, p0, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 11244
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$Return;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 11245
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$Return;

    .line 11246
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Return;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Return;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    .line 11247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    .line 11248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    .line 11249
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    .line 11250
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    .line 11251
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    .line 11252
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    .line 11253
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 11254
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 11255
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 11260
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 11262
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Return;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 11263
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11264
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11265
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11266
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11267
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11268
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11269
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11270
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$RoundingAdjustment;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11271
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$MoneyAmounts;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 11272
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 2

    .line 11227
    new-instance v0, Lcom/squareup/orders/model/Order$Return$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Return$Builder;-><init>()V

    .line 11228
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->uid:Ljava/lang/String;

    .line 11229
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->source_order_id:Ljava/lang/String;

    .line 11230
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_line_items:Ljava/util/List;

    .line 11231
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_service_charges:Ljava/util/List;

    .line 11232
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_taxes:Ljava/util/List;

    .line 11233
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_discounts:Ljava/util/List;

    .line 11234
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_tips:Ljava/util/List;

    .line 11235
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 11236
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 11237
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Return;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$Return$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 11081
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Return;->newBuilder()Lcom/squareup/orders/model/Order$Return$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 11279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11280
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11281
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_order_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->source_order_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11282
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", return_line_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_line_items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11283
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", return_service_charges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_service_charges:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11284
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", return_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11285
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", return_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11286
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", return_tips="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_tips:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11287
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    if-eqz v1, :cond_7

    const-string v1, ", rounding_adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11288
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v1, :cond_8

    const-string v1, ", return_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Return{"

    .line 11289
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
