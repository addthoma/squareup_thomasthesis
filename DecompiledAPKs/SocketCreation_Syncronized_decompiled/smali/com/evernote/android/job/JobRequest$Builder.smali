.class public final Lcom/evernote/android/job/JobRequest$Builder;
.super Ljava/lang/Object;
.source "JobRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/android/job/JobRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# static fields
.field private static final CREATE_ID:I = -0x223d


# instance fields
.field private mBackoffMs:J

.field private mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

.field private mEndMs:J

.field private mExact:Z

.field private mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

.field private mExtrasXml:Ljava/lang/String;

.field private mFlexMs:J

.field private mId:I

.field private mIntervalMs:J

.field private mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

.field private mRequirementsEnforced:Z

.field private mRequiresBatteryNotLow:Z

.field private mRequiresCharging:Z

.field private mRequiresDeviceIdle:Z

.field private mRequiresStorageNotLow:Z

.field private mStartMs:J

.field final mTag:Ljava/lang/String;

.field private mTransient:Z

.field private mTransientExtras:Landroid/os/Bundle;

.field private mUpdateCurrent:Z


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 4

    .line 639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    const-string v0, "_id"

    .line 640
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    const-string v0, "tag"

    .line 641
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    const-string v0, "startMs"

    .line 643
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    const-string v0, "endMs"

    .line 644
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    const-string v0, "backoffMs"

    .line 646
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    :try_start_0
    const-string v0, "backoffPolicy"

    .line 648
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->valueOf(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 650
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;)V

    .line 651
    sget-object v0, Lcom/evernote/android/job/JobRequest;->DEFAULT_BACKOFF_POLICY:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    :goto_0
    const-string v0, "intervalMs"

    .line 654
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    const-string v0, "flexMs"

    .line 655
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    const-string v0, "requirementsEnforced"

    .line 657
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    const-string v0, "requiresCharging"

    .line 658
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    const-string v0, "requiresDeviceIdle"

    .line 659
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    const-string v0, "requiresBatteryNotLow"

    .line 660
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_4

    :cond_3
    const/4 v0, 0x0

    :goto_4
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    const-string v0, "requiresStorageNotLow"

    .line 661
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_5

    :cond_4
    const/4 v0, 0x0

    :goto_5
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    const-string v0, "exact"

    .line 662
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_6

    :cond_5
    const/4 v0, 0x0

    :goto_6
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    :try_start_1
    const-string v0, "networkType"

    .line 664
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$NetworkType;->valueOf(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest$NetworkType;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_7

    :catchall_1
    move-exception v0

    .line 666
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;)V

    .line 667
    sget-object v0, Lcom/evernote/android/job/JobRequest;->DEFAULT_NETWORK_TYPE:Lcom/evernote/android/job/JobRequest$NetworkType;

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    :goto_7
    const-string v0, "extras"

    .line 670
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    const-string v0, "transient"

    .line 671
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result p1

    if-lez p1, :cond_6

    goto :goto_8

    :cond_6
    const/4 v1, 0x0

    :goto_8
    iput-boolean v1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;Lcom/evernote/android/job/JobRequest$1;)V
    .locals 0

    .line 580
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method private constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;)V
    .locals 1

    const/4 v0, 0x0

    .line 676
    invoke-direct {p0, p1, v0}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;Z)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;Lcom/evernote/android/job/JobRequest$1;)V
    .locals 0

    .line 580
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;)V

    return-void
.end method

.method private constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;Z)V
    .locals 2

    .line 679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    if-eqz p2, :cond_0

    const/16 p2, -0x223d

    goto :goto_0

    .line 680
    :cond_0
    iget p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    :goto_0
    iput p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    .line 681
    iget-object p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    iput-object p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    .line 683
    iget-wide v0, p1, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    .line 684
    iget-wide v0, p1, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    .line 686
    iget-wide v0, p1, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    .line 687
    iget-object p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    iput-object p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 689
    iget-wide v0, p1, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    .line 690
    iget-wide v0, p1, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    .line 692
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    .line 693
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    .line 694
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    .line 695
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    .line 696
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    .line 697
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    .line 698
    iget-object p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    iput-object p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    .line 700
    iget-object p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    iput-object p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    .line 701
    iget-object p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    iput-object p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    .line 703
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mUpdateCurrent:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mUpdateCurrent:Z

    .line 704
    iget-boolean p2, p1, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    iput-boolean p2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    .line 705
    iget-object p1, p1, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;ZLcom/evernote/android/job/JobRequest$1;)V
    .locals 0

    .line 580
    invoke-direct {p0, p1, p2}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    .line 626
    invoke-static {p1}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    const/16 p1, -0x223d

    .line 627
    iput p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    const-wide/16 v0, -0x1

    .line 629
    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    .line 630
    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    const-wide/16 v0, 0x7530

    .line 632
    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    .line 633
    sget-object p1, Lcom/evernote/android/job/JobRequest;->DEFAULT_BACKOFF_POLICY:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 635
    sget-object p1, Lcom/evernote/android/job/JobRequest;->DEFAULT_NETWORK_TYPE:Lcom/evernote/android/job/JobRequest$NetworkType;

    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    return-void
.end method

.method static synthetic access$100(Lcom/evernote/android/job/JobRequest$Builder;)I
    .locals 0

    .line 580
    iget p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    return p0
.end method

.method static synthetic access$1000(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    return p0
.end method

.method static synthetic access$1200(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    return p0
.end method

.method static synthetic access$1300(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/JobRequest$NetworkType;
    .locals 0

    .line 580
    iget-object p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/util/support/PersistableBundleCompat;
    .locals 0

    .line 580
    iget-object p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-object p0
.end method

.method static synthetic access$1402(Lcom/evernote/android/job/JobRequest$Builder;Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/util/support/PersistableBundleCompat;
    .locals 0

    .line 580
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/evernote/android/job/JobRequest$Builder;)Ljava/lang/String;
    .locals 0

    .line 580
    iget-object p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mUpdateCurrent:Z

    return p0
.end method

.method static synthetic access$1700(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    return p0
.end method

.method static synthetic access$1800(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    return p0
.end method

.method static synthetic access$1900(Lcom/evernote/android/job/JobRequest$Builder;)Landroid/os/Bundle;
    .locals 0

    .line 580
    iget-object p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    return-object p0
.end method

.method static synthetic access$200(Lcom/evernote/android/job/JobRequest$Builder;)J
    .locals 2

    .line 580
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    return-wide v0
.end method

.method static synthetic access$2200(Lcom/evernote/android/job/JobRequest$Builder;Landroid/content/ContentValues;)V
    .locals 0

    .line 580
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobRequest$Builder;->fillContentValues(Landroid/content/ContentValues;)V

    return-void
.end method

.method static synthetic access$300(Lcom/evernote/android/job/JobRequest$Builder;)J
    .locals 2

    .line 580
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/JobRequest$BackoffPolicy;
    .locals 0

    .line 580
    iget-object p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    return-object p0
.end method

.method static synthetic access$500(Lcom/evernote/android/job/JobRequest$Builder;)J
    .locals 2

    .line 580
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/evernote/android/job/JobRequest$Builder;)J
    .locals 2

    .line 580
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/evernote/android/job/JobRequest$Builder;)J
    .locals 2

    .line 580
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    return p0
.end method

.method static synthetic access$900(Lcom/evernote/android/job/JobRequest$Builder;)Z
    .locals 0

    .line 580
    iget-boolean p0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    return p0
.end method

.method private fillContentValues(Landroid/content/ContentValues;)V
    .locals 2

    .line 709
    iget v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 710
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    const-string v1, "tag"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "startMs"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 713
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "endMs"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 715
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "backoffMs"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 716
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "backoffPolicy"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "intervalMs"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 719
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "flexMs"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 721
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "requirementsEnforced"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 722
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "requiresCharging"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 723
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "requiresDeviceIdle"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 724
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "requiresBatteryNotLow"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 725
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "requiresStorageNotLow"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 726
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "exact"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 727
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$NetworkType;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "networkType"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    const-string v1, "extras"

    if-eqz v0, :cond_0

    .line 730
    invoke-virtual {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->saveToXml()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "transient"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public addExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 1

    .line 807
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    if-nez v0, :cond_0

    .line 808
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    goto :goto_0

    .line 810
    :cond_0
    invoke-virtual {v0, p1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putAll(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)V

    :goto_0
    const/4 p1, 0x0

    .line 812
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/evernote/android/job/JobRequest;
    .locals 19

    move-object/from16 v0, p0

    .line 1110
    iget-object v1, v0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    invoke-static {v1}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 1111
    iget-wide v1, v0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    const-string v3, "backoffMs must be > 0"

    invoke-static {v1, v2, v3}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentPositive(JLjava/lang/String;)J

    .line 1112
    iget-object v1, v0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    invoke-static {v1}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113
    iget-object v1, v0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    invoke-static {v1}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1115
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    const/4 v1, 0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    cmp-long v4, v2, v10

    if-lez v4, :cond_1

    .line 1116
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->getMinInterval()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    const-string v8, "intervalMs"

    invoke-static/range {v2 .. v8}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentInRange(JJJLjava/lang/String;)J

    .line 1117
    iget-wide v12, v0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    invoke-static {}, Lcom/evernote/android/job/JobRequest;->getMinFlex()J

    move-result-wide v14

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    const-string v18, "flexMs"

    move-wide/from16 v16, v2

    invoke-static/range {v12 .. v18}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentInRange(JJJLjava/lang/String;)J

    .line 1119
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    sget-wide v4, Lcom/evernote/android/job/JobRequest;->MIN_INTERVAL:J

    cmp-long v6, v2, v4

    if-ltz v6, :cond_0

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    sget-wide v4, Lcom/evernote/android/job/JobRequest;->MIN_FLEX:J

    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    .line 1121
    :cond_0
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    .line 1122
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v9

    sget-wide v4, Lcom/evernote/android/job/JobRequest;->MIN_INTERVAL:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    iget-wide v5, v0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-wide v5, Lcom/evernote/android/job/JobRequest;->MIN_FLEX:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "AllowSmallerIntervals enabled, this will crash on Android N and later, interval %d (minimum is %d), flex %d (minimum is %d)"

    .line 1121
    invoke-virtual {v2, v4, v3}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1126
    :cond_1
    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    if-eqz v2, :cond_3

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    cmp-long v4, v2, v10

    if-gtz v4, :cond_2

    goto :goto_0

    .line 1127
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t call setExact() on a periodic job."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1129
    :cond_3
    :goto_0
    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    if-eqz v2, :cond_5

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    iget-wide v4, v0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_4

    goto :goto_1

    .line 1130
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t call setExecutionWindow() for an exact job."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1132
    :cond_5
    :goto_1
    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    if-eqz v2, :cond_7

    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    if-nez v2, :cond_6

    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    if-nez v2, :cond_6

    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    if-nez v2, :cond_6

    sget-object v2, Lcom/evernote/android/job/JobRequest;->DEFAULT_NETWORK_TYPE:Lcom/evernote/android/job/JobRequest$NetworkType;

    iget-object v3, v0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    invoke-virtual {v2, v3}, Lcom/evernote/android/job/JobRequest$NetworkType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    if-nez v2, :cond_6

    iget-boolean v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    if-nez v2, :cond_6

    goto :goto_2

    .line 1134
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t require any condition for an exact job."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1137
    :cond_7
    :goto_2
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v10

    if-gtz v6, :cond_9

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    cmp-long v6, v2, v4

    if-eqz v6, :cond_8

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    cmp-long v6, v2, v4

    if-eqz v6, :cond_8

    goto :goto_3

    .line 1138
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "You\'re trying to build a job with no constraints, this is not allowed."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1140
    :cond_9
    :goto_3
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    cmp-long v6, v2, v10

    if-lez v6, :cond_b

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_a

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_a

    goto :goto_4

    .line 1141
    :cond_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t call setExecutionWindow() on a periodic job."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1143
    :cond_b
    :goto_4
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    cmp-long v4, v2, v10

    if-lez v4, :cond_d

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    const-wide/16 v4, 0x7530

    cmp-long v6, v2, v4

    if-nez v6, :cond_c

    sget-object v2, Lcom/evernote/android/job/JobRequest;->DEFAULT_BACKOFF_POLICY:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    iget-object v3, v0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    invoke-virtual {v2, v3}, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    goto :goto_5

    .line 1144
    :cond_c
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "A periodic job will not respect any back-off policy, so calling setBackoffCriteria() with setPeriodic() is an error."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1148
    :cond_d
    :goto_5
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    cmp-long v4, v2, v10

    if-gtz v4, :cond_f

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    const-wide v4, 0x2aaaaaaaaaaaaaaaL

    cmp-long v6, v2, v4

    if-gtz v6, :cond_e

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    cmp-long v6, v2, v4

    if-lez v6, :cond_f

    .line 1149
    :cond_e
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object v2

    const-string v3, "Attention: your execution window is too large. This could result in undesired behavior, see https://github.com/evernote/android-job/wiki/FAQ"

    invoke-virtual {v2, v3}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;)V

    .line 1152
    :cond_f
    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    cmp-long v4, v2, v10

    if-gtz v4, :cond_10

    iget-wide v2, v0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x16d

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-lez v6, :cond_10

    .line 1153
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    aput-object v3, v1, v9

    const-string v3, "Warning: job with tag %s scheduled over a year in the future"

    invoke-virtual {v2, v3, v1}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1156
    :cond_10
    iget v1, v0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    const-string v2, "id can\'t be negative"

    const/16 v3, -0x223d

    if-eq v1, v3, :cond_11

    .line 1157
    invoke-static {v1, v2}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentNonnegative(ILjava/lang/String;)I

    .line 1160
    :cond_11
    new-instance v1, Lcom/evernote/android/job/JobRequest$Builder;

    invoke-direct {v1, v0}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;)V

    .line 1161
    iget v4, v0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    if-ne v4, v3, :cond_12

    .line 1162
    invoke-static {}, Lcom/evernote/android/job/JobManager;->instance()Lcom/evernote/android/job/JobManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/evernote/android/job/JobManager;->getJobStorage()Lcom/evernote/android/job/JobStorage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/evernote/android/job/JobStorage;->nextJobId()I

    move-result v3

    iput v3, v1, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    .line 1163
    iget v3, v1, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    invoke-static {v3, v2}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentNonnegative(ILjava/lang/String;)I

    .line 1166
    :cond_12
    new-instance v2, Lcom/evernote/android/job/JobRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/evernote/android/job/JobRequest;-><init>(Lcom/evernote/android/job/JobRequest$Builder;Lcom/evernote/android/job/JobRequest$1;)V

    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 1172
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 1174
    :cond_1
    check-cast p1, Lcom/evernote/android/job/JobRequest$Builder;

    .line 1176
    iget v2, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    iget p1, p1, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 1181
    iget v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mId:I

    return v0
.end method

.method public setBackoffCriteria(JLcom/evernote/android/job/JobRequest$BackoffPolicy;)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 1

    const-string v0, "backoffMs must be > 0"

    .line 1069
    invoke-static {p1, p2, v0}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentPositive(JLjava/lang/String;)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffMs:J

    .line 1070
    invoke-static {p3}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mBackoffPolicy:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    return-object p0
.end method

.method public setExact(J)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 7

    const/4 v0, 0x1

    .line 984
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExact:Z

    const-wide v1, 0x5555555555555554L    # 1.1945305291614953E103

    cmp-long v3, p1, v1

    if-lez v3, :cond_0

    .line 986
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, p1, p2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v4, v5

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v4, v0

    const-string p1, "exactInMs clamped from %d days to %d days"

    invoke-virtual {v3, p1, v4}, Lcom/evernote/android/job/util/JobCat;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    move-wide p1, v1

    .line 990
    :cond_0
    invoke-virtual {p0, p1, p2, p1, p2}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 9

    const-string v0, "startInMs must be greater than 0"

    .line 770
    invoke-static {p1, p2, v0}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentPositive(JLjava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    const-wide v6, 0x7fffffffffffffffL

    const-string v8, "endInMs"

    move-wide v2, p3

    move-wide v4, p1

    .line 771
    invoke-static/range {v2 .. v8}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentInRange(JJJLjava/lang/String;)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    .line 773
    iget-wide p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    const/4 p3, 0x1

    const/4 p4, 0x0

    const/4 v0, 0x2

    const-wide v1, 0x5555555555555554L    # 1.1945305291614953E103

    cmp-long v3, p1, v1

    if-lez v3, :cond_0

    .line 774
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object p1

    new-array p2, v0, [Ljava/lang/Object;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, p2, p4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, p2, p3

    const-string v3, "startInMs reduced from %d days to %d days"

    invoke-virtual {p1, v3, p2}, Lcom/evernote/android/job/util/JobCat;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 775
    iput-wide v1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mStartMs:J

    .line 777
    :cond_0
    iget-wide p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    cmp-long v3, p1, v1

    if-lez v3, :cond_1

    .line 778
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->access$000()Lcom/evernote/android/job/util/JobCat;

    move-result-object p1

    new-array p2, v0, [Ljava/lang/Object;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v3, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    invoke-virtual {v0, v3, v4}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, p2, p4

    sget-object p4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p4, v1, v2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    aput-object p4, p2, p3

    const-string p3, "endInMs reduced from %d days to %d days"

    invoke-virtual {p1, p3, p2}, Lcom/evernote/android/job/util/JobCat;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    iput-wide v1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mEndMs:J

    :cond_1
    return-object p0
.end method

.method public setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 792
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    .line 793
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtrasXml:Ljava/lang/String;

    goto :goto_0

    .line 795
    :cond_0
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0, p1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)V

    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    :goto_0
    return-object p0
.end method

.method public setPeriodic(J)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 1026
    invoke-virtual {p0, p1, p2, p1, p2}, Lcom/evernote/android/job/JobRequest$Builder;->setPeriodic(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setPeriodic(JJ)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 7

    .line 1048
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->getMinInterval()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    const-string v6, "intervalMs"

    move-wide v0, p1

    invoke-static/range {v0 .. v6}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentInRange(JJJLjava/lang/String;)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    .line 1049
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->getMinFlex()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/evernote/android/job/JobRequest$Builder;->mIntervalMs:J

    const-string v6, "flexMs"

    move-wide v0, p3

    invoke-static/range {v0 .. v6}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentInRange(JJJLjava/lang/String;)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mFlexMs:J

    return-object p0
.end method

.method public setRequiredNetworkType(Lcom/evernote/android/job/JobRequest$NetworkType;)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 863
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mNetworkType:Lcom/evernote/android/job/JobRequest$NetworkType;

    return-object p0
.end method

.method public setRequirementsEnforced(Z)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 843
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequirementsEnforced:Z

    return-object p0
.end method

.method public setRequiresBatteryNotLow(Z)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 922
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresBatteryNotLow:Z

    return-object p0
.end method

.method public setRequiresCharging(Z)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 882
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresCharging:Z

    return-object p0
.end method

.method public setRequiresDeviceIdle(Z)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 903
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresDeviceIdle:Z

    return-object p0
.end method

.method public setRequiresStorageNotLow(Z)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 943
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mRequiresStorageNotLow:Z

    return-object p0
.end method

.method public setTransientExtras(Landroid/os/Bundle;)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 1101
    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    .line 1102
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransient:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_1
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    :goto_1
    iput-object v0, p0, Lcom/evernote/android/job/JobRequest$Builder;->mTransientExtras:Landroid/os/Bundle;

    return-object p0
.end method

.method public setUpdateCurrent(Z)Lcom/evernote/android/job/JobRequest$Builder;
    .locals 0

    .line 1083
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest$Builder;->mUpdateCurrent:Z

    return-object p0
.end method

.method public startNow()Lcom/evernote/android/job/JobRequest$Builder;
    .locals 2

    const-wide/16 v0, 0x1

    .line 1009
    invoke-virtual {p0, v0, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setExact(J)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object v0

    return-object v0
.end method
