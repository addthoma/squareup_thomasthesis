.class public final Lcom/evernote/android/job/JobRequest;
.super Ljava/lang/Object;
.source "JobRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/android/job/JobRequest$JobScheduledCallback;,
        Lcom/evernote/android/job/JobRequest$NetworkType;,
        Lcom/evernote/android/job/JobRequest$BackoffPolicy;,
        Lcom/evernote/android/job/JobRequest$Builder;
    }
.end annotation


# static fields
.field private static final CAT:Lcom/evernote/android/job/util/JobCat;

.field public static final DEFAULT_BACKOFF_MS:J = 0x7530L

.field public static final DEFAULT_BACKOFF_POLICY:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

.field public static final DEFAULT_JOB_SCHEDULED_CALLBACK:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

.field public static final DEFAULT_NETWORK_TYPE:Lcom/evernote/android/job/JobRequest$NetworkType;

.field public static final MIN_FLEX:J

.field public static final MIN_INTERVAL:J

.field static final START_NOW:J = 0x1L

.field private static final WINDOW_THRESHOLD_MAX:J = 0x5555555555555554L

.field private static final WINDOW_THRESHOLD_WARNING:J = 0x2aaaaaaaaaaaaaaaL


# instance fields
.field private final mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

.field private mFailureCount:I

.field private mFlexSupport:Z

.field private mLastRun:J

.field private mScheduledAt:J

.field private mStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 54
    sget-object v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->EXPONENTIAL:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    sput-object v0, Lcom/evernote/android/job/JobRequest;->DEFAULT_BACKOFF_POLICY:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 61
    sget-object v0, Lcom/evernote/android/job/JobRequest$NetworkType;->ANY:Lcom/evernote/android/job/JobRequest$NetworkType;

    sput-object v0, Lcom/evernote/android/job/JobRequest;->DEFAULT_NETWORK_TYPE:Lcom/evernote/android/job/JobRequest$NetworkType;

    .line 68
    new-instance v0, Lcom/evernote/android/job/JobRequest$1;

    invoke-direct {v0}, Lcom/evernote/android/job/JobRequest$1;-><init>()V

    sput-object v0, Lcom/evernote/android/job/JobRequest;->DEFAULT_JOB_SCHEDULED_CALLBACK:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

    .line 90
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/evernote/android/job/JobRequest;->MIN_INTERVAL:J

    .line 104
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/evernote/android/job/JobRequest;->MIN_FLEX:J

    .line 109
    new-instance v0, Lcom/evernote/android/job/util/JobCat;

    const-string v1, "JobRequest"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/util/JobCat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/android/job/JobRequest;->CAT:Lcom/evernote/android/job/util/JobCat;

    return-void
.end method

.method private constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;)V
    .locals 0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;Lcom/evernote/android/job/JobRequest$1;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobRequest;-><init>(Lcom/evernote/android/job/JobRequest$Builder;)V

    return-void
.end method

.method static synthetic access$000()Lcom/evernote/android/job/util/JobCat;
    .locals 1

    .line 42
    sget-object v0, Lcom/evernote/android/job/JobRequest;->CAT:Lcom/evernote/android/job/util/JobCat;

    return-object v0
.end method

.method private static context()Landroid/content/Context;
    .locals 1

    .line 122
    invoke-static {}, Lcom/evernote/android/job/JobManager;->instance()Lcom/evernote/android/job/JobManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/android/job/JobManager;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static fromCursor(Landroid/database/Cursor;)Lcom/evernote/android/job/JobRequest;
    .locals 4

    .line 543
    new-instance v0, Lcom/evernote/android/job/JobRequest$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Landroid/database/Cursor;Lcom/evernote/android/job/JobRequest$1;)V

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    const-string v1, "numFailures"

    .line 544
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    const-string v1, "scheduledAt"

    .line 545
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/evernote/android/job/JobRequest;->mScheduledAt:J

    const-string v1, "started"

    .line 546
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/evernote/android/job/JobRequest;->mStarted:Z

    const-string v1, "flexSupport"

    .line 547
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, v0, Lcom/evernote/android/job/JobRequest;->mFlexSupport:Z

    const-string v1, "lastRun"

    .line 548
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/evernote/android/job/JobRequest;->mLastRun:J

    .line 550
    iget p0, v0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    const-string v1, "failure count can\'t be negative"

    invoke-static {p0, v1}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentNonnegative(ILjava/lang/String;)I

    .line 551
    iget-wide v1, v0, Lcom/evernote/android/job/JobRequest;->mScheduledAt:J

    const-string p0, "scheduled at can\'t be negative"

    invoke-static {v1, v2, p0}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentNonnegative(JLjava/lang/String;)J

    return-object v0
.end method

.method static getMinFlex()J
    .locals 3

    .line 116
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->isAllowSmallerIntervalsForMarshmallow()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    sget-wide v0, Lcom/evernote/android/job/JobRequest;->MIN_FLEX:J

    :goto_0
    return-wide v0
.end method

.method static getMinInterval()J
    .locals 3

    .line 112
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->isAllowSmallerIntervalsForMarshmallow()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    sget-wide v0, Lcom/evernote/android/job/JobRequest;->MIN_INTERVAL:J

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public cancelAndEdit()Lcom/evernote/android/job/JobRequest$Builder;
    .locals 9

    .line 478
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest;->mScheduledAt:J

    .line 480
    invoke-static {}, Lcom/evernote/android/job/JobManager;->instance()Lcom/evernote/android/job/JobManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/evernote/android/job/JobManager;->cancel(I)Z

    .line 481
    new-instance v2, Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v3, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;Lcom/evernote/android/job/JobRequest$1;)V

    const/4 v3, 0x0

    .line 482
    iput-boolean v3, p0, Lcom/evernote/android/job/JobRequest;->mStarted:Z

    .line 484
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->isPeriodic()Z

    move-result v3

    if-nez v3, :cond_0

    .line 485
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getClock()Lcom/evernote/android/job/util/Clock;

    move-result-object v3

    invoke-interface {v3}, Lcom/evernote/android/job/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    const-wide/16 v0, 0x1

    .line 487
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getStartMs()J

    move-result-wide v5

    sub-long/2addr v5, v3

    invoke-static {v0, v1, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getEndMs()J

    move-result-wide v7

    sub-long/2addr v7, v3

    invoke-static {v0, v1, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-virtual {v2, v5, v6, v0, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    :cond_0
    return-object v2
.end method

.method createBuilder()Lcom/evernote/android/job/JobRequest$Builder;
    .locals 4

    .line 494
    new-instance v0, Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v1, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;ZLcom/evernote/android/job/JobRequest$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 559
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 561
    :cond_1
    check-cast p1, Lcom/evernote/android/job/JobRequest;

    .line 563
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    iget-object p1, p1, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobRequest$Builder;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public getBackoffMs()J
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$500(Lcom/evernote/android/job/JobRequest$Builder;)J

    move-result-wide v0

    return-wide v0
.end method

.method getBackoffOffset(Z)J
    .locals 6

    .line 291
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->isPeriodic()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    return-wide v1

    .line 296
    :cond_0
    sget-object v0, Lcom/evernote/android/job/JobRequest$3;->$SwitchMap$com$evernote$android$job$JobRequest$BackoffPolicy:[I

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getBackoffPolicy()Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->ordinal()I

    move-result v3

    aget v0, v0, v3

    const/4 v3, 0x1

    if-eq v0, v3, :cond_3

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    .line 302
    iget v0, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    if-nez v0, :cond_1

    goto :goto_0

    .line 305
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getBackoffMs()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v2, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double v0, v0, v2

    double-to-long v1, v0

    goto :goto_0

    .line 310
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "not implemented"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 298
    :cond_3
    iget v0, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    int-to-long v0, v0

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getBackoffMs()J

    move-result-wide v2

    mul-long v1, v0, v2

    :goto_0
    if-eqz p1, :cond_4

    .line 313
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->isExact()Z

    move-result p1

    if-nez p1, :cond_4

    long-to-float p1, v1

    const v0, 0x3f99999a    # 1.2f

    mul-float p1, p1, v0

    float-to-long v1, p1

    .line 317
    :cond_4
    sget-object p1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x5

    invoke-virtual {p1, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getBackoffPolicy()Lcom/evernote/android/job/JobRequest$BackoffPolicy;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$400(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getEndMs()J
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$300(Lcom/evernote/android/job/JobRequest$Builder;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1400(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1500(Lcom/evernote/android/job/JobRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1500(Lcom/evernote/android/job/JobRequest$Builder;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->fromXml(Ljava/lang/String;)Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/evernote/android/job/JobRequest$Builder;->access$1402(Lcom/evernote/android/job/JobRequest$Builder;Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1400(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object v0

    return-object v0
.end method

.method public getFailureCount()I
    .locals 1

    .line 351
    iget v0, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    return v0
.end method

.method public getFlexMs()J
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$700(Lcom/evernote/android/job/JobRequest$Builder;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getIntervalMs()J
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$600(Lcom/evernote/android/job/JobRequest$Builder;)J

    move-result-wide v0

    return-wide v0
.end method

.method getJobApi()Lcom/evernote/android/job/JobApi;
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1700(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/evernote/android/job/JobApi;->V_14:Lcom/evernote/android/job/JobApi;

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/evernote/android/job/JobRequest;->context()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/android/job/JobApi;->getDefault(Landroid/content/Context;)Lcom/evernote/android/job/JobApi;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getJobId()I
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$100(Lcom/evernote/android/job/JobRequest$Builder;)I

    move-result v0

    return v0
.end method

.method public getLastRun()J
    .locals 2

    .line 382
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest;->mLastRun:J

    return-wide v0
.end method

.method public getScheduledAt()J
    .locals 2

    .line 341
    iget-wide v0, p0, Lcom/evernote/android/job/JobRequest;->mScheduledAt:J

    return-wide v0
.end method

.method public getStartMs()J
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$200(Lcom/evernote/android/job/JobRequest$Builder;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v0, v0, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public getTransientExtras()Landroid/os/Bundle;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1900(Lcom/evernote/android/job/JobRequest$Builder;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public hasRequirements()Z
    .locals 2

    .line 262
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresCharging()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresDeviceIdle()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresBatteryNotLow()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresStorageNotLow()Z

    move-result v0

    if-nez v0, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiredNetworkType()Lcom/evernote/android/job/JobRequest$NetworkType;

    move-result-object v0

    sget-object v1, Lcom/evernote/android/job/JobRequest;->DEFAULT_NETWORK_TYPE:Lcom/evernote/android/job/JobRequest$NetworkType;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 568
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$Builder;->hashCode()I

    move-result v0

    return v0
.end method

.method public isExact()Z
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1700(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method isFlexSupport()Z
    .locals 1

    .line 368
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest;->mFlexSupport:Z

    return v0
.end method

.method public isPeriodic()Z
    .locals 5

    .line 193
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getIntervalMs()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isStarted()Z
    .locals 1

    .line 364
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest;->mStarted:Z

    return v0
.end method

.method public isTransient()Z
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1800(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method public isUpdateCurrent()Z
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1600(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method public requiredNetworkType()Lcom/evernote/android/job/JobRequest$NetworkType;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1300(Lcom/evernote/android/job/JobRequest$Builder;)Lcom/evernote/android/job/JobRequest$NetworkType;

    move-result-object v0

    return-object v0
.end method

.method public requirementsEnforced()Z
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$800(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method public requiresBatteryNotLow()Z
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1100(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method public requiresCharging()Z
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$900(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method public requiresDeviceIdle()Z
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1000(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method public requiresStorageNotLow()Z
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$1200(Lcom/evernote/android/job/JobRequest$Builder;)Z

    move-result v0

    return v0
.end method

.method reschedule(ZZ)Lcom/evernote/android/job/JobRequest;
    .locals 3

    .line 498
    new-instance v0, Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v1, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Lcom/evernote/android/job/JobRequest$Builder;ZLcom/evernote/android/job/JobRequest$1;)V

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 500
    iget p1, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p2, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    .line 503
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Lcom/evernote/android/job/JobRequest;->schedule()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 505
    sget-object v0, Lcom/evernote/android/job/JobRequest;->CAT:Lcom/evernote/android/job/util/JobCat;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;)V

    :goto_0
    return-object p2
.end method

.method public schedule()I
    .locals 1

    .line 430
    invoke-static {}, Lcom/evernote/android/job/JobManager;->instance()Lcom/evernote/android/job/JobManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/evernote/android/job/JobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    .line 431
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v0

    return v0
.end method

.method public scheduleAsync()V
    .locals 1

    .line 444
    sget-object v0, Lcom/evernote/android/job/JobRequest;->DEFAULT_JOB_SCHEDULED_CALLBACK:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

    invoke-virtual {p0, v0}, Lcom/evernote/android/job/JobRequest;->scheduleAsync(Lcom/evernote/android/job/JobRequest$JobScheduledCallback;)V

    return-void
.end method

.method public scheduleAsync(Lcom/evernote/android/job/JobRequest$JobScheduledCallback;)V
    .locals 2

    .line 454
    invoke-static {p1}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/evernote/android/job/JobRequest$2;

    invoke-direct {v1, p0, p1}, Lcom/evernote/android/job/JobRequest$2;-><init>(Lcom/evernote/android/job/JobRequest;Lcom/evernote/android/job/JobRequest$JobScheduledCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method setFlexSupport(Z)V
    .locals 0

    .line 372
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest;->mFlexSupport:Z

    return-void
.end method

.method setScheduledAt(J)V
    .locals 0

    .line 325
    iput-wide p1, p0, Lcom/evernote/android/job/JobRequest;->mScheduledAt:J

    return-void
.end method

.method setStarted(Z)V
    .locals 2

    .line 525
    iput-boolean p1, p0, Lcom/evernote/android/job/JobRequest;->mStarted:Z

    .line 526
    new-instance p1, Landroid/content/ContentValues;

    invoke-direct {p1}, Landroid/content/ContentValues;-><init>()V

    .line 527
    iget-boolean v0, p0, Lcom/evernote/android/job/JobRequest;->mStarted:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "started"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 528
    invoke-static {}, Lcom/evernote/android/job/JobManager;->instance()Lcom/evernote/android/job/JobManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/android/job/JobManager;->getJobStorage()Lcom/evernote/android/job/JobStorage;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/evernote/android/job/JobStorage;->update(Lcom/evernote/android/job/JobRequest;Landroid/content/ContentValues;)V

    return-void
.end method

.method toContentValues()Landroid/content/ContentValues;
    .locals 3

    .line 532
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 533
    iget-object v1, p0, Lcom/evernote/android/job/JobRequest;->mBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    invoke-static {v1, v0}, Lcom/evernote/android/job/JobRequest$Builder;->access$2200(Lcom/evernote/android/job/JobRequest$Builder;Landroid/content/ContentValues;)V

    .line 534
    iget v1, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "numFailures"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 535
    iget-wide v1, p0, Lcom/evernote/android/job/JobRequest;->mScheduledAt:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "scheduledAt"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 536
    iget-boolean v1, p0, Lcom/evernote/android/job/JobRequest;->mStarted:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "started"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 537
    iget-boolean v1, p0, Lcom/evernote/android/job/JobRequest;->mFlexSupport:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "flexSupport"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 538
    iget-wide v1, p0, Lcom/evernote/android/job/JobRequest;->mLastRun:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "lastRun"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 573
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", transient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->isTransient()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method updateStats(ZZ)V
    .locals 2

    .line 512
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    if-eqz p1, :cond_0

    .line 514
    iget p1, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    .line 515
    iget p1, p0, Lcom/evernote/android/job/JobRequest;->mFailureCount:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "numFailures"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 518
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getClock()Lcom/evernote/android/job/util/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/evernote/android/job/util/Clock;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/evernote/android/job/JobRequest;->mLastRun:J

    .line 519
    iget-wide p1, p0, Lcom/evernote/android/job/JobRequest;->mLastRun:J

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const-string p2, "lastRun"

    invoke-virtual {v0, p2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 521
    :cond_1
    invoke-static {}, Lcom/evernote/android/job/JobManager;->instance()Lcom/evernote/android/job/JobManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/android/job/JobManager;->getJobStorage()Lcom/evernote/android/job/JobStorage;

    move-result-object p1

    invoke-virtual {p1, p0, v0}, Lcom/evernote/android/job/JobStorage;->update(Lcom/evernote/android/job/JobRequest;Landroid/content/ContentValues;)V

    return-void
.end method
