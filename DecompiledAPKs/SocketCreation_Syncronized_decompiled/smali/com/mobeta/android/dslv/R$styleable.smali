.class public final Lcom/mobeta/android/dslv/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mobeta/android/dslv/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final DragSortListView:[I

.field public static final DragSortListView_click_remove_id:I = 0x0

.field public static final DragSortListView_collapsed_height:I = 0x1

.field public static final DragSortListView_drag_enabled:I = 0x2

.field public static final DragSortListView_drag_handle_id:I = 0x3

.field public static final DragSortListView_drag_scroll_start:I = 0x4

.field public static final DragSortListView_drag_start_mode:I = 0x5

.field public static final DragSortListView_drop_animation_duration:I = 0x6

.field public static final DragSortListView_fling_handle_id:I = 0x7

.field public static final DragSortListView_float_alpha:I = 0x8

.field public static final DragSortListView_float_background_color:I = 0x9

.field public static final DragSortListView_max_drag_scroll_speed:I = 0xa

.field public static final DragSortListView_remove_animation_duration:I = 0xb

.field public static final DragSortListView_remove_enabled:I = 0xc

.field public static final DragSortListView_remove_mode:I = 0xd

.field public static final DragSortListView_slide_shuffle_speed:I = 0xe

.field public static final DragSortListView_sort_enabled:I = 0xf

.field public static final DragSortListView_track_drag_sort:I = 0x10

.field public static final DragSortListView_use_default_controller:I = 0x11


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x12

    new-array v0, v0, [I

    .line 46
    fill-array-data v0, :array_0

    sput-object v0, Lcom/mobeta/android/dslv/R$styleable;->DragSortListView:[I

    return-void

    :array_0
    .array-data 4
        0x7f0400c3
        0x7f0400d1
        0x7f040134
        0x7f040135
        0x7f040136
        0x7f040137
        0x7f04014a
        0x7f040187
        0x7f040188
        0x7f040189
        0x7f0402bc
        0x7f04032c
        0x7f04032d
        0x7f04032e
        0x7f040358
        0x7f04035b
        0x7f040478
        0x7f040490
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
