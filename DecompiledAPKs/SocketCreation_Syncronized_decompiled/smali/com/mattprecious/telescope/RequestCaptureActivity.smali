.class public final Lcom/mattprecious/telescope/RequestCaptureActivity;
.super Landroid/app/Activity;
.source "RequestCaptureActivity.java"


# static fields
.field private static final REQUEST_CODE:I = 0x1

.field public static final RESULT_EXTRA_CODE:Ljava/lang/String; = "code"

.field public static final RESULT_EXTRA_DATA:Ljava/lang/String; = "data"

.field public static final RESULT_EXTRA_PROMPT_SHOWN:Ljava/lang/String; = "prompt-shown"

.field private static final TAG:Ljava/lang/String; = "TelescopeCapture"


# instance fields
.field private requestStartTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static getResultBroadcastAction(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ".telescope.CAPTURE"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private promptShown()Z
    .locals 5

    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/mattprecious/telescope/RequestCaptureActivity;->requestStartTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xc8

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private requestCapture()V
    .locals 3

    const-string v0, "media_projection"

    .line 41
    invoke-virtual {p0, v0}, Lcom/mattprecious/telescope/RequestCaptureActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/projection/MediaProjectionManager;

    .line 42
    invoke-virtual {v0}, Landroid/media/projection/MediaProjectionManager;->createScreenCaptureIntent()Landroid/content/Intent;

    move-result-object v0

    .line 44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mattprecious/telescope/RequestCaptureActivity;->requestStartTime:J

    const/4 v1, 0x1

    .line 45
    invoke-virtual {p0, v0, v1}, Lcom/mattprecious/telescope/RequestCaptureActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 50
    new-instance p1, Landroid/content/Intent;

    invoke-static {p0}, Lcom/mattprecious/telescope/RequestCaptureActivity;->getResultBroadcastAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "code"

    .line 51
    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p2, "data"

    .line 52
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 53
    invoke-direct {p0}, Lcom/mattprecious/telescope/RequestCaptureActivity;->promptShown()Z

    move-result p2

    const-string p3, "prompt-shown"

    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 54
    invoke-virtual {p0, p1}, Lcom/mattprecious/telescope/RequestCaptureActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 55
    invoke-virtual {p0}, Lcom/mattprecious/telescope/RequestCaptureActivity;->finish()V

    return-void

    .line 59
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-ge p1, v0, :cond_0

    const-string p1, "TelescopeCapture"

    const-string v0, "System capture activity started pre-lollipop."

    .line 31
    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-virtual {p0}, Lcom/mattprecious/telescope/RequestCaptureActivity;->finish()V

    return-void

    .line 36
    :cond_0
    invoke-direct {p0}, Lcom/mattprecious/telescope/RequestCaptureActivity;->requestCapture()V

    return-void
.end method
