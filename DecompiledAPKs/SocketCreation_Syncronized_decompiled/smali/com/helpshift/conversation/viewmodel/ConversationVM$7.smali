.class Lcom/helpshift/conversation/viewmodel/ConversationVM$7;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->onAgentTypingUpdate(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

.field final synthetic val$isAgentTyping:Z


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Z)V
    .locals 0

    .line 433
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iput-boolean p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;->val$isAgentTyping:Z

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 438
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;->val$isAgentTyping:Z

    .line 441
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$7;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->updateTypingIndicatorStatus(Z)V

    :cond_1
    return-void
.end method
