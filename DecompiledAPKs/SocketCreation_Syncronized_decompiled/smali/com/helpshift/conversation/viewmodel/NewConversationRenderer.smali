.class public interface abstract Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;
.super Ljava/lang/Object;
.source "NewConversationRenderer.java"


# virtual methods
.method public abstract clearDescriptionError()V
.end method

.method public abstract clearEmailError()V
.end method

.method public abstract clearNameError()V
.end method

.method public abstract disableImageAttachmentClickable()V
.end method

.method public abstract enableImageAttachmentClickable()V
.end method

.method public abstract exit()V
.end method

.method public abstract gotoConversation(J)V
.end method

.method public abstract hideImageAttachmentButton()V
.end method

.method public abstract hideImageAttachmentContainer()V
.end method

.method public abstract hideProfileForm()V
.end method

.method public abstract hideProgressBar()V
.end method

.method public abstract hideStartConversationButton()V
.end method

.method public abstract onAuthenticationFailure()V
.end method

.method public abstract setDescription(Ljava/lang/String;)V
.end method

.method public abstract setEmail(Ljava/lang/String;)V
.end method

.method public abstract setEmailRequired()V
.end method

.method public abstract setName(Ljava/lang/String;)V
.end method

.method public abstract showAttachmentPreviewScreenFromDraft(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
.end method

.method public abstract showConversationStartedMessage()V
.end method

.method public abstract showDescriptionEmptyError()V
.end method

.method public abstract showDescriptionLessThanMinimumError()V
.end method

.method public abstract showDescriptionOnlySpecialCharactersError()V
.end method

.method public abstract showEmailEmptyError()V
.end method

.method public abstract showEmailInvalidError()V
.end method

.method public abstract showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V
.end method

.method public abstract showImageAttachmentButton()V
.end method

.method public abstract showImageAttachmentContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
.end method

.method public abstract showNameEmptyError()V
.end method

.method public abstract showNameOnlySpecialCharactersError()V
.end method

.method public abstract showProfileForm()V
.end method

.method public abstract showProgressBar()V
.end method

.method public abstract showSearchResultFragment(Ljava/util/ArrayList;)V
.end method

.method public abstract showStartConversationButton()V
.end method
