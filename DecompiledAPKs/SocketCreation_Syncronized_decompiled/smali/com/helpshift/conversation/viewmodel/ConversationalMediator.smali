.class Lcom/helpshift/conversation/viewmodel/ConversationalMediator;
.super Lcom/helpshift/conversation/viewmodel/ConversationMediator;
.source "ConversationalMediator.java"


# instance fields
.field private botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

.field private final conversationalMediatorCallback:Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;

.field private isBotExecuting:Z


# direct methods
.method constructor <init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;-><init>(Lcom/helpshift/common/domain/Domain;)V

    .line 18
    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->conversationalMediatorCallback:Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;

    return-void
.end method

.method private getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    return-object v0
.end method

.method private showOptions(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V
    .locals 2

    .line 49
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->type:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->PILL:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    if-ne v0, v1, :cond_0

    .line 50
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showInput(Lcom/helpshift/conversation/activeconversation/message/input/Input;)V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->conversationalMediatorCallback:Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;->showListPicker(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getRenderedBotMessage()Lcom/helpshift/conversation/activeconversation/message/MessageDM;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    return-object v0
.end method

.method protected renderReplyBoxWidget()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-nez v0, :cond_0

    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->isBotExecuting:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->isConversationRejected:Z

    if-nez v0, :cond_3

    .line 65
    sget-object v0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator$1;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->showOptions(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V

    return-void

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    .line 68
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v1

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/TextInput;

    invoke-interface {v1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showInput(Lcom/helpshift/conversation/activeconversation/message/input/Input;)V

    return-void

    .line 77
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showSendReplyUI()V

    goto :goto_1

    .line 81
    :cond_4
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideSendReplyUI()V

    :goto_1
    return-void
.end method

.method public setBotMessageDMToRender(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->botMessageDM:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    return-void
.end method

.method public setIsBotExecuting(Z)V
    .locals 0

    .line 32
    iput-boolean p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->isBotExecuting:Z

    return-void
.end method
