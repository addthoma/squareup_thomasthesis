.class Lcom/helpshift/conversation/viewmodel/ConversationVM$5;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleAppReviewRequestClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

.field final synthetic val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 3

    .line 388
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "reviewUrl"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 389
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->setAppReviewed(Z)V

    .line 391
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/viewmodel/ConversationVM$5$1;

    invoke-direct {v2, p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM$5$1;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationVM$5;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$5;->val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleAppReviewRequestClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V

    return-void
.end method
