.class Lcom/helpshift/conversation/activeconversation/ConversationDM$1;
.super Lcom/helpshift/common/domain/F;
.source "ConversationDM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleAppReviewRequestClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

.field final synthetic val$acceptedAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

.field final synthetic val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->val$acceptedAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    iput-object p3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 3

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->val$acceptedAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;->send(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/activeconversation/ConversationServerInfo;)V

    .line 276
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->handleAcceptedReviewSuccess(Lcom/helpshift/common/platform/Platform;)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 279
    iget-object v1, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->CONVERSATION_ARCHIVED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v1, v2, :cond_0

    .line 280
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;->val$requestAppReviewMessageDM:Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->setIsReviewButtonClickable(Z)V

    .line 284
    throw v0
.end method
