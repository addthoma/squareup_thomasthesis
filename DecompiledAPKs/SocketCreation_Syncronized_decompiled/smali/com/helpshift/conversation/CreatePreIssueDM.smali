.class public Lcom/helpshift/conversation/CreatePreIssueDM;
.super Lcom/helpshift/common/domain/F;
.source "CreatePreIssueDM.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_CrtePreIsue"


# instance fields
.field private final conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

.field private final conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

.field private listener:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;",
            ">;"
        }
    .end annotation
.end field

.field private final sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    .line 33
    iput-object p2, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 34
    iput-object p1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    .line 35
    iput-object p3, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 36
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->listener:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public f()V
    .locals 3

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "conversationGreetingMessage"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v2, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->createPreIssueNetwork(Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateLastUserActivityTime(J)V

    .line 55
    iget-object v0, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->listener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->listener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;

    iget-object v1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;->onCreateConversationSuccess(J)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Helpshift_CrtePreIsue"

    const-string v2, "Error filing a pre-issue"

    .line 61
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    iget-object v1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->listener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getPreIssueId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    iget-object v1, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->listener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;

    invoke-interface {v1, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;->onCreateConversationFailure(Ljava/lang/Exception;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setListener(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V
    .locals 1

    .line 71
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/helpshift/conversation/CreatePreIssueDM;->listener:Ljava/lang/ref/WeakReference;

    return-void
.end method
