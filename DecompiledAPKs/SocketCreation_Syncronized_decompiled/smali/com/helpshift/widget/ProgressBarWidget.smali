.class public Lcom/helpshift/widget/ProgressBarWidget;
.super Lcom/helpshift/widget/Widget;
.source "ProgressBarWidget.java"


# instance fields
.field private isVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/helpshift/widget/ProgressBarWidget;->isVisible:Z

    return-void
.end method


# virtual methods
.method public isVisible()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/helpshift/widget/ProgressBarWidget;->isVisible:Z

    return v0
.end method

.method public setVisible(Z)V
    .locals 0

    .line 12
    iput-boolean p1, p0, Lcom/helpshift/widget/ProgressBarWidget;->isVisible:Z

    .line 13
    invoke-virtual {p0}, Lcom/helpshift/widget/ProgressBarWidget;->notifyChanged()V

    return-void
.end method
