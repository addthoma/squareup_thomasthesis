.class public Lcom/helpshift/widget/ButtonWidget;
.super Lcom/helpshift/widget/Widget;
.source "ButtonWidget.java"


# instance fields
.field private isEnabled:Z

.field private isVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 7
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/helpshift/widget/ButtonWidget;->isEnabled:Z

    .line 10
    iput-boolean v0, p0, Lcom/helpshift/widget/ButtonWidget;->isVisible:Z

    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/helpshift/widget/ButtonWidget;->isEnabled:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/helpshift/widget/ButtonWidget;->isVisible:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 17
    iput-boolean p1, p0, Lcom/helpshift/widget/ButtonWidget;->isEnabled:Z

    .line 18
    invoke-virtual {p0}, Lcom/helpshift/widget/ButtonWidget;->notifyChanged()V

    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/helpshift/widget/ButtonWidget;->isVisible:Z

    .line 27
    invoke-virtual {p0}, Lcom/helpshift/widget/ButtonWidget;->notifyChanged()V

    return-void
.end method
