.class public Lcom/helpshift/common/platform/AndroidPlatform;
.super Ljava/lang/Object;
.source "AndroidPlatform.java"

# interfaces
.implements Lcom/helpshift/common/platform/Platform;


# static fields
.field private static final TAG:Ljava/lang/String; = "AndroidPlatform"


# instance fields
.field private analyticsEventDAO:Lcom/helpshift/analytics/AnalyticsEventDAO;

.field private apiKey:Ljava/lang/String;

.field private appId:Ljava/lang/String;

.field private backupDAO:Lcom/helpshift/common/dao/BackupDAO;

.field private clearedUserDAO:Lcom/helpshift/account/dao/ClearedUserDAO;

.field private final context:Landroid/content/Context;

.field private conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

.field private conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

.field private customIssueFieldDAO:Lcom/helpshift/cif/dao/CustomIssueFieldDAO;

.field private data:Lcom/helpshift/support/HSApiData;

.field private device:Lcom/helpshift/common/platform/Device;

.field private domain:Ljava/lang/String;

.field private downloader:Lcom/helpshift/downloader/SupportDownloader;

.field private faqEventDAO:Lcom/helpshift/faq/dao/FaqEventDAO;

.field private faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

.field private jsonifier:Lcom/helpshift/common/platform/Jsonifier;

.field private legacyAnalyticsEventIDDAO:Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;

.field private legacyProfileMigrationDAO:Lcom/helpshift/migration/LegacyProfileMigrationDAO;

.field private metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

.field private networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

.field private redactionDAO:Lcom/helpshift/redaction/RedactionDAO;

.field private storage:Lcom/helpshift/common/platform/KVStore;

.field private uiContext:Landroid/content/Context;

.field private uiThreader:Lcom/helpshift/common/domain/Threader;

.field private userDAO:Lcom/helpshift/account/dao/UserDAO;

.field private userManagerDAO:Lcom/helpshift/account/dao/AndroidUserManagerDAO;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Lcom/helpshift/common/platform/AndroidPlatform;->apiKey:Ljava/lang/String;

    .line 89
    iput-object p3, p0, Lcom/helpshift/common/platform/AndroidPlatform;->domain:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lcom/helpshift/common/platform/AndroidPlatform;->appId:Ljava/lang/String;

    return-void
.end method

.method private getData()Lcom/helpshift/support/HSApiData;
    .locals 2

    .line 501
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->data:Lcom/helpshift/support/HSApiData;

    if-nez v0, :cond_1

    .line 502
    monitor-enter p0

    .line 503
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->data:Lcom/helpshift/support/HSApiData;

    if-nez v0, :cond_0

    .line 504
    new-instance v0, Lcom/helpshift/support/HSApiData;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/helpshift/support/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->data:Lcom/helpshift/support/HSApiData;

    .line 506
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 508
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->data:Lcom/helpshift/support/HSApiData;

    return-object v0
.end method


# virtual methods
.method public clearNotifications(Ljava/lang/String;)V
    .locals 2

    .line 442
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/helpshift/util/ApplicationUtil;->cancelNotification(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method public compressAndCopyScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/helpshift/common/exception/RootAPIException;
        }
    .end annotation

    .line 391
    :try_start_0
    invoke-static {p1, p2}, Lcom/helpshift/support/util/AttachmentUtil;->copyAttachment(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 394
    invoke-static {p1}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object p1

    throw p1
.end method

.method public compressAndStoreScreenshot(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 372
    :try_start_0
    invoke-static {p1, p2}, Lcom/helpshift/support/util/AttachmentUtil;->copyAttachment(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p2

    :try_start_1
    const-string v0, "AndroidPlatform"

    const-string v1, "Saving attachment"

    .line 376
    invoke-static {v0, v1, p2}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    return-object p1

    .line 381
    :goto_1
    throw p1
.end method

.method public getAPIKey()Ljava/lang/String;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->apiKey:Ljava/lang/String;

    return-object v0
.end method

.method public getAnalyticsEventDAO()Lcom/helpshift/analytics/AnalyticsEventDAO;
    .locals 2

    .line 173
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->analyticsEventDAO:Lcom/helpshift/analytics/AnalyticsEventDAO;

    if-nez v0, :cond_1

    .line 174
    monitor-enter p0

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->analyticsEventDAO:Lcom/helpshift/analytics/AnalyticsEventDAO;

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Lcom/helpshift/support/storage/AndroidAnalyticsEventDAO;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/support/storage/AndroidAnalyticsEventDAO;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->analyticsEventDAO:Lcom/helpshift/analytics/AnalyticsEventDAO;

    .line 178
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 180
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->analyticsEventDAO:Lcom/helpshift/analytics/AnalyticsEventDAO;

    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getBackupDAO()Lcom/helpshift/common/dao/BackupDAO;
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    if-nez v0, :cond_1

    .line 198
    const-class v0, Lcom/helpshift/common/platform/AndroidPlatform;

    monitor-enter v0

    .line 199
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    if-nez v1, :cond_0

    .line 200
    new-instance v1, Lcom/helpshift/common/platform/AndroidBackupDAO;

    invoke-direct {v1}, Lcom/helpshift/common/platform/AndroidBackupDAO;-><init>()V

    iput-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    .line 202
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 204
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    return-object v0
.end method

.method public getCampaignModuleAPIs()Lcom/helpshift/providers/ICampaignsModuleAPIs;
    .locals 1

    .line 447
    invoke-static {}, Lcom/helpshift/providers/CrossModuleDataProvider;->getCampaignModuleAPIs()Lcom/helpshift/providers/ICampaignsModuleAPIs;

    move-result-object v0

    return-object v0
.end method

.method public getClearedUserDAO()Lcom/helpshift/account/dao/ClearedUserDAO;
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->clearedUserDAO:Lcom/helpshift/account/dao/ClearedUserDAO;

    if-nez v0, :cond_1

    .line 280
    monitor-enter p0

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->clearedUserDAO:Lcom/helpshift/account/dao/ClearedUserDAO;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Lcom/helpshift/account/dao/AndroidClearedUserDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/helpshift/account/dao/UserDB;->getInstance(Landroid/content/Context;)Lcom/helpshift/account/dao/UserDB;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/account/dao/AndroidClearedUserDAO;-><init>(Lcom/helpshift/account/dao/UserDB;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->clearedUserDAO:Lcom/helpshift/account/dao/ClearedUserDAO;

    .line 284
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 286
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->clearedUserDAO:Lcom/helpshift/account/dao/ClearedUserDAO;

    return-object v0
.end method

.method public getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    if-nez v0, :cond_1

    .line 138
    monitor-enter p0

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/helpshift/common/platform/AndroidConversationDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidConversationDAO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    .line 142
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 144
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    return-object v0
.end method

.method public getConversationInboxDAO()Lcom/helpshift/conversation/dao/ConversationInboxDAO;
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    if-nez v0, :cond_1

    .line 126
    monitor-enter p0

    .line 127
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/helpshift/common/platform/AndroidConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/helpshift/common/platform/AndroidConversationInboxDAO;-><init>(Landroid/content/Context;Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    .line 130
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 132
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    return-object v0
.end method

.method public getCustomIssueFieldDAO()Lcom/helpshift/cif/dao/CustomIssueFieldDAO;
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->customIssueFieldDAO:Lcom/helpshift/cif/dao/CustomIssueFieldDAO;

    if-nez v0, :cond_1

    .line 186
    monitor-enter p0

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->customIssueFieldDAO:Lcom/helpshift/cif/dao/CustomIssueFieldDAO;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Lcom/helpshift/common/platform/AndroidCustomIssueFieldDAO;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidCustomIssueFieldDAO;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->customIssueFieldDAO:Lcom/helpshift/cif/dao/CustomIssueFieldDAO;

    .line 190
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 192
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->customIssueFieldDAO:Lcom/helpshift/cif/dao/CustomIssueFieldDAO;

    return-object v0
.end method

.method public getDevice()Lcom/helpshift/common/platform/Device;
    .locals 4

    .line 110
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->device:Lcom/helpshift/common/platform/Device;

    if-nez v0, :cond_1

    .line 111
    monitor-enter p0

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->device:Lcom/helpshift/common/platform/Device;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/helpshift/common/platform/AndroidDevice;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v2

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getBackupDAO()Lcom/helpshift/common/dao/BackupDAO;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/helpshift/common/platform/AndroidDevice;-><init>(Landroid/content/Context;Lcom/helpshift/common/platform/KVStore;Lcom/helpshift/common/dao/BackupDAO;)V

    .line 115
    invoke-virtual {v0}, Lcom/helpshift/common/platform/AndroidDevice;->updateDeviceIdInBackupDAO()V

    .line 116
    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->device:Lcom/helpshift/common/platform/Device;

    .line 118
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 120
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->device:Lcom/helpshift/common/platform/Device;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->domain:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloader()Lcom/helpshift/downloader/SupportDownloader;
    .locals 3

    .line 347
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->downloader:Lcom/helpshift/downloader/SupportDownloader;

    if-nez v0, :cond_1

    .line 348
    monitor-enter p0

    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->downloader:Lcom/helpshift/downloader/SupportDownloader;

    if-nez v0, :cond_0

    .line 350
    new-instance v0, Lcom/helpshift/common/platform/AndroidSupportDownloader;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    .line 351
    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/helpshift/common/platform/AndroidSupportDownloader;-><init>(Landroid/content/Context;Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->downloader:Lcom/helpshift/downloader/SupportDownloader;

    .line 353
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 355
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->downloader:Lcom/helpshift/downloader/SupportDownloader;

    return-object v0
.end method

.method public getFAQSearchDM()Lcom/helpshift/faq/domainmodel/FAQSearchDM;
    .locals 2

    .line 219
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    if-nez v0, :cond_1

    .line 220
    monitor-enter p0

    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    if-nez v0, :cond_0

    .line 222
    new-instance v0, Lcom/helpshift/common/platform/AndroidFAQSearchDM;

    invoke-direct {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getData()Lcom/helpshift/support/HSApiData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidFAQSearchDM;-><init>(Lcom/helpshift/support/HSApiData;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    .line 224
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 226
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    return-object v0
.end method

.method public getFAQSuggestionsDAO()Lcom/helpshift/conversation/dao/FAQSuggestionsDAO;
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    if-nez v0, :cond_1

    .line 150
    monitor-enter p0

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Lcom/helpshift/common/platform/AndroidConversationDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidConversationDAO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    .line 154
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 156
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    check-cast v0, Lcom/helpshift/conversation/dao/FAQSuggestionsDAO;

    return-object v0
.end method

.method public getFaqEventDAO()Lcom/helpshift/faq/dao/FaqEventDAO;
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqEventDAO:Lcom/helpshift/faq/dao/FaqEventDAO;

    if-nez v0, :cond_1

    .line 304
    monitor-enter p0

    .line 305
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqEventDAO:Lcom/helpshift/faq/dao/FaqEventDAO;

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Lcom/helpshift/common/platform/AndroidFaqEventDAO;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidFaqEventDAO;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqEventDAO:Lcom/helpshift/faq/dao/FaqEventDAO;

    .line 308
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 310
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->faqEventDAO:Lcom/helpshift/faq/dao/FaqEventDAO;

    return-object v0
.end method

.method public getHTTPTransport()Lcom/helpshift/common/platform/network/HTTPTransport;
    .locals 1

    .line 214
    new-instance v0, Lcom/helpshift/common/platform/AndroidHTTPTransport;

    invoke-direct {v0}, Lcom/helpshift/common/platform/AndroidHTTPTransport;-><init>()V

    return-object v0
.end method

.method public getJsonifier()Lcom/helpshift/common/platform/Jsonifier;
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->jsonifier:Lcom/helpshift/common/platform/Jsonifier;

    if-nez v0, :cond_1

    .line 244
    monitor-enter p0

    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->jsonifier:Lcom/helpshift/common/platform/Jsonifier;

    if-nez v0, :cond_0

    .line 246
    new-instance v0, Lcom/helpshift/common/platform/AndroidJsonifier;

    invoke-direct {v0}, Lcom/helpshift/common/platform/AndroidJsonifier;-><init>()V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->jsonifier:Lcom/helpshift/common/platform/Jsonifier;

    .line 248
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 250
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->jsonifier:Lcom/helpshift/common/platform/Jsonifier;

    return-object v0
.end method

.method public getKVStore()Lcom/helpshift/common/platform/KVStore;
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->storage:Lcom/helpshift/common/platform/KVStore;

    if-nez v0, :cond_1

    .line 232
    monitor-enter p0

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->storage:Lcom/helpshift/common/platform/KVStore;

    if-nez v0, :cond_0

    .line 234
    new-instance v0, Lcom/helpshift/support/storage/SupportKeyValueDBStorage;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/helpshift/support/storage/SupportKeyValueDBStorage;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->storage:Lcom/helpshift/common/platform/KVStore;

    .line 236
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 238
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->storage:Lcom/helpshift/common/platform/KVStore;

    return-object v0
.end method

.method public getLegacyAnalyticsEventIDDAO()Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;
    .locals 2

    .line 478
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyAnalyticsEventIDDAO:Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;

    if-nez v0, :cond_1

    .line 479
    monitor-enter p0

    .line 480
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyAnalyticsEventIDDAO:Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;

    if-nez v0, :cond_0

    .line 481
    new-instance v0, Lcom/helpshift/account/dao/AndroidLegacyAnalyticsEventIDDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/helpshift/account/dao/UserDB;->getInstance(Landroid/content/Context;)Lcom/helpshift/account/dao/UserDB;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/account/dao/AndroidLegacyAnalyticsEventIDDAO;-><init>(Lcom/helpshift/account/dao/UserDB;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyAnalyticsEventIDDAO:Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;

    .line 483
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 485
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyAnalyticsEventIDDAO:Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;

    return-object v0
.end method

.method public getLegacyUserMigrationDataSource()Lcom/helpshift/migration/LegacyProfileMigrationDAO;
    .locals 2

    .line 466
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyProfileMigrationDAO:Lcom/helpshift/migration/LegacyProfileMigrationDAO;

    if-nez v0, :cond_1

    .line 467
    monitor-enter p0

    .line 468
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyProfileMigrationDAO:Lcom/helpshift/migration/LegacyProfileMigrationDAO;

    if-nez v0, :cond_0

    .line 469
    new-instance v0, Lcom/helpshift/account/dao/AndroidLegacyProfileMigrationDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/helpshift/account/dao/UserDB;->getInstance(Landroid/content/Context;)Lcom/helpshift/account/dao/UserDB;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/account/dao/AndroidLegacyProfileMigrationDAO;-><init>(Lcom/helpshift/account/dao/UserDB;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyProfileMigrationDAO:Lcom/helpshift/migration/LegacyProfileMigrationDAO;

    .line 471
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 473
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->legacyProfileMigrationDAO:Lcom/helpshift/migration/LegacyProfileMigrationDAO;

    return-object v0
.end method

.method public getMetaDataDAO()Lcom/helpshift/meta/dao/MetaDataDAO;
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    if-nez v0, :cond_1

    .line 162
    monitor-enter p0

    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Lcom/helpshift/common/platform/AndroidMetadataDAO;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidMetadataDAO;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    .line 166
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 168
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    return-object v0
.end method

.method public getMimeTypeForFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 365
    invoke-static {p1}, Lcom/helpshift/util/FileUtil;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getMinimumConversationDescriptionLength()I
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    .line 407
    :goto_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/helpshift/R$integer;->hs__issue_description_min_chars:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    if-nez v0, :cond_1

    .line 292
    monitor-enter p0

    .line 293
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    if-nez v0, :cond_0

    .line 294
    new-instance v0, Lcom/helpshift/common/platform/AndroidNetworkRequestDAO;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/common/platform/AndroidNetworkRequestDAO;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    .line 296
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 298
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    return-object v0
.end method

.method public getRedactionDAO()Lcom/helpshift/redaction/RedactionDAO;
    .locals 2

    .line 490
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->redactionDAO:Lcom/helpshift/redaction/RedactionDAO;

    if-nez v0, :cond_1

    .line 491
    monitor-enter p0

    .line 492
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->redactionDAO:Lcom/helpshift/redaction/RedactionDAO;

    if-nez v0, :cond_0

    .line 493
    new-instance v0, Lcom/helpshift/account/dao/AndroidRedactionDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/helpshift/account/dao/UserDB;->getInstance(Landroid/content/Context;)Lcom/helpshift/account/dao/UserDB;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/account/dao/AndroidRedactionDAO;-><init>(Lcom/helpshift/account/dao/UserDB;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->redactionDAO:Lcom/helpshift/redaction/RedactionDAO;

    .line 495
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 497
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->redactionDAO:Lcom/helpshift/redaction/RedactionDAO;

    return-object v0
.end method

.method public getResponseParser()Lcom/helpshift/common/platform/network/ResponseParser;
    .locals 1

    .line 209
    new-instance v0, Lcom/helpshift/common/platform/AndroidResponseParser;

    invoke-direct {v0}, Lcom/helpshift/common/platform/AndroidResponseParser;-><init>()V

    return-object v0
.end method

.method public getUIThreader()Lcom/helpshift/common/domain/Threader;
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiThreader:Lcom/helpshift/common/domain/Threader;

    if-nez v0, :cond_1

    .line 321
    monitor-enter p0

    .line 322
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiThreader:Lcom/helpshift/common/domain/Threader;

    if-nez v0, :cond_0

    .line 323
    new-instance v0, Lcom/helpshift/common/platform/AndroidPlatform$1;

    invoke-direct {v0, p0}, Lcom/helpshift/common/platform/AndroidPlatform$1;-><init>(Lcom/helpshift/common/platform/AndroidPlatform;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiThreader:Lcom/helpshift/common/domain/Threader;

    .line 340
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 342
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiThreader:Lcom/helpshift/common/domain/Threader;

    return-object v0
.end method

.method public getUserDAO()Lcom/helpshift/account/dao/UserDAO;
    .locals 2

    .line 267
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userDAO:Lcom/helpshift/account/dao/UserDAO;

    if-nez v0, :cond_1

    .line 268
    monitor-enter p0

    .line 269
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userDAO:Lcom/helpshift/account/dao/UserDAO;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/helpshift/account/dao/AndroidUserDAO;

    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/helpshift/account/dao/UserDB;->getInstance(Landroid/content/Context;)Lcom/helpshift/account/dao/UserDB;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/account/dao/AndroidUserDAO;-><init>(Lcom/helpshift/account/dao/UserDB;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userDAO:Lcom/helpshift/account/dao/UserDAO;

    .line 272
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 274
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userDAO:Lcom/helpshift/account/dao/UserDAO;

    return-object v0
.end method

.method public getUserManagerDAO()Lcom/helpshift/account/dao/UserManagerDAO;
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userManagerDAO:Lcom/helpshift/account/dao/AndroidUserManagerDAO;

    if-nez v0, :cond_1

    .line 256
    monitor-enter p0

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userManagerDAO:Lcom/helpshift/account/dao/AndroidUserManagerDAO;

    if-nez v0, :cond_0

    .line 258
    new-instance v0, Lcom/helpshift/account/dao/AndroidUserManagerDAO;

    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidPlatform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/account/dao/AndroidUserManagerDAO;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userManagerDAO:Lcom/helpshift/account/dao/AndroidUserManagerDAO;

    .line 260
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 262
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->userManagerDAO:Lcom/helpshift/account/dao/AndroidUserManagerDAO;

    return-object v0
.end method

.method public isCurrentThreadUIThread()Z
    .locals 2

    .line 315
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOnline()Z
    .locals 1

    .line 452
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/util/HelpshiftConnectionUtil;->isOnline(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public isSupportedMimeType(Ljava/lang/String;)Z
    .locals 0

    .line 360
    invoke-static {p1}, Lcom/helpshift/util/FileUtil;->isSupportedMimeType(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public setUIContext(Ljava/lang/Object;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 458
    iput-object p1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiContext:Landroid/content/Context;

    goto :goto_0

    .line 460
    :cond_0
    instance-of v0, p1, Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 461
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiContext:Landroid/content/Context;

    :cond_1
    :goto_0
    return-void
.end method

.method public showNotification(Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .line 416
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->uiContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/util/ApplicationUtil;->getContextWithUpdatedLocale(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 424
    :goto_0
    invoke-static {v0, p1, p2, p3, p4}, Lcom/helpshift/support/util/SupportNotification;->createNotification(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 430
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 432
    new-instance p3, Lcom/helpshift/notifications/NotificationChannelsManager;

    iget-object p4, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-direct {p3, p4}, Lcom/helpshift/notifications/NotificationChannelsManager;-><init>(Landroid/content/Context;)V

    .line 433
    sget-object p4, Lcom/helpshift/notifications/NotificationChannelsManager$NotificationChannelType;->SUPPORT:Lcom/helpshift/notifications/NotificationChannelsManager$NotificationChannelType;

    invoke-virtual {p3, p1, p4}, Lcom/helpshift/notifications/NotificationChannelsManager;->attachChannelId(Landroid/app/Notification;Lcom/helpshift/notifications/NotificationChannelsManager$NotificationChannelType;)Landroid/app/Notification;

    move-result-object p1

    .line 434
    iget-object p3, p0, Lcom/helpshift/common/platform/AndroidPlatform;->context:Landroid/content/Context;

    invoke-static {p3, p2, p1}, Lcom/helpshift/util/ApplicationUtil;->showNotification(Landroid/content/Context;Ljava/lang/String;Landroid/app/Notification;)V

    :cond_1
    return-void
.end method
