.class public Lcom/helpshift/common/domain/Domain;
.super Ljava/lang/Object;
.source "Domain.java"


# instance fields
.field private analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

.field private attachmentFileManagerDM:Lcom/helpshift/common/domain/AttachmentFileManagerDM;

.field private authenticationFailureDM:Lcom/helpshift/account/AuthenticationFailureDM;

.field private autoRetryFailedEventDM:Lcom/helpshift/common/AutoRetryFailedEventDM;

.field private conversationInboxManagerDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

.field private cryptoDM:Lcom/helpshift/crypto/CryptoDM;

.field private customIssueFieldDM:Lcom/helpshift/cif/CustomIssueFieldDM;

.field private delayedThreader:Lcom/helpshift/common/domain/DelayedThreader;

.field private errorReportsDM:Lcom/helpshift/logger/ErrorReportsDM;

.field private faqsDM:Lcom/helpshift/faq/FaqsDM;

.field private localeProviderDM:Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

.field private metaDataDM:Lcom/helpshift/meta/MetaDataDM;

.field private parallelThreader:Lcom/helpshift/common/domain/Threader;

.field private final platform:Lcom/helpshift/common/platform/Platform;

.field private sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field private serialThreader:Lcom/helpshift/common/domain/Threader;

.field private uiThreadDelegateDecorator:Lcom/helpshift/delegate/UIThreadDelegateDecorator;

.field private userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

.field private webSocketAuthDM:Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    .line 58
    new-instance p1, Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    invoke-direct {p1, p0}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;-><init>(Lcom/helpshift/common/domain/Domain;)V

    iput-object p1, p0, Lcom/helpshift/common/domain/Domain;->uiThreadDelegateDecorator:Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    return-void
.end method

.method private getDelayedThreader()Lcom/helpshift/common/domain/DelayedThreader;
    .locals 3

    .line 86
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->delayedThreader:Lcom/helpshift/common/domain/DelayedThreader;

    if-nez v0, :cond_1

    .line 87
    monitor-enter p0

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->delayedThreader:Lcom/helpshift/common/domain/DelayedThreader;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 89
    new-instance v1, Lcom/helpshift/common/domain/HSThreadFactory;

    const-string v2, "core-d"

    invoke-direct {v1, v2}, Lcom/helpshift/common/domain/HSThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    .line 90
    new-instance v1, Lcom/helpshift/common/domain/BackgroundDelayedThreader;

    invoke-direct {v1, v0}, Lcom/helpshift/common/domain/BackgroundDelayedThreader;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v1, p0, Lcom/helpshift/common/domain/Domain;->delayedThreader:Lcom/helpshift/common/domain/DelayedThreader;

    .line 92
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 94
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->delayedThreader:Lcom/helpshift/common/domain/DelayedThreader;

    return-object v0
.end method


# virtual methods
.method public getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    if-nez v0, :cond_1

    .line 137
    monitor-enter p0

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    .line 141
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 143
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    return-object v0
.end method

.method public getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->attachmentFileManagerDM:Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    if-nez v0, :cond_1

    .line 224
    monitor-enter p0

    .line 225
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->attachmentFileManagerDM:Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/common/domain/AttachmentFileManagerDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->attachmentFileManagerDM:Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    .line 228
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 230
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->attachmentFileManagerDM:Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    return-object v0
.end method

.method public getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->authenticationFailureDM:Lcom/helpshift/account/AuthenticationFailureDM;

    if-nez v0, :cond_1

    .line 286
    monitor-enter p0

    .line 287
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->authenticationFailureDM:Lcom/helpshift/account/AuthenticationFailureDM;

    if-nez v0, :cond_0

    .line 288
    new-instance v0, Lcom/helpshift/account/AuthenticationFailureDM;

    invoke-direct {v0, p0}, Lcom/helpshift/account/AuthenticationFailureDM;-><init>(Lcom/helpshift/common/domain/Domain;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->authenticationFailureDM:Lcom/helpshift/account/AuthenticationFailureDM;

    .line 290
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 292
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->authenticationFailureDM:Lcom/helpshift/account/AuthenticationFailureDM;

    return-object v0
.end method

.method public getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;
    .locals 4

    .line 264
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->autoRetryFailedEventDM:Lcom/helpshift/common/AutoRetryFailedEventDM;

    if-nez v0, :cond_1

    .line 265
    monitor-enter p0

    .line 266
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->autoRetryFailedEventDM:Lcom/helpshift/common/AutoRetryFailedEventDM;

    if-nez v0, :cond_0

    .line 268
    new-instance v0, Lcom/helpshift/common/poller/HttpBackoff$Builder;

    invoke-direct {v0}, Lcom/helpshift/common/poller/HttpBackoff$Builder;-><init>()V

    const-wide/16 v1, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 269
    invoke-static {v1, v2, v3}, Lcom/helpshift/common/poller/Delay;->of(JLjava/util/concurrent/TimeUnit;)Lcom/helpshift/common/poller/Delay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->setBaseInterval(Lcom/helpshift/common/poller/Delay;)Lcom/helpshift/common/poller/HttpBackoff$Builder;

    move-result-object v0

    const-wide/16 v1, 0x3c

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 270
    invoke-static {v1, v2, v3}, Lcom/helpshift/common/poller/Delay;->of(JLjava/util/concurrent/TimeUnit;)Lcom/helpshift/common/poller/Delay;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->setMaxInterval(Lcom/helpshift/common/poller/Delay;)Lcom/helpshift/common/poller/HttpBackoff$Builder;

    move-result-object v0

    const/16 v1, 0xa

    .line 271
    invoke-virtual {v0, v1}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->setMaxAttempts(I)Lcom/helpshift/common/poller/HttpBackoff$Builder;

    move-result-object v0

    const v1, 0x3dcccccd    # 0.1f

    .line 272
    invoke-virtual {v0, v1}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->setRandomness(F)Lcom/helpshift/common/poller/HttpBackoff$Builder;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 273
    invoke-virtual {v0, v1}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->setMultiplier(F)Lcom/helpshift/common/poller/HttpBackoff$Builder;

    move-result-object v0

    sget-object v1, Lcom/helpshift/common/poller/HttpBackoff$RetryPolicy;->FAILURE:Lcom/helpshift/common/poller/HttpBackoff$RetryPolicy;

    .line 274
    invoke-virtual {v0, v1}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->setRetryPolicy(Lcom/helpshift/common/poller/HttpBackoff$RetryPolicy;)Lcom/helpshift/common/poller/HttpBackoff$Builder;

    move-result-object v0

    .line 275
    invoke-virtual {v0}, Lcom/helpshift/common/poller/HttpBackoff$Builder;->build()Lcom/helpshift/common/poller/HttpBackoff;

    move-result-object v0

    .line 277
    new-instance v1, Lcom/helpshift/common/AutoRetryFailedEventDM;

    iget-object v2, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v1, p0, v2, v0}, Lcom/helpshift/common/AutoRetryFailedEventDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/poller/HttpBackoff;)V

    iput-object v1, p0, Lcom/helpshift/common/domain/Domain;->autoRetryFailedEventDM:Lcom/helpshift/common/AutoRetryFailedEventDM;

    .line 279
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 281
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->autoRetryFailedEventDM:Lcom/helpshift/common/AutoRetryFailedEventDM;

    return-object v0
.end method

.method public getConversationInboxManagerDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->conversationInboxManagerDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    if-nez v0, :cond_1

    .line 114
    monitor-enter p0

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->conversationInboxManagerDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    .line 117
    invoke-virtual {p0}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserManagerDM;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->conversationInboxManagerDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    .line 119
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 121
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->conversationInboxManagerDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    return-object v0
.end method

.method public getCryptoDM()Lcom/helpshift/crypto/CryptoDM;
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->cryptoDM:Lcom/helpshift/crypto/CryptoDM;

    if-nez v0, :cond_1

    .line 180
    monitor-enter p0

    .line 181
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->cryptoDM:Lcom/helpshift/crypto/CryptoDM;

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Lcom/helpshift/crypto/CryptoDM;

    invoke-direct {v0}, Lcom/helpshift/crypto/CryptoDM;-><init>()V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->cryptoDM:Lcom/helpshift/crypto/CryptoDM;

    .line 184
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 186
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->cryptoDM:Lcom/helpshift/crypto/CryptoDM;

    return-object v0
.end method

.method public getCustomIssueFieldDM()Lcom/helpshift/cif/CustomIssueFieldDM;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->customIssueFieldDM:Lcom/helpshift/cif/CustomIssueFieldDM;

    if-nez v0, :cond_1

    .line 169
    monitor-enter p0

    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->customIssueFieldDM:Lcom/helpshift/cif/CustomIssueFieldDM;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/helpshift/cif/CustomIssueFieldDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/cif/CustomIssueFieldDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->customIssueFieldDM:Lcom/helpshift/cif/CustomIssueFieldDM;

    .line 173
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 175
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->customIssueFieldDM:Lcom/helpshift/cif/CustomIssueFieldDM;

    return-object v0
.end method

.method public getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->uiThreadDelegateDecorator:Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    return-object v0
.end method

.method public getErrorReportsDM()Lcom/helpshift/logger/ErrorReportsDM;
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->errorReportsDM:Lcom/helpshift/logger/ErrorReportsDM;

    if-nez v0, :cond_1

    .line 297
    monitor-enter p0

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->errorReportsDM:Lcom/helpshift/logger/ErrorReportsDM;

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Lcom/helpshift/logger/ErrorReportsDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, v1, p0}, Lcom/helpshift/logger/ErrorReportsDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->errorReportsDM:Lcom/helpshift/logger/ErrorReportsDM;

    .line 301
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 303
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->errorReportsDM:Lcom/helpshift/logger/ErrorReportsDM;

    return-object v0
.end method

.method public getFaqsDM()Lcom/helpshift/faq/FaqsDM;
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->faqsDM:Lcom/helpshift/faq/FaqsDM;

    if-nez v0, :cond_1

    .line 191
    monitor-enter p0

    .line 192
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->faqsDM:Lcom/helpshift/faq/FaqsDM;

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Lcom/helpshift/faq/FaqsDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/faq/FaqsDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->faqsDM:Lcom/helpshift/faq/FaqsDM;

    .line 195
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 197
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->faqsDM:Lcom/helpshift/faq/FaqsDM;

    return-object v0
.end method

.method public getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;
    .locals 3

    .line 212
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->localeProviderDM:Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    if-nez v0, :cond_1

    .line 213
    monitor-enter p0

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->localeProviderDM:Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    invoke-virtual {p0}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, v1, v2}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;-><init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->localeProviderDM:Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    .line 217
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 219
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->localeProviderDM:Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    return-object v0
.end method

.method public getMetaDataDM()Lcom/helpshift/meta/MetaDataDM;
    .locals 3

    .line 157
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    if-nez v0, :cond_1

    .line 158
    monitor-enter p0

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/helpshift/meta/MetaDataDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p0}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/helpshift/meta/MetaDataDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    .line 162
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 164
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    return-object v0
.end method

.method public getParallelThreader()Lcom/helpshift/common/domain/Threader;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->parallelThreader:Lcom/helpshift/common/domain/Threader;

    if-nez v0, :cond_1

    .line 75
    monitor-enter p0

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->parallelThreader:Lcom/helpshift/common/domain/Threader;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/helpshift/common/domain/HSThreadFactory;

    const-string v1, "core-p"

    invoke-direct {v0, v1}, Lcom/helpshift/common/domain/HSThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/helpshift/common/domain/BackgroundThreader;

    invoke-direct {v1, v0}, Lcom/helpshift/common/domain/BackgroundThreader;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v1, p0, Lcom/helpshift/common/domain/Domain;->parallelThreader:Lcom/helpshift/common/domain/Threader;

    .line 80
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 82
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->parallelThreader:Lcom/helpshift/common/domain/Threader;

    return-object v0
.end method

.method public getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    if-nez v0, :cond_1

    .line 126
    monitor-enter p0

    .line 127
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 130
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 132
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    return-object v0
.end method

.method public getSerialThreader()Lcom/helpshift/common/domain/Threader;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->serialThreader:Lcom/helpshift/common/domain/Threader;

    if-nez v0, :cond_1

    .line 63
    monitor-enter p0

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->serialThreader:Lcom/helpshift/common/domain/Threader;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/helpshift/common/domain/HSThreadFactory;

    const-string v1, "core-s"

    invoke-direct {v0, v1}, Lcom/helpshift/common/domain/HSThreadFactory;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/helpshift/common/domain/BackgroundThreader;

    invoke-direct {v1, v0}, Lcom/helpshift/common/domain/BackgroundThreader;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v1, p0, Lcom/helpshift/common/domain/Domain;->serialThreader:Lcom/helpshift/common/domain/Threader;

    .line 68
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 70
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->serialThreader:Lcom/helpshift/common/domain/Threader;

    return-object v0
.end method

.method public getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    if-nez v0, :cond_1

    .line 99
    monitor-enter p0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/helpshift/account/domainmodel/UserManagerDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, v1, p0}, Lcom/helpshift/account/domainmodel/UserManagerDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;)V

    .line 102
    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->init()V

    .line 105
    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    .line 107
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 109
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    return-object v0
.end method

.method public getWebSocketAuthDM()Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->webSocketAuthDM:Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;

    if-nez v0, :cond_1

    .line 202
    monitor-enter p0

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->webSocketAuthDM:Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;

    iget-object v1, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/common/domain/Domain;->webSocketAuthDM:Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;

    .line 206
    :cond_0
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 208
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->webSocketAuthDM:Lcom/helpshift/auth/domainmodel/WebSocketAuthDM;

    return-object v0
.end method

.method public runDelayed(Lcom/helpshift/common/domain/F;J)V
    .locals 1

    .line 251
    invoke-direct {p0}, Lcom/helpshift/common/domain/Domain;->getDelayedThreader()Lcom/helpshift/common/domain/DelayedThreader;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/helpshift/common/domain/DelayedThreader;->thread(Lcom/helpshift/common/domain/F;J)Lcom/helpshift/common/domain/F;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    return-void
.end method

.method public runDelayedInParallel(Lcom/helpshift/common/domain/F;J)V
    .locals 1

    .line 255
    new-instance v0, Lcom/helpshift/common/domain/Domain$1;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/common/domain/Domain$1;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/domain/F;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/helpshift/common/domain/Domain;->runDelayed(Lcom/helpshift/common/domain/F;J)V

    return-void
.end method

.method public runOnUI(Lcom/helpshift/common/domain/F;)V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->isCurrentThreadUIThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    goto :goto_0

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getUIThreader()Lcom/helpshift/common/domain/Threader;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/helpshift/common/domain/Threader;->thread(Lcom/helpshift/common/domain/F;)Lcom/helpshift/common/domain/F;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    :goto_0
    return-void
.end method

.method public runParallel(Lcom/helpshift/common/domain/F;)V
    .locals 1

    .line 238
    invoke-virtual {p0}, Lcom/helpshift/common/domain/Domain;->getParallelThreader()Lcom/helpshift/common/domain/Threader;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/helpshift/common/domain/Threader;->thread(Lcom/helpshift/common/domain/F;)Lcom/helpshift/common/domain/F;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    return-void
.end method

.method public runSerial(Lcom/helpshift/common/domain/F;)V
    .locals 1

    .line 234
    invoke-virtual {p0}, Lcom/helpshift/common/domain/Domain;->getSerialThreader()Lcom/helpshift/common/domain/Threader;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/helpshift/common/domain/Threader;->thread(Lcom/helpshift/common/domain/F;)Lcom/helpshift/common/domain/F;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    return-void
.end method

.method public setDelegate(Lcom/helpshift/delegate/RootDelegate;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 152
    iget-object v0, p0, Lcom/helpshift/common/domain/Domain;->uiThreadDelegateDecorator:Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    invoke-virtual {v0, p1}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->setDelegate(Lcom/helpshift/delegate/RootDelegate;)V

    :cond_0
    return-void
.end method
