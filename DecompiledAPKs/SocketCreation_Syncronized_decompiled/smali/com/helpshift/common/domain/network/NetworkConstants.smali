.class public Lcom/helpshift/common/domain/network/NetworkConstants;
.super Ljava/lang/Object;
.source "NetworkConstants.java"


# static fields
.field public static final CONNECT_TIMEOUT:I = 0x1388

.field public static DEFAULT_POOL_SIZE:I = 0x1000

.field public static DEFAULT_REQUEST_MAX_SIZE:I = 0x8

.field public static final POOL_SHUTDOWN_TIMEOUT:I = 0x3c

.field public static final SOCKET_TIMEOUT:I = 0x1388

.field public static SUPPORT_CONFIG_ROUTE:Ljava/lang/String; = "/config/"

.field public static final UPLOAD_CONNECT_TIMEOUT:I = 0x7530

.field public static final apiBase:Ljava/lang/String; = "/api/lib/"

.field public static final apiVersion:Ljava/lang/String; = "3"

.field public static final contentType:Ljava/lang/String; = "application/x-www-form-urlencoded"

.field public static scheme:Ljava/lang/String; = "https://"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
