.class Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;
.super Ljava/lang/Object;
.source "ConversationalFragmentRenderer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showNetworkErrorFooter(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;

.field final synthetic val$networkConnectivityProgress:Landroid/widget/ProgressBar;

.field final synthetic val$networkErrorIcon:Landroid/widget/ImageView;

.field final synthetic val$networkErrorText:Landroid/widget/TextView;

.field final synthetic val$resources:Landroid/content/res/Resources;

.field final synthetic val$tapToRetry:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;Landroid/widget/TextView;Landroid/content/res/Resources;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->this$0:Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;

    iput-object p2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$networkErrorText:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$resources:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$tapToRetry:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$networkErrorIcon:Landroid/widget/ImageView;

    iput-object p6, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$networkConnectivityProgress:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 241
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$networkErrorText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$resources:Landroid/content/res/Resources;

    sget v1, Lcom/helpshift/R$string;->hs__connecting:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$tapToRetry:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$networkErrorIcon:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 244
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->val$networkConnectivityProgress:Landroid/widget/ProgressBar;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 245
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;->this$0:Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;

    iget-object p1, p1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

    invoke-interface {p1}, Lcom/helpshift/support/conversations/ConversationFragmentRouter;->retryPreIssueCreation()V

    return-void
.end method
