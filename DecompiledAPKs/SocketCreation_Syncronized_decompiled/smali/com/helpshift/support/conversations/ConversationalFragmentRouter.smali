.class public interface abstract Lcom/helpshift/support/conversations/ConversationalFragmentRouter;
.super Ljava/lang/Object;
.source "ConversationalFragmentRouter.java"


# virtual methods
.method public abstract handleOptionSelectedForPicker(Lcom/helpshift/conversation/viewmodel/OptionUIModel;Z)V
.end method

.method public abstract onListPickerSearchQueryChange(Ljava/lang/String;)V
.end method

.method public abstract resetToolbarImportanceForAccessibility()V
.end method

.method public abstract setToolbarImportanceForAccessibility(I)V
.end method
