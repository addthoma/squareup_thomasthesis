.class public Lcom/felhr/usbserial/XdcVcpSerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "XdcVcpSerialDevice.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final CLASS_ID:Ljava/lang/String;

.field private static final DEFAULT_BAUDRATE:I = 0x1c200

.field private static final XDCVCP_GET_LINE_CTL:I = 0x4

.field private static final XDCVCP_IFC_ENABLE:I = 0x0

.field private static final XDCVCP_LINE_CTL_DEFAULT:I = 0x800

.field private static final XDCVCP_MHS_ALL:I = 0x11

.field private static final XDCVCP_MHS_DEFAULT:I = 0x0

.field private static final XDCVCP_MHS_DTR:I = 0x1

.field private static final XDCVCP_MHS_RTS:I = 0x10

.field private static final XDCVCP_REQTYPE_DEVICE2HOST:I = 0xc1

.field private static final XDCVCP_REQTYPE_HOST2DEVICE:I = 0x41

.field private static final XDCVCP_SET_BAUDDIV:I = 0x1

.field private static final XDCVCP_SET_BAUDRATE:I = 0x1e

.field private static final XDCVCP_SET_CHARS:I = 0x19

.field private static final XDCVCP_SET_FLOW:I = 0x13

.field private static final XDCVCP_SET_LINE_CTL:I = 0x3

.field private static final XDCVCP_SET_MHS:I = 0x7

.field private static final XDCVCP_SET_XOFF:I = 0xa

.field private static final XDCVCP_SET_XON:I = 0x9

.field private static final XDCVCP_UART_DISABLE:I = 0x0

.field private static final XDCVCP_UART_ENABLE:I = 0x1

.field private static final XDCVCP_XOFF:I

.field private static final XDCVCP_XON:I


# instance fields
.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    const-class v0, Lcom/felhr/usbserial/XdcVcpSerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->CLASS_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    const/4 v0, -0x1

    .line 58
    invoke-direct {p0, p1, p2, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    if-ltz p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 65
    :goto_0
    invoke-virtual {p1, p3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void
.end method

.method private getCTL()[B
    .locals 9

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 369
    iget-object v1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v5

    array-length v7, v0

    const/16 v2, 0xc1

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v8, 0x0

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v1

    .line 370
    sget-object v2, Lcom/felhr/usbserial/XdcVcpSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Control Transfer Response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private setControlCommand(II[B)I
    .locals 9

    if-eqz p3, :cond_0

    .line 359
    array-length v0, p3

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 361
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0x41

    iget-object v0, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v5

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 362
    sget-object p2, Lcom/felhr/usbserial/XdcVcpSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Control Transfer Response: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 124
    invoke-direct {p0, v0, v0, v1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    .line 125
    invoke-virtual {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->killWorkingThread()V

    .line 126
    invoke-virtual {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->killWriteThread()V

    .line 127
    iget-object v0, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    return-void
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    return-void
.end method

.method public open()Z
    .locals 7

    .line 73
    invoke-virtual {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->restartWorkingThread()V

    .line 74
    invoke-virtual {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->restartWriteThread()V

    .line 76
    iget-object v0, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 78
    sget-object v0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_1

    .line 89
    iget-object v4, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 90
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 91
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v6, 0x80

    if-ne v5, v6, :cond_0

    .line 93
    iput-object v4, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 96
    :cond_0
    iput-object v4, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, v1, v2, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    move-result v3

    if-gez v3, :cond_2

    return v1

    :cond_2
    const v3, 0x1c200

    .line 104
    invoke-virtual {p0, v3}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setBaudRate(I)V

    const/4 v3, 0x3

    const/16 v4, 0x800

    .line 105
    invoke-direct {p0, v3, v4, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    move-result v3

    if-gez v3, :cond_3

    return v1

    .line 107
    :cond_3
    invoke-virtual {p0, v1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setFlowControl(I)V

    const/4 v3, 0x7

    .line 108
    invoke-direct {p0, v3, v1, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    move-result v0

    if-gez v0, :cond_4

    return v1

    .line 112
    :cond_4
    new-instance v0, Landroid/hardware/usb/UsbRequest;

    invoke-direct {v0}, Landroid/hardware/usb/UsbRequest;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v3}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 116
    iget-object v1, p0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    return v2

    .line 81
    :cond_5
    sget-object v0, Lcom/felhr/usbserial/XdcVcpSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method public setBaudRate(I)V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    const/16 p1, 0x1e

    .line 151
    invoke-direct {p0, p1, v2, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setBreak(Z)V
    .locals 0

    return-void
.end method

.method public setDTR(Z)V
    .locals 0

    return-void
.end method

.method public setDataBits(I)V
    .locals 4

    .line 157
    invoke-direct {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->getCTL()[B

    move-result-object v0

    const/4 v1, 0x5

    const/16 v2, 0x8

    const/4 v3, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x6

    if-eq p1, v1, :cond_2

    const/4 v1, 0x7

    if-eq p1, v1, :cond_1

    if-eq p1, v2, :cond_0

    return-void

    .line 170
    :cond_0
    aput-byte v2, v0, v3

    goto :goto_0

    .line 167
    :cond_1
    aput-byte v1, v0, v3

    goto :goto_0

    .line 164
    :cond_2
    aput-byte v1, v0, v3

    goto :goto_0

    .line 161
    :cond_3
    aput-byte v1, v0, v3

    .line 175
    :goto_0
    aget-byte p1, v0, v3

    shl-int/2addr p1, v2

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    or-int/2addr p1, v0

    int-to-byte p1, p1

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 176
    invoke-direct {p0, v0, p1, v1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setFlowControl(I)V
    .locals 4

    const/16 v0, 0x13

    const/16 v1, 0x10

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    const/4 v3, 0x1

    if-eq p1, v3, :cond_2

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x3

    if-eq p1, v3, :cond_0

    return-void

    :cond_0
    new-array p1, v1, [B

    .line 281
    fill-array-data p1, :array_0

    const/4 v1, 0x6

    new-array v1, v1, [B

    .line 288
    fill-array-data v1, :array_1

    const/16 v3, 0x19

    .line 292
    invoke-direct {p0, v3, v2, v1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    .line 293
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_1
    new-array p1, v1, [B

    .line 272
    fill-array-data p1, :array_2

    .line 278
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_2
    new-array p1, v1, [B

    .line 263
    fill-array-data p1, :array_3

    .line 269
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_3
    new-array p1, v1, [B

    .line 254
    fill-array-data p1, :array_4

    .line 260
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    :goto_0
    return-void

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x0t
        0x43t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x11t
        0x13t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x12t
        0x0t
        0x0t
        0x0t
        0x40t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x9t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data

    :array_4
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x0t
        0x40t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data
.end method

.method public setParity(I)V
    .locals 5

    .line 208
    invoke-direct {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->getCTL()[B

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_4

    if-eq p1, v2, :cond_3

    const/4 v4, 0x2

    if-eq p1, v4, :cond_2

    if-eq p1, v1, :cond_1

    const/4 v4, 0x4

    if-eq p1, v4, :cond_0

    return-void

    .line 236
    :cond_0
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x11

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 237
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x21

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 238
    aget-byte p1, v0, v3

    or-int/lit8 p1, p1, 0x40

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 239
    aget-byte p1, v0, v3

    and-int/lit16 p1, p1, -0x81

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto :goto_0

    .line 230
    :cond_1
    aget-byte p1, v0, v3

    or-int/lit8 p1, p1, 0x10

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 231
    aget-byte p1, v0, v3

    or-int/lit8 p1, p1, 0x20

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 232
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x41

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 233
    aget-byte p1, v0, v3

    and-int/lit16 p1, p1, -0x81

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto :goto_0

    .line 224
    :cond_2
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x11

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 225
    aget-byte p1, v0, v3

    or-int/lit8 p1, p1, 0x20

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 226
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x41

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 227
    aget-byte p1, v0, v3

    and-int/lit16 p1, p1, -0x81

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto :goto_0

    .line 218
    :cond_3
    aget-byte p1, v0, v3

    or-int/lit8 p1, p1, 0x10

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 219
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x21

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 220
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x41

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 221
    aget-byte p1, v0, v3

    and-int/lit16 p1, p1, -0x81

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto :goto_0

    .line 212
    :cond_4
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x11

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 213
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x21

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 214
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x41

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 215
    aget-byte p1, v0, v3

    and-int/lit16 p1, p1, -0x81

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 244
    :goto_0
    aget-byte p1, v0, v2

    shl-int/lit8 p1, p1, 0x8

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    or-int/2addr p1, v0

    int-to-byte p1, p1

    const/4 v0, 0x0

    .line 245
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setRTS(Z)V
    .locals 0

    return-void
.end method

.method public setStopBits(I)V
    .locals 5

    .line 183
    invoke-direct {p0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->getCTL()[B

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq p1, v2, :cond_2

    const/4 v4, 0x2

    if-eq p1, v4, :cond_1

    if-eq p1, v1, :cond_0

    return-void

    .line 191
    :cond_0
    aget-byte p1, v0, v3

    or-int/2addr p1, v2

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 192
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x3

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto :goto_0

    .line 195
    :cond_1
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x2

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 196
    aget-byte p1, v0, v3

    or-int/2addr p1, v4

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    goto :goto_0

    .line 187
    :cond_2
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x2

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 188
    aget-byte p1, v0, v3

    and-int/lit8 p1, p1, -0x3

    int-to-byte p1, p1

    aput-byte p1, v0, v3

    .line 201
    :goto_0
    aget-byte p1, v0, v2

    shl-int/lit8 p1, p1, 0x8

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    or-int/2addr p1, v0

    int-to-byte p1, p1

    const/4 v0, 0x0

    .line 202
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/XdcVcpSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public syncClose()V
    .locals 0

    return-void
.end method

.method public syncOpen()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
