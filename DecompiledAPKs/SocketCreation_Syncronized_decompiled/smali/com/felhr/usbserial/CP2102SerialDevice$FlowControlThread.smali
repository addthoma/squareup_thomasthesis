.class Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "CP2102SerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/CP2102SerialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlowControlThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

.field private final time:J


# direct methods
.method private constructor <init>(Lcom/felhr/usbserial/CP2102SerialDevice;)V
    .locals 2

    .line 407
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    const-wide/16 v0, 0x28

    .line 409
    iput-wide v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->time:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/felhr/usbserial/CP2102SerialDevice;Lcom/felhr/usbserial/CP2102SerialDevice$1;)V
    .locals 0

    .line 407
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;-><init>(Lcom/felhr/usbserial/CP2102SerialDevice;)V

    return-void
.end method

.method private pollLines()[B
    .locals 2

    .line 493
    monitor-enter p0

    const-wide/16 v0, 0x28

    .line 497
    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 500
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 502
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$1100(Lcom/felhr/usbserial/CP2102SerialDevice;)[B

    move-result-object v0

    return-object v0

    .line 502
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method


# virtual methods
.method public doRun()V
    .locals 7

    .line 414
    iget-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->firstTime:Z

    const/4 v1, 0x0

    if-nez v0, :cond_8

    .line 416
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->pollLines()[B

    move-result-object v0

    .line 417
    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v2}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$000(Lcom/felhr/usbserial/CP2102SerialDevice;)[B

    move-result-object v2

    .line 420
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$100(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v3

    const/16 v4, 0x10

    const/4 v5, 0x1

    if-eqz v3, :cond_1

    .line 422
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$200(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v3

    aget-byte v6, v0, v1

    and-int/2addr v6, v4

    if-ne v6, v4, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-eq v3, v6, :cond_1

    .line 424
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$200(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v6

    xor-int/2addr v6, v5

    invoke-static {v3, v6}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$202(Lcom/felhr/usbserial/CP2102SerialDevice;Z)Z

    .line 425
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$300(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 426
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$300(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v3

    iget-object v6, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v6}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$200(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v6

    invoke-interface {v3, v6}, Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;->onCTSChanged(Z)V

    .line 431
    :cond_1
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$400(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 433
    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$500(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v3

    aget-byte v0, v0, v1

    const/16 v6, 0x20

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eq v3, v0, :cond_3

    .line 435
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$500(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v3

    xor-int/2addr v3, v5

    invoke-static {v0, v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$502(Lcom/felhr/usbserial/CP2102SerialDevice;Z)Z

    .line 436
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$600(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 437
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$600(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    iget-object v3, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$500(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;->onDSRChanged(Z)V

    .line 442
    :cond_3
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$700(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 444
    aget-byte v0, v2, v1

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_4

    .line 446
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$700(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;->onParityError()V

    .line 451
    :cond_4
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$800(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 453
    aget-byte v0, v2, v1

    const/4 v3, 0x2

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_5

    .line 455
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$800(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;->onFramingError()V

    .line 460
    :cond_5
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$900(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 462
    aget-byte v0, v2, v1

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_6

    .line 464
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$900(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;->onBreakInterrupt()V

    .line 470
    :cond_6
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$1000(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 472
    aget-byte v0, v2, v1

    const/4 v3, 0x4

    and-int/2addr v0, v3

    if-eq v0, v3, :cond_7

    aget-byte v0, v2, v1

    const/16 v1, 0x8

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_b

    .line 475
    :cond_7
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$1000(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;->onOverrunError()V

    goto :goto_2

    .line 481
    :cond_8
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$100(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$300(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 482
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$300(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    move-result-object v0

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v2}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$200(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v2

    invoke-interface {v0, v2}, Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;->onCTSChanged(Z)V

    .line 484
    :cond_9
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$400(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$600(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 485
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$600(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    move-result-object v0

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->this$0:Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-static {v2}, Lcom/felhr/usbserial/CP2102SerialDevice;->access$500(Lcom/felhr/usbserial/CP2102SerialDevice;)Z

    move-result v2

    invoke-interface {v0, v2}, Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;->onDSRChanged(Z)V

    .line 487
    :cond_a
    iput-boolean v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->firstTime:Z

    :cond_b
    :goto_2
    return-void
.end method
