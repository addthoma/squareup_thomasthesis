.class public Lcom/felhr/usbserial/CH34xSerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "CH34xSerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;
    }
.end annotation


# static fields
.field private static final CH341_NBREAK_BITS_REG1:I = 0x1

.field private static final CH341_NBREAK_BITS_REG2:I = 0x40

.field private static final CH341_REG_BREAK1:I = 0x5

.field private static final CH341_REG_BREAK2:I = 0x18

.field private static final CH341_REQ_READ_REG:I = 0x95

.field private static final CH341_REQ_WRITE_REG:I = 0x9a

.field private static final CH34X_115200_1312:I = 0xcc83

.field private static final CH34X_1200_0f2c:I = 0x3b

.field private static final CH34X_1200_1312:I = 0xb281

.field private static final CH34X_1228800_0f2c:I = 0x21

.field private static final CH34X_1228800_1312:I = 0xfb03

.field private static final CH34X_19200_0f2c_rest:I = 0x7

.field private static final CH34X_19200_1312:I = 0xd982

.field private static final CH34X_2000000_0f2c:I = 0x2

.field private static final CH34X_2000000_1312:I = 0xfd03

.field private static final CH34X_230400_1312:I = 0xe683

.field private static final CH34X_2400_0f2c:I = 0x1e

.field private static final CH34X_2400_1312:I = 0xd981

.field private static final CH34X_300_0f2c:I = 0xeb

.field private static final CH34X_300_1312:I = 0xd980

.field private static final CH34X_38400_1312:I = 0x6483

.field private static final CH34X_460800_1312:I = 0xf383

.field private static final CH34X_4800_0f2c:I = 0xf

.field private static final CH34X_4800_1312:I = 0x6482

.field private static final CH34X_57600_1312:I = 0x9883

.field private static final CH34X_600_0f2c:I = 0x76

.field private static final CH34X_600_1312:I = 0x6481

.field private static final CH34X_921600_1312:I = 0xf387

.field private static final CH34X_9600_0f2c:I = 0x8

.field private static final CH34X_9600_1312:I = 0xb282

.field private static final CH34X_FLOW_CONTROL_DSR_DTR:I = 0x202

.field private static final CH34X_FLOW_CONTROL_NONE:I = 0x0

.field private static final CH34X_FLOW_CONTROL_RTS_CTS:I = 0x101

.field private static final CH34X_PARITY_EVEN:I = 0xdb

.field private static final CH34X_PARITY_MARK:I = 0xeb

.field private static final CH34X_PARITY_NONE:I = 0xc3

.field private static final CH34X_PARITY_ODD:I = 0xcb

.field private static final CH34X_PARITY_SPACE:I = 0xfb

.field private static final CLASS_ID:Ljava/lang/String;

.field private static final DEFAULT_BAUDRATE:I = 0x2580

.field private static final REQTYPE_HOST_FROM_DEVICE:I = 0xc0

.field private static final REQTYPE_HOST_TO_DEVICE:I = 0x40


# instance fields
.field private ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

.field private ctsState:Z

.field private dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

.field private dsrState:Z

.field private dtr:Z

.field private dtrDsrEnabled:Z

.field private flowControlThread:Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private rts:Z

.field private rtsCtsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    const-class v0, Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p1, 0x0

    .line 98
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtr:Z

    .line 99
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rts:Z

    .line 100
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsState:Z

    .line 101
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrState:Z

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 0

    .line 111
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x0

    .line 98
    iput-boolean p2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtr:Z

    .line 99
    iput-boolean p2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rts:Z

    .line 100
    iput-boolean p2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsState:Z

    .line 101
    iput-boolean p2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrState:Z

    .line 112
    iput-boolean p2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rtsCtsEnabled:Z

    .line 113
    iput-boolean p2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtrDsrEnabled:Z

    if-ltz p3, :cond_0

    move p2, p3

    .line 114
    :cond_0
    invoke-virtual {p1, p2}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void
.end method

.method static synthetic access$100(Lcom/felhr/usbserial/CH34xSerialDevice;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rtsCtsEnabled:Z

    return p0
.end method

.method static synthetic access$200(Lcom/felhr/usbserial/CH34xSerialDevice;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsState:Z

    return p0
.end method

.method static synthetic access$202(Lcom/felhr/usbserial/CH34xSerialDevice;Z)Z
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsState:Z

    return p1
.end method

.method static synthetic access$300(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    return-object p0
.end method

.method static synthetic access$400(Lcom/felhr/usbserial/CH34xSerialDevice;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtrDsrEnabled:Z

    return p0
.end method

.method static synthetic access$500(Lcom/felhr/usbserial/CH34xSerialDevice;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrState:Z

    return p0
.end method

.method static synthetic access$502(Lcom/felhr/usbserial/CH34xSerialDevice;Z)Z
    .locals 0

    .line 19
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrState:Z

    return p1
.end method

.method static synthetic access$600(Lcom/felhr/usbserial/CH34xSerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    return-object p0
.end method

.method static synthetic access$700(Lcom/felhr/usbserial/CH34xSerialDevice;)Z
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkCTS()Z

    move-result p0

    return p0
.end method

.method static synthetic access$800(Lcom/felhr/usbserial/CH34xSerialDevice;)Z
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkDSR()Z

    move-result p0

    return p0
.end method

.method private checkCTS()Z
    .locals 5

    const/4 v0, 0x2

    new-array v1, v0, [B

    const/4 v2, 0x0

    const/16 v3, 0x95

    const/16 v4, 0x706

    .line 540
    invoke-direct {p0, v3, v4, v2, v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandIn(III[B)I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 544
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected 2 bytes, but get "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    .line 548
    :cond_0
    aget-byte v0, v1, v2

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    return v1

    :cond_1
    return v2
.end method

.method private checkDSR()Z
    .locals 5

    const/4 v0, 0x2

    new-array v1, v0, [B

    const/4 v2, 0x0

    const/16 v3, 0x95

    const/16 v4, 0x706

    .line 560
    invoke-direct {p0, v3, v4, v2, v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandIn(III[B)I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 564
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected 2 bytes, but get "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    .line 568
    :cond_0
    aget-byte v1, v1, v2

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v2
.end method

.method private checkState(Ljava/lang/String;II[I)I
    .locals 2

    .line 524
    array-length v0, p4

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 525
    invoke-direct {p0, p2, p3, v1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandIn(III[B)I

    move-result p2

    .line 527
    array-length p3, p4

    if-eq p2, p3, :cond_0

    .line 529
    sget-object p3, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p4, p4

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, " bytes, but get "

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " ["

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, -0x1

    return p1

    :cond_0
    return v1
.end method

.method private createFlowControlThread()V
    .locals 2

    .line 613
    new-instance v0, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;-><init>(Lcom/felhr/usbserial/CH34xSerialDevice;Lcom/felhr/usbserial/CH34xSerialDevice$1;)V

    iput-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->flowControlThread:Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

    return-void
.end method

.method private init()I
    .locals 10

    const/4 v0, 0x0

    const/16 v1, 0xa1

    const v2, 0xc29c

    const v3, 0xb2b9

    .line 432
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v1

    const/4 v2, -0x1

    if-gez v1, :cond_0

    .line 434
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #1"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_0
    const/16 v1, 0xdf

    const/16 v3, 0xa4

    const/4 v4, 0x0

    .line 438
    invoke-direct {p0, v3, v1, v4, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v1

    if-gez v1, :cond_1

    .line 440
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #2"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_1
    const/16 v1, 0x9f

    .line 444
    invoke-direct {p0, v3, v1, v4, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v1

    if-gez v1, :cond_2

    .line 446
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #3"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_2
    const/4 v1, 0x2

    new-array v3, v1, [I

    .line 450
    fill-array-data v3, :array_0

    const/16 v5, 0x706

    const/16 v6, 0x95

    const-string v7, "init #4"

    invoke-direct {p0, v7, v6, v5, v3}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkState(Ljava/lang/String;II[I)I

    move-result v3

    if-ne v3, v2, :cond_3

    return v2

    :cond_3
    const/16 v3, 0x2727

    const/16 v7, 0x9a

    .line 453
    invoke-direct {p0, v7, v3, v4, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v8

    if-gez v8, :cond_4

    .line 455
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #5"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_4
    const/16 v8, 0x1312

    const v9, 0xb282

    .line 459
    invoke-direct {p0, v7, v8, v9, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v8

    if-gez v8, :cond_5

    .line 461
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #6"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_5
    const/16 v8, 0xf2c

    const/16 v9, 0x8

    .line 465
    invoke-direct {p0, v7, v8, v9, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v8

    if-gez v8, :cond_6

    .line 467
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #7"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_6
    const/16 v8, 0x2518

    const/16 v9, 0xc3

    .line 471
    invoke-direct {p0, v7, v8, v9, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v8

    if-gez v8, :cond_7

    .line 473
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #8"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_7
    new-array v1, v1, [I

    .line 477
    fill-array-data v1, :array_1

    const-string v8, "init #9"

    invoke-direct {p0, v8, v6, v5, v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkState(Ljava/lang/String;II[I)I

    move-result v1

    if-ne v1, v2, :cond_8

    return v2

    .line 480
    :cond_8
    invoke-direct {p0, v7, v3, v4, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v0

    if-gez v0, :cond_9

    .line 482
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "init failed! #10"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_9
    return v4

    :array_0
    .array-data 4
        0x9f
        0xee
    .end array-data

    :array_1
    .array-data 4
        0x9f
        0xee
    .end array-data
.end method

.method private openCH34X()Z
    .locals 8

    .line 398
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 400
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_2

    .line 411
    iget-object v4, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 412
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 413
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v7, 0x80

    if-ne v5, v7, :cond_0

    .line 415
    iput-object v4, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 416
    :cond_0
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    if-ne v5, v6, :cond_1

    .line 417
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    if-nez v5, :cond_1

    .line 419
    iput-object v4, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 423
    :cond_2
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->init()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1

    .line 403
    :cond_4
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private setBaudRate(II)I
    .locals 5

    const/4 v0, 0x0

    const/16 v1, 0x9a

    const/16 v2, 0x1312

    .line 491
    invoke-direct {p0, v1, v2, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result p1

    const/4 v2, -0x1

    if-gez p1, :cond_0

    return v2

    :cond_0
    const/16 p1, 0xf2c

    .line 493
    invoke-direct {p0, v1, p1, p2, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result p1

    if-gez p1, :cond_1

    return v2

    :cond_1
    const/16 p1, 0x95

    const/16 p2, 0x706

    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 495
    fill-array-data v3, :array_0

    const-string v4, "set_baud_rate"

    invoke-direct {p0, v4, p1, p2, v3}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkState(Ljava/lang/String;II[I)I

    move-result p1

    if-ne p1, v2, :cond_2

    return v2

    :cond_2
    const/16 p1, 0x2727

    const/4 p2, 0x0

    .line 497
    invoke-direct {p0, v1, p1, p2, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result p1

    if-gez p1, :cond_3

    return v2

    :cond_3
    return p2

    :array_0
    .array-data 4
        0x9f
        0xee
    .end array-data
.end method

.method private setCh340xFlow(I)I
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 515
    fill-array-data v0, :array_0

    const-string v1, "set_flow_control"

    const/16 v2, 0x95

    const/16 v3, 0x706

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkState(Ljava/lang/String;II[I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    const/16 v0, 0x9a

    const/16 v2, 0x2727

    const/4 v3, 0x0

    .line 517
    invoke-direct {p0, v0, v2, p1, v3}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result p1

    if-ne p1, v1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1

    :array_0
    .array-data 4
        0x9f
        0xee
    .end array-data
.end method

.method private setCh340xParity(I)I
    .locals 6

    const/4 v0, 0x0

    const/16 v1, 0x9a

    const/16 v2, 0x2518

    .line 504
    invoke-direct {p0, v1, v2, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result p1

    const/4 v2, -0x1

    if-gez p1, :cond_0

    return v2

    :cond_0
    const/16 p1, 0x95

    const/16 v3, 0x706

    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 506
    fill-array-data v4, :array_0

    const-string v5, "set_parity"

    invoke-direct {p0, v5, p1, v3, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkState(Ljava/lang/String;II[I)I

    move-result p1

    if-ne p1, v2, :cond_1

    return v2

    :cond_1
    const/16 p1, 0x2727

    const/4 v3, 0x0

    .line 508
    invoke-direct {p0, v1, p1, v3, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result p1

    if-gez p1, :cond_2

    return v2

    :cond_2
    return v3

    nop

    :array_0
    .array-data 4
        0x9f
        0xee
    .end array-data
.end method

.method private setControlCommandIn(III[B)I
    .locals 9

    if-eqz p4, :cond_0

    .line 604
    array-length v0, p4

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 606
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0xc0

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 607
    sget-object p2, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Control Transfer Response: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method

.method private setControlCommandOut(III[B)I
    .locals 9

    if-eqz p4, :cond_0

    .line 592
    array-length v0, p4

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 594
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0x40

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 595
    sget-object p2, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Control Transfer Response: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method

.method private startFlowControlThread()V
    .locals 1

    .line 618
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->flowControlThread:Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->flowControlThread:Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->start()V

    :cond_0
    return-void
.end method

.method private stopFlowControlThread()V
    .locals 1

    .line 624
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->flowControlThread:Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

    if-eqz v0, :cond_0

    .line 626
    invoke-virtual {v0}, Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;->stopThread()V

    const/4 v0, 0x0

    .line 627
    iput-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->flowControlThread:Lcom/felhr/usbserial/CH34xSerialDevice$FlowControlThread;

    :cond_0
    return-void
.end method

.method private writeHandshakeByte()I
    .locals 4

    .line 579
    iget-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtr:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-boolean v2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rts:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x40

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    or-int/2addr v0, v2

    not-int v0, v0

    const/4 v2, 0x0

    const/16 v3, 0xa4

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/felhr/usbserial/CH34xSerialDevice;->setControlCommandOut(III[B)I

    move-result v0

    if-gez v0, :cond_2

    .line 581
    sget-object v0, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "Failed to set handshake byte"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    return v0

    :cond_2
    return v1
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 151
    invoke-virtual {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->killWorkingThread()V

    .line 152
    invoke-virtual {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->killWriteThread()V

    .line 153
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->stopFlowControlThread()V

    .line 154
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    const/4 v0, 0x0

    .line 155
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->isOpen:Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    .line 369
    iput-object p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    return-void
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    return-void
.end method

.method public open()Z
    .locals 3

    .line 120
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->openCH34X()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Lcom/felhr/utils/SafeUsbRequest;

    invoke-direct {v0}, Lcom/felhr/utils/SafeUsbRequest;-><init>()V

    .line 125
    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 128
    invoke-virtual {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->restartWorkingThread()V

    .line 129
    invoke-virtual {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->restartWriteThread()V

    .line 132
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->createFlowControlThread()V

    .line 135
    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    const/4 v0, 0x1

    .line 137
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->asyncMode:Z

    .line 138
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->isOpen:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 143
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->isOpen:Z

    return v0
.end method

.method public setBaudRate(I)V
    .locals 5

    const/16 v0, 0x12c

    const-string v1, "SetBaudRate failed!"

    const/4 v2, -0x1

    if-gt p1, v0, :cond_0

    const p1, 0xd980

    const/16 v0, 0xeb

    .line 195
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 197
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    const/16 v3, 0x258

    if-le p1, v0, :cond_1

    if-gt p1, v3, :cond_1

    const/16 p1, 0x6481

    const/16 v0, 0x76

    .line 200
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 202
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    const/16 v0, 0x4b0

    if-le p1, v3, :cond_2

    if-gt p1, v0, :cond_2

    const p1, 0xb281

    const/16 v0, 0x3b

    .line 206
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 208
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    const/16 v3, 0x960

    if-le p1, v0, :cond_3

    if-gt p1, v3, :cond_3

    const p1, 0xd981

    const/16 v0, 0x1e

    .line 211
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 213
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const/16 v0, 0x12c0

    if-le p1, v3, :cond_4

    if-gt p1, v0, :cond_4

    const/16 p1, 0x6482

    const/16 v0, 0xf

    .line 216
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 218
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const/16 v3, 0x2580

    if-le p1, v0, :cond_5

    if-gt p1, v3, :cond_5

    const p1, 0xb282

    const/16 v0, 0x8

    .line 221
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 223
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const/16 v0, 0x4b00

    const/4 v4, 0x7

    if-le p1, v3, :cond_6

    if-gt p1, v0, :cond_6

    const p1, 0xd982

    .line 226
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 228
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const v3, 0x9600

    if-le p1, v0, :cond_7

    if-gt p1, v3, :cond_7

    const/16 p1, 0x6483

    .line 231
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 233
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    const v0, 0xe100

    if-le p1, v3, :cond_8

    if-gt p1, v0, :cond_8

    const p1, 0x9883

    .line 236
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 238
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    const v3, 0x1c200

    if-le p1, v0, :cond_9

    if-gt p1, v3, :cond_9

    const p1, 0xcc83

    .line 241
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 243
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    const v0, 0x38400

    if-le p1, v3, :cond_a

    if-gt p1, v0, :cond_a

    const p1, 0xe683

    .line 246
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 248
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_a
    const v3, 0x70800

    if-le p1, v0, :cond_b

    if-gt p1, v3, :cond_b

    const p1, 0xf383

    .line 251
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 253
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_b
    if-le p1, v3, :cond_c

    const v0, 0xe1000

    if-gt p1, v0, :cond_c

    const p1, 0xf387

    .line 256
    invoke-direct {p0, p1, v4}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 258
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_c
    const v0, 0xe1000

    if-le p1, v0, :cond_d

    const v0, 0x12c000

    if-gt p1, v0, :cond_d

    const p1, 0xfb03

    const/16 v0, 0x21

    .line 261
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 263
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_d
    const v0, 0x12c000

    if-le p1, v0, :cond_e

    const v0, 0x1e8480

    if-gt p1, v0, :cond_e

    const p1, 0xfd03

    const/4 v0, 0x2

    .line 266
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setBaudRate(II)I

    move-result p1

    if-ne p1, v2, :cond_e

    .line 268
    sget-object p1, Lcom/felhr/usbserial/CH34xSerialDevice;->CLASS_ID:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    :goto_0
    return-void
.end method

.method public setBreak(Z)V
    .locals 0

    return-void
.end method

.method public setDTR(Z)V
    .locals 0

    .line 356
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtr:Z

    .line 357
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->writeHandshakeByte()I

    return-void
.end method

.method public setDataBits(I)V
    .locals 0

    return-void
.end method

.method public setFlowControl(I)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    goto :goto_0

    .line 335
    :cond_0
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rtsCtsEnabled:Z

    .line 336
    iput-boolean v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtrDsrEnabled:Z

    const/16 p1, 0x202

    .line 337
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xFlow(I)I

    .line 338
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkDSR()Z

    move-result p1

    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dsrState:Z

    .line 339
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->startFlowControlThread()V

    goto :goto_0

    .line 328
    :cond_1
    iput-boolean v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rtsCtsEnabled:Z

    .line 329
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtrDsrEnabled:Z

    const/16 p1, 0x101

    .line 330
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xFlow(I)I

    .line 331
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->checkCTS()Z

    move-result p1

    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->ctsState:Z

    .line 332
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->startFlowControlThread()V

    goto :goto_0

    .line 323
    :cond_2
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rtsCtsEnabled:Z

    .line 324
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->dtrDsrEnabled:Z

    .line 325
    invoke-direct {p0, v0}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xFlow(I)I

    :goto_0
    return-void
.end method

.method public setParity(I)V
    .locals 1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 p1, 0xfb

    .line 310
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xParity(I)I

    goto :goto_0

    :cond_1
    const/16 p1, 0xeb

    .line 307
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xParity(I)I

    goto :goto_0

    :cond_2
    const/16 p1, 0xdb

    .line 304
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xParity(I)I

    goto :goto_0

    :cond_3
    const/16 p1, 0xcb

    .line 301
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xParity(I)I

    goto :goto_0

    :cond_4
    const/16 p1, 0xc3

    .line 298
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/CH34xSerialDevice;->setCh340xParity(I)I

    :goto_0
    return-void
.end method

.method public setRTS(Z)V
    .locals 0

    .line 349
    iput-boolean p1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->rts:Z

    .line 350
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->writeHandshakeByte()I

    return-void
.end method

.method public setStopBits(I)V
    .locals 0

    return-void
.end method

.method public syncClose()V
    .locals 2

    .line 185
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->stopFlowControlThread()V

    .line 186
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    const/4 v0, 0x0

    .line 187
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->isOpen:Z

    return-void
.end method

.method public syncOpen()Z
    .locals 3

    .line 161
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->openCH34X()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/felhr/usbserial/CH34xSerialDevice;->createFlowControlThread()V

    .line 166
    iget-object v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v2, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v2}, Lcom/felhr/usbserial/CH34xSerialDevice;->setSyncParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    .line 167
    iput-boolean v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->asyncMode:Z

    const/4 v0, 0x1

    .line 168
    iput-boolean v0, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->isOpen:Z

    .line 171
    new-instance v1, Lcom/felhr/usbserial/SerialInputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialInputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->inputStream:Lcom/felhr/usbserial/SerialInputStream;

    .line 172
    new-instance v1, Lcom/felhr/usbserial/SerialOutputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialOutputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->outputStream:Lcom/felhr/usbserial/SerialOutputStream;

    return v0

    .line 177
    :cond_0
    iput-boolean v1, p0, Lcom/felhr/usbserial/CH34xSerialDevice;->isOpen:Z

    return v1
.end method
