.class public Lcom/felhr/usbserial/SerialInputStream;
.super Ljava/io/InputStream;
.source "SerialInputStream.java"


# instance fields
.field private final buffer:[B

.field private bufferSize:I

.field protected final device:Lcom/felhr/usbserial/UsbSerialInterface;

.field private maxBufferSize:I

.field private pointer:I

.field private timeout:I


# direct methods
.method public constructor <init>(Lcom/felhr/usbserial/UsbSerialInterface;)V
    .locals 2

    .line 19
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->timeout:I

    const/16 v1, 0x4000

    .line 10
    iput v1, p0, Lcom/felhr/usbserial/SerialInputStream;->maxBufferSize:I

    .line 20
    iput-object p1, p0, Lcom/felhr/usbserial/SerialInputStream;->device:Lcom/felhr/usbserial/UsbSerialInterface;

    .line 21
    iget p1, p0, Lcom/felhr/usbserial/SerialInputStream;->maxBufferSize:I

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/felhr/usbserial/SerialInputStream;->buffer:[B

    .line 22
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    const/4 p1, -0x1

    .line 23
    iput p1, p0, Lcom/felhr/usbserial/SerialInputStream;->bufferSize:I

    return-void
.end method

.method public constructor <init>(Lcom/felhr/usbserial/UsbSerialInterface;I)V
    .locals 2

    .line 27
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->timeout:I

    const/16 v1, 0x4000

    .line 10
    iput v1, p0, Lcom/felhr/usbserial/SerialInputStream;->maxBufferSize:I

    .line 28
    iput-object p1, p0, Lcom/felhr/usbserial/SerialInputStream;->device:Lcom/felhr/usbserial/UsbSerialInterface;

    .line 29
    iput p2, p0, Lcom/felhr/usbserial/SerialInputStream;->maxBufferSize:I

    .line 30
    iget p1, p0, Lcom/felhr/usbserial/SerialInputStream;->maxBufferSize:I

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/felhr/usbserial/SerialInputStream;->buffer:[B

    .line 31
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    const/4 p1, -0x1

    .line 32
    iput p1, p0, Lcom/felhr/usbserial/SerialInputStream;->bufferSize:I

    return-void
.end method

.method private checkFromBuffer()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/felhr/usbserial/SerialInputStream;->bufferSize:I

    if-lez v0, :cond_0

    iget v1, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    if-ge v1, v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/felhr/usbserial/SerialInputStream;->buffer:[B

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 95
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    const/4 v0, -0x1

    .line 96
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->bufferSize:I

    return v0
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 81
    iget v0, p0, Lcom/felhr/usbserial/SerialInputStream;->bufferSize:I

    if-lez v0, :cond_0

    .line 82
    iget v1, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    sub-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 3

    .line 38
    invoke-direct {p0}, Lcom/felhr/usbserial/SerialInputStream;->checkFromBuffer()I

    move-result v0

    if-ltz v0, :cond_0

    return v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialInputStream;->device:Lcom/felhr/usbserial/UsbSerialInterface;

    iget-object v1, p0, Lcom/felhr/usbserial/SerialInputStream;->buffer:[B

    iget v2, p0, Lcom/felhr/usbserial/SerialInputStream;->timeout:I

    invoke-interface {v0, v1, v2}, Lcom/felhr/usbserial/UsbSerialInterface;->syncRead([BI)I

    move-result v0

    if-ltz v0, :cond_1

    .line 44
    iput v0, p0, Lcom/felhr/usbserial/SerialInputStream;->bufferSize:I

    .line 45
    iget-object v0, p0, Lcom/felhr/usbserial/SerialInputStream;->buffer:[B

    iget v1, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/felhr/usbserial/SerialInputStream;->pointer:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public read([B)I
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/felhr/usbserial/SerialInputStream;->device:Lcom/felhr/usbserial/UsbSerialInterface;

    iget v1, p0, Lcom/felhr/usbserial/SerialInputStream;->timeout:I

    invoke-interface {v0, p1, v1}, Lcom/felhr/usbserial/UsbSerialInterface;->syncRead([BI)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 2

    if-ltz p2, :cond_3

    if-ltz p3, :cond_2

    .line 68
    array-length v0, p1

    sub-int/2addr v0, p2

    if-gt p3, v0, :cond_1

    if-nez p2, :cond_0

    .line 72
    array-length v0, p1

    if-ne p3, v0, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Lcom/felhr/usbserial/SerialInputStream;->read([B)I

    move-result p1

    return p1

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialInputStream;->device:Lcom/felhr/usbserial/UsbSerialInterface;

    iget v1, p0, Lcom/felhr/usbserial/SerialInputStream;->timeout:I

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/felhr/usbserial/UsbSerialInterface;->syncRead([BIII)I

    move-result p1

    return p1

    .line 69
    :cond_1
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    const-string p2, "Length greater than b.length - off"

    invoke-direct {p1, p2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 65
    :cond_2
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    const-string p2, "Length must positive"

    invoke-direct {p1, p2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 61
    :cond_3
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    const-string p2, "Offset must be >= 0"

    invoke-direct {p1, p2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTimeout(I)V
    .locals 0

    .line 88
    iput p1, p0, Lcom/felhr/usbserial/SerialInputStream;->timeout:I

    return-void
.end method
