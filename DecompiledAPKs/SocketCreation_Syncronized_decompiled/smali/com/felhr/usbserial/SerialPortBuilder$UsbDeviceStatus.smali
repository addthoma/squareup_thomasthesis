.class Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;
.super Ljava/lang/Object;
.source "SerialPortBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/SerialPortBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbDeviceStatus"
.end annotation


# instance fields
.field public open:Z

.field final synthetic this$0:Lcom/felhr/usbserial/SerialPortBuilder;

.field public usbDevice:Landroid/hardware/usb/UsbDevice;

.field public usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;


# direct methods
.method public constructor <init>(Lcom/felhr/usbserial/SerialPortBuilder;Landroid/hardware/usb/UsbDevice;)V
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270
    iput-object p2, p0, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 275
    check-cast p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    .line 276
    iget-object p1, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result p1

    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
