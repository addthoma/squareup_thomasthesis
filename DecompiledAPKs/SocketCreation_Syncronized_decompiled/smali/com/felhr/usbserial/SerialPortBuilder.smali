.class public Lcom/felhr/usbserial/SerialPortBuilder;
.super Ljava/lang/Object;
.source "SerialPortBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;,
        Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;,
        Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;
    }
.end annotation


# static fields
.field private static final ACTION_USB_PERMISSION:Ljava/lang/String; = "com.felhr.usbserial.USB_PERMISSION"

.field private static final MODE_OPEN:I = 0x1

.field private static final MODE_START:I

.field private static SerialPortBuilder:Lcom/felhr/usbserial/SerialPortBuilder;


# instance fields
.field private baudRate:I

.field private broadcastRegistered:Z

.field private currentPendingPermission:Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

.field private dataBits:I

.field private devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;",
            ">;"
        }
    .end annotation
.end field

.field private flowControl:I

.field private mode:I

.field private parity:I

.field private volatile processingPermission:Z

.field private final queuedPermissions:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue<",
            "Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;",
            ">;"
        }
    .end annotation
.end field

.field private serialDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/felhr/usbserial/UsbSerialDevice;",
            ">;"
        }
    .end annotation
.end field

.field private final serialPortCallback:Lcom/felhr/usbserial/SerialPortCallback;

.field private stopBits:I

.field private usbManager:Landroid/hardware/usb/UsbManager;

.field private final usbReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>(Lcom/felhr/usbserial/SerialPortCallback;)V
    .locals 2

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    .line 32
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->queuedPermissions:Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v0, 0x0

    .line 33
    iput-boolean v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->processingPermission:Z

    .line 40
    iput v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->mode:I

    .line 42
    iput-boolean v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->broadcastRegistered:Z

    .line 200
    new-instance v0, Lcom/felhr/usbserial/SerialPortBuilder$1;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/SerialPortBuilder$1;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;)V

    iput-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbReceiver:Landroid/content/BroadcastReceiver;

    .line 45
    iput-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialPortCallback:Lcom/felhr/usbserial/SerialPortCallback;

    return-void
.end method

.method static synthetic access$100(Lcom/felhr/usbserial/SerialPortBuilder;)Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->currentPendingPermission:Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/felhr/usbserial/SerialPortBuilder;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->dataBits:I

    return p0
.end method

.method static synthetic access$1100(Lcom/felhr/usbserial/SerialPortBuilder;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->stopBits:I

    return p0
.end method

.method static synthetic access$1200(Lcom/felhr/usbserial/SerialPortBuilder;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->parity:I

    return p0
.end method

.method static synthetic access$1300(Lcom/felhr/usbserial/SerialPortBuilder;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->flowControl:I

    return p0
.end method

.method static synthetic access$200(Lcom/felhr/usbserial/SerialPortBuilder;Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder;->createAllPorts(Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)V

    return-void
.end method

.method static synthetic access$300(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/concurrent/ArrayBlockingQueue;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->queuedPermissions:Ljava/util/concurrent/ArrayBlockingQueue;

    return-object p0
.end method

.method static synthetic access$400(Lcom/felhr/usbserial/SerialPortBuilder;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/felhr/usbserial/SerialPortBuilder;->launchPermission()V

    return-void
.end method

.method static synthetic access$502(Lcom/felhr/usbserial/SerialPortBuilder;Z)Z
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->processingPermission:Z

    return p1
.end method

.method static synthetic access$600(Lcom/felhr/usbserial/SerialPortBuilder;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->mode:I

    return p0
.end method

.method static synthetic access$700(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/List;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$800(Lcom/felhr/usbserial/SerialPortBuilder;)Lcom/felhr/usbserial/SerialPortCallback;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialPortCallback:Lcom/felhr/usbserial/SerialPortCallback;

    return-object p0
.end method

.method static synthetic access$900(Lcom/felhr/usbserial/SerialPortBuilder;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->baudRate:I

    return p0
.end method

.method private createAllPorts(Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)V
    .locals 4

    .line 185
    iget-object v0, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v2, v0, -0x1

    if-gt v1, v2, :cond_1

    .line 187
    iget-object v2, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v2, :cond_0

    .line 188
    iget-object v2, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbManager:Landroid/hardware/usb/UsbManager;

    iget-object v3, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v2, v3}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v2

    iput-object v2, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 191
    :cond_0
    iget-object v2, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    iget-object v3, p1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-static {v2, v3, v1}, Lcom/felhr/usbserial/UsbSerialDevice;->createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)Lcom/felhr/usbserial/UsbSerialDevice;

    move-result-object v2

    .line 196
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static createSerialPortBuilder(Lcom/felhr/usbserial/SerialPortCallback;)Lcom/felhr/usbserial/SerialPortBuilder;
    .locals 1

    .line 50
    sget-object v0, Lcom/felhr/usbserial/SerialPortBuilder;->SerialPortBuilder:Lcom/felhr/usbserial/SerialPortBuilder;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/SerialPortBuilder;-><init>(Lcom/felhr/usbserial/SerialPortCallback;)V

    sput-object v0, Lcom/felhr/usbserial/SerialPortBuilder;->SerialPortBuilder:Lcom/felhr/usbserial/SerialPortBuilder;

    .line 52
    sget-object p0, Lcom/felhr/usbserial/SerialPortBuilder;->SerialPortBuilder:Lcom/felhr/usbserial/SerialPortBuilder;

    return-object p0

    :cond_0
    return-object v0
.end method

.method private createUsbPermission(Landroid/content/Context;Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;
    .locals 2

    .line 155
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.felhr.usbserial.USB_PERMISSION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    .line 156
    new-instance v0, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;Lcom/felhr/usbserial/SerialPortBuilder$1;)V

    .line 157
    iput-object p1, v0, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;->pendingIntent:Landroid/app/PendingIntent;

    .line 158
    iput-object p2, v0, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;->usbDeviceStatus:Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    return-object v0
.end method

.method private initReceiver(Landroid/content/Context;)V
    .locals 2

    .line 176
    iget-boolean v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->broadcastRegistered:Z

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.felhr.usbserial.USB_PERMISSION"

    .line 178
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 p1, 0x1

    .line 180
    iput-boolean p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->broadcastRegistered:Z

    :cond_0
    return-void
.end method

.method static synthetic lambda$disconnectDevice$3(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice;)Z
    .locals 0

    .line 128
    invoke-virtual {p0}, Lcom/felhr/usbserial/UsbSerialDevice;->getDeviceId()I

    move-result p0

    invoke-virtual {p1}, Lcom/felhr/usbserial/UsbSerialDevice;->getDeviceId()I

    move-result p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$disconnectDevice$4(Landroid/hardware/usb/UsbDevice;Lcom/felhr/usbserial/UsbSerialDevice;)Z
    .locals 0

    .line 134
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result p0

    invoke-virtual {p1}, Lcom/felhr/usbserial/UsbSerialDevice;->getDeviceId()I

    move-result p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$disconnectDevice$5(Landroid/hardware/usb/UsbDevice;Lcom/felhr/usbserial/UsbSerialDevice;)Z
    .locals 0

    .line 140
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result p0

    invoke-virtual {p1}, Lcom/felhr/usbserial/UsbSerialDevice;->getDeviceId()I

    move-result p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private launchPermission()V
    .locals 3

    const/4 v0, 0x1

    .line 165
    :try_start_0
    iput-boolean v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->processingPermission:Z

    .line 166
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->queuedPermissions:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    iput-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->currentPendingPermission:Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    .line 167
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbManager:Landroid/hardware/usb/UsbManager;

    iget-object v1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->currentPendingPermission:Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    iget-object v1, v1, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;->usbDeviceStatus:Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    iget-object v1, v1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;->usbDevice:Landroid/hardware/usb/UsbDevice;

    iget-object v2, p0, Lcom/felhr/usbserial/SerialPortBuilder;->currentPendingPermission:Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    iget-object v2, v2, Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 170
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    const/4 v0, 0x0

    .line 171
    iput-boolean v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->processingPermission:Z

    :goto_0
    return-void
.end method


# virtual methods
.method public disconnectDevice(Landroid/hardware/usb/UsbDevice;)Z
    .locals 6

    .line 133
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$Y9UQc4UfDh5SU5gOKOTD484ylAc;

    invoke-direct {v1, p1}, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$Y9UQc4UfDh5SU5gOKOTD484ylAc;-><init>(Landroid/hardware/usb/UsbDevice;)V

    .line 134
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 138
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/felhr/usbserial/UsbSerialDevice;

    .line 139
    invoke-virtual {v4}, Lcom/felhr/usbserial/UsbSerialDevice;->syncClose()V

    .line 140
    iget-object v4, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    new-instance v5, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$GMaiif_o7fmpZ9jrfkKYIfq08Go;

    invoke-direct {v5, p1}, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$GMaiif_o7fmpZ9jrfkKYIfq08Go;-><init>(Landroid/hardware/usb/UsbDevice;)V

    invoke-static {v4, v5}, Lcom/felhr/utils/Utils;->removeIf(Ljava/util/Collection;Lcom/annimon/stream/function/Predicate;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 144
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-ne v3, p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public disconnectDevice(Lcom/felhr/usbserial/UsbSerialDevice;)Z
    .locals 2

    .line 127
    invoke-virtual {p1}, Lcom/felhr/usbserial/UsbSerialDevice;->syncClose()V

    .line 128
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    new-instance v1, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$fyFr4MKEpbXn0JAGLg1rk_-jZV8;

    invoke-direct {v1, p1}, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$fyFr4MKEpbXn0JAGLg1rk_-jZV8;-><init>(Lcom/felhr/usbserial/UsbSerialDevice;)V

    invoke-static {v0, v1}, Lcom/felhr/utils/Utils;->removeIf(Ljava/util/Collection;Lcom/annimon/stream/function/Predicate;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->serialDevices:Ljava/util/List;

    const/4 p1, 0x1

    return p1
.end method

.method public getPossibleSerialPorts(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation

    const-string v0, "usb"

    .line 61
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/hardware/usb/UsbManager;

    iput-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbManager:Landroid/hardware/usb/UsbManager;

    .line 63
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object p1

    .line 64
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object p1

    sget-object v0, Lcom/felhr/usbserial/-$$Lambda$idnBWZ0ewe5DMP8uuJ8tQMV-j2c;->INSTANCE:Lcom/felhr/usbserial/-$$Lambda$idnBWZ0ewe5DMP8uuJ8tQMV-j2c;

    .line 65
    invoke-virtual {p1, v0}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getSerialPorts(Landroid/content/Context;)Z
    .locals 4

    .line 73
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder;->initReceiver(Landroid/content/Context;)V

    .line 75
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->devices:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 93
    :cond_0
    invoke-virtual {p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder;->getPossibleSerialPorts(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v2, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$WGdWJnsp2XE9MQZUdbll7c8oU-U;

    invoke-direct {v2, p0}, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$WGdWJnsp2XE9MQZUdbll7c8oU-U;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;)V

    .line 94
    invoke-virtual {v0, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v2, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$BoLaOje2op5xNj_uMGb1c-I-dRE;

    invoke-direct {v2, p0}, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$BoLaOje2op5xNj_uMGb1c-I-dRE;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;)V

    .line 95
    invoke-virtual {v0, v2}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    return v1

    .line 101
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    .line 102
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder;->queuedPermissions:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {p0, p1, v2}, Lcom/felhr/usbserial/SerialPortBuilder;->createUsbPermission(Landroid/content/Context;Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ArrayBlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    :cond_2
    iget-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->devices:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 107
    iget-boolean p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->processingPermission:Z

    if-nez p1, :cond_6

    .line 108
    invoke-direct {p0}, Lcom/felhr/usbserial/SerialPortBuilder;->launchPermission()V

    goto :goto_3

    .line 76
    :cond_3
    :goto_1
    invoke-virtual {p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder;->getPossibleSerialPorts(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v2, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$ZTZ3JhwhjbcsDcWa9qt3W1viZHg;

    invoke-direct {v2, p0}, Lcom/felhr/usbserial/-$$Lambda$SerialPortBuilder$ZTZ3JhwhjbcsDcWa9qt3W1viZHg;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;)V

    .line 77
    invoke-virtual {v0, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->devices:Ljava/util/List;

    .line 80
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->devices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    return v1

    .line 83
    :cond_4
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->devices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    .line 84
    iget-object v2, p0, Lcom/felhr/usbserial/SerialPortBuilder;->queuedPermissions:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {p0, p1, v1}, Lcom/felhr/usbserial/SerialPortBuilder;->createUsbPermission(Landroid/content/Context;Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)Lcom/felhr/usbserial/SerialPortBuilder$PendingUsbPermission;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ArrayBlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 87
    :cond_5
    iget-boolean p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->processingPermission:Z

    if-nez p1, :cond_6

    .line 88
    invoke-direct {p0}, Lcom/felhr/usbserial/SerialPortBuilder;->launchPermission()V

    :cond_6
    :goto_3
    const/4 p1, 0x1

    return p1
.end method

.method public synthetic lambda$getSerialPorts$0$SerialPortBuilder(Landroid/hardware/usb/UsbDevice;)Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;
    .locals 1

    .line 77
    new-instance v0, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    invoke-direct {v0, p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;Landroid/hardware/usb/UsbDevice;)V

    return-object v0
.end method

.method public synthetic lambda$getSerialPorts$1$SerialPortBuilder(Landroid/hardware/usb/UsbDevice;)Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;
    .locals 1

    .line 94
    new-instance v0, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;

    invoke-direct {v0, p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;-><init>(Lcom/felhr/usbserial/SerialPortBuilder;Landroid/hardware/usb/UsbDevice;)V

    return-object v0
.end method

.method public synthetic lambda$getSerialPorts$2$SerialPortBuilder(Lcom/felhr/usbserial/SerialPortBuilder$UsbDeviceStatus;)Z
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->devices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public openSerialPorts(Landroid/content/Context;IIIII)Z
    .locals 0

    .line 117
    iput p2, p0, Lcom/felhr/usbserial/SerialPortBuilder;->baudRate:I

    .line 118
    iput p3, p0, Lcom/felhr/usbserial/SerialPortBuilder;->dataBits:I

    .line 119
    iput p4, p0, Lcom/felhr/usbserial/SerialPortBuilder;->stopBits:I

    .line 120
    iput p5, p0, Lcom/felhr/usbserial/SerialPortBuilder;->parity:I

    .line 121
    iput p6, p0, Lcom/felhr/usbserial/SerialPortBuilder;->flowControl:I

    const/4 p2, 0x1

    .line 122
    iput p2, p0, Lcom/felhr/usbserial/SerialPortBuilder;->mode:I

    .line 123
    invoke-virtual {p0, p1}, Lcom/felhr/usbserial/SerialPortBuilder;->getSerialPorts(Landroid/content/Context;)Z

    move-result p1

    return p1
.end method

.method public unregisterListeners(Landroid/content/Context;)V
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->broadcastRegistered:Z

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder;->usbReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 p1, 0x0

    .line 150
    iput-boolean p1, p0, Lcom/felhr/usbserial/SerialPortBuilder;->broadcastRegistered:Z

    :cond_0
    return-void
.end method
