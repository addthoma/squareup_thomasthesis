.class public Lcom/felhr/usbserial/PL2303SerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "PL2303SerialDevice.java"


# static fields
.field private static final CLASS_ID:Ljava/lang/String;

.field private static final PL2303_REQTYPE_DEVICE2HOST_VENDOR:I = 0xc0

.field private static final PL2303_REQTYPE_HOST2DEVICE:I = 0x21

.field private static final PL2303_REQTYPE_HOST2DEVICE_VENDOR:I = 0x40

.field private static final PL2303_SET_CONTROL_REQUEST:I = 0x22

.field private static final PL2303_SET_LINE_CODING:I = 0x20

.field private static final PL2303_VENDOR_WRITE_REQUEST:I = 0x1


# instance fields
.field private final defaultSetLine:[B

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    const-class v0, Lcom/felhr/usbserial/PL2303SerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/PL2303SerialDevice;->CLASS_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    const/4 v0, -0x1

    .line 43
    invoke-direct {p0, p1, p2, v0}, Lcom/felhr/usbserial/PL2303SerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x7

    new-array p2, p2, [B

    .line 25
    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    const/4 p2, 0x1

    if-gt p3, p2, :cond_1

    if-ltz p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 55
    :goto_0
    invoke-virtual {p1, p3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void

    .line 52
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Multi-interface PL2303 devices not supported!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    nop

    :array_0
    .array-data 1
        -0x80t
        0x25t
        0x0t
        0x0t
        0x0t
        0x0t
        0x8t
    .end array-data
.end method

.method private openPL2303()Z
    .locals 10

    .line 323
    iget-object v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_11

    .line 325
    sget-object v0, Lcom/felhr/usbserial/PL2303SerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_2

    .line 336
    iget-object v4, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 337
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 338
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v7, 0x80

    if-ne v5, v7, :cond_0

    .line 339
    iput-object v4, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 340
    :cond_0
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    if-ne v5, v6, :cond_1

    .line 341
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    if-nez v5, :cond_1

    .line 342
    iput-object v4, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    new-array v0, v2, [B

    const/16 v5, 0xc0

    const/4 v6, 0x1

    const v7, 0x8484

    const/4 v8, 0x0

    move-object v4, p0

    move-object v9, v0

    .line 348
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_3

    return v1

    :cond_3
    const/16 v5, 0x40

    const/4 v6, 0x1

    const/16 v7, 0x404

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, p0

    .line 350
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_4

    return v1

    :cond_4
    const/16 v5, 0xc0

    const/4 v6, 0x1

    const v7, 0x8484

    const/4 v8, 0x0

    move-object v4, p0

    move-object v9, v0

    .line 352
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_5

    return v1

    :cond_5
    const/16 v5, 0xc0

    const/4 v6, 0x1

    const v7, 0x8383

    const/4 v8, 0x0

    move-object v4, p0

    move-object v9, v0

    .line 354
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_6

    return v1

    :cond_6
    const/16 v5, 0xc0

    const/4 v6, 0x1

    const v7, 0x8484

    const/4 v8, 0x0

    move-object v4, p0

    move-object v9, v0

    .line 356
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_7

    return v1

    :cond_7
    const/16 v5, 0x40

    const/4 v6, 0x1

    const/16 v7, 0x404

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v4, p0

    .line 358
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_8

    return v1

    :cond_8
    const/16 v5, 0xc0

    const/4 v6, 0x1

    const v7, 0x8484

    const/4 v8, 0x0

    move-object v4, p0

    move-object v9, v0

    .line 360
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v3

    if-gez v3, :cond_9

    return v1

    :cond_9
    const/16 v5, 0xc0

    const/4 v6, 0x1

    const v7, 0x8383

    const/4 v8, 0x0

    move-object v4, p0

    move-object v9, v0

    .line 362
    invoke-direct/range {v4 .. v9}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_a

    return v1

    :cond_a
    const/16 v4, 0x40

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v3, p0

    .line 364
    invoke-direct/range {v3 .. v8}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_b

    return v1

    :cond_b
    const/16 v4, 0x40

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    .line 366
    invoke-direct/range {v3 .. v8}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_c

    return v1

    :cond_c
    const/16 v4, 0x40

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/16 v7, 0x44

    const/4 v8, 0x0

    move-object v3, p0

    .line 368
    invoke-direct/range {v3 .. v8}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_d

    return v1

    :cond_d
    const/16 v4, 0x21

    const/16 v5, 0x22

    const/4 v6, 0x3

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    .line 371
    invoke-direct/range {v3 .. v8}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_e

    return v1

    :cond_e
    const/16 v4, 0x21

    const/16 v5, 0x20

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 373
    iget-object v8, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_f

    return v1

    :cond_f
    const/16 v4, 0x40

    const/4 v5, 0x1

    const/16 v6, 0x505

    const/16 v7, 0x1311

    const/4 v8, 0x0

    move-object v3, p0

    .line 375
    invoke-direct/range {v3 .. v8}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    move-result v0

    if-gez v0, :cond_10

    return v1

    :cond_10
    return v2

    .line 328
    :cond_11
    sget-object v0, Lcom/felhr/usbserial/PL2303SerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private setControlCommand(IIII[B)I
    .locals 9

    if-eqz p5, :cond_0

    .line 385
    array-length v0, p5

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 386
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/4 v8, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 387
    sget-object p2, Lcom/felhr/usbserial/PL2303SerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Control Transfer Response: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 90
    invoke-virtual {p0}, Lcom/felhr/usbserial/PL2303SerialDevice;->killWorkingThread()V

    .line 91
    invoke-virtual {p0}, Lcom/felhr/usbserial/PL2303SerialDevice;->killWriteThread()V

    .line 92
    iget-object v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    const/4 v0, 0x0

    .line 93
    iput-boolean v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->isOpen:Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    return-void
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    return-void
.end method

.method public open()Z
    .locals 3

    .line 61
    invoke-direct {p0}, Lcom/felhr/usbserial/PL2303SerialDevice;->openPL2303()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    new-instance v0, Lcom/felhr/utils/SafeUsbRequest;

    invoke-direct {v0}, Lcom/felhr/utils/SafeUsbRequest;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 70
    invoke-virtual {p0}, Lcom/felhr/usbserial/PL2303SerialDevice;->restartWorkingThread()V

    .line 71
    invoke-virtual {p0}, Lcom/felhr/usbserial/PL2303SerialDevice;->restartWriteThread()V

    .line 74
    iget-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/PL2303SerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    const/4 v0, 0x1

    .line 76
    iput-boolean v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->asyncMode:Z

    .line 77
    iput-boolean v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->isOpen:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 82
    iput-boolean v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->isOpen:Z

    return v0
.end method

.method public setBaudRate(I)V
    .locals 12

    const/4 v0, 0x4

    new-array v0, v0, [B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v4, 0x2

    aput-byte v1, v0, v4

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 133
    aget-byte p1, v0, v2

    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte v6, v5, v2

    if-ne p1, v6, :cond_0

    aget-byte p1, v0, v3

    aget-byte v6, v5, v3

    if-ne p1, v6, :cond_0

    aget-byte p1, v0, v4

    aget-byte v6, v5, v4

    if-ne p1, v6, :cond_0

    aget-byte p1, v0, v1

    aget-byte v5, v5, v1

    if-eq p1, v5, :cond_1

    .line 136
    :cond_0
    iget-object v11, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v0, v2

    aput-byte p1, v11, v2

    .line 137
    aget-byte p1, v0, v3

    aput-byte p1, v11, v3

    .line 138
    aget-byte p1, v0, v4

    aput-byte p1, v11, v4

    .line 139
    aget-byte p1, v0, v1

    aput-byte p1, v11, v1

    const/16 v7, 0x21

    const/16 v8, 0x20

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, p0

    .line 140
    invoke-direct/range {v6 .. v11}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    :cond_1
    return-void
.end method

.method public setBreak(Z)V
    .locals 0

    return-void
.end method

.method public setDTR(Z)V
    .locals 0

    return-void
.end method

.method public setDataBits(I)V
    .locals 14

    const/4 v0, 0x5

    const/4 v1, 0x6

    if-eq p1, v0, :cond_3

    if-eq p1, v1, :cond_2

    const/4 v0, 0x7

    if-eq p1, v0, :cond_1

    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    return-void

    .line 171
    :cond_0
    iget-object v7, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v7, v1

    if-eq p1, v0, :cond_4

    .line 173
    aput-byte v0, v7, v1

    const/16 v3, 0x21

    const/16 v4, 0x20

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    .line 174
    invoke-direct/range {v2 .. v7}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 164
    :cond_1
    iget-object v13, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v13, v1

    if-eq p1, v0, :cond_4

    .line 166
    aput-byte v0, v13, v1

    const/16 v9, 0x21

    const/16 v10, 0x20

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v8, p0

    .line 167
    invoke-direct/range {v8 .. v13}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 157
    :cond_2
    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v5, v1

    if-eq p1, v1, :cond_4

    .line 159
    aput-byte v1, v5, v1

    const/16 v1, 0x21

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    .line 160
    invoke-direct/range {v0 .. v5}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 150
    :cond_3
    iget-object v11, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v11, v1

    if-eq p1, v0, :cond_4

    .line 152
    aput-byte v0, v11, v1

    const/16 v7, 0x21

    const/16 v8, 0x20

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, p0

    .line 153
    invoke-direct/range {v6 .. v11}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    :cond_4
    :goto_0
    return-void
.end method

.method public setFlowControl(I)V
    .locals 0

    return-void
.end method

.method public setParity(I)V
    .locals 14

    const/4 v0, 0x5

    if-eqz p1, :cond_4

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    return-void

    .line 248
    :cond_0
    iget-object v7, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v7, v0

    if-eq p1, v1, :cond_5

    .line 250
    aput-byte v1, v7, v0

    const/16 v3, 0x21

    const/16 v4, 0x20

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    .line 251
    invoke-direct/range {v2 .. v7}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 241
    :cond_1
    iget-object v13, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v13, v0

    if-eq p1, v1, :cond_5

    .line 243
    aput-byte v1, v13, v0

    const/16 v9, 0x21

    const/16 v10, 0x20

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v8, p0

    .line 244
    invoke-direct/range {v8 .. v13}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 234
    :cond_2
    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v5, v0

    if-eq p1, v1, :cond_5

    .line 236
    aput-byte v1, v5, v0

    const/16 v1, 0x21

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    .line 237
    invoke-direct/range {v0 .. v5}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 227
    :cond_3
    iget-object v11, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v11, v0

    if-eq p1, v1, :cond_5

    .line 229
    aput-byte v1, v11, v0

    const/16 v7, 0x21

    const/16 v8, 0x20

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, p0

    .line 230
    invoke-direct/range {v6 .. v11}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 220
    :cond_4
    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte p1, v5, v0

    if-eqz p1, :cond_5

    const/4 p1, 0x0

    .line 222
    aput-byte p1, v5, v0

    const/16 v1, 0x21

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    .line 223
    invoke-direct/range {v0 .. v5}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    :cond_5
    :goto_0
    return-void
.end method

.method public setRTS(Z)V
    .locals 0

    return-void
.end method

.method public setStopBits(I)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x4

    if-eq p1, v1, :cond_2

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x3

    if-eq p1, v3, :cond_0

    return-void

    .line 196
    :cond_0
    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte v0, v5, v2

    if-eq v0, v1, :cond_3

    .line 198
    aput-byte v1, v5, v2

    const/16 v1, 0x21

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    .line 199
    invoke-direct/range {v0 .. v5}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 203
    :cond_1
    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte v0, v5, v2

    if-eq v0, v3, :cond_3

    .line 205
    aput-byte v3, v5, v2

    const/16 v1, 0x21

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    .line 206
    invoke-direct/range {v0 .. v5}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    goto :goto_0

    .line 189
    :cond_2
    iget-object v5, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->defaultSetLine:[B

    aget-byte v0, v5, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    .line 191
    aput-byte v0, v5, v2

    const/16 v1, 0x21

    const/16 v2, 0x20

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    .line 192
    invoke-direct/range {v0 .. v5}, Lcom/felhr/usbserial/PL2303SerialDevice;->setControlCommand(IIII[B)I

    :cond_3
    :goto_0
    return-void
.end method

.method public syncClose()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    const/4 v0, 0x0

    .line 122
    iput-boolean v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->isOpen:Z

    return-void
.end method

.method public syncOpen()Z
    .locals 3

    .line 99
    invoke-direct {p0}, Lcom/felhr/usbserial/PL2303SerialDevice;->openPL2303()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v2, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v2}, Lcom/felhr/usbserial/PL2303SerialDevice;->setSyncParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    .line 103
    iput-boolean v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->asyncMode:Z

    const/4 v0, 0x1

    .line 104
    iput-boolean v0, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->isOpen:Z

    .line 107
    new-instance v1, Lcom/felhr/usbserial/SerialInputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialInputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->inputStream:Lcom/felhr/usbserial/SerialInputStream;

    .line 108
    new-instance v1, Lcom/felhr/usbserial/SerialOutputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialOutputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->outputStream:Lcom/felhr/usbserial/SerialOutputStream;

    return v0

    .line 113
    :cond_0
    iput-boolean v1, p0, Lcom/felhr/usbserial/PL2303SerialDevice;->isOpen:Z

    return v1
.end method
