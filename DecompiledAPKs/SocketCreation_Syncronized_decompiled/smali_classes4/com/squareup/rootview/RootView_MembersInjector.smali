.class public final Lcom/squareup/rootview/RootView_MembersInjector;
.super Ljava/lang/Object;
.source "RootView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/rootview/RootView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerPresenter<",
            "Lcom/squareup/ui/main/MainActivityScope$View;",
            ">;>;"
        }
    .end annotation
.end field

.field private final readerMessageBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/messagebar/api/ReaderMessageBar;",
            ">;"
        }
    .end annotation
.end field

.field private final rootViewBinderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewBinder;",
            ">;"
        }
    .end annotation
.end field

.field private final touchEventMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoordinatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCoordinator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerPresenter<",
            "Lcom/squareup/ui/main/MainActivityScope$View;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewBinder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCoordinator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/messagebar/api/ReaderMessageBar;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/rootview/RootView_MembersInjector;->mainContainerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/rootview/RootView_MembersInjector;->rootViewBinderProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/rootview/RootView_MembersInjector;->touchEventMonitorProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/rootview/RootView_MembersInjector;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/rootview/RootView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/rootview/RootView_MembersInjector;->tutorialCoordinatorProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/rootview/RootView_MembersInjector;->readerMessageBarProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerPresenter<",
            "Lcom/squareup/ui/main/MainActivityScope$View;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewBinder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCoordinator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/messagebar/api/ReaderMessageBar;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/rootview/RootView;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/rootview/RootView_MembersInjector;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/rootview/RootView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static injectBadMaybeSquareDeviceCheck(Lcom/squareup/rootview/RootView;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/rootview/RootView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectMainContainer(Lcom/squareup/rootview/RootView;Lcom/squareup/container/ContainerPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/rootview/RootView;",
            "Lcom/squareup/container/ContainerPresenter<",
            "Lcom/squareup/ui/main/MainActivityScope$View;",
            ">;)V"
        }
    .end annotation

    .line 75
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->mainContainer:Lcom/squareup/container/ContainerPresenter;

    return-void
.end method

.method public static injectReaderMessageBar(Lcom/squareup/rootview/RootView;Lcom/squareup/messagebar/api/ReaderMessageBar;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->readerMessageBar:Lcom/squareup/messagebar/api/ReaderMessageBar;

    return-void
.end method

.method public static injectRootViewBinder(Lcom/squareup/rootview/RootView;Lcom/squareup/rootview/RootViewBinder;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->rootViewBinder:Lcom/squareup/rootview/RootViewBinder;

    return-void
.end method

.method public static injectTouchEventMonitor(Lcom/squareup/rootview/RootView;Lcom/squareup/ui/TouchEventMonitor;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    return-void
.end method

.method public static injectTutorialCoordinator(Lcom/squareup/rootview/RootView;Lcom/squareup/tutorialv2/TutorialCoordinator;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/rootview/RootView;->tutorialCoordinator:Lcom/squareup/tutorialv2/TutorialCoordinator;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/rootview/RootView;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectMainContainer(Lcom/squareup/rootview/RootView;Lcom/squareup/container/ContainerPresenter;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->rootViewBinderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/rootview/RootViewBinder;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectRootViewBinder(Lcom/squareup/rootview/RootView;Lcom/squareup/rootview/RootViewBinder;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->touchEventMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/TouchEventMonitor;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectTouchEventMonitor(Lcom/squareup/rootview/RootView;Lcom/squareup/ui/TouchEventMonitor;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectBadMaybeSquareDeviceCheck(Lcom/squareup/rootview/RootView;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectFeatures(Lcom/squareup/rootview/RootView;Lcom/squareup/settings/server/Features;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->tutorialCoordinatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialCoordinator;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectTutorialCoordinator(Lcom/squareup/rootview/RootView;Lcom/squareup/tutorialv2/TutorialCoordinator;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/rootview/RootView_MembersInjector;->readerMessageBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/messagebar/api/ReaderMessageBar;

    invoke-static {p1, v0}, Lcom/squareup/rootview/RootView_MembersInjector;->injectReaderMessageBar(Lcom/squareup/rootview/RootView;Lcom/squareup/messagebar/api/ReaderMessageBar;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/rootview/RootView;

    invoke-virtual {p0, p1}, Lcom/squareup/rootview/RootView_MembersInjector;->injectMembers(Lcom/squareup/rootview/RootView;)V

    return-void
.end method
