.class public final Lcom/squareup/progress/ProgressBinder;
.super Ljava/lang/Object;
.source "ProgressBinder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00080\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/progress/ProgressBinder;",
        "",
        "coordinatorProvider",
        "Lcom/squareup/caller/ProgressDialogCoordinator$Provider;",
        "(Lcom/squareup/caller/ProgressDialogCoordinator$Provider;)V",
        "bindProgressDialogs",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "Lcom/squareup/progress/Progress$ScreenData;",
        "Lcom/squareup/progress/Progress$ProgressComplete;",
        "ProgressDialogFactory",
        "progress_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final coordinatorProvider:Lcom/squareup/caller/ProgressDialogCoordinator$Provider;


# direct methods
.method public constructor <init>(Lcom/squareup/caller/ProgressDialogCoordinator$Provider;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "coordinatorProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/progress/ProgressBinder;->coordinatorProvider:Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    return-void
.end method

.method public static final synthetic access$getCoordinatorProvider$p(Lcom/squareup/progress/ProgressBinder;)Lcom/squareup/caller/ProgressDialogCoordinator$Provider;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/progress/ProgressBinder;->coordinatorProvider:Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    return-object p0
.end method


# virtual methods
.method public final bindProgressDialogs()Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;"
        }
    .end annotation

    .line 24
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 25
    sget-object v1, Lcom/squareup/progress/Progress;->INSTANCE:Lcom/squareup/progress/Progress;

    invoke-virtual {v1}, Lcom/squareup/progress/Progress;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 26
    new-instance v2, Lcom/squareup/progress/ProgressBinder$bindProgressDialogs$1;

    invoke-direct {v2, p0}, Lcom/squareup/progress/ProgressBinder$bindProgressDialogs$1;-><init>(Lcom/squareup/progress/ProgressBinder;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 24
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    return-object v0
.end method
