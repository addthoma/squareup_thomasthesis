.class public interface abstract Lcom/squareup/register/tutorial/RegisterTutorial;
.super Ljava/lang/Object;
.source "RegisterTutorial.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;
    }
.end annotation


# virtual methods
.method public abstract handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
.end method

.method public abstract isTriggered()Z
.end method

.method public abstract onBarTapped()V
.end method

.method public abstract onEnd()V
.end method

.method public abstract onRequestExitTutorial()V
.end method

.method public abstract onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
.end method

.method public abstract onShowingThanks()V
.end method

.method public abstract onStart()V
.end method
