.class final enum Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;
.super Ljava/lang/Enum;
.source "AbstractFirstPaymentTutorial.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

.field public static final enum NOT_STARTED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

.field public static final enum READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

.field public static final enum RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

.field public static final enum STOPPED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 55
    new-instance v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v1, 0x0

    const-string v2, "NOT_STARTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->NOT_STARTED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 57
    new-instance v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v2, 0x1

    const-string v3, "RUNNING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 59
    new-instance v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v3, 0x2

    const-string v4, "READY_TO_FINISH"

    invoke-direct {v0, v4, v3}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 64
    new-instance v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v4, 0x3

    const-string v5, "STOPPED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->STOPPED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 53
    sget-object v5, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->NOT_STARTED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->STOPPED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->$VALUES:[Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->$VALUES:[Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    invoke-virtual {v0}, [Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    return-object v0
.end method


# virtual methods
.method public varargs in([Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;)Z
    .locals 4

    .line 68
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    if-ne p0, v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method
