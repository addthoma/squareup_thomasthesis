.class Lcom/squareup/register/tutorial/TutorialPresenter$1;
.super Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;
.source "TutorialPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/TutorialPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/tutorial/TutorialPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/register/tutorial/TutorialPresenter;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter$1;->this$0:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-direct {p0}, Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;-><init>()V

    return-void
.end method


# virtual methods
.method public handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 0

    return-void
.end method

.method public isTriggered()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onRequestExitTutorial()V
    .locals 0

    return-void
.end method

.method public onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 32
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter$1;->this$0:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    return-void
.end method

.method public onShowingThanks()V
    .locals 0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "None Tutorial"

    return-object v0
.end method
