.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "LoyaltyTourScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants;,
        Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$ParentComponent;,
        Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;,
        Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyTourScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyTourScreen.kt\ncom/squareup/register/tutorial/loyalty/LoyaltyTourScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,102:1\n35#2:103\n43#2:104\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyTourScreen.kt\ncom/squareup/register/tutorial/loyalty/LoyaltyTourScreen\n*L\n38#1:103\n71#1:104\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 $2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u0004$%&\'B\r\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u00132\u0006\u0010!\u001a\u00020\"H\u0016J\u0008\u0010#\u001a\u00020\u0017H\u0016R\u0014\u0010\n\u001a\u00020\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/LocksOrientation;",
        "Lcom/squareup/container/spot/HasSpot;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/RegistersInScope;",
        "tourType",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;",
        "(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V",
        "orientationForPhone",
        "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "getOrientationForPhone",
        "()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "orientationForTablet",
        "getOrientationForTablet",
        "getTourType",
        "()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "context",
        "Landroid/content/Context;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "register",
        "scope",
        "Lmortar/MortarScope;",
        "screenLayout",
        "Companion",
        "Component",
        "ParentComponent",
        "TutorialsConstants",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;


# instance fields
.field private final tourType:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->Companion:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;

    .line 97
    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026[parcel.readInt()])\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V
    .locals 1

    const-string v0, "tourType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->tourType:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    return-void
.end method

.method public static final loyaltyEnrollTour()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->Companion:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;->loyaltyEnrollTour()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->tourType:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-virtual {p2}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    const-string v0, "BELOW"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getTourType()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;->tourType:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    const-class v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 71
    check-cast p1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;

    .line 72
    invoke-interface {p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;->coordinator()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    const-class v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 38
    check-cast v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;

    .line 39
    invoke-interface {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;->runner()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 67
    sget v0, Lcom/squareup/pos/tutorials/R$layout;->loyalty_tour_view:I

    return v0
.end method
