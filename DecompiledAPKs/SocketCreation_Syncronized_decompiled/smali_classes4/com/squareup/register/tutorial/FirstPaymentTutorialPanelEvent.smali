.class Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "FirstPaymentTutorialPanelEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;
    }
.end annotation


# instance fields
.field final amount:J

.field final payment_type:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/String;J)V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TUTORIAL_STEP:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->name()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 13
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent;->payment_type:Ljava/lang/String;

    .line 14
    iput-wide p3, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent;->amount:J

    return-void
.end method
