.class public final Lcom/squareup/register/tutorial/PosTutorialApi_Factory;
.super Ljava/lang/Object;
.source "PosTutorialApi_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/PosTutorialApi;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final cashTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final placeholderCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final v2CreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->cardTutorialProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->cashTutorialProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->v2CreatorProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->placeholderCreatorProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p9, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/PosTutorialApi_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)",
            "Lcom/squareup/register/tutorial/PosTutorialApi_Factory;"
        }
    .end annotation

    .line 69
    new-instance v10, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/register/tutorial/PosTutorialApi;
    .locals 11

    .line 77
    new-instance v10, Lcom/squareup/register/tutorial/PosTutorialApi;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/register/tutorial/PosTutorialApi;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/PosTutorialApi;
    .locals 10

    .line 59
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->cardTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->cashTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->v2CreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->placeholderCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-static/range {v1 .. v9}, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/register/tutorial/PosTutorialApi;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/PosTutorialApi_Factory;->get()Lcom/squareup/register/tutorial/PosTutorialApi;

    move-result-object v0

    return-object v0
.end method
