.class public Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;
.super Ljava/lang/Object;
.source "FirstPaymentTutorialTextRenderer.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final readerActionFactory:Lcom/squareup/register/tutorial/ReaderActionFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/register/tutorial/ReaderActionFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/register/tutorial/ReaderActionFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p3, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->transaction:Lcom/squareup/payment/Transaction;

    .line 47
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    .line 48
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->context:Landroid/content/Context;

    .line 49
    iput-object p4, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 50
    iput-object p5, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 51
    iput-object p6, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->readerActionFactory:Lcom/squareup/register/tutorial/ReaderActionFactory;

    return-void
.end method

.method private allItems()Ljava/lang/String;
    .locals 2

    .line 243
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_all_items:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formatCardAmount(J)Ljava/lang/CharSequence;
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private formatCardMaximum()Ljava/lang/CharSequence;
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardAmount(J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private formatCardMinimum()Ljava/lang/CharSequence;
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardAmount(J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private keypad()Ljava/lang/String;
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_keypad:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private library()Ljava/lang/String;
    .locals 2

    .line 239
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/common/R$string;->navigation_open_tab_library:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private withAmount(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "amount"

    .line 218
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getCardEntryScreenText()Ljava/lang/CharSequence;
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_cnp_entry:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getContinuePaymentText()Ljava/lang/CharSequence;
    .locals 5

    .line 182
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_continue_payment:I

    sget v3, Lcom/squareup/common/strings/R$string;->continue_label:I

    const-string v4, "continue"

    invoke-static {v0, v1, v2, v4, v3}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getDoneScreenText()Ljava/lang/CharSequence;
    .locals 5

    .line 177
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_new_sale:I

    sget v3, Lcom/squareup/checkout/R$string;->new_sale:I

    const-string v4, "new_sale"

    invoke-static {v0, v1, v2, v4, v3}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getInvoiceScreenText()Ljava/lang/CharSequence;
    .locals 5

    .line 148
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_invoice:I

    sget v3, Lcom/squareup/utilities/R$string;->send:I

    const-string v4, "send"

    invoke-static {v0, v1, v2, v4, v3}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getOpenTicketsText()Ljava/lang/CharSequence;
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_choose_or_create_ticket:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentTypeScreenText(Ljava/util/EnumSet;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->readerActionFactory:Lcom/squareup/register/tutorial/ReaderActionFactory;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/ReaderActionFactory;->paymentTypePromptFor(Ljava/util/EnumSet;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getReceiptScreenText()Ljava/lang/CharSequence;
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_receipt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getSignScreenText()Ljava/lang/CharSequence;
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_sign:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getStartTextItemsKeypadPhoneLandscape()Ljava/lang/CharSequence;
    .locals 3

    .line 59
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_keypad_phone_landscape:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 60
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardMinimum()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 61
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "keypad"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getStartTextItemsKeypadPhonePortrait()Ljava/lang/CharSequence;
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_keypad_phone_portrait:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 67
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardMinimum()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 68
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "keypad"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 69
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->library()Ljava/lang/String;

    move-result-object v1

    const-string v2, "library"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getStartTextItemsKeypadTabletLandscape(Z)Ljava/lang/CharSequence;
    .locals 2

    if-nez p1, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextWithMinimum()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 77
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_keypad_tablet_landscape:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 78
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardMinimum()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "amount"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 79
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "keypad"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 80
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->allItems()Ljava/lang/String;

    move-result-object v0

    const-string v1, "all_items"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getStartTextItemsKeypadTabletPortrait(Z)Ljava/lang/CharSequence;
    .locals 2

    if-nez p1, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextWithMinimum()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 88
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_keypad_tablet_portrait:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 89
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardMinimum()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "amount"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 90
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "keypad"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getStartTextItemsLibraryPhoneLandscape(Z)Ljava/lang/CharSequence;
    .locals 1

    if-eqz p1, :cond_0

    .line 95
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_phone_landscape:I

    .line 96
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_no_items_phone_landscape:I

    .line 97
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getStartTextItemsLibraryPhonePortrait(Z)Ljava/lang/CharSequence;
    .locals 2

    if-eqz p1, :cond_0

    .line 101
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_phone_portrait:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_no_items_phone_portrait:I

    .line 104
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 105
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v0

    const-string v1, "keypad"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getStartTextItemsLibraryTabletLandscape(Z)Ljava/lang/CharSequence;
    .locals 2

    if-eqz p1, :cond_0

    .line 110
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_tablet_landscape:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_no_items_tablet_landscape:I

    .line 113
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 114
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v0

    const-string v1, "keypad"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getStartTextItemsLibraryTabletPortrait(Z)Ljava/lang/CharSequence;
    .locals 2

    if-eqz p1, :cond_0

    .line 119
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_tablet_portrait:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start_items_library_no_items_tablet_portrait:I

    .line 122
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 123
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->keypad()Ljava/lang/String;

    move-result-object v0

    const-string v1, "keypad"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getStartTextWithMinimum()Ljava/lang/CharSequence;
    .locals 1

    .line 55
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start:I

    invoke-virtual {p0, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withMinimum(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTapChargeText()Ljava/lang/CharSequence;
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_tap_charge:I

    sget v3, Lcom/squareup/checkout/R$string;->charge:I

    const-string v4, "charge"

    invoke-static {v0, v1, v2, v4, v3}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTipScreenText()Ljava/lang/CharSequence;
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_tipping:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public onHandleDoneScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;
    .locals 1

    .line 167
    const-class v0, Lcom/squareup/ui/buyer/InReceiptScreen;

    .line 168
    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasReceiptForLastPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->transaction:Lcom/squareup/payment/Transaction;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->shouldSkipReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 170
    invoke-static {p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->isReceiptCompleteScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, ""

    return-object p1

    .line 171
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getDoneScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public onHandleReceiptScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;
    .locals 1

    .line 153
    const-class v0, Lcom/squareup/ui/buyer/InReceiptScreen;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->isReceiptSelectionScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 154
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->shouldSkipReceipt()Z

    move-result p1

    if-nez p1, :cond_1

    .line 156
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getReceiptScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_1
    const-string p1, ""

    return-object p1
.end method

.method public onHandleSignatureScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;
    .locals 1

    .line 198
    const-class v0, Lcom/squareup/ui/buyer/InSignScreen;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getSignScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method public onHandleTipScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;
    .locals 0

    .line 187
    invoke-static {p1}, Lcom/squareup/checkoutflow/core/tip/TipScreen;->isTipScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getTipScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method

.method public withMaximum(I)Ljava/lang/CharSequence;
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardMaximum()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withAmount(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public withMinimum(I)Ljava/lang/CharSequence;
    .locals 1

    .line 129
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->formatCardMinimum()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withAmount(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
