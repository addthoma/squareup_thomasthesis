.class Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;
.super Lcom/squareup/tutorialv2/TutorialSeed;
.source "FirstCardPaymentTutorialV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Seed"
.end annotation


# instance fields
.field private hasItems:Z

.field private hasPosIntent:Z

.field final synthetic this$0:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;


# direct methods
.method constructor <init>(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/tutorialv2/TutorialSeed$Priority;ZZ)V
    .locals 0

    .line 599
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;->this$0:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    .line 600
    invoke-direct {p0, p2}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    .line 601
    iput-boolean p3, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;->hasItems:Z

    .line 602
    iput-boolean p4, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;->hasPosIntent:Z

    return-void
.end method


# virtual methods
.method protected doCreate()Lcom/squareup/tutorialv2/Tutorial;
    .locals 2

    .line 606
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;->this$0:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    invoke-static {v0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->access$000(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;

    const/4 v1, 0x1

    .line 607
    iput-boolean v1, v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->redirectToFavoritesOrKeypad:Z

    .line 608
    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;->hasItems:Z

    iput-boolean v1, v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    .line 609
    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;->hasPosIntent:Z

    iput-boolean v1, v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasPosBestAvailableProductIntent:Z

    return-object v0
.end method
