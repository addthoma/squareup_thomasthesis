.class public final Lcom/squareup/register/tutorial/TutorialPresenter_Factory;
.super Ljava/lang/Object;
.source "TutorialPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/TutorialPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final launcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final screenEmitterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->screenEmitterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->tutorialsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->launcherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/TutorialPresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)",
            "Lcom/squareup/register/tutorial/TutorialPresenter_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/register/tutorial/TutorialScreenEmitter;ZLdagger/Lazy;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/register/tutorial/TutorialPresenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;",
            "Z",
            "Ldagger/Lazy<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;>;",
            "Lcom/squareup/util/BrowserLauncher;",
            ")",
            "Lcom/squareup/register/tutorial/TutorialPresenter;"
        }
    .end annotation

    .line 56
    new-instance v6, Lcom/squareup/register/tutorial/TutorialPresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/tutorial/TutorialPresenter;-><init>(Ldagger/Lazy;Lcom/squareup/register/tutorial/TutorialScreenEmitter;ZLdagger/Lazy;Lcom/squareup/util/BrowserLauncher;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/TutorialPresenter;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->screenEmitterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    iget-object v2, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->tutorialsProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->launcherProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/BrowserLauncher;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/register/tutorial/TutorialScreenEmitter;ZLdagger/Lazy;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/register/tutorial/TutorialPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter_Factory;->get()Lcom/squareup/register/tutorial/TutorialPresenter;

    move-result-object v0

    return-object v0
.end method
