.class public Lcom/squareup/register/tutorial/TutorialAppletWatcher;
.super Ljava/lang/Object;
.source "TutorialAppletWatcher.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private selection:Lcom/squareup/applet/AppletSelection;

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletSelection;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher;->selection:Lcom/squareup/applet/AppletSelection;

    .line 18
    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$TutorialAppletWatcher(Lcom/squareup/applet/Applet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Applet Selected"

    invoke-interface {v0, v1, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher;->selection:Lcom/squareup/applet/AppletSelection;

    .line 23
    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$TutorialAppletWatcher$3BxNsNzt5ymzgNSDSAL031MMIYs;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$TutorialAppletWatcher$3BxNsNzt5ymzgNSDSAL031MMIYs;-><init>(Lcom/squareup/register/tutorial/TutorialAppletWatcher;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 22
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
