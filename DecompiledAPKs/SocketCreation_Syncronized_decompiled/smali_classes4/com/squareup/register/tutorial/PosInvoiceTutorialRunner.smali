.class public Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;
.super Ljava/lang/Object;
.source "PosInvoiceTutorialRunner.java"

# interfaces
.implements Lcom/squareup/register/tutorial/InvoiceTutorialRunner;


# instance fields
.field private final firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

.field private final newInvoiceFeaturesTutorial:Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    .line 15
    iput-object p2, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->newInvoiceFeaturesTutorial:Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    return-void
.end method


# virtual methods
.method public customerOnInvoice(Z)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->customerOnInvoice(Z)V

    return-void
.end method

.method public forceStartFirstInvoiceTutorial()V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->forceStart()V

    return-void
.end method

.method public invoiceCustomAmountUpdated(Z)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->onCustomAmountUpdated(Z)V

    return-void
.end method

.method public invoiceCustomerUpdated(Z)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->onContactUpdated(Z)V

    return-void
.end method

.method public isDraftInvoice(Z)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->isDraftInvoice(Z)V

    return-void
.end method

.method public itemOnInvoice(Z)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->itemOnInvoice(Z)V

    return-void
.end method

.method public readyToFinishInvoices()V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->readyToFinish()V

    return-void
.end method

.method public readyToFinishInvoicesQuietly()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->readyToFinishQuietly()V

    return-void
.end method

.method public setCreatingCustomAmount(Z)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->setIsCreatingCustomAmount(Z)V

    return-void
.end method

.method public setInvoicesAppletActiveState(Z)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->setInvoicesAppletActive(Z)V

    .line 32
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->newInvoiceFeaturesTutorial:Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->setInvoicesAppletActive(Z)V

    return-void
.end method

.method public startInvoiceTutorialIfPossible()V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->shouldActivateTutorial()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->startAndActivateTutorial()V

    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->newInvoiceFeaturesTutorial:Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->shouldActivateTutorial()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->newInvoiceFeaturesTutorial:Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    invoke-virtual {v0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;->startAndActivateTutorial()V

    :cond_1
    :goto_0
    return-void
.end method

.method public updateInvoicePreviewBanner(Ljava/lang/String;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;->firstInvoiceTutorial:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;->updatePreviewBanner(Ljava/lang/String;)V

    return-void
.end method
