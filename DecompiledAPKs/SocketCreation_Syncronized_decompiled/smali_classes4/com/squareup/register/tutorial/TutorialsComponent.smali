.class public interface abstract Lcom/squareup/register/tutorial/TutorialsComponent;
.super Ljava/lang/Object;
.source "TutorialsComponent.java"


# virtual methods
.method public abstract inject(Lcom/squareup/register/tutorial/TutorialView;)V
.end method

.method public abstract tutorialCore()Lcom/squareup/tutorialv2/TutorialCore;
.end method

.method public abstract tutorialPresenter()Lcom/squareup/register/tutorial/TutorialPresenter;
.end method
