.class public Lcom/squareup/register/tutorial/TutorialPresenter;
.super Lmortar/ViewPresenter;
.source "TutorialPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/register/tutorial/TutorialView;",
        ">;"
    }
.end annotation


# instance fields
.field private final NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

.field private active:Lcom/squareup/register/tutorial/RegisterTutorial;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdk:Z

.field private lastScreen:Lcom/squareup/container/ContainerTreeKey;

.field private final launcher:Lcom/squareup/util/BrowserLauncher;

.field private scope:Lmortar/MortarScope;

.field private final screenEmitter:Lcom/squareup/register/tutorial/TutorialScreenEmitter;

.field private final tutorials:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ldagger/Lazy;Lcom/squareup/register/tutorial/TutorialScreenEmitter;ZLdagger/Lazy;Lcom/squareup/util/BrowserLauncher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/register/tutorial/TutorialScreenEmitter;",
            "Z",
            "Ldagger/Lazy<",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;>;",
            "Lcom/squareup/util/BrowserLauncher;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 65
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 26
    new-instance v0, Lcom/squareup/register/tutorial/TutorialPresenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/register/tutorial/TutorialPresenter$1;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;)V

    iput-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

    .line 58
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

    iput-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    .line 66
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->flow:Ldagger/Lazy;

    .line 67
    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->screenEmitter:Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    .line 68
    iput-boolean p3, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->isReaderSdk:Z

    .line 69
    iput-object p4, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->tutorials:Ldagger/Lazy;

    .line 70
    iput-object p5, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->launcher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method private getFirstTriggeredTutorial()Lcom/squareup/register/tutorial/RegisterTutorial;
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->tutorials:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/RegisterTutorial;

    .line 130
    invoke-interface {v1}, Lcom/squareup/register/tutorial/RegisterTutorial;->isTriggered()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public static synthetic lambda$-URmRCozpJRLdpR8yZLeQfAjrcU(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 136
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->lastScreen:Lcom/squareup/container/ContainerTreeKey;

    .line 137
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0, p1}, Lcom/squareup/register/tutorial/RegisterTutorial;->onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private setAndStartTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 142
    invoke-static {p1}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "starting tutorial %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/RegisterTutorial;->onEnd()V

    .line 145
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    .line 146
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {p1}, Lcom/squareup/register/tutorial/RegisterTutorial;->onStart()V

    .line 147
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->lastScreen:Lcom/squareup/container/ContainerTreeKey;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0, p1}, Lcom/squareup/register/tutorial/RegisterTutorial;->onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public endTutorial()V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setAndStartTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V

    return-void
.end method

.method public hasActiveTutorial()Z
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    iget-object v1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hideTutorialBar()V
    .locals 1

    .line 176
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialView;

    if-nez v0, :cond_0

    return-void

    .line 178
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialView;->hideTutorialBar()V

    return-void
.end method

.method public launchBrowser(Ljava/lang/String;)V
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->launcher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public maybeLookForNewTutorial()V
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->isReaderSdk:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->scope:Lmortar/MortarScope;

    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hasActiveTutorial()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    return-void

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialView;

    if-nez v0, :cond_3

    return-void

    .line 119
    :cond_3
    invoke-direct {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->getFirstTriggeredTutorial()Lcom/squareup/register/tutorial/RegisterTutorial;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_2

    .line 121
    :cond_4
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->NONE:Lcom/squareup/register/tutorial/RegisterTutorial;

    :goto_2
    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setAndStartTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V

    return-void
.end method

.method onBarClicked()V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/RegisterTutorial;->onBarTapped()V

    return-void
.end method

.method onDialogButtonTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0, p1, p2}, Lcom/squareup/register/tutorial/RegisterTutorial;->handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 74
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->scope:Lmortar/MortarScope;

    .line 75
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->tutorials:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/RegisterTutorial;

    .line 76
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->screenEmitter:Lcom/squareup/register/tutorial/TutorialScreenEmitter;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/TutorialScreenEmitter;->traversalCompleting()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$TutorialPresenter$-URmRCozpJRLdpR8yZLeQfAjrcU;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$TutorialPresenter$-URmRCozpJRLdpR8yZLeQfAjrcU;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->scope:Lmortar/MortarScope;

    .line 84
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method onExitTutorial()V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/RegisterTutorial;->onRequestExitTutorial()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 88
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->maybeLookForNewTutorial()V

    return-void
.end method

.method public onShowingThanks()V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->active:Lcom/squareup/register/tutorial/RegisterTutorial;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/RegisterTutorial;->onShowingThanks()V

    return-void
.end method

.method public prompt(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    invoke-direct {v1, p1, p2}, Lcom/squareup/register/tutorial/TutorialDialogScreen;-><init>(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public replaceTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V
    .locals 0

    .line 152
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->setAndStartTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V

    return-void
.end method

.method public setContent(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x0

    .line 162
    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    return-void
.end method

.method public setContent(Ljava/lang/CharSequence;Ljava/lang/Integer;)V
    .locals 1

    .line 168
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialView;

    if-nez v0, :cond_0

    return-void

    .line 170
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialView;->showTutorialBar()V

    .line 171
    invoke-virtual {v0, p1, p2}, Lcom/squareup/register/tutorial/TutorialView;->setBarContent(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    return-void
.end method

.method public showScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialPresenter;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
