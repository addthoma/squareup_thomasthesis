.class public Lcom/squareup/register/widgets/SendReceiptNonCollapsingLinearLayout;
.super Landroid/widget/LinearLayout;
.source "SendReceiptNonCollapsingLinearLayout.java"


# instance fields
.field private minHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, -0x1

    .line 14
    iput p1, p0, Lcom/squareup/register/widgets/SendReceiptNonCollapsingLinearLayout;->minHeight:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 0

    .line 21
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 22
    invoke-virtual {p0}, Lcom/squareup/register/widgets/SendReceiptNonCollapsingLinearLayout;->getHeight()I

    move-result p1

    .line 23
    iget p2, p0, Lcom/squareup/register/widgets/SendReceiptNonCollapsingLinearLayout;->minHeight:I

    if-le p1, p2, :cond_0

    .line 24
    iput p1, p0, Lcom/squareup/register/widgets/SendReceiptNonCollapsingLinearLayout;->minHeight:I

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/SendReceiptNonCollapsingLinearLayout;->setMinimumHeight(I)V

    :cond_0
    return-void
.end method
