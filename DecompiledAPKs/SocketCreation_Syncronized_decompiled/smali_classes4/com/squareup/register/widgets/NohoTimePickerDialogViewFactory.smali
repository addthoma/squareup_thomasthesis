.class public final Lcom/squareup/register/widgets/NohoTimePickerDialogViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "NohoTimePickerDialogViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoTimePickerDialogViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 8
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 9
    const-class v3, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, v0, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 10
    sget-object v3, Lcom/squareup/register/widgets/NohoTimePickerDialogViewFactory$1;->INSTANCE:Lcom/squareup/register/widgets/NohoTimePickerDialogViewFactory$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 8
    invoke-virtual {v2, v0, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 7
    invoke-direct {p0, v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
