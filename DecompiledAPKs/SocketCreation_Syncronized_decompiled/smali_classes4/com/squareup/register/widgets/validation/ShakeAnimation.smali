.class public Lcom/squareup/register/widgets/validation/ShakeAnimation;
.super Landroid/view/animation/TranslateAnimation;
.source "ShakeAnimation.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const/high16 v0, -0x3dcc0000    # -45.0f

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>(F)V

    return-void
.end method

.method public constructor <init>(F)V
    .locals 2

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p1, v0, v0, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v0, 0x190

    .line 14
    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setDuration(J)V

    .line 15
    new-instance p1, Landroid/view/animation/BounceInterpolator;

    invoke-direct {p1}, Landroid/view/animation/BounceInterpolator;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    return-void
.end method
