.class public final Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory;
.super Ljava/lang/Object;
.source "SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory$InstanceHolder;->access$000()Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideThirdPartyGiftCardPanValidationStrategy()Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;
    .locals 2

    .line 28
    invoke-static {}, Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule;->provideThirdPartyGiftCardPanValidationStrategy()Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory;->provideThirdPartyGiftCardPanValidationStrategy()Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule_ProvideThirdPartyGiftCardPanValidationStrategyFactory;->get()Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;

    move-result-object v0

    return-object v0
.end method
