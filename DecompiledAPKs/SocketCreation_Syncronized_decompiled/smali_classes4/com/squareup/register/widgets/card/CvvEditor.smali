.class public Lcom/squareup/register/widgets/card/CvvEditor;
.super Landroid/widget/FrameLayout;
.source "CvvEditor.java"


# instance fields
.field private brandGuesser:Lrx/functions/Func0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func0<",
            "Lcom/squareup/Card$Brand;",
            ">;"
        }
    .end annotation
.end field

.field private final cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

.field private cvvScrubber:Lcom/squareup/text/CvvScrubber;

.field private cvvValid:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private focusChange:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private ignoreFocusChange:Z

.field private strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance p2, Lcom/squareup/text/CvvScrubber;

    invoke-direct {p2}, Lcom/squareup/text/CvvScrubber;-><init>()V

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvScrubber:Lcom/squareup/text/CvvScrubber;

    .line 37
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvValid:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 38
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CvvEditor;->focusChange:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 45
    sget p2, Lcom/squareup/widgets/pos/R$layout;->cvv_editor:I

    invoke-static {p1, p2, p0}, Lcom/squareup/register/widgets/card/CvvEditor;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    sget p1, Lcom/squareup/widgets/pos/R$id;->cvv_input:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/CardEditorEditText;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    .line 47
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvScrubber:Lcom/squareup/text/CvvScrubber;

    iget-object p2, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {p1, p2}, Lcom/squareup/text/CvvScrubber;->setOnInvalidContentListener(Lcom/squareup/text/OnInvalidContentListener;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    new-instance p2, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvScrubber:Lcom/squareup/text/CvvScrubber;

    invoke-direct {p2, v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/CardEditorEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/card/CvvEditor;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CvvEditor;->assertStrategyAndBrandGuesserAreSet()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/register/widgets/card/CvvEditor;)Lrx/functions/Func0;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->brandGuesser:Lrx/functions/Func0;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/register/widgets/card/CvvEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/register/widgets/card/CvvEditor;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvValid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/register/widgets/card/CvvEditor;)Lcom/squareup/register/widgets/card/CardEditorEditText;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    return-object p0
.end method

.method private addFocusAndInputListeners()V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    new-instance v1, Lcom/squareup/register/widgets/card/-$$Lambda$CvvEditor$xPvndYEhT-mlUhYKoFezC2eXpMA;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$CvvEditor$xPvndYEhT-mlUhYKoFezC2eXpMA;-><init>(Lcom/squareup/register/widgets/card/CvvEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    new-instance v1, Lcom/squareup/register/widgets/card/CvvEditor$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/CvvEditor$1;-><init>(Lcom/squareup/register/widgets/card/CvvEditor;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method private assertStrategyAndBrandGuesserAreSet()V
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "setStrategyAndBrandGuesser needs to be called after view creation."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public extractCvv()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getPaint()Landroid/text/TextPaint;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    return-object v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public isFocused()Z
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->isFocused()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$addFocusAndInputListeners$0$CvvEditor(Landroid/view/View;Z)V
    .locals 1

    .line 129
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CvvEditor;->assertStrategyAndBrandGuesserAreSet()V

    .line 131
    iget-boolean p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->ignoreFocusChange:Z

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    .line 132
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/CvvEditor;->extractCvv()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->brandGuesser:Lrx/functions/Func0;

    invoke-interface {v0}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/Card$Brand;

    invoke-interface {p1, p2, v0}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 133
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->onInvalidContent()V

    .line 135
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->focusChange:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onCvvValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvValid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onFocusChange()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->focusChange:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/register/widgets/card/CardEditorEditText;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method public setBrand(Lcom/squareup/Card$Brand;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvScrubber:Lcom/squareup/text/CvvScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/CvvScrubber;->setBrand(Lcom/squareup/Card$Brand;)V

    return-void
.end method

.method public final setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setIgnoreFocusChange()V
    .locals 1

    const/4 v0, 0x1

    .line 92
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->ignoreFocusChange:Z

    return-void
.end method

.method public setMinWidth(I)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setMinWidth(I)V

    return-void
.end method

.method public setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    return-void
.end method

.method public setSaveEnabled(Z)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setSaveEnabled(Z)V

    return-void
.end method

.method public setSelection(Landroid/text/Editable;I)V
    .locals 0

    .line 112
    invoke-static {p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    return-void
.end method

.method public setStrategyAndBrandGuesser(Lcom/squareup/register/widgets/card/PanValidationStrategy;Lrx/functions/Func0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/card/PanValidationStrategy;",
            "Lrx/functions/Func0<",
            "Lcom/squareup/Card$Brand;",
            ">;)V"
        }
    .end annotation

    .line 54
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    .line 55
    iput-object p2, p0, Lcom/squareup/register/widgets/card/CvvEditor;->brandGuesser:Lrx/functions/Func0;

    .line 56
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CvvEditor;->addFocusAndInputListeners()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CvvEditor;->cvvInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
