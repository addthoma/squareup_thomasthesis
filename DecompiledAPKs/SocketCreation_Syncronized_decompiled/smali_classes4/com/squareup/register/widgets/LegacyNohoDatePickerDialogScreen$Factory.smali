.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$Factory;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    const-string v1, "get(context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;

    .line 53
    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;

    invoke-static {v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;->access$getArgs$p(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;)Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;-><init>(Landroid/content/Context;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;)V

    invoke-virtual {v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->build()Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(DialogBuilde\u2026xt, screen.args).build())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
