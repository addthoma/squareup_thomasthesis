.class public Lcom/squareup/register/widgets/GridSizer;
.super Ljava/lang/Object;
.source "GridSizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/GridSizer$Rect;,
        Lcom/squareup/register/widgets/GridSizer$ParentSize;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActualCellWidth(Lcom/squareup/register/widgets/GridSizer$ParentSize;IIII)I
    .locals 1

    .line 53
    rem-int/2addr p1, p2

    .line 55
    invoke-virtual {p0}, Lcom/squareup/register/widgets/GridSizer$ParentSize;->getAvailableWidth()I

    move-result p0

    add-int/lit8 v0, p2, -0x1

    mul-int p4, p4, v0

    sub-int/2addr p0, p4

    mul-int p4, p3, p2

    sub-int/2addr p0, p4

    if-lez p0, :cond_0

    .line 69
    div-int p4, p0, p2

    .line 71
    rem-int/2addr p0, p2

    add-int/2addr p3, p4

    add-int/lit8 p0, p0, -0x1

    if-gt p1, p0, :cond_0

    add-int/lit8 p3, p3, 0x1

    :cond_0
    return p3
.end method

.method public static getChildBounds(Lcom/squareup/register/widgets/GridSizer$ParentSize;Lcom/squareup/register/widgets/GridSizer$Rect;III[II)V
    .locals 5

    .line 86
    rem-int v0, p3, p2

    .line 87
    div-int p2, p3, p2

    mul-int v1, p2, p4

    mul-int p4, p4, v0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/register/widgets/GridSizer$ParentSize;->getPaddingLeft()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    .line 93
    aget v4, p5, v3

    add-int/2addr v2, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    add-int/2addr v2, p4

    .line 97
    iput v2, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    mul-int p2, p2, p6

    .line 99
    invoke-virtual {p0}, Lcom/squareup/register/widgets/GridSizer$ParentSize;->getPaddingTop()I

    move-result p0

    add-int/2addr p2, p0

    add-int/2addr p2, v1

    iput p2, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    .line 100
    iget p0, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    aget p2, p5, p3

    add-int/2addr p0, p2

    iput p0, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    .line 101
    iget p0, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    add-int/2addr p0, p6

    iput p0, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    return-void
.end method

.method public static getColumnCount(Lcom/squareup/register/widgets/GridSizer$ParentSize;II)I
    .locals 3

    .line 30
    invoke-virtual {p0}, Lcom/squareup/register/widgets/GridSizer$ParentSize;->getAvailableWidth()I

    move-result p0

    .line 33
    div-int v0, p0, p1

    if-lez p2, :cond_0

    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    mul-int v1, v0, p1

    add-int/lit8 v2, v0, -0x1

    mul-int v2, v2, p2

    add-int/2addr v1, v2

    if-le v1, p0, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method public static getDesiredCellSize(Lcom/squareup/register/widgets/GridSizer$ParentSize;II)I
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/register/widgets/GridSizer$ParentSize;->getAvailableWidth()I

    move-result p0

    add-int/lit8 v0, p1, -0x1

    mul-int p2, p2, v0

    sub-int/2addr p0, p2

    .line 25
    div-int/2addr p0, p1

    return p0
.end method

.method private static getRowCount(II)I
    .locals 1

    .line 105
    div-int v0, p0, p1

    .line 106
    rem-int/2addr p0, p1

    if-lez p0, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public static getTotalHeight(Lcom/squareup/register/widgets/GridSizer$ParentSize;IIII)I
    .locals 0

    .line 14
    invoke-static {p1, p2}, Lcom/squareup/register/widgets/GridSizer;->getRowCount(II)I

    move-result p1

    mul-int p4, p4, p1

    add-int/lit8 p1, p1, -0x1

    mul-int p3, p3, p1

    add-int/2addr p4, p3

    .line 15
    invoke-virtual {p0}, Lcom/squareup/register/widgets/GridSizer$ParentSize;->getVerticalPadding()I

    move-result p0

    add-int/2addr p4, p0

    return p4
.end method
