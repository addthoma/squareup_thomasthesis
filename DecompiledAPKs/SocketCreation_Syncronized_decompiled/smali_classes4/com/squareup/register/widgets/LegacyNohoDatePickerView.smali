.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerView;
.super Landroidx/appcompat/widget/AppCompatTextView;
.source "LegacyNohoDatePickerView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Component;,
        Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyNohoDatePickerView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyNohoDatePickerView.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerView\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,143:1\n66#2:144\n1103#3,7:145\n*E\n*S KotlinDebug\n*F\n+ 1 LegacyNohoDatePickerView.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerView\n*L\n61#1:144\n67#1,7:145\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use NohoDatePickerDialogWorkflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 @2\u00020\u0001:\u0002@AB%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\n\u00101\u001a\u0004\u0018\u00010\u0010H\u0002J\u0008\u00102\u001a\u00020$H\u0014J\u0010\u00103\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0014J\u0010\u00106\u001a\u00020$2\u0006\u00107\u001a\u000208H\u0016J\u0008\u00109\u001a\u000208H\u0016J\u0010\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020=H\u0017J\u0012\u0010>\u001a\u00020$2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0002J\u0008\u0010?\u001a\u00020$H\u0002R\u001e\u0010\t\u001a\u00020\n8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR(\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00108F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u0013\"\u0004\u0008\u001e\u0010\u0015R\u001c\u0010\u001f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010\u0013\"\u0004\u0008!\u0010\u0015R*\u0010\"\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020$\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008%\u0010&\"\u0004\u0008\'\u0010(R\u001e\u0010)\u001a\u00020*8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008+\u0010,\"\u0004\u0008-\u0010.R\u000e\u0010/\u001a\u000200X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006B"
    }
    d2 = {
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerView;",
        "Landroidx/appcompat/widget/AppCompatTextView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "getCurrentTime$widgets_pos_release",
        "()Lcom/squareup/time/CurrentTime;",
        "setCurrentTime$widgets_pos_release",
        "(Lcom/squareup/time/CurrentTime;)V",
        "value",
        "Lorg/threeten/bp/LocalDate;",
        "date",
        "getDate",
        "()Lorg/threeten/bp/LocalDate;",
        "setDate",
        "(Lorg/threeten/bp/LocalDate;)V",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "getDateFormat",
        "()Ljava/text/DateFormat;",
        "edgeController",
        "Lcom/squareup/noho/NohoEdgeController;",
        "maxDate",
        "getMaxDate",
        "setMaxDate",
        "minDate",
        "getMinDate",
        "setMinDate",
        "onDateSelectedListener",
        "Lkotlin/Function1;",
        "",
        "getOnDateSelectedListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setOnDateSelectedListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "runner",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
        "getRunner$widgets_pos_release",
        "()Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
        "setRunner$widgets_pos_release",
        "(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V",
        "token",
        "",
        "getInternalDate",
        "onAttachedToWindow",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "onRestoreInstanceState",
        "state",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "onTouchEvent",
        "",
        "event",
        "Landroid/view/MotionEvent;",
        "setInternalDate",
        "showDatePickerDialog",
        "Companion",
        "Component",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Companion;

.field private static final KEY_SUPER_STATE:Ljava/lang/String; = "parent_state"

.field private static final KEY_TOKEN:Ljava/lang/String; = "picker_token"


# instance fields
.field public currentTime:Lcom/squareup/time/CurrentTime;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final dateFormat:Ljava/text/DateFormat;

.field private final edgeController:Lcom/squareup/noho/NohoEdgeController;

.field private maxDate:Lorg/threeten/bp/LocalDate;

.field private minDate:Lorg/threeten/bp/LocalDate;

.field private onDateSelectedListener:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private token:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->Companion:Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    invoke-static {p1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    const-string v1, "android.text.format.Date\u2026etLongDateFormat(context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->dateFormat:Ljava/text/DateFormat;

    .line 49
    new-instance v0, Lcom/squareup/noho/NohoEdgeController;

    move-object v3, p0

    check-cast v3, Landroid/view/View;

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v0

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/squareup/noho/NohoEdgeController;-><init>(Landroid/view/View;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    .line 51
    new-instance p2, Ljava/util/Random;

    invoke-direct {p2}, Ljava/util/Random;-><init>()V

    invoke-virtual {p2}, Ljava/util/Random;->nextLong()J

    move-result-wide p2

    iput-wide p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->token:J

    .line 144
    const-class p2, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Component;

    .line 62
    invoke-interface {p1, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$Component;->inject(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V

    const/4 p1, 0x0

    .line 64
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x1

    .line 65
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setClickable(Z)V

    .line 66
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setFocusableInTouchMode(Z)V

    .line 145
    new-instance p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$$special$$inlined$onClickDebounced$1;

    invoke-direct {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$$special$$inlined$onClickDebounced$1;-><init>()V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 36
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 37
    sget p3, Lcom/squareup/noho/R$attr;->nohoEditTextStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getToken$p(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)J
    .locals 2

    .line 33
    iget-wide v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->token:J

    return-wide v0
.end method

.method public static final synthetic access$setInternalDate(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;Lorg/threeten/bp/LocalDate;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setInternalDate(Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method public static final synthetic access$setToken$p(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;J)V
    .locals 0

    .line 33
    iput-wide p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->token:J

    return-void
.end method

.method public static final synthetic access$showDatePickerDialog(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->showDatePickerDialog()V

    return-void
.end method

.method private final getInternalDate()Lorg/threeten/bp/LocalDate;
    .locals 4

    .line 124
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "it"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 126
    iget-object v1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 127
    invoke-static {v0}, Lcom/squareup/util/Dates;->toCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 129
    invoke-static {v0}, Lcom/squareup/util/Calendars;->getYear(Ljava/util/Calendar;)I

    move-result v1

    invoke-static {v0}, Lcom/squareup/util/Calendars;->getMonth(Ljava/util/Calendar;)I

    move-result v3

    add-int/2addr v3, v2

    invoke-static {v0}, Lcom/squareup/util/Calendars;->getDay(Ljava/util/Calendar;)I

    move-result v0

    invoke-static {v1, v3, v0}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    move-object v3, v0

    :cond_3
    return-object v3
.end method

.method private final setInternalDate(Lorg/threeten/bp/LocalDate;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1}, Lcom/squareup/utilities/threeten/compat/LocalDatesKt;->toDate(Lorg/threeten/bp/LocalDate;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->onDateSelectedListener:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method private final showDatePickerDialog()V
    .locals 10

    .line 111
    invoke-direct {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getInternalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    if-nez v1, :cond_0

    const-string v2, "runner"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 113
    :cond_0
    new-instance v2, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    .line 114
    iget-wide v4, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->token:J

    if-eqz v0, :cond_1

    move-object v6, v0

    goto :goto_0

    .line 115
    :cond_1
    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->currentTime:Lcom/squareup/time/CurrentTime;

    if-nez v3, :cond_2

    const-string v6, "currentTime"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-interface {v3}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    move-object v6, v3

    .line 116
    :goto_0
    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->minDate:Lorg/threeten/bp/LocalDate;

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/squareup/noho/NohoDatePicker;->Companion:Lcom/squareup/noho/NohoDatePicker$Companion;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoDatePicker$Companion;->getDEFAULT_MIN_DATE()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    :goto_1
    move-object v7, v3

    .line 117
    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->maxDate:Lorg/threeten/bp/LocalDate;

    if-eqz v3, :cond_4

    goto :goto_2

    :cond_4
    sget-object v3, Lcom/squareup/noho/NohoDatePicker;->Companion:Lcom/squareup/noho/NohoDatePicker$Companion;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoDatePicker$Companion;->getDEFAULT_MAX_DATE()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    :goto_2
    move-object v8, v3

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    const/4 v9, 0x1

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    const/4 v9, 0x0

    :goto_3
    move-object v3, v2

    .line 113
    invoke-direct/range {v3 .. v9}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;-><init>(JLorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)V

    .line 112
    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->showDatePickerDialog(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;)V

    return-void
.end method


# virtual methods
.method public final getCurrentTime$widgets_pos_release()Lcom/squareup/time/CurrentTime;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->currentTime:Lcom/squareup/time/CurrentTime;

    if-nez v0, :cond_0

    const-string v1, "currentTime"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 57
    invoke-direct {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getInternalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public final getDateFormat()Ljava/text/DateFormat;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->dateFormat:Ljava/text/DateFormat;

    return-object v0
.end method

.method public final getMaxDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->maxDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getMinDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->minDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getOnDateSelectedListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->onDateSelectedListener:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getRunner$widgets_pos_release()Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    if-nez v0, :cond_0

    const-string v1, "runner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 74
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatTextView;->onAttachedToWindow()V

    .line 76
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    if-nez v0, :cond_0

    const-string v1, "runner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->datePicked()Lrx/Observable;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$1;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$2;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$2;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.datePicked()\n    \u2026etInternalDate(it.date) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    move-object v1, p0

    check-cast v1, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEdgeController;->onDraw(Landroid/graphics/Canvas;)V

    .line 92
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 102
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 106
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "parent_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroidx/appcompat/widget/AppCompatTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "picker_token"

    .line 107
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->token:J

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 95
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 96
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "parent_state"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 97
    iget-wide v1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->token:J

    const-string v3, "picker_token"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 95
    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->performClick()Z

    .line 87
    :cond_0
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public final setCurrentTime$widgets_pos_release(Lcom/squareup/time/CurrentTime;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->currentTime:Lcom/squareup/time/CurrentTime;

    return-void
.end method

.method public final setDate(Lorg/threeten/bp/LocalDate;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setInternalDate(Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method public final setMaxDate(Lorg/threeten/bp/LocalDate;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->maxDate:Lorg/threeten/bp/LocalDate;

    return-void
.end method

.method public final setMinDate(Lorg/threeten/bp/LocalDate;)V
    .locals 0

    .line 52
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->minDate:Lorg/threeten/bp/LocalDate;

    return-void
.end method

.method public final setOnDateSelectedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 54
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->onDateSelectedListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setRunner$widgets_pos_release(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    return-void
.end method
