.class public final Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;
.super Ljava/lang/Object;
.source "NohoDurationPickerRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;)Lcom/squareup/register/widgets/NohoDurationPickerRunner;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;-><init>(Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/register/widgets/NohoDurationPickerRunner;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-static {v0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;->newInstance(Lflow/Flow;)Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner_Factory;->get()Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-result-object v0

    return-object v0
.end method
