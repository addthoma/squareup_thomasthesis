.class final Lcom/squareup/register/widgets/LearnMoreMessages$1;
.super Ljava/lang/Object;
.source "LearnMoreMessages.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/LearnMoreMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/register/widgets/LearnMoreMessages;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/LearnMoreMessages;
    .locals 7

    .line 10
    new-instance v6, Lcom/squareup/register/widgets/LearnMoreMessages;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/LearnMoreMessages;-><init>(Ljava/lang/String;IIII)V

    return-object v6
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LearnMoreMessages$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/register/widgets/LearnMoreMessages;
    .locals 0

    .line 15
    new-array p1, p1, [Lcom/squareup/register/widgets/LearnMoreMessages;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LearnMoreMessages$1;->newArray(I)[Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object p1

    return-object p1
.end method
