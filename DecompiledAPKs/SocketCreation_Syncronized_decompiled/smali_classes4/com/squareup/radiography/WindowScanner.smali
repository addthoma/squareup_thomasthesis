.class public Lcom/squareup/radiography/WindowScanner;
.super Ljava/lang/Object;
.source "WindowScanner.java"


# static fields
.field private static final GET_DEFAULT_IMPL:Ljava/lang/String; = "getDefault"

.field private static final GET_GLOBAL_INSTANCE:Ljava/lang/String; = "getInstance"

.field private static final INSTANCE:Lcom/squareup/radiography/WindowScanner;

.field private static final VIEWS_FIELD:Ljava/lang/String; = "mViews"

.field private static final WINDOW_MANAGER_GLOBAL_CLASS:Ljava/lang/String; = "android.view.WindowManagerGlobal"

.field private static final WINDOW_MANAGER_IMPL_CLASS:Ljava/lang/String; = "android.view.WindowManagerImpl"


# instance fields
.field private initialized:Z

.field private viewsField:Ljava/lang/reflect/Field;

.field private windowManager:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/radiography/WindowScanner;

    invoke-direct {v0}, Lcom/squareup/radiography/WindowScanner;-><init>()V

    sput-object v0, Lcom/squareup/radiography/WindowScanner;->INSTANCE:Lcom/squareup/radiography/WindowScanner;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/squareup/radiography/WindowScanner;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/radiography/WindowScanner;->INSTANCE:Lcom/squareup/radiography/WindowScanner;

    return-object v0
.end method

.method private initialize()V
    .locals 5

    const/4 v0, 0x1

    .line 61
    iput-boolean v0, p0, Lcom/squareup/radiography/WindowScanner;->initialized:Z

    .line 64
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-le v1, v2, :cond_0

    const-string v1, "android.view.WindowManagerGlobal"

    const-string v2, "getInstance"

    goto :goto_0

    :cond_0
    const-string v1, "android.view.WindowManagerImpl"

    const-string v2, "getDefault"

    .line 73
    :goto_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Class;

    .line 74
    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v4, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 75
    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/radiography/WindowScanner;->windowManager:Ljava/lang/Object;

    const-string v2, "mViews"

    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/radiography/WindowScanner;->viewsField:Ljava/lang/reflect/Field;

    .line 77
    iget-object v1, p0, Lcom/squareup/radiography/WindowScanner;->viewsField:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public declared-synchronized findAllRootViews()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 38
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/radiography/WindowScanner;->initialized:Z

    if-nez v0, :cond_0

    .line 39
    invoke-direct {p0}, Lcom/squareup/radiography/WindowScanner;->initialize()V

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/squareup/radiography/WindowScanner;->windowManager:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/radiography/WindowScanner;->viewsField:Ljava/lang/reflect/Field;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    goto :goto_0

    .line 47
    :cond_1
    :try_start_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_2

    .line 48
    iget-object v0, p0, Lcom/squareup/radiography/WindowScanner;->viewsField:Ljava/lang/reflect/Field;

    iget-object v1, p0, Lcom/squareup/radiography/WindowScanner;->windowManager:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    check-cast v0, [Landroid/view/View;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 51
    :cond_2
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/radiography/WindowScanner;->viewsField:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lcom/squareup/radiography/WindowScanner;->windowManager:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 56
    :catch_0
    :try_start_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    .line 54
    :catch_1
    :try_start_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object v0

    .line 43
    :cond_3
    :goto_0
    :try_start_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
