.class public Lcom/squareup/referrals/ReferralRunner$ReferralData;
.super Ljava/lang/Object;
.source "ReferralRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/referrals/ReferralRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReferralData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
    }
.end annotation


# instance fields
.field public final balance:Ljava/lang/CharSequence;

.field public final expiresAt:Ljava/lang/CharSequence;

.field public final freeProcessingNote:Ljava/lang/CharSequence;

.field public final freeProcessingTitle:Ljava/lang/CharSequence;

.field public final hasError:Z

.field public final prettyUrl:Ljava/lang/String;

.field private final response:Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;


# direct methods
.method private constructor <init>(ZLcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-boolean p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->hasError:Z

    .line 194
    iput-object p2, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->response:Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    .line 195
    iput-object p3, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->balance:Ljava/lang/CharSequence;

    .line 196
    iput-object p4, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->expiresAt:Ljava/lang/CharSequence;

    .line 197
    iput-object p5, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->freeProcessingTitle:Ljava/lang/CharSequence;

    .line 198
    iput-object p6, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->freeProcessingNote:Ljava/lang/CharSequence;

    .line 199
    iput-object p7, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->prettyUrl:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(ZLcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/referrals/ReferralRunner$1;)V
    .locals 0

    .line 181
    invoke-direct/range {p0 .. p7}, Lcom/squareup/referrals/ReferralRunner$ReferralData;-><init>(ZLcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/referrals/ReferralRunner$ReferralData;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;
    .locals 0

    .line 181
    iget-object p0, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->response:Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    return-object p0
.end method

.method public static error()Lcom/squareup/referrals/ReferralRunner$ReferralData;
    .locals 9

    .line 207
    new-instance v8, Lcom/squareup/referrals/ReferralRunner$ReferralData;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/referrals/ReferralRunner$ReferralData;-><init>(ZLcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    return-object v8
.end method


# virtual methods
.method public hasBalance()Z
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData;->balance:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
