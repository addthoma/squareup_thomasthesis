.class public final enum Lcom/squareup/referrals/ReferralScreen$Style;
.super Ljava/lang/Enum;
.source "ReferralScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/referrals/ReferralScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Style"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/referrals/ReferralScreen$Style;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/referrals/ReferralScreen$Style;

.field public static final enum ACTION_BAR:Lcom/squareup/referrals/ReferralScreen$Style;

.field public static final enum ACTION_BAR_MAYBE_MASTER_DETAIL:Lcom/squareup/referrals/ReferralScreen$Style;

.field public static final enum ONBOARDING:Lcom/squareup/referrals/ReferralScreen$Style;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 24
    new-instance v0, Lcom/squareup/referrals/ReferralScreen$Style;

    const/4 v1, 0x0

    const-string v2, "ONBOARDING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/referrals/ReferralScreen$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/referrals/ReferralScreen$Style;->ONBOARDING:Lcom/squareup/referrals/ReferralScreen$Style;

    new-instance v0, Lcom/squareup/referrals/ReferralScreen$Style;

    const/4 v2, 0x1

    const-string v3, "ACTION_BAR"

    invoke-direct {v0, v3, v2}, Lcom/squareup/referrals/ReferralScreen$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/referrals/ReferralScreen$Style;->ACTION_BAR:Lcom/squareup/referrals/ReferralScreen$Style;

    new-instance v0, Lcom/squareup/referrals/ReferralScreen$Style;

    const/4 v3, 0x2

    const-string v4, "ACTION_BAR_MAYBE_MASTER_DETAIL"

    invoke-direct {v0, v4, v3}, Lcom/squareup/referrals/ReferralScreen$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/referrals/ReferralScreen$Style;->ACTION_BAR_MAYBE_MASTER_DETAIL:Lcom/squareup/referrals/ReferralScreen$Style;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/referrals/ReferralScreen$Style;

    sget-object v4, Lcom/squareup/referrals/ReferralScreen$Style;->ONBOARDING:Lcom/squareup/referrals/ReferralScreen$Style;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/referrals/ReferralScreen$Style;->ACTION_BAR:Lcom/squareup/referrals/ReferralScreen$Style;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/referrals/ReferralScreen$Style;->ACTION_BAR_MAYBE_MASTER_DETAIL:Lcom/squareup/referrals/ReferralScreen$Style;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/referrals/ReferralScreen$Style;->$VALUES:[Lcom/squareup/referrals/ReferralScreen$Style;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/referrals/ReferralScreen$Style;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/referrals/ReferralScreen$Style;

    return-object p0
.end method

.method public static values()[Lcom/squareup/referrals/ReferralScreen$Style;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/referrals/ReferralScreen$Style;->$VALUES:[Lcom/squareup/referrals/ReferralScreen$Style;

    invoke-virtual {v0}, [Lcom/squareup/referrals/ReferralScreen$Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/referrals/ReferralScreen$Style;

    return-object v0
.end method
