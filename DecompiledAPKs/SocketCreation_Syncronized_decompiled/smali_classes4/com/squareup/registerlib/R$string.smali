.class public final Lcom/squareup/registerlib/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/registerlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activate_account:I = 0x7f120040

.field public static final add_card:I = 0x7f12007c

.field public static final add_card_on_file:I = 0x7f12007d

.field public static final add_gift_card_or_swipe:I = 0x7f12007f

.field public static final add_item:I = 0x7f120080

.field public static final add_photo:I = 0x7f120092

.field public static final allow_offline_mode:I = 0x7f1200c7

.field public static final allow_offline_mode_short:I = 0x7f1200c8

.field public static final am:I = 0x7f1200c9

.field public static final apply_modifier_set:I = 0x7f1200da

.field public static final approved:I = 0x7f1200dc

.field public static final balance:I = 0x7f12011d

.field public static final bank_account_title:I = 0x7f120151

.field public static final brightness_uppercase:I = 0x7f120192

.field public static final bulk_settle_no_search_results:I = 0x7f1201a2

.field public static final buyer_display_settings_label:I = 0x7f120205

.field public static final buyer_order_name_action_bar:I = 0x7f120212

.field public static final buyer_remote_receipt_cash_with_change_title:I = 0x7f12025b

.field public static final canceling:I = 0x7f1202ae

.field public static final card:I = 0x7f1202d0

.field public static final card_on_file:I = 0x7f120317

.field public static final cards_on_file:I = 0x7f120345

.field public static final cart_view_title:I = 0x7f12035e

.field public static final cash:I = 0x7f12035f

.field public static final categories_hint_url:I = 0x7f1203d6

.field public static final category_delete_button:I = 0x7f1203d9

.field public static final category_delete_message:I = 0x7f1203da

.field public static final category_name_required_warning_message:I = 0x7f1203dc

.field public static final category_name_required_warning_title:I = 0x7f1203dd

.field public static final category_new_hint:I = 0x7f1203de

.field public static final category_row_delete_content_description:I = 0x7f1203e0

.field public static final choose_account_type:I = 0x7f12041e

.field public static final choose_photo:I = 0x7f120420

.field public static final clock_skew_error_button:I = 0x7f12042a

.field public static final clock_skew_error_message:I = 0x7f12042b

.field public static final clock_skew_error_title:I = 0x7f12042c

.field public static final contact_square_support:I = 0x7f120497

.field public static final contactless:I = 0x7f120499

.field public static final content_description_clear_search:I = 0x7f1204be

.field public static final content_description_search_items:I = 0x7f1204ce

.field public static final create_discount:I = 0x7f1205bd

.field public static final create_item:I = 0x7f1205bf

.field public static final create_modifier_set:I = 0x7f1205e5

.field public static final create_service:I = 0x7f1205ea

.field public static final credit_card:I = 0x7f1205f0

.field public static final crm_customer_management_settings_in_cart_hint:I = 0x7f120687

.field public static final crm_customer_management_settings_in_cart_label:I = 0x7f120688

.field public static final crm_customer_management_settings_post_transaction_hint:I = 0x7f120689

.field public static final crm_customer_management_settings_post_transaction_label:I = 0x7f12068a

.field public static final crm_customer_management_settings_save_card_hint:I = 0x7f12068b

.field public static final crm_customer_management_settings_save_card_label:I = 0x7f12068c

.field public static final crm_customer_management_settings_save_card_label_short:I = 0x7f12068d

.field public static final crm_customer_management_settings_save_card_post_transaction_hint:I = 0x7f12068e

.field public static final crm_customer_management_settings_save_card_post_transaction_label:I = 0x7f12068f

.field public static final crm_email_collection_settings_screen_label:I = 0x7f1206ba

.field public static final crm_email_collection_settings_url:I = 0x7f1206bb

.field public static final current_drawer_start_drawer:I = 0x7f120793

.field public static final customer_checkout_default:I = 0x7f12079d

.field public static final customer_checkout_default_hint:I = 0x7f12079e

.field public static final customer_checkout_show_cart:I = 0x7f1207a0

.field public static final customer_checkout_show_cart_hint:I = 0x7f1207a1

.field public static final customer_checkout_standard:I = 0x7f1207a2

.field public static final customer_typing_body:I = 0x7f1207aa

.field public static final dashboard_account_relative_url:I = 0x7f1207c1

.field public static final dashboard_discount_url:I = 0x7f1207c2

.field public static final dashboard_employees_relative_url:I = 0x7f1207c3

.field public static final dashboard_modifier_url:I = 0x7f1207c5

.field public static final dashboard_team_permissions_relative_url:I = 0x7f1207c7

.field public static final dashboard_timecards_relative_url:I = 0x7f1207c8

.field public static final date_format:I = 0x7f1207c9

.field public static final date_of_birth:I = 0x7f1207cb

.field public static final day_format:I = 0x7f1207d9

.field public static final deposit_options_url:I = 0x7f1207f4

.field public static final deposit_schedule_close_of_day:I = 0x7f1207fa

.field public static final deposit_speed:I = 0x7f1207fe

.field public static final deposit_speed_custom:I = 0x7f1207ff

.field public static final deposit_speed_hint:I = 0x7f120800

.field public static final deposit_speed_one_to_two_business_days:I = 0x7f120802

.field public static final deposit_speed_same_day:I = 0x7f120803

.field public static final device_name:I = 0x7f12083a

.field public static final device_name_hint:I = 0x7f12083b

.field public static final discount_delete:I = 0x7f12087d

.field public static final discount_help_text:I = 0x7f12087e

.field public static final discount_name_hint:I = 0x7f120880

.field public static final display_disconnected_successful_payment_body:I = 0x7f120890

.field public static final display_disconnected_successful_payment_title:I = 0x7f120891

.field public static final display_disconnected_unsuccessful_payment_body:I = 0x7f120892

.field public static final display_disconnected_unsuccessful_payment_title:I = 0x7f120893

.field public static final done_editing:I = 0x7f1208d4

.field public static final due_date:I = 0x7f1208e5

.field public static final edit_category_tile:I = 0x7f1208fc

.field public static final edit_item_name_hint:I = 0x7f120923

.field public static final edit_item_photo_instructions:I = 0x7f120927

.field public static final edit_photo:I = 0x7f12094b

.field public static final employee_management_dashboard_url:I = 0x7f120a0f

.field public static final enabled:I = 0x7f120a75

.field public static final facebook_url:I = 0x7f120aa6

.field public static final favorite_tooltip_message:I = 0x7f120ab0

.field public static final favorite_tooltip_title:I = 0x7f120ab1

.field public static final feedback_url:I = 0x7f120aba

.field public static final firmware_updating:I = 0x7f120ac1

.field public static final firmware_version:I = 0x7f120ac2

.field public static final first_payment_dialog_action:I = 0x7f120ad1

.field public static final first_payment_dialog_content:I = 0x7f120ad2

.field public static final first_payment_dialog_skip_walkthrough:I = 0x7f120ad3

.field public static final foreground_service_notification_message:I = 0x7f120ad7

.field public static final foreground_service_notification_title:I = 0x7f120ad8

.field public static final forgot_password_send:I = 0x7f120adb

.field public static final free_processing_url:I = 0x7f120ae1

.field public static final full_refund:I = 0x7f120ae5

.field public static final gift_card_value:I = 0x7f120b2a

.field public static final giftcards_url:I = 0x7f120b42

.field public static final hardware_serial_number:I = 0x7f120b4c

.field public static final instant_deposits_allow_switch:I = 0x7f120c0f

.field public static final instant_deposits_deposit_schedule_url:I = 0x7f120c20

.field public static final instant_deposits_section_name_deposit_schedule:I = 0x7f120c2e

.field public static final instant_deposits_section_name_instant_deposit:I = 0x7f120c2f

.field public static final instant_deposits_set_up_instant_deposit:I = 0x7f120c30

.field public static final invalid_email:I = 0x7f120c59

.field public static final invalid_email_message:I = 0x7f120c5a

.field public static final invalid_routing:I = 0x7f120c66

.field public static final inventory_update_required_message:I = 0x7f120c6f

.field public static final inventory_update_required_title:I = 0x7f120c70

.field public static final invoice_send:I = 0x7f120d8d

.field public static final item_appeareance_image_tile_label:I = 0x7f120dcb

.field public static final item_appeareance_text_tile_label:I = 0x7f120dcf

.field public static final item_library_categories:I = 0x7f120e22

.field public static final item_note:I = 0x7f120e3b

.field public static final item_setup:I = 0x7f120e3e

.field public static final item_setup_grid:I = 0x7f120e3f

.field public static final item_setup_message:I = 0x7f120e40

.field public static final item_variation_sku_hint:I = 0x7f120e44

.field public static final items_hint_url:I = 0x7f120e6e

.field public static final keypad:I = 0x7f120e7b

.field public static final label_color:I = 0x7f120ead

.field public static final later:I = 0x7f120eb2

.field public static final library:I = 0x7f120ec8

.field public static final long_friday:I = 0x7f120efd

.field public static final long_monday:I = 0x7f120efe

.field public static final long_saturday:I = 0x7f120eff

.field public static final long_sunday:I = 0x7f120f00

.field public static final long_thursday:I = 0x7f120f01

.field public static final long_tuesday:I = 0x7f120f02

.field public static final long_wednesday:I = 0x7f120f03

.field public static final loyalty_url:I = 0x7f120f89

.field public static final masked_numbers:I = 0x7f120f95

.field public static final message:I = 0x7f120fc4

.field public static final messages_count_multiple:I = 0x7f120fe4

.field public static final messages_count_one:I = 0x7f120fe5

.field public static final messages_visit_help_center:I = 0x7f120fe8

.field public static final modifier_assignment_title:I = 0x7f120ffb

.field public static final modifier_assignment_title_no_name:I = 0x7f120ffc

.field public static final modifier_hint_url:I = 0x7f121003

.field public static final modifier_items_count_plural:I = 0x7f121005

.field public static final modifier_items_count_single:I = 0x7f121006

.field public static final modifier_items_count_zero:I = 0x7f121007

.field public static final modifier_option_delete_content_description:I = 0x7f12100b

.field public static final modifier_option_name_hint:I = 0x7f12100c

.field public static final modifier_option_name_required_warning_message:I = 0x7f12100d

.field public static final modifier_option_required_warning_message:I = 0x7f12100e

.field public static final modifier_option_required_warning_title:I = 0x7f12100f

.field public static final modifier_options_count_plural:I = 0x7f121010

.field public static final modifier_options_count_single:I = 0x7f121011

.field public static final modifier_options_count_zero:I = 0x7f121012

.field public static final modifier_options_hint:I = 0x7f121013

.field public static final modifier_set:I = 0x7f121016

.field public static final modifier_set_advanced_modifier_instruction:I = 0x7f121017

.field public static final modifier_set_delete_button:I = 0x7f121018

.field public static final modifier_set_name_required_warning_message:I = 0x7f121019

.field public static final modifier_set_null_subtitle:I = 0x7f12101a

.field public static final modifier_set_null_title:I = 0x7f12101b

.field public static final new_card:I = 0x7f121064

.field public static final new_category:I = 0x7f121065

.field public static final no:I = 0x7f12107a

.field public static final no_sale:I = 0x7f12108e

.field public static final no_sale_uppercase:I = 0x7f121090

.field public static final no_thanks:I = 0x7f121091

.field public static final offline_mode:I = 0x7f1210cb

.field public static final open_tickets:I = 0x7f121169

.field public static final open_tickets_as_home_screen_toggle_hint:I = 0x7f12116b

.field public static final open_tickets_as_home_screen_toggle_text:I = 0x7f12116c

.field public static final open_tickets_as_home_screen_toggle_text_short:I = 0x7f12116d

.field public static final open_tickets_card_not_stored:I = 0x7f12116f

.field public static final open_tickets_delete_ticket_confirm:I = 0x7f121176

.field public static final open_tickets_merge_ticket:I = 0x7f12118e

.field public static final open_tickets_move_ticket:I = 0x7f121196

.field public static final open_tickets_no_tickets:I = 0x7f1211a0

.field public static final open_tickets_print_bill:I = 0x7f1211a5

.field public static final open_tickets_reprint_ticket:I = 0x7f1211a6

.field public static final open_tickets_search_people:I = 0x7f1211aa

.field public static final open_tickets_sort_by_employee:I = 0x7f1211ac

.field public static final open_tickets_sort_by_name:I = 0x7f1211ad

.field public static final open_tickets_sort_by_recent:I = 0x7f1211ae

.field public static final open_tickets_sort_by_total:I = 0x7f1211af

.field public static final open_tickets_split_ticket:I = 0x7f1211b0

.field public static final open_tickets_ticket_saved:I = 0x7f1211b7

.field public static final open_tickets_toggle_text:I = 0x7f1211bc

.field public static final open_tickets_toggle_text_short:I = 0x7f1211bd

.field public static final open_tickets_transfer_ticket:I = 0x7f1211c3

.field public static final open_tickets_uppercase_sort_employee:I = 0x7f1211c9

.field public static final open_tickets_uppercase_sort_name:I = 0x7f1211ca

.field public static final open_tickets_uppercase_sort_recent:I = 0x7f1211cb

.field public static final open_tickets_uppercase_sort_total:I = 0x7f1211cc

.field public static final open_tickets_url:I = 0x7f1211cd

.field public static final outstanding:I = 0x7f1212f9

.field public static final page_label_edit_hint:I = 0x7f1212fa

.field public static final page_label_help_text:I = 0x7f1212fc

.field public static final pairing_fallback_help:I = 0x7f12130c

.field public static final pairing_fallback_no_readers_found:I = 0x7f12130e

.field public static final paper_signature_additional_auth_slip:I = 0x7f12131c

.field public static final paper_signature_additional_auth_slip_hint:I = 0x7f12131d

.field public static final paper_signature_automatically_print_receipt:I = 0x7f121321

.field public static final paper_signature_help_url:I = 0x7f121323

.field public static final paper_signature_sign_and_tip_on_printed_receipts_without_printer_dialog_content:I = 0x7f12132f

.field public static final paper_signature_sign_on_device:I = 0x7f121330

.field public static final paper_signature_sign_on_device_short:I = 0x7f121333

.field public static final paper_signature_sign_on_printed_receipt:I = 0x7f121334

.field public static final paper_signature_sign_on_printed_receipt_short:I = 0x7f121336

.field public static final partial_auth_insufficient_balance:I = 0x7f121344

.field public static final partial_auth_message2:I = 0x7f121346

.field public static final pass:I = 0x7f12135e

.field public static final pay_other_tender_button:I = 0x7f1213ac

.field public static final payment_devices_connect:I = 0x7f1213b4

.field public static final payment_devices_learn_more_check_compatibility:I = 0x7f1213b6

.field public static final payment_devices_learn_more_connect_wirelessly:I = 0x7f1213b7

.field public static final payment_devices_learn_more_contactless_message:I = 0x7f1213b8

.field public static final payment_devices_learn_more_magstripe_message:I = 0x7f1213b9

.field public static final payment_devices_learn_more_order_reader:I = 0x7f1213ba

.field public static final payment_failed_message:I = 0x7f1213c2

.field public static final payment_type_above_maximum:I = 0x7f1213fb

.field public static final payment_type_below_minimum:I = 0x7f1213ff

.field public static final payment_type_other_uppercase:I = 0x7f121408

.field public static final payment_type_zero_amount:I = 0x7f12140a

.field public static final payment_type_zero_amount_uppercase:I = 0x7f12140b

.field public static final payment_types_settings_minimum_1_payment_type_required:I = 0x7f121419

.field public static final payment_types_settings_preview:I = 0x7f12141a

.field public static final payroll_learn_more:I = 0x7f121421

.field public static final payroll_url:I = 0x7f121426

.field public static final photo_error:I = 0x7f12143b

.field public static final pm:I = 0x7f12144d

.field public static final pre_charge_card_inserted:I = 0x7f12145d

.field public static final pre_charge_card_must_be_inserted:I = 0x7f12145e

.field public static final pre_charge_card_swiped:I = 0x7f12145f

.field public static final predefined_tickets_automatic_ticket_names:I = 0x7f12146a

.field public static final predefined_tickets_confirm_delete_ticket_group:I = 0x7f12146d

.field public static final predefined_tickets_count:I = 0x7f12146f

.field public static final predefined_tickets_custom_ticket_names:I = 0x7f121472

.field public static final predefined_tickets_custom_ticket_names_hint:I = 0x7f121473

.field public static final predefined_tickets_delete_ticket_group:I = 0x7f121474

.field public static final predefined_tickets_group:I = 0x7f121479

.field public static final predefined_tickets_no_tickets_message:I = 0x7f121481

.field public static final predefined_tickets_opt_in_button_text:I = 0x7f121484

.field public static final predefined_tickets_preview:I = 0x7f121488

.field public static final predefined_tickets_select_ticket_group:I = 0x7f12148a

.field public static final predefined_tickets_show_in_ticket_group:I = 0x7f12148b

.field public static final predefined_tickets_tickets:I = 0x7f121493

.field public static final predefined_tickets_toggle_hint:I = 0x7f121494

.field public static final predefined_tickets_toggle_text:I = 0x7f121495

.field public static final predefined_tickets_toggle_text_short:I = 0x7f121496

.field public static final print_a_ticket_for_each_item_label:I = 0x7f12149f

.field public static final print_compact_tickets_label:I = 0x7f1214a2

.field public static final print_order_ticket_stubs_value:I = 0x7f1214a5

.field public static final print_order_tickets_autonumber_label:I = 0x7f1214a6

.field public static final print_order_tickets_custom_name_label:I = 0x7f1214a8

.field public static final print_order_tickets_help_message:I = 0x7f1214a9

.field public static final print_order_tickets_no_categories_message:I = 0x7f1214aa

.field public static final print_order_tickets_value:I = 0x7f1214ab

.field public static final print_receipts:I = 0x7f1214ac

.field public static final printer_connected:I = 0x7f1214b7

.field public static final printer_disconnected:I = 0x7f1214b9

.field public static final printer_nickname:I = 0x7f1214bb

.field public static final printer_stations_new_station_hint:I = 0x7f1214cb

.field public static final printer_stations_remove_button_confirmation:I = 0x7f1214d0

.field public static final printer_stations_remove_button_text:I = 0x7f1214d1

.field public static final printer_stations_role_not_supported:I = 0x7f1214d2

.field public static final printer_stations_select_hardware_printer_title:I = 0x7f1214da

.field public static final printer_stations_set_up_printers_url:I = 0x7f1214db

.field public static final printer_uppercase_from_this_device:I = 0x7f1214e4

.field public static final printer_uppercase_include_on_tickets:I = 0x7f1214e5

.field public static final printer_uppercase_order_tickets:I = 0x7f1214e6

.field public static final prod_base_url:I = 0x7f1214f9

.field public static final reader_type_screen_button_r12:I = 0x7f1215a5

.field public static final receipt_title:I = 0x7f1215d3

.field public static final record_card_payment:I = 0x7f1215df

.field public static final refunded:I = 0x7f12165c

.field public static final remove:I = 0x7f121668

.field public static final remove_attachment:I = 0x7f121669

.field public static final remove_tax:I = 0x7f121673

.field public static final restart:I = 0x7f12169b

.field public static final routing_number:I = 0x7f1216ac

.field public static final save_item:I = 0x7f121770

.field public static final search_library_hint_all_services:I = 0x7f12178f

.field public static final secondary_button_save:I = 0x7f121795

.field public static final settings_title:I = 0x7f1217d8

.field public static final shared_settings_empty_state_message_text:I = 0x7f1217e2

.field public static final shared_settings_empty_state_title_text:I = 0x7f1217e3

.field public static final short_friday:I = 0x7f1217ed

.field public static final short_monday:I = 0x7f1217ee

.field public static final short_saturday:I = 0x7f1217ef

.field public static final short_sunday:I = 0x7f1217f0

.field public static final short_thursday:I = 0x7f1217f1

.field public static final short_tuesday:I = 0x7f1217f2

.field public static final short_wednesday:I = 0x7f1217f3

.field public static final signature_clear_signature:I = 0x7f1217fe

.field public static final skip_device_tour_cancel_exit:I = 0x7f121810

.field public static final skip_device_tour_confirm_exit:I = 0x7f121811

.field public static final skip_device_tour_message:I = 0x7f121812

.field public static final skip_device_tour_title:I = 0x7f121813

.field public static final skip_receipt_screen:I = 0x7f121815

.field public static final sku_not_found_message:I = 0x7f121822

.field public static final split_tender_unavailable_message:I = 0x7f121862

.field public static final split_tender_unavailable_title:I = 0x7f121863

.field public static final split_ticket_null_state:I = 0x7f121868

.field public static final square_market_url:I = 0x7f121894

.field public static final square_reader_bluetooth_info_page_name:I = 0x7f121896

.field public static final square_reader_bluetooth_info_page_name_short:I = 0x7f121897

.field public static final square_relative_url:I = 0x7f12189e

.field public static final square_support:I = 0x7f1218a4

.field public static final staging_base_url:I = 0x7f1218ab

.field public static final start_drawer_modal_text:I = 0x7f1218bb

.field public static final start_drawer_modal_title:I = 0x7f1218bc

.field public static final stop_movie:I = 0x7f1218cc

.field public static final submit:I = 0x7f1218ce

.field public static final swipe_chip_cards_popup_button:I = 0x7f1218e4

.field public static final swipe_chip_cards_popup_liability_warning:I = 0x7f1218e6

.field public static final swipe_chip_cards_toggle_text:I = 0x7f1218eb

.field public static final take_photo:I = 0x7f121911

.field public static final tap_to_set_label:I = 0x7f12191d

.field public static final tax_basis_help_url:I = 0x7f12192e

.field public static final tax_inclusion_type_excluded:I = 0x7f121944

.field public static final tax_inclusion_type_included:I = 0x7f121945

.field public static final tax_list_help:I = 0x7f121947

.field public static final tax_not_modifiable_help:I = 0x7f12194a

.field public static final tax_percentage_row_name:I = 0x7f12194d

.field public static final tender_adjustment:I = 0x7f121950

.field public static final tender_adjustment_uppercase:I = 0x7f121951

.field public static final tender_amount:I = 0x7f121952

.field public static final tender_unknown:I = 0x7f121953

.field public static final tender_unknown_uppercase:I = 0x7f121954

.field public static final ticket_template_delete_content_description:I = 0x7f121970

.field public static final tip_amount:I = 0x7f1219af

.field public static final tip_amount_first:I = 0x7f1219b0

.field public static final tip_amount_second:I = 0x7f1219b1

.field public static final tip_amount_third:I = 0x7f1219b2

.field public static final tip_hint_collect:I = 0x7f1219b4

.field public static final tip_hint_custom:I = 0x7f1219b5

.field public static final tip_hint_separate_screen:I = 0x7f1219b6

.field public static final tip_hint_smart:I = 0x7f1219b7

.field public static final tip_manual_label:I = 0x7f1219b8

.field public static final tip_manual_label_short:I = 0x7f1219b9

.field public static final tip_on:I = 0x7f1219bf

.field public static final tip_post_tax_label:I = 0x7f1219c1

.field public static final tip_pre_tax_label:I = 0x7f1219c2

.field public static final tip_row_format:I = 0x7f1219c3

.field public static final tip_separate_screen_label:I = 0x7f1219c4

.field public static final tip_separate_screen_label_short:I = 0x7f1219c5

.field public static final tip_smart_label:I = 0x7f1219ca

.field public static final tips_collect_label:I = 0x7f1219cb

.field public static final tips_title:I = 0x7f1219cc

.field public static final titlecase_items:I = 0x7f1219dd

.field public static final titlecase_items_appointments:I = 0x7f1219de

.field public static final titlecase_register:I = 0x7f1219e0

.field public static final transactions_history:I = 0x7f1219f6

.field public static final transactions_url:I = 0x7f1219f8

.field public static final type:I = 0x7f121add

.field public static final unable_to_load_image:I = 0x7f121ae0

.field public static final update_square_register_message:I = 0x7f121af4

.field public static final update_square_vertical:I = 0x7f121af5

.field public static final uppercase_card:I = 0x7f121b0a

.field public static final uppercase_choose_label_color:I = 0x7f121b1c

.field public static final uppercase_custom_percentage_amounts:I = 0x7f121b23

.field public static final uppercase_gift_card:I = 0x7f121b2c

.field public static final uppercase_header_basic_info:I = 0x7f121b32

.field public static final uppercase_header_contact_information:I = 0x7f121b36

.field public static final uppercase_header_profile_image:I = 0x7f121b3d

.field public static final uppercase_items_assigned_to_category:I = 0x7f121b48

.field public static final uppercase_keypad:I = 0x7f121b49

.field public static final uppercase_learn_more:I = 0x7f121b4a

.field public static final uppercase_library:I = 0x7f121b4c

.field public static final uppercase_modifier_sets:I = 0x7f121b50

.field public static final uppercase_options:I = 0x7f121b57

.field public static final uppercase_per_transaction_limit:I = 0x7f121b63

.field public static final uppercase_photo_edit_label:I = 0x7f121b64

.field public static final uppercase_taxes:I = 0x7f121b89

.field public static final uppercase_ticket_groups:I = 0x7f121b8a

.field public static final value:I = 0x7f121b92

.field public static final verified:I = 0x7f121b9c

.field public static final weekend_balance:I = 0x7f121bd7

.field public static final weekend_balance_hint:I = 0x7f121bd8

.field public static final yes:I = 0x7f121bf0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
