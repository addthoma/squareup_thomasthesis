.class public final Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "LookupRewardByCodeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLookupRewardByCodeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LookupRewardByCodeCoordinator.kt\ncom/squareup/redeemrewards/LookupRewardByCodeCoordinator\n*L\n1#1,47:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;Lcom/squareup/settings/server/Features;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final runner:Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;->runner:Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;)Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;->runner:Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 25
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 26
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/redeemrewards/R$string;->coupon_redeem_rewards:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 27
    new-instance v0, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator$attach$1;-><init>(Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 29
    sget v0, Lcom/squareup/redeemrewards/R$id;->crm_reward_code_digits:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/DigitInputView;

    .line 31
    iget-object v1, p0, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ALPHANUMERIC_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/widgets/DigitInputView;->setAlphaNumeric()V

    .line 33
    sget v1, Lcom/squareup/redeemrewards/R$id;->crm_reward_code_description:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketTextView;

    .line 34
    sget v2, Lcom/squareup/redeemrewards/R$string;->coupon_search_by_code_hint_alphanumeric:I

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 37
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/DigitInputView;->digits()Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 38
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v1

    .line 39
    new-instance v2, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator$attach$2;

    invoke-direct {v2, v0}, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator$attach$2;-><init>(Lcom/squareup/widgets/DigitInputView;)V

    check-cast v2, Lio/reactivex/functions/Predicate;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "rewardCodeInput.digits()\u2026ardCodeInput.digitCount }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v2, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator$attach$3;

    invoke-direct {v2, p0}, Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator$attach$3;-><init>(Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 42
    invoke-virtual {v0}, Lcom/squareup/widgets/DigitInputView;->getDigits()Ljava/lang/String;

    move-result-object p1

    const-string v1, "rewardCodeInput.digits"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 43
    invoke-virtual {v0}, Lcom/squareup/widgets/DigitInputView;->showKeyboard()V

    :cond_2
    return-void
.end method
