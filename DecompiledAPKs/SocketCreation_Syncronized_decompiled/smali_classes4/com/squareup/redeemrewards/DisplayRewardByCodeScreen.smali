.class public final Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;
.super Lcom/squareup/redeemrewards/InRedeemRewardScope;
.source "DisplayRewardByCodeScreen.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;,
        Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u00162\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u0016\u0017B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0010H\u0016R\u0014\u0010\u0008\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\n\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;",
        "Lcom/squareup/redeemrewards/InRedeemRewardScope;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/MaybePersistent;",
        "redeemRewardsScope",
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V",
        "isPersistent",
        "",
        "()Z",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "Runner",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;->Companion:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion;

    .line 68
    sget-object v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026ava.classLoader)!!)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V
    .locals 1

    const-string v0, "redeemRewardsScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/InRedeemRewardScope;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-super {p0, p1, p2}, Lcom/squareup/redeemrewards/InRedeemRewardScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;->getRedeemRewardsScope$redeem_rewards_release()Lcom/squareup/redeemrewards/RedeemRewardsScope;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    invoke-interface {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;->displayRewardByCodeCoordinator()Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/redeemrewards/R$layout;->crm_display_reward_by_code:I

    return v0
.end method
