.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;->invoke(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rewardRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

.field final synthetic this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;->$rewardRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;

    .line 167
    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;

    iget-object v1, v1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5;->$this_toViewRewardsScreen:Lcom/squareup/redeemrewards/ViewRewardsProps;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getLoyaltySectionInfo()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->getCurrentPoints()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;->$rewardRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;->getPoints()I

    move-result v2

    add-int/2addr v1, v2

    .line 168
    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$5$1;->$rewardRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 166
    invoke-direct {v0, v1, v2}, Lcom/squareup/redeemrewards/ViewRewardsState$ReturningLoyaltyTier;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
