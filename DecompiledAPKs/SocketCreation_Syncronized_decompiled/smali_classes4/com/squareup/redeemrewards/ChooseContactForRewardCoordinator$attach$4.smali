.class final Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$4;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseContactForRewardCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/String;",
        "+",
        "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\"\u0010\u0002\u001a\u001e\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$4;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$4;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;

    .line 147
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$4;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {v1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getContactLoader$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/crm/RolodexContactLoader;

    move-result-object v1

    new-instance v2, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    .line 149
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getOnSearchTermChanged()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    const-string v1, "searchTerm"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
