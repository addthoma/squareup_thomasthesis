.class public final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lcom/squareup/redeemrewards/RedeemRewardWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/redeemrewards/RedeemRewardProps;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRedeemRewardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRedeemRewardWorkflow.kt\ncom/squareup/redeemrewards/RealRedeemRewardWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 7 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 8 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,469:1\n214#1:482\n214#1:488\n214#1:498\n32#2,12:470\n149#3,5:483\n149#3,5:489\n149#3,5:499\n149#3,5:504\n41#4:494\n56#4,2:495\n85#4:509\n276#5:497\n276#5:511\n240#6:510\n501#7:512\n486#7,6:513\n1176#8,2:519\n1190#8,4:521\n1265#8,12:525\n2073#8,5:537\n704#8:542\n777#8,2:543\n1360#8:545\n1429#8,2:546\n1360#8:548\n1429#8,3:549\n1431#8:552\n*E\n*S KotlinDebug\n*F\n+ 1 RealRedeemRewardWorkflow.kt\ncom/squareup/redeemrewards/RealRedeemRewardWorkflow\n*L\n110#1:482\n156#1:488\n208#1:498\n85#1,12:470\n110#1,5:483\n156#1,5:489\n208#1,5:499\n214#1,5:504\n160#1:494\n160#1,2:495\n233#1:509\n160#1:497\n233#1:511\n233#1:510\n270#1:512\n270#1,6:513\n302#1,2:519\n302#1,4:521\n320#1,12:525\n321#1,5:537\n326#1:542\n326#1,2:543\n327#1:545\n327#1,2:546\n327#1:548\n327#1,3:549\n327#1:552\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0005\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002BO\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0002\u0010\u001dJ\u001c\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001f2\u0006\u0010#\u001a\u00020$H\u0002J\u001c\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001f2\u0006\u0010&\u001a\u00020\'H\u0002J\u0018\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0002J\u001a\u0010.\u001a\u00020\u00042\u0006\u0010/\u001a\u00020\u00032\u0008\u00100\u001a\u0004\u0018\u000101H\u0016J&\u00102\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fj\u0002`4032\u0006\u00105\u001a\u000206H\u0002J$\u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001f2\u0006\u0010&\u001a\u00020\'2\u0006\u00108\u001a\u00020\'H\u0002J\u001a\u00109\u001a\u0004\u0018\u00010:2\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\'H\u0002JR\u0010>\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010/\u001a\u00020\u00032\u0006\u00105\u001a\u00020\u00042\u0012\u0010?\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050@H\u0016J\u0012\u0010A\u001a\u0004\u0018\u00010B2\u0006\u0010C\u001a\u00020DH\u0002J\u0010\u0010E\u001a\u0002012\u0006\u00105\u001a\u00020\u0004H\u0016J@\u0010F\u001a$\u0012\u0004\u0012\u00020G\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020G`\n\"\n\u0008\u0000\u0010H\u0018\u0001*\u00020I*\u0002HHH\u0082\u0008\u00a2\u0006\u0002\u0010JJ\u000c\u0010K\u001a\u00020L*\u00020+H\u0002J*\u0010M\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fj\u0002`40N*\u0002062\u0006\u0010*\u001a\u00020+H\u0002J$\u0010O\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fj\u0002`4*\u00020P2\u0006\u0010Q\u001a\u00020RH\u0002J \u0010S\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001f*\u00020T2\u0006\u0010U\u001a\u00020\'H\u0002J\u000c\u0010V\u001a\u00020\'*\u00020+H\u0002J\u001e\u0010W\u001a\u000e\u0012\u0004\u0012\u00020D\u0012\u0004\u0012\u00020L0\u0006*\u0008\u0012\u0004\u0012\u00020D0XH\u0002J%\u0010Y\u001a\u00020)*\u00020)2\u0006\u0010*\u001a\u00020+2\n\u0008\u0002\u0010Z\u001a\u0004\u0018\u00010<H\u0002\u00a2\u0006\u0002\u0010[J\u0018\u0010\\\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001f*\u00020PH\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006]"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/redeemrewards/RedeemRewardProps;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "redeemRewardByCodeWorkflow",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;",
        "viewRewardsWorkflow",
        "Lcom/squareup/redeemrewards/ViewRewardsWorkflow;",
        "addEligibleItemForCouponWorkflow",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;",
        "res",
        "Lcom/squareup/util/Res;",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "holdsCustomer",
        "Lcom/squareup/payment/Transaction;",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "loyalty",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;Lcom/squareup/util/Res;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/Transaction;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/LoyaltySettings;)V",
        "backToChooseContactAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "closeAction",
        "useRewardCodeAction",
        "addingEligibleItemFromCodeAction",
        "newState",
        "Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;",
        "contactChosenAction",
        "contactToken",
        "",
        "createInitialViewRewardsProps",
        "Lcom/squareup/redeemrewards/ViewRewardsProps;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "gotStatus",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "lookupContact",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/redeemrewards/Action;",
        "state",
        "Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;",
        "lookupContactFailedAction",
        "errorMessage",
        "loyaltySectionInfoWithHoldsCoupons",
        "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
        "currentPoints",
        "",
        "phoneToken",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "shouldLaunchAddEligibleWorkflow",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "snapshotState",
        "asCard",
        "Lcom/squareup/container/PosLayering;",
        "ScreenT",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;",
        "isAddedToSale",
        "",
        "loyaltyGetStatusForContact",
        "Lio/reactivex/Single;",
        "processViewRewardsOutput",
        "Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;",
        "output",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "searchTermChangedAction",
        "Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;",
        "newSearchTerm",
        "toDisplayName",
        "toMapWithHoldsCoupons",
        "",
        "withLatestHoldsCoupons",
        "updatedCurrentPoints",
        "(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;)Lcom/squareup/redeemrewards/ViewRewardsProps;",
        "withLatestHoldsCouponsAction",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addEligibleItemForCouponWorkflow:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;

.field private final backToChooseContactAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final closeAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field private final holdsCustomer:Lcom/squareup/payment/Transaction;

.field private final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final redeemRewardByCodeWorkflow:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final useRewardCodeAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final viewRewardsWorkflow:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;Lcom/squareup/util/Res;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/Transaction;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "redeemRewardByCodeWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewRewardsWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addEligibleItemForCouponWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holdsCoupons"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holdsCustomer"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodex"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyalty"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->redeemRewardByCodeWorkflow:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->viewRewardsWorkflow:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->addEligibleItemForCouponWorkflow:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;

    iput-object p4, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iput-object p6, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCustomer:Lcom/squareup/payment/Transaction;

    iput-object p7, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iput-object p8, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iput-object p9, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 350
    sget-object p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$closeAction$1;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$closeAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x1

    const/4 p3, 0x0

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->closeAction:Lcom/squareup/workflow/WorkflowAction;

    .line 352
    sget-object p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$backToChooseContactAction$1;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$backToChooseContactAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->backToChooseContactAction:Lcom/squareup/workflow/WorkflowAction;

    .line 362
    sget-object p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$useRewardCodeAction$1;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$useRewardCodeAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->useRewardCodeAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$addingEligibleItemFromCodeAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->addingEligibleItemFromCodeAction(Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$contactChosenAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->contactChosenAction(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createInitialViewRewardsProps(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->createInitialViewRewardsProps(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getBackToChooseContactAction$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->backToChooseContactAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getCloseAction$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->closeAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getHoldsCoupons$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/checkout/HoldsCoupons;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-object p0
.end method

.method public static final synthetic access$getHoldsCustomer$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCustomer:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/util/Res;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getUseRewardCodeAction$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->useRewardCodeAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$lookupContactFailedAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->lookupContactFailedAction(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$loyaltyGetStatusForContact(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltyGetStatusForContact(Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$processViewRewardsOutput(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->processViewRewardsOutput(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$searchTermChangedAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->searchTermChangedAction(Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$shouldLaunchAddEligibleWorkflow(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->shouldLaunchAddEligibleWorkflow(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$withLatestHoldsCoupons(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->withLatestHoldsCoupons(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$withLatestHoldsCouponsAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->withLatestHoldsCouponsAction(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final addingEligibleItemFromCodeAction(Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    .line 393
    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$addingEligibleItemFromCodeAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$addingEligibleItemFromCodeAction$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final synthetic asCard(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(TScreenT;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 505
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    const/4 v1, 0x4

    const-string v2, "ScreenT"

    .line 506
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 507
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 505
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 214
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final contactChosenAction(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    .line 367
    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$contactChosenAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$contactChosenAction$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final createInitialViewRewardsProps(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 6

    .line 262
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->isAddedToSale(Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result v0

    .line 263
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->toDisplayName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p1

    .line 264
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;->getCoupons()Ljava/util/List;

    move-result-object v1

    .line 265
    invoke-direct {p0, v1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->toMapWithHoldsCoupons(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    .line 512
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 513
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 514
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 271
    iget-object v4, v4, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    sget-object v5, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    if-eq v4, v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_0

    .line 515
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 273
    :cond_3
    instance-of v1, p2, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    const/4 v3, 0x0

    if-nez v1, :cond_4

    move-object v1, v3

    goto :goto_3

    :cond_4
    move-object v1, p2

    :goto_3
    check-cast v1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz v1, :cond_5

    .line 275
    check-cast p2, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result v1

    .line 276
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getPhoneToken()Ljava/lang/String;

    move-result-object p2

    .line 274
    invoke-direct {p0, v1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltySectionInfoWithHoldsCoupons(ILjava/lang/String;)Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object v3

    .line 261
    :cond_5
    new-instance p2, Lcom/squareup/redeemrewards/ViewRewardsProps;

    invoke-direct {p2, v0, p1, v2, v3}, Lcom/squareup/redeemrewards/ViewRewardsProps;-><init>(ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)V

    return-object p2
.end method

.method private final isAddedToSale(Lcom/squareup/protos/client/rolodex/Contact;)Z
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCustomer:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerId()Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final lookupContact(Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;>;"
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;->getContactToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodex.getContact(state\u2026ct)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 509
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 510
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 511
    const-class v0, Lcom/squareup/workflow/WorkflowAction;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/redeemrewards/RedeemRewardOutput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final lookupContactFailedAction(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    .line 378
    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContactFailedAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContactFailedAction$1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final loyaltyGetStatusForContact(Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;>;"
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {v0, p2}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object v0

    .line 240
    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loyalty.getLoyaltyStatus\u2026  )\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final loyaltySectionInfoWithHoldsCoupons(ILjava/lang/String;)Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;
    .locals 13

    .line 310
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->rewardTiers()Ljava/util/List;

    move-result-object v0

    .line 312
    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->canRedeemPoints()Z

    move-result v1

    if-eqz v1, :cond_d

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_d

    .line 315
    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltySectionInfoWithHoldsCoupons$1;

    invoke-direct {v1, v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltySectionInfoWithHoldsCoupons$1;-><init>(Ljava/util/List;)V

    .line 319
    check-cast v0, Ljava/lang/Iterable;

    .line 525
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 532
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 533
    check-cast v6, Lcom/squareup/server/account/protos/RewardTier;

    .line 320
    iget-object v7, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object v6, v6, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-nez v6, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-interface {v7, v6}, Lcom/squareup/checkout/HoldsCoupons;->getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 534
    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_2

    .line 536
    :cond_3
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 538
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v5, 0x0

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 539
    check-cast v6, Lcom/squareup/checkout/Discount;

    const-string v7, "it"

    .line 321
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltySectionInfoWithHoldsCoupons$1;->invoke(Lcom/squareup/checkout/Discount;)I

    move-result v6

    add-int/2addr v5, v6

    goto :goto_3

    :cond_4
    add-int/2addr v5, p1

    .line 542
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 543
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/squareup/server/account/protos/RewardTier;

    .line 326
    iget-object v6, v6, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    int-to-long v8, v5

    cmp-long v10, v6, v8

    if-gtz v10, :cond_6

    const/4 v6, 0x1

    goto :goto_5

    :cond_6
    const/4 v6, 0x0

    :goto_5
    if-eqz v6, :cond_5

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 544
    :cond_7
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 545
    new-instance v0, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 546
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 547
    check-cast v6, Lcom/squareup/server/account/protos/RewardTier;

    const-string v7, "tier"

    .line 329
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    iget-object v7, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    .line 331
    iget-object v8, v6, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-nez v8, :cond_8

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 330
    :cond_8
    invoke-interface {v7, v8}, Lcom/squareup/checkout/HoldsCoupons;->getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    const-string v8, "holdsCoupons.getAllAdded\u2026token!!\n                )"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Ljava/lang/Iterable;

    .line 548
    new-instance v8, Ljava/util/ArrayList;

    invoke-static {v7, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v8, Ljava/util/Collection;

    .line 549
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 550
    check-cast v9, Lcom/squareup/checkout/Discount;

    .line 332
    invoke-virtual {v9}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_9

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_9
    invoke-interface {v8, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 551
    :cond_a
    check-cast v8, Ljava/util/List;

    .line 333
    iget-object v7, v6, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    int-to-long v11, p1

    cmp-long v7, v9, v11

    if-gtz v7, :cond_b

    const/4 v7, 0x1

    goto :goto_8

    :cond_b
    const/4 v7, 0x0

    .line 328
    :goto_8
    new-instance v9, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;

    invoke-direct {v9, v6, v8, v7}, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;-><init>(Lcom/squareup/server/account/protos/RewardTier;Ljava/util/List;Z)V

    .line 334
    invoke-interface {v0, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 552
    :cond_c
    check-cast v0, Ljava/util/List;

    .line 337
    new-instance v1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    invoke-direct {v1, p1, v5, p2, v0}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;-><init>(IILjava/lang/String;Ljava/util/List;)V

    goto :goto_9

    :cond_d
    const/4 v1, 0x0

    :goto_9
    return-object v1
.end method

.method private final processViewRewardsOutput(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    .line 410
    instance-of v0, p2, Lcom/squareup/redeemrewards/ViewRewardsOutput$ApplyCoupon;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 411
    move-object v0, p2

    check-cast v0, Lcom/squareup/redeemrewards/ViewRewardsOutput$ApplyCoupon;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/ViewRewardsOutput$ApplyCoupon;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->shouldLaunchAddEligibleWorkflow(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v3

    const/4 v5, 0x0

    .line 413
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->getViewRewardsProps()Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object v4

    .line 414
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v6

    .line 415
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/ViewRewardsOutput$ApplyCoupon;->getMaybeUpdatedCurrentPoints()Ljava/lang/Integer;

    move-result-object v0

    .line 413
    invoke-direct {p0, v4, v6, v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->withLatestHoldsCoupons(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/4 v9, 0x0

    move-object v4, p1

    .line 412
    invoke-static/range {v4 .. v9}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->copy$default(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/redeemrewards/ViewRewardsProps;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    move-result-object v0

    if-eqz v3, :cond_0

    .line 421
    new-instance p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$1;

    invoke-direct {p1, v0, p2, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 430
    :cond_0
    new-instance v3, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;

    invoke-direct {v3, p0, p1, p2, v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 441
    :cond_1
    instance-of v0, p2, Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$3;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 454
    :cond_2
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsOutput$BackToChooseContact;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsOutput$BackToChooseContact;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->backToChooseContactAction:Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 456
    :cond_3
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsOutput$Close;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsOutput$Close;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->closeAction:Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 458
    :cond_4
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsOutput$AddCustomerAndClose;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsOutput$AddCustomerAndClose;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance p2, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$4;

    invoke-direct {p2, p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$4;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p2, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 463
    :cond_5
    sget-object p1, Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCustomerAndClose;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCustomerAndClose;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    new-instance p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$5;

    invoke-direct {p1, p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$5;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final searchTermChangedAction(Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    .line 357
    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$searchTermChangedAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$searchTermChangedAction$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final shouldLaunchAddEligibleWorkflow(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 8

    .line 402
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->hasQualifyingItem(Lcom/squareup/protos/client/coupons/Coupon;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 403
    invoke-static {p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt;->toAddEligibleItemForCouponProps(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x3

    const/4 v7, 0x0

    .line 404
    invoke-static/range {v2 .. v7}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->copy$default(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;ZILjava/lang/Object;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method private final toDisplayName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_contact_default_display_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 346
    invoke-static {p1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final toMapWithHoldsCoupons(Ljava/util/List;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 302
    check-cast p1, Ljava/lang/Iterable;

    .line 519
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 521
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 522
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    move-object v3, v1

    check-cast v3, Lcom/squareup/protos/client/coupons/Coupon;

    .line 303
    iget-object v4, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object v3, v3, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {v4, v3}, Lcom/squareup/checkout/HoldsCoupons;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 524
    :cond_0
    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method private final withLatestHoldsCoupons(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 7

    .line 292
    invoke-direct {p0, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->isAddedToSale(Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result v1

    .line 293
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getCoupons()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->toMapWithHoldsCoupons(Ljava/util/List;)Ljava/util/Map;

    move-result-object v3

    .line 294
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getLoyaltySectionInfo()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object p2

    if-eqz p2, :cond_1

    if-eqz p3, :cond_0

    .line 296
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getLoyaltySectionInfo()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->getCurrentPoints()I

    move-result p2

    .line 297
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsProps;->getLoyaltySectionInfo()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->getPhoneToken()Ljava/lang/String;

    move-result-object p3

    .line 295
    invoke-direct {p0, p2, p3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltySectionInfoWithHoldsCoupons(ILjava/lang/String;)Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object p2

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    move-object v4, p2

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v2, 0x0

    move-object v0, p1

    .line 291
    invoke-static/range {v0 .. v6}, Lcom/squareup/redeemrewards/ViewRewardsProps;->copy$default(Lcom/squareup/redeemrewards/ViewRewardsProps;ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;ILjava/lang/Object;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object p1

    return-object p1
.end method

.method static synthetic withLatestHoldsCoupons$default(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 290
    check-cast p3, Ljava/lang/Integer;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->withLatestHoldsCoupons(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/Integer;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object p0

    return-object p0
.end method

.method private final withLatestHoldsCouponsAction(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    .line 387
    new-instance v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$withLatestHoldsCouponsAction$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$withLatestHoldsCouponsAction$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/RedeemRewardState;
    .locals 5

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, ""

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 470
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    const/4 v3, 0x0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p2, :cond_3

    .line 475
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v4, "Parcel.obtain()"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 476
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 477
    array-length v4, p2

    invoke-virtual {v2, p2, v3, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 478
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 479
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 480
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v1

    .line 481
    :goto_2
    check-cast p2, Lcom/squareup/redeemrewards/RedeemRewardState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 86
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardProps;->getContactTokenAddedToSale()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    invoke-direct {v1, p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object p2, v1

    check-cast p2, Lcom/squareup/redeemrewards/RedeemRewardState;

    :goto_3
    if-eqz p2, :cond_6

    goto :goto_4

    .line 87
    :cond_6
    new-instance p1, Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;-><init>(Ljava/lang/String;)V

    move-object p2, p1

    check-cast p2, Lcom/squareup/redeemrewards/RedeemRewardState;

    :goto_4
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->initialState(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/RedeemRewardState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardProps;

    check-cast p2, Lcom/squareup/redeemrewards/RedeemRewardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->render(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/redeemrewards/RedeemRewardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/redeemrewards/RedeemRewardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardProps;",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "-",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v9, p3

    const-string v3, "props"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v3

    .line 98
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;

    const-string v5, ""

    if-eqz v4, :cond_0

    .line 100
    new-instance v1, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;

    const/4 v9, 0x0

    .line 102
    iget-object v4, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/redeemrewards/impl/R$string;->choose_contact_for_reward_title:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 103
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/redeemrewards/RedeemRewardState;->getSearchTerm()Ljava/lang/String;

    move-result-object v8

    .line 104
    new-instance v4, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$1;

    invoke-direct {v4, v0, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v10, v4

    check-cast v10, Lkotlin/jvm/functions/Function0;

    .line 105
    new-instance v4, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;

    invoke-direct {v4, v0, v3, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$2;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/redeemrewards/RedeemRewardState;)V

    move-object v11, v4

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 108
    new-instance v2, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$3;

    invoke-direct {v2, v0, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$3;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v12, v2

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 109
    new-instance v2, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$4;

    invoke-direct {v2, v0, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$4;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v13, v2

    check-cast v13, Lkotlin/jvm/functions/Function0;

    move-object v6, v1

    .line 100
    invoke-direct/range {v6 .. v13}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 484
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 485
    const-class v3, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 486
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 484
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 482
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 113
    :cond_0
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$UsingRewardCode;

    if-eqz v4, :cond_1

    .line 114
    iget-object v1, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->redeemRewardByCodeWorkflow:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    sget-object v3, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v4, 0x0

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;

    invoke-direct {v1, v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object/from16 v1, p3

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    return-object v1

    .line 139
    :cond_1
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    if-eqz v4, :cond_2

    .line 140
    move-object v4, v2

    check-cast v4, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    invoke-direct {v0, v4}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->lookupContact(Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;)Lcom/squareup/workflow/Worker;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/redeemrewards/RedeemRewardState;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$6;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$6;

    check-cast v7, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v4, v6, v7}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 142
    new-instance v4, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;

    const/4 v11, 0x1

    .line 144
    iget-object v6, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/redeemrewards/impl/R$string;->choose_contact_for_reward_title:I

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 145
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/redeemrewards/RedeemRewardState;->getSearchTerm()Ljava/lang/String;

    move-result-object v10

    .line 146
    new-instance v2, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$7;

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$7;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/workflow/Sink;)V

    move-object v12, v2

    check-cast v12, Lkotlin/jvm/functions/Function0;

    .line 153
    sget-object v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$8;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$8;

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 154
    sget-object v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$9;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$9;

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 155
    sget-object v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$10;->INSTANCE:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$10;

    move-object v15, v1

    check-cast v15, Lkotlin/jvm/functions/Function0;

    move-object v8, v4

    .line 142
    invoke-direct/range {v8 .. v15}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 490
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 491
    const-class v2, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 492
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 490
    invoke-direct {v1, v2, v4, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 488
    sget-object v2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 159
    :cond_2
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    if-eqz v4, :cond_4

    .line 160
    iget-object v1, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v1}, Lcom/squareup/checkout/HoldsCoupons;->discountsChanged()Lio/reactivex/Observable;

    move-result-object v1

    const-string v3, "holdsCoupons.discountsChanged()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 494
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    const-string v3, "this.toFlowable(BUFFER)"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_3

    .line 496
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 497
    const-class v3, Lkotlin/Unit;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 160
    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$11;

    invoke-direct {v1, v0, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$11;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object/from16 v3, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 164
    iget-object v1, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->viewRewardsWorkflow:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    move-object v1, v2

    check-cast v1, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->getViewRewardsProps()Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object v4

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;

    invoke-direct {v1, v0, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    move-object/from16 v1, p3

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v8

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    return-object v1

    .line 496
    :cond_3
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 169
    :cond_4
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;

    if-eqz v4, :cond_5

    .line 170
    iget-object v1, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->addEligibleItemForCouponWorkflow:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    move-object v1, v2

    check-cast v1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;->getProps()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$13;

    invoke-direct {v1, v0, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$13;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v1, p3

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v8

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    return-object v1

    .line 182
    :cond_5
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    if-eqz v4, :cond_6

    .line 183
    iget-object v1, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->addEligibleItemForCouponWorkflow:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    move-object v1, v2

    check-cast v1, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->getProps()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;

    invoke-direct {v1, v0, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v1, p3

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v8

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    return-object v1

    .line 196
    :cond_6
    instance-of v4, v2, Lcom/squareup/redeemrewards/RedeemRewardState$ErrorLookingUpContact;

    if-eqz v4, :cond_8

    .line 197
    new-instance v4, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;

    .line 198
    iget-object v6, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/redeemrewards/impl/R$string;->choose_contact_for_reward_title:I

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 199
    check-cast v2, Lcom/squareup/redeemrewards/RedeemRewardState$ErrorLookingUpContact;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/RedeemRewardState$ErrorLookingUpContact;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    .line 200
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/redeemrewards/RedeemRewardProps;->getContactTokenAddedToSale()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_7

    const/4 v7, 0x1

    goto :goto_0

    :cond_7
    const/4 v7, 0x0

    .line 201
    :goto_0
    new-instance v8, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$15;

    invoke-direct {v8, v0, v1, v3}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$15;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/workflow/Sink;)V

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 197
    invoke-direct {v4, v6, v2, v7, v8}, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 500
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 501
    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 502
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 500
    invoke-direct {v1, v2, v4, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 498
    sget-object v2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    return-object v1

    :cond_8
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/redeemrewards/RedeemRewardState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardState;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->snapshotState(Lcom/squareup/redeemrewards/RedeemRewardState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
