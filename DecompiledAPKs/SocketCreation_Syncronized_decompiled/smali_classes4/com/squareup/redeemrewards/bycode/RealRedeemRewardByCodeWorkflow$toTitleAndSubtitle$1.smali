.class final Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardByCodeWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->toTitleAndSubtitle(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;Ljava/lang/String;)Lkotlin/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "formatWithCode",
        "",
        "resId",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $code:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->$code:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->invoke(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(I)Ljava/lang/String;
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 178
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->$code:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "code"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 179
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 180
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
