.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,607:1\n1360#2:608\n1429#2,3:609\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1\n*L\n266#1:608\n266#1,3:609\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001ab\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002 \u0004*0\u0012*\u0008\u0001\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "kotlin.jvm.PlatformType",
        "pointsResponse",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1;->call(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;>;"
        }
    .end annotation

    .line 258
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;->getCoupons()Ljava/util/List;

    move-result-object v1

    :cond_1
    if-eqz v1, :cond_4

    .line 260
    move-object p1, v1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    if-eq p1, v0, :cond_2

    goto :goto_1

    .line 266
    :cond_2
    check-cast v1, Ljava/lang/Iterable;

    .line 608
    new-instance p1, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 609
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 610
    check-cast v1, Lcom/squareup/protos/client/coupons/Coupon;

    .line 266
    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {v2, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$hasCouponApplied(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/coupons/Coupon;)Lrx/Observable;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 611
    :cond_3
    check-cast p1, Ljava/util/List;

    .line 267
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1$2;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1$2;

    check-cast v0, Lrx/functions/FuncN;

    .line 265
    invoke-static {p1, v0}, Lrx/Observable;->combineLatest(Ljava/util/List;Lrx/functions/FuncN;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 261
    :cond_4
    :goto_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
