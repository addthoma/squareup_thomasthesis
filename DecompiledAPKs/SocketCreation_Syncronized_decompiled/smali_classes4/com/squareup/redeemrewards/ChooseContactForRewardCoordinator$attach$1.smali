.class public final Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "ChooseContactForRewardCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$1",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "v",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 86
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$1;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const-string p3, "v"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$1;->this$0:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->access$getSearchBox$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    const-string p2, "searchBox.editText"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
