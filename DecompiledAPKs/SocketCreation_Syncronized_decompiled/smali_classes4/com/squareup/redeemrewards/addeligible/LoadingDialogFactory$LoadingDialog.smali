.class public final Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory$LoadingDialog;
.super Lcom/squareup/dialog/GlassDialog;
.source "LoadingDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadingDialog"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory$LoadingDialog;",
        "Lcom/squareup/dialog/GlassDialog;",
        "context",
        "Landroid/content/Context;",
        "screen",
        "Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;",
        "(Landroid/content/Context;Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 31
    sget v0, Lcom/squareup/redeemrewards/addeligible/impl/R$layout;->loading_dialog_content:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory$LoadingDialog;->setContentView(Landroid/view/View;)V

    .line 34
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory$LoadingDialog$1;

    invoke-direct {p1, p2}, Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory$LoadingDialog$1;-><init>(Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;)V

    check-cast p1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/LoadingDialogFactory$LoadingDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method
