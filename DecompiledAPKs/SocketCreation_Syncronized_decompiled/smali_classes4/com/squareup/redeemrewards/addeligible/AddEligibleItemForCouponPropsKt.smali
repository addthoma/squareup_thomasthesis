.class public final Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt;
.super Ljava/lang/Object;
.source "AddEligibleItemForCouponProps.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a\u0016\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004\u001a\u000c\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "toAddEligibleItemForCouponProps",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "Lcom/squareup/checkout/Discount;",
        "includeConfirmDialog",
        "",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toAddEligibleItemForCouponProps(Lcom/squareup/checkout/Discount;Z)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 2

    const-string v0, "$this$toAddEligibleItemForCouponProps"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0}, Lcom/squareup/checkout/Discount;->getMatches()Lcom/squareup/checkout/Discount$Matches;

    move-result-object v0

    .line 60
    instance-of v1, v0, Lcom/squareup/checkout/Discount$Matches$Categories;

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    .line 61
    sget-object v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;->Category:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/checkout/Discount;->getMatches()Lcom/squareup/checkout/Discount$Matches;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/checkout/Discount$Matches$Categories;

    invoke-virtual {p0}, Lcom/squareup/checkout/Discount$Matches$Categories;->getCategoryIds()Ljava/util/List;

    move-result-object p0

    .line 60
    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)V

    goto :goto_0

    .line 62
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.checkout.Discount.Matches.Categories"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 66
    :cond_1
    instance-of v0, v0, Lcom/squareup/checkout/Discount$Matches$Items;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    .line 67
    sget-object v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;->Variation:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/checkout/Discount;->getMatches()Lcom/squareup/checkout/Discount$Matches;

    move-result-object p0

    if-eqz p0, :cond_2

    check-cast p0, Lcom/squareup/checkout/Discount$Matches$Items;

    invoke-virtual {p0}, Lcom/squareup/checkout/Discount$Matches$Items;->getItemVariationIds()Ljava/util/List;

    move-result-object p0

    .line 66
    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)V

    goto :goto_0

    .line 68
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.checkout.Discount.Matches.Items"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static final toAddEligibleItemForCouponProps(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 12

    const-string v0, "$this$toAddEligibleItemForCouponProps"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;-><init>(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 37
    iget-object p0, p0, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->ordinal()I

    move-result p0

    aget p0, v1, p0

    const/4 v1, 0x1

    if-eq p0, v1, :cond_2

    const/4 v1, 0x2

    if-eq p0, v1, :cond_1

    :goto_0
    const/4 p0, 0x0

    goto :goto_1

    .line 43
    :cond_1
    new-instance p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    .line 44
    sget-object v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;->Category:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    .line 45
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;->invoke()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    .line 43
    invoke-direct/range {v0 .. v5}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    .line 38
    :cond_2
    new-instance p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    .line 39
    sget-object v7, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;->Variation:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    .line 40
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;->invoke()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, p0

    .line 38
    invoke-direct/range {v6 .. v11}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_1
    return-object p0
.end method

.method public static synthetic toAddEligibleItemForCouponProps$default(Lcom/squareup/checkout/Discount;ZILjava/lang/Object;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 0

    const/4 p3, 0x1

    and-int/2addr p2, p3

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    .line 57
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt;->toAddEligibleItemForCouponProps(Lcom/squareup/checkout/Discount;Z)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object p0

    return-object p0
.end method
