.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->toViewRewardsScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "rewardRow",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $this_toViewRewardsScreen:Lcom/squareup/redeemrewards/ViewRewardsProps;

.field final synthetic this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;->$this_toViewRewardsScreen:Lcom/squareup/redeemrewards/ViewRewardsProps;

    iput-object p3, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;->invoke(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V
    .locals 4

    const-string v0, "rewardRow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    .line 154
    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    new-instance v2, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$4;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v3, 0x1

    invoke-static {v1, p1, v2, v3, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 153
    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
