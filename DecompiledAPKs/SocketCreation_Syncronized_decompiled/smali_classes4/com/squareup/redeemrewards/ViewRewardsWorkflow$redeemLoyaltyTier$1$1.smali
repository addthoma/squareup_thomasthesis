.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $successOrFailure:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

.field final synthetic this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;->$successOrFailure:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsState$ErrorRedeemingLoyaltyTier;

    .line 251
    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;->$successOrFailure:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;

    iget-object v1, v1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    invoke-static {v1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->access$getPointsTermsFormatter$p(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;)Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object v1

    sget v2, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_failure_redeeming:I

    invoke-virtual {v1, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v1

    .line 250
    :goto_0
    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/ViewRewardsState$ErrorRedeemingLoyaltyTier;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
