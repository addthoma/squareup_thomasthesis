.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1$2;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1;->call(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Ljava/lang/Throwable;",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "error",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rewardCode:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1$2;->$rewardCode:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Throwable;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;
    .locals 3

    .line 163
    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1$2;->$rewardCode:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1$2;->call(Ljava/lang/Throwable;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    move-result-object p1

    return-object p1
.end method
