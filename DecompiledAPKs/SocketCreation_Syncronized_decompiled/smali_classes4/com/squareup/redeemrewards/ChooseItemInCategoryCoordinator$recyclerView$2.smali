.class final Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$recyclerView$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseItemInCategoryCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;-><init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Landroidx/recyclerview/widget/RecyclerView;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$recyclerView$2;->this$0:Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Landroidx/recyclerview/widget/RecyclerView;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$recyclerView$2;->this$0:Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->access$getRootView$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/squareup/redeemrewards/R$id;->items_recyclerview:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$recyclerView$2;->invoke()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    return-object v0
.end method
