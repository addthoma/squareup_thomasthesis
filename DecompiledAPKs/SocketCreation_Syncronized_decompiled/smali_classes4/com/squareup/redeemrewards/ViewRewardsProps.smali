.class public final Lcom/squareup/redeemrewards/ViewRewardsProps;
.super Ljava/lang/Object;
.source "ViewRewardsProps.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ViewRewardsProps$Creator;,
        Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;,
        Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0087\u0008\u0018\u00002\u00020\u0001:\u0002$%B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\u0015\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\nH\u00c6\u0003J?\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u00072\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\nH\u00c6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0019H\u00d6\u0001R\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u000eR\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ViewRewardsProps;",
        "Landroid/os/Parcelable;",
        "isContactAddedToSale",
        "",
        "title",
        "",
        "coupons",
        "",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "loyaltySectionInfo",
        "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
        "(ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)V",
        "getCoupons",
        "()Ljava/util/Map;",
        "()Z",
        "getLoyaltySectionInfo",
        "()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
        "getTitle",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "LoyaltySectionInfo",
        "RewardTierState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final coupons:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isContactAddedToSale:Z

.field private final loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsProps$Creator;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/ViewRewardsProps$Creator;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/ViewRewardsProps;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
            ")V"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coupons"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    iput-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/ViewRewardsProps;ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;ILjava/lang/Object;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-boolean p1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/redeemrewards/ViewRewardsProps;->copy(ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    return-object v0
.end method

.method public final component4()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    return-object v0
.end method

.method public final copy(ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)Lcom/squareup/redeemrewards/ViewRewardsProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
            ")",
            "Lcom/squareup/redeemrewards/ViewRewardsProps;"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coupons"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsProps;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/redeemrewards/ViewRewardsProps;-><init>(ZLjava/lang/String;Ljava/util/Map;Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsProps;

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    iget-boolean v1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    iget-object p1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCoupons()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    return-object v0
.end method

.method public final getLoyaltySectionInfo()Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    return v0
.end method

.method public final isContactAddedToSale()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewRewardsProps(isContactAddedToSale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", coupons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loyaltySectionInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->isContactAddedToSale:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->coupons:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps;->loyaltySectionInfo:Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_1
    return-void
.end method
