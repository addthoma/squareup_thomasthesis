.class public final Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;
.super Ljava/lang/Object;
.source "RealBlockedPrinterLogRunner.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/RealBlockedPrinterLogRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "TimeoutRunner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\u0008\u0081\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;",
        "Ljava/lang/Runnable;",
        "printJob",
        "Lcom/squareup/print/PrintJob;",
        "(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V",
        "getPrintJob$hardware_release",
        "()Lcom/squareup/print/PrintJob;",
        "uniqueJobId",
        "Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;",
        "getUniqueJobId",
        "()Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;",
        "run",
        "",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final printJob:Lcom/squareup/print/PrintJob;

.field final synthetic this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

.field private final uniqueJobId:Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;


# direct methods
.method public constructor <init>(Lcom/squareup/print/RealBlockedPrinterLogRunner;Lcom/squareup/print/PrintJob;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintJob;",
            ")V"
        }
    .end annotation

    const-string v0, "printJob"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->printJob:Lcom/squareup/print/PrintJob;

    .line 63
    iget-object p2, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->printJob:Lcom/squareup/print/PrintJob;

    invoke-virtual {p1, p2}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->getUniqueLogId$hardware_release(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->uniqueJobId:Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    return-void
.end method


# virtual methods
.method public final getPrintJob$hardware_release()Lcom/squareup/print/PrintJob;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->printJob:Lcom/squareup/print/PrintJob;

    return-object v0
.end method

.method public final getUniqueJobId()Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->uniqueJobId:Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    return-object v0
.end method

.method public run()V
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutThrowable;

    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->printJob:Lcom/squareup/print/PrintJob;

    invoke-direct {v0, v1}, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutThrowable;-><init>(Lcom/squareup/print/PrintJob;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Timeout: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->printJob:Lcom/squareup/print/PrintJob;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->this$0:Lcom/squareup/print/RealBlockedPrinterLogRunner;

    invoke-static {v0}, Lcom/squareup/print/RealBlockedPrinterLogRunner;->access$getJobToRunnableMap$p(Lcom/squareup/print/RealBlockedPrinterLogRunner;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/print/RealBlockedPrinterLogRunner$TimeoutRunner;->uniqueJobId:Lcom/squareup/print/RealBlockedPrinterLogRunner$UniqueLogId;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
