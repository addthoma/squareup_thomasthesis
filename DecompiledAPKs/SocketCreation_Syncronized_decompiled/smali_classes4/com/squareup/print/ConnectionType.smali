.class public final enum Lcom/squareup/print/ConnectionType;
.super Ljava/lang/Enum;
.source "ConnectionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/ConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/ConnectionType;

.field public static final enum BT:Lcom/squareup/print/ConnectionType;

.field public static final enum FAKE:Lcom/squareup/print/ConnectionType;

.field public static final enum FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

.field public static final enum INTERNAL:Lcom/squareup/print/ConnectionType;

.field public static final enum TCP:Lcom/squareup/print/ConnectionType;

.field public static final enum USB:Lcom/squareup/print/ConnectionType;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 4
    new-instance v0, Lcom/squareup/print/ConnectionType;

    const/4 v1, 0x0

    const-string v2, "USB"

    invoke-direct {v0, v2, v1, v2}, Lcom/squareup/print/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    .line 5
    new-instance v0, Lcom/squareup/print/ConnectionType;

    const/4 v2, 0x1

    const-string v3, "TCP"

    const-string v4, "Network"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/print/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    .line 6
    new-instance v0, Lcom/squareup/print/ConnectionType;

    const/4 v3, 0x2

    const-string v4, "BT"

    const-string v5, "Bluetooth"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/print/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/print/ConnectionType;->BT:Lcom/squareup/print/ConnectionType;

    .line 7
    new-instance v0, Lcom/squareup/print/ConnectionType;

    const/4 v4, 0x3

    const-string v5, "INTERNAL"

    invoke-direct {v0, v5, v4, v5}, Lcom/squareup/print/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    .line 9
    new-instance v0, Lcom/squareup/print/ConnectionType;

    const/4 v5, 0x4

    const-string v6, "FAKE"

    const-string v7, "Unknown"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/print/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/print/ConnectionType;->FAKE:Lcom/squareup/print/ConnectionType;

    .line 12
    new-instance v0, Lcom/squareup/print/ConnectionType;

    const/4 v6, 0x5

    const-string v7, "FAKE_INTERNAL"

    const-string v8, "Unknown_Internal"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/print/ConnectionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/print/ConnectionType;

    .line 3
    sget-object v7, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/ConnectionType;->BT:Lcom/squareup/print/ConnectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/print/ConnectionType;->FAKE:Lcom/squareup/print/ConnectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/print/ConnectionType;->$VALUES:[Lcom/squareup/print/ConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput-object p3, p0, Lcom/squareup/print/ConnectionType;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/print/ConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/ConnectionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/print/ConnectionType;->$VALUES:[Lcom/squareup/print/ConnectionType;

    invoke-virtual {v0}, [Lcom/squareup/print/ConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/ConnectionType;

    return-object v0
.end method
