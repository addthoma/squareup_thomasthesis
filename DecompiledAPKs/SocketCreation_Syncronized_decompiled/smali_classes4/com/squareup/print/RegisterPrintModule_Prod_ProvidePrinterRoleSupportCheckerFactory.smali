.class public final Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory;
.super Ljava/lang/Object;
.source "RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory$InstanceHolder;->access$000()Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static providePrinterRoleSupportChecker()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/print/RegisterPrintModule$Prod;->providePrinterRoleSupportChecker()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory;->providePrinterRoleSupportChecker()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/print/RegisterPrintModule_Prod_ProvidePrinterRoleSupportCheckerFactory;->get()Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    move-result-object v0

    return-object v0
.end method
