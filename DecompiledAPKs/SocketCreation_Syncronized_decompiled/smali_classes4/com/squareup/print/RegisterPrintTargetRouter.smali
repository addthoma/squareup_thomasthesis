.class public Lcom/squareup/print/RegisterPrintTargetRouter;
.super Ljava/lang/Object;
.source "RegisterPrintTargetRouter.java"

# interfaces
.implements Lcom/squareup/print/PrintTargetRouter;


# instance fields
.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final printerStations:Lcom/squareup/print/PrinterStations;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/HardwarePrinterTracker;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintTargetRouter;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 22
    iput-object p2, p0, Lcom/squareup/print/RegisterPrintTargetRouter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    return-void
.end method

.method private getHardwarePrinterById(Ljava/lang/String;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintTargetRouter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object p1

    if-nez p1, :cond_0

    .line 49
    sget-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_UNAVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    goto :goto_0

    .line 51
    :cond_0
    sget-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_AVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 54
    :goto_0
    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private getHardwarePrinterForTarget(Ljava/lang/String;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintTargetRouter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrinterStations;->getPrinterStationById(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 63
    sget-object p1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_DOES_NOT_EXIST:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 66
    sget-object p1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_HAS_NO_HARDWARE_PRINTER:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    goto :goto_0

    .line 68
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object p1

    .line 69
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintTargetRouter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object v0

    if-nez v0, :cond_2

    .line 71
    sget-object p1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_UNAVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    goto :goto_0

    .line 73
    :cond_2
    sget-object p1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_AVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 76
    :goto_0
    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, p1, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private routeForOldPrintingInfrastructure(Ljava/lang/String;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintTargetRouter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object p1

    if-nez p1, :cond_0

    .line 38
    sget-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_HAS_NO_HARDWARE_PRINTER:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    goto :goto_0

    .line 40
    :cond_0
    sget-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_AVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 42
    :goto_0
    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method


# virtual methods
.method public retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/print/RegisterPrintTargetRouter;->getHardwarePrinterById(Ljava/lang/String;)Lkotlin/Pair;

    move-result-object p1

    return-object p1

    .line 30
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/print/RegisterPrintTargetRouter;->getHardwarePrinterForTarget(Ljava/lang/String;)Lkotlin/Pair;

    move-result-object p1

    return-object p1
.end method
