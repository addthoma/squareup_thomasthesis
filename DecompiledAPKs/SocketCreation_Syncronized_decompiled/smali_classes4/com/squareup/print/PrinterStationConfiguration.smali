.class public Lcom/squareup/print/PrinterStationConfiguration;
.super Ljava/lang/Object;
.source "PrinterStationConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrinterStationConfiguration$Builder;
    }
.end annotation


# static fields
.field public static final NO_PRINTER_SELECTED_ID:Ljava/lang/String;


# instance fields
.field public final autoPrintItemizedReceipts:Z

.field public final disabledCategoryIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final internal:Z

.field public final name:Ljava/lang/String;

.field public final printATicketForEachItem:Z

.field public final printCompactTickets:Z

.field public final printsUncategorizedItems:Z

.field public final selectedHardwarePrinterId:Ljava/lang/String;

.field public final selectedRoles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;ZZZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;ZZZZZ)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/print/PrinterStationConfiguration;->name:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/squareup/print/PrinterStationConfiguration;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/squareup/print/PrinterStationConfiguration;->selectedRoles:Ljava/util/Set;

    .line 28
    iput-object p4, p0, Lcom/squareup/print/PrinterStationConfiguration;->disabledCategoryIds:Ljava/util/Set;

    .line 29
    iput-boolean p5, p0, Lcom/squareup/print/PrinterStationConfiguration;->printsUncategorizedItems:Z

    .line 30
    iput-boolean p6, p0, Lcom/squareup/print/PrinterStationConfiguration;->autoPrintItemizedReceipts:Z

    .line 31
    iput-boolean p7, p0, Lcom/squareup/print/PrinterStationConfiguration;->printATicketForEachItem:Z

    .line 32
    iput-boolean p8, p0, Lcom/squareup/print/PrinterStationConfiguration;->printCompactTickets:Z

    .line 33
    iput-boolean p9, p0, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;ZZZZZLcom/squareup/print/PrinterStationConfiguration$1;)V
    .locals 0

    .line 7
    invoke-direct/range {p0 .. p9}, Lcom/squareup/print/PrinterStationConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;ZZZZZ)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 10

    .line 37
    new-instance v9, Lcom/squareup/print/PrinterStationConfiguration$Builder;

    iget-object v1, p0, Lcom/squareup/print/PrinterStationConfiguration;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/print/PrinterStationConfiguration;->selectedHardwarePrinterId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/print/PrinterStationConfiguration;->selectedRoles:Ljava/util/Set;

    iget-object v4, p0, Lcom/squareup/print/PrinterStationConfiguration;->disabledCategoryIds:Ljava/util/Set;

    iget-boolean v5, p0, Lcom/squareup/print/PrinterStationConfiguration;->printsUncategorizedItems:Z

    iget-boolean v6, p0, Lcom/squareup/print/PrinterStationConfiguration;->autoPrintItemizedReceipts:Z

    iget-boolean v7, p0, Lcom/squareup/print/PrinterStationConfiguration;->printATicketForEachItem:Z

    iget-boolean v8, p0, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/PrinterStationConfiguration$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;ZZZZ)V

    return-object v9
.end method
