.class Lcom/squareup/print/FileThreadPrintQueueExecutor;
.super Ljava/lang/Object;
.source "FileThreadPrintQueueExecutor.java"

# interfaces
.implements Lcom/squareup/print/PrintQueueExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/FileThreadPrintQueueExecutor$Failure;
    }
.end annotation


# instance fields
.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final printJobQueue:Lcom/squareup/print/PrintJobQueue;


# direct methods
.method constructor <init>(Lcom/squareup/print/PrintJobQueue;Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->printJobQueue:Lcom/squareup/print/PrintJobQueue;

    .line 30
    iput-object p2, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 31
    iput-object p3, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private enforceMainThread()V
    .locals 1

    const-string v0, "Cannot interact with tickets from outside of main thread."

    .line 73
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    return-void
.end method

.method private executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/print/PrintCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;",
            "Lcom/squareup/print/PrintCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$vdbZnYtoIpqOKT7K9lELhzo-zXo;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$vdbZnYtoIpqOKT7K9lELhzo-zXo;-><init>(Lcom/squareup/print/FileThreadPrintQueueExecutor;Ljava/util/concurrent/Callable;Lcom/squareup/print/PrintCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static fail(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintCallback;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/print/PrintCallback<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 83
    new-instance v0, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$MYpDxnxvR5esDhVaQYilmxmWOas;

    invoke-direct {v0, p1, p2}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$MYpDxnxvR5esDhVaQYilmxmWOas;-><init>(Lcom/squareup/print/PrintCallback;Ljava/lang/Throwable;)V

    invoke-interface {p0, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$fail$6(Lcom/squareup/print/PrintCallback;Ljava/lang/Throwable;)V
    .locals 1

    .line 83
    new-instance v0, Lcom/squareup/print/FileThreadPrintQueueExecutor$Failure;

    invoke-direct {v0, p1}, Lcom/squareup/print/FileThreadPrintQueueExecutor$Failure;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {p0, v0}, Lcom/squareup/print/PrintCallback;->call(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V

    return-void
.end method

.method static synthetic lambda$null$4(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$succeed$5(Lcom/squareup/print/PrintCallback;Ljava/lang/Object;)V
    .locals 1

    .line 78
    new-instance v0, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$4Dl5GGm6OQ_nUhBSm9xGesV_tFE;

    invoke-direct {v0, p1}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$4Dl5GGm6OQ_nUhBSm9xGesV_tFE;-><init>(Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Lcom/squareup/print/PrintCallback;->call(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V

    return-void
.end method

.method private static succeed(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintCallback;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/print/PrintCallback<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 78
    new-instance v0, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$7BU7cZ2cMMJuF7EIEDb5AN-wZnY;

    invoke-direct {v0, p1, p2}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$7BU7cZ2cMMJuF7EIEDb5AN-wZnY;-><init>(Lcom/squareup/print/PrintCallback;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/print/PrintQueueExecutor$PrintTask<",
            "TT;>;",
            "Lcom/squareup/print/PrintCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Lcom/squareup/print/FileThreadPrintQueueExecutor;->enforceMainThread()V

    const-string v0, "task"

    .line 52
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "callback"

    .line 53
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$IgAv4VyfTxD_TE6wgym-lIaQQr4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$IgAv4VyfTxD_TE6wgym-lIaQQr4;-><init>(Lcom/squareup/print/FileThreadPrintQueueExecutor;Lcom/squareup/print/PrintQueueExecutor$PrintTask;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/print/FileThreadPrintQueueExecutor;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public execute(Lcom/squareup/print/PrintQueueExecutor$PrintWork;)V
    .locals 2

    .line 35
    invoke-direct {p0}, Lcom/squareup/print/FileThreadPrintQueueExecutor;->enforceMainThread()V

    const-string/jumbo v0, "work"

    .line 36
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 37
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$4N5DRaqTLOxMVFaimG_dYBYvQLQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$4N5DRaqTLOxMVFaimG_dYBYvQLQ;-><init>(Lcom/squareup/print/FileThreadPrintQueueExecutor;Lcom/squareup/print/PrintQueueExecutor$PrintWork;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public execute(Lcom/squareup/print/PrintQueueExecutor$PrintWork;Ljava/lang/Runnable;)V
    .locals 2

    .line 41
    invoke-direct {p0}, Lcom/squareup/print/FileThreadPrintQueueExecutor;->enforceMainThread()V

    const-string/jumbo v0, "work"

    .line 42
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "afterWork"

    .line 43
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$HuvR3G_dxc6Znc82f9OQONOs_dE;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/print/-$$Lambda$FileThreadPrintQueueExecutor$HuvR3G_dxc6Znc82f9OQONOs_dE;-><init>(Lcom/squareup/print/FileThreadPrintQueueExecutor;Lcom/squareup/print/PrintQueueExecutor$PrintWork;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$execute$0$FileThreadPrintQueueExecutor(Lcom/squareup/print/PrintQueueExecutor$PrintWork;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->printJobQueue:Lcom/squareup/print/PrintJobQueue;

    invoke-interface {p1, v0}, Lcom/squareup/print/PrintQueueExecutor$PrintWork;->perform(Lcom/squareup/print/PrintJobQueue;)V

    return-void
.end method

.method public synthetic lambda$execute$1$FileThreadPrintQueueExecutor(Lcom/squareup/print/PrintQueueExecutor$PrintWork;Ljava/lang/Runnable;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->printJobQueue:Lcom/squareup/print/PrintJobQueue;

    invoke-interface {p1, v0}, Lcom/squareup/print/PrintQueueExecutor$PrintWork;->perform(Lcom/squareup/print/PrintJobQueue;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$execute$2$FileThreadPrintQueueExecutor(Lcom/squareup/print/PrintQueueExecutor$PrintTask;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->printJobQueue:Lcom/squareup/print/PrintJobQueue;

    invoke-interface {p1, v0}, Lcom/squareup/print/PrintQueueExecutor$PrintTask;->perform(Lcom/squareup/print/PrintJobQueue;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$executeOnFileThread$3$FileThreadPrintQueueExecutor(Ljava/util/concurrent/Callable;Lcom/squareup/print/PrintCallback;)V
    .locals 1

    .line 61
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, p2, p1}, Lcom/squareup/print/FileThreadPrintQueueExecutor;->succeed(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintCallback;Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    const-string v0, "Error executing print task."

    .line 63
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 64
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-static {p1, p2, v0}, Lcom/squareup/print/FileThreadPrintQueueExecutor;->fail(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/PrintCallback;Ljava/lang/Throwable;)V

    return-void
.end method
