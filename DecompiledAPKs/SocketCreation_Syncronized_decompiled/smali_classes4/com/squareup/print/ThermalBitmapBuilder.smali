.class public Lcom/squareup/print/ThermalBitmapBuilder;
.super Ljava/lang/Object;
.source "ThermalBitmapBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;,
        Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;,
        Lcom/squareup/print/ThermalBitmapBuilder$Factory;
    }
.end annotation


# static fields
.field private static final BACKGROUND_COLOR:I = -0x1

.field private static final BARCODE_HEIGHT_PX:I = 0x68

.field private static final BIG_HEADLINE_TEXT_SIZE_PT:I = 0x24

.field private static final BIG_TEXT_SIZE_PT:I = 0xe

.field private static final BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

.field private static final CHECKBOX_RIGHT_MARGIN:I

.field private static final CHECKBOX_THICKNESS:I

.field private static final DEBUG_TEXT_COLOR:I = -0x777778

.field private static final EXTRA_BIG_HEADLINE_TEXT_SIZE_PT:I = 0x30

.field private static final GUTTER_PT:I = 0x14

.field private static final HEADLINE_BLOCK_HEIGHT_PX:I = 0x32

.field private static final HEADLINE_TEXT_SIZE_PT:I = 0x18

.field private static final INCLUDEPAD:Z = false

.field private static final INK_COLOR:I = -0x1000000

.field private static final LARGE_SPACE_HEIGHT_PX:I = 0x23

.field private static final LIGHT_DIVIDER_HEIGHT_PX:I = 0x1

.field private static final LOGO_MAX_HEIGHT_PX:I = 0x96

.field private static final LOGO_MAX_WIDTH_PX:I = 0x12c

.field private static final MARGIN_PT:I = 0x11

.field private static final MEDIUM_DIVIDER_HEIGHT_PX:I = 0x3

.field private static final MEDIUM_SPACE_HEIGHT_PX:I = 0x19

.field private static final MIN_TEXT_SIZE_PT:I = 0x7

.field private static final NARROW_PAPER_THRESHOLD_INCH:F = 2.0f

.field private static final PPI:I = 0x7d

.field private static final QUICKTIP_TITLE_HEIGHT_PX:I = 0x2c

.field private static final REPRINT_BLOCK_HEIGHT_PX:I = 0x50

.field private static final SCALE:F = 1.35f

.field private static final SECTION_HEADER_BLOCK_HEIGHT_PX:I = 0x34

.field private static final SIGN_MARK:C = '\u00d7'

.field private static final SMALL_HEADLINE_TEXT_SIZE_PT:I = 0xf

.field private static final SMALL_SPACE_HEIGHT_PX:I = 0x14

.field private static final SMALL_TEXT_SIZE_PT:I = 0xa

.field private static final SPACINGADD:F = 0.0f

.field private static final SPACINGMULT_BODY:F = 1.15f

.field private static final SPACINGMULT_ONE:F = 1.0f

.field private static final STANDARD_TEXT_SIZE_PT:I = 0xc

.field private static final THIN_DIVIDER_HEIGHT_PX:I = 0x2

.field private static final TICKET_ITEM_GUTTER:I = 0x14

.field private static final TICKET_ITEM_LINE_SPACING:I = 0x8

.field private static final TICKET_QUANTITY_WIDTH_PX:I = 0x24

.field private static final TICKET_TOP_PADDING_IN_PX:I = 0x9c

.field private static final TINY_SPACE_HEIGHT_PX:I = 0xc

.field private static final TRANSACTION_TYPE_BLOCK_HEIGHT_PX:I = 0x34

.field private static final WRITE_LINE_LABEL_PADDING_PX:I = 0x3

.field private static final WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;


# instance fields
.field private final backgroundPaint:Landroid/graphics/Paint;

.field private final bigHeadlinePaint:Landroid/text/TextPaint;

.field private final bigMediumPaint:Landroid/text/TextPaint;

.field private final bigRegularPaint:Landroid/text/TextPaint;

.field private final bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

.field private final bitmaps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final boxContentWidth:I

.field private final contentWidth:I

.field private final diningOptionPaint:Landroid/text/TextPaint;

.field private final extraBigHeadlinePaint:Landroid/text/TextPaint;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final gutter:I

.field private final headlinePaint:Landroid/text/TextPaint;

.field private final heightDebugPaint:Landroid/text/TextPaint;

.field private final horizontalMargin:I

.field private final inkPaint:Landroid/graphics/Paint;

.field private final invertedStandardMediumPaint:Landroid/text/TextPaint;

.field private final mainScheduler:Lrx/Scheduler;

.field private final margin:I

.field private final mediumSpan:Landroid/text/style/CharacterStyle;

.field private final narrowContent:Z

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final quickTipCheckboxSize:I

.field private final quickTipRowTextAvailableWidth:I

.field private final smallDiningOptionPaint:Landroid/text/TextPaint;

.field private final smallHeadlinePaint:Landroid/text/TextPaint;

.field private final smallMediumPaint:Landroid/text/TextPaint;

.field private final smallRegularPaint:Landroid/text/TextPaint;

.field private final standardItalicPaint:Landroid/text/TextPaint;

.field private final standardMediumPaint:Landroid/text/TextPaint;

.field private final standardRegularPaint:Landroid/text/TextPaint;

.field private final thumbor:Lcom/squareup/pollexor/Thumbor;

.field private final ticketItemItalicPaint:Landroid/text/TextPaint;

.field private final ticketItemPaint:Landroid/text/TextPaint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 130
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    .line 186
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget v0, v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    sput v0, Lcom/squareup/print/ThermalBitmapBuilder;->CHECKBOX_RIGHT_MARGIN:I

    .line 187
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    iget v0, v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->sizePx:I

    sput v0, Lcom/squareup/print/ThermalBitmapBuilder;->CHECKBOX_THICKNESS:I

    .line 190
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder;->WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;)V
    .locals 5
    .param p5    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    iput-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->features:Lcom/squareup/settings/server/Features;

    .line 275
    iput-object p3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->picasso:Lcom/squareup/picasso/Picasso;

    .line 276
    iput-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->thumbor:Lcom/squareup/pollexor/Thumbor;

    .line 277
    iput-object p5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mainScheduler:Lrx/Scheduler;

    .line 278
    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    .line 280
    iget p2, p6, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerLine:I

    iget p3, p6, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerInch:I

    div-int/2addr p2, p3

    int-to-float p2, p2

    const/4 p3, 0x0

    const/4 p4, 0x1

    const/high16 p5, 0x40000000    # 2.0f

    cmpg-float p2, p2, p5

    if-gez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 281
    :goto_0
    iput-boolean p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->narrowContent:Z

    const/high16 p2, 0x41880000    # 17.0f

    .line 283
    invoke-direct {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->px(F)I

    move-result p2

    iput p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->margin:I

    const/high16 p2, 0x41a00000    # 20.0f

    .line 284
    invoke-direct {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->px(F)I

    move-result p2

    iput p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    .line 285
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->hasNarrowContent()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    iget p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->margin:I

    :goto_1
    iput p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->horizontalMargin:I

    .line 286
    iget p2, p6, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerLine:I

    iget p5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->horizontalMargin:I

    mul-int/lit8 p5, p5, 0x2

    sub-int/2addr p2, p5

    iput p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    .line 287
    iget p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object p5, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    iget p5, p5, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->sizePx:I

    sget-object p6, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget p6, p6, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    add-int/2addr p5, p6

    mul-int/lit8 p5, p5, 0x2

    sub-int/2addr p2, p5

    iput p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->boxContentWidth:I

    .line 289
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object p2

    .line 290
    sget-object p5, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, p5, p3, p4}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object p3

    .line 291
    sget-object p5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, p5}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object p5

    .line 293
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    .line 294
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v0, 0x41700000    # 15.0f

    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v0

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 295
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v0, -0x1000000

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 296
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 298
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->headlinePaint:Landroid/text/TextPaint;

    .line 299
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->headlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x41c00000    # 24.0f

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 300
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->headlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 301
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->headlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 303
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigHeadlinePaint:Landroid/text/TextPaint;

    .line 304
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x42100000    # 36.0f

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 305
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigHeadlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 306
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigHeadlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 308
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->extraBigHeadlinePaint:Landroid/text/TextPaint;

    .line 309
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->extraBigHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x42400000    # 48.0f

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 310
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->extraBigHeadlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 311
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->extraBigHeadlinePaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 313
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    .line 314
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v2

    invoke-virtual {p6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 315
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 316
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 318
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallRegularPaint:Landroid/text/TextPaint;

    .line 319
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallRegularPaint:Landroid/text/TextPaint;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {p0, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v3

    invoke-virtual {p6, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 320
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 321
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 323
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigRegularPaint:Landroid/text/TextPaint;

    .line 324
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigRegularPaint:Landroid/text/TextPaint;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-direct {p0, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v4

    invoke-virtual {p6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 325
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 326
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 328
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallDiningOptionPaint:Landroid/text/TextPaint;

    .line 329
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallDiningOptionPaint:Landroid/text/TextPaint;

    invoke-direct {p0, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v4

    invoke-virtual {p6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 330
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallDiningOptionPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 331
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallDiningOptionPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 333
    new-instance p6, Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallDiningOptionPaint:Landroid/text/TextPaint;

    invoke-direct {p6, v4}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->diningOptionPaint:Landroid/text/TextPaint;

    .line 334
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->diningOptionPaint:Landroid/text/TextPaint;

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v4

    invoke-virtual {p6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 336
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    .line 337
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 338
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 339
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 341
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    .line 342
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    invoke-direct {p0, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 343
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 344
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 346
    new-instance p6, Landroid/text/TextPaint;

    invoke-direct {p6, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    .line 347
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    invoke-direct {p0, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 348
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 349
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 351
    new-instance p6, Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-direct {p6, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardItalicPaint:Landroid/text/TextPaint;

    .line 352
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardItalicPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 354
    new-instance p6, Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-direct {p6, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->invertedStandardMediumPaint:Landroid/text/TextPaint;

    .line 355
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->invertedStandardMediumPaint:Landroid/text/TextPaint;

    const/4 v1, -0x1

    invoke-virtual {p6, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 356
    iget-object p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->invertedStandardMediumPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 358
    new-instance p5, Landroid/text/TextPaint;

    invoke-direct {p5, p4}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->heightDebugPaint:Landroid/text/TextPaint;

    .line 359
    iget-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->heightDebugPaint:Landroid/text/TextPaint;

    const/high16 p5, 0x40a00000    # 5.0f

    invoke-direct {p0, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result p5

    invoke-virtual {p4, p5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 360
    iget-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->heightDebugPaint:Landroid/text/TextPaint;

    const p5, -0x777778

    invoke-virtual {p4, p5}, Landroid/text/TextPaint;->setColor(I)V

    .line 361
    iget-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->heightDebugPaint:Landroid/text/TextPaint;

    invoke-virtual {p4, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 363
    new-instance p4, Landroid/text/TextPaint;

    iget-object p5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    invoke-direct {p4, p5}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->ticketItemPaint:Landroid/text/TextPaint;

    .line 364
    iget-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->ticketItemPaint:Landroid/text/TextPaint;

    invoke-virtual {p4, p2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 366
    new-instance p2, Landroid/text/TextPaint;

    iget-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    invoke-direct {p2, p4}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->ticketItemItalicPaint:Landroid/text/TextPaint;

    .line 367
    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->ticketItemItalicPaint:Landroid/text/TextPaint;

    invoke-virtual {p2, p3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 369
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->inkPaint:Landroid/graphics/Paint;

    .line 370
    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->inkPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 372
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->backgroundPaint:Landroid/graphics/Paint;

    .line 373
    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 375
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    .line 377
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bitmaps:Ljava/util/List;

    .line 379
    iget-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object p1

    iget p1, p1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    neg-int p1, p1

    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    .line 380
    invoke-virtual {p2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object p2

    iget p2, p2, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    .line 381
    iget p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->boxContentWidth:I

    iget p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sub-int/2addr p1, p2

    sget p2, Lcom/squareup/print/ThermalBitmapBuilder;->CHECKBOX_RIGHT_MARGIN:I

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipRowTextAvailableWidth:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/ThermalBitmapBuilder$1;)V
    .locals 0

    .line 64
    invoke-direct/range {p0 .. p6}, Lcom/squareup/print/ThermalBitmapBuilder;-><init>(Landroid/content/Context;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;)V

    return-void
.end method

.method private append(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1499
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bitmaps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private appendTextCenteredInBlackRectangle(Ljava/lang/CharSequence;I)V
    .locals 5

    .line 1034
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, p2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    const/high16 v1, -0x1000000

    .line 1035
    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1037
    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->invertedStandardMediumPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const v4, 0x3f933333    # 1.15f

    .line 1038
    invoke-static {p1, v1, v4, v2, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1041
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1042
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result v2

    sub-int/2addr p2, v2

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1043
    invoke-virtual {p1, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1045
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private appendTextCenteredInDivider(Ljava/lang/CharSequence;Landroid/text/TextPaint;)V
    .locals 11

    .line 1058
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const v2, 0x3f933333    # 1.15f

    .line 1059
    invoke-static {p1, p2, v2, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    .line 1061
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v1

    .line 1062
    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1063
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1065
    iget v3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    div-int/lit8 v10, v3, 0x2

    .line 1066
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p2, p1, v4, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p1

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p1, p1

    add-int/lit8 p1, p1, 0x28

    .line 1069
    div-int/lit8 p2, v1, 0x2

    div-int/lit8 p1, p1, 0x2

    sub-int v7, v10, p1

    sget-object v8, Lcom/squareup/print/ThermalBitmapBuilder;->WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    const/4 v6, 0x0

    move-object v3, p0

    move-object v4, v9

    move v5, p2

    invoke-direct/range {v3 .. v8}, Lcom/squareup/print/ThermalBitmapBuilder;->drawHorizontalRule(Landroid/graphics/Canvas;IIILcom/squareup/print/ThermalBitmapBuilder$RuleSize;)V

    add-int v6, v10, p1

    .line 1071
    iget v7, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v8, Lcom/squareup/print/ThermalBitmapBuilder;->WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    invoke-direct/range {v3 .. v8}, Lcom/squareup/print/ThermalBitmapBuilder;->drawHorizontalRule(Landroid/graphics/Canvas;IIILcom/squareup/print/ThermalBitmapBuilder$RuleSize;)V

    .line 1074
    invoke-virtual {v0, v9}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1075
    invoke-direct {p0, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private bitmapFromLayout(Landroid/text/Layout;)Landroid/graphics/Bitmap;
    .locals 2

    .line 1258
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1259
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1260
    invoke-virtual {p1, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method private calculateTextVisualPaddingTop(Landroid/text/Layout;)F
    .locals 5

    .line 1383
    invoke-virtual {p1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1386
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 1390
    :cond_0
    invoke-virtual {p1}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 1393
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    const/4 v3, 0x0

    .line 1394
    invoke-virtual {p1, v3, v2}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1397
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 1398
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v0, v3, v4, p1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1400
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 1402
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    sub-int/2addr v1, p1

    int-to-float p1, v1

    sub-float/2addr p1, v0

    return p1
.end method

.method private canFitIntoTwoColumnsOnSingleLine(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;)Z
    .locals 0

    .line 1211
    invoke-static {p1, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->getMaxWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result p1

    .line 1212
    invoke-static {p2, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->getMaxWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result p2

    .line 1213
    iget p3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    add-int/2addr p1, p3

    add-int/2addr p1, p2

    iget p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    if-gt p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private centerBitmapHorizontally(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5

    .line 1552
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    if-ne v0, v1, :cond_0

    return-object p1

    .line 1556
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget-object v2, Lcom/squareup/print/ThermalBitmapBuilder;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1557
    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    const/4 v2, -0x1

    .line 1559
    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1560
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    int-to-float v1, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1561
    invoke-virtual {v2, p1, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-object v0
.end method

.method private centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V
    .locals 2

    .line 1253
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-static {p2, p1, p3, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    const/4 p2, 0x1

    new-array p2, p2, [Landroid/text/Layout;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    .line 1254
    invoke-direct {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignTopBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private static concatenateBitmaps(IIILjava/util/List;)Landroid/graphics/Bitmap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .line 1571
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    .line 1572
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1573
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_0

    :cond_0
    add-int/2addr p2, p1

    add-int/2addr v2, p2

    mul-int/lit8 p2, p0, 0x2

    add-int/2addr v3, p2

    .line 1579
    sget-object p2, Lcom/squareup/print/ThermalBitmapBuilder;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, p2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p2

    const/4 v0, -0x1

    .line 1580
    invoke-virtual {p2, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1581
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    int-to-float p0, p0

    int-to-float p1, p1

    .line 1584
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1587
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Bitmap;

    const/4 p3, 0x0

    int-to-float v2, v1

    const/4 v3, 0x0

    .line 1588
    invoke-virtual {v0, p1, p3, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1589
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    add-int/2addr v1, p1

    goto :goto_1

    :cond_1
    return-object p2
.end method

.method private createAutoScaledLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;
    .locals 2

    const/high16 v0, 0x40e00000    # 7.0f

    .line 219
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->pxExact(F)F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    invoke-static {p1, p2, p3, v0, v1}, Lcom/squareup/text/Fonts;->getFittedTextSize(Landroid/text/TextPaint;Ljava/lang/CharSequence;IFF)I

    move-result v0

    .line 220
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1, p1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    int-to-float p1, v0

    .line 221
    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    const p1, 0x3f933333    # 1.15f

    .line 223
    invoke-static {p2, v1, p1, p3, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    return-object p1
.end method

.method private createBitmap(II)Landroid/graphics/Bitmap;
    .locals 7

    .line 1503
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTING_DEBUG_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 1504
    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p1, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, -0x1

    .line 1505
    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    if-eqz v0, :cond_0

    .line 1508
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1509
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    const/16 v3, 0x32

    const/16 v4, 0xff

    .line 1510
    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-static {v3, v5, v6, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    .line 1511
    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1514
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "px"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->heightDebugPaint:Landroid/text/TextPaint;

    const/high16 v4, 0x3f800000    # 1.0f

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 1515
    invoke-static {v2, v3, v4, p2, v5}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    const/4 v2, 0x0

    .line 1517
    div-int/lit8 p1, p1, 0x2

    invoke-virtual {p2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr p1, v3

    int-to-float p1, p1

    invoke-virtual {v0, v2, p1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1519
    invoke-virtual {p2, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-object v1
.end method

.method private static createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;
    .locals 8

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    move-object v1, p0

    .line 229
    new-instance p0, Landroid/text/StaticLayout;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p4

    move v5, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object p0
.end method

.method private divider(I)V
    .locals 1

    .line 1493
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p1

    const/high16 v0, -0x1000000

    .line 1494
    invoke-virtual {p1, v0}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1495
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private downloadLogoImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    .line 1532
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 1536
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->thumbor:Lcom/squareup/pollexor/Thumbor;

    invoke-virtual {v0, p1}, Lcom/squareup/pollexor/Thumbor;->buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    const/16 v0, 0x195

    const/16 v2, 0xca

    .line 1537
    invoke-virtual {p1, v0, v2}, Lcom/squareup/pollexor/ThumborUrlBuilder;->resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    .line 1538
    invoke-virtual {p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->fitIn()Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 1539
    invoke-static {}, Lcom/squareup/util/picasso/PicassoHelpers;->twoTone()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->filter([Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    .line 1540
    invoke-virtual {p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object p1

    .line 1542
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 1544
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mainScheduler:Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/util/picasso/PicassoHelpers;->toSingle(Lcom/squareup/picasso/RequestCreator;Lrx/Scheduler;)Lrx/Single;

    move-result-object p1

    const-wide/16 v2, 0x5

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1545
    invoke-static {v1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object v1

    invoke-virtual {p1, v2, v3, v0, v1}, Lrx/Single;->timeout(JLjava/util/concurrent/TimeUnit;Lrx/Single;)Lrx/Single;

    move-result-object p1

    .line 1547
    invoke-virtual {p1}, Lrx/Single;->toBlocking()Lrx/singles/BlockingSingle;

    move-result-object p1

    .line 1548
    invoke-virtual {p1}, Lrx/singles/BlockingSingle;->value()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private drawHorizontalRule(Landroid/graphics/Canvas;IIILcom/squareup/print/ThermalBitmapBuilder$RuleSize;)V
    .locals 6

    int-to-float v1, p3

    .line 1086
    iget p3, p5, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->sizePx:I

    sub-int p3, p2, p3

    int-to-float v2, p3

    int-to-float v3, p4

    int-to-float v4, p2

    iget-object v5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->inkPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawHorizontalRuleFromBottom(Landroid/graphics/Canvas;Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;II)V
    .locals 7

    .line 1080
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    sub-int v3, v0, p4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/ThermalBitmapBuilder;->drawHorizontalRule(Landroid/graphics/Canvas;IIILcom/squareup/print/ThermalBitmapBuilder$RuleSize;)V

    return-void
.end method

.method private drawQuickTipCheckbox(Landroid/graphics/Canvas;)V
    .locals 7

    .line 966
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    int-to-float v4, v0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->inkPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 967
    sget v0, Lcom/squareup/print/ThermalBitmapBuilder;->CHECKBOX_THICKNESS:I

    int-to-float v2, v0

    int-to-float v3, v0

    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sub-int v4, v1, v0

    int-to-float v4, v4

    sub-int/2addr v1, v0

    int-to-float v5, v1

    iget-object v6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->backgroundPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private fullWidthText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V
    .locals 2

    .line 1248
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {p2, p1, p3, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    const/4 p2, 0x1

    new-array p2, p2, [Landroid/text/Layout;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    .line 1249
    invoke-direct {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignTopBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private static getMaxWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I
    .locals 6

    .line 235
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    .line 236
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v4, p0, v2

    int-to-float v3, v3

    .line 237
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v4, v1, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v3, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v3
.end method

.method private labelledWriteLineOffset(Ljava/lang/String;I)V
    .locals 4

    .line 990
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {p1, v0, v3, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    .line 993
    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result p1

    float-to-double v1, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int p1, v1

    add-int/lit8 p1, p1, 0x3

    .line 996
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 997
    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-virtual {p2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object p2

    iget p2, p2, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 998
    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 999
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1001
    invoke-virtual {v0, v2}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1002
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder;->WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    invoke-direct {p0, v2, v0, p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->drawHorizontalRuleFromBottom(Landroid/graphics/Canvas;Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;II)V

    .line 1004
    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private px(F)I
    .locals 1

    const v0, 0x3faccccd    # 1.35f

    mul-float p1, p1, v0

    .line 205
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    iget v0, v0, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerInch:I

    int-to-float v0, v0

    mul-float p1, p1, v0

    const/high16 v0, 0x42fa0000    # 125.0f

    div-float/2addr p1, v0

    float-to-int p1, p1

    return p1
.end method

.method private pxExact(F)F
    .locals 1

    const v0, 0x3faccccd    # 1.35f

    mul-float p1, p1, v0

    .line 200
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    iget v0, v0, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerInch:I

    int-to-float v0, v0

    mul-float p1, p1, v0

    const/high16 v0, 0x42fa0000    # 125.0f

    div-float/2addr p1, v0

    return p1
.end method

.method public static pxToInches(ILcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;)F
    .locals 1

    int-to-float p0, p0

    .line 209
    iget p1, p1, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerInch:I

    int-to-float p1, p1

    const v0, 0x3faccccd    # 1.35f

    mul-float p1, p1, v0

    div-float/2addr p0, p1

    return p0
.end method

.method private quickTipCustomTipRow(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14

    move-object v6, p0

    .line 929
    iget v0, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipRowTextAvailableWidth:I

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget v1, v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    sub-int/2addr v0, v1

    div-int/lit8 v7, v0, 0x2

    .line 930
    iget v0, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sget v1, Lcom/squareup/print/ThermalBitmapBuilder;->CHECKBOX_RIGHT_MARGIN:I

    add-int v8, v0, v1

    add-int v4, v8, v7

    .line 931
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget v0, v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    add-int v9, v4, v0

    .line 933
    iget-object v0, v6, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const v2, 0x3f933333    # 1.15f

    move-object v3, p1

    .line 934
    invoke-static {p1, v0, v2, v7, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v10

    .line 936
    iget-object v0, v6, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v3, p2

    .line 937
    invoke-static {v3, v0, v2, v7, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v11

    .line 939
    iget v0, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget v1, v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    add-int/2addr v0, v1

    .line 940
    invoke-virtual {v10}, Landroid/text/Layout;->getHeight()I

    move-result v1

    .line 941
    invoke-virtual {v10}, Landroid/text/Layout;->getHeight()I

    move-result v2

    .line 940
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 942
    iget v1, v6, Lcom/squareup/print/ThermalBitmapBuilder;->boxContentWidth:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 943
    new-instance v13, Landroid/graphics/Canvas;

    invoke-direct {v13, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 945
    invoke-direct {p0, v13}, Lcom/squareup/print/ThermalBitmapBuilder;->drawQuickTipCheckbox(Landroid/graphics/Canvas;)V

    .line 946
    iget v2, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sget-object v5, Lcom/squareup/print/ThermalBitmapBuilder;->WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    move-object v0, p0

    move-object v1, v13

    move v3, v8

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder;->drawHorizontalRule(Landroid/graphics/Canvas;IIILcom/squareup/print/ThermalBitmapBuilder$RuleSize;)V

    .line 948
    iget v2, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    add-int v4, v9, v7

    sget-object v5, Lcom/squareup/print/ThermalBitmapBuilder;->WRITE_LINE_THICKNESS:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    move v3, v9

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder;->drawHorizontalRule(Landroid/graphics/Canvas;IIILcom/squareup/print/ThermalBitmapBuilder$RuleSize;)V

    .line 951
    invoke-virtual {v13}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v8

    .line 952
    iget v1, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sget-object v2, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget v2, v2, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 953
    invoke-virtual {v10, v13}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 954
    invoke-virtual {v13}, Landroid/graphics/Canvas;->restore()V

    .line 956
    invoke-virtual {v13}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v9

    .line 957
    iget v1, v6, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sget-object v2, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget v2, v2, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v13, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 958
    invoke-virtual {v11, v13}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 959
    invoke-virtual {v13}, Landroid/graphics/Canvas;->restore()V

    return-object v12
.end method

.method private quickTipOptionRow(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    .line 899
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 900
    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    invoke-static {p1, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    const-string v1, " "

    .line 901
    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    .line 902
    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 904
    iget-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    iget p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipRowTextAvailableWidth:I

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const v2, 0x3f933333    # 1.15f

    invoke-static {v0, p1, v2, p2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 907
    iget p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 908
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->boxContentWidth:I

    invoke-direct {p0, p2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 909
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 910
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->drawQuickTipCheckbox(Landroid/graphics/Canvas;)V

    .line 912
    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCheckboxSize:I

    sget v2, Lcom/squareup/print/ThermalBitmapBuilder;->CHECKBOX_RIGHT_MARGIN:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 913
    invoke-virtual {p1, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    return-object p2
.end method

.method private quickTipSpace(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Landroid/graphics/Bitmap;
    .locals 1

    .line 973
    iget p1, p1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->boxContentWidth:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private singleColumn(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V
    .locals 2

    .line 1111
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {p1, p2, p3, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    const/4 p2, 0x1

    new-array p2, p2, [Landroid/text/Layout;

    const/4 p3, 0x0

    aput-object p1, p2, p3

    .line 1112
    invoke-direct {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignTopBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1113
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private singleColumnAlignLeftBitmapFromLayouts(ILjava/util/List;)Landroid/graphics/Bitmap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Landroid/text/Layout;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .line 1269
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_7

    .line 1278
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1279
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1280
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/text/Layout;

    .line 1282
    invoke-virtual {v4}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1283
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, p1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    .line 1285
    :goto_1
    invoke-virtual {v4}, Landroid/text/Layout;->getWidth()I

    move-result v6

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1286
    invoke-virtual {v4}, Landroid/text/Layout;->getHeight()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    if-nez v3, :cond_3

    const/4 p1, 0x0

    return-object p1

    .line 1294
    :cond_3
    invoke-direct {p0, v2, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1295
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1298
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 1299
    :cond_4
    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1300
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/text/Layout;

    .line 1302
    invoke-virtual {v3}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1303
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    move v4, p1

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    .line 1305
    :goto_3
    invoke-virtual {v3, v2}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    const/4 v5, 0x0

    .line 1306
    invoke-virtual {v3}, Landroid/text/Layout;->getHeight()I

    move-result v3

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_2

    :cond_6
    return-object v0

    .line 1270
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "No layouts passed."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private varargs singleRowAlignBottomBitmapFromLayouts(Z[Landroid/text/Layout;)Landroid/graphics/Bitmap;
    .locals 10

    .line 1451
    array-length v0, p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-object v6, p2, v2

    if-eqz p1, :cond_0

    .line 1453
    invoke-virtual {v6}, Landroid/text/Layout;->getWidth()I

    move-result v7

    add-int/2addr v4, v7

    goto :goto_1

    .line 1455
    :cond_0
    invoke-virtual {v6}, Landroid/text/Layout;->getWidth()I

    move-result v7

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1458
    :goto_1
    invoke-virtual {v6}, Landroid/text/Layout;->getHeight()I

    move-result v7

    if-le v7, v3, :cond_1

    .line 1459
    invoke-virtual {v6}, Landroid/text/Layout;->getHeight()I

    move-result v3

    .line 1460
    invoke-virtual {v6}, Landroid/text/Layout;->getLineCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v6, v5}, Landroid/text/Layout;->getLineDescent(I)I

    move-result v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1464
    :cond_2
    invoke-direct {p0, v3, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1465
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1467
    array-length v4, p2

    :goto_2
    if-ge v1, v4, :cond_4

    aget-object v6, p2, v1

    .line 1468
    invoke-virtual {v6}, Landroid/text/Layout;->getHeight()I

    move-result v7

    sub-int v7, v3, v7

    .line 1469
    invoke-virtual {v6}, Landroid/text/Layout;->getLineCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v8}, Landroid/text/Layout;->getLineDescent(I)I

    move-result v8

    sub-int v8, v5, v8

    sub-int/2addr v7, v8

    int-to-float v8, v7

    const/4 v9, 0x0

    .line 1472
    invoke-virtual {v2, v9, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1473
    invoke-virtual {v6, v2}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    neg-int v7, v7

    int-to-float v7, v7

    .line 1474
    invoke-virtual {v2, v9, v7}, Landroid/graphics/Canvas;->translate(FF)V

    if-eqz p1, :cond_3

    .line 1477
    invoke-virtual {v6}, Landroid/text/Layout;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    return-object v0
.end method

.method private varargs singleRowAlignTopBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;
    .locals 9

    .line 1317
    array-length v0, p1

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1319
    :goto_0
    array-length v7, p1

    if-ge v3, v7, :cond_0

    .line 1320
    aget-object v7, p1, v3

    .line 1321
    invoke-direct {p0, v7}, Lcom/squareup/print/ThermalBitmapBuilder;->calculateTextVisualPaddingTop(Landroid/text/Layout;)F

    move-result v8

    .line 1322
    aput v8, v0, v3

    .line 1325
    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 1327
    invoke-virtual {v7}, Landroid/text/Layout;->getWidth()I

    move-result v8

    int-to-float v8, v8

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 1328
    invoke-virtual {v7}, Landroid/text/Layout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-static {v7, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1331
    :cond_0
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1332
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1334
    :goto_1
    array-length v5, p1

    if-ge v1, v5, :cond_1

    .line 1337
    aget v5, v0, v1

    sub-float v5, v6, v5

    .line 1340
    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    .line 1341
    invoke-virtual {v4, v2, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1342
    aget-object v5, p1, v1

    invoke-virtual {v5, v4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1343
    invoke-virtual {v4}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object v3
.end method

.method private varargs singleRowCenterVerticalBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;
    .locals 10

    .line 1409
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v5, p1, v2

    .line 1410
    invoke-virtual {v5}, Landroid/text/Layout;->getWidth()I

    move-result v6

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1411
    invoke-virtual {v5}, Landroid/text/Layout;->getHeight()I

    move-result v5

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1414
    :cond_0
    invoke-direct {p0, v3, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1415
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1416
    array-length v4, p1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_2

    aget-object v6, p1, v5

    .line 1418
    invoke-virtual {v6}, Landroid/text/Layout;->getHeight()I

    move-result v7

    sub-int v7, v3, v7

    if-eqz v7, :cond_1

    if-lez v3, :cond_1

    .line 1421
    invoke-virtual {v6}, Landroid/text/Layout;->getHeight()I

    move-result v7

    sub-int v7, v3, v7

    div-int/2addr v7, v3

    .line 1422
    invoke-virtual {v6}, Landroid/text/Layout;->getHeight()I

    move-result v8

    sub-int v8, v3, v8

    div-int/lit8 v8, v8, 0x2

    add-int/lit8 v7, v7, 0x1

    mul-int v7, v7, v8

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    :goto_2
    int-to-float v8, v7

    const/4 v9, 0x0

    .line 1425
    invoke-virtual {v2, v9, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1426
    invoke-virtual {v6, v2}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    neg-int v6, v7

    int-to-float v6, v6

    .line 1427
    invoke-virtual {v2, v9, v6}, Landroid/graphics/Canvas;->translate(FF)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private space(I)V
    .locals 1

    .line 1485
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(II)V

    return-void
.end method

.method private space(II)V
    .locals 0

    .line 1489
    invoke-direct {p0, p2, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private taxBreakdownRow(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V
    .locals 5

    const-string v0, "col1Text"

    .line 1144
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    const-string v0, "col2Text"

    .line 1145
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    const-string v0, "col3Text"

    .line 1146
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    const-string v0, "col4Text"

    .line 1147
    invoke-static {p4, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 1149
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p5, p2, v1, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1151
    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    div-int/lit8 v1, v1, 0x4

    mul-int/lit8 v2, v1, 0x2

    sub-int v3, v2, v0

    .line 1153
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1154
    invoke-static {p1, p5, p6, v3, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1155
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {p2, p5, p6, v0, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    .line 1156
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {p3, p5, p6, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p3

    .line 1157
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {p4, p5, p6, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p4

    .line 1159
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result p5

    invoke-virtual {p2}, Landroid/text/Layout;->getHeight()I

    move-result p6

    invoke-static {p5, p6}, Ljava/lang/Math;->max(II)I

    move-result p5

    .line 1160
    invoke-virtual {p3}, Landroid/text/Layout;->getHeight()I

    move-result p6

    invoke-static {p5, p6}, Ljava/lang/Math;->max(II)I

    move-result p5

    .line 1161
    invoke-virtual {p4}, Landroid/text/Layout;->getHeight()I

    move-result p6

    invoke-static {p5, p6}, Ljava/lang/Math;->max(II)I

    move-result p5

    .line 1162
    iget p6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, p5, p6}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p5

    .line 1163
    new-instance p6, Landroid/graphics/Canvas;

    invoke-direct {p6, p5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1165
    invoke-virtual {p1, p6}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1166
    invoke-virtual {p6}, Landroid/graphics/Canvas;->save()I

    int-to-float p1, v3

    const/4 v0, 0x0

    .line 1167
    invoke-virtual {p6, p1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1168
    invoke-virtual {p2, p6}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1169
    invoke-virtual {p6}, Landroid/graphics/Canvas;->restore()V

    .line 1171
    invoke-virtual {p6}, Landroid/graphics/Canvas;->save()I

    int-to-float p1, v2

    .line 1172
    invoke-virtual {p6, p1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1173
    invoke-virtual {p3, p6}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1174
    invoke-virtual {p6}, Landroid/graphics/Canvas;->restore()V

    .line 1176
    invoke-virtual {p6}, Landroid/graphics/Canvas;->save()I

    mul-int/lit8 v1, v1, 0x3

    int-to-float p1, v1

    .line 1177
    invoke-virtual {p6, p1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1178
    invoke-virtual {p4, p6}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1179
    invoke-virtual {p6}, Landroid/graphics/Canvas;->restore()V

    .line 1181
    invoke-direct {p0, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private textAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;ZZZ)V
    .locals 9

    if-eqz p3, :cond_0

    .line 1094
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    move-object v4, v0

    const/high16 v6, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1097
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    move-object v4, v0

    const v6, 0x3f933333    # 1.15f

    .line 1100
    :goto_0
    new-instance v0, Lcom/squareup/text/TabularSpan;

    invoke-direct {v0}, Lcom/squareup/text/TabularSpan;-><init>()V

    invoke-static {p2, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    if-eqz p4, :cond_1

    .line 1102
    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    invoke-static {v0, v1}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    :cond_1
    move-object v2, v0

    .line 1106
    iget-object v3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    move-object v0, p0

    move-object v1, p1

    move v7, p5

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsAlignRight(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZZ)V

    return-void
.end method

.method private twoColumnsAlignLeft(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FF)V
    .locals 3

    .line 1118
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p4, p2, v1, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1119
    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    .line 1120
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1121
    invoke-static {p1, p3, p5, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1122
    iget p3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    iget p5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    sub-int/2addr p3, p5

    sub-int/2addr p3, v1

    sget-object p5, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {p2, p4, p6, p3, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    .line 1125
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result p3

    invoke-virtual {p2}, Landroid/text/Layout;->getHeight()I

    move-result p4

    invoke-static {p3, p4}, Ljava/lang/Math;->max(II)I

    move-result p3

    .line 1127
    iget p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, p3, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p3

    .line 1128
    new-instance p4, Landroid/graphics/Canvas;

    invoke-direct {p4, p3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1129
    invoke-virtual {p1, p4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1130
    invoke-virtual {p4}, Landroid/graphics/Canvas;->save()I

    .line 1131
    iget p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    add-int/2addr v1, p1

    int-to-float p1, v1

    const/4 p5, 0x0

    invoke-virtual {p4, p1, p5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1132
    invoke-virtual {p2, p4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1133
    invoke-virtual {p4}, Landroid/graphics/Canvas;->restore()V

    .line 1134
    invoke-direct {p0, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private twoColumnsAlignRight(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZZ)V
    .locals 4

    .line 1187
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p4, p2, v1, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 1188
    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    iget v3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {p1, p3, p5, v2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1190
    iget p3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object p5, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    .line 1191
    invoke-static {p2, p4, p6, p3, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    const/4 p3, 0x1

    const/4 p4, 0x2

    if-eqz p8, :cond_0

    new-array p4, p4, [Landroid/text/Layout;

    aput-object p1, p4, v1

    aput-object p2, p4, p3

    .line 1195
    invoke-direct {p0, v1, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignBottomBitmapFromLayouts(Z[Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz p7, :cond_1

    new-array p4, p4, [Landroid/text/Layout;

    aput-object p1, p4, v1

    aput-object p2, p4, p3

    .line 1197
    invoke-direct {p0, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowCenterVerticalBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-array p4, p4, [Landroid/text/Layout;

    aput-object p1, p4, v1

    aput-object p2, p4, p3

    .line 1199
    invoke-direct {p0, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignTopBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1201
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V
    .locals 3

    .line 1222
    invoke-static {p1, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->getMaxWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v0

    .line 1223
    invoke-static {p2, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->getMaxWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v1

    .line 1225
    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    iget v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    if-gt v0, v2, :cond_0

    .line 1227
    sget-object p7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1228
    invoke-static {p1, p3, p5, v2, p7}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1229
    iget p3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object p5, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-static {p2, p4, p6, p3, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    const/4 p3, 0x2

    new-array p3, p3, [Landroid/text/Layout;

    const/4 p4, 0x0

    aput-object p1, p3, p4

    const/4 p1, 0x1

    aput-object p2, p3, p1

    .line 1231
    invoke-direct {p0, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignTopBitmapFromLayouts([Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1232
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1235
    :cond_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1236
    invoke-static {p1, p3, p5, v2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1237
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->bitmapFromLayout(Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    if-eqz v1, :cond_2

    if-eqz p7, :cond_1

    .line 1239
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 1240
    :cond_1
    iget p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object p3, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {p2, p4, p6, p1, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 1242
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->bitmapFromLayout(Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public appendSectionHeader(Ljava/lang/CharSequence;)V
    .locals 1

    const/16 v0, 0x34

    .line 1012
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendTextCenteredInBlackRectangle(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public barcode(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 447
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    const/16 v1, 0x68

    invoke-static {p1, v0, v1}, Lcom/squareup/print/BarcodeCreator;->generateBarcode(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 448
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-object p0
.end method

.method public bigHeaderSingleLine(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 664
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V

    return-object p0
.end method

.method public boldTitleAndBigAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 521
    iget-object v0, p1, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    invoke-static {v0, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v3

    iget-object v4, p1, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->textAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;ZZZ)V

    return-object p0
.end method

.method public canFitIntoTwoColumnsOnSingleLine(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 1

    .line 1205
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->canFitIntoTwoColumnsOnSingleLine(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;)Z

    move-result p1

    return p1
.end method

.method public centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 493
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public diningOptionHeader(Ljava/lang/CharSequence;Z)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 769
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->thinDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    const/16 v0, 0xc

    .line 770
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    if-eqz p2, :cond_0

    .line 771
    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallDiningOptionPaint:Landroid/text/TextPaint;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->diningOptionPaint:Landroid/text/TextPaint;

    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p2, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    .line 773
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    .line 774
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->thinDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    return-object p0
.end method

.method public dividerWithMinText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    .line 401
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallMediumPaint:Landroid/text/TextPaint;

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendTextCenteredInDivider(Ljava/lang/CharSequence;Landroid/text/TextPaint;)V

    return-object p0
.end method

.method public eMoneyReprintHeader(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 756
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 757
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->headlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    .line 758
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-object p0
.end method

.method public fullWidthBigMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 478
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthCenteredHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 458
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthCenteredMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 483
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthCenteredSmallText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 463
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallRegularPaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 453
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthItalicText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 488
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardItalicPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 473
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 468
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const v1, 0x3f933333    # 1.15f

    invoke-direct {p0, v0, p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Landroid/text/TextPaint;Ljava/lang/CharSequence;F)V

    return-object p0
.end method

.method public hasNarrowContent()Z
    .locals 1

    .line 213
    iget-boolean v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->narrowContent:Z

    return v0
.end method

.method public headlineBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0x32

    .line 764
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendTextCenteredInBlackRectangle(Ljava/lang/CharSequence;I)V

    return-object p0
.end method

.method public labelledWriteLineHalfWidth(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    .line 843
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->labelledWriteLineOffset(Ljava/lang/String;I)V

    return-object p0
.end method

.method public largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0x23

    .line 421
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    return-object p0
.end method

.method public lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 386
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->divider(I)V

    return-object p0
.end method

.method public logo(Ljava/lang/String;Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 0

    .line 852
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->downloadLogoImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 855
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->centerBitmapHorizontally(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 856
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    if-eqz p2, :cond_0

    .line 859
    invoke-virtual {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_0
    return-object p0
.end method

.method public mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/4 v0, 0x3

    .line 396
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->divider(I)V

    return-object p0
.end method

.method public mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0x19

    .line 416
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    return-object p0
.end method

.method public multilineTextAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 498
    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder;->textAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;ZZZ)V

    return-object p0
.end method

.method public multilineTitleAmountAndText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 7

    .line 504
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/ThermalBitmapBuilder;->textAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;ZZZ)V

    .line 505
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-lez p1, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v2, ""

    move-object v0, p0

    move-object v1, p3

    .line 506
    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder;->textAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;ZZZ)V

    :cond_0
    return-object p0
.end method

.method public quickTipOptions(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/print/ThermalBitmapBuilder;"
        }
    .end annotation

    .line 780
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 783
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v0, v1, :cond_3

    const/16 v0, 0x2c

    .line 787
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendTextCenteredInBlackRectangle(Ljava/lang/CharSequence;I)V

    .line 789
    new-instance p1, Ljava/util/ArrayList;

    const/16 v0, 0x9

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    .line 791
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 792
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipOptionRow(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 794
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 795
    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-direct {p0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipSpace(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 799
    :cond_1
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_2

    .line 800
    sget-object p2, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-direct {p0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipSpace(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Landroid/graphics/Bitmap;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801
    invoke-direct {p0, p4, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipCustomTipRow(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 804
    :cond_2
    sget-object p2, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    iget p2, p2, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->sizePx:I

    .line 805
    sget-object p3, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget p3, p3, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    add-int/2addr p3, p2

    sget-object p4, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget p4, p4, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    sget-object p5, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    iget p5, p5, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    .line 806
    invoke-static {p3, p4, p5, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->concatenateBitmaps(IIILjava/util/List;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 809
    new-instance p3, Landroid/graphics/Canvas;

    invoke-direct {p3, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    int-to-float v3, p2

    .line 810
    invoke-virtual {p3}, Landroid/graphics/Canvas;->getHeight()I

    move-result p4

    int-to-float v4, p4

    iget-object v5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->inkPaint:Landroid/graphics/Paint;

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 811
    invoke-virtual {p3}, Landroid/graphics/Canvas;->getWidth()I

    move-result p4

    sub-int/2addr p4, p2

    int-to-float v1, p4

    invoke-virtual {p3}, Landroid/graphics/Canvas;->getWidth()I

    move-result p2

    int-to-float v3, p2

    invoke-virtual {p3}, Landroid/graphics/Canvas;->getHeight()I

    move-result p2

    int-to-float v4, p2

    iget-object v5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->inkPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 814
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    .line 816
    sget-object p1, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    iget p1, p1, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->sizePx:I

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->divider(I)V

    return-object p0

    .line 784
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Must specify custom and total label or neither."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 781
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Size of options must match size of calculatedOptions."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public render()Landroid/graphics/Bitmap;
    .locals 3

    .line 879
    iget v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->horizontalMargin:I

    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->margin:I

    iget-object v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bitmaps:Ljava/util/List;

    invoke-static {v0, v1, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->concatenateBitmaps(IIILjava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public reprintBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0x50

    .line 745
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendTextCenteredInBlackRectangle(Ljava/lang/CharSequence;I)V

    return-object p0
.end method

.method public signatureLine()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    const-string/jumbo v0, "\u00d7"

    const/4 v1, 0x0

    .line 829
    invoke-direct {p0, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->labelledWriteLineOffset(Ljava/lang/String;I)V

    return-object p0
.end method

.method public singleColumn(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 674
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V

    return-object p0
.end method

.method public smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0x14

    .line 411
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    return-object p0
.end method

.method public space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 0

    .line 426
    iget p1, p1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->sizePx:I

    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    return-object p0
.end method

.method public stubHeaderSingleLine(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 669
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->extraBigHeadlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V

    return-object p0
.end method

.method public taxBreakdownRow(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 7

    if-eqz p1, :cond_0

    .line 872
    iget-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    :goto_0
    move-object v5, p1

    const v6, 0x3f933333    # 1.15f

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    .line 873
    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/ThermalBitmapBuilder;->taxBreakdownRow(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V

    return-object p0
.end method

.method public thinDivider()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/4 v0, 0x2

    .line 391
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->divider(I)V

    return-object p0
.end method

.method public ticketHeaderSingleLine(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 2

    .line 659
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->headlinePaint:Landroid/text/TextPaint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;Landroid/text/TextPaint;F)V

    return-object p0
.end method

.method public ticketQuantityAndItem(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")",
            "Lcom/squareup/print/ThermalBitmapBuilder;"
        }
    .end annotation

    .line 555
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    .line 556
    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->ticketItemPaint:Landroid/text/TextPaint;

    .line 557
    iget-object v2, p0, Lcom/squareup/print/ThermalBitmapBuilder;->ticketItemItalicPaint:Landroid/text/TextPaint;

    .line 560
    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    const/16 v4, 0x24

    .line 561
    invoke-direct {p0, v0, p1, v4, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->createAutoScaledLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;ILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    .line 564
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual {v0, p3, v5, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v3, v6

    .line 565
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const v7, 0x3f933333    # 1.15f

    .line 566
    invoke-static {p3, v0, v7, v3, v6}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p3

    .line 569
    iget v6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    add-int/lit8 v6, v6, -0x14

    sub-int/2addr v6, v4

    sub-int/2addr v6, v3

    .line 571
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 573
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 574
    invoke-static {p4, v0, v7, v6, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p4

    .line 573
    invoke-interface {v3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_0

    .line 577
    sget-object p4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 578
    invoke-static {p2, v1, v7, v6, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    .line 577
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581
    :cond_0
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/CharSequence;

    .line 582
    sget-object p5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 583
    invoke-static {p4, v1, v7, v6, p5}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p4

    .line 582
    invoke-interface {v3, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 586
    :cond_1
    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 587
    invoke-static {p6, v2, v7, v6, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    .line 586
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p2, 0x2

    new-array p2, p2, [Landroid/text/Layout;

    aput-object p1, p2, v5

    const/4 p1, 0x1

    aput-object p3, p2, p1

    .line 589
    invoke-direct {p0, p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->singleRowAlignBottomBitmapFromLayouts(Z[Landroid/text/Layout;)Landroid/graphics/Bitmap;

    move-result-object p1

    const/16 p2, 0x8

    .line 591
    invoke-direct {p0, p2, v3}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumnAlignLeftBitmapFromLayouts(ILjava/util/List;)Landroid/graphics/Bitmap;

    move-result-object p2

    if-nez p2, :cond_2

    goto :goto_1

    .line 592
    :cond_2
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 594
    :goto_1
    iget p4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    .line 595
    invoke-virtual {p3}, Landroid/text/Layout;->getHeight()I

    move-result p3

    invoke-static {p3, v5}, Ljava/lang/Math;->max(II)I

    move-result p3

    .line 597
    invoke-direct {p0, p3, p4}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object p3

    .line 598
    new-instance p4, Landroid/graphics/Canvas;

    invoke-direct {p4, p3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 p5, 0x0

    const/4 p6, 0x0

    .line 600
    invoke-virtual {p4, p1, p6, p6, p5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 601
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    add-int/lit8 p1, p1, 0x14

    int-to-float p1, p1

    invoke-virtual {p4, p1, p6}, Landroid/graphics/Canvas;->translate(FF)V

    if-eqz p2, :cond_3

    .line 604
    invoke-virtual {p4, p2, p6, p6, p5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 607
    :cond_3
    invoke-direct {p0, p3}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-object p0
.end method

.method public ticketTopPadding(Z)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 0

    if-eqz p1, :cond_0

    .line 432
    sget-object p1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_0
    const/16 p1, 0x9c

    .line 434
    invoke-direct {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    :goto_0
    return-object p0
.end method

.method public tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0xc

    .line 406
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(I)V

    return-object p0
.end method

.method public titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    .line 512
    iget-object v0, p1, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    move-result-object p1

    return-object p1
.end method

.method public titleAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 516
    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder;->textAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;ZZZ)V

    return-object p0
.end method

.method public transactionTypeBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 0

    .line 440
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 441
    invoke-virtual {p0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthCenteredMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 442
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-object p0
.end method

.method public twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 7

    .line 613
    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    const v6, 0x3f933333    # 1.15f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsAlignLeft(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FF)V

    return-object p0
.end method

.method public twoColumnsLeftExpandingTextMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 619
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v2

    iget-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpan:Landroid/text/style/CharacterStyle;

    invoke-static {p2, p1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v3

    iget-object v5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    const v6, 0x3f933333    # 1.15f

    const v7, 0x3f933333    # 1.15f

    move-object v1, p0

    move-object v4, v5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsAlignLeft(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FF)V

    return-object p0
.end method

.method public twoColumnsLeftHeadlineOrCollapseRightAlignPadded(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 680
    iget-object v3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->smallHeadlinePaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const/high16 v5, 0x3f800000    # 1.0f

    const v6, 0x3f933333    # 1.15f

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V

    return-object p0
.end method

.method public twoColumnsOrCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 625
    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    const v6, 0x3f933333    # 1.15f

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V

    return-object p0
.end method

.method public twoColumnsOrCollapseBigMediumText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 646
    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    const v6, 0x3f933333    # 1.15f

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V

    return-object p0
.end method

.method public twoColumnsOrCollapseBigText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 639
    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigRegularPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    const v6, 0x3f933333    # 1.15f

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V

    return-object p0
.end method

.method public twoColumnsOrCollapseLeftTextBigMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 653
    iget-object v3, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigMediumPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->bigRegularPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    const v6, 0x3f933333    # 1.15f

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V

    return-object p0
.end method

.method public twoColumnsOrCollapseMediumText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 632
    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardMediumPaint:Landroid/text/TextPaint;

    const v5, 0x3f933333    # 1.15f

    const v6, 0x3f933333    # 1.15f

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextPaint;FFZ)V

    return-object p0
.end method

.method public twoColumnsRightExpandsLeftOverflows(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 8

    .line 692
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    invoke-static {p2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->getMaxWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v0

    const-string v1, "\n"

    .line 693
    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 696
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {p1, v3, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    iget v5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    iget v6, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    sub-int/2addr v5, v6

    sub-int/2addr v5, v0

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const v7, 0x3f933333    # 1.15f

    invoke-static {v2, v4, v7, v5, v6}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v2

    .line 700
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    array-length v5, v1

    add-int/lit8 v5, v5, 0x1

    if-le v4, v5, :cond_1

    .line 701
    array-length v1, v1

    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineStart(I)I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 705
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_0

    .line 706
    invoke-interface {p1, v3, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    .line 708
    :cond_0
    invoke-interface {p1, v3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 711
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-interface {p1, v1, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    .line 712
    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    iget v4, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    iget v5, p0, Lcom/squareup/print/ThermalBitmapBuilder;->gutter:I

    sub-int/2addr v4, v5

    sub-int/2addr v4, v0

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {v2, v1, v7, v4, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v2

    .line 714
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 715
    invoke-static {p1, v0, v7, v1, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 719
    :goto_1
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder;->standardRegularPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    .line 720
    invoke-static {p2, v0, v7, v1, v4}, Lcom/squareup/print/ThermalBitmapBuilder;->createStaticLayout(Ljava/lang/CharSequence;Landroid/text/TextPaint;FILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object p2

    .line 724
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v0

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    move-result v3

    :goto_2
    add-int/2addr v0, v3

    .line 725
    invoke-virtual {p2}, Landroid/text/Layout;->getHeight()I

    move-result v1

    .line 723
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 727
    iget v1, p0, Lcom/squareup/print/ThermalBitmapBuilder;->contentWidth:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 728
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 729
    invoke-virtual {v2, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 730
    invoke-virtual {p2, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    if-eqz p1, :cond_3

    .line 733
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    const/4 p2, 0x0

    .line 734
    invoke-virtual {v2}, Landroid/text/Layout;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, p2, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 735
    invoke-virtual {p1, v1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 736
    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 739
    :cond_3
    invoke-direct {p0, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->append(Landroid/graphics/Bitmap;)V

    return-object p0
.end method

.method public voidBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;
    .locals 1

    const/16 v0, 0x50

    .line 751
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendTextCenteredInBlackRectangle(Ljava/lang/CharSequence;I)V

    return-object p0
.end method
