.class Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;
.super Ljava/lang/Object;
.source "StarMicronicsPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/StarMicronicsPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StarGetPortResult"
.end annotation


# instance fields
.field final attempts:I

.field final port:Lcom/starmicronics/stario/StarIOPort;

.field final totalTimeMs:J


# direct methods
.method constructor <init>(Lcom/starmicronics/stario/StarIOPort;IJ)V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->port:Lcom/starmicronics/stario/StarIOPort;

    .line 62
    iput p2, p0, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->attempts:I

    .line 63
    iput-wide p3, p0, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->totalTimeMs:J

    return-void
.end method
