.class public Lcom/squareup/print/payload/TicketBillPayloadFactory;
.super Lcom/squareup/print/payload/ReceiptPayloadFactory;
.source "TicketBillPayloadFactory.java"


# instance fields
.field private final formatter:Lcom/squareup/print/ReceiptFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V
    .locals 14
    .param p9    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object v9, p1

    move-object/from16 v10, p10

    move-object/from16 v11, p5

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    .line 46
    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/ReceiptPayloadFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V

    move-object/from16 v1, p3

    .line 49
    iput-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    return-void
.end method


# virtual methods
.method public fromOrder(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/print/payload/TicketBillPayload;
    .locals 19

    move-object/from16 v0, p0

    .line 53
    invoke-static/range {p1 .. p1}, Lcom/squareup/util/TaxBreakdown;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v9

    .line 56
    iget-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/OrderSnapshot;->getEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v13

    .line 58
    iget-object v10, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v11, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/OrderSnapshot;->timestampAsDate()Ljava/util/Date;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v17, v1

    invoke-static/range {v10 .. v17}, Lcom/squareup/print/sections/HeaderSection;->createSection(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/Date;Lcom/squareup/permissions/Employee;Lcom/squareup/print/payload/ReceiptPayload$RenderType;ZZLcom/squareup/settings/server/Features;)Lcom/squareup/print/sections/HeaderSection;

    move-result-object v10

    .line 62
    iget-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-static/range {v1 .. v7}, Lcom/squareup/print/sections/CodesSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/sections/CodesSection;

    move-result-object v11

    .line 65
    iget-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v5, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v3, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    .line 66
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/util/Locale;

    iget-object v3, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 67
    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v7

    iget-object v8, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->res:Lcom/squareup/util/Res;

    move-object/from16 v3, p1

    move-object v4, v9

    .line 65
    invoke-static/range {v1 .. v8}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;Ljava/util/Locale;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v5

    .line 69
    iget-object v12, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    .line 75
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v16

    const/4 v15, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    move-object/from16 v13, p1

    .line 70
    invoke-static/range {v12 .. v18}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v6

    .line 79
    iget-object v1, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 80
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/OrderSnapshot;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v4, p1

    invoke-static {v1, v4, v9, v2, v3}, Lcom/squareup/print/sections/TotalSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/TotalSection;

    move-result-object v7

    .line 83
    iget-object v12, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v13, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v15, v0, Lcom/squareup/print/payload/TicketBillPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v14, p1

    .line 84
    invoke-static/range {v12 .. v17}, Lcom/squareup/print/sections/FooterSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/print/sections/FooterSection;

    move-result-object v8

    .line 86
    new-instance v1, Lcom/squareup/print/payload/TicketBillPayload;

    move-object v2, v1

    move-object v3, v10

    move-object v4, v11

    invoke-direct/range {v2 .. v8}, Lcom/squareup/print/payload/TicketBillPayload;-><init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/FooterSection;)V

    return-object v1
.end method
