.class public Lcom/squareup/print/papersig/TipSections;
.super Ljava/lang/Object;
.source "TipSections.java"


# instance fields
.field public final quick:Lcom/squareup/print/papersig/QuickTipSection;

.field public final traditional:Lcom/squareup/print/papersig/TraditionalTipSection;


# direct methods
.method public constructor <init>(Lcom/squareup/print/papersig/QuickTipSection;)V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "quickTipSection"

    .line 16
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/papersig/QuickTipSection;

    iput-object p1, p0, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    const/4 p1, 0x0

    .line 17
    iput-object p1, p0, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/papersig/TraditionalTipSection;)V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "traditionalTipSection"

    .line 11
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/papersig/TraditionalTipSection;

    iput-object p1, p0, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    const/4 p1, 0x0

    .line 12
    iput-object p1, p0, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 22
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 24
    :cond_1
    check-cast p1, Lcom/squareup/print/papersig/TipSections;

    .line 26
    iget-object v2, p0, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    invoke-virtual {v2, v3}, Lcom/squareup/print/papersig/TraditionalTipSection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 29
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    iget-object p1, p1, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Lcom/squareup/print/papersig/QuickTipSection;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_1

    :cond_4
    if-eqz p1, :cond_5

    :goto_1
    return v1

    :cond_5
    return v0

    :cond_6
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/print/papersig/TraditionalTipSection;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 36
    iget-object v2, p0, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/squareup/print/papersig/QuickTipSection;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method
