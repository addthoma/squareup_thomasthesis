.class public final Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;
.super Ljava/lang/Object;
.source "PrintModule_ProvidePrintSpoolerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/PrintSpooler;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final blockedPrinterLogRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/BlockedPrinterLogRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final initialListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final printQueueExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintQueueExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final printTargetRouterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintTargetRouter;",
            ">;"
        }
    .end annotation
.end field

.field private final rendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PayloadRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PayloadRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintTargetRouter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintQueueExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/BlockedPrinterLogRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->rendererProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->printTargetRouterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->hardwarePrinterExecutorProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->printQueueExecutorProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->initialListenersProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->blockedPrinterLogRunnerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p10, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PayloadRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintTargetRouter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintQueueExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/BlockedPrinterLogRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;"
        }
    .end annotation

    .line 77
    new-instance v11, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static providePrintSpooler(Lcom/squareup/print/PayloadRenderer;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/HardwarePrinterExecutor;Ljava/lang/Object;Ljava/util/Set;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/BlockedPrinterLogRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)Lcom/squareup/print/PrintSpooler;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PayloadRenderer;",
            "Lcom/squareup/print/PrintTargetRouter;",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            "Lcom/squareup/print/HardwarePrinterExecutor;",
            "Ljava/lang/Object;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/print/BlockedPrinterLogRunner;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/util/Clock;",
            ")",
            "Lcom/squareup/print/PrintSpooler;"
        }
    .end annotation

    .line 85
    move-object v4, p4

    check-cast v4, Lcom/squareup/print/PrintQueueExecutor;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v0 .. v9}, Lcom/squareup/print/PrintModule;->providePrintSpooler(Lcom/squareup/print/PayloadRenderer;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/HardwarePrinterExecutor;Lcom/squareup/print/PrintQueueExecutor;Ljava/util/Set;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/BlockedPrinterLogRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)Lcom/squareup/print/PrintSpooler;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrintSpooler;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/PrintSpooler;
    .locals 11

    .line 64
    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->rendererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/print/PayloadRenderer;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->printTargetRouterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/PrintTargetRouter;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->hardwarePrinterExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/HardwarePrinterExecutor;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->printQueueExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->initialListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Set;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->blockedPrinterLogRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/print/BlockedPrinterLogRunner;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/Clock;

    invoke-static/range {v1 .. v10}, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->providePrintSpooler(Lcom/squareup/print/PayloadRenderer;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/HardwarePrinterExecutor;Ljava/lang/Object;Ljava/util/Set;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/BlockedPrinterLogRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)Lcom/squareup/print/PrintSpooler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_ProvidePrintSpoolerFactory;->get()Lcom/squareup/print/PrintSpooler;

    move-result-object v0

    return-object v0
.end method
