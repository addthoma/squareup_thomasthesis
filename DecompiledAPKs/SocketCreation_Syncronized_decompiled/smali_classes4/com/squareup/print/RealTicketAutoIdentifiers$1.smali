.class Lcom/squareup/print/RealTicketAutoIdentifiers$1;
.super Lcom/squareup/server/ErrorLoggingCallback;
.source "RealTicketAutoIdentifiers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealTicketAutoIdentifiers;->requestNextTicketIdentifier()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/ErrorLoggingCallback<",
        "Lcom/squareup/server/TicketIdentifierResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/print/RealTicketAutoIdentifiers;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealTicketAutoIdentifiers;Ljava/lang/String;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/print/RealTicketAutoIdentifiers$1;->this$0:Lcom/squareup/print/RealTicketAutoIdentifiers;

    invoke-direct {p0, p2}, Lcom/squareup/server/ErrorLoggingCallback;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/server/TicketIdentifierResponse;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers$1;->this$0:Lcom/squareup/print/RealTicketAutoIdentifiers;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/print/RealTicketAutoIdentifiers;->access$002(Lcom/squareup/print/RealTicketAutoIdentifiers;Z)Z

    .line 122
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers$1;->this$0:Lcom/squareup/print/RealTicketAutoIdentifiers;

    invoke-static {v0}, Lcom/squareup/print/RealTicketAutoIdentifiers;->access$100(Lcom/squareup/print/RealTicketAutoIdentifiers;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const-string v1, "[RealTicketAutoIdentifiers] Got ticket identifier response: %s"

    .line 123
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/print/RealTicketAutoIdentifiers$1;->this$0:Lcom/squareup/print/RealTicketAutoIdentifiers;

    iget p1, p1, Lcom/squareup/server/TicketIdentifierResponse;->counter:I

    invoke-static {v0, p1}, Lcom/squareup/print/RealTicketAutoIdentifiers;->access$200(Lcom/squareup/print/RealTicketAutoIdentifiers;I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 119
    check-cast p1, Lcom/squareup/server/TicketIdentifierResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/print/RealTicketAutoIdentifiers$1;->call(Lcom/squareup/server/TicketIdentifierResponse;)V

    return-void
.end method
