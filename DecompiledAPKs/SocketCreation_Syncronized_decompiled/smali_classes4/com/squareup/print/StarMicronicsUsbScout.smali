.class public Lcom/squareup/print/StarMicronicsUsbScout;
.super Lcom/squareup/print/PrinterScout;
.source "StarMicronicsUsbScout.java"


# static fields
.field private static final PRINTER_SEARCH_CODE:Ljava/lang/String; = "USB:"

.field private static final USB_SCOUT_TIMEOUT_MS:I = 0x3e8


# instance fields
.field private final context:Landroid/content/Context;

.field private final starMicronicsPrinterFactory:Lcom/squareup/print/StarMicronicsPrinter$Factory;

.field private started:Z

.field private final usbManager:Lcom/squareup/hardware/usb/UsbManager;

.field private final usbPortMapper:Lcom/squareup/usb/UsbPortMapper;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/settings/server/Features;Lcom/squareup/usb/UsbPortMapper;Lcom/squareup/print/StarMicronicsPrinter$Factory;)V
    .locals 0

    .line 41
    invoke-direct {p0, p2, p3, p5}, Lcom/squareup/print/PrinterScout;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/print/StarMicronicsUsbScout;->context:Landroid/content/Context;

    .line 43
    iput-object p4, p0, Lcom/squareup/print/StarMicronicsUsbScout;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    .line 44
    iput-object p6, p0, Lcom/squareup/print/StarMicronicsUsbScout;->usbPortMapper:Lcom/squareup/usb/UsbPortMapper;

    .line 45
    iput-object p7, p0, Lcom/squareup/print/StarMicronicsUsbScout;->starMicronicsPrinterFactory:Lcom/squareup/print/StarMicronicsPrinter$Factory;

    return-void
.end method

.method private getDeviceAddressFromPortName(Ljava/lang/String;)I
    .locals 1

    const-string v0, "USB:([0-9]*)-([0-9]*)"

    .line 160
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 161
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 163
    invoke-virtual {p1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_0
    const/4 p1, -0x1

    return p1
.end method

.method private getUniqueIdentifier(Lcom/starmicronics/stario/PortInfo;Lcom/starmicronics/stario/StarIOPort;)Ljava/lang/String;
    .locals 1

    .line 149
    invoke-virtual {p1}, Lcom/starmicronics/stario/PortInfo;->getUSBSerialNumber()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "SN:null"

    .line 150
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    .line 155
    :cond_0
    invoke-virtual {p2}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/StarMicronicsUsbScout;->getDeviceAddressFromPortName(Ljava/lang/String;)I

    move-result p1

    .line 156
    iget-object p2, p0, Lcom/squareup/print/StarMicronicsUsbScout;->usbPortMapper:Lcom/squareup/usb/UsbPortMapper;

    invoke-interface {p2, p1}, Lcom/squareup/usb/UsbPortMapper;->getStableId(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$mJw-b_nAGTq9gDcHGDwKddaKpyU(Lcom/squareup/print/StarMicronicsUsbScout;Lcom/squareup/hardware/UsbDevicesChangedEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/print/StarMicronicsUsbScout;->onUsbUpdated(Lcom/squareup/hardware/UsbDevicesChangedEvent;)V

    return-void
.end method

.method private onUsbUpdated(Lcom/squareup/hardware/UsbDevicesChangedEvent;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 76
    const-class v1, Lcom/squareup/hardware/UsbDevicesChangedEvent;

    .line 77
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p1, Lcom/squareup/hardware/UsbDevicesChangedEvent;->attached:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/print/StarMicronicsUsbScout;->started:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "Received %s, with attached: %s! Scout running: %s"

    .line 76
    invoke-virtual {p0, v1, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-boolean v0, p0, Lcom/squareup/print/StarMicronicsUsbScout;->started:Z

    if-nez v0, :cond_0

    iget-boolean p1, p1, Lcom/squareup/hardware/UsbDevicesChangedEvent;->attached:Z

    if-nez p1, :cond_1

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsUsbScout;->postScout()V

    :cond_1
    return-void
.end method


# virtual methods
.method protected getConnectionType()Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    return-object v0
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 72
    const-class v0, Lcom/squareup/hardware/UsbDevicesChangedEvent;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/print/-$$Lambda$StarMicronicsUsbScout$mJw-b_nAGTq9gDcHGDwKddaKpyU;

    invoke-direct {v0, p0}, Lcom/squareup/print/-$$Lambda$StarMicronicsUsbScout$mJw-b_nAGTq9gDcHGDwKddaKpyU;-><init>(Lcom/squareup/print/StarMicronicsUsbScout;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected scout()Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    const-string v2, "[Scouting] Port unable to be closed cleanly."

    const-string v3, "[Scouting] Port closed cleanly."

    const-string v4, "[Scouting] Port was never opened."

    const/4 v5, 0x0

    new-array v0, v5, [Ljava/lang/Object;

    const-string v6, "[Scouting] Starting!"

    .line 91
    invoke-virtual {v1, v6, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 93
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 99
    iget-object v0, v1, Lcom/squareup/print/StarMicronicsUsbScout;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    invoke-interface {v0}, Lcom/squareup/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    new-array v0, v5, [Ljava/lang/Object;

    const-string v2, "[Scouting] Stopped! UsbManager reports 0 connected devices."

    .line 100
    invoke-virtual {v1, v2, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v6

    :cond_0
    const/4 v7, 0x1

    :try_start_0
    const-string v0, "USB:"

    .line 106
    iget-object v8, v1, Lcom/squareup/print/StarMicronicsUsbScout;->context:Landroid/content/Context;

    invoke-static {v0, v8}, Lcom/starmicronics/stario/StarIOPort;->searchPrinter(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_4

    new-array v8, v7, [Ljava/lang/Object;

    .line 111
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    const-string v9, "[Scouting] Found %d ports."

    invoke-virtual {v1, v9, v8}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/starmicronics/stario/PortInfo;

    const/4 v9, 0x0

    .line 115
    :try_start_1
    invoke-virtual {v0}, Lcom/starmicronics/stario/PortInfo;->getPortName()Ljava/lang/String;

    move-result-object v10

    const-string v11, ""

    const/16 v12, 0x3e8

    iget-object v13, v1, Lcom/squareup/print/StarMicronicsUsbScout;->context:Landroid/content/Context;

    invoke-static {v10, v11, v12, v13}, Lcom/starmicronics/stario/StarIOPort;->getPort(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v9

    .line 116
    invoke-virtual {v9}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object v15

    .line 117
    invoke-virtual {v9}, Lcom/starmicronics/stario/StarIOPort;->getFirmwareInformation()Ljava/util/Map;

    move-result-object v10

    const-string v11, "ModelName"

    .line 118
    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Lcom/squareup/print/StarMicronicsPrinter;->getCleanModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 119
    invoke-static {v15}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-static {v12}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 120
    invoke-direct {v1, v0, v9}, Lcom/squareup/print/StarMicronicsUsbScout;->getUniqueIdentifier(Lcom/starmicronics/stario/PortInfo;Lcom/starmicronics/stario/StarIOPort;)Ljava/lang/String;

    move-result-object v14

    .line 121
    iget-object v10, v1, Lcom/squareup/print/StarMicronicsUsbScout;->starMicronicsPrinterFactory:Lcom/squareup/print/StarMicronicsPrinter$Factory;

    const-string v11, "Star"

    sget-object v13, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    iget-object v0, v1, Lcom/squareup/print/StarMicronicsUsbScout;->context:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v10 .. v16}, Lcom/squareup/print/StarMicronicsPrinter$Factory;->create(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/print/StarMicronicsPrinter;

    move-result-object v0

    .line 123
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v10, "[Scouting] Printer found! Printer ID: %s"

    new-array v11, v7, [Ljava/lang/Object;

    .line 124
    invoke-virtual {v0}, Lcom/squareup/print/StarMicronicsPrinter;->getId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v5

    invoke-virtual {v1, v10, v11}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    const-string v0, "[Scouting] Printer rejected! portName: %s model: %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v15, v10, v5

    aput-object v12, v10, v7

    .line 126
    invoke-virtual {v1, v0, v10}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v9, :cond_2

    .line 133
    :try_start_2
    invoke-static {v9}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    new-array v0, v5, [Ljava/lang/Object;

    .line 134
    invoke-virtual {v1, v3, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    new-array v0, v5, [Ljava/lang/Object;

    .line 136
    invoke-virtual {v1, v2, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-array v0, v5, [Ljava/lang/Object;

    .line 139
    invoke-virtual {v1, v4, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_3
    const-string v10, "[Scouting] Exception opening port: %s"

    new-array v11, v7, [Ljava/lang/Object;

    .line 129
    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v5

    invoke-virtual {v1, v10, v11}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v9, :cond_3

    .line 133
    :try_start_4
    invoke-static {v9}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    new-array v0, v5, [Ljava/lang/Object;

    .line 134
    invoke-virtual {v1, v3, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    :catch_2
    new-array v0, v5, [Ljava/lang/Object;

    .line 136
    invoke-virtual {v1, v2, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    new-array v0, v5, [Ljava/lang/Object;

    .line 139
    invoke-virtual {v1, v4, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :goto_2
    if-eqz v9, :cond_4

    .line 133
    :try_start_5
    invoke-static {v9}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V

    new-array v4, v5, [Ljava/lang/Object;

    .line 134
    invoke-virtual {v1, v3, v4}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    new-array v3, v5, [Ljava/lang/Object;

    .line 136
    invoke-virtual {v1, v2, v3}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    new-array v2, v5, [Ljava/lang/Object;

    .line 139
    invoke-virtual {v1, v4, v2}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    :goto_3
    throw v0

    :cond_5
    new-array v0, v5, [Ljava/lang/Object;

    const-string v2, "[Scouting] Stopping!"

    .line 143
    invoke-virtual {v1, v2, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v6

    :catch_4
    move-exception v0

    new-array v2, v7, [Ljava/lang/Object;

    .line 108
    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    const-string v0, "[Scouting] Exception while searching for printers: %s"

    invoke-virtual {v1, v0, v2}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v6
.end method

.method public start()V
    .locals 2

    .line 49
    iget-boolean v0, p0, Lcom/squareup/print/StarMicronicsUsbScout;->started:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Scout was asked to start, even though scout was already started."

    .line 50
    invoke-virtual {p0, v1, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Starting!"

    .line 52
    invoke-virtual {p0, v1, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x1

    .line 55
    iput-boolean v0, p0, Lcom/squareup/print/StarMicronicsUsbScout;->started:Z

    .line 57
    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsUsbScout;->postScout()V

    return-void
.end method

.method public stop()V
    .locals 3

    .line 61
    iget-boolean v0, p0, Lcom/squareup/print/StarMicronicsUsbScout;->started:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "Stopping!"

    .line 62
    invoke-virtual {p0, v2, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "Scout was asked to stop, but was not started."

    .line 64
    invoke-virtual {p0, v2, v0}, Lcom/squareup/print/StarMicronicsUsbScout;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :goto_0
    iput-boolean v1, p0, Lcom/squareup/print/StarMicronicsUsbScout;->started:Z

    .line 68
    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsUsbScout;->cancelPendingScouting()V

    return-void
.end method
