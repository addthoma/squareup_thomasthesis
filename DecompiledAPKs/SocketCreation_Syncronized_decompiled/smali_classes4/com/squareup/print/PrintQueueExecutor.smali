.class interface abstract Lcom/squareup/print/PrintQueueExecutor;
.super Ljava/lang/Object;
.source "PrintQueueExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintQueueExecutor$PrintWork;,
        Lcom/squareup/print/PrintQueueExecutor$PrintTask;
    }
.end annotation


# virtual methods
.method public abstract execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/print/PrintQueueExecutor$PrintTask<",
            "TT;>;",
            "Lcom/squareup/print/PrintCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract execute(Lcom/squareup/print/PrintQueueExecutor$PrintWork;)V
.end method

.method public abstract execute(Lcom/squareup/print/PrintQueueExecutor$PrintWork;Ljava/lang/Runnable;)V
.end method
