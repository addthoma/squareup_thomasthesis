.class public final Lcom/squareup/print/PrintTimingData;
.super Ljava/lang/Object;
.source "PrintTimingData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintTimingData$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintTimingData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintTimingData.kt\ncom/squareup/print/PrintTimingData\n*L\n1#1,104:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010!\n\u0002\u0008\u0012\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 ,2\u00020\u0001:\u0001,B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002BI\u0012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0004\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0004\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0004\u0012\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0004H\u00c2\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0006H\u00c2\u0003J\t\u0010\u0019\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0004H\u00c6\u0003J\u000f\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000bH\u00c6\u0003J\u000e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 JM\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0002\u0010\t\u001a\u00020\u00042\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000bH\u00c6\u0001J\u000e\u0010\"\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\u0013\u0010#\u001a\u00020$2\u0008\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u000e\u0010&\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\u000e\u0010)\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\t\u0010*\u001a\u00020+H\u00d6\u0001R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u0008\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010\"\u0004\u0008\u0014\u0010\u0012R\u001a\u0010\u0007\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0010\"\u0004\u0008\u0016\u0010\u0012\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/print/PrintTimingData;",
        "",
        "()V",
        "lastEventTimestampMs",
        "",
        "lastEvent",
        "Lcom/squareup/print/Event;",
        "totalFromEnqueuedToFinishedMs",
        "enqueuedToConnectAttemptMs",
        "sentDataToFinishedMs",
        "connectAttemptIntervalsMs",
        "",
        "(JLcom/squareup/print/Event;JJJLjava/util/List;)V",
        "getConnectAttemptIntervalsMs",
        "()Ljava/util/List;",
        "getEnqueuedToConnectAttemptMs",
        "()J",
        "setEnqueuedToConnectAttemptMs",
        "(J)V",
        "getSentDataToFinishedMs",
        "setSentDataToFinishedMs",
        "getTotalFromEnqueuedToFinishedMs",
        "setTotalFromEnqueuedToFinishedMs",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "connecting",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "copy",
        "enqueuing",
        "equals",
        "",
        "other",
        "finished",
        "hashCode",
        "",
        "sendingDataToPrinter",
        "toString",
        "",
        "Companion",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/print/PrintTimingData$Companion;

.field private static final SKIPPED_CONNECTION:J = -0x1L


# instance fields
.field private final connectAttemptIntervalsMs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private enqueuedToConnectAttemptMs:J

.field private lastEvent:Lcom/squareup/print/Event;

.field private lastEventTimestampMs:J

.field private sentDataToFinishedMs:J

.field private totalFromEnqueuedToFinishedMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/print/PrintTimingData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/print/PrintTimingData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/print/PrintTimingData;->Companion:Lcom/squareup/print/PrintTimingData$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 12

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrintTimingData;-><init>(JLcom/squareup/print/Event;JJJLjava/util/List;)V

    return-void
.end method

.method public constructor <init>(JLcom/squareup/print/Event;JJJLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/print/Event;",
            "JJJ",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const-string v0, "connectAttemptIntervalsMs"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    iput-object p3, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    iput-wide p4, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    iput-wide p6, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    iput-wide p8, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    iput-object p10, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(JLcom/squareup/print/Event;JJJLjava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p11, 0x1

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    move-wide v3, v1

    goto :goto_0

    :cond_0
    move-wide v3, p1

    :goto_0
    and-int/lit8 v0, p11, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 17
    check-cast v0, Lcom/squareup/print/Event;

    goto :goto_1

    :cond_1
    move-object v0, p3

    :goto_1
    and-int/lit8 v5, p11, 0x4

    if-eqz v5, :cond_2

    move-wide v5, v1

    goto :goto_2

    :cond_2
    move-wide v5, p4

    :goto_2
    and-int/lit8 v7, p11, 0x8

    if-eqz v7, :cond_3

    move-wide v7, v1

    goto :goto_3

    :cond_3
    move-wide/from16 v7, p6

    :goto_3
    and-int/lit8 v9, p11, 0x10

    if-eqz v9, :cond_4

    goto :goto_4

    :cond_4
    move-wide/from16 v1, p8

    :goto_4
    and-int/lit8 v9, p11, 0x20

    if-eqz v9, :cond_5

    .line 21
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    check-cast v9, Ljava/util/List;

    goto :goto_5

    :cond_5
    move-object/from16 v9, p10

    :goto_5
    move-object p1, p0

    move-wide p2, v3

    move-object p4, v0

    move-wide p5, v5

    move-wide/from16 p7, v7

    move-wide/from16 p9, v1

    move-object/from16 p11, v9

    invoke-direct/range {p1 .. p11}, Lcom/squareup/print/PrintTimingData;-><init>(JLcom/squareup/print/Event;JJJLjava/util/List;)V

    return-void
.end method

.method private final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    return-wide v0
.end method

.method private final component2()Lcom/squareup/print/Event;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/print/PrintTimingData;JLcom/squareup/print/Event;JJJLjava/util/List;ILjava/lang/Object;)Lcom/squareup/print/PrintTimingData;
    .locals 11

    move-object v0, p0

    and-int/lit8 v1, p11, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p11, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    goto :goto_1

    :cond_1
    move-object v3, p3

    :goto_1
    and-int/lit8 v4, p11, 0x4

    if-eqz v4, :cond_2

    iget-wide v4, v0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    goto :goto_2

    :cond_2
    move-wide v4, p4

    :goto_2
    and-int/lit8 v6, p11, 0x8

    if-eqz v6, :cond_3

    iget-wide v6, v0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    goto :goto_3

    :cond_3
    move-wide/from16 v6, p6

    :goto_3
    and-int/lit8 v8, p11, 0x10

    if-eqz v8, :cond_4

    iget-wide v8, v0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    goto :goto_4

    :cond_4
    move-wide/from16 v8, p8

    :goto_4
    and-int/lit8 v10, p11, 0x20

    if-eqz v10, :cond_5

    iget-object v10, v0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    goto :goto_5

    :cond_5
    move-object/from16 v10, p10

    :goto_5
    move-wide p1, v1

    move-object p3, v3

    move-wide p4, v4

    move-wide/from16 p6, v6

    move-wide/from16 p8, v8

    move-object/from16 p10, v10

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/print/PrintTimingData;->copy(JLcom/squareup/print/Event;JJJLjava/util/List;)Lcom/squareup/print/PrintTimingData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    return-wide v0
.end method

.method public final component5()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    return-wide v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    return-object v0
.end method

.method public final connecting(Lcom/squareup/util/Clock;)V
    .locals 4

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 38
    iget-wide v2, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    sub-long v2, v0, v2

    .line 39
    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    .line 40
    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    .line 42
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/print/PrintTimingData$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/print/Event;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 45
    :goto_0
    new-instance p1, Lcom/squareup/print/UnexpectedTimingEvent;

    invoke-direct {p1}, Lcom/squareup/print/UnexpectedTimingEvent;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected connecting timing event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 44
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 43
    :cond_2
    iput-wide v2, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    .line 47
    :goto_1
    sget-object p1, Lcom/squareup/print/Event;->CONNECTING:Lcom/squareup/print/Event;

    iput-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    return-void
.end method

.method public final copy(JLcom/squareup/print/Event;JJJLjava/util/List;)Lcom/squareup/print/PrintTimingData;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/print/Event;",
            "JJJ",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/squareup/print/PrintTimingData;"
        }
    .end annotation

    const-string v0, "connectAttemptIntervalsMs"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/print/PrintTimingData;

    move-object v1, v0

    move-wide v2, p1

    move-object v4, p3

    move-wide/from16 v5, p4

    move-wide/from16 v7, p6

    move-wide/from16 v9, p8

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrintTimingData;-><init>(JLcom/squareup/print/Event;JJJLjava/util/List;)V

    return-object v0
.end method

.method public final enqueuing(Lcom/squareup/util/Clock;)V
    .locals 2

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    .line 29
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    if-nez p1, :cond_0

    const-wide/16 v0, 0x0

    .line 30
    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    goto :goto_0

    .line 31
    :cond_0
    new-instance p1, Lcom/squareup/print/UnexpectedTimingEvent;

    invoke-direct {p1}, Lcom/squareup/print/UnexpectedTimingEvent;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected enqueue timing event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 33
    :goto_0
    sget-object p1, Lcom/squareup/print/Event;->ENQUEUEING:Lcom/squareup/print/Event;

    iput-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/print/PrintTimingData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/print/PrintTimingData;

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    iget-wide v2, p1, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    iget-object v1, p1, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    iget-wide v2, p1, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    iget-wide v2, p1, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    iget-wide v2, p1, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final finished(Lcom/squareup/util/Clock;)V
    .locals 4

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 70
    iget-wide v2, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    sub-long v2, v0, v2

    .line 71
    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    .line 72
    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    .line 74
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/print/PrintTimingData$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p1}, Lcom/squareup/print/Event;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    .line 80
    :goto_0
    new-instance p1, Lcom/squareup/print/UnexpectedTimingEvent;

    invoke-direct {p1}, Lcom/squareup/print/UnexpectedTimingEvent;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected finished timing event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 76
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 75
    :cond_2
    iput-wide v2, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    .line 83
    :cond_3
    :goto_1
    sget-object p1, Lcom/squareup/print/Event;->FINISHED:Lcom/squareup/print/Event;

    iput-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    return-void
.end method

.method public final getConnectAttemptIntervalsMs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    return-object v0
.end method

.method public final getEnqueuedToConnectAttemptMs()J
    .locals 2

    .line 19
    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    return-wide v0
.end method

.method public final getSentDataToFinishedMs()J
    .locals 2

    .line 20
    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    return-wide v0
.end method

.method public final getTotalFromEnqueuedToFinishedMs()J
    .locals 2

    .line 18
    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    invoke-static {v3, v4}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    invoke-static {v3, v4}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    invoke-static {v3, v4}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public final sendingDataToPrinter(Lcom/squareup/util/Clock;)V
    .locals 4

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 52
    iget-wide v2, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    sub-long v2, v0, v2

    .line 53
    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    .line 54
    iget-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    .line 56
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/print/PrintTimingData$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/print/Event;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 62
    :goto_0
    new-instance p1, Lcom/squareup/print/UnexpectedTimingEvent;

    invoke-direct {p1}, Lcom/squareup/print/UnexpectedTimingEvent;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected sending data to printer timing event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 60
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-wide/16 v0, -0x1

    .line 59
    iput-wide v0, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    .line 65
    :goto_1
    sget-object p1, Lcom/squareup/print/Event;->SENDING_DATA:Lcom/squareup/print/Event;

    iput-object p1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    return-void
.end method

.method public final setEnqueuedToConnectAttemptMs(J)V
    .locals 0

    .line 19
    iput-wide p1, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    return-void
.end method

.method public final setSentDataToFinishedMs(J)V
    .locals 0

    .line 20
    iput-wide p1, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    return-void
.end method

.method public final setTotalFromEnqueuedToFinishedMs(J)V
    .locals 0

    .line 18
    iput-wide p1, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrintTimingData(lastEventTimestampMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/print/PrintTimingData;->lastEventTimestampMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", lastEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/PrintTimingData;->lastEvent:Lcom/squareup/print/Event;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalFromEnqueuedToFinishedMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/print/PrintTimingData;->totalFromEnqueuedToFinishedMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", enqueuedToConnectAttemptMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/print/PrintTimingData;->enqueuedToConnectAttemptMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", sentDataToFinishedMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/print/PrintTimingData;->sentDataToFinishedMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", connectAttemptIntervalsMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/PrintTimingData;->connectAttemptIntervalsMs:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
