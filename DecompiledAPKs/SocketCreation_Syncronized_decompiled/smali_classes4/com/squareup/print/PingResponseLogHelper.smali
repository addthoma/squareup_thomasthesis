.class Lcom/squareup/print/PingResponseLogHelper;
.super Ljava/lang/Object;
.source "PingResponseLogHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PingResponseLogHelper$PingReport;,
        Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;
    }
.end annotation


# instance fields
.field private final bufferCapacity:I

.field private bufferHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/util/Clock;

.field private final simpleDateFormat:Ljava/text/SimpleDateFormat;


# direct methods
.method constructor <init>(Lcom/squareup/util/Clock;I)V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/print/PingResponseLogHelper;->clock:Lcom/squareup/util/Clock;

    .line 47
    iput p2, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferCapacity:I

    .line 48
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget-object p2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v0, "HH:mm:ss"

    invoke-direct {p1, v0, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/print/PingResponseLogHelper;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    .line 49
    iget-object p1, p0, Lcom/squareup/print/PingResponseLogHelper;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    const-string p2, "UTC"

    invoke-static {p2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 50
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/print/PingResponseLogHelper;)Lcom/squareup/util/Clock;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/print/PingResponseLogHelper;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/print/PingResponseLogHelper;)Ljava/text/SimpleDateFormat;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/print/PingResponseLogHelper;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    return-object p0
.end method


# virtual methods
.method pingResponseTimedOut()V
    .locals 3

    .line 68
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 69
    iget-object v2, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->addPingResponse(Ljava/lang/Long;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method recordPingResponse(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 3

    .line 59
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;

    iget v2, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferCapacity:I

    invoke-direct {v1, p0, v2}, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;-><init>(Lcom/squareup/print/PingResponseLogHelper;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;

    invoke-virtual {p1, p2}, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->addPingResponse(Ljava/lang/Long;)V

    return-void
.end method

.method removePrinter(Ljava/lang/String;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method stringify(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/print/PingResponseLogHelper;->bufferHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;

    invoke-virtual {p1}, Lcom/squareup/print/PingResponseLogHelper$PingResponseRingBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
