.class public final Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;
.super Ljava/lang/Object;
.source "RegisterPrintModule_ProvideSettingFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/GsonLocalSettingFactory<",
        "Lcom/squareup/print/PrinterStationConfiguration;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;->preferencesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSettingFactory(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;)Lcom/squareup/settings/GsonLocalSettingFactory;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/GsonLocalSettingFactory<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/print/RegisterPrintModule;->provideSettingFactory(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;)Lcom/squareup/settings/GsonLocalSettingFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/GsonLocalSettingFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/GsonLocalSettingFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/GsonLocalSettingFactory<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    invoke-static {v0, v1}, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;->provideSettingFactory(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;)Lcom/squareup/settings/GsonLocalSettingFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/print/RegisterPrintModule_ProvideSettingFactoryFactory;->get()Lcom/squareup/settings/GsonLocalSettingFactory;

    move-result-object v0

    return-object v0
.end method
