.class public interface abstract Lcom/squareup/print/OrderTicketPrintingSettings;
.super Ljava/lang/Object;
.source "OrderTicketPrintingSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/OrderTicketPrintingSettings$OrderTicketPrintingEnabled;,
        Lcom/squareup/print/OrderTicketPrintingSettings$OrderTicketPrintingDisabled;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0002\u0004\u0005J\u0008\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/print/OrderTicketPrintingSettings;",
        "",
        "enabled",
        "",
        "OrderTicketPrintingDisabled",
        "OrderTicketPrintingEnabled",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract enabled()Z
.end method
