.class public Lcom/squareup/print/sections/ShiftSection;
.super Ljava/lang/Object;
.source "ShiftSection.java"


# instance fields
.field public final clockinText:Ljava/lang/String;

.field public final clockoutText:Ljava/lang/String;

.field public final startTime:Ljava/lang/String;

.field public final stopTime:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final totalHours:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/print/sections/ShiftSection;->title:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/squareup/print/sections/ShiftSection;->totalHours:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/squareup/print/sections/ShiftSection;->clockinText:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/squareup/print/sections/ShiftSection;->clockoutText:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/squareup/print/sections/ShiftSection;->startTime:Ljava/lang/String;

    .line 30
    iput-object p6, p0, Lcom/squareup/print/sections/ShiftSection;->stopTime:Ljava/lang/String;

    return-void
.end method

.method private static formatDate(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 4

    .line 90
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "M/d/yy"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 93
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p0, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static formatTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 4

    .line 83
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "h:mm a"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 86
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p0, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static fromSummary(Lcom/squareup/util/Res;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;Ljava/lang/Integer;)Lcom/squareup/print/sections/ShiftSection;
    .locals 7

    .line 37
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v0, v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    iget-object p2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object p2, p2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 40
    sget v0, Lcom/squareup/print/R$string;->timecards_shift_default_title:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 41
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    const-string v1, "shift_number"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 42
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 43
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    move-object v1, p2

    .line 46
    iget-object p2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    .line 47
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    .line 49
    invoke-static {p2}, Lcom/squareup/print/sections/ShiftSection;->formatTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-static {v0}, Lcom/squareup/print/sections/ShiftSection;->formatTime(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v3

    .line 51
    invoke-static {p2}, Lcom/squareup/print/sections/ShiftSection;->formatDate(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p2

    .line 52
    invoke-static {v0}, Lcom/squareup/print/sections/ShiftSection;->formatDate(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 p2, 0x1

    aput-object v2, v5, p2

    const-string v2, "%s  %s"

    .line 55
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    aput-object v3, v4, p2

    .line 56
    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    move-object v6, p2

    goto :goto_1

    :cond_2
    move-object v5, v2

    move-object v6, v3

    .line 59
    :goto_1
    new-instance p2, Lcom/squareup/print/sections/ShiftSection;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->paid_seconds:Ljava/lang/Long;

    .line 61
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/squareup/print/sections/EmployeeSummarySection;->formatTotalHours(Lcom/squareup/util/Res;J)Ljava/lang/String;

    move-result-object v2

    sget p1, Lcom/squareup/print/R$string;->timecards_clock_in:I

    .line 62
    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget p1, Lcom/squareup/print/R$string;->timecards_clock_out:I

    .line 63
    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/sections/ShiftSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 2

    .line 69
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 71
    iget-object v0, p0, Lcom/squareup/print/sections/ShiftSection;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 72
    iget-object v1, p0, Lcom/squareup/print/sections/ShiftSection;->totalHours:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapseBigMediumText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 73
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/sections/ShiftSection;->clockinText:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/print/sections/ShiftSection;->startTime:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapseBigText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 77
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 78
    iget-object v0, p0, Lcom/squareup/print/sections/ShiftSection;->clockoutText:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/print/sections/ShiftSection;->stopTime:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapseBigText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
