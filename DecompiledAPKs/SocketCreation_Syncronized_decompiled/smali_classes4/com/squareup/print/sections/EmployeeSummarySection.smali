.class public Lcom/squareup/print/sections/EmployeeSummarySection;
.super Ljava/lang/Object;
.source "EmployeeSummarySection.java"


# static fields
.field private static final DECIMAL_FORMAT_ROUND_TO_TWO_DECIMALS:Ljava/text/DecimalFormat;

.field private static final ONE_HOUR_SECONDS:I = 0xe10


# instance fields
.field public final date:Ljava/lang/String;

.field public final employeeName:Ljava/lang/String;

.field public final location:Ljava/lang/String;

.field public final logoUrl:Ljava/lang/String;

.field public final totalHours:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/print/sections/EmployeeSummarySection;->DECIMAL_FORMAT_ROUND_TO_TWO_DECIMALS:Ljava/text/DecimalFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->employeeName:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->totalHours:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->logoUrl:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->date:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->location:Ljava/lang/String;

    return-void
.end method

.method private static formatDate(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 76
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const/4 v0, 0x0

    .line 80
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    iget-object v1, v1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    .line 81
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    iget-object p0, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    .line 82
    iget-object v2, v1, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 84
    new-instance v4, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    const-string v6, "MMM d, yyyy"

    invoke-direct {v4, v6, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 85
    invoke-virtual {v4, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 87
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, v1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 88
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p0, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v4, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 90
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v0

    aput-object p0, v2, v3

    const-string p0, "%s - %s"

    .line 94
    invoke-static {p0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatTotalHours(Lcom/squareup/util/Res;J)Ljava/lang/String;
    .locals 2

    long-to-double p1, p1

    const-wide v0, 0x40ac200000000000L    # 3600.0

    div-double/2addr p1, v0

    .line 68
    sget-object v0, Lcom/squareup/print/sections/EmployeeSummarySection;->DECIMAL_FORMAT_ROUND_TO_TWO_DECIMALS:Ljava/text/DecimalFormat;

    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    .line 69
    sget p2, Lcom/squareup/print/R$string;->timecards_total_hours:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p2, "total_hours"

    .line 70
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 72
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static fromEmployeeAndSummary(Lcom/squareup/util/Res;Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/EmployeeSummarySection;
    .locals 8

    .line 42
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p1

    .line 43
    iget-object v0, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/squareup/print/sections/EmployeeSummarySection;->formatTotalHours(Lcom/squareup/util/Res;J)Ljava/lang/String;

    move-result-object v4

    .line 44
    iget-object p2, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-static {p2}, Lcom/squareup/print/sections/EmployeeSummarySection;->formatDate(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    .line 46
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getPrintedReceiptImageUrl()Ljava/lang/String;

    move-result-object v5

    .line 47
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v7

    .line 49
    new-instance p2, Lcom/squareup/print/sections/EmployeeSummarySection;

    .line 50
    invoke-virtual {p1, p0}, Lcom/squareup/permissions/EmployeeInfo;->getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    move-object v2, p2

    invoke-direct/range {v2 .. v7}, Lcom/squareup/print/sections/EmployeeSummarySection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->logoUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 55
    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->MEDIUM:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->logo(Ljava/lang/String;Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 60
    :goto_0
    iget-object v0, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->employeeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthBigMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 61
    iget-object v0, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->totalHours:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->bigHeaderSingleLine(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 62
    iget-object v0, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->date:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/print/sections/EmployeeSummarySection;->location:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapseLeftTextBigMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 63
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
