.class public Lcom/squareup/print/sections/PrintedAtSection;
.super Ljava/lang/Object;
.source "PrintedAtSection.java"


# instance fields
.field public final text:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/print/sections/PrintedAtSection;->text:Ljava/lang/String;

    return-void
.end method

.method public static fromClock(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)Lcom/squareup/print/sections/PrintedAtSection;
    .locals 6

    .line 19
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    .line 20
    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    .line 22
    new-instance p1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "M/d/yy"

    invoke-direct {p1, v4, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 23
    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "h:mm a"

    invoke-direct {v3, v5, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 24
    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 25
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 27
    sget v0, Lcom/squareup/print/R$string;->timecards_print_timestamp:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 28
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "date"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 29
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "time"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 30
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 31
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    .line 33
    new-instance p1, Lcom/squareup/print/sections/PrintedAtSection;

    invoke-direct {p1, p0}, Lcom/squareup/print/sections/PrintedAtSection;-><init>(Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/print/sections/PrintedAtSection;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthCenteredSmallText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
