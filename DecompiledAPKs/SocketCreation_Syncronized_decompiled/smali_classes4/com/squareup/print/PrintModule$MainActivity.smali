.class public abstract Lcom/squareup/print/PrintModule$MainActivity;
.super Ljava/lang/Object;
.source "PrintModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MainActivity"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static scopeHardwarePrinterTrackerListeners(Lcom/squareup/print/HardwarePrinterTracker;Ljava/util/Set;)Lmortar/Scoped;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
            ">;)",
            "Lmortar/Scoped;"
        }
    .end annotation

    .line 153
    new-instance v0, Lcom/squareup/print/PrintModule$MainActivity$1;

    invoke-direct {v0, p1, p0}, Lcom/squareup/print/PrintModule$MainActivity$1;-><init>(Ljava/util/Set;Lcom/squareup/print/HardwarePrinterTracker;)V

    return-object v0
.end method


# virtual methods
.method abstract provideHardwarePrinterHudToasterListener(Lcom/squareup/print/HardwarePrinterHudToaster;)Lcom/squareup/print/HardwarePrinterTracker$Listener;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideHardwarePrinterRenameHandlerListener(Lcom/squareup/print/HardwarePrinterRenameHandler;)Lcom/squareup/print/HardwarePrinterTracker$Listener;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
