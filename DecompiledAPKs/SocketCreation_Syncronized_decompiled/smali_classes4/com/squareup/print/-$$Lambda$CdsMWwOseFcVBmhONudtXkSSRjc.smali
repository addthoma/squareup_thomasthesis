.class public final synthetic Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/print/PrintQueueExecutor$PrintTask;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;

    invoke-direct {v0}, Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;-><init>()V

    sput-object v0, Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;->INSTANCE:Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/print/PrintJobQueue;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1}, Lcom/squareup/print/PrintJobQueue;->retrieveHeadPrintJobPerPrintTarget()Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
