.class public final Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;
.super Ljava/lang/Object;
.source "PrintablePaymentOrder.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrderItemModifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintablePaymentOrder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PrintablePaymentOrderItemModifier"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;",
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "orderModifier",
        "Lcom/squareup/checkout/OrderModifier;",
        "(Lcom/squareup/checkout/OrderModifier;)V",
        "basePriceTimesModifierQuantity",
        "Lcom/squareup/protos/common/Money;",
        "getBasePriceTimesModifierQuantity",
        "()Lcom/squareup/protos/common/Money;",
        "hideFromCustomer",
        "",
        "getHideFromCustomer",
        "()Z",
        "getOrderModifier",
        "()Lcom/squareup/checkout/OrderModifier;",
        "getDisplayName",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final basePriceTimesModifierQuantity:Lcom/squareup/protos/common/Money;

.field private final hideFromCustomer:Z

.field private final orderModifier:Lcom/squareup/checkout/OrderModifier;


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/OrderModifier;)V
    .locals 1

    const-string v0, "orderModifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->orderModifier:Lcom/squareup/checkout/OrderModifier;

    .line 104
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->orderModifier:Lcom/squareup/checkout/OrderModifier;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifier;->getHideFromCustomer()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->hideFromCustomer:Z

    .line 107
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->orderModifier:Lcom/squareup/checkout/OrderModifier;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->basePriceTimesModifierQuantity:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->basePriceTimesModifierQuantity:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->orderModifier:Lcom/squareup/checkout/OrderModifier;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getHideFromCustomer()Z
    .locals 1

    .line 104
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->hideFromCustomer:Z

    return v0
.end method

.method public final getOrderModifier()Lcom/squareup/checkout/OrderModifier;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;->orderModifier:Lcom/squareup/checkout/OrderModifier;

    return-object v0
.end method
