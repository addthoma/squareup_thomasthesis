.class public Lcom/squareup/print/RealPrinterStationFactory;
.super Ljava/lang/Object;
.source "RealPrinterStationFactory.java"

# interfaces
.implements Lcom/squareup/print/PrinterStationFactory;


# instance fields
.field private final settingsFactory:Lcom/squareup/settings/GsonLocalSettingFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/GsonLocalSettingFactory<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/GsonLocalSettingFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/GsonLocalSettingFactory<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/print/RealPrinterStationFactory;->settingsFactory:Lcom/squareup/settings/GsonLocalSettingFactory;

    return-void
.end method


# virtual methods
.method public generate(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;
    .locals 3

    .line 18
    new-instance v0, Lcom/squareup/print/RealPrinterStation;

    iget-object v1, p0, Lcom/squareup/print/RealPrinterStationFactory;->settingsFactory:Lcom/squareup/settings/GsonLocalSettingFactory;

    sget-object v2, Lcom/squareup/print/RealPrinterStation;->EMPTY_CONFIGURATION:Lcom/squareup/print/PrinterStationConfiguration;

    invoke-virtual {v1, p1, v2}, Lcom/squareup/settings/GsonLocalSettingFactory;->generate(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/print/RealPrinterStation;-><init>(Lcom/squareup/settings/GsonLocalSetting;Ljava/lang/String;)V

    return-object v0
.end method
