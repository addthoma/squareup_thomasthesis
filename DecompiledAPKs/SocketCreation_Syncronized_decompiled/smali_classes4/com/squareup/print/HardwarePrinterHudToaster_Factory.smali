.class public final Lcom/squareup/print/HardwarePrinterHudToaster_Factory;
.super Ljava/lang/Object;
.source "HardwarePrinterHudToaster_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/HardwarePrinterHudToaster;",
        ">;"
    }
.end annotation


# instance fields
.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->resProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/HardwarePrinterHudToaster_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)",
            "Lcom/squareup/print/HardwarePrinterHudToaster_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;)Lcom/squareup/print/HardwarePrinterHudToaster;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/print/HardwarePrinterHudToaster;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/HardwarePrinterHudToaster;-><init>(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/HardwarePrinterHudToaster;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStations;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->newInstance(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;)Lcom/squareup/print/HardwarePrinterHudToaster;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinterHudToaster_Factory;->get()Lcom/squareup/print/HardwarePrinterHudToaster;

    move-result-object v0

    return-object v0
.end method
