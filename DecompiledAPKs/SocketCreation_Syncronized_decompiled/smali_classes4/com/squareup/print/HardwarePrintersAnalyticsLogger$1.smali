.class Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;
.super Ljava/lang/Object;
.source "HardwarePrintersAnalyticsLogger.java"

# interfaces
.implements Lcom/squareup/print/HardwarePrinterTracker$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/HardwarePrintersAnalyticsLogger;-><init>(Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/print/PrinterStations;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/print/HardwarePrintersAnalyticsLogger;

.field final synthetic val$analytics:Lcom/squareup/analytics/Analytics;

.field final synthetic val$features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->this$0:Lcom/squareup/print/HardwarePrintersAnalyticsLogger;

    iput-object p2, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->val$features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public printerConnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->this$0:Lcom/squareup/print/HardwarePrintersAnalyticsLogger;

    .line 32
    invoke-static {v2, p1}, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->access$000(Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/print/HardwarePrinter;)Ljava/util/List;

    move-result-object v2

    .line 31
    invoke-static {v1, v2}, Lcom/squareup/print/PrinterEventKt;->forPrinterConnect(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/util/List;)Lcom/squareup/print/PrinterEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->val$features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 36
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "[Print_HardwarePrinterTracker] Printer connected: %s"

    .line 35
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 3

    .line 41
    iget-object v0, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->this$0:Lcom/squareup/print/HardwarePrintersAnalyticsLogger;

    .line 42
    invoke-static {v2, p1}, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;->access$000(Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/print/HardwarePrinter;)Ljava/util/List;

    move-result-object v2

    .line 41
    invoke-static {v1, v2}, Lcom/squareup/print/PrinterEventKt;->forPrinterDisconnect(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/util/List;)Lcom/squareup/print/PrinterEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/print/HardwarePrintersAnalyticsLogger$1;->val$features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 46
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "[Print_HardwarePrinterTracker] Printer disconnected: %s"

    .line 45
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
