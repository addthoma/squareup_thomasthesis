.class public abstract Lcom/squareup/print/PrintModule;
.super Ljava/lang/Object;
.source "PrintModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintModule$MainActivity;,
        Lcom/squareup/print/PrintModule$Scouts;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideHardwarePrinters(Lcom/google/gson/Gson;Landroid/content/SharedPreferences;Lcom/squareup/analytics/Analytics;)Lcom/squareup/print/HardwarePrinterTracker;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 91
    new-instance v0, Lcom/squareup/print/PrintModule$1;

    invoke-direct {v0}, Lcom/squareup/print/PrintModule$1;-><init>()V

    .line 92
    invoke-virtual {v0}, Lcom/squareup/print/PrintModule$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "hardware-info-cache"

    .line 94
    invoke-static {p1, v1, p0, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    .line 97
    invoke-interface {p0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    .line 98
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 101
    :cond_0
    new-instance p1, Lcom/squareup/print/HardwarePrinterTracker;

    invoke-direct {p1, p0, p2}, Lcom/squareup/print/HardwarePrinterTracker;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;)V

    return-object p1
.end method

.method static providePrintSpooler(Lcom/squareup/print/PayloadRenderer;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/HardwarePrinterExecutor;Lcom/squareup/print/PrintQueueExecutor;Ljava/util/Set;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/BlockedPrinterLogRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)Lcom/squareup/print/PrintSpooler;
    .locals 11
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PayloadRenderer;",
            "Lcom/squareup/print/PrintTargetRouter;",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            "Lcom/squareup/print/HardwarePrinterExecutor;",
            "Lcom/squareup/print/PrintQueueExecutor;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/print/BlockedPrinterLogRunner;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/util/Clock;",
            ")",
            "Lcom/squareup/print/PrintSpooler;"
        }
    .end annotation

    .line 77
    new-instance v10, Lcom/squareup/print/PrintSpooler;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/squareup/print/PrintSpooler;-><init>(Lcom/squareup/print/PayloadRenderer;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrintQueueExecutor;Lcom/squareup/print/HardwarePrinterExecutor;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/BlockedPrinterLogRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)V

    .line 81
    invoke-interface/range {p5 .. p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;

    .line 82
    invoke-virtual {v10, v1}, Lcom/squareup/print/PrintSpooler;->addPrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V

    goto :goto_0

    :cond_0
    return-object v10
.end method

.method static providePrinterCoroutineScope(Lkotlinx/coroutines/CoroutineDispatcher;)Lkotlinx/coroutines/CoroutineScope;
    .locals 0
    .param p0    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/print/PrinterThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 127
    invoke-static {p0}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object p0

    return-object p0
.end method

.method static provideSocketProvider()Lcom/squareup/print/SocketProvider;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 131
    new-instance v0, Lcom/squareup/print/SocketProvider$RealSocketProvider;

    invoke-direct {v0}, Lcom/squareup/print/SocketProvider$RealSocketProvider;-><init>()V

    return-object v0
.end method

.method static provideStarMicronicsTcpScout(Landroid/app/Application;Lcom/squareup/print/SocketProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsTcpScout;
    .locals 11
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 119
    new-instance v10, Lcom/squareup/print/StarMicronicsTcpScout;

    const-string v0, "Sq-STAR MICRONICS TCP SCOUT"

    const/16 v1, 0xa

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/squareup/thread/executor/Executors;->stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v2

    move-object v0, v10

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/squareup/print/StarMicronicsTcpScout;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/print/SocketProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/StarMicronicsPrinter$Factory;)V

    return-object v10
.end method

.method static provideStarMicronicsUsbScout(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/settings/server/Features;Lcom/squareup/usb/UsbPortMapper;Lcom/squareup/print/StarMicronicsPrinter$Factory;)Lcom/squareup/print/StarMicronicsUsbScout;
    .locals 9
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 108
    new-instance v8, Lcom/squareup/print/StarMicronicsUsbScout;

    const-string v0, "Sq-STAR MICRONICS USB SCOUT"

    const/16 v1, 0xa

    const/4 v2, 0x1

    .line 109
    invoke-static {v0, v1, v2}, Lcom/squareup/thread/executor/Executors;->stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v2

    move-object v0, v8

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/StarMicronicsUsbScout;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/settings/server/Features;Lcom/squareup/usb/UsbPortMapper;Lcom/squareup/print/StarMicronicsPrinter$Factory;)V

    return-object v8
.end method

.method static provideStationUuids(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/settings/StringSetPreferenceAdapter;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/settings/StringSetPreferenceAdapter;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 136
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    const-string v1, "printer-station-uuids"

    invoke-virtual {p0, v1, v0, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method
