.class public final enum Lcom/squareup/print/PrinterStation$Role;
.super Ljava/lang/Enum;
.source "PrinterStation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrinterStation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Role"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/PrinterStation$Role;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/PrinterStation$Role;

.field public static final enum RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

.field public static final enum STUBS:Lcom/squareup/print/PrinterStation$Role;

.field public static final enum TICKETS:Lcom/squareup/print/PrinterStation$Role;


# instance fields
.field public final stringResId:I

.field public final stringResIdPlural:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 22
    new-instance v0, Lcom/squareup/print/PrinterStation$Role;

    sget v1, Lcom/squareup/hardware/R$string;->printer_stations_role_receipt:I

    sget v2, Lcom/squareup/hardware/R$string;->printer_stations_role_receipts:I

    const/4 v3, 0x0

    const-string v4, "RECEIPTS"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/print/PrinterStation$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 23
    new-instance v0, Lcom/squareup/print/PrinterStation$Role;

    sget v1, Lcom/squareup/hardware/R$string;->printer_stations_role_ticket:I

    sget v2, Lcom/squareup/hardware/R$string;->printer_stations_role_tickets:I

    const/4 v4, 0x1

    const-string v5, "TICKETS"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/print/PrinterStation$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    .line 24
    new-instance v0, Lcom/squareup/print/PrinterStation$Role;

    sget v1, Lcom/squareup/hardware/R$string;->printer_stations_role_stub:I

    sget v2, Lcom/squareup/hardware/R$string;->printer_stations_role_stubs:I

    const/4 v5, 0x2

    const-string v6, "STUBS"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/print/PrinterStation$Role;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/print/PrinterStation$Role;

    .line 21
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/print/PrinterStation$Role;->$VALUES:[Lcom/squareup/print/PrinterStation$Role;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/squareup/print/PrinterStation$Role;->stringResId:I

    .line 31
    iput p4, p0, Lcom/squareup/print/PrinterStation$Role;->stringResIdPlural:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/PrinterStation$Role;
    .locals 1

    .line 21
    const-class v0, Lcom/squareup/print/PrinterStation$Role;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrinterStation$Role;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/PrinterStation$Role;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->$VALUES:[Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v0}, [Lcom/squareup/print/PrinterStation$Role;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/PrinterStation$Role;

    return-object v0
.end method
