.class public Lcom/squareup/print/PrinterStationConfiguration$Builder;
.super Ljava/lang/Object;
.source "PrinterStationConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrinterStationConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private autoPrintItemizedReceipts:Z

.field private final disabledCategoryIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private internal:Z

.field public name:Ljava/lang/String;

.field private printATicketForEachItem:Z

.field private printCompactTickets:Z

.field private printsUncategorizedItems:Z

.field private selectedHardwarePrinterId:Ljava/lang/String;

.field private final selectedRoles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printsUncategorizedItems:Z

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printATicketForEachItem:Z

    .line 50
    iput-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printCompactTickets:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;ZZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;ZZZZ)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 42
    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printsUncategorizedItems:Z

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printATicketForEachItem:Z

    .line 50
    iput-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printCompactTickets:Z

    .line 60
    iput-object p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 62
    iput-boolean p6, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->autoPrintItemizedReceipts:Z

    .line 63
    iget-object p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    invoke-interface {p1, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 64
    iget-object p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    invoke-interface {p1, p4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 65
    iput-boolean p5, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printsUncategorizedItems:Z

    .line 66
    iput-boolean p7, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printATicketForEachItem:Z

    .line 67
    iput-boolean p8, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->internal:Z

    return-void
.end method


# virtual methods
.method public addDisabledCategoryId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addRole(Lcom/squareup/print/PrinterStation$Role;)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/print/PrinterStationConfiguration;
    .locals 12

    .line 126
    new-instance v11, Lcom/squareup/print/PrinterStationConfiguration;

    iget-object v1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedHardwarePrinterId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    iget-object v4, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    iget-boolean v5, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printsUncategorizedItems:Z

    iget-boolean v6, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->autoPrintItemizedReceipts:Z

    iget-boolean v7, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printATicketForEachItem:Z

    iget-boolean v8, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printCompactTickets:Z

    iget-boolean v9, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->internal:Z

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/print/PrinterStationConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;ZZZZZLcom/squareup/print/PrinterStationConfiguration$1;)V

    return-object v11
.end method

.method public getDisabledCategoryIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedHardwarePrinterId()Ljava/lang/String;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedHardwarePrinterId:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedRoles()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    return-object v0
.end method

.method public isAutoPrintItemizedReceiptsEnabled()Z
    .locals 1

    .line 152
    iget-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->autoPrintItemizedReceipts:Z

    return v0
.end method

.method public isInternal()Z
    .locals 1

    .line 164
    iget-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->internal:Z

    return v0
.end method

.method public isPrintATicketForEachItemEnabled()Z
    .locals 1

    .line 156
    iget-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printATicketForEachItem:Z

    return v0
.end method

.method public isPrintCompactTickets()Z
    .locals 1

    .line 160
    iget-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printCompactTickets:Z

    return v0
.end method

.method public isPrintsUncategorizedItemsEnabled()Z
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printsUncategorizedItems:Z

    return v0
.end method

.method public removeDisabledCategoryId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->disabledCategoryIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public removeRole(Lcom/squareup/print/PrinterStation$Role;)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedRoles:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public setAutoPrintItemizedReceipts(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 106
    iput-boolean p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->autoPrintItemizedReceipts:Z

    return-object p0
.end method

.method public setInternal(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 121
    iput-boolean p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->internal:Z

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public setPrintATicketForEachItem(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 111
    iput-boolean p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printATicketForEachItem:Z

    return-object p0
.end method

.method public setPrintCompactTickets(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 116
    iput-boolean p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printCompactTickets:Z

    return-object p0
.end method

.method public setPrintsUncategorizedItems(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 101
    iput-boolean p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->printsUncategorizedItems:Z

    return-object p0
.end method

.method public setSelectedHardwarePrinterId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->selectedHardwarePrinterId:Ljava/lang/String;

    return-object p0
.end method
