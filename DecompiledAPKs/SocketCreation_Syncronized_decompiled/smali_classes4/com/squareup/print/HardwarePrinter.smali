.class public abstract Lcom/squareup/print/HardwarePrinter;
.super Ljava/lang/Object;
.source "HardwarePrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/HardwarePrinter$HardwareInfo;,
        Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;,
        Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;
    }
.end annotation


# static fields
.field private static final SUFFIX_LENGTH:I = 0x3


# instance fields
.field private final bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

.field private final hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

.field private final id:Ljava/lang/String;

.field private final textRenderingSpecs:Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;)V
    .locals 9

    move-object v0, p0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    new-instance v8, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-object v1, v8

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;)V

    iput-object v8, v0, Lcom/squareup/print/HardwarePrinter;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-object/from16 v1, p7

    .line 190
    iput-object v1, v0, Lcom/squareup/print/HardwarePrinter;->bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    move-object/from16 v1, p8

    .line 191
    iput-object v1, v0, Lcom/squareup/print/HardwarePrinter;->textRenderingSpecs:Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;

    .line 194
    iget-object v1, v0, Lcom/squareup/print/HardwarePrinter;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/print/HardwarePrinter;->id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 228
    :cond_0
    instance-of v1, p1, Lcom/squareup/print/HardwarePrinter;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 229
    :cond_1
    check-cast p1, Lcom/squareup/print/HardwarePrinter;

    .line 230
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v3

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 231
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getBitmapRenderingSpecs()Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter;->bitmapRenderingSpecs:Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    return-object v0
.end method

.method public getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTextRenderingSpecs()Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter;->textRenderingSpecs:Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 235
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public abstract performOpenCashDrawer()V
.end method

.method public abstract performPrint(Landroid/graphics/Bitmap;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
.end method

.method public abstract performPrint(Lcom/squareup/print/text/RenderedRows;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
.end method

.method public resetPrinterAndGetStatus()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    .line 207
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "resetPrinterAndGetStatus() has not been implemented for this printer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
