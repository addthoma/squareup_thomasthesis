.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;
.super Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.source "RealQuickAmountsSettingsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadedFromQuickAmounts"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u0017*\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;",
        "quickAmounts",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "currency",
        "Lcom/squareup/protos/connect/v2/common/Currency;",
        "(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)V",
        "getCurrency",
        "()Lcom/squareup/protos/connect/v2/common/Currency;",
        "getQuickAmounts",
        "()Lcom/squareup/quickamounts/QuickAmounts;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currency:Lcom/squareup/protos/connect/v2/common/Currency;

.field private final quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)V
    .locals 1

    const-string v0, "quickAmounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 244
    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;ILjava/lang/Object;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->copy(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-static {v0, v1}, Lcom/squareup/quickamounts/settings/QuickAmountsFormattingKt;->maybeAddZeroAmount(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    .line 248
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    .line 249
    instance-of v2, v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$InitialState;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eqz v2, :cond_0

    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    invoke-direct {v1, v0, v5, v4, v3}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;-><init>(Lcom/squareup/quickamounts/QuickAmounts;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    goto :goto_0

    .line 250
    :cond_0
    instance-of v2, v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    if-eqz v2, :cond_1

    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    invoke-direct {v1, v0, v5, v4, v3}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;-><init>(Lcom/squareup/quickamounts/QuickAmounts;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    goto :goto_0

    .line 251
    :cond_1
    instance-of v1, v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    invoke-direct {v1, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;-><init>(Lcom/squareup/quickamounts/QuickAmounts;)V

    check-cast v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    .line 248
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 251
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final component1()Lcom/squareup/quickamounts/QuickAmounts;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object v0
.end method

.method public final copy(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;
    .locals 1

    const-string v0, "quickAmounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    invoke-direct {v0, p1, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iget-object p1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrency()Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object v0
.end method

.method public final getQuickAmounts()Lcom/squareup/quickamounts/QuickAmounts;
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoadedFromQuickAmounts(quickAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
