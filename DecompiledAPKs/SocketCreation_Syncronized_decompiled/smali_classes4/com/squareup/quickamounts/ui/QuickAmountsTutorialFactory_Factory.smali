.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;-><init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/AppletsDrawerRunner;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/AppIdling;

    iget-object v3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->newInstance(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory_Factory;->get()Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;

    move-result-object v0

    return-object v0
.end method
