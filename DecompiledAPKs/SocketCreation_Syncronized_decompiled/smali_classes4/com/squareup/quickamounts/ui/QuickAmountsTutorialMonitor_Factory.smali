.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;>;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;>;)",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;)",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;-><init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->newInstance(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;Lcom/squareup/settings/server/Features;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor_Factory;->get()Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    move-result-object v0

    return-object v0
.end method
