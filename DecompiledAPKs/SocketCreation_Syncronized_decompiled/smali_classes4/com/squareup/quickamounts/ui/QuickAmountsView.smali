.class public final Lcom/squareup/quickamounts/ui/QuickAmountsView;
.super Landroid/widget/LinearLayout;
.source "QuickAmountsView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmountsView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmountsView.kt\ncom/squareup/quickamounts/ui/QuickAmountsView\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,141:1\n1642#2,2:142\n34#3,9:144\n*E\n*S KotlinDebug\n*F\n+ 1 QuickAmountsView.kt\ncom/squareup/quickamounts/ui/QuickAmountsView\n*L\n116#1,2:142\n52#1,9:144\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0019J\u000e\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u001cR\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "drawBottomLine",
        "",
        "drawTopLine",
        "linePaint",
        "Landroid/graphics/Paint;",
        "maxCount",
        "quickAmountClicked",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/quickamounts/ui/ClickEvent;",
        "textColor",
        "textSelectorResId",
        "draw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "quickAmountClickedView",
        "Lio/reactivex/Observable;",
        "setQuickAmounts",
        "quickAmounts",
        "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private drawBottomLine:Z

.field private drawTopLine:Z

.field private final linePaint:Landroid/graphics/Paint;

.field private maxCount:I

.field private final quickAmountClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/quickamounts/ui/ClickEvent;",
            ">;"
        }
    .end annotation
.end field

.field private textColor:I

.field private textSelectorResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/quickamounts/ui/QuickAmountsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/quickamounts/ui/QuickAmountsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->linePaint:Landroid/graphics/Paint;

    .line 49
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->quickAmountClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 52
    sget-object v0, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView:[I

    const-string v1, "R.styleable.QuickAmountsView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 147
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string p3, "a"

    .line 149
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget p3, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_buttonSelector:I

    .line 55
    sget v0, Lcom/squareup/padlock/R$drawable;->keypad_selector:I

    .line 53
    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    invoke-static {p0, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$setTextSelectorResId$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;I)V

    .line 59
    sget p3, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_android_textColor:I

    .line 60
    sget v0, Lcom/squareup/padlock/R$color;->text_color:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 58
    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    invoke-static {p0, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$setTextColor$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;I)V

    .line 64
    invoke-static {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$getLinePaint$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Landroid/graphics/Paint;

    move-result-object p3

    sget v0, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_lineWidth:I

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 66
    invoke-static {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$getLinePaint$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Landroid/graphics/Paint;

    move-result-object p3

    .line 67
    sget v0, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_lineColor:I

    .line 68
    sget v2, Lcom/squareup/padlock/R$color;->line_color:I

    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 66
    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 71
    sget p1, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_amountCount:I

    const p3, 0x7fffffff

    invoke-virtual {p2, p1, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$setMaxCount$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;I)V

    .line 73
    sget p1, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_drawBottomLine:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$setDrawBottomLine$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;Z)V

    .line 74
    sget p1, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView_drawTopLine:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$setDrawTopLine$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;Z)V

    .line 75
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    invoke-virtual {p0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->setWillNotDraw(Z)V

    .line 78
    invoke-virtual {p0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->setOrientation(I)V

    return-void

    :catchall_0
    move-exception p1

    .line 151
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 38
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 39
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/quickamounts/ui/QuickAmountsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getDrawBottomLine$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->drawBottomLine:Z

    return p0
.end method

.method public static final synthetic access$getDrawTopLine$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->drawTopLine:Z

    return p0
.end method

.method public static final synthetic access$getLinePaint$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Landroid/graphics/Paint;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->linePaint:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic access$getMaxCount$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)I
    .locals 0

    .line 35
    iget p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->maxCount:I

    return p0
.end method

.method public static final synthetic access$getQuickAmountClicked$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->quickAmountClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getTextColor$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)I
    .locals 0

    .line 35
    iget p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->textColor:I

    return p0
.end method

.method public static final synthetic access$getTextSelectorResId$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)I
    .locals 0

    .line 35
    iget p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->textSelectorResId:I

    return p0
.end method

.method public static final synthetic access$setDrawBottomLine$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;Z)V
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->drawBottomLine:Z

    return-void
.end method

.method public static final synthetic access$setDrawTopLine$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;Z)V
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->drawTopLine:Z

    return-void
.end method

.method public static final synthetic access$setMaxCount$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;I)V
    .locals 0

    .line 35
    iput p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->maxCount:I

    return-void
.end method

.method public static final synthetic access$setTextColor$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;I)V
    .locals 0

    .line 35
    iput p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->textColor:I

    return-void
.end method

.method public static final synthetic access$setTextSelectorResId$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;I)V
    .locals 0

    .line 35
    iput p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->textSelectorResId:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getChildCount()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    :goto_0
    if-ge v2, v1, :cond_0

    int-to-float v3, v2

    mul-float v7, v0, v3

    const/4 v6, 0x0

    .line 91
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getMeasuredHeight()I

    move-result v3

    int-to-float v8, v3

    iget-object v9, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->linePaint:Landroid/graphics/Paint;

    move-object v4, p1

    move v5, v7

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 96
    iget-boolean v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->drawTopLine:Z

    if-eqz v1, :cond_1

    const/4 v3, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getMeasuredWidth()I

    move-result v1

    int-to-float v5, v1

    iget-object v7, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->linePaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v0

    move v6, v0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 100
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->drawBottomLine:Z

    if-eqz v1, :cond_2

    .line 101
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v6, v1, v0

    const/4 v3, 0x0

    .line 102
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getMeasuredWidth()I

    move-result v0

    int-to-float v5, v0

    iget-object v7, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->linePaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_2
    return-void
.end method

.method public final quickAmountClickedView()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/quickamounts/ui/ClickEvent;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->quickAmountClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final setQuickAmounts(Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;)V
    .locals 13

    const-string v0, "quickAmounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;->getAmounts()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    iget v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->maxCount:I

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    .line 108
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->removeAllViews()V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/quickamounts/ui/R$dimen;->quick_amount_value_text_size_min:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 113
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/quickamounts/ui/R$dimen;->quick_amount_value_text_size_max:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 114
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v3, v4}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v10

    const-string v3, "getTypeface(context, REGULAR)"

    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    check-cast p1, Ljava/lang/Iterable;

    .line 142
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    invoke-virtual {v3}, Lcom/squareup/quickamounts/ui/UiQuickAmount;->component1()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object v4

    invoke-virtual {v3}, Lcom/squareup/quickamounts/ui/UiQuickAmount;->component2()Ljava/lang/CharSequence;

    move-result-object v3

    .line 117
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/squareup/quickamounts/ui/R$layout;->quick_amount_entry:I

    move-object v7, p0

    check-cast v7, Landroid/view/ViewGroup;

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getChildCount()I

    move-result v5

    sub-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 121
    sget v6, Lcom/squareup/quickamounts/ui/R$id;->quick_amount_value:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const-string v6, "lastAddedView.findViewBy\u2026(R.id.quick_amount_value)"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v11, v5

    check-cast v11, Landroid/widget/TextView;

    .line 122
    iget v5, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->textColor:I

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 123
    iget v5, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView;->textSelectorResId:I

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 124
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const/4 v5, 0x0

    .line 125
    invoke-static {v11, v0, v2, v1, v5}, Landroidx/core/widget/TextViewCompat;->setAutoSizeTextTypeUniformWithConfiguration(Landroid/widget/TextView;IIII)V

    .line 129
    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    new-instance v12, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;

    move-object v3, v12

    move-object v5, v11

    move-object v6, p0

    move-object v7, v10

    move v8, v0

    move v9, v2

    invoke-direct/range {v3 .. v9}, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Landroid/widget/TextView;Lcom/squareup/quickamounts/ui/QuickAmountsView;Landroid/graphics/Typeface;II)V

    check-cast v12, Landroid/view/View$OnClickListener;

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    return-void
.end method
