.class public final Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;
.super Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
.source "RealQuickAmountsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Cache"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u00c6\u0003J%\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0010\u0008\u0002\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J \u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00032\u000e\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u0016J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0019\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
        "status",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "setAmounts",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V",
        "getSetAmounts",
        "()Ljava/util/List;",
        "getStatus",
        "()Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "onCache",
        "newSetAmounts",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final setAmounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final status:Lcom/squareup/quickamounts/QuickAmountsStatus;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 159
    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iput-object p2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->copy(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    invoke-direct {v0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iget-object v1, p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSetAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    return-object v0
.end method

.method public final getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    new-instance v0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    :goto_0
    invoke-direct {v0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
    .locals 0

    .line 156
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;

    move-result-object p1

    check-cast p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", setAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;->setAmounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
