.class public final Lcom/squareup/readertutorial/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/readertutorial/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final glyph_message:I = 0x7f0a07b1

.field public static final glyph_title:I = 0x7f0a07b7

.field public static final r12_education_cable:I = 0x7f0a0cb5

.field public static final r12_education_charge:I = 0x7f0a0cb6

.field public static final r12_education_chip_card:I = 0x7f0a0cb7

.field public static final r12_education_dip:I = 0x7f0a0cb8

.field public static final r12_education_dip_matte_container:I = 0x7f0a0cb9

.field public static final r12_education_dots_green:I = 0x7f0a0cba

.field public static final r12_education_dots_orange:I = 0x7f0a0cbb

.field public static final r12_education_image:I = 0x7f0a0cbc

.field public static final r12_education_learn_even_more:I = 0x7f0a0cbd

.field public static final r12_education_mag_card:I = 0x7f0a0cbe

.field public static final r12_education_matte:I = 0x7f0a0cbf

.field public static final r12_education_message:I = 0x7f0a0cc0

.field public static final r12_education_next:I = 0x7f0a0cc1

.field public static final r12_education_pager:I = 0x7f0a0cc2

.field public static final r12_education_pager_indicator:I = 0x7f0a0cc3

.field public static final r12_education_phone_apple_pay:I = 0x7f0a0cc4

.field public static final r12_education_phone_r4:I = 0x7f0a0cc5

.field public static final r12_education_r12:I = 0x7f0a0cc6

.field public static final r12_education_start_selling:I = 0x7f0a0cc7

.field public static final r12_education_sticker:I = 0x7f0a0cc8

.field public static final r12_education_swipe:I = 0x7f0a0cc9

.field public static final r12_education_tap:I = 0x7f0a0cca

.field public static final r12_education_tap_card_with_hand:I = 0x7f0a0ccb

.field public static final r12_education_title:I = 0x7f0a0ccc

.field public static final r12_education_video:I = 0x7f0a0ccd

.field public static final r12_education_video_play:I = 0x7f0a0cce

.field public static final r12_education_video_preview:I = 0x7f0a0ccf

.field public static final r12_education_welcome:I = 0x7f0a0cd0

.field public static final r12_education_x_phone:I = 0x7f0a0cd1

.field public static final r12_education_x_tablet:I = 0x7f0a0cd2

.field public static final r6_first_time_video_done:I = 0x7f0a0cd3

.field public static final r6_first_time_video_fallback_background:I = 0x7f0a0cd4

.field public static final r6_first_time_video_layout_container:I = 0x7f0a0cd5

.field public static final r6_first_time_video_surface_view:I = 0x7f0a0cd6

.field public static final r6_first_time_video_text_container:I = 0x7f0a0cd7

.field public static final r6_first_time_video_text_layer:I = 0x7f0a0cd8

.field public static final speed_test_primary_button:I = 0x7f0a0eb9

.field public static final speed_test_secondary_button:I = 0x7f0a0eba

.field public static final speedtest_up_button:I = 0x7f0a0ebb

.field public static final spinner_glyph:I = 0x7f0a0ebd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
