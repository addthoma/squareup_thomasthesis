.class public final Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;
.super Lcom/squareup/noho/NohoActionBar;
.source "NohoActionBarWithTwoActionIcons.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoActionBarWithTwoActionIcons.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoActionBarWithTwoActionIcons.kt\ncom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,72:1\n1103#2,7:73\n1261#2:80\n*E\n*S KotlinDebug\n*F\n+ 1 NohoActionBarWithTwoActionIcons.kt\ncom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons\n*L\n61#1,7:73\n23#1:80\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B#\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u000b\u001a\u00020\u000cJ0\u0010\r\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0014R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;",
        "Lcom/squareup/noho/NohoActionBar;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "actionIcon2",
        "Landroid/widget/ImageView;",
        "hideSecondActionIcon",
        "",
        "setSecondActionIcon",
        "iconRes",
        "tooltip",
        "Lcom/squareup/util/ViewString;",
        "isEnabled",
        "",
        "command",
        "Lkotlin/Function0;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionIcon2:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    sget p1, Lcom/squareup/salesreport/impl/R$layout;->noho_action_bar_second_action_icon:I

    move-object p2, p0

    check-cast p2, Landroid/view/ViewGroup;

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    .line 28
    iget-object p1, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->addView(Landroid/view/View;)V

    .line 29
    move-object p1, p0

    check-cast p1, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    .line 30
    new-instance p2, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {p2}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 31
    move-object p3, p1

    check-cast p3, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {p2, p3}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 33
    sget v0, Lcom/squareup/noho/R$id;->action_icon:I

    .line 35
    sget v1, Lcom/squareup/salesreport/impl/R$id;->action_icon_2:I

    const/4 v2, 0x7

    const/4 v3, 0x6

    .line 32
    invoke-virtual {p2, v0, v2, v1, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 39
    sget v0, Lcom/squareup/salesreport/impl/R$id;->action_icon_2:I

    .line 41
    invoke-virtual {p1}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->getId()I

    move-result p1

    .line 38
    invoke-virtual {p2, v0, v2, p1, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 44
    invoke-virtual {p2, p3}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 22
    sget p3, Lcom/squareup/noho/R$attr;->nohoActionBarStyle:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic setSecondActionIcon$default(Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;ILcom/squareup/util/ViewString;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x1

    .line 55
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->setSecondActionIcon(ILcom/squareup/util/ViewString;ZLkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public final hideSecondActionIcon()V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method public final setSecondActionIcon(ILcom/squareup/util/ViewString;ZLkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/util/ViewString;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "tooltip"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v1, p1, v2, v3, v2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    check-cast p1, Landroid/view/View;

    .line 73
    new-instance p2, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons$setSecondActionIcon$$inlined$onClickDebounced$1;

    invoke-direct {p2, p4}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons$setSecondActionIcon$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->actionIcon2:Landroid/widget/ImageView;

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method
