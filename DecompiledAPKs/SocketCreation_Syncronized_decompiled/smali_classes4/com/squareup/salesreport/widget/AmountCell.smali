.class public final Lcom/squareup/salesreport/widget/AmountCell;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "AmountCell.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAmountCell.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AmountCell.kt\ncom/squareup/salesreport/widget/AmountCell\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,80:1\n1261#2:81\n*E\n*S KotlinDebug\n*F\n+ 1 AmountCell.kt\ncom/squareup/salesreport/widget/AmountCell\n*L\n25#1:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\"\u001a\u00020#2\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0002J\u0008\u0010$\u001a\u00020#H\u0002R$\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR&\u0010\u0010\u001a\u00020\u00072\u0008\u0008\u0001\u0010\t\u001a\u00020\u00078G@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0018\u001a\u00020\u00172\u0006\u0010\t\u001a\u00020\u00178F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\t\u001a\u00020\u00172\u0006\u0010\t\u001a\u00020\u00178F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001e\u0010\u001a\"\u0004\u0008\u001f\u0010\u001cR\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/salesreport/widget/AmountCell;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "value",
        "",
        "percentage",
        "getPercentage",
        "()Ljava/lang/String;",
        "setPercentage",
        "(Ljava/lang/String;)V",
        "percentageColorRes",
        "getPercentageColorRes",
        "()I",
        "setPercentageColorRes",
        "(I)V",
        "percentageView",
        "Lcom/squareup/noho/NohoLabel;",
        "Lcom/squareup/util/ViewString;",
        "title",
        "getTitle",
        "()Lcom/squareup/util/ViewString;",
        "setTitle",
        "(Lcom/squareup/util/ViewString;)V",
        "titleView",
        "getValue",
        "setValue",
        "valueView",
        "Landroid/widget/TextView;",
        "applyAttributes",
        "",
        "bindViews",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private percentageView:Lcom/squareup/noho/NohoLabel;

.field private titleView:Lcom/squareup/noho/NohoLabel;

.field private valueView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/AmountCell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/AmountCell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p3, Lcom/squareup/salesreport/impl/R$layout;->amount_cell:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p3, v0}, Lcom/squareup/noho/NohoLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 57
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p3, -0x2

    const/4 v0, -0x1

    invoke-direct {p1, p3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/widget/AmountCell;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p1, 0x1

    .line 58
    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/widget/AmountCell;->setOrientation(I)V

    .line 59
    invoke-direct {p0}, Lcom/squareup/salesreport/widget/AmountCell;->bindViews()V

    .line 60
    invoke-direct {p0, p2}, Lcom/squareup/salesreport/widget/AmountCell;->applyAttributes(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 23
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 24
    sget p3, Lcom/squareup/salesreport/impl/R$attr;->amountCellStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/salesreport/widget/AmountCell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final applyAttributes(Landroid/util/AttributeSet;)V
    .locals 2

    .line 70
    invoke-virtual {p0}, Lcom/squareup/salesreport/widget/AmountCell;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/squareup/salesreport/impl/R$styleable;->AmountCell:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 74
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->titleView:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/salesreport/impl/R$styleable;->AmountCell_android_title:I

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->valueView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string/jumbo v1, "valueView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/salesreport/impl/R$styleable;->AmountCell_sqMessageText:I

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 64
    sget v0, Lcom/squareup/salesreport/impl/R$id;->amount_cell_value:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/AmountCell;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.amount_cell_value)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->valueView:Landroid/widget/TextView;

    .line 65
    sget v0, Lcom/squareup/salesreport/impl/R$id;->amount_cell_label:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/AmountCell;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.amount_cell_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->titleView:Lcom/squareup/noho/NohoLabel;

    .line 66
    sget v0, Lcom/squareup/salesreport/impl/R$id;->amount_cell_percentage_value:I

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/AmountCell;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.amount_cell_percentage_value)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->percentageView:Lcom/squareup/noho/NohoLabel;

    return-void
.end method


# virtual methods
.method public final getPercentage()Ljava/lang/String;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->percentageView:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "percentageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPercentageColorRes()I
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->percentageView:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "percentageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public final getTitle()Lcom/squareup/util/ViewString;
    .locals 3

    .line 36
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    iget-object v1, p0, Lcom/squareup/salesreport/widget/AmountCell;->titleView:Lcom/squareup/noho/NohoLabel;

    if-nez v1, :cond_0

    const-string v2, "titleView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "titleView.text"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final getValue()Lcom/squareup/util/ViewString;
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    iget-object v1, p0, Lcom/squareup/salesreport/widget/AmountCell;->valueView:Landroid/widget/TextView;

    if-nez v1, :cond_0

    const-string/jumbo v2, "valueView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    const-string/jumbo v2, "valueView.text"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final setPercentage(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->percentageView:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "percentageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setPercentageColorRes(I)V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->percentageView:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "percentageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/widget/AmountCell;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    return-void
.end method

.method public final setTitle(Lcom/squareup/util/ViewString;)V
    .locals 3

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->titleView:Lcom/squareup/noho/NohoLabel;

    if-nez v0, :cond_0

    const-string v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/widget/AmountCell;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setValue(Lcom/squareup/util/ViewString;)V
    .locals 3

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/salesreport/widget/AmountCell;->valueView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "valueView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/widget/AmountCell;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
