.class public final Lcom/squareup/salesreport/util/DetailLevelsKt;
.super Ljava/lang/Object;
.source "DetailLevels.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007\"\u0018\u0010\u0008\u001a\u00020\u0006*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "defaultComparisonRange",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "Lcom/squareup/api/salesreport/DetailLevel;",
        "getDefaultComparisonRange",
        "(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/customreport/data/ComparisonRange;",
        "isComparisonEnabled",
        "",
        "(Lcom/squareup/api/salesreport/DetailLevel;)Z",
        "isOverviewEnabled",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getDefaultComparisonRange(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/customreport/data/ComparisonRange;
    .locals 1

    const-string v0, "$this$defaultComparisonRange"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p0}, Lcom/squareup/salesreport/util/DetailLevelsKt;->isComparisonEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    :goto_0
    check-cast p0, Lcom/squareup/customreport/data/ComparisonRange;

    return-object p0
.end method

.method public static final isComparisonEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z
    .locals 1

    const-string v0, "$this$isComparisonEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    instance-of v0, p0, Lcom/squareup/api/salesreport/DetailLevel$Basic;

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 18
    :cond_0
    instance-of p0, p0, Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    return p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final isOverviewEnabled(Lcom/squareup/api/salesreport/DetailLevel;)Z
    .locals 1

    const-string v0, "$this$isOverviewEnabled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    instance-of v0, p0, Lcom/squareup/api/salesreport/DetailLevel$Basic;

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 29
    :cond_0
    instance-of p0, p0, Lcom/squareup/api/salesreport/DetailLevel$Detailed;

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    return p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
