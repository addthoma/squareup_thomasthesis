.class public final Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;
.super Lcom/squareup/salesreport/util/SalesReportRow;
.source "SalesReportRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/util/SalesReportRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SalesChartRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J;\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "comparisonReportConfig",
        "salesSummaryReport",
        "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "salesChart",
        "Lcom/squareup/customreport/data/WithSalesChartReport;",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V",
        "getChartSalesSelection",
        "()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "getComparisonReportConfig",
        "()Lcom/squareup/customreport/data/ReportConfig;",
        "getReportConfig",
        "getSalesChart",
        "()Lcom/squareup/customreport/data/WithSalesChartReport;",
        "getSalesSummaryReport",
        "()Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

.field private final comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field private final reportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field private final salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

.field private final salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;


# direct methods
.method public constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "comparisonReportConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSummaryReport"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesChart"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 136
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/util/SalesReportRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;ILjava/lang/Object;)Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->copy(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public final component2()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public final component3()Lcom/squareup/customreport/data/WithSalesSummaryReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    return-object v0
.end method

.method public final component4()Lcom/squareup/customreport/data/WithSalesChartReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    return-object v0
.end method

.method public final component5()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    return-object v0
.end method

.method public final copy(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;
    .locals 7

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "comparisonReportConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSummaryReport"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesChart"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    iget-object p1, p1, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    return-object v0
.end method

.method public final getComparisonReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public final getReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public final getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    return-object v0
.end method

.method public final getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SalesChartRow(reportConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", comparisonReportConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->comparisonReportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", salesSummaryReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", salesChart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->salesChart:Lcom/squareup/customreport/data/WithSalesChartReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", chartSalesSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->chartSalesSelection:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
