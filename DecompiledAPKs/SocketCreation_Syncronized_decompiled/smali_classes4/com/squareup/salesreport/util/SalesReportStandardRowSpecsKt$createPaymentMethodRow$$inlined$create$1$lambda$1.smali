.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n*L\n1#1,87:1\n529#2,14:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$$special$$inlined$bind$10"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $bar$inlined:Landroid/view/View;

.field final synthetic $name$inlined:Landroid/widget/TextView;

.field final synthetic $quantity$inlined:Landroid/widget/TextView;

.field final synthetic $sales$inlined:Landroid/widget/TextView;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$name$inlined:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$quantity$inlined:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$sales$inlined:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$bar$inlined:Landroid/view/View;

    iput-object p6, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$name$inlined:Landroid/widget/TextView;

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$quantity$inlined:Landroid/widget/TextView;

    const-string v0, "quantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;

    iget-object v0, v0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;->$wholeNumberPercentageFormatter$inlined:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$sales$inlined:Landroid/widget/TextView;

    const-string v0, "sales"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;->getSales()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$bar$inlined:Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;->getColorResId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 94
    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide p1

    double-to-float p1, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 95
    :goto_0
    new-instance p2, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {p2}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 96
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {p2, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 97
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_payment_method_bar:I

    invoke-virtual {p2, v0, p1}, Landroidx/constraintlayout/widget/ConstraintSet;->setHorizontalWeight(IF)V

    .line 98
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_payment_method_empty_bar:I

    const/16 v1, 0x64

    int-to-float v1, v1

    sub-float/2addr v1, p1

    invoke-virtual {p2, v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->setHorizontalWeight(IF)V

    .line 99
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {p2, p1}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method
