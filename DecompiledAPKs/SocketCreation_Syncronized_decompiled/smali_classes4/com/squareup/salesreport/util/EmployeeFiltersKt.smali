.class public final Lcom/squareup/salesreport/util/EmployeeFiltersKt;
.super Ljava/lang/Object;
.source "EmployeeFilters.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "displayValue",
        "",
        "Lcom/squareup/customreport/data/EmployeeFilter;",
        "res",
        "Lcom/squareup/util/Res;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final displayValue(Lcom/squareup/customreport/data/EmployeeFilter;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$displayValue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    instance-of v0, p0, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;

    invoke-virtual {p0}, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;->getDisplayName()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 11
    :cond_0
    instance-of p0, p0, Lcom/squareup/customreport/data/EmployeeFilter$UnattributedEmployeeFilter;

    if-eqz p0, :cond_1

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_unattributed_employee:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
