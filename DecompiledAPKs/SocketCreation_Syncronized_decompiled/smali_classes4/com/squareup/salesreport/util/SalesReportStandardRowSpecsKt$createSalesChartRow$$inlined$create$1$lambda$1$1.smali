.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;
.super Ljava/lang/Object;
.source "SalesReportStandardRowSpecs.kt"

# interfaces
.implements Lcom/squareup/chartography/ChartView$ScrubbingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0003H\u0016\u00a8\u0006\u0007\u00b8\u0006\u0008"
    }
    d2 = {
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$1$1$1",
        "Lcom/squareup/chartography/ChartView$ScrubbingListener;",
        "onScrub",
        "",
        "domainStep",
        "",
        "onScrubEnd",
        "impl_release",
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$$special$$inlined$bind$5$lambda$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $compareDefaultLabel:Ljava/lang/String;

.field final synthetic $compareLabel:Landroid/widget/TextView;

.field final synthetic $compareValue:Landroid/widget/TextView;

.field final synthetic $currentDefaultLabel:Ljava/lang/String;

.field final synthetic $currentLabel:Landroid/widget/TextView;

.field final synthetic $currentValue:Landroid/widget/TextView;

.field final synthetic $defaultTextForComparisonValue:Ljava/lang/CharSequence;

.field final synthetic $defaultTextForCurrentValue:Ljava/lang/CharSequence;

.field final synthetic $row:Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

.field final synthetic $salesAdapter:Lcom/squareup/salesreport/chart/SalesAdapter;

.field final synthetic this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;


# direct methods
.method constructor <init>(Landroid/widget/TextView;Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/salesreport/chart/SalesAdapter;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentLabel:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$row:Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$salesAdapter:Lcom/squareup/salesreport/chart/SalesAdapter;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareLabel:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentValue:Landroid/widget/TextView;

    iput-object p6, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareValue:Landroid/widget/TextView;

    iput-object p7, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentDefaultLabel:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareDefaultLabel:Ljava/lang/String;

    iput-object p9, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$defaultTextForCurrentValue:Ljava/lang/CharSequence;

    iput-object p10, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$defaultTextForComparisonValue:Ljava/lang/CharSequence;

    iput-object p11, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrub(I)V
    .locals 8

    .line 212
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentLabel:Landroid/widget/TextView;

    const-string v1, "currentLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$row:Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    .line 213
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    .line 214
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v4, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localeProvider$inlined:Ljavax/inject/Provider;

    .line 215
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v5, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localTimeFormatter$inlined:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    .line 216
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$salesAdapter:Lcom/squareup/salesreport/chart/SalesAdapter;

    invoke-virtual {v1}, Lcom/squareup/salesreport/chart/SalesAdapter;->getData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {v1}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getDomain()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lorg/threeten/bp/LocalDateTime;

    const/4 v7, 0x0

    .line 212
    invoke-static/range {v2 .. v7}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$scrubbingDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareLabel:Landroid/widget/TextView;

    const-string v1, "compareLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$row:Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    .line 220
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$res$inlined:Lcom/squareup/util/Res;

    .line 221
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v4, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localeProvider$inlined:Ljavax/inject/Provider;

    .line 222
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v1, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v5, v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$localTimeFormatter$inlined:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    .line 223
    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$salesAdapter:Lcom/squareup/salesreport/chart/SalesAdapter;

    invoke-virtual {v1}, Lcom/squareup/salesreport/chart/SalesAdapter;->getData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {v1}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getDomain()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lorg/threeten/bp/LocalDateTime;

    const/4 v7, 0x1

    .line 219
    invoke-static/range {v2 .. v7}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$scrubbingDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$salesAdapter:Lcom/squareup/salesreport/chart/SalesAdapter;

    invoke-virtual {v0}, Lcom/squareup/salesreport/chart/SalesAdapter;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {v0}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getCurrentSale()J

    move-result-wide v1

    .line 228
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentValue:Landroid/widget/TextView;

    const-string v3, "currentValue"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    iget-object v3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$row:Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    invoke-virtual {v3}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v3

    .line 230
    iget-object v4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v4, v4, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v4, v4, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$currencyCode$inlined:Lcom/squareup/protos/common/CurrencyCode;

    .line 231
    iget-object v5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v5, v5, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v5, v5, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 232
    iget-object v6, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v6, v6, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v6, v6, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 228
    invoke-static/range {v1 .. v6}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->textFromChartSalesSelection(JLcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$salesAdapter:Lcom/squareup/salesreport/chart/SalesAdapter;

    invoke-virtual {v0}, Lcom/squareup/salesreport/chart/SalesAdapter;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/salesreport/chart/SalesDataPoint;

    invoke-virtual {p1}, Lcom/squareup/salesreport/chart/SalesDataPoint;->getPreviousDaySale()J

    move-result-wide v0

    .line 236
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareValue:Landroid/widget/TextView;

    const-string v2, "compareValue"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$row:Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    invoke-virtual {v2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v2

    .line 238
    iget-object v3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v3, v3, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v3, v3, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$currencyCode$inlined:Lcom/squareup/protos/common/CurrencyCode;

    .line 239
    iget-object v4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v4, v4, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v4, v4, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueMoneyFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 240
    iget-object v5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;

    iget-object v5, v5, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    iget-object v5, v5, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;->$exactValueNumberFormatter$inlined:Lcom/squareup/text/Formatter;

    .line 236
    invoke-static/range {v0 .. v5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->textFromChartSalesSelection(JLcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onScrubEnd()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentLabel:Landroid/widget/TextView;

    const-string v1, "currentLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentDefaultLabel:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareLabel:Landroid/widget/TextView;

    const-string v1, "compareLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareDefaultLabel:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$currentValue:Landroid/widget/TextView;

    const-string v1, "currentValue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$defaultTextForCurrentValue:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$compareValue:Landroid/widget/TextView;

    const-string v1, "compareValue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1$lambda$1$1;->$defaultTextForComparisonValue:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
