.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n*L\n1#1,87:1\n476#2,2:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/salesreport/util/SalesReportStandardRowSpecsKt$$special$$inlined$bind$9"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $glyph$inlined:Lcom/squareup/glyph/SquareGlyphView;

.field final synthetic $money$inlined:Lcom/squareup/noho/NohoLabel;

.field final synthetic $name$inlined:Lcom/squareup/noho/NohoLabel;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/noho/NohoLabel;Lcom/squareup/noho/NohoLabel;Lcom/squareup/glyph/SquareGlyphView;Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$name$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$money$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$glyph$inlined:Lcom/squareup/glyph/SquareGlyphView;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->this$0:Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;

    iget-object v0, p1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;->$resources$inlined:Landroid/content/res/Resources;

    move-object v1, p2

    check-cast v1, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v2

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$name$inlined:Lcom/squareup/noho/NohoLabel;

    const-string p2, "name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p1

    check-cast v3, Landroid/widget/TextView;

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$money$inlined:Lcom/squareup/noho/NohoLabel;

    const-string p2, "money"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p1

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1$lambda$1;->$glyph$inlined:Lcom/squareup/glyph/SquareGlyphView;

    const-string p1, "glyph"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static/range {v0 .. v5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->access$bindSalesDetailRow(Landroid/content/res/Resources;Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/glyph/SquareGlyphView;)V

    return-void
.end method
