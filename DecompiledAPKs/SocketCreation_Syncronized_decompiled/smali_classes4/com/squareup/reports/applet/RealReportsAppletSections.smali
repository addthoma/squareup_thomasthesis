.class public final Lcom/squareup/reports/applet/RealReportsAppletSections;
.super Ljava/lang/Object;
.source "ReportsAppletSections.kt"

# interfaces
.implements Lcom/squareup/reports/applet/ReportsAppletSections;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/reports/applet/RealReportsAppletSections;",
        "Lcom/squareup/reports/applet/ReportsAppletSections;",
        "currentDrawer",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
        "drawerHistory",
        "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
        "salesReportSection",
        "Lcom/squareup/reports/applet/sales/SalesReportSection;",
        "loyaltyReportSection",
        "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
        "cashDrawerShiftManager",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
        "disputesSection",
        "Lcom/squareup/reports/applet/disputes/DisputesSection;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/util/Res;)V",
        "orderedEntries",
        "",
        "Lcom/squareup/applet/AppletSectionsListEntry;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final currentDrawer:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

.field private final disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

.field private final drawerHistory:Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

.field private final handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

.field private final loyaltyReportSection:Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

.field private final res:Lcom/squareup/util/Res;

.field private final salesReportSection:Lcom/squareup/reports/applet/sales/SalesReportSection;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentDrawer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawerHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportSection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyReportSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawerShiftManager"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputesSection"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlesDisputes"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->currentDrawer:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    iput-object p2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->drawerHistory:Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    iput-object p3, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->salesReportSection:Lcom/squareup/reports/applet/sales/SalesReportSection;

    iput-object p4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->loyaltyReportSection:Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    iput-object p5, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iput-object p6, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

    iput-object p7, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    iput-object p8, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public orderedEntries()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/applet/AppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    new-instance v1, Lcom/squareup/applet/AppletSectionsListEntry;

    iget-object v2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->salesReportSection:Lcom/squareup/reports/applet/sales/SalesReportSection;

    check-cast v2, Lcom/squareup/applet/AppletSection;

    sget v3, Lcom/squareup/reports/applet/R$string;->reports_sales:I

    iget-object v4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    new-instance v1, Lcom/squareup/reports/applet/CurrentDrawerEntry;

    iget-object v2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->currentDrawer:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    iget-object v3, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/reports/applet/CurrentDrawerEntry;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    new-instance v1, Lcom/squareup/applet/AppletSectionsListEntry;

    iget-object v2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->drawerHistory:Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    check-cast v2, Lcom/squareup/applet/AppletSection;

    sget v3, Lcom/squareup/reports/applet/R$string;->reports_drawer_history:I

    iget-object v4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    new-instance v1, Lcom/squareup/reports/applet/DisputesEntry;

    iget-object v2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

    iget-object v3, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/reports/applet/DisputesEntry;-><init>(Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/util/Res;Lcom/squareup/disputes/api/HandlesDisputes;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    new-instance v1, Lcom/squareup/applet/AppletSectionsListEntry;

    iget-object v2, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->loyaltyReportSection:Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    check-cast v2, Lcom/squareup/applet/AppletSection;

    sget v3, Lcom/squareup/reports/applet/R$string;->reports_loyalty:I

    iget-object v4, p0, Lcom/squareup/reports/applet/RealReportsAppletSections;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
