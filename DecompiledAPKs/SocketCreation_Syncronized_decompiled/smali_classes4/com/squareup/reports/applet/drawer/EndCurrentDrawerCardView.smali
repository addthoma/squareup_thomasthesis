.class public Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;
.super Lcom/squareup/widgets/PairLayout;
.source "EndCurrentDrawerCardView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private contentFrame:Landroid/view/ViewGroup;

.field private detailsView:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

.field private drawerDetailsContainer:Landroid/view/ViewGroup;

.field private endDrawerButton:Lcom/squareup/ui/ConfirmButton;

.field private endDrawerMessageContainer:Landroid/view/ViewGroup;

.field presenter:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const-class p2, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Component;->inject(Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;)V

    return-void
.end method

.method private hideKeyboard()V
    .locals 0

    .line 83
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public animateToEndDrawerMessage()V
    .locals 4

    .line 87
    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->contentFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->drawerDetailsContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->endDrawerMessageContainer:Landroid/view/ViewGroup;

    const-wide/16 v2, 0x14d

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/util/Views;->crossFade(Landroid/view/View;Landroid/view/View;J)V

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->drawerDetailsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 75
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$onFinishInflate$0$EndCurrentDrawerCardView()V
    .locals 1

    .line 56
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->hideKeyboard()V

    .line 57
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->presenter:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->endDrawer()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->presenter:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->detailsView:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->dropPresenter()V

    .line 66
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->presenter:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 67
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 45
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 47
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 48
    sget v0, Lcom/squareup/reports/applet/R$id;->content_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->contentFrame:Landroid/view/ViewGroup;

    .line 49
    sget v0, Lcom/squareup/reports/applet/R$id;->drawer_details_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->drawerDetailsContainer:Landroid/view/ViewGroup;

    .line 50
    sget v0, Lcom/squareup/reports/applet/R$id;->end_drawer_message_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->endDrawerMessageContainer:Landroid/view/ViewGroup;

    .line 52
    sget v0, Lcom/squareup/reports/applet/R$id;->end_drawer_details:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->detailsView:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 53
    sget v0, Lcom/squareup/reports/applet/R$id;->end_drawer_card_end_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->endDrawerButton:Lcom/squareup/ui/ConfirmButton;

    .line 54
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->endDrawerButton:Lcom/squareup/ui/ConfirmButton;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setButtonWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->endDrawerButton:Lcom/squareup/ui/ConfirmButton;

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerCardView$h4k_Wc-Isz6sm5A_Uw6U9ovBaxM;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$EndCurrentDrawerCardView$h4k_Wc-Isz6sm5A_Uw6U9ovBaxM;-><init>(Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->detailsView:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->presenter:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    iget-object v1, v1, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndConfigurePresenter(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->presenter:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public showEndDrawerMessage()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->drawerDetailsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerCardView;->endDrawerMessageContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method
