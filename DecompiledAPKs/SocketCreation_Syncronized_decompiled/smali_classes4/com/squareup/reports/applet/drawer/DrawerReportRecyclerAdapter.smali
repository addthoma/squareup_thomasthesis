.class public Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "DrawerReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final CASH_DRAWER_DETAILS_ROW:I = 0x0

.field private static final PAID_IN_OUT_HISTORY_ROW:I = 0x2

.field private static final PAID_IN_OUT_TITLE_AND_DIVIDER:I = 0x1

.field private static final TOTAL_PAID_IN_OUT_ROW:I = 0x3


# instance fields
.field private final BOTTOM_ROW_COUNT:I

.field private final TOP_ROW_COUNT:I

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final hasPaidInOutEvents:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;Z)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    .line 41
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->dateFormatter:Ljava/text/DateFormat;

    .line 42
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 43
    iput-boolean p4, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->hasPaidInOutEvents:Z

    const/4 p1, 0x1

    if-nez p4, :cond_0

    .line 47
    iput p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->TOP_ROW_COUNT:I

    const/4 p1, 0x0

    .line 49
    iput p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->BOTTOM_ROW_COUNT:I

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    .line 52
    iput p2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->TOP_ROW_COUNT:I

    .line 54
    iput p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->BOTTOM_ROW_COUNT:I

    :goto_0
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .line 95
    iget v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->TOP_ROW_COUNT:I

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getPaidInOutEventsSize()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->BOTTOM_ROW_COUNT:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 101
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->hasPaidInOutEvents:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    return v0

    .line 104
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->getItemCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-ne p1, v1, :cond_2

    const/4 p1, 0x3

    return p1

    :cond_2
    const/4 p1, 0x2

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->onBindViewHolder(Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;I)V
    .locals 2

    .line 80
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->getItemViewType()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 p2, 0x3

    if-eq v0, p2, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->bindTotalPaidInOutRow()V

    goto :goto_0

    .line 85
    :cond_1
    iget v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->TOP_ROW_COUNT:I

    sub-int/2addr p2, v0

    .line 86
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    invoke-virtual {v0, p2}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getPaidInOutEvent(I)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->bindPaidInOutHistoryRow(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)V

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;->bindCashDrawerDetailsRow()V

    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;
    .locals 3

    if-eqz p2, :cond_3

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 71
    sget p2, Lcom/squareup/reports/applet/R$layout;->paid_in_out_total_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 74
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "viewType not supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 68
    :cond_1
    sget p2, Lcom/squareup/reports/applet/R$layout;->paid_in_out_event_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 65
    :cond_2
    sget p2, Lcom/squareup/reports/applet/R$layout;->drawer_history_paid_in_out_title:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 62
    :cond_3
    sget p2, Lcom/squareup/reports/applet/R$layout;->drawer_history_cash_drawer_details:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 76
    :goto_0
    new-instance p2, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {p2, p1, v0, v1, v2}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;)V

    return-object p2
.end method
