.class public final Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "DrawerEmailScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;
    .locals 9

    .line 68
    new-instance v8, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cashmanagement/CashManagementSettings;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static/range {v1 .. v7}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen_Presenter_Factory;->get()Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
