.class public final Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;
.super Lcom/squareup/settings/cashmanagement/InCashManagementScope;
.source "CashManagementSettingsCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Component;,
        Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 147
    sget-object v0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$HisBvUBppKuyy187L11qDDsOd9s;->INSTANCE:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$CashManagementSettingsCardScreen$HisBvUBppKuyy187L11qDDsOd9s;

    .line 148
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    invoke-direct {v0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;
    .locals 1

    .line 149
    const-class v0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    .line 150
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    .line 151
    new-instance v0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 143
    invoke-super {p0, p1, p2}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;->getParentKey()Lcom/squareup/settings/cashmanagement/CashManagementScope;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 139
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->REPORTS_CURRENT_DRAWER_CASH_MANAGEMENT_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 135
    sget v0, Lcom/squareup/reports/applet/R$layout;->cash_management_settings_card_view:I

    return v0
.end method
