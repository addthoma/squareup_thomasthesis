.class Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "DrawerReportScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/DrawerReportScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/drawer/DrawerReportView;",
        ">;"
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

.field private final dateFormatter:Ljava/text/DateFormat;

.field final detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

.field private final flow:Lflow/Flow;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private orderedPaidInOutEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final printingDispatcher:Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

.field private final res:Lcom/squareup/util/Res;

.field private shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->flow:Lflow/Flow;

    .line 78
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 79
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 80
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 81
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 82
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    .line 83
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->printingDispatcher:Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    .line 84
    iput-object p8, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    .line 85
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->orderedPaidInOutEvents:Ljava/util/List;

    return-void
.end method

.method private createAdapter(Z)Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;
    .locals 3

    .line 141
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;-><init>(Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Z)V

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)I
    .locals 2

    .line 128
    iget-object p0, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 129
    iget-object p0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide p0

    .line 130
    invoke-static {p0, p1, v0, v1}, Ljava/lang/Long;->compare(JJ)I

    move-result p0

    return p0
.end method

.method private loadCashDrawerShift()V
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->screen()Lcom/squareup/reports/applet/drawer/DrawerReportScreen;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;->access$000(Lcom/squareup/reports/applet/drawer/DrawerReportScreen;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$ca-DLdskqI_BU8-Ply0hT-1fBtY;

    invoke-direct {v2, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$ca-DLdskqI_BU8-Ply0hT-1fBtY;-><init>(Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCashDrawerShift(Ljava/lang/String;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    return-void
.end method

.method private screen()Lcom/squareup/reports/applet/drawer/DrawerReportScreen;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/DrawerReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/DrawerReportView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;

    return-object v0
.end method


# virtual methods
.method public getPaidInOutEvent(I)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->orderedPaidInOutEvents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    return-object p1
.end method

.method public getPaidInOutEventsSize()I
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->orderedPaidInOutEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPaidInOutTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$loadCashDrawerShift$4$DrawerReportScreen$Presenter(Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V
    .locals 4

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 116
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    .line 117
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {p1, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->setShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 120
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 121
    iget-object v2, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eq v2, v3, :cond_1

    iget-object v2, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v2, v3, :cond_0

    .line 122
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_2
    sget-object p1, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$BnvtRdfDZ0WtITyincBdX8-YSeU;->INSTANCE:Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$BnvtRdfDZ0WtITyincBdX8-YSeU;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 133
    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->orderedPaidInOutEvents:Ljava/util/List;

    .line 134
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->orderedPaidInOutEvents:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->createAdapter(Z)Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->adapter:Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;

    .line 135
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerReportView;

    iget-object p1, p1, Lcom/squareup/reports/applet/drawer/DrawerReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->adapter:Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->adapter:Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerReportRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$DrawerReportScreen$Presenter()V
    .locals 2

    .line 94
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/DrawerReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/DrawerReportView;->hideKeyboard()V

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$DrawerReportScreen$Presenter()V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;

    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->screen()Lcom/squareup/reports/applet/drawer/DrawerReportScreen;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;->access$000(Lcom/squareup/reports/applet/drawer/DrawerReportScreen;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$DrawerReportScreen$Presenter()V
    .locals 4

    .line 103
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->printingDispatcher:Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->orderedPaidInOutEvents:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;->print(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Ljava/util/List;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 89
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 90
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->resetConfig()V

    .line 91
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->drawer_history_drawer_report:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 93
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$RtfWGesalAiJScO1TjJVu5P1gqE;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$RtfWGesalAiJScO1TjJVu5P1gqE;-><init>(Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/reports/applet/R$string;->drawer_history_email_drawer_report:I

    .line 98
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$5rOtKsTlqtn6_wbPkjaIaQBPw68;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$5rOtKsTlqtn6_wbPkjaIaQBPw68;-><init>(Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cashmanagement/R$string;->cash_drawer_print_report:I

    .line 102
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setThirdButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$Z4xzx-G6Dh-EqDgGnJWPtTvXhzM;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerReportScreen$Presenter$Z4xzx-G6Dh-EqDgGnJWPtTvXhzM;-><init>(Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showThirdButton(Ljava/lang/Runnable;)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen$Presenter;->loadCashDrawerShift()V

    return-void
.end method
