.class public Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;
.super Lcom/squareup/applet/AppletSection;
.source "CurrentDrawerSection.java"


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection$1;-><init>(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;

    return-object v0
.end method
