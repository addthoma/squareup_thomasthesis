.class public final Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;
.super Ljava/lang/Object;
.source "DrawerHistorySection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/drawer/DrawerHistorySection;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/drawer/DrawerHistorySection;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;-><init>(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/drawer/DrawerHistorySection;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistorySection_Factory;->get()Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    move-result-object v0

    return-object v0
.end method
