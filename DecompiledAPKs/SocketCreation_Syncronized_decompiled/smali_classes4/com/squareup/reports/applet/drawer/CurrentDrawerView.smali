.class public Lcom/squareup/reports/applet/drawer/CurrentDrawerView;
.super Landroid/widget/LinearLayout;
.source "CurrentDrawerView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCurrentDrawerView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CurrentDrawerView.kt\ncom/squareup/reports/applet/drawer/CurrentDrawerView\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 4 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,162:1\n1163#2:163\n1104#3,2:164\n52#4:166\n*E\n*S KotlinDebug\n*F\n+ 1 CurrentDrawerView.kt\ncom/squareup/reports/applet/drawer/CurrentDrawerView\n*L\n149#1:163\n149#1,2:164\n55#1:166\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0007\u0008\u0016\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0016J\u0008\u00101\u001a\u00020\u0008H\u0016J\u0008\u00102\u001a\u00020.H\u0014J\u0008\u00103\u001a\u00020.H\u0014J\u0012\u00104\u001a\u00020.2\u0008\u00105\u001a\u0004\u0018\u000106H\u0016J\u0012\u00107\u001a\u00020.2\u0008\u00108\u001a\u0004\u0018\u000109H\u0016J\u0008\u0010:\u001a\u00020.H\u0016J\u0008\u0010;\u001a\u00020.H\u0016J\u0008\u0010<\u001a\u00020.H\u0016J\u0008\u0010=\u001a\u00020.H\u0016J\u0016\u0010>\u001a\u00020.*\u00020\u00162\u0008\u0010?\u001a\u0004\u0018\u00010\u000eH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000f\u001a\u00020\u00108\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u001a\u001a\u00020\u001b8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR\u001e\u0010 \u001a\u00020!8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010#\"\u0004\u0008$\u0010%R\u000e\u0010&\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "cashDrawerDetails",
        "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;",
        "cashManagementDisabledStateMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "endDrawerButton",
        "Landroid/view/View;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "getFeatures$reports_applet_release",
        "()Lcom/squareup/settings/server/Features;",
        "setFeatures$reports_applet_release",
        "(Lcom/squareup/settings/server/Features;)V",
        "inProgressDrawerContainer",
        "Landroid/view/ViewGroup;",
        "loadingStateContainer",
        "paidInOutRow",
        "Lcom/squareup/ui/account/view/LineRow;",
        "presenter",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;",
        "getPresenter$reports_applet_release",
        "()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;",
        "setPresenter$reports_applet_release",
        "(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "getPriceLocaleHelper$reports_applet_release",
        "()Lcom/squareup/money/PriceLocaleHelper;",
        "setPriceLocaleHelper$reports_applet_release",
        "(Lcom/squareup/money/PriceLocaleHelper;)V",
        "shiftDescription",
        "Landroid/widget/EditText;",
        "startDrawerButton",
        "Lcom/squareup/ui/ConfirmButton;",
        "startDrawerContainer",
        "startingCash",
        "Lcom/squareup/widgets/SelectableEditText;",
        "configurePaidInOutRowWithCount",
        "",
        "count",
        "",
        "getActionBar",
        "onDetachedFromWindow",
        "onFinishInflate",
        "setShiftDescription",
        "description",
        "",
        "setStartingCashDefault",
        "startingCashAmount",
        "",
        "showCashManagementDisabledState",
        "showInProgressDrawerState",
        "showLoadingState",
        "showStartDrawerState",
        "setVisible",
        "childToShow",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private cashDrawerDetails:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

.field private cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

.field private endDrawerButton:Landroid/view/View;

.field public features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private inProgressDrawerContainer:Landroid/view/ViewGroup;

.field private loadingStateContainer:Landroid/view/ViewGroup;

.field private paidInOutRow:Lcom/squareup/ui/account/view/LineRow;

.field public presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private shiftDescription:Landroid/widget/EditText;

.field private startDrawerButton:Lcom/squareup/ui/ConfirmButton;

.field private startDrawerContainer:Landroid/view/ViewGroup;

.field private startingCash:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "getContext()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    const-class p2, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Component;

    .line 56
    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Component;->inject(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    return-void
.end method

.method public static final synthetic access$getShiftDescription$p(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)Landroid/widget/EditText;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->shiftDescription:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "shiftDescription"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getStartingCash$p(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 30
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startingCash:Lcom/squareup/widgets/SelectableEditText;

    if-nez p0, :cond_0

    const-string v0, "startingCash"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setShiftDescription$p(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Landroid/widget/EditText;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->shiftDescription:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$setStartingCash$p(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startingCash:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method private final setVisible(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 3

    .line 163
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    new-instance v2, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$setVisible$$inlined$getChildren$1;

    invoke-direct {v2, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$setVisible$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 164
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 150
    instance-of v2, v0, Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v2, :cond_0

    if-ne v0, p2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public _$_clearFindViewByIdCache()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->_$_findViewCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method

.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public configurePaidInOutRowWithCount(I)V
    .locals 2

    const-string v0, "paidInOutRow"

    const/4 v1, 0x1

    if-lt p1, v1, :cond_1

    .line 123
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->paidInOutRow:Lcom/squareup/ui/account/view/LineRow;

    if-nez v1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 125
    :cond_1
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->paidInOutRow:Lcom/squareup/ui/account/view/LineRow;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const-string v0, ""

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getFeatures$reports_applet_release()Lcom/squareup/settings/server/Features;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->features:Lcom/squareup/settings/server/Features;

    if-nez v0, :cond_0

    const-string v1, "features"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getPresenter$reports_applet_release()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getPriceLocaleHelper$reports_applet_release()Lcom/squareup/money/PriceLocaleHelper;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    if-nez v0, :cond_0

    const-string v1, "priceLocaleHelper"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .line 130
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->inProgressDrawerContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "inProgressDrawerContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    const-string v1, "presenter"

    if-nez v0, :cond_3

    .line 131
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->shiftDescription:Landroid/widget/EditText;

    if-nez v2, :cond_2

    const-string v3, "shiftDescription"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->saveShiftDescription(Ljava/lang/String;)V

    .line 133
    :cond_3
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v0, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashDrawerDetails:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    if-nez v2, :cond_5

    const-string v3, "cashDrawerDetails"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->dropView(Ljava/lang/Object;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->dropView(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    .line 135
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 62
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 63
    sget v0, Lcom/squareup/reports/applet/R$id;->start_drawer_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startDrawerContainer:Landroid/view/ViewGroup;

    .line 64
    sget v0, Lcom/squareup/reports/applet/R$id;->loading_state_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->loadingStateContainer:Landroid/view/ViewGroup;

    .line 65
    sget v0, Lcom/squareup/reports/applet/R$id;->current_drawer_starting_cash:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startingCash:Lcom/squareup/widgets/SelectableEditText;

    .line 66
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    if-nez v0, :cond_0

    const-string v1, "priceLocaleHelper"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startingCash:Lcom/squareup/widgets/SelectableEditText;

    if-nez v1, :cond_1

    const-string v2, "startingCash"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Lcom/squareup/text/HasSelectableText;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 67
    sget v0, Lcom/squareup/reports/applet/R$id;->current_drawer_start_drawer_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startDrawerButton:Lcom/squareup/ui/ConfirmButton;

    .line 68
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startDrawerButton:Lcom/squareup/ui/ConfirmButton;

    const-string v1, "startDrawerButton"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setButtonWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startDrawerButton:Lcom/squareup/ui/ConfirmButton;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$1;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    check-cast v1, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 77
    sget v0, Lcom/squareup/reports/applet/R$id;->in_progress_drawer_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->inProgressDrawerContainer:Landroid/view/ViewGroup;

    .line 78
    sget v0, Lcom/squareup/reports/applet/R$id;->shift_description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->shiftDescription:Landroid/widget/EditText;

    .line 79
    sget v0, Lcom/squareup/reports/applet/R$id;->end_drawer_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->endDrawerButton:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->endDrawerButton:Landroid/view/View;

    if-nez v0, :cond_4

    const-string v1, "endDrawerButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    sget v0, Lcom/squareup/reports/applet/R$id;->paid_in_out_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->paidInOutRow:Lcom/squareup/ui/account/view/LineRow;

    .line 88
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->paidInOutRow:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_5

    const-string v1, "paidInOutRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$3;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$3;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    sget v0, Lcom/squareup/reports/applet/R$id;->cash_drawer_details:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashDrawerDetails:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 97
    sget v0, Lcom/squareup/reports/applet/R$id;->cash_management_settings_disabled_state_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    .line 98
    sget-object v0, Lcom/squareup/noho/ResizeDrawable;->Companion:Lcom/squareup/noho/ResizeDrawable$Companion;

    .line 99
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v2, "cashManagementDisabledStateMessage"

    if-nez v1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v1}, Lcom/squareup/noho/NohoMessageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "cashManagementDisabledStateMessage.context"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    sget v3, Lcom/squareup/vectoricons/R$drawable;->device_cash_drawer_80:I

    .line 101
    sget v4, Lcom/squareup/noho/R$dimen;->noho_message_null_state_icon_height:I

    .line 98
    invoke-virtual {v0, v1, v3, v4}, Lcom/squareup/noho/ResizeDrawable$Companion;->createFromResources(Landroid/content/Context;II)Lcom/squareup/noho/ResizeDrawable;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v1, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoMessageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->features:Lcom/squareup/settings/server/Features;

    if-nez v0, :cond_8

    const-string v1, "features"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REMOTELY_CLOSE_OPEN_CASH_DRAWERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 106
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 107
    :cond_9
    sget v1, Lcom/squareup/reports/applet/R$string;->current_drawer_turn_on_cash_management:I

    .line 106
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    new-instance v1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$4;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$4;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    check-cast v1, Lcom/squareup/debounce/DebouncedOnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 117
    :cond_b
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    const-string v1, "presenter"

    if-nez v0, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    iget-object v0, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashDrawerDetails:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    if-nez v2, :cond_d

    const-string v3, "cashDrawerDetails"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->takeView(Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    if-nez v0, :cond_e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public final setFeatures$reports_applet_release(Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public final setPresenter$reports_applet_release(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->presenter:Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    return-void
.end method

.method public final setPriceLocaleHelper$reports_applet_release(Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public setShiftDescription(Ljava/lang/String;)V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->shiftDescription:Landroid/widget/EditText;

    if-nez v0, :cond_0

    const-string v1, "shiftDescription"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setStartingCashDefault(Ljava/lang/CharSequence;)V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startingCash:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_0

    const-string v1, "startingCash"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showCashManagementDisabledState()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->cashManagementDisabledStateMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_0

    const-string v1, "cashManagementDisabledStateMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->setVisible(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method

.method public showInProgressDrawerState()V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->inProgressDrawerContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "inProgressDrawerContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->setVisible(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method

.method public showLoadingState()V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->loadingStateContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "loadingStateContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->setVisible(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method

.method public showStartDrawerState()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->startDrawerContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "startDrawerContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->setVisible(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method
