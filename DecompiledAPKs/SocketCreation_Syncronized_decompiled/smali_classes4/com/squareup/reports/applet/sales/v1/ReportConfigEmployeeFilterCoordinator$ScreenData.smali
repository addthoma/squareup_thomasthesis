.class public abstract Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$Loading;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000f\u0010\u0011B/\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\n\u0082\u0001\u0003\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
        "",
        "showLoading",
        "",
        "allEmployeesButtonChecked",
        "filterByEmployeesButtonChecked",
        "employeeListHeaderVisible",
        "employeeListVisible",
        "(ZZZZZ)V",
        "getAllEmployeesButtonChecked",
        "()Z",
        "getEmployeeListHeaderVisible",
        "getEmployeeListVisible",
        "getFilterByEmployeesButtonChecked",
        "getShowLoading",
        "AllEmployees",
        "FilterByEmployee",
        "Loading",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$Loading;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allEmployeesButtonChecked:Z

.field private final employeeListHeaderVisible:Z

.field private final employeeListVisible:Z

.field private final filterByEmployeesButtonChecked:Z

.field private final showLoading:Z


# direct methods
.method private constructor <init>(ZZZZZ)V
    .locals 0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->showLoading:Z

    iput-boolean p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->allEmployeesButtonChecked:Z

    iput-boolean p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->filterByEmployeesButtonChecked:Z

    iput-boolean p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->employeeListHeaderVisible:Z

    iput-boolean p5, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->employeeListVisible:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZZZZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 143
    invoke-direct/range {p0 .. p5}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;-><init>(ZZZZZ)V

    return-void
.end method


# virtual methods
.method public final getAllEmployeesButtonChecked()Z
    .locals 1

    .line 145
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->allEmployeesButtonChecked:Z

    return v0
.end method

.method public final getEmployeeListHeaderVisible()Z
    .locals 1

    .line 147
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->employeeListHeaderVisible:Z

    return v0
.end method

.method public final getEmployeeListVisible()Z
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->employeeListVisible:Z

    return v0
.end method

.method public final getFilterByEmployeesButtonChecked()Z
    .locals 1

    .line 146
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->filterByEmployeesButtonChecked:Z

    return v0
.end method

.method public final getShowLoading()Z
    .locals 1

    .line 144
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;->showLoading:Z

    return v0
.end method
