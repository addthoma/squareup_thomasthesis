.class public Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;
.super Ljava/lang/Object;
.source "SalesChartEntry.java"


# instance fields
.field public final grossSalesFormatted:Ljava/lang/String;

.field public final grossSalesMoney:Lcom/squareup/protos/common/Money;

.field public final groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public final groupByValue:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

.field public final transactionCount:J


# direct methods
.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Ljava/lang/String;)V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->grossSalesFormatted:Ljava/lang/String;

    .line 27
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p2, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p2, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->grossSalesMoney:Lcom/squareup/protos/common/Money;

    .line 28
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object p2, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p2, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->transactionCount:J

    .line 29
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 30
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByValue:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Ljava/lang/String;)V
    .locals 2

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->grossSalesMoney:Lcom/squareup/protos/common/Money;

    const-wide/16 v0, 0x0

    .line 19
    iput-wide v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->transactionCount:J

    .line 20
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 21
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByValue:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    .line 22
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->grossSalesFormatted:Ljava/lang/String;

    return-void
.end method
