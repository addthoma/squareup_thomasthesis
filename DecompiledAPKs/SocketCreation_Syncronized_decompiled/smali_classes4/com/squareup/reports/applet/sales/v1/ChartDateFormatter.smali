.class public Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;
.super Ljava/lang/Object;
.source "ChartDateFormatter.java"


# instance fields
.field private calendar:Ljava/util/Calendar;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final hourFormat:Ljava/text/SimpleDateFormat;

.field private final monthFormat:Ljava/text/SimpleDateFormat;

.field private final shortDateFormat:Ljava/text/DateFormat;

.field private final shortMonthFormat:Ljava/text/SimpleDateFormat;

.field private final shortMonthNoYearFormat:Ljava/text/SimpleDateFormat;

.field private final shortNoYearDateFormat:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Ljava/util/Locale;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->dateFormat:Ljava/text/DateFormat;

    .line 38
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortNoYearDateFormat:Ljava/text/DateFormat;

    .line 39
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortDateFormat:Ljava/text/DateFormat;

    .line 40
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget p2, Lcom/squareup/reports/applet/R$string;->report_config_month_format:I

    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, p5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->monthFormat:Ljava/text/SimpleDateFormat;

    .line 41
    new-instance p1, Ljava/text/SimpleDateFormat;

    sget p2, Lcom/squareup/reports/applet/R$string;->report_config_month_short_format:I

    .line 42
    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, p5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortMonthFormat:Ljava/text/SimpleDateFormat;

    .line 43
    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string p2, "LLL"

    invoke-direct {p1, p2, p5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortMonthNoYearFormat:Ljava/text/SimpleDateFormat;

    .line 44
    sget p1, Lcom/squareup/reports/applet/R$string;->report_config_hour_format:I

    invoke-interface {p4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 45
    invoke-static {p5, p1}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 46
    new-instance p2, Ljava/text/SimpleDateFormat;

    invoke-direct {p2, p1, p5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->hourFormat:Ljava/text/SimpleDateFormat;

    .line 47
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->calendar:Ljava/util/Calendar;

    return-void
.end method

.method private timeRangeBeginToDate(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Ljava/util/Date;
    .locals 4

    .line 77
    new-instance v0, Ljava/util/Date;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public format(Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;ZZ)Ljava/lang/String;
    .locals 2

    .line 51
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-ne v0, v1, :cond_0

    .line 52
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByValue:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 53
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->calendar:Ljava/util/Calendar;

    const/16 p3, 0xb

    invoke-virtual {p2, p3, p1}, Ljava/util/Calendar;->set(II)V

    .line 54
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->hourFormat:Ljava/text/SimpleDateFormat;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->calendar:Ljava/util/Calendar;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 55
    :cond_0
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-ne v0, v1, :cond_3

    .line 56
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByValue:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->timeRangeBeginToDate(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Ljava/util/Date;

    move-result-object p1

    if-eqz p2, :cond_2

    if-eqz p3, :cond_1

    .line 59
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortDateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 61
    :cond_1
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortNoYearDateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 63
    :cond_2
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 65
    :cond_3
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByValue:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->timeRangeBeginToDate(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Ljava/util/Date;

    move-result-object p1

    if-eqz p2, :cond_5

    if-eqz p3, :cond_4

    .line 68
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortMonthFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 70
    :cond_4
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->shortMonthNoYearFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 72
    :cond_5
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->monthFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
