.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->screenData()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
        "state",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;
    .locals 1

    .line 80
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$InitialState;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$Loading;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$Loading;

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;

    goto :goto_0

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$AllEmployeesSelected;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;

    goto :goto_0

    .line 82
    :cond_1
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;->getListedEmployees()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$FilterByEmployee;-><init>(Ljava/util/List;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$screenData$2;->call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;)Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;

    move-result-object p1

    return-object p1
.end method
