.class Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$1;
.super Ljava/lang/Object;
.source "ReportConfigCardView.java"

# interfaces
.implements Lcom/squareup/register/widgets/date/DatePickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDaySelected(III)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->onDaySelected(III)V

    return-void
.end method

.method public onMonthChange(II)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->onDisplayedMonthChange(II)V

    return-void
.end method
